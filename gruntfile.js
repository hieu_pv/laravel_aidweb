/*jshint -W075 */
module.exports = function (grunt) {
  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin: {
      sitecss: {
        options: {
          banner: '/* Admin CSS */'
        },
        files: {
          'public_html/admin/css/site.css': [
            //'public_html/admin/css/fonts.css',
            'public_html/admin/bs3/css/bootstrap.css',
            //'public_html/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.css',
            'public_html/admin/css/bootstrap-reset.css',
            'public_html/admin/font-awesome/css/font-awesome.css',
            'public_html/admin/js/jvector-map/jquery-jvectormap-1.2.2.css',
            'public_html/admin/css/clndr.css',
            'public_html/admin/js/css3clock/css/style.css',
            'public_html/admin/js/morris-chart/morris.css',
            'public_html/admin/js/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.css',
            'public_html/admin/js/bootstrap-datepicker/css/datepicker.css',
            'public_html/admin/js/advanced-datatable/css/jquery.dataTables.css',
            'public_html/admin/js/data-tables/DT_bootstrap.css',
            'public_html/admin/js/darktooltip/darktooltip.min.css',
            'public_html/admin/js/iCheck/skins/flat/_all.css',
            'public_html/admin/js/ladda-bootstrap/dist/ladda-themeless.min.css',
            'public_html/admin/js/gritter/css/jquery.gritter.css',
            'public_html/admin/css/bootstrap-switch.css',
            'public_html/admin/js/bootstrap-fileupload/bootstrap-fileupload.css',
            'public_html/admin/css/bucket-ico-fonts.css',
            'public_html/packages/select2/select2.css',
            'public_html/packages/select2/select2-bootstrap.css',
            'public_html/admin/css/style.css',
            'public_html/admin/css/style-responsive.css'
          ],
          'public_html/css/aidweb.css': [
            //'public_html/fonts.css',
            'public_html/css/bootstrap.css',
            'public_html/style.css',
            'public_html/css/dark.css',
            'public_html/css/font-icons.css',
            'public_html/css/animate.css',
            'public_html/css/magnific-popup.css',
            'public_html/css/responsive.css',
            'public_html/js/perfect-scrollbar/perfect-scrollbar.css',
            'public_html/js/plugins/ladda-themeless.min.css',
          ]
        }
      }
    },
    uglify: {
      options: {
        compress: false
      },
      my_target: {
        files: {
          'public_html/admin/js/site.js': [
            'public_html/admin/js/jquery.js',
            //'public_html/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.js',
            'public_html/admin/bs3/js/bootstrap.js',
            //'public_html/admin/js/fleava.js',
            'public_html/admin/js/jquery.dcjqaccordion.2.7.js',
            'public_html/admin/js/jquery.scrollTo.min.js',
            'public_html/admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js',
            'public_html/admin/js/jquery.nicescroll.js',
            'public_html/admin/js/skycons/skycons.js',
            'public_html/admin/js/jquery.scrollTo/jquery.scrollTo.js',
            'public_html/admin/js/jquery.easing.min.js',
            'public_html/admin/js/calendar/clndr.js',
            'public_html/admin/js/calendar/moment-2.2.1.js',
            //'public_html/admin/js/jvector-map/jquery-jvectormap-1.2.2.min.js',
            //'public_html/admin/js/jvector-map/jquery-jvectormap-us-lcc-en.js',
            'public_html/admin/js/gauge/gauge.js',
            'public_html/admin/js/css3clock/js/css3clock.js',
            //'public_html/admin/js/chart-js/Chart.js',
            //'public_html/admin/js/chartjs.init.js',
            //'public_html/admin/js/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.all.min.js',
            'public_html/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'public_html/admin/js/advanced-datatable/js/jquery.dataTables.js',
            'public_html/admin/js/data-tables/DT_bootstrap.js',
            'public_html/admin/js/darktooltip/jquery.darktooltip.min.js',
            'public_html/admin/js/admin/jquery.browser.js',
            'public_html/admin/js/iCheck/jquery.icheck.js',
            'public_html/packages/underscore/underscore-min.js',
            'public_html/admin/js/jquery.blockUI.js',
            'public_html/packages/uri.js/src/URI.min.js',
            'public_html/admin/js/ladda-bootstrap/dist/spin.min.js',
            'public_html/admin/js/ladda-bootstrap/dist/ladda.min.js',
            'public_html/admin/js/gritter/js/jquery.gritter.js',
            'public_html/admin/js/bootstrap-switch.js',
            'public_html/admin/js/bootstrap-confirmation.js',
            'public_html/admin/js/bootstrap-fileupload/bootstrap-fileupload.js',
            'public_html/packages/select2/select2.min.js',
            'public_html/packages/angular/angular.min.js',
            'public_html/admin/js/scripts.js',
            'public_html/packages/jquery-validation/dist/jquery.validate.min.js',
            'public_html/packages/jquery-validation/dist/additional-methods.min.js',
          ],
          'public_html/admin/js/site-alt.js': [
            'public_html/admin/js/jquery.js',
            //'public_html/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.js',
            'public_html/admin/bs3/js/bootstrap.js',
            //  'public_html/admin/js/fleava.js',
            'public_html/admin/js/jquery.dcjqaccordion.2.7.js',
            'public_html/admin/js/jquery.scrollTo.min.js',
            'public_html/admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js',
            'public_html/admin/js/jquery.nicescroll.js',
            'public_html/admin/js/skycons/skycons.js',
            'public_html/admin/js/jquery.scrollTo/jquery.scrollTo.js',
            'public_html/admin/js/jquery.easing.min.js',
            'public_html/admin/js/calendar/clndr.js',
            'public_html/admin/js/calendar/moment-2.2.1.js',
            //'public_html/admin/js/jvector-map/jquery-jvectormap-1.2.2.min.js',
            //'public_html/admin/js/jvector-map/jquery-jvectormap-us-lcc-en.js',
            'public_html/admin/js/gauge/gauge.js',
            'public_html/admin/js/css3clock/js/css3clock.js',
            //'public_html/admin/js/chart-js/Chart.js',
            //'public_html/admin/js/chartjs.init.js',
            //'public_html/admin/js/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.all.min.js',
            'public_html/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'public_html/admin/js/advanced-datatable/js/jquery.dataTables.js',
            'public_html/admin/js/data-tables/DT_bootstrap.js',
            'public_html/admin/js/darktooltip/jquery.darktooltip.min.js',
            'public_html/admin/js/admin/jquery.browser.js',
            'public_html/admin/js/iCheck/jquery.icheck.js',
            'public_html/packages/underscore/underscore-min.js',
            'public_html/admin/js/jquery.blockUI.js',
            'public_html/packages/uri.js/src/URI.min.js',
            'public_html/admin/js/ladda-bootstrap/dist/spin.min.js',
            'public_html/admin/js/ladda-bootstrap/dist/ladda.min.js',
            'public_html/admin/js/gritter/js/jquery.gritter.js',
            'public_html/admin/js/bootstrap-switch.js',
            'public_html/admin/js/bootstrap-confirmation.js',
            'public_html/admin/js/bootstrap-fileupload/bootstrap-fileupload.js',
            'public_html/packages/select2/select2.min.js',
            'public_html/packages/angular/angular.min.js',
            'public_html/admin/js/scripts.js',
            'public_html/packages/jquery-validation/dist/jquery.validate.min.js',
            'public_html/packages/jquery-validation/dist/additional-methods.min.js',
          ],
          'public_html/admin/js/admin/script.js': [
            'public_html/admin/js/admin/startup.js',
            'public_html/admin/js/admin/common_libraries.js',
            'public_html/admin/js/admin/app.js',
            'public_html/admin/js/admin/controllers_for_change_password.js',
            'public_html/admin/js/admin/controllers_for_blog.js',
            'public_html/admin/js/admin/controllers_for_news.js',
            'public_html/admin/js/admin/controllers_for_takeactions.js',
            'public_html/admin/js/admin/controllers_for_videos.js',
            //'public_html/admin/js/admin/controllers_for_organisations.js',
            //'public_html/admin/js/admin/controllers_for_individuals.js',
            //'public_html/admin/js/admin/controllers_for_publicities.js',
            'public_html/admin/js/admin/manager_for_blog.js',
            'public_html/admin/js/admin/manager_for_news.js',
            'public_html/admin/js/admin/manager_for_takeactions.js',
            'public_html/admin/js/admin/manager_for_videos.js',
            //'public_html/admin/js/admin/manager_for_organisations.js',
            //'public_html/admin/js/admin/manager_for_individuals.js',
            //'public_html/admin/js/admin/manager_for_publicities.js',
            //'public_html/admin/js/admin/manager_for_media_gallery.js',
          ],
          'public_html/js/aidweb.js': [
            //'public_html/js/jquery.js',
            'public_html/js/plugins.js',
            //'public_html/js/plugins/clamp.js',
            'public_html/js/plugins/jquery.dotdotdot.js',
            'public_html/js/plugins/spin.min.js',
            'public_html/js/plugins/ladda.min.js',
            'public_html/js/plugins/jquery.lazyload.js',
            'public_html/js/perfect-scrollbar/perfect-scrollbar.jquery.min.js',
            'public_html/packages/angular/angular.min.js',
            'public_html/packages/angular-sanitize/angular-sanitize.min.js',
            //'public_html/js/app/core.js',
          ]
        }
      }
    },
    copy: {
      main: {
        files: [
          // // includes files within path
          // {expand: true, src: ['path/*'], dest: 'dest/', filter: 'isFile'},

          // makes all src relative to cwd
          // includes files within path and its sub-directories
          {expand: true, cwd: 'public_html/admin/bs3/fonts/', src: ['**'], dest: 'public_html/admin/fonts/'},
          {expand: true, cwd: 'public_html/admin/js/jquery-ui/images/', src: ['**'], dest: 'public_html/admin/css/images/'},
          {expand: true, cwd: 'public_html/admin/font-awesome/fonts/', src: ['**'], dest: 'public_html/admin/fonts/'},
          {expand: true, cwd: 'public_html/admin/css3clock/images/', src: ['**'], dest: 'public_html/admin/images/'},
          {expand: true, cwd: 'public_html/admin/advanced-datatable/images/', src: ['**'], dest: 'public_html/admin/images/'},
          {expand: true, cwd: 'public_html/admin/js/data-tables/images/', src: ['**'], dest: 'public_html/admin/css/images/'},
          {expand: true, cwd: 'public_html/admin/js/iCheck/skins/flat', src: ['**'], dest: 'public_html/admin/css/'},
          {expand: true, cwd: 'public_html/admin/js/gritter/images/', src: ['**'], dest: 'public_html/admin/images/'},
          {expand: true, cwd: 'public_html/packages/select2/', src: ['*.png'], dest: 'public_html/admin/css/'},
          {expand: true, cwd: 'public_html/packages/select2/', src: ['*.gif'], dest: 'public_html/admin/css/'},

          // // makes all src relative to cwd
          // {expand: true, cwd: 'path/', src: ['**'], dest: 'dest/'},

          // // flattens results to a single level
          // {expand: true, flatten: true, src: ['path/**'], dest: 'dest/', filter: 'isFile'},
        ],
      },
    }
  });
  // Default task.
  grunt.registerTask('default', ['uglify', 'cssmin', 'copy']);
};
