-- Status:65:1497:MP_0:aidweb:php:1.24.4::5.5.42-cll-lve:1:::utf8:EXTINFO
--
-- TABLE-INFO
-- TABLE|bases|0|16384||InnoDB
-- TABLE|beneficiaries|35|16384||InnoDB
-- TABLE|blog_posts|18|163840||InnoDB
-- TABLE|blog_posts_beneficiaries|10|49152||InnoDB
-- TABLE|blog_posts_countries|7|49152||InnoDB
-- TABLE|blog_posts_crisis|7|49152||InnoDB
-- TABLE|blog_posts_interventions|0|49152||InnoDB
-- TABLE|blog_posts_regions|12|49152||InnoDB
-- TABLE|blog_posts_sectors|10|49152||InnoDB
-- TABLE|blog_posts_themes|12|49152||InnoDB
-- TABLE|comments|1|16384||InnoDB
-- TABLE|contacts|8|16384||InnoDB
-- TABLE|countries|239|16384||InnoDB
-- TABLE|crisis|53|16384||InnoDB
-- TABLE|interventions|4|16384||InnoDB
-- TABLE|level_of_activities|3|16384||InnoDB
-- TABLE|level_of_responsibilities|3|16384||InnoDB
-- TABLE|likes|10|16384||InnoDB
-- TABLE|member_statuses|3|16384||InnoDB
-- TABLE|migrations|56|16384||InnoDB
-- TABLE|nationalities|193|16384||InnoDB
-- TABLE|news_items|12|147456||InnoDB
-- TABLE|news_items_beneficiaries|11|49152||InnoDB
-- TABLE|news_items_countries|9|49152||InnoDB
-- TABLE|news_items_crisis|10|49152||InnoDB
-- TABLE|news_items_interventions|0|49152||InnoDB
-- TABLE|news_items_regions|12|49152||InnoDB
-- TABLE|news_items_sectors|10|49152||InnoDB
-- TABLE|news_items_themes|12|49152||InnoDB
-- TABLE|office_locations|0|16384||InnoDB
-- TABLE|office_types|3|16384||InnoDB
-- TABLE|organisation_types|11|16384||InnoDB
-- TABLE|organisations|47|131072||InnoDB
-- TABLE|password_reminders|0|81920||InnoDB
-- TABLE|platforms|21|16384||InnoDB
-- TABLE|platforms_profiles|85|16384||InnoDB
-- TABLE|platforms_top_profiles|86|16384||InnoDB
-- TABLE|professional_statuses|3|16384||InnoDB
-- TABLE|publicities|10|81920||InnoDB
-- TABLE|publicities_platforms|10|16384||InnoDB
-- TABLE|regions|7|16384||InnoDB
-- TABLE|registrations|76|16384||InnoDB
-- TABLE|sectors|36|16384||InnoDB
-- TABLE|settings|4|16384||InnoDB
-- TABLE|take_actions|13|81920||InnoDB
-- TABLE|take_actions_beneficiaries|13|49152||InnoDB
-- TABLE|take_actions_countries|4|49152||InnoDB
-- TABLE|take_actions_crisis|0|49152||InnoDB
-- TABLE|take_actions_interventions|0|49152||InnoDB
-- TABLE|take_actions_regions|13|49152||InnoDB
-- TABLE|take_actions_sectors|9|49152||InnoDB
-- TABLE|take_actions_themes|13|49152||InnoDB
-- TABLE|themes|25|16384||InnoDB
-- TABLE|types_of_publicities|3|16384||InnoDB
-- TABLE|types_of_take_actions|7|16384||InnoDB
-- TABLE|user_profiles|77|131072||InnoDB
-- TABLE|users|77|32768||InnoDB
-- TABLE|videos|14|65536||InnoDB
-- TABLE|videos_beneficiaries|14|49152||InnoDB
-- TABLE|videos_countries|8|49152||InnoDB
-- TABLE|videos_crisis|10|49152||InnoDB
-- TABLE|videos_interventions|0|49152||InnoDB
-- TABLE|videos_regions|14|49152||InnoDB
-- TABLE|videos_sectors|10|49152||InnoDB
-- TABLE|videos_themes|14|49152||InnoDB
-- EOF TABLE-INFO
--
-- Dump by MySQLDumper 1.24.4 (http://mysqldumper.net)
/*!40101 SET NAMES 'utf8' */;
SET FOREIGN_KEY_CHECKS=0;
-- Dump created: 2015-08-15 07:02

--
-- Create Table `bases`
--

DROP TABLE IF EXISTS `bases`;
CREATE TABLE `bases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `bases`
--

/*!40000 ALTER TABLE `bases` DISABLE KEYS */;
/*!40000 ALTER TABLE `bases` ENABLE KEYS */;


--
-- Create Table `beneficiaries`
--

DROP TABLE IF EXISTS `beneficiaries`;
CREATE TABLE `beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `beneficiaries`
--

/*!40000 ALTER TABLE `beneficiaries` DISABLE KEYS */;
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Animals','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Environment','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Children & Youth','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Disaster affected','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Enterpreneurs','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('6','Families','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('7','Farmers','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('8','General Population','0','2015-05-04 09:59:36','2015-05-19 08:57:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('9','Handicapped','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('10','Homeless','1','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('11','Virus infected','0','2015-05-04 09:59:37','2015-05-19 08:59:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('12','Indigenous People','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('13','Internally Displaced','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('14','Illiterate & Uneducated','0','2015-05-04 09:59:37','2015-05-19 09:00:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('15','Older People','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('16','Orphans','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('17','Poorest','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('18','Refugees','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('19','Right-Deprived','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('20','Small-scale Farmers','1','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('21','Stateless','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('22','Unemployed','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('23','Victims of domectic violence','0','2015-05-04 09:59:37','2015-05-19 08:50:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('24','War affected','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('25','Women','0','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('26','Victims of armed conflict','0','2015-05-19 08:50:00','2015-05-19 08:50:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('27','Fauna & FLora','0','2015-05-19 08:57:00','2015-05-19 08:57:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('28','Kidnapped & Missing','0','2015-05-19 09:00:00','2015-05-19 09:00:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('29','Prisoners','0','2015-05-19 09:01:00','2015-05-19 09:01:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('30','Victims of torture','0','2015-05-19 09:02:00','2015-05-19 09:03:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('31','Victims of disaster','0','2015-05-19 09:02:00','2015-05-19 09:02:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('32','Peace & Security','1','2015-05-19 09:35:00','2015-05-19 09:35:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('33','Population & Settlement','1','2015-05-19 09:36:00','2015-05-19 09:36:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('34','Population & Settlement','1','2015-05-19 09:36:00','2015-05-19 09:36:00');
INSERT INTO `beneficiaries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('35','Homeless','0','2015-07-11 06:13:00','2015-07-11 06:13:00');
/*!40000 ALTER TABLE `beneficiaries` ENABLE KEYS */;


--
-- Create Table `blog_posts`
--

DROP TABLE IF EXISTS `blog_posts`;
CREATE TABLE `blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `image_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_legend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments_count` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `blog_posts_organisation_id_index` (`organisation_id`),
  KEY `blog_posts_user_profile_id_index` (`user_profile_id`),
  KEY `blog_posts_user_id_index` (`user_id`),
  CONSTRAINT `blog_posts_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `blog_posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `blog_posts_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts`
--

/*!40000 ALTER TABLE `blog_posts` DISABLE KEYS */;
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('51','16955ADD-B19F-45BA-A2A0-6D01E0D4132A','Solutions for Inclusive, Green, and Resilient Cities','solutions-for-inclusive-green-and-resilient-cities','Speech by ADB President Takehiko Nakao at the Solutions for Inclusive, Green, and Resilient Cities Delhi Sustainable Development Summit (DSDS) 2015: Sustainable Development Goals and Dealing with Climate Change on 5 February 2015 in Delhi, India.<br />\r\n<br />\r\n<br />\r\nIntroduction<br />\r\n<br />\r\nYour Excellencies, Distinguished Guests, Ladies and Gentlemen:<br />\r\n<br />\r\nI am very pleased to speak to you today about &ldquo;Solutions for Inclusive, Green, and Resilient Cities&rdquo;. Before getting specifically into this topic, I would like to set the stage by talking more broadly about the international development agenda in 2015 &ndash; in particular, the Sustainable Development Goals (SDGs) and the prospects for a new global climate agreement.<br />\r\n<br />\r\n2015 will be a milestone year for international development. An agreement on the SDGs and the post 2015 agenda is expected to be reached at the September United Nations (UN) summit in New York. In December, a new climate deal is also expected to be agreed at COP 21 in Paris.<br />\r\n<br />\r\nIn July 2014, the UN Open Working Group (OWG) on the SDGs delivered its final report. The SDGs place environmental and social sustainability at the core of the new integrated development agenda. At the same time, the SDGs aim to eradicate extreme poverty, and tackle other unfinished business of the Millennium Development Goals (MDGs) by 2030 including for education and health. Economic transformation will be key to achieving many of the goals.<br />\r\n<br />\r\nThe global development community is now finalizing the SDGs. ADB supports key proposals of the SDGs. I want to stress 3 points about the transition from the MDGs to the SDGs. First, we still need to fully achieve the MDGs agenda. Second, poverty and inclusive growth cannot be set aside on environmental grounds. Third, neither can the environment be neglected in the pursuit of growth. Upholding these principles will be essential for sustainable development.<br />\r\n<br />\r\nAlong with the SDGs, the international community has never been so close to agreeing on a global deal on climate change involving all the important players. Intensive negotiations are ongoing in the lead-up to COP 21 in Paris in December.<br />\r\n<br />\r\nThe Lima Call for Climate Action agreed at COP 20 last December provides the framework for several elements of the new agreement and establishes ground rules on how nations can submit their contributions to the climate change agenda. Countries also agreed to raise adaptation to the same level as mitigation action.<br />\r\n<br />\r\nThe issue of climate finance will be critical for the new agreement. Recent pledging for the Green Climate Fund is an encouraging signal. Developed countries need to show leadership in global climate actions including technological innovation and transfer while developing countries will also contribute according to the principle of common but differentiated responsibility.<br />\r\n<br />\r\nI believe ADB and other multilateral development banks should make a key contribution to financing both SDGs and global climate actions. In doing so, special attention should be paid to their role of catalyzing public and private resources.<br />\r\nReshaping Asia&rsquo;s booming cities is the key to meeting the SDG and climate change goals<br />\r\n<br />\r\nNow I would like to go to the main topic of my discussion today, which is &quot;Solutions for Inclusive, Green and Resilient Cities&quot;. Cities are the key to meeting the SDGs and the global climate goals. The proposal for SDGs identified sustainable urban development as a critical area of focus. Several SDGs call for actions mainly in urban areas where most economic activities and investments take place. And cities are where much of the climate mitigation and adaptation action will have to take place.<br />\r\n<br />\r\nWhile cities are engines of growth, poverty and disparities are increasing.<br />\r\n<br />\r\nCities have driven economic growth in Asia &ndash; now producing about 80% of GDP &ndash; and have lifted tens of million out of poverty, especially in the last two decades. Asia is the most rapidly urbanizing region in the world. More than half of the world&rsquo;s largest cities are in Asia. By 2050, 3 billion people &ndash; about 65% of all Asians &ndash; will live in cities. This urbanization process should be in tandem with further economic growth.<br />\r\n<br />\r\nHowever, rapid urbanization also means the urbanization of poverty. Of a total of about 1.6 billion urban people in Asia, more than 500 million live in high density, degraded slums. Asia accounts for about 60% of the world&rsquo;s slum dwellers. Large disparities have emerged in urban areas, and the poor are the most vulnerable to economic and environmental shocks. It is clear that the way cities are developed and managed will heavily influence the effort to eradicate poverty in Asia.<br />\r\n<br />\r\nEnvironmental sustainability remains a major concern. As cities swell in size and numbers, they are under increasing environmental stress. Cities struggle with air and water pollution, traffic congestion, inadequate solid waste management and wastewater treatment. Only about 10% of solid waste ends up in properly managed landfill sites. Current levels of Investment in water, sanitation, housing and other urban infrastructure are not sufficient to cope with such a rapid urbanization process.<br />\r\n<br />\r\nVulnerability to climate change should also be addressed. Rapid and often poorly managed urbanization intensifies climate change risks and amplifies its impacts on infrastructure. Asian cities are especially vulnerable to the hazards caused by climate change: 238 million Asian urban poor are expected to be hit first and hardest by the effects of climate change. Natural disasters routinely erase 1% to 5% of GDP each year and this figure is increasing. Several Asian cities, including Dhaka, Kolkata, Mumbai, Shanghai, Bangkok, Yangon, and Manila, are at risk of coastal flooding as sea levels rise. With the cost of natural disasters set to rise in the near future, cities will become a major battleground in the region&rsquo;s fight against climate change.<br />\r\nPriorities for action: Inclusive, green, and resilient cities<br />\r\n<br />\r\nTo reduce poverty, address environmental challenges and mitigate climate related disasters, we must focus on building inclusive, green, and resilient cities.<br />\r\n<br />\r\nFirst, inclusive growth is key to reduce poverty. Creating inclusive cities means giving the urban poor better access to basic services such as primary health care, education, water, affordable transport and adequate housing. Inclusive cities should also expand quality job opportunities for the poor.<br />\r\n<br />\r\nADB supports such efforts in our developing member countries. For example, In India, we are working with the government to support its National Urban Health Mission (NUHM) efforts to strengthen urban primary health systems. The improved quality of urban health services is expected to benefit about 400 million people, including about 70 million currently living in urban slums. More than half the beneficiaries will be women, since maternal and child health services comprise a large part of urban primary health care.<br />\r\n<br />\r\nADB is also firmly committed to supporting India&rsquo;s 100 Smart Cities Initiatives. The use of technology and intelligent systems will improve urban services for the poor, including sanitation and affordable transportation.<br />\r\n<br />\r\nSecond, to make cities greener, they must become more energy and resource efficient through promotion of low carbon development and smart use of land and water. They need to invest more in mass public transport systems and better waste and wastewater management. Transport systems need to be better integrated. For example, in Vientiane, Laos, ADB is supporting mass public transport systems including Bus Rapid Transit (BRT) linked to cycle and pedestrian pathways. In this way, 700,000 people can easily switch from one mode of transport to another.<br />\r\n<br />\r\nAnother example of ADB&rsquo;s support for &ldquo;green&rdquo; cities is a private sector loan to develop waste-to-energy projects in more than 20 secondary cities across the People&rsquo;s Republic of China. As of June 2014, 12 of these plants are already in operation. Together, they can process about 4.6 million tonnes of household waste annually, generating approximately 1.3 billion kilowatt hours of on-grid electricity each year.<br />\r\n<br />\r\nThird, to make cities more resilient, decision makers should always consider natural hazards and climate change risks when designing cities and urban infrastructure. Relatively small up-front investments can save lives and avoid large scale infrastructure rebuilding and rehabilitation costs later. Asia needs to invest more in climate resilient infrastructure such as enlarged drainage systems in cities, elevated roads, and bigger storm-water retention reservoirs to accommodate variations in rainfall.<br />\r\n<br />\r\nInnovation and knowledge partnerships will be crucial in these efforts. At ADB, all investment projects are screened for climate risks; those at risk undergo climate impact assessments to ensure we can plan, build and manage investments that are more resilient.<br />\r\n<br />\r\nHere, I would like to mention an ADB-supported project in Bangladesh. The Coastal Towns Environmental Infrastructure Improvement Project is designed to reduce the vulnerability of municipal infrastructure &ndash; such as water supply, sanitation, solid waste management and bus terminals &ndash; to climate change and disaster risks. The project also includes investments in cyclone shelters, emergency access roads and bridges, and stronger homes in slums.<br />\r\nImportance of governance and institutions<br />\r\n<br />\r\nFinally, inclusive, green and resilient cities need better governance and more effective institutions. Greater use of integrated urban planning, better use of land, and more timely investments in sustainable and resilient infrastructure, are all essential. Cities also need additional funding beyond traditional sources. For example, they can consider the development of new finance instruments to better mobilize local resources &ndash; such as municipal bond mechanisms and public private partnerships (PPPs).<br />\r\n<br />\r\nOne example of innovative financing is the use of private equity funds to promote PPPs in the Philippines. The Philippine Investment Alliance for Infrastructure, whose investors comprise ADB, the Philippine government pension fund, a Dutch pension fund manager and an Australian private investment bank, is designed to draw other private funds into infrastructure.<br />\r\nClosing<br />\r\n<br />\r\nIn closing, I would like to commit that ADB will continue to play an important role in the development of inclusive, green and resilient cities and the success of the SDGs and the global Climate Change agenda.','/uploads/media/109/posts/Nakao-ADB.jpg','http://holduparchitecture.com/MONT-PARNASSE','','0','1','0','1','2015-05-20 01:05:08','2015-07-09 11:42:53',NULL,'109','109','6');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('52','258F4F9F-46D6-4863-A563-1E95AD168BB6','Photo of the Week: Vanuatu’s ‘10,000 in 10’ campaign','photo-of-the-week-vanuatus-10000-in-10-campaign','Vanuatu, 2015: Tropical Cyclone Pam, which hit on 13 March, has disrupted access to safe water and sanitation in the South Pacific island nation, increasing children&rsquo;s risk of water- and vector-borne diseases.<br />\r\n<br />\r\nThe &lsquo;10,000 in 10&rsquo; campaign, launched on 18 March, aims to immunize 10,000 children 6 to 59 months of age against measles and rubella, in 21 villages over a period of 10 days.<br />\r\n<br />\r\nIn this photo, children near the shore in Port Vila watch a boat further out that is taking an immunization team to a nearby island.<br />\r\n<br />\r\nTo see more images from UNICEF visit UNICEF Photography (http://www.unicef.org/photography/photo_app.php).','/uploads/media/132/posts/Nesbitt-Unicef.jpg','©UNICEF/ NYHQ2015-0523/Sokhin','','0','1','0','1','2015-05-20 01:21:52','2015-07-09 11:41:40',NULL,'132','132','11');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('53','8135ED9F-4CD4-49B4-B53C-D2AE8D81DEEB','Filling the Void: Thinking About Limited Data in the Developing World','filling-the-void-thinking-about-limited-data-in-the-developing-world','Watching baseball&rsquo;s Opening Day this week reminded me of how the sport sparked my passion for numbers and statistics at an early age. One of my science fair projects way back when was on a (very limited) statistical analysis of whether or not expansion teams to Major League Baseball benefited pitchers more than batters.<br />\r\n<br />\r\nWhile the use of sabermetrics is relatively new to the game (I&rsquo;d highly recommend reading Moneyball if you haven&rsquo;t already), statistics have been a part of baseball since the 19th century and are as much a part of the game as hot dogs and athletic cups.<br />\r\n<br />\r\nThe greats of baseball were defined by their stats, from their tally of home runs or stolen bases to the number of World Series they led their teams to. Listening to or watching baseball games, you&rsquo;ll hear the commentator pull some of the most ridiculous statistics, which makes you think about how each part of the game is tracked in incredible detail &mdash; down to which team&rsquo;s players sport the most facial hair.<br />\r\nStudents participate in a local mapping project in Haiti in 2013. / Kendra Helmer, USAID<br />\r\n<br />\r\nWith all the data that&rsquo;s collected and pored over for baseball and other sports in the United States, you would think data would be as easily accessible across all areas. Unfortunately, that&rsquo;s not the case, especially for our work in developing countries. When working with statistics about the developing world, you find a lot of holes due to a whole host of problems. And for the data we do have access to, much of it is outdated or unreliable, as mentioned in a report released by the Center for Global Development last summer:<br />\r\n<br />\r\n&ldquo;But nowhere in the world is the need for better data more urgent than in sub-Saharan Africa &mdash; the region with perhaps the most potential for progress under a new development agenda. Despite a decade of rapid economic growth in most countries, the accuracy of the most basic data indicators such as GDP, number of kids attending school, and vaccination rates remains low, and improvements have been sluggish.<br />\r\n<br />\r\nThis is a problem especially apparent as the United Nations cultivates the Sustainable Development Goals, the successor to the Millennium Development Goals. In order to determine progress toward the 17 goals, the United Nations needs to collect good data to track a wide range of indicators. In the search for good data, it must accept imperfection as is done with plenty of statistics and data in the developed world.<br />\r\n<br />\r\nAs USAID, other agencies and donors conduct evaluations and analyses to identify critical areas or assess projects effectiveness, the work can often be hindered by the lack of (usable) data.<br />\r\n<br />\r\nTo address the data void, USAID is finding new and innovative ways for collection and measurement. For instance, the USAID GeoCenter has been engaging with university students in the United States and host countries through mapathons to chart unmapped areas of the world, such as Nepal, Bangladesh and the Philippines. The mapping data, openly shared, provide USAID and partners with better baseline information for monitoring projects. When combined with household surveys, the data can improve analyses and understanding of specific areas of vulnerability within a country.<br />\r\n<br />\r\nThe development community is far from reaching the level and reliability of statistics collected on Major League Baseball, which has been allowing general managers, coaches and fantasy baseball fanatics to make more informed decisions to improve their teams for decades.<br />\r\n<br />\r\nBy concentrating efforts to alleviate some of the systematic problems that lead to a lack of data in the first place, the development community would not just be improving access to reliable data, but would be solving some of the underlying problems of developing countries in the first place.<br />\r\n<br />\r\nWith more abundant, reliable and geocoded data about the developing world, USAID and other organizations can make more informed decisions about how to better target poverty, helping us reach the goal of eradicating extreme poverty by 2030.&nbsp;<br />\r\nRELATED LINKS:<br />\r\n<br />\r\nRead more about the Center for Global Development&rsquo;s recommendations for improving data access and quality&nbsp;Check out this video of Geographer Carrie Stokes explaining the USAID GeoCenter&rsquo;s work with Texas Tech to map an unmapped part of Bangladesh<br />\r\nFollow @USAIDEconomic and @GlobalDevLab','/uploads/media/131/posts/Chafetz.jpg','','Ryan Zimmerman, third baseman for the Washington Nationals, on his way to homeplate to be mobbed by his ecstatic teammates after hitting a game-winning home run against the Atlanta Braves on opening night at the inaugural game played at Nationals Park in ','0','1','0','1','2015-05-20 01:28:51','2015-07-13 23:34:39',NULL,'131','131','3');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('54','3ED8C23D-9875-40A6-89CF-F48905FC5EB7','Tools to integrate children’s rights into business','tools-to-integrate-childrens-rights-into-business','We received quite a bit of feedback on last month&rsquo;s post about UNICEF&rsquo;s new partnership with the LEGO Group (http://blogs.unicef.org/2015/03/13/unicef-the-lego-group-team-up-for-child-rights/), and several questions asking exactly how UNICEF works with companies and what it means to &lsquo;integrate children&rsquo;s rights,&rsquo; into business practice.<br />\r\n<br />\r\nWhen you ask: &ldquo;How does UNICEF work with companies on these issues?&rdquo; the answer is that we guide them in using four tools to understand: (1) where children&rsquo;s rights fit into their policies, (2) how their activities impact children, (3) how they should engage various stakeholders (from suppliers to employees) during the process of improving respect for children in their business, and (4) how they can monitor and report on their respect for children and other sustainability activities.<br />\r\n<br />\r\nThe 4 corresponding tools are:<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp; The Children&rsquo;s Rights in Policies and Codes of Conduct tool,<br />\r\n&nbsp;&nbsp;&nbsp; The Children&rsquo;s Rights in Impact Assessment tool,<br />\r\n&nbsp;&nbsp;&nbsp; The Engaging Stakeholders on Children&rsquo;s Rights tool, and<br />\r\n&nbsp;&nbsp;&nbsp; The Children&rsquo;s Rights in Sustainability Reporting tool.<br />\r\n<br />\r\n(1) The Children&rsquo;s Rights in Policies and Codes of Conduct tool can assist a company in integrating children&rsquo;s rights elements into its existing policies. Let&rsquo;s use a company&rsquo;s marketing and communications policies as an example. How does the tool work? Well, first, the tool walks the company through a series of questions testing the extent to which existing policies consider children&rsquo;s rights. Then, the tool references the relevant Children&rsquo;s Rights and Business Principle (which in the case of marketing, refers to Principle 6) and goes through a list of relevant considerations.<br />\r\n<br />\r\nFor example: Does the company&rsquo;s existing marketing and product labeling policies ensure that parents and children are empowered to make informed choices? This includes a policy that specifies the minimum age for child-targeted advertisement and what defines child-targeted advertisement for that company.<br />\r\n<br />\r\n(2) The Children&rsquo;s Rights in Impact Assessment tool identifies impact assessment criteria for each of the Children&rsquo;s Rights and Business Principles that a company can use when assessing its impact in each of the business areas. For example, sticking with Principle 6 (related to marketing) the impact assessment criteria proposed by the tool cover three areas of business: (1) policy, (2) due diligence, and (3) remediation.<br />\r\n<br />\r\nGlance at page 37 of the tool and you will find a table that specifically lays out imperative policy criteria for marketing and advertising, as well as the specific actions a company can take to meet that criterion if they currently do not. For instance, the table encourages companies to ask whether or not their current marketing policies take into account the evolving impacts related to the use of digital media, including the use of personalized promotions aimed specifically at children.<br />\r\n<br />\r\n(3) Clearly companies will need to engage different stakeholders when actively working to enhance their standards and practices at both the corporate and site levels. The Engaging Stakeholders on Children&rsquo;s Rights tool aids companies in determining the relevance and appropriate level of engagement with each type of stakeholder. With regards to Principle 6, this tool can be used to identify which stakeholders to engage when developing child-friendly marketing policies. This might include engaging with children directly to study the impact of marketing and advertising on their behavior through focus group interviews, surveys, etc.<br />\r\n<br />\r\n(4) The Children&rsquo;s Rights in Sustainability Reporting tool simply guides business in how to report on children&rsquo;s rights against the reporting framework provided by the Global Reporting Initiative (GRI). The tool includes examples of company information to report on.<br />\r\n<br />\r\nRelated to Principle 6, this means pointing out that a company should report on whether or not they have, for example, formal mechanisms for complaints concerning violations relating to children&rsquo;s rights in the context of marketing and advertising. This tool also suggests indicators that can be used to report on each of the Principles. For Principle 6, a possible indicator is &ldquo;programmes for adherence to laws, standards, and voluntary codes related to marketing communications, including advertising, promotion, and sponsorship.&rdquo;<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp; Full copies of the 4 tools for companies are freely available online (http://www.unicef.org/csr/88.htm).','/uploads/media/135/posts/Nylund-Unicef.jpg','UNICEF/2013/Pirozzi','Two small children play with toy xylophones at an ECD center in Bolivia. ','0','1','0','1','2015-05-21 07:10:36','2015-07-09 11:43:36',NULL,'135','135','3');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('55','2EACE5D7-E081-43DE-8A70-A7EA9A7F8B52','The New Alliance for Food Security & Nutrition: Big risk to small farmers','the-new-alliance-for-food-security','<p>Today over 1 billion people live in extreme poverty, surviving on less than &pound;1 a day, and an further 805 million people continue to go hungry, the majority of them children. Given the level of need the UK Government, like many other donors, are increasingly looking to others to support in the fight against hunger. Progressively, donors are turning to businesses to play their role &ndash; not as contractors but as fully-contributing as partners in development.&nbsp;</p>\r\n\r\n<p>Today&rsquo;s latest report by the Independent Commission for Aid Impact (ICAI) &ndash; an independent body responsible for scrutinising UK aid &ndash; on how the Department for International Development (DFID) works with and through business has reinforced our concerns about the New Alliance for Food Security and Nutrition initiative.</p>\r\n\r\n<p>DFID, the department responsible for leading the UK&rsquo;s work to end extreme poverty and hunger, scored amber-red across all areas of ICAI&rsquo;s assessment &ndash; from its objectives, delivery, and impact to its learning &ndash; meaning that in terms of their work with and through business for development their programmes &ldquo;perform relatively poorly overall against ICAI&rsquo;s criteria for effectiveness and value for money, and significant improvements should be made.&rdquo;&nbsp;</p>\r\n\r\n<p>The New Alliance, formally established in 2012 and made up of big players in agribusiness, aims to improve food security and nutrition by helping 50 million people in sub-Saharan Africa out of poverty by 2022. To achieve this goal, the New Alliance <span style=\"line-height: 20.7999992370605px;\">for Food Security and Nutrition</span>&nbsp;is counting on speeding up the involvement of private capital to develop African Agriculture. DFID has pledged &pound;600 million to the New Alliance <span style=\"line-height: 20.7999992370605px;\">for Food Security and Nutrition&nbsp;</span>initiative.</p>\r\n\r\n<p>While we are of the opinion that we hall have a&nbsp;part to play in the fight against child hunger, we strongly believe in a core development principle of first &lsquo;do no harm&rsquo; and that those involved, and those facilitating them, must ensure that new initiatives do not unintentionally threaten the food security and nutrition of people across some of the world&rsquo;s poorest and most food insecure countries.&nbsp;</p>\r\n\r\n<p>The New Alliance, in particular, has raised some concerns about their ability to support small-scale family farming, to speed up the reduction of rural poverty. A year ago Action Against Hunger raised serious concerns in our report &ndash; Hunger: Just Another Business &ndash; that the New Alliance initiative was not and would not, as it currently functions, lead to tangible benefits for the world&rsquo;s poorest and most vulnerable to hunger. Furthermore, that&nbsp;the New Alliance could even threaten the food security of people across Africa.&nbsp;</p>\r\n\r\n<p>In order to drive forward radical reform our report called for:</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp;Greater support to smallholder farmers and local economies to combat poverty and hunger</strong></p>\r\n\r\n<p>If the New Alliance is to meaningfully combat poverty and hunger, then earlier commitments need to be revisited and reviewed, with priority given to those that have a proven impact on food and nutrition insecurity.</p>\r\n\r\n<p>If DFID is to continue funding the New Alliance then its projects need to have a direct, proven impact on the food and nutrition security of local people and support family farming sustainably. It should also cease funding companies that contribute to the supplanting of local enterprises, consequently endangering the livelihoods and food security of family farmers and smallholders.</p>\r\n\r\n<p>The latest ICAI report reinforces this and makes similar recommendations, calling on DFID to be more specific on how the strategy for working with business will translate into programmes that are implementable, with specific guidance on how businesses should engage in efforts that focus on the poor.</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp;2. Major improvements in the transparency and accountability of the New Alliance</strong></p>\r\n\r\n<p>Graham Ward, ICAI Chief Commissioner, said that this collaboration hold &nbsp;&ldquo;great potential&rdquo; it is not yet clear &ldquo;how DFID will translate these goals into practical actions&rdquo; that will benefit the poor.</p>\r\n\r\n<p>In order to increase their transparency, donors like DFID need to publish all its New Alliance financial and political commitments. We recommend that DFID produce annual progress report, similar to what they have done for their Nutrition for Growth commitments.</p>\r\n\r\n<p>ICAI reports concerns that current mechanism are not fit for purpose. Out of ten DFID-business partnerships reviewed, the New Alliance was the only one that had not set out clear targets for how it would benefit the world&rsquo;s poor. Similarly, in our report we noted the need for performance indicators and strict accountability mechanisms for all those involved in the New Alliance, including private companies. We recommend a single monitoring format &ndash; to measure impact &ndash; that every New Alliance member including private business partners would be required to fill in annually and funding would be dependant on completing this exercise.</p>\r\n\r\n<p>Similarly, ICAI recommended that DFID must do more to pull together and share information to ensure that lessons are being learned, even from mistakes, so they can be used to improve performance of their programmes in order to have real benefits for poor.</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp;3.&nbsp;&nbsp; &nbsp;A focus on improving people&rsquo;s food and nutrition security above profits</strong></p>\r\n\r\n<p>We are concerned that, although the purpose of the New Alliance is to improve people&rsquo;s food and nutrition security, profit-making could take precedent over the assisting the worlds poorest and most vulnerable to work their way out of poverty.&nbsp;Members of the New Alliance must agree above all to create a situation where an &ldquo;environment that is favourable&rdquo; to food and nutrition security takes priority over an environment that is favourable to investment that mainly goes to multinationals.&nbsp;</p>\r\n\r\n<p>African countries need guarantees&nbsp;to protect their agricultural sector through tariff and tax regimes, for example, that favour family and smallholder farming.</p>\r\n\r\n<p>An area what was overlooked by ICAI, but one which we believe is critical for improving people&rsquo;s food and nutrition is the role of women. Women make up more half of the agricultural labour force in many developing countries and are often the ones responsible for the food and nutrition of their families. It is widely accepted that gender-sensitive policies have a positive impact on food and nutrition security. If the New Alliance is serious about improving households access to food then investments urgently needs to be focus programmes that empower women and to involve them in combatting poverty and food and nutrition security.&nbsp;</p>\r\n\r\n<p>The fact that DFIDs work with business for development has performed relatively poor against ICAI&rsquo;s criteria for effectiveness and value for money is very concerning, particularly in regards to the New Alliance. DFID must make significant reforms in accordance with our own findings and those of ICAI that will lead to benefits for the world&rsquo;s poorest.</p>\r\n\r\n','/uploads/media/145/posts/2091764687_de Souza.jpg','','Action Against Hunger\'s Samuel Hauenstein Swan and Sabrina De Souza outline why DFID must implement significant reforms that will lead to benefits for the world’s poorest.','0','1','0','1','2015-07-09 23:26:34','2015-07-26 13:09:39',NULL,'145','145','5');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('56','77106A21-C8CB-4314-B329-723083141E09','Ending the Injustice of Hunger in the Era of Global Climate Disruption','ending-the-injustice-of-hunger-in-the-era-of-global-climate-disruption','<p>We are already living in a world of over 7 billion people that is now 0.8&deg; Celsius warmer, where one billion people live in poverty where a shocking <strong>795 million people suffer from chronic hunger</strong> and <strong>161 million children are malnourished and stunted</strong>. Compounding that unnecessary injustice is climate change, which threatens to reduce and even reverse development gains, deepening poverty and widening inequality in a growing era of global climate disruption. Exacerbating and driving climate change, resource degradation, hunger and inequality are the High Income Countries, which are over consuming resources at a precarious rate.</p>\r\n\r\n<p>Ahead of us are numerous interconnected global challenges but by far the biggest must be how we tackle poverty, end the injustice of hunger and stop climate change, especially for resource poor smallholders in some of the world&rsquo;s poorest and most vulnerable communities.</p>\r\n\r\n<h2>Climate change compounds the complexity of poverty</h2>\r\n\r\n<p>Poor people living in rural and urban areas spend as much as <strong>70% of their income on food</strong> and in just 15 years, 60% of the world&rsquo;s population will be living in cities. By 2050, world population is projected to rise to over nine billion people, meaning agriculture production will have to increase by 60% to meet food needs. Compounding that complexity is climate change, which is well underway and its causes and consequences are widening the inequality gap. Increases in extreme weather events and impacts are unfolding already plus predictions are for a warmer world of 4 degrees or more by 2100.</p>\r\n\r\n<p>We are already seeing impacts in the communities CARE works in, where hundreds of millions of small-scale food producers are living in poverty, many of whom are women and who suffer the worst impacts.&nbsp;</p>\r\n\r\n<p>Not only are <strong>women often responsible for much of food production</strong>, but 79% of them do not have equal access to resources, meaning their yields are often 20-30% lower than those of male farmers. If women had equal access to resources we would see their production increase by 30%, which could reduce the number of hungry people by up to 150 million.</p>\r\n\r\n<p>Agriculture has a key role to play in adapting to climate change, reducing emissions and supporting food security and nutrition, but smallholder farmers in developing countries face many numerous challenges.</p>\r\n\r\n<p>So what can we do? We can intervene and create methods to create less food waste or reduce food chains in industrialised agriculture, but we need to do more that directly supports the efforts of small-scale farmers.</p>\r\n\r\n<h2>Innovative strategies of adaptation and advocacy</h2>\r\n\r\n<p>CARE is dedicated to tackling the <strong>interdependent reality of poverty and climate change</strong> and works with communities to adapt to a new world of uncertain and unpredictable climate by using new farming techniques and seed varieties to build their resilience, among other community-centred and driven work. In our innovative Farmer Field Schools and Conservation Agriculture projects in Mozambique, for example, preliminary results show a 400% increase in yields in demonstration plots over three agricultural seasons.</p>\r\n\r\n<p>And CARE doesn&rsquo;t just stop at its work on the ground; it also uses its influence and expertise to raise awareness and help pull the big political levers that drive global negotiations. And the good news is that 2015 is a perfect political year to make substantial changes.</p>\r\n\r\n<p>We have the opportunity to steer the right course with potentially significant poverty reduction and climate change deals being negotiated this year. So what do we do on the road to New York and Paris for the post-2015 Development Agenda (also known as Sustainable Development Goals) and the UN Framework Convention on Climate Change, respectively? And what do we do there and ask for while negotiators make deals for humanity?</p>\r\n\r\n<p>This is what CARE is demanding:</p>\r\n\r\n<ul>\r\n	<li>A <strong>stronger focus on good governance</strong>, human rights and gender equality to ensure rights-based approaches that put the needs of women and vulnerable girls at the forefront of sustainable solutions.</li>\r\n	<li>To get all countries to <strong>commit to rapidly reducing emissions</strong> and support long-term goals that keep global temperature rise as low as possible and not further than a 1.5<strong>&deg; </strong>increase.</li>\r\n	<li><strong>Prioritisation of food security</strong> and adaptation for small-scale food producers, including scaled up financing that is targeted for the poorest.</li>\r\n	<li><strong>Participatory approaches</strong> to food security and adaptation from policy to planning to implementation and beyond.</li>\r\n</ul>\r\n\r\n<p>We have an unprecedented moment in global negotiations where we can influence how systems work and think about the problems that humanity faces. The window of opportunity is rapidly closing, but we still have time to influence change that will put women and communities at the heart of agreements ensuring that inequality gaps will grow narrower, while helping nutritiously feeding humanity and keeping the planet from warming to even more dangerous levels.</p>\r\n\r\n<p>The time for action is now to end the injustice of hunger and climate change. This is something we can do and we can all play our part.</p>\r\n','/uploads/media/150/posts/1643945089_Bangladesh Floods_PR_1.JPG','CARE','Extreme weather events, such as the floods in Bangladesh of June and July 2015, will only exacerbate the complexity of poverty.','0','1','0','1','2015-07-10 01:23:20','2015-07-10 01:23:20',NULL,'150','150','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('57','34CA8FDA-10B8-43B1-B5FE-97D32791DBE1','A childhood on the streets in Djibouti','a-childhood-on-the-streets-in-djibouti','&ldquo;Caritas is our home&rdquo;. This is what a small group of children revealed to me one hot January morning in Djibouti. I say &ldquo;hot&rdquo; even though, as in Europe, January is part of the country&rsquo;s coolest season. It&rsquo;s a pity that for them &ldquo;cool&rdquo; means 28-30&deg; in the sun. This is one of the hottest countries in the world, on the coast of the Horn of Africa. Djibouti is such a small country that many people haven&rsquo;t even heard of it. Yet it&rsquo;s extremely important strategically, which is why waves of migrants from neighbouring countries, especially Somalia and Ethiopia, aim to get there. This is not something new. Migrants also flee to Djibouti from protracted wars and grinding poverty, in the hope of finding an easier life in a country that, compared with its neighbours, is slightly better due to the presence of organisations from around the world, especially military and UN personnel, and therefore it&rsquo;s easier to get a little money from begging or by offering services such as shining shoes or washing cars.\r\n<p>The many migrants are of all ages. The ones whom Caritas Djibouti looks after are between 6 and 17 years old, although it&rsquo;s difficult to determine their age in the absence of documents to prove it. They flee for the same reasons as adults, or because they are the youngest children in the family, resources are lacking to look after them too.</p>\r\n\r\n<p>This is the situation of youngsters and street children at the Caritas day centre. Every day around 80 of these children show up on the dot of 8am to be welcomed in a small space that provides food to the guests, the chance to wash themselves and their clothes, a small infirmary and a few activities designed for them, such as literacy classes, do-it-yourself, sport, sewing, etc. The children are usually healthy, apart from some with curable diseases that are common in Africa, often including skin infections. In the most serious cases the children are taken to a hospital that has a healthcare agreement with Caritas.</p>\r\n\r\n<p>Some children come to the centre more often than others, but almost 450 are registered (namely they have been to Caritas more than once). The children live on the streets, on the beach or in front of a supermarket or restaurant. They have no documents, so they have little opportunity to be fully part of society. Some of the children were born in Djibouti and have Djibutian or foreign parents, while others arrived in Djibouti alone from Ethiopia and Somalia. They have grown up quickly but are still children, although their situation forces them to get by, to find a way of earning a little money (the famous 20-30 cents per day that is given to the workers who take care of them while they are at the centre) and a means of survival. When they arrive at Caritas they go back to being children again. Playing and seeking affection are high priorities, as well as human support.</p>\r\n\r\n<p>Working at the Caritas centre is upsetting and fascinating at the same time. It&rsquo;s upsetting to hear the street children&rsquo;s stories, and fascinating to see how they manage to survive and grow up like all their peers. It&rsquo;s surprising the extent to which boys outnumber girls. This is for various reasons, mainly due to the fact that girls are often kept at home to help with domestic tasks, and therefore they are not allowed to leave as easily as the boys are. Unfortunately, the girls who live in the streets are sometimes picked up by the police to clean their offices, and then released after a few hours.</p>\r\n\r\n<div class=\"wp-caption aligncenter\" id=\"attachment_32341\" style=\"width: 660px\"><img alt=\"Djibouti2\" height=\"433\" src=\"http://www.caritas.org/wp-content/uploads/2015/06/Djibouti2.jpg\" width=\"650\" />\r\n<p class=\"wp-caption-text\">Photo by Van Wyuytswinkel/Caritas</p>\r\n</div>\r\n\r\n<p>There are also cases like little Khalifa, an 8-year old boy who naively left Ethiopia with his friends as a game, only to find himself in world that feels alien to him and where he doesn&rsquo;t want to live. He left because his friends persuaded him to try his luck, without reflecting too much on such an adult decision. Maybe he was expecting something different, or maybe he thought it would be easier to go back home. Caritas has also started working on this, although unfortunately its even difficult to send children back to where they came from, as a long process of support and reintegration within their families of origin is required. What has been achieved, however, is to get a large number of children into basic schooling and a small group of adolescents into a vocational college where they can learn a trade. Nevertheless, all these minors remain illegal.</p>\r\n\r\n<p>Caritas Djibouti is the only facility in the country that looks after these children, while the number of minor migrants is constantly growing and cannot go unnoticed. The work Caritas does helps a great deal, but the problems underlying the situation need to be resolved, as reality cannot be hidden and should be faced up to, at national as well as other levels. In my opinion, the most pressing issue is documentation, which doesn&rsquo;t exist for too many new-born children. Unfortunately, not having an officially recognised identity means not having a place in society, neither in one&rsquo;s country of origin nor in the host country. Djibouti should not be left to tackle this issue on its own. The common aim and will of the other countries involved, which in this case are very close geographically, is also required. Guaranteeing a dignified future to those who have the courage to try and build one for themselves should be among any country&rsquo;s priorities, so that &ldquo;their home&rdquo; is not only the centre where they spend a few hours, but the entire society in which they live.</p>\r\n','/uploads/media/151/posts/2108236900_Brunelli.jpg','Van Wyuytswinkel / Caritas','Caritas Djibouti gets a large number of street children into basic schooling and a small group of adolescents into a vocational college where they can learn a trade.','0','1','0','1','2015-07-10 02:11:35','2015-07-24 01:20:14',NULL,'151','151','1');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('58','0F21757C-4C18-4A5D-B4BD-7B6845BDF70D','Count on the nuclear industry to have strange things happen','count-on-the-nuclear-industry-to-have-strange-things-happen','<p>A good case in point is the bizarre antics in Finland right now. On June 30th, Fennovoima, a Finnish utility, submitted an application to the government to build a nuclear plant. One of the utility&#39;s partners is Rosatom, the Russian state nuclear corporation.</p>\r\n\r\n<p>To apply for a license, the government requires the project to be 60% owned by companies from the European Union or the European Trade Association. The 60% criterion was put in place by the Finnish government in order to control Russian influence over the country&#39;s energy policy. And that means that Rosatom can&#39;t be the biggest player in this game.</p>\r\n\r\n<p>But, a strange thing happened on the filing deadline of June 30. Out of the blue, a new financing partner was found so that the project could meet the 60% rule and could go ahead. At least, that&#39;s the claim.</p>\r\n\r\n<p>Greenpeace Nordic decided to take a closer look at this strangely fortuitous development for Rosatom&#39;s Finnish nuclear project. We uncovered what appears to be quite a different story, from a serendipitous turn of events in the form of a new nuclear investment partner.</p>\r\n\r\n<p>Instead of a viable European company with a track record that would suggest it is a credible business partner, Greenpeace found a Croatian company, Migrit Solarna Energija, that operates out of an apartment block in Zagreb. It has no employees, capital stock of only 26,000 Euros, and absolutely no income in 2012-2013. And yet, this company is supposedly going to be able to contribute 150 million Euros to the project?</p>\r\n\r\n<p>More importantly, Greenpeace found what appear to be strong ties between this tiny company holed up in an apartment complex, and Russia&#39;s nuclear giant, Rosatom. The company, established by Russian Mikhail Zhukov, is closely linked to Titan enterprises. Migrit Solarna Energija and Titan enterprises share an office and a fax number. The owners of Migrit Solarna Energija are related to Titan Energija&#39;s director and a businessman with a background with Inteco. This raises the concern that this company, Migrit Solarna Energija, a subsidiary of Migrit Energija, does not fulfill the 60% criterion for the project to move forward.</p>\r\n\r\n<p>There&#39;s more. Mikhail Zhukov heads up Inteco, which used to be owned by the richest woman in Russia, Yelena Baturina. She happens to be married to Yuri Lužkov, the former mayor of Moscow. Baturina sold Inteco to 50% state-owned Sberbank and to billionaire Mihail Shishkanov. Sberbank is an essential financier of Rosatom.</p>\r\n\r\n<p>Given these unsettling findings, Greenpeace warned the Finnish government to carefully examine the license application by Fennovoima to ensure it meets ownership criteria and is in best interests of the country. But the concerns are bigger than Finland. As our Finnish program manager, Sini Harkki, said: &quot;This game that Fennovoima and Rosatom appear to be playing should be a concern to any country that is in discussions with Rosatom regarding building nuclear reactors. If the state corporation is ready to play a game with something as simple as ownership rules, what else will it play games with in building a dangerous reactor?&quot;</p>\r\n\r\n<p>Rosatom is actively pursuing nuclear contracts around the world. And this warning is something many other countries should heed. In October 2014, <a class=\"pdf\" href=\"http://www.greenpeace.org/hungary/PageFiles/636986/rosatom_risks.pdf\" target=\"_blank\">Greenpeace released a report on the problems with Rosatom and the Russian nuclear industry</a>. This ownership game appears to be consistent with the kinds of problems that plague Rosatom and should be required reading for politicians in any country thinking of cutting a deal with Rosatom.</p>\r\n\r\n<p>Fennovoima and Rosatom looked for years for investors. Yet it only took a few days to expose what appears to be a hoax, and a front for Russian capital.</p>\r\n\r\n<p>That&#39;s not the end of nuclear problems in Finland. The country is suffering through a protracted mess with Areva, the French nuclear company, over the building the Olkiluoto 3 nuclear plant. The project is years late and billions over budget with no end to the problems in sight.</p>\r\n\r\n<p>With lessons like those from Rosatom and Areva&#39;s Finnish nuclear projects, it is no wonder that in Finland the public majority is against nuclear. In spite of the people&#39;s will, Finland&#39;s current energy strategy relies on nuclear. But with ample renewable resources to be developed and the usual mess with nuclear projects, it is time to reconsider that strategy, listen to the will of the Finnish citizens, and move into the nuclear-free clean-energy future.</p>\r\n','/uploads/media/152/posts/1447219307_Blomme.jpg','Greenpeace, Nick Cobbing','It has been said often on the Nuclear Reaction blog but bears repeating: the nuclear industry really can\'t be trusted.','0','1','0','1','2015-07-10 03:02:27','2015-07-10 03:02:27',NULL,'152','152','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('59','3A77D04C-9490-457C-BD70-2618D78B8E95','Fighting inequality in Latin America: The road ahead','fighting-inequality-in-latin-america-the-road-ahead','<p>Last week, I was in Santiago attending a <a href=\"http://www.imfsantiago2014.org/\"><strong>high-level conference </strong></a>hosted by Chile&rsquo;s Ministry of Finance and the International Monetary Fund.</p>\r\n\r\n<p>Along with policy-makers, academics, opinion leaders, and financial sector executives, I was exploring challenges and opportunities for securing inclusive and sustainable growth in Latin America.</p>\r\n\r\n<p><strong>Latin America is proof that the global trend of rising economic inequality can be reversed, if the political will exists.</strong> Despite historically being the most unequal region in the world, it is the only region that has managed to reduce inequality during the past decade.</p>\r\n\r\n<p>This success is the result of the right mix of government policies that focus on poor people. Increased spending on health and education since 2000 has had the greatest impact on inequality reduction. As a result of social public expenditures, many of the region&rsquo;s poorest citizens have been able to access essential services without having to become indebted to pay for them. For Latin American youth, education is not only an equalizer but it will stimulate innovation, entrepreneurship and growth.</p>\r\n\r\n<p>Increasing the minimum wage, public pensions and employment opportunities has also created secure livelihoods for millions.</p>\r\n\r\n<p>But this is not the end of the road. <strong>Despite these advances, Latin America is still the world&acute;s most unequal region.</strong></p>\r\n\r\n<p>Close to a third of the population live in poverty &ndash; while the annual income of Latin America&rsquo;s 113 billionaires this year is equal to the public budget of El Salvador, Guatemala and Nicaragua put together. That same income is equal to the public health expenditure of nine Latin American countries.</p>\r\n\r\n<p>So, the question is: Can Latin America remain an example for the world, and maintain and sustain inequality and poverty reduction trends?</p>\r\n\r\n<p><strong>Oxfam thinks it can &ndash; if Latin American governments continue to invest</strong> in health, education and social protection, if alternatives are found to primary commodities exports as the engine of growth, and if progress is bolstered by progressive fiscal reforms.</p>\r\n\r\n<p>Growth in Latin America over the last decade has been largely driven by the commodity boom. Several economies in the region depend on oil and other extractive industries. This makes their economies vulnerable. Indeed, growth in LAC has now has stalled, and new drivers of growth are being sought. There is a need to diversify from primary commodity sectors, to sectors which can create many jobs.</p>\r\n\r\n<p>As the<a href=\"https://www.imf.org/external/pubs/ft/sdn/2013/sdn1310.pdf\"><strong> IMF has pointed out</strong></a>, &ldquo;the challenges of growth, job creation, and inclusion are closely intertwined.&rdquo; Growth gives women job opportunities, but women&rsquo;s participation in the labor market is also a part of the growth and poverty-reducing equation.</p>\r\n\r\n<p><strong>There has been a steady increase in women&rsquo;s participation</strong> in the labor market in recent decades, but in most countries in the region women are much more likely than men to hold low-paid jobs. The wage gap between men and women is also <a href=\"http://publications.iadb.org/bitstream/handle/11319/6384/New%20Century%20Old%20Disparities.pdf?sequence=1\"><strong>substantial</strong></a>, lagging behind OECD countries.</p>\r\n\r\n<p>Closing this gap will go a long way to equitable and sustainable development &ndash; triggering growth and reducing inequality.</p>\r\n\r\n<p><strong>Inequality can also be reduced by progressive taxation</strong> &ndash; an under-used instrument to reduce in Latin America thus far. A <a href=\"http://www.oxfam.org/en/research/fiscal-justice-reduce-inequality-latin-america-and-caribbean\"><strong>recent Oxfam report</strong></a> highlights the low tax collection levels in the region, in comparison to the great social needs. Our research shows tax systems are largely skewed towards benefitting economic and political elites &ndash; rather than the majority of the people.</p>\r\n\r\n<p>More than half of tax in Latin American and the Caribbean comes from consumption taxes, such as VAT. This means the poorest devote a greater share of their income to pay taxes than the rich.</p>\r\n\r\n<p>Further, corporate tax exemptions in the region amount to $138,000 million per year. In the Dominican Republic tax exemptions for free zones, tourism and other industries are about $720 million - enough to double the health budget. In Nicaragua in 2008 tax exemptions amounted to $415.6 million, 40 per cent more than the Ministry of Health&rsquo;s total budget that year.</p>\r\n\r\n<p><strong>Enormous tax evasion in the region is also a problem</strong>. <a href=\"http://www.oxfam.org/en/research/fiscal-justice-reduce-inequality-latin-america-and-caribbean\"><strong>Oxfam&rsquo;s calculation</strong></a> is that money hidden in tax havens would be enough for 32 million people to be lifted out of poverty. That is, all people living in poverty from Bolivia, Colombia, Ecuador, El Salvador and Peru.</p>\r\n\r\n<p>Latin American leaders should improve their domestic resource mobilization systems, cooperate to stop the race to the bottom on corporate tax exemptions, and demand a say in the G20/OECD-led negotiations to reform global tax rules for curbing illicit financial flows.</p>\r\n\r\n<p><strong>Courageous steps have been taken</strong> by many Latin American leaders in the last decade, and the successes speak for themselves. Now more courageous reforms are needed, to achieve a fiscal system which will dislodge entrenched inequalities and benefit all Latin America&rsquo;s people equally, and in the long term.</p>\r\n\r\n<p>Originally published on the Huffington Post as: <a href=\"http://www.huffingtonpost.com/winnie-byanyima/inclusive-sustainable-gro_b_6278690.html\">Inclusive, Sustainable Growth Latin America: The Road Ahead</a></p>\r\n','/uploads/media/153/posts/2028786408_Ny.jpg','Gilvan Barreto/Oxfam','Emelina Dominguez, agricultural technician, 42, tending to her vegetables. Honduras, 14 January 2007.','0','1','0','1','2015-07-10 05:47:49','2015-08-11 03:08:19',NULL,'153','153','5');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('60','67CD1BD4-DF67-4D5F-899B-8F8095766256','test','test','test','/uploads/media/135/posts/1544380486_Cuso.jpg','','','0','0','0','1','2015-07-24 08:04:43','2015-07-24 08:04:43',NULL,'135','135','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('61','598D615A-EABD-4BFF-AB1B-E1B9F0C0FB1E','test2','test2','test 2','/uploads/media/135/posts/256675244_Ayala-Foundation.jpg','','','0','0','0','1','2015-07-24 08:08:16','2015-07-24 08:08:16',NULL,'135','135','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('62','BD81FCF6-29F7-4456-8BD9-63EEDBA482D6','test3','test3','test3','/uploads/media/135/posts/816337216_AER.jpg','','','0','0','0','1','2015-07-24 08:22:18','2015-07-24 08:22:18',NULL,'135','135','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('63','B31E0601-3C1F-4A6C-BE60-3B153D51CBCD','Test 5','test-5','Test 5','/uploads/media/135/posts/1979894200_Asian Farmers.jpg','','','0','0','0','1','2015-07-24 08:52:29','2015-07-24 08:52:29',NULL,'135','135','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('64','B2BF6994-28D6-439B-A529-9DFAEA639769','Title test','title-test','Text test <a href=\"http://aidpost.org\">http://aidpost.org</a>','/uploads/media/170/posts/1323777164_Advocacy.jpg','Photo source test','Photo legend test','1','1','0','1','2015-08-11 23:26:35','2015-08-11 23:28:06',NULL,'170','170','1');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('65','E8C3C2C5-31B6-4093-BE8A-1735AA2FB388','Title test','title-test','Text test','/uploads/media/170/posts/466234662_Adds.JPG','Photo source test','Legend test','0','1','0','1','2015-08-11 23:32:54','2015-08-11 23:32:54',NULL,'170','170','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('66','39E15AF5-C4F7-4E4F-808B-3865D46A8FC1','Photo of the Week: Vanuatu’s ‘10,000 in 10’ campaign','photo-of-the-week-vanuatus-10000-in-10-campaign','Vanuatu, 2015: Tropical Cyclone Pam, which hit on 13 March, has disrupted access to safe water and sanitation in the South Pacific island nation, increasing children&rsquo;s risk of water- and vector-borne diseases.<br />\r\n<br />\r\nThe &lsquo;10,000 in 10&rsquo; campaign, launched on 18 March, aims to immunize 10,000 children 6 to 59 months of age against measles and rubella, in 21 villages over a period of 10 days.<br />\r\n<br />\r\nIn this photo, children near the shore in Port Vila watch a boat further out that is taking an immunization team to a nearby island.<br />\r\n<br />\r\nTo see more images from UNICEF visit UNICEF Photography (http://www.unicef.org/photography/photo_app.php).','/uploads/media/132/posts/574390363_Nesbitt-Unicef.jpg','©UNICEF/ NYHQ2015-0523/Sokhin','','0','1','0','1','2015-08-12 23:29:02','2015-08-15 02:40:19',NULL,'132','132','1');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('67','FB9D5B9E-98BD-43F1-A94C-1F0150B7F26D','Photo of the Week: Vanuatu’s ‘10,000 in 10’ campaign','photo-of-the-week-vanuatus-10000-in-10-campaign','Vanuatu, 2015: Tropical Cyclone Pam, which hit on 13 March, has disrupted access to safe water and sanitation in the South Pacific island nation, increasing children&rsquo;s risk of water- and vector-borne diseases.<br />\r\n<br />\r\nThe &lsquo;10,000 in 10&rsquo; campaign, launched on 18 March, aims to immunize 10,000 children 6 to 59 months of age against measles and rubella, in 21 villages over a period of 10 days.<br />\r\n<br />\r\nIn this photo, children near the shore in Port Vila watch a boat further out that is taking an immunization team to a nearby island.<br />\r\n<br />\r\nTo see more images from UNICEF visit UNICEF Photography (http://www.unicef.org/photography/photo_app.php).','/uploads/media/172/posts/264657554_Nesbitt-Unicef.jpg','©UNICEF/ NYHQ2015-0523/Sokhin','','0','1','0','1','2015-08-12 23:51:28','2015-08-12 23:51:28',NULL,'172','172','0');
INSERT INTO `blog_posts` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_source`,`image_legend`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('68','F9D52E79-EB19-4AB9-9DDD-A8B665A1AC6D','WebKit custom scrollbars','webkit-custom-scrollbars','fasdfa sd fasd','/uploads/media/176/posts/46810561_20141221_2018213.jpg','','','0','1','0','1','2015-08-15 01:27:48','2015-08-15 01:27:49',NULL,'176','176','0');
/*!40000 ALTER TABLE `blog_posts` ENABLE KEYS */;


--
-- Create Table `blog_posts_beneficiaries`
--

DROP TABLE IF EXISTS `blog_posts_beneficiaries`;
CREATE TABLE `blog_posts_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_beneficiaries_post_id_index` (`post_id`),
  KEY `blog_posts_beneficiaries_beneficiary_id_index` (`beneficiary_id`),
  CONSTRAINT `blog_posts_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  CONSTRAINT `blog_posts_beneficiaries_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_beneficiaries`
--

/*!40000 ALTER TABLE `blog_posts_beneficiaries` DISABLE KEYS */;
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('55','51','8','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('56','54','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('58','56','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('60','57','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('61','58','8','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('63','59','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('65','55','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('66','64','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('67','66','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_beneficiaries` (`id`,`post_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('68','67','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `blog_posts_beneficiaries` ENABLE KEYS */;


--
-- Create Table `blog_posts_countries`
--

DROP TABLE IF EXISTS `blog_posts_countries`;
CREATE TABLE `blog_posts_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_countries_post_id_index` (`post_id`),
  KEY `blog_posts_countries_country_id_index` (`country_id`),
  CONSTRAINT `blog_posts_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `blog_posts_countries_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_countries`
--

/*!40000 ALTER TABLE `blog_posts_countries` DISABLE KEYS */;
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('53','57','59','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('54','58','73','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('56','59','97','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('57','64','10','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('58','65','10','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('59','66','229','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_countries` (`id`,`post_id`,`country_id`,`created_at`,`updated_at`) VALUES ('60','67','229','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `blog_posts_countries` ENABLE KEYS */;


--
-- Create Table `blog_posts_crisis`
--

DROP TABLE IF EXISTS `blog_posts_crisis`;
CREATE TABLE `blog_posts_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_crisis_post_id_index` (`post_id`),
  KEY `blog_posts_crisis_crisis_id_index` (`crisis_id`),
  CONSTRAINT `blog_posts_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  CONSTRAINT `blog_posts_crisis_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_crisis`
--

/*!40000 ALTER TABLE `blog_posts_crisis` DISABLE KEYS */;
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('53','56','29','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('54','58','39','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('56','55','32','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('57','64','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('58','65','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('59','66','45','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_crisis` (`id`,`post_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('60','67','45','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `blog_posts_crisis` ENABLE KEYS */;


--
-- Create Table `blog_posts_interventions`
--

DROP TABLE IF EXISTS `blog_posts_interventions`;
CREATE TABLE `blog_posts_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_interventions_post_id_index` (`post_id`),
  KEY `blog_posts_interventions_intervention_id_index` (`intervention_id`),
  CONSTRAINT `blog_posts_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  CONSTRAINT `blog_posts_interventions_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_interventions`
--

/*!40000 ALTER TABLE `blog_posts_interventions` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_posts_interventions` ENABLE KEYS */;


--
-- Create Table `blog_posts_regions`
--

DROP TABLE IF EXISTS `blog_posts_regions`;
CREATE TABLE `blog_posts_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_regions_post_id_index` (`post_id`),
  KEY `blog_posts_regions_region_id_index` (`region_id`),
  CONSTRAINT `blog_posts_regions_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`),
  CONSTRAINT `blog_posts_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_regions`
--

/*!40000 ALTER TABLE `blog_posts_regions` DISABLE KEYS */;
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('55','51','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('56','54','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('58','56','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('60','57','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('61','58','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('63','59','4','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('65','55','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('66','53','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('67','64','4','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('68','65','4','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('69','66','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_regions` (`id`,`post_id`,`region_id`,`created_at`,`updated_at`) VALUES ('70','67','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `blog_posts_regions` ENABLE KEYS */;


--
-- Create Table `blog_posts_sectors`
--

DROP TABLE IF EXISTS `blog_posts_sectors`;
CREATE TABLE `blog_posts_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_sectors_post_id_index` (`post_id`),
  KEY `blog_posts_sectors_sector_id_index` (`sector_id`),
  CONSTRAINT `blog_posts_sectors_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`),
  CONSTRAINT `blog_posts_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_sectors`
--

/*!40000 ALTER TABLE `blog_posts_sectors` DISABLE KEYS */;
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('54','51','14','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('55','54','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('57','56','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('59','57','12','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('60','58','13','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('62','59','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('64','55','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('65','64','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('66','66','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_sectors` (`id`,`post_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('67','67','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `blog_posts_sectors` ENABLE KEYS */;


--
-- Create Table `blog_posts_themes`
--

DROP TABLE IF EXISTS `blog_posts_themes`;
CREATE TABLE `blog_posts_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_themes_post_id_index` (`post_id`),
  KEY `blog_posts_themes_theme_id_index` (`theme_id`),
  CONSTRAINT `blog_posts_themes_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`),
  CONSTRAINT `blog_posts_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `blog_posts_themes`
--

/*!40000 ALTER TABLE `blog_posts_themes` DISABLE KEYS */;
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('55','51','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('56','54','21','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('58','56','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('60','57','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('61','58','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('63','59','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('65','55','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('66','53','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('67','64','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('68','65','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('69','66','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `blog_posts_themes` (`id`,`post_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('70','67','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `blog_posts_themes` ENABLE KEYS */;


--
-- Create Table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `commented_by` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `comments`
--

/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`,`module_id`,`item_id`,`commented_by`,`content`,`created_at`,`updated_at`) VALUES ('1','1','64','170','test','2015-08-11 23:28:06','2015-08-11 23:28:06');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;


--
-- Create Table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `contacts`
--

/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('1','3','roger markowski','rogermarkowski@hotmail.com','Test','2015-06-29 02:25:00','2015-06-29 02:25:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('2','1','roger m','rogermarmowski@hotmail.com','Test from heading','2015-06-29 02:26:00','2015-06-29 02:26:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('3','2','Sébastien','sebastien.lemarquis@gmail.com','Test','2015-06-30 13:48:00','2015-06-30 13:48:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('4','2','mr test himself','rogermarkowski@hotmail.com','Test message from the bottom heading','2015-07-04 23:53:00','2015-07-04 23:53:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('5','3','Mr test himself','rogermarkowski@hotmail.com','Message test from the feedback page','2015-07-04 23:54:00','2015-07-04 23:54:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('6','1','roger markowski','rogermarkowski@hotmail.com','Test from bottom','2015-07-08 00:26:00','2015-07-08 00:26:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('7','2','roger markowski','rogermarkowski@hotmail.com','Test from feedback page','2015-07-08 00:26:00','2015-07-08 00:26:00');
INSERT INTO `contacts` (`id`,`subject`,`name`,`email`,`message`,`created_at`,`updated_at`) VALUES ('8','3','roger','rogermarkowski@hotmail.com','blabbbbbbbbbbbbbbbbb bbbbbbbbbbbbb bbbbbbbbbbbbbb bbbbbbbbbb bbbbbbbbbbbb bbbbbbbbbbbbbb bbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbb','2015-07-08 00:30:00','2015-07-08 00:30:00');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;


--
-- Create Table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `countries`
--

/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Afghanistan','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Albania','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Algeria','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','American Samoa','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Andorra','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('6','Angola','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('7','Anguilla','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('8','Antarctica','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('9','Antigua and Barbuda','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('10','Argentina','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('11','Armenia','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('12','Aruba','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('13','Australia','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('14','Austria','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('15','Azerbaijan','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('16','Bahamas','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('17','Bahrain','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('18','Bangladesh','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('19','Barbados','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('20','Belarus','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('21','Belgium','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('22','Belize','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('23','Benin','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('24','Bermuda','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('25','Bhutan','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('26','Bolivia','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('27','Bosnia and Herzegowina','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('28','Botswana','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('29','Bouvet Island','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('30','Brazil','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('31','British Indian Ocean Territory','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('32','Brunei Darussalam','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('33','Bulgaria','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('34','Burkina Faso','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('35','Burundi','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('36','Cambodia','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('37','Cameroon','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('38','Canada','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('39','Cape Verde','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('40','Cayman Islands','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('41','Central African Republic','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('42','Chad','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('43','Chile','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('44','China','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('45','Christmas Island','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('46','Cocos (Keeling) Islands','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('47','Colombia','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('48','Comoros','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('49','Congo','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('50','Congo, the Democratic Republic of the','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('51','Cook Islands','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('52','Costa Rica','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('53','Cote d\'Ivoire','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('54','Croatia (Hrvatska)','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('55','Cuba','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('56','Cyprus','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('57','Czech Republic','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('58','Denmark','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('59','Djibouti','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('60','Dominica','0','2015-05-04 09:59:29','2015-05-04 09:59:29');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('61','Dominican Republic','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('62','East Timor','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('63','Ecuador','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('64','Egypt','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('65','El Salvador','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('66','Equatorial Guinea','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('67','Eritrea','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('68','Estonia','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('69','Ethiopia','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('70','Falkland Islands (Malvinas)','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('71','Faroe Islands','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('72','Fiji','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('73','Finland','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('74','France','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('75','France Metropolitan','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('76','French Guiana','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('77','French Polynesia','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('78','French Southern Territories','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('79','Gabon','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('80','Gambia','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('81','Georgia','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('82','Germany','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('83','Ghana','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('84','Gibraltar','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('85','Greece','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('86','Greenland','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('87','Grenada','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('88','Guadeloupe','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('89','Guam','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('90','Guatemala','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('91','Guinea','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('92','Guinea-Bissau','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('93','Guyana','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('94','Haiti','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('95','Heard and Mc Donald Islands','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('96','Holy See (Vatican City State)','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('97','Honduras','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('98','Hong Kong','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('99','Hungary','0','2015-05-04 09:59:30','2015-05-04 09:59:30');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('100','Iceland','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('101','India','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('102','Indonesia','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('103','Iran (Islamic Republic of)','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('104','Iraq','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('105','Ireland','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('106','Israel','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('107','Italy','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('108','Jamaica','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('109','Japan','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('110','Jordan','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('111','Kazakhstan','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('112','Kenya','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('113','Kiribati','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('114','Korea, Democratic People\'s Republic of','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('115','Korea, Republic of','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('116','Kuwait','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('117','Kyrgyzstan','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('118','Lao, People\'s Democratic Republic','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('119','Latvia','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('120','Lebanon','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('121','Lesotho','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('122','Liberia','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('123','Libyan Arab Jamahiriya','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('124','Liechtenstein','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('125','Lithuania','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('126','Luxembourg','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('127','Macau','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('128','Macedonia, The Former Yugoslav Republic of','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('129','Madagascar','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('130','Malawi','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('131','Malaysia','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('132','Maldives','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('133','Mali','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('134','Malta','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('135','Marshall Islands','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('136','Martinique','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('137','Mauritania','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('138','Mauritius','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('139','Mayotte','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('140','Mexico','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('141','Micronesia, Federated States of','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('142','Moldova, Republic of','0','2015-05-04 09:59:31','2015-05-04 09:59:31');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('143','Monaco','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('144','Mongolia','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('145','Montserrat','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('146','Morocco','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('147','Mozambique','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('148','Myanmar','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('149','Namibia','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('150','Nauru','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('151','Nepal','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('152','Netherlands','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('153','Netherlands Antilles','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('154','New Caledonia','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('155','New Zealand','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('156','Nicaragua','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('157','Niger','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('158','Nigeria','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('159','Niue','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('160','Norfolk Island','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('161','Northern Mariana Islands','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('162','Norway','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('163','Oman','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('164','Pakistan','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('165','Palau','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('166','Panama','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('167','Papua New Guinea','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('168','Paraguay','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('169','Peru','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('170','Philippines','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('171','Pitcairn','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('172','Poland','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('173','Portugal','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('174','Puerto Rico','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('175','Qatar','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('176','Reunion','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('177','Romania','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('178','Russian Federation','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('179','Rwanda','0','2015-05-04 09:59:32','2015-05-04 09:59:32');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('180','Saint Kitts and Nevis','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('181','Saint Lucia','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('182','Saint Vincent and the Grenadines','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('183','Samoa','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('184','San Marino','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('185','Sao Tome and Principe','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('186','Saudi Arabia','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('187','Senegal','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('188','Seychelles','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('189','Sierra Leone','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('190','Singapore','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('191','Slovakia (Slovak Republic)','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('192','Slovenia','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('193','Solomon Islands','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('194','Somalia','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('195','South Africa','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('196','South Georgia and the South Sandwich Islands','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('197','Spain','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('198','Sri Lanka','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('199','St. Helena','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('200','St. Pierre and Miquelon','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('201','Sudan','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('202','Suriname','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('203','Svalbard and Jan Mayen Islands','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('204','Swaziland','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('205','Sweden','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('206','Switzerland','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('207','Syrian Arab Republic','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('208','Taiwan, Province of China','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('209','Tajikistan','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('210','Tanzania, United Republic of','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('211','Thailand','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('212','Togo','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('213','Tokelau','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('214','Tonga','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('215','Trinidad and Tobago','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('216','Tunisia','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('217','Turkey','0','2015-05-04 09:59:33','2015-05-04 09:59:33');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('218','Turkmenistan','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('219','Turks and Caicos Islands','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('220','Tuvalu','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('221','Uganda','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('222','Ukraine','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('223','United Arab Emirates','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('224','United Kingdom','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('225','United States','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('226','United States Minor Outlying Islands','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('227','Uruguay','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('228','Uzbekistan','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('229','Vanuatu','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('230','Venezuela','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('231','Vietnam','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('232','Virgin Islands (British)','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('233','Virgin Islands (U.S.)','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('234','Wallis and Futuna Islands','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('235','Western Sahara','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('236','Yemen','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('237','Yugoslavia','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('238','Zambia','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `countries` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('239','Zimbabwe','0','2015-05-04 09:59:34','2015-05-04 09:59:34');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


--
-- Create Table `crisis`
--

DROP TABLE IF EXISTS `crisis`;
CREATE TABLE `crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `crisis`
--

/*!40000 ALTER TABLE `crisis` DISABLE KEYS */;
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Afghanistan','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Cameroon','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Central African Republic','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Chad','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Congo','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('6','Ebola','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('7','Irak','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('8','Kenya','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('9','Lybia','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('10','Mali','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('11','Myanmar','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('12','Niger','1','2015-05-04 09:59:34','2015-05-04 09:59:34');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('13','Nigeria','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('14','Pakistan','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('15','Palestine','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('16','Sahel','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('17','Somalia','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('18','South Sudan','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('19','Southern Africa Floods','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('20','Sudan','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('21','Syria','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('22','Ukraine','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('23','Yemen','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('24','Animal Plague','0','2015-05-19 09:05:00','2015-05-19 09:05:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('25','Avalanche','1','2015-05-19 09:05:00','2015-05-19 09:05:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('26','Biological Hazard','0','2015-05-19 09:06:00','2015-05-19 09:06:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('27','Chemical Hazard','0','2015-05-19 09:06:00','2015-05-19 09:06:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('28','Civil Unrest','0','2015-05-19 09:07:00','2015-05-19 09:07:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('29','Climate Change','0','2015-05-19 09:07:00','2015-05-19 09:07:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('30','Armed Conflict','0','2015-05-19 09:07:00','2015-05-19 09:07:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('31','Disease Epidemic','0','2015-05-19 09:12:00','2015-05-19 09:12:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('32','Drought','0','2015-05-19 09:14:00','2015-05-19 09:14:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('33','Earthquake','0','2015-05-19 09:14:00','2015-05-19 09:14:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('34','Fire','0','2015-05-19 09:17:00','2015-05-19 09:19:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('35','Extreme Temperature','0','2015-05-19 09:19:00','2015-05-19 09:19:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('36','Famine','0','2015-05-19 09:20:00','2015-05-19 09:20:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('37','Flood','0','2015-05-19 09:20:00','2015-05-19 09:20:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('38','Genocide','0','2015-05-19 09:21:00','2015-05-19 09:21:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('39','Industrial accident','0','2015-05-19 09:22:00','2015-05-19 09:22:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('40','Insect Plague','0','2015-05-19 09:22:00','2015-05-19 09:22:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('41','Landslide','0','2015-05-19 09:22:00','2015-05-19 09:22:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('42','Pandemic','0','2015-05-19 09:22:00','2015-05-19 09:22:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('43','Pollution','0','2015-05-19 09:23:00','2015-05-19 09:23:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('44','Species Extinction','0','2015-05-19 09:23:00','2015-05-19 09:23:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('45','Storm','0','2015-05-19 09:23:00','2015-05-19 09:23:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('46','Transport Accident','0','2015-05-19 09:23:00','2015-05-19 09:23:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('47','Under development','0','2015-05-19 09:24:00','2015-05-19 09:24:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('48','Unplanned Urbanization','0','2015-05-19 09:24:00','2015-05-19 09:24:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('49','Tsunami & Tidal Surge','0','2015-05-19 09:27:00','2015-05-19 09:27:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('50','Climate Change','1','2015-05-19 09:28:00','2015-05-19 09:28:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('51','Volcanic Eruption','0','2015-05-19 09:29:00','2015-05-19 09:29:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('52','Avalanche','0','2015-07-11 06:14:00','2015-07-11 06:14:00');
INSERT INTO `crisis` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('53','Recreational','0','2015-07-23 10:27:00','2015-07-23 10:27:00');
/*!40000 ALTER TABLE `crisis` ENABLE KEYS */;


--
-- Create Table `interventions`
--

DROP TABLE IF EXISTS `interventions`;
CREATE TABLE `interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `interventions`
--

/*!40000 ALTER TABLE `interventions` DISABLE KEYS */;
INSERT INTO `interventions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','International Development','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `interventions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','National issues','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `interventions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Relief Assistance','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `interventions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Recovery and Rehabilitation','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
/*!40000 ALTER TABLE `interventions` ENABLE KEYS */;


--
-- Create Table `level_of_activities`
--

DROP TABLE IF EXISTS `level_of_activities`;
CREATE TABLE `level_of_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `level_of_activities`
--

/*!40000 ALTER TABLE `level_of_activities` DISABLE KEYS */;
INSERT INTO `level_of_activities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('1','Local','2015-07-01 01:40:15','0000-00-00 00:00:00');
INSERT INTO `level_of_activities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('2',' National','2015-07-01 01:40:15','0000-00-00 00:00:00');
INSERT INTO `level_of_activities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('3','International','2015-07-01 01:40:39','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `level_of_activities` ENABLE KEYS */;


--
-- Create Table `level_of_responsibilities`
--

DROP TABLE IF EXISTS `level_of_responsibilities`;
CREATE TABLE `level_of_responsibilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `level_of_responsibilities`
--

/*!40000 ALTER TABLE `level_of_responsibilities` DISABLE KEYS */;
INSERT INTO `level_of_responsibilities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('1','Local','2015-07-01 01:41:57','0000-00-00 00:00:00');
INSERT INTO `level_of_responsibilities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('2','National','2015-07-01 01:41:57','0000-00-00 00:00:00');
INSERT INTO `level_of_responsibilities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('3','International','2015-07-01 01:42:22','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `level_of_responsibilities` ENABLE KEYS */;


--
-- Create Table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `liked_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `likes`
--

/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('1','4','44','101','2015-06-03 07:08:17','2015-06-03 07:08:17');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('2','2','112','135','2015-06-06 00:28:55','2015-06-06 00:28:55');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('3','2','111','135','2015-06-06 00:30:53','2015-06-06 00:30:53');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('4','4','43','135','2015-06-06 00:32:03','2015-06-06 00:32:03');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('5','1','52','146','2015-07-09 11:42:09','2015-07-09 11:42:09');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('6','1','51','146','2015-07-09 11:43:08','2015-07-09 11:43:08');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('7','1','54','146','2015-07-09 11:43:43','2015-07-09 11:43:43');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('8','4','35','146','2015-07-09 11:47:45','2015-07-09 11:47:45');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('9','2','113','101','2015-07-11 23:59:33','2015-07-11 23:59:33');
INSERT INTO `likes` (`id`,`module_id`,`item_id`,`liked_by`,`created_at`,`updated_at`) VALUES ('10','1','55','101','2015-07-17 01:43:18','2015-07-17 01:43:18');
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;


--
-- Create Table `member_statuses`
--

DROP TABLE IF EXISTS `member_statuses`;
CREATE TABLE `member_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `member_statuses`
--

/*!40000 ALTER TABLE `member_statuses` DISABLE KEYS */;
INSERT INTO `member_statuses` (`id`,`name`,`description`,`created_at`,`updated_at`) VALUES ('1','Non-profit employee','','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `member_statuses` (`id`,`name`,`description`,`created_at`,`updated_at`) VALUES ('2','Aid Collaborator','your work relates closely to the aid or social sector','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `member_statuses` (`id`,`name`,`description`,`created_at`,`updated_at`) VALUES ('3','Aid Supporter','you personally support non-profits','2015-05-04 09:59:42','2015-05-04 09:59:42');
/*!40000 ALTER TABLE `member_statuses` ENABLE KEYS */;


--
-- Create Table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `migrations`
--

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_05_045909_create_table_settings','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_05_055729_create_lookup_tables','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_05_153438_create_filter_tables','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_06_160613_create_table_users','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_07_055730_create_table_user_profiles','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_07_055740_create_table_organisations','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_02_10_073623_create_table_blog_posts','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_02_154616_create_table_blog_posts_countries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_02_154623_create_table_blog_posts_regions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_02_154629_create_table_blog_posts_crisis','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_02_154636_create_table_blog_posts_sectors','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_02_154642_create_table_blog_posts_themes','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_02_154648_create_table_blog_posts_beneficiaries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_123910_create_table_news_items','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_153856_create_table_news_items_countries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_153903_create_table_news_items_regions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_153907_create_table_news_items_crisis','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_153911_create_table_news_items_sectors','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_153916_create_table_news_items_themes','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_05_153922_create_table_news_items_beneficiaries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_17_150936_create_table_types_of_take_actions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_18_150936_create_table_take_actions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_24_153505_create_table_take_actions_countries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_24_153518_create_table_take_actions_regions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_24_153525_create_table_take_actions_crisis','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_24_153530_create_table_take_actions_sectors','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_24_153535_create_table_take_actions_themes','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_24_153540_create_table_take_actions_beneficiaries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050412_create_table_videos','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050445_create_table_videos_countries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050454_create_table_videos_regions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050504_create_table_videos_crisis','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050512_create_table_videos_sectors','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050522_create_table_videos_themes','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_03_27_050537_create_table_videos_beneficiaries','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_06_140325_create_table_password_reminders','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_015432_alter_table_organisations_add_description_field','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_015703_alter_table_user_profiles_add_description_field','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_025412_alter_table_news_items_drop_readmore','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_025641_alter_table_blog_posts__drop_readmore','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_032831_alter_table_user_profiles_add_gender_field','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_041931_alter_table_organisations_add_acronym_field','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_082126_create_table_filter_interventions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_09_082655_create_table_postings_cross_interventions','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_13_031938_create_table_registrations','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_14_133957_alter_table_organisations_add_socialmedia_fields','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_14_134950_alter_table_user_profiles_add_socialmedia_fields','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_15_143439_alter_table_organisations_add_phone','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_17_013922_alter_table_videos_add_description_and_duration','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_18_135514_create_table_types_of_publicities','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_18_135515_create_table_publicities','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_26_161640_alter_table_news_items_add_popularity_fields','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_26_161927_create_table_likes','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_26_173456_alter_table_videos_add_popularity_fields','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_27_132131_alter_table_blog_posts_add_popularity_fields','1');
INSERT INTO `migrations` (`migration`,`batch`) VALUES ('2015_04_28_161943_create_comments_table','1');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


--
-- Create Table `nationalities`
--

DROP TABLE IF EXISTS `nationalities`;
CREATE TABLE `nationalities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `nationalities`
--

/*!40000 ALTER TABLE `nationalities` DISABLE KEYS */;
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('1','Afghan','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('2','Albanian','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('3','Algerian','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('4','American','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('5','Andorran','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('6','Angolan','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('7','Antiguans','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('8','Argentinean','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('9','Armenian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('10','Australian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('11','Austrian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('12','Azerbaijani','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('13','Bahamian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('14','Bahraini','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('15','Bangladeshi','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('16','Barbadian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('17','Barbudans','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('18','Batswana','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('19','Belarusian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('20','Belgian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('21','Belizean','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('22','Beninese','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('23','Bhutanese','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('24','Bolivian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('25','Bosnian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('26','Brazilian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('27','British','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('28','Bruneian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('29','Bulgarian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('30','Burkinabe','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('31','Burmese','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('32','Burundian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('33','Cambodian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('34','Cameroonian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('35','Canadian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('36','Cape Verdean','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('37','Central African','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('38','Chadian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('39','Chilean','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('40','Chinese','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('41','Colombian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('42','Comoran','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('43','Congolese','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('44','Costa Rican','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('45','Croatian','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('46','Cuban','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('47','Cypriot','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('48','Czech','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('49','Danish','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('50','Djibouti','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('51','Dominican','2015-05-04 09:59:38','2015-05-04 09:59:38');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('52','Dutch','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('53','East Timorese','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('54','Ecuadorean','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('55','Egyptian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('56','Emirian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('57','Equatorial Guinean','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('58','Eritrean','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('59','Estonian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('60','Ethiopian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('61','Fijian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('62','Filipino','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('63','Finnish','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('64','French','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('65','Gabonese','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('66','Gambian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('67','Georgian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('68','German','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('69','Ghanaian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('70','Greek','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('71','Grenadian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('72','Guatemalan','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('73','Guinea-Bissauan','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('74','Guinean','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('75','Guyanese','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('76','Haitian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('77','Herzegovinian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('78','Honduran','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('79','Hungarian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('80','I-Kiribati','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('81','Icelander','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('82','Indian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('83','Indonesian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('84','Iranian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('85','Iraqi','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('86','Irish','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('87','Israeli','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('88','Italian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('89','Ivorian','2015-05-04 09:59:39','2015-05-04 09:59:39');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('90','Jamaican','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('91','Japanese','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('92','Jordanian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('93','Kazakhstani','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('94','Kenyan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('95','Kittian and Nevisian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('96','Kuwaiti','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('97','Kyrgyz','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('98','Laotian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('99','Latvian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('100','Lebanese','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('101','Liberian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('102','Libyan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('103','Liechtensteiner','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('104','Lithuanian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('105','Luxembourger','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('106','Macedonian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('107','Malagasy','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('108','Malawian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('109','Malaysian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('110','Maldivan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('111','Malian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('112','Maltese','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('113','Marshallese','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('114','Mauritanian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('115','Mauritian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('116','Mexican','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('117','Micronesian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('118','Moldovan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('119','Monacan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('120','Mongolian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('121','Moroccan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('122','Mosotho','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('123','Motswana','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('124','Mozambican','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('125','Namibian','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('126','Nauruan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('127','Nepalese','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('128','New Zealander','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('129','Nicaraguan','2015-05-04 09:59:40','2015-05-04 09:59:40');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('130','Nigerian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('131','Nigerien','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('132','North Korean','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('133','Northern Irish','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('134','Norwegian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('135','Omani','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('136','Pakistani','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('137','Palauan','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('138','Panamanian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('139','Papua New Guinean','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('140','Paraguayan','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('141','Peruvian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('142','Polish','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('143','Portuguese','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('144','Qatari','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('145','Romanian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('146','Russian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('147','Rwandan','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('148','Saint Lucian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('149','Salvadoran','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('150','Samoan','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('151','San Marinese','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('152','Sao Tomean','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('153','Saudi','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('154','Scottish','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('155','Senegalese','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('156','Serbian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('157','Seychellois','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('158','Sierra Leonean','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('159','Singaporean','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('160','Slovakian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('161','Slovenian','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('162','Solomon Islander','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('163','Somali','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('164','South African','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('165','South Korean','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('166','Spanish','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('167','Sri Lankan','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('168','Sudanese','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('169','Surinamer','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('170','Swazi','2015-05-04 09:59:41','2015-05-04 09:59:41');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('171','Swedish','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('172','Swiss','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('173','Syrian','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('174','Taiwanese','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('175','Tajik','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('176','Tanzanian','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('177','Thai','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('178','Togolese','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('179','Tongan','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('180','Trinidadian/Tobagonian','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('181','Tunisian','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('182','Turkish','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('183','Tuvaluan','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('184','Ugandan','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('185','Ukrainian','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('186','Uruguayan','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('187','Uzbekistani','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('188','Venezuelan','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('189','Vietnamese','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('190','Welsh','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('191','Yemenite','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('192','Zambian','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `nationalities` (`id`,`name`,`created_at`,`updated_at`) VALUES ('193','Zimbabwean','2015-05-04 09:59:42','2015-05-04 09:59:42');
/*!40000 ALTER TABLE `nationalities` ENABLE KEYS */;


--
-- Create Table `news_items`
--

DROP TABLE IF EXISTS `news_items`;
CREATE TABLE `news_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `image_legend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photographer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments_count` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `news_items_organisation_id_index` (`organisation_id`),
  KEY `news_items_user_profile_id_index` (`user_profile_id`),
  KEY `news_items_user_id_index` (`user_id`),
  CONSTRAINT `news_items_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `news_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `news_items_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items`
--

/*!40000 ALTER TABLE `news_items` DISABLE KEYS */;
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('104','75F75519-F0AA-4491-A6F8-1AB536057D80','UAE: Three sisters released after three months in secret detention for tweeting','uae-three-sisters-released-after-three-months-in-secret-detention-for-tweeting','Three sisters were reunited with their family today after spending three months in secret detention after the United Arab Emirates (UAE) authorities subjected them to enforced disappearance, Amnesty International said. They were detained after posting comments on Twitter on behalf of their brother, a prisoner of conscience in the Gulf state.<br />\r\n<br />\r\nAccording to Ahmed Mansoor, a prominent human rights defender, the sisters, Asma Khalifa al-Suwaidi, Mariam Khalifa al-Suwaidi and Dr Alyaziyah Khalifa al-Suwaidi, were dropped off at their family home at close to noon local time today.<br />\r\n<br />\r\nThey had not been heard from since they were summoned for questioning at an Abu Dhabi police station on 15 February and then taken into the custody of the UAE&rsquo;s state security apparatus.\r\n<blockquote>\r\n<p><strong style=\"line-height: 1.6em;\"><q>It is not yet known what pressure the al-Suwaidi sisters were under while in detention, if they were charged with any offence, or if their release carries any conditions. What is clear, however, is that these three women should never have been detained in the first place.</q>&nbsp;</strong><strong style=\"line-height: 1.6em;\">Said Boumedouha, Amnesty International&#39;s Deputy Director for the Middle East and North Africa Programme</strong></p>\r\n</blockquote>\r\n<br />\r\n&ldquo;It is not yet known what pressure the al-Suwaidi sisters were under while in detention, if they were charged with any offence, or if their release carries any conditions,&rdquo; said Said Boumedouha, Amnesty International&#39;s Deputy Director for the Middle East and North Africa programme.<br />\r\n<br />\r\n&ldquo;What is clear, however, is that these three women should never have been detained in the first place. If necessary, we&rsquo;ll continue to campaign on the world stage to call for all charges and conditions to be dropped.<br />\r\n<br />\r\n&ldquo;Enforced disappearance is a crime under international law. It is a chilling act of repression for the state to silence activists&rsquo; families by locking them up for months, with no access to their loved ones or the outside world.<br />\r\n<br />\r\n&quot;While the three sisters&rsquo; release must clearly come as a huge relief for their family and loved ones, the fact of the matter remains that their peaceful tweets have been punished with enforced disappearance. Those responsible must be brought to justice in fair trials.&rdquo;<br />\r\n<br />\r\nPrisoners held in secret detention under the authority of the UAE&rsquo;s state security apparatus are extremely vulnerable and are at particularly high risk of torture or other ill-treatment. Enforced disappearance in itself violates the absolute ban on torture and other ill-treatment. Amnesty International has been campaigning since February for the immediate and unconditional release of the three al-Suwaidi sisters.<br />\r\n<br />\r\nThe organization is calling on the UAE authorities to immediately and unconditionally release all prisoners of conscience.','/uploads/media/106/news/Amesty.png','','Sophie James / Shutterstock.com','0','1','0','1','2015-05-19 00:53:51','2015-05-20 23:43:07','55','106','106','3');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('105','B9D9311A-BE44-4E57-B7F3-ED9D0D94B9D8','ADB, Japan to Help Asia Tap Space Technologies to Counter Disasters ','adb-japan-to-help-asia-tap-space-technologies-to-counter-disasters','MANILA, PHILIPPINES &ndash; The Asian Development Bank (ADB) and Japan are to help developing countries in Asia and the Pacific tap the latest technologies, including satellite maps, to help them prepare and respond more effectively and quickly to natural disasters.<br />\r\n<br />\r\nA $2 million technical assistance grant from the Japan Fund for Poverty Reduction, administered by ADB, will be used to train government, community officials and local volunteers in Armenia, Bangladesh, Fiji, and the Philippines to use new state-of-the-art space-based technology and other high tech tools for disaster planning. These four countries will act as pilots for the potential wider adoption of these technologies across the region.<br />\r\n<br />\r\nThe use of space-based technology, including satellite-based systems like GPS, for disaster planning and response has been growing in recent years. However many developing countries lack the funds and expertise to adopt new technologies which can supplement their existing early warning and disaster monitoring systems.<br />\r\n<br />\r\n&ldquo;Countries which are vulnerable to catastrophes need more information-based disaster risk management and response tools to prepare better before disasters strike, and to respond better after earthquakes, floods or typhoons hit,&rdquo; said Yusuke Muraki, Infrastructure Specialist with ADB&rsquo;s Regional and Sustainable Development Department. &ldquo;Space-based technology can help these pilot countries improve their resilience in an efficient, sustainable way with reliable and timely disaster-related data.&rdquo;<br />\r\n<br />\r\nThe technical assistance project will train government agencies and local communities in the target countries to use OpenStreetMap, a community-based digital mapping platform, and mobile phone applications, which allow them to collect community-based information for disaster risk planning. They will also get support to use other computer-based and mobile phone applications for managing post-disaster situations more effectively, including for evacuations and the timely delivery of relief goods. &nbsp;Combining satellite-based hazard maps with existing local government maps of vulnerable areas helps identify potential disaster locations more precisely.<br />\r\n<br />\r\nAlong with training, the project will also draw up policy guidelines to guide the use of the new technologies, providing a potential blueprint for their wider adoption in other parts of the region.<br />\r\n<br />\r\nADB, based in Manila, is dedicated to reducing poverty in Asia and the Pacific through inclusive economic growth, environmentally sustainable growth, and regional integration. Established in 1966, it is owned by 67 members &ndash; 48 from the region.&nbsp;','/uploads/media/101/news/ADB.jpg','ADB’s technical assistance will help pilot countries adopt modern technology to supplement their existing early warning and disaster monitoring systems. Devastation caused by Typhoon Haiyan (Yolanda) in the Philippines. ','','0','1','0','1','2015-05-20 23:29:44','2015-05-20 23:56:20','54','105','105','1');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('106','86ED9ACA-AB8B-40FA-9623-8079A73AD5BB','Support for Vanuatu\'s cyclone recovery','support-for-vanuatus-cyclone-recovery','Media release - 16 April 2015. Australia will provide additional assistance to Vanuatu to support its recovery from Tropical Cyclone Pam, meeting additional needs and to ensure its long-term recovery.<br />\r\n<br />\r\nEducation, health and food security have been assessed by the Government of Vanuatu and its partners as priority areas for assistance. Australia will provide an extra $5 million to help meet these needs, comprising:<br />\r\n<br />\r\n&nbsp; &nbsp; $2.3 million for urgent school repairs and the replacement of damaged learning materials;<br />\r\n&nbsp; &nbsp; $1.5 million to repair health infrastructure, re-stock pharmaceutical supplies, strengthen immunisation and support the cold storage and transport of medicines; and<br />\r\n&nbsp; &nbsp; $1.2 million to address urgent food needs and restore local food sources.<br />\r\n<br />\r\nThis package builds on the more than $10 million already provided by Australia for emergency relief efforts.<br />\r\n<br />\r\nAustralia&rsquo;s additional assistance marks the transition from our initial emergency response, including the drawdown of Australian Defence Force assets and personnel.','/uploads/media/108/news/AUSAID.JPG','Relief supplies have begun arriving in Port Vila','Care Australia','0','1','0','1','2015-05-20 23:51:19','2015-08-15 04:26:06','57','108','108','1');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('107','B033B124-FF8D-425C-9371-9BFF0A1D0296','UK leads global crackdown on illegal wildlife trade','uk-leads-global-crackdown-on-illegal-wildlife-trade','<br />\r\nBritain will provide new funding to a global crackdown on the trade in rhino horn, elephant ivory and other illegal wildlife products, International Development Secretary Justine Greening announced today.<br />\r\n<br />\r\nAs well as strengthening law enforcement and reducing demand for illegal products, the &pound;3 million of new support will focus on helping communities affected by the illegal wildlife trade boost their incomes through wildlife conservation.<br />\r\n<br />\r\nJustine Greening said:<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;Poaching threatens endangered animals such as the white rhino, African elephant and snow leopard. It also feeds corruption, undermines global security and costs governments billions in lost revenues every year.&quot;<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;We cannot and will not look the other way while this continues.&quot;<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;This new funding will improve law enforcement, reduce demand for illegal wildlife products and, importantly, help communities find new sources of income and free themselves from this horrific trade.&quot;<br />\r\n<br />\r\nSpeaking from the Illegal Wildlife Trade Conference in Botswana, DEFRA Minister Lord De Mauley said:<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;Wildlife crime has reached unprecedented levels. We must not stand idly by. We must honour the commitments made in London and build on them at Kasane. This issue affects many of our important international partners deeply; and is much more than an environmental issue; it is about tackling corruption, improving security and raising livelihoods.&quot;<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;If we wait any longer then we will wake up to find that all the endangered species living in the wild have been killed. We must send a powerful message to poachers and traffickers wherever they operate: the illegal wildlife trade ends here.&quot;<br />\r\n<br />\r\nThe additional &pound;3 million for the Illegal Wildlife Trade (IWT) challenge fund, financed by the Department for International Development (DFID) and managed by the Department for Environment, Food and Rural Affairs (DEFRA), builds on the &pound;10 million fund announced ahead of the London Conference on the Illegal Wildlife Trade, held in February last year. The first round of Britain&rsquo;s challenge fund is now supporting 19 projects around the world.<br />\r\n<br />\r\nProjects supported in the first call for funding include:\r\n<ul>\r\n	<li>&nbsp; &nbsp; Sending experts to border points in the Horn Of Africa to provide training on wildlife law</li>\r\n	<li>&nbsp; &nbsp; Funding Interpol to improve cooperation between national agencies in Uganda, Kenya and South Africa</li>\r\n	<li>&nbsp; &nbsp; Increasing the number of successful convictions across Africa with a new wildlife forensic network, linking DNA &nbsp; &nbsp;laboratories across the region.</li>\r\n</ul>\r\nThe illegal wildlife trade is booming. Rhino horn is now worth more than gold and is more valuable on the black market than diamonds or cocaine. The ivory trade has more than doubled since 2007. UNEP estimate that the trade costs between $7 and $23 billion every year in lost revenues, primarily for governments in the developing world.<br />\r\n<br />\r\nBut since last year&rsquo;s London Conference there has been solid progress. Over the past 12 months ivory stockpiles have been destroyed in countries from Hong Kong to Chad. There is now tightened security, more training and more arrests across the globe.<br />\r\n<br />\r\nThe second call for proposals to the IWT fund will soon be open for applications. There will be &pound;5 million available in this second round of the Illegal Wildlife Trade Challenge Fund, including &pound;3 million of new funds plus &pound;2 million in existing funding.<br />\r\n<br />\r\nOrganisations can bid for funding for projects in developing countries that address the following themes:\r\n<ul>\r\n	<li>&nbsp; &nbsp; Developing sustainable livelihoods for communities affected by illegal wildlife trade</li>\r\n	<li>&nbsp; &nbsp; Strengthening law enforcement and the role of the criminal justice system</li>\r\n	<li>&nbsp; &nbsp; Reducing demand for the products of the illegal wildlife trade</li>\r\n</ul>\r\n&nbsp;\r\n\r\n<p><strong>Press office</strong></p>\r\n\r\n<div>\r\n<div>Email<br />\r\n<a href=\"mailto:pressoffice@dfid.gov.uk\">pressoffice@dfid.gov.uk</a><br />\r\nTelephone020 7023 0600</div>\r\n\r\n<div class=\"comments\" style=\"border: none; margin: 10px 0px; padding: 7px 0px 3px; line-height: 1.25; width: 302px; float: left;\">Follow the DFID Press office on Twitter - @DFID_Press</div>\r\n</div>\r\n','/uploads/media/113/news/DFID 2.jpg','The UK Border Force cracks down on international trade in endangered species. Grant Miller with some of the items confiscated at Heathrow ','Glenn Copus','0','1','0','1','2015-05-21 00:18:12','2015-08-15 04:41:06','61','113','113','4');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('108','A2EB0EB9-E469-49F0-872A-F912FC756F91','Harper Government Announces Support to Improve Children’s Access to Quality Education in Developing Countries','harper-government-announces-support-to-improve-childrens-access-to-quality-education-in-developing-countries','April 16, 2015 - Washington, D.C. - Foreign Affairs, Trade and Development Canada<br />\r\n<br />\r\nToday, the Honourable Christian Paradis, Minister of International Development and La Francophonie, announced that Canada will contribute a total of C$120 million to the Global Partnership for Education, aimed at enhancing education in developing countries. This signals Canada&rsquo;s continued and deep commitment to promoting education for girls and boys in developing countries.<br />\r\n<br />\r\nCanada is also committed to working with key partners, including UN Special Envoy for Global Education Gordon Brown, the Global Partnership for Education, and UNICEF, to ensure that children in crisis situations have opportunities to learn. Minister Paradis announced that Canada is contributing an additional C$10 million to UNICEF for education and child protection in humanitarian crises around the world.<br />\r\n<br />\r\nHe made the announcement following the Safe Schools: Reaching all Children with Education in Lebanon event during the Spring Meetings of the World Bank and International Monetary Fund in Washington, D.C. He was joined by UN Special Envoy for Global Education Gordon Brown, the Global Partnership for Education&rsquo;s Chief Executive Officer, Alice Albright, and UNICEF&rsquo;s Executive Director, Anthony Lake.<br />\r\n<br />\r\nCanada is committed to ensuring the world&rsquo;s poorest and most vulnerable children and youth have access to quality education in safe and secure environments. Canada&#39;s international development programming in basic education is framed by the Securing the Future of Children and Youth strategy, which identifies access to quality basic education as a priority for action for Canada.<br />\r\nQuick Facts<br />\r\n<br />\r\n&nbsp; &nbsp; The Global Partnership for Education comprises close to 60 developing countries, as well as donor governments, international organizations, the private sector, teachers, and local and global civil society organizations. It has achieved significant results in primary school enrolment, literacy, and girls&rsquo; education, particularly in fragile and conflict-affected states.<br />\r\n&nbsp; &nbsp; UNICEF works in 190 countries and territories with a special focus on reaching the most vulnerable and excluded children, to the benefit of all children. In 2014, UNICEF helped provide over 8.6 million children in humanitarian situations around the world with access to basic education.<br />\r\n&nbsp; &nbsp; Improving maternal, newborn and child health is Canada&#39;s top development priority. With our global partners, we have achieved remarkable results: fewer women are dying in pregnancy and childbirth, and millions more children are celebrating their fifth birthday.<br />\r\n<br />\r\nQuotes<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;This will make a real difference in the lives of children in developing countries. It will help to ensure they have access to quality education, which is vital to help them move out of poverty and build prosperous futures. Canada aims to ensure that children not only survive, but have the opportunity to thrive, from their first years of life to adulthood. This is also why we have taken a leadership role worldwide to promote maternal, newborn and child health and to make strides to end child, early and forced marriage.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Christian Paradis, Minister of International Development and La Francophonie<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;In this crucial year as we approach the MDG deadline for achieving universal education, the new announcements today demonstrate Canada&#39;s clear leadership on global education and commitment to the 58 million out-of-school children. I am pleased that Minister Paradis has pledged to do even more to support education for millions of children in conflict-affected areas and humanitarian crises, including Syrian refugees in Lebanon and other neighboring countries.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Gordon Brown, UN Special Envoy for Global Education<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;We are delighted about Canada&rsquo;s contribution and we are grateful for this increased funding for the next four years and for the government&rsquo;s leadership on helping improve education in some of the poorest countries around the world, many of them torn by conflict.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Alice Albright, Chief Executive Officer of the Global Partnership for Education<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;A child&rsquo;s right to education is most at risk during humanitarian crises. Yet those are precisely the times when education is most needed, offering hope, protection and a vital sense of normalcy for children whose lives have been devastated by conflict or natural disaster. That&rsquo;s why we are so grateful to the Canadian government and people for their continued, generous support to UNICEF&#39;s education in emergencies programs.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Anthony Lake, Executive Director for UNICEF.','/uploads/media/115/news/FATDC-Tanzanian school children greet Prime Minister Harper.jpg','Tanzanian school children greet Prime Minister Harper - Tanzania, Nov. 26, 2007.','TOM HANSON / The Canadian Press','0','1','0','1','2015-05-21 00:53:20','2015-06-11 09:07:12','63','115','115','0');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('109','CCE3D778-2CA8-4381-8A7C-E8BA0C76F776','Millions of Yemenis face food insecurity amidst escalating conflict. ','millions-of-yemenis-face-food-insecurity-amidst-escalating-conflict-more-than-8-million-needed-to-support-farmers-during-crucial-cropping-season','15 April 2015, Rome - Amidst escalating conflict at a crucial time in the country&rsquo;s cropping season, almost 11 million people in Yemen are severely food insecure and millions more are at risk of not meeting their basic food needs, FAO said today.<br />\r\n<br />\r\nAccording to the organization&#39;s latest assessment, increasing conflict in nearly all major towns across the country is disrupting markets and trade, driving up local food prices and hampering agricultural production, including land preparation and planting for the 2015 maize and sorghum harvests.<br />\r\n<br />\r\n10.6 million Yemenis are now severely food insecure, of which 4.8 million are facing &quot;emergency&quot; conditions, suffering from severe lack of food access, very high and increasing malnutrition, and irreversible destruction of livelihoods.<br />\r\n<br />\r\nAround 850,000 children are acutely malnourished.<br />\r\n<br />\r\nMore than half of Yemen&rsquo;s population &ndash; some 16 million out of a total of 26 million&mdash; is in need of some form of humanitarian aid and has no access to safe water.<br />\r\n<br />\r\nThe latest escalation of conflicts is expected to further increase food insecurity in the poverty-stricken country. Paradoxically, some 2.5 million food producers, including farmers, pastoralists, fishermen and agricultural wage labourers, are among those identified as food insecure.<br />\r\n<br />\r\n&ldquo;We are entering a crucial period for crop production in Yemen and now, more than ever, agriculture cannot be an afterthought if we want to prevent more people from becoming food insecure amidst this crisis,&rdquo; said FAO Representative for Yemen, Salah Hajj Hassan.<br />\r\n<br />\r\nGovernorates in the far Northwest and South are most severely affected by food insecurity.<br />\r\n<br />\r\nMarket disruption<br />\r\n<br />\r\nIn some areas, like the western port city of Hodeidah, food prices have doubled and fuel prices have quadrupled. Further increases are expected as a result of fuel shortages and the impact of civil unrest on imports and transportation networks across Yemen. While agriculture provides the livelihood of nearly two-thirds of Yemenis, the country also relies heavily on imports of staple crops.<br />\r\n<br />\r\nAt the same time, service infrastructure has collapsed and government safety net programs have been suspended, handing an extra blow to millions of poor households.<br />\r\n<br />\r\nCritical work in Yemen<br />\r\n<br />\r\nIn a very challenging field environment, FAO and partners have since 2014 been working to support local farmers and internally displaced people to strengthen their livelihoods by distributing crop production packages, home gardening kits and fisheries inputs. They have also provided vaccinated poultry and goats for backyard livestock production.<br />\r\n<br />\r\nAdditional animal vaccination drives and plant health campaigns have helped farmers protect their agricultural assets, such as livestock and trees, from disease and locust threats.<br />\r\n<br />\r\nSince 2014, more than 90,000 people (13,450 families) have benefited from these FAO programs. Security conditions permitting, the Organization aims to reach nearly 235,000 people through its 2014-15 response plan for Yemen, but more funding is needed. Currently, only $4 million of the required $12 million have been made available for the livelihood programs.<br />\r\n<br />\r\n&ldquo;Even before fighting intensified this spring, Yemenis were in dire need of support to build up their agricultural production,&rdquo; said Abdessalam Ould Ahmed, FAO Assistant Director-General for North Africa and the Near East. &ldquo;The deteriorating situation means we need to double down on our efforts to ensure that as many farmers as possible are able to plant this growing season and strengthen their ability to withstand future shocks.&rdquo;','/uploads/media/114/news/FAO Food_Insecurity_Yemen.jpg','Escalation of conflict is expected to further increase food insecurity in the poverty-stricken Yemen.','','0','1','0','1','2015-05-21 01:00:32','2015-08-13 00:09:31','62','114','114','4');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('110','00C20A2F-9B49-43E8-BA3B-3A917C50E114','Didier Drogba to join Ronaldo and Zidane in 12th annual Match Against Poverty','didier-drogba-to-join-ronaldo-and-zidane-in-12th-annual-match-against-poverty','Proceeds to support Ebola recovery efforts<br />\r\n<br />\r\nGeneva / St-Etienne &ndash; Football superstar and Chelsea striker Didier Drogba has joined a star-studded array of international active and retired players for the 12th Annual Match Against Poverty which will take place on 20 April at the Geoffroy-Guichard Stadium in Saint-Etienne, France. Didier Drogba, who like Ronaldo and Zin&eacute;dine Zidane is also a UNDP Goodwill Ambassador, will team up with the other football stars for a match against an AS St-Etienne All Stars team to help boost Ebola recovery efforts.&nbsp;<br />\r\n<br />\r\nTwo thirds of the match proceeds will support UNDP&rsquo;s work in the hardest-hit countries of Guinea, Liberia and Sierra Leone, helping them to build back better from the epidemic. The remaining third will go to the Club&rsquo;s Association &ldquo;ASSE Coeur-Vert&rdquo;, asd which supports social projects in Saint-Etienne.&nbsp;<br />\r\n<br />\r\n&ldquo;I am honoured to support the people of the three countries who are trying to get through the devastation caused by the Ebola epidemic&rdquo;, said Drogba, &ldquo;and I encourage everyone to pull together to end this crisis, and to prevent it ever happening again.&rdquo; The Ivorian football legend joins the friendly match for the second time. &ldquo;I am also thrilled to be with Ronaldo, Zidane and all the other players to make a difference in the fight against Ebola.&rdquo;&nbsp;<br />\r\n<br />\r\nThe players confirmed up to now in the &ldquo;Drogba, Ronaldo, Zidane, &amp; Friends&rdquo; team are: Cafu, Clarence Seedorf, Edwin van der Sar, &Eacute;ric Abidal, Christian Karembeu, Youri Djorkaeff and Fabien Barthez. Former and current St. Etienne players will comprise the St Etienne All Stars team. Thirteen players have confirmed up to now: Alex, J&eacute;r&eacute;mie Janot, Pascal Feindouno, Dominique Rocheteau, Lionel Potillon, Ľubom&iacute;r Moravč&iacute;k, Laurent Paganelli, Bjorn Kvarme, Efstathios Tavlaridis, S&eacute;bastien Perez, St&eacute;phane P&eacute;dron, Julien Sabl&eacute; and Laurent Batlles. AS St Etienne&rsquo;s coach Christophe Galtier will coach the team. Pierluigi Collina, the legendary six-time World Referee winner, will referee the Match. More top international and AS Saint-Etienne names are expected to confirm their participation in the coming days.&nbsp;<br />\r\n<br />\r\nThe match, which will be televised globally, is supported by football&rsquo;s governing body, the F&eacute;d&eacute;ration Internationale de Football Association (FIFA) and organised in partnership with the Union des Associations Europ&eacute;ennes de Football (UEFA).&nbsp;<br />\r\n<br />\r\nThe 12th Match Against Poverty in St. Etienne will start at 20:00 local time. Ticket prices range from 8 to 12 Euros and are on sale at: http://www.asse.fr and ticket counters of the Stade Geoffroy-Guichard,14, rue Paul et Pierre Guichard, 42000 Saint-Etienne.<br />\r\nContact Information<br />\r\n<br />\r\nAziyad&eacute; Poltier-Mutal, UNDP in Geneva: aziyade.poltier@undp.org ; tel: +41 917 83 68, cell: +41 79 349 16 10&nbsp;','/uploads/media/124/news/UNDP.jpg','Drogba, Ronaldo and Zidane at the Match Against Poverty in 2012','','0','1','0','1','2015-05-21 01:10:20','2015-08-13 00:09:09','72','124','124','6');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('111','7E97D9E2-240A-4130-B76D-75776B77FDF2','Uprooted by Boko Haram. Stories of displaced children and women in Nigeria','uprooted-by-boko-haram-stories-of-displaced-children-and-women-in-nigeria','Violent attacks carried out by members of the Boko Haram rebel group have resulted in unspeakable atrocities in north-eastern Nigeria. Children have seen their loved ones killed and watched as their homes and schools have been damaged or destroyed. Some have walked for days to escape, fleeing with nothing more than the clothes on their backs.<br />\r\nThese are the stories of children and women who have survived the conflict and sought refuge in encampments in Yola, Adamawa State. They are among more than 1.2 million people uprooted by the violence who remain internally displaced.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0474/Esiebo<br />\r\n<br />\r\n(Right) Rose Zeeharrah watched while members of Boko Haram attacked her village and began killing the men who lived there &mdash; including her husband. As she fled into the bush with her nine children, the last sight she saw was her home being set ablaze. &ldquo;We didn&rsquo;t bring anything with us. We just ran,&rdquo; she said. Her 2-year-old son passed away while they were in hiding. &ldquo;He died from the stress,&rdquo; Rose explained. Now she and her children are living in a camp for internally displaced people in Yola, Adamawa State. Even though she knows she has lost her home and cattle, Rose longs to go home. &ldquo;I want to go home and harvest so we can eat,&rdquo; she said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0475/Esiebo<br />\r\n<br />\r\nJohn, 17, was getting ready for church when members of Boko Haram attacked his village. He had already lost his father, a carpenter, and his mother to illness before the violence reached his hometown. Along with his aunt, John managed to escape to a displacement camp in Yola. What he misses most is his father&rsquo;s house, his village and the memories he made there. &ldquo;I want to go back there with my aunt,&rdquo; he said. John is attending a UNICEF-supported school in the camp and is determined to become a pastor and have a positive impact on his community. &ldquo;I want to work for God,&rdquo; he said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0476/Esiebo<br />\r\n<br />\r\nEvelyn was attending church when members of Boko Haram entered her hometown, in the Local Government Area of Michika, and began shooting and killing people, and kidnapping some of the girls from the community. Evelyn grabbed her 1-year-old daughter, Rose (above), and ran, but her 5-year-old son, Wisdom, was in another part of the church made festive with special decorations for children. &ldquo;It was children&rsquo;s day,&rdquo; Evelyn explained in a soft voice. She hid in the mountains with other children, women and men from her community. One week later, she was reunited with Wisdom, whom members of the church brought to her, but she has not seen or heard from her husband since the attack. &ldquo;I don&rsquo;t know if he is alive or dead,&rdquo; she said. With only the clothes on their backs, she and her children stayed in the mountains for one month, barely surviving on berries and swamp water, before making the journey to Yola, where they now live in a camp for displaced people. Evelyn, who studied economics before she married, longs to return home, but she knows it will be difficult. &ldquo;They have taken everything,&rdquo; she said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0477/Esiebo<br />\r\n<br />\r\nWhen members of Boko Haram attacked 13-year-old Aisha&rsquo;s hometown &mdash; Gwozo, in Borno State &mdash; they killed her father and abducted her mother. She managed to escape, fleeing to a displacement camp in Yola with an older sister. The two continue to look after each other. Aisha has received UNICEF-supported counselling. Now strong enough to return to learning, she attends a UNICEF-supported school, which operates in two shifts in the camp to accommodate more children. &ldquo;I enjoy the school here,&rdquo; she said, &ldquo;but I want to go back to my village.&rdquo;<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0479/Esiebo<br />\r\n<br />\r\nWhen Alia, 10, still lived in her hometown, in the Local Government Area of Michika, word came on a Friday that members of Boko Haram had attacked neighbouring villages. The next day, the men arrived in her town. Her father was killed. Alia, together with her mother and other family members, managed to flee to the town of Mubi, leaving all their belongings behind, but three months later, that town also came under attack. They then fled across the border to neighbouring Cameroon before slowly making their way back to Nigeria and ending up in a displacement camp in Yola, where Alia is attending a UNICEF-supported school. Her mother suffered from high blood pressure and diabetes before the attacks, and now she is very sick, adding to Alia&rsquo;s worries. Alia misses her father and her friends, and she is scared that attacks will occur again, but she refuses to give up hope. &ldquo;I want to be a nurse,&rdquo; she said. &ldquo;I want to help people.&rdquo;<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0478/Esiebo<br />\r\n<br />\r\nA few months back, members of Boko Haram arrived in Lydia James&rsquo;s hometown as her husband set out to visit his mother and take food to her. They tied him up, dragged him beneath a tree and shot him before turning their sights on Lydia&rsquo;s house. She and her nine children ran for their lives. &ldquo;I ask God to help me so I can take care of the children,&rdquo; she said. Lydia now shares a cramped dormitory with 78 other women and children in a camp for internally displaced people, in Yola.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0480/Esiebo<br />\r\n<br />\r\nMaryamu Yakudu normally would have been in church when members of Boko Haram attacked her hometown, in the Local Government Area of Michika, but she was home sick that day. When she heard gunshots, she grabbed her daughter, 1-year-old Hyladan Yakudu, and ran. She has not seen her husband since and fears the worst. Her mother, who was too frail to run, remained in the village. &ldquo;I just brought my one wrapper &mdash; nothing else,&rdquo; says Maryamu. She longs to return home, as soon as it is safe &mdash; even though she knows there is little to which to return. &ldquo;There is nothing in our house,&rdquo; she said. &ldquo;They took it all. We had a motorcycle and many cows. It&rsquo;s all gone.&rdquo;<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0481/Esiebo<br />\r\n<br />\r\nA few years ago, Samson, 16, lost his father, who was a member of government forces fighting against Boko Haram. When the news reached his mother, the shock was too much for her to handle &mdash; she became ill, and shortly after, she passed away, leaving Samson to be cared for by his grandmother. But the violence would soon catch up with Samson again. A few months ago, the church in his home village, in the Local Government Area of Michika, was attacked. Many of the men were killed, while women and children fled in panic. &ldquo;We just started running,&rdquo; recalled Samson. He and his grandmother arrived at a displacement camp in Yola, where Samson now attends a UNICEF-supported school, but he dreams of going home. &ldquo;I miss the house and all the people in the community, and I miss playing football with my friends,&rdquo; he said. He also misses feeling safe. Samson already knows that he wants to follow in his father&rsquo;s footsteps. &ldquo;I want to be a soldier,&rdquo; he said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0482/Esiebo<br />\r\n<br />\r\nTwo years ago, Hajja Bello was shopping in a marketplace in the village of Dagu, in Borno State, when members of Boko Haram attacked. &ldquo;It was a Sunday morning, and they assembled everyone and then separated the women from the men,&rdquo; she recalled. &ldquo;They shot the husbands before our eyes.&rdquo; She managed to escape with all five of her children, but she lost five relatives that day, including a brother and four half-brothers. With the support of UNICEF and partners, her children are attending school in the camp, but she misses everything about home &mdash; the farm, her neighbours, the marketplace and the other women there with whom she used to chat. &ldquo;I just want this to end so I can go back,&rdquo; she said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0483/Esiebo<br />\r\n<br />\r\n&ldquo;When the shooting started, we ran in panic into the mountains,&rdquo; remembered Lydia John, 15, about the day members of Boko Haram attacked her village, Gidel in the Local Government Area of Michika. Residents could see their homes being burned and schools being damaged from the mountains, where they remained trapped for two weeks, occasionally using the cover of night to gather a bit of food. After they managed to leave, they walked for seven straight days to reach the town of Mubi and eventually made it to Yola, where Lydia now lives in a camp for internally displaced people. She misses being home on her family&rsquo;s farm, where there was plenty to eat, and like any adolescent, she misses her friends, who have been scattered across the country, with some fleeing to Abuja, the capital; six of her friends remain missing. Like so many others in the camp, she has lost loved ones in the violence; her uncle and cousin were killed during the attack on her village. &ldquo;I always feel scared when I think of what happened,&rdquo; she said. Lydia is attending school every day in Yola and wants to be a doctor. &ldquo;I want to help my community,&rdquo; she explained. But then the worry returns. With the family&rsquo;s farm lost and their animals, including sheep, stolen or slaughtered, she is unsure how she, her parents and eight siblings will have enough to survive. &ldquo;It&rsquo;s not only what we will eat &mdash; but if all of that is lost, how will we have enough money for me to go to school?&rdquo; she asked.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0484/Esiebo<br />\r\n<br />\r\nSalamatu Chinaypi used to sell food in a local market to supplement the income her family earned from their farm &mdash; helping feed her nine children and send them all to school. But that, along with the family&rsquo;s home and some of their loved ones, was all lost when members of Boko Haram attacked Salamatu&rsquo;s hometown, Askira Uba, in Borno State. They separated the men from the women and then began killing the men, including Salamatu&rsquo;s brother. &ldquo;That&rsquo;s when everyone started running,&rdquo; she said. She and a surviving brother began to flee, but a shot rang out, and he, too, was killed. &ldquo;They burned our houses. There is nothing left,&rdquo; she said. She, her husband and their children managed to escape, and it took them three weeks and four days to reach Yola, where they now live in a camp for internally displaced people. &ldquo;We came barefooted,&rdquo; she said. &ldquo;We could bring nothing with us.&rdquo;<br />\r\n<br />\r\n<br />\r\nWhat would you miss most if you were forced to flee your home?<br />\r\nJoin the #BringBackOurChildhood campaign.','/uploads/media/125/news/UNICEF.jpg','','','0','1','0','1','2015-05-21 01:21:26','2015-07-26 13:08:47','73','125','125','7');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('112','7DE33FE2-9E38-4EAA-8F27-9089EFB40F07','USAID Announces $126 Million to Rebuild Life-Saving Health Services in Ebola-Affected Countries','usaid-announces-126-million-to-rebuild-life-saving-health-services-in-ebola-affected-countries','Saturday, April 18, 2015. Washington, DC: U.S. Agency for International Development (USAID) Associate Administrator Mark Feierstein announced plans to spend $126 million to help rebuild West African health systems impacted by the Ebola outbreak at the Global Citizen Concert on the National Mall. &nbsp;The funds will help Liberia, Sierra Leone, and Guinea restart critical health services that stopped due to the Ebola outbreak, including vaccinations, water and sanitation services, prenatal and maternal health care and nutrition, and programs to prevent and treat malaria and other infectious diseases.<br />\r\n<br />\r\nIn September last year, epidemiologists predicted the Ebola outbreak could result in 1.4 million cases within a few months, and the first case of Ebola landed in the U.S. homeland. &nbsp; The U.S. Government responded by providing $1.4 billion to fund 10,000 civilian responders, provide huge volumes of personal protective equipment, fund and train healthcare workers, deploy laboratories, support epidemiological surveillance and disease tracing, and launch a massive social messaging and education campaign to inform Liberians about practices to protect themselves against infection.<br />\r\n<br />\r\n&ldquo;USAID has helped West African nations by beating back the Ebola outbreak,&rdquo; said Associate Administrator Mark Feierstein. &nbsp;&ldquo;Now we&rsquo;re helping ensure people have food to eat, schools are open and educating children, people are able to communicate through a strong infrastructure network, and families can support themselves by returning to jobs and markets.&rdquo;<br />\r\n<br />\r\nThe U.S. Government and its partners helped Liberia&mdash;once the heart of the epidemic&mdash;bring down new cases of Ebola from more than 50 a day to zero.','/uploads/media/127/news/USAID.jpg','Health care workers put on personal protective equipment (PPE) before going into the hot zone at Island Clinic in Monrovia, Liberia on Sept. 22 2014.','Morgana Wingard, USAID','0','1','0','1','2015-05-21 01:38:47','2015-08-15 02:38:49','75','127','127','11');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('113','80497EA6-4EA7-4305-9F2A-019F35645270','Caribbean and Central American countries formalize partnership for catastrophe risk insurance','caribbean-and-central-american-countries-formalize-partnership-for-catastrophe-risk-insurance','WASHINGTON, April 18, 2015 - The Council of Ministers of Finance of Central America, Panama and the Dominican Republic (COSEFIN) and CCRIF SPC (formerly the Caribbean Catastrophe Risk Insurance Facility) signed today a memorandum of understanding that enables Central American countries to formally join the facility to access low cost, high quality sovereign catastrophe risk insurance.<br />\r\n<br />\r\nDuring the ceremony, CCRIF SPC and the Government of Nicaragua also signed a Participation Agreement for Nicaragua to become the first Central American country to formally join the facility. Other member nations of COSEFIN are expected to join CCRIF SPC later this year and in 2016.<br />\r\n<br />\r\n&ldquo;For Nicaragua, it is an honor to be the first member of COSEFIN countries to join the CCRIF. This insurance will allow us to strengthen financial resilience to natural disasters and continue our efforts to reduce poverty and respond to climate change challenges as part of our National Human Development Plan,&rdquo; said Ivan Acosta, Minister of Finance of Nicaragua.<br />\r\n<br />\r\nNine countries in Central America and the Caribbean experienced at least one disaster with an economic impact of more than 50 percent of their annual gross domestic product (GDP) since 1980. The impact of Haiti&rsquo;s earthquake was estimated at 120 percent of GDP. The same year, tropical cyclone Agatha, in Guatemala, had devastating consequences and poverty rates increased by 5.5 percent. Climate change also represents a significant development challenge, with average annual economic losses due to weather-related disasters amounting to 1 percent or more of GDP in ten Caribbean countries and four Central American nations, including Nicaragua.<br />\r\n<br />\r\nEstablished in 2007, CCRIF is the world&rsquo;s first multi-country catastrophe risk pooling mechanism which offers sovereign insurance at affordable rates to its members against hurricanes, earthquakes and excess rainfall. Currently, 16 Caribbean countries are members of CCRIF.<br />\r\n<br />\r\nThe facility enhances the fiscal resilience of its member countries to catastrophes caused by natural hazard events by providing immediate financial resources in the aftermath of a disaster, allowing governments to better respond to the initial needs of their populations and continue providing critical services. Since its inception, CCRIF has made twelve payouts totaling US$35.6 million to eight member governments. All payouts were transferred within two weeks after each event.<br />\r\n<br />\r\n&ldquo;After exploring options for engaging in sovereign disaster risk financing, Central American countries concluded that joining the CCRIF SPC facility was the most efficient and cost-effective insurance mechanism to pool our risk&rdquo;, said Mart&iacute;n Portillo, Executive Secretary of COSEFIN. &ldquo;This will allow us to reduce our countries&rsquo; fiscal vulnerability to the adverse effects associated with earthquakes, tropical cyclones, excess rainfall and other events.&rdquo;<br />\r\n<br />\r\nThis new 23-nation partnership will benefit both existing and new CCRIF members, providing low prices due to more efficient use of capital and insurance market instruments. New members will be able to take advantage of CCRIF&rsquo;s low premium costs and existing members could realize premium reductions due to the increased size of the CCRIF portfolio. This partnership between Caribbean and Central American countries could strengthen economic engagement throughout the greater Caribbean Basin.<br />\r\n<br />\r\n&ldquo;We foresee a range of benefits to both regions emanating from this partnership,&rdquo; said Milo Pearson, Chairman of CCRIF SPC, &ldquo;including the creation of a strategic mechanism in which Caribbean and COSEFIN countries can share best practices in disaster risk management and learn lessons from each other in advancing collaborative approaches to reducing vulnerabilities to hazards, alleviating poverty, enhancing debt and financial sustainability and creating a framework for the sustainable prosperity of the countries of both regions.&rdquo;<br />\r\n<br />\r\nThe World Bank provided the initial finance and technical advisory services to establish CCRIF in 2007 and in 2014 the Bank supplied resources to finance entrance fees to CCRIF for Honduras and Nicaragua, as well as annual insurance premiums for four years.<br />\r\n<br />\r\n&ldquo;This a real example of a regional public good where collective action has clear financial benefits and can help countries tackle the adverse impacts of climate change,&rdquo;, said Jorge Familiar, World Bank Vice President for Latin America and the Caribbean. &ldquo;We look forward to deepening our engagement with COSEFIN, CARICOM and CCRIF as part of this ambitious regional initiative.&rdquo;<br />\r\n<br />\r\nAbout CCRIF SPC<br />\r\n<br />\r\nCCRIF was developed under the technical leadership of the World Bank and with a grant from the Government of Japan. It was capitalized through contributions to a multi-donor Trust Fund by the Government of Canada, the European Union, the World Bank, the governments of the United Kingdom and France, the Caribbean Development Bank and the governments of Ireland and Bermuda, as well as through membership fees paid by participating governments','/uploads/media/128/news/WB-10-major-natural-disasters-predicted-soon-427002.jpg','','www.gfdrr.org','0','1','0','1','2015-05-21 01:51:09','2015-08-13 00:28:50','76','128','128','10');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('115','35BAFF3E-A4A3-46BA-94A8-704E8980E12B','Test1','test1','T1','/uploads/media/115/news/1508084994_Asian Farmers.jpg','','','0','0','0','1','2015-07-24 09:08:00','2015-07-24 09:08:00','63','115','115','0');
INSERT INTO `news_items` (`id`,`name`,`title`,`slug`,`content`,`image`,`image_legend`,`photographer_name`,`comments_count`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`view_count`) VALUES ('116','D4294CDD-3D7E-4D83-AB57-995EF5B8E4EB','Title test','title-test','text test','/uploads/media/171/news/87840280_Aidpost headline image JPEG.jpg','Photo legend test','Photo source test','0','1','0','1','2015-08-12 00:54:51','2015-08-12 00:54:51','97','171','171','0');
/*!40000 ALTER TABLE `news_items` ENABLE KEYS */;


--
-- Create Table `news_items_beneficiaries`
--

DROP TABLE IF EXISTS `news_items_beneficiaries`;
CREATE TABLE `news_items_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_beneficiaries_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_beneficiaries_beneficiary_id_index` (`beneficiary_id`),
  CONSTRAINT `news_items_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  CONSTRAINT `news_items_beneficiaries_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_beneficiaries`
--

/*!40000 ALTER TABLE `news_items_beneficiaries` DISABLE KEYS */;
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('107','105','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('118','110','17','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('120','107','27','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('123','109','26','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('126','106','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('127','104','29','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('130','108','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('131','112','11','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('133','111','13','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('135','113','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_beneficiaries` (`id`,`newsitem_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('136','116','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news_items_beneficiaries` ENABLE KEYS */;


--
-- Create Table `news_items_countries`
--

DROP TABLE IF EXISTS `news_items_countries`;
CREATE TABLE `news_items_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_countries_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_countries_country_id_index` (`country_id`),
  CONSTRAINT `news_items_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `news_items_countries_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_countries`
--

/*!40000 ALTER TABLE `news_items_countries` DISABLE KEYS */;
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('113','110','74','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('114','107','224','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('117','109','236','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('120','106','229','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('121','104','223','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('123','108','210','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('125','111','158','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('127','113','156','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_countries` (`id`,`newsitem_id`,`country_id`,`created_at`,`updated_at`) VALUES ('128','116','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news_items_countries` ENABLE KEYS */;


--
-- Create Table `news_items_crisis`
--

DROP TABLE IF EXISTS `news_items_crisis`;
CREATE TABLE `news_items_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_crisis_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_crisis_crisis_id_index` (`crisis_id`),
  CONSTRAINT `news_items_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  CONSTRAINT `news_items_crisis_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_crisis`
--

/*!40000 ALTER TABLE `news_items_crisis` DISABLE KEYS */;
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('103','105','49','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('114','110','47','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('116','107','44','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('119','109','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('122','106','45','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('125','108','47','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('126','112','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('128','111','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('130','113','45','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_crisis` (`id`,`newsitem_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('131','116','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news_items_crisis` ENABLE KEYS */;


--
-- Create Table `news_items_interventions`
--

DROP TABLE IF EXISTS `news_items_interventions`;
CREATE TABLE `news_items_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_interventions_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_interventions_intervention_id_index` (`intervention_id`),
  CONSTRAINT `news_items_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  CONSTRAINT `news_items_interventions_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_interventions`
--

/*!40000 ALTER TABLE `news_items_interventions` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_items_interventions` ENABLE KEYS */;


--
-- Create Table `news_items_regions`
--

DROP TABLE IF EXISTS `news_items_regions`;
CREATE TABLE `news_items_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_regions_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_regions_region_id_index` (`region_id`),
  CONSTRAINT `news_items_regions_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`),
  CONSTRAINT `news_items_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_regions`
--

/*!40000 ALTER TABLE `news_items_regions` DISABLE KEYS */;
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('108','105','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('119','110','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('121','107','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('124','109','5','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('127','106','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('128','104','5','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('131','108','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('132','112','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('134','111','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('136','113','4','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('137','115','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_regions` (`id`,`newsitem_id`,`region_id`,`created_at`,`updated_at`) VALUES ('138','116','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news_items_regions` ENABLE KEYS */;


--
-- Create Table `news_items_sectors`
--

DROP TABLE IF EXISTS `news_items_sectors`;
CREATE TABLE `news_items_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_sectors_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_sectors_sector_id_index` (`sector_id`),
  CONSTRAINT `news_items_sectors_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`),
  CONSTRAINT `news_items_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_sectors`
--

/*!40000 ALTER TABLE `news_items_sectors` DISABLE KEYS */;
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('106','105','32','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('116','107','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('119','109','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('121','106','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('122','104','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('124','108','12','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('125','112','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('127','111','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('129','113','9','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_sectors` (`id`,`newsitem_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('130','116','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news_items_sectors` ENABLE KEYS */;


--
-- Create Table `news_items_themes`
--

DROP TABLE IF EXISTS `news_items_themes`;
CREATE TABLE `news_items_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_themes_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_themes_theme_id_index` (`theme_id`),
  CONSTRAINT `news_items_themes_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`),
  CONSTRAINT `news_items_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `news_items_themes`
--

/*!40000 ALTER TABLE `news_items_themes` DISABLE KEYS */;
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('107','105','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('118','110','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('120','107','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('123','109','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('126','106','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('127','104','21','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('130','108','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('131','112','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('133','111','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('135','113','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('136','115','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `news_items_themes` (`id`,`newsitem_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('137','116','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `news_items_themes` ENABLE KEYS */;


--
-- Create Table `office_locations`
--

DROP TABLE IF EXISTS `office_locations`;
CREATE TABLE `office_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `office_locations`
--

/*!40000 ALTER TABLE `office_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `office_locations` ENABLE KEYS */;


--
-- Create Table `office_types`
--

DROP TABLE IF EXISTS `office_types`;
CREATE TABLE `office_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `office_types`
--

/*!40000 ALTER TABLE `office_types` DISABLE KEYS */;
INSERT INTO `office_types` (`id`,`name`,`created_at`,`updated_at`) VALUES ('1','Headquarter','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `office_types` (`id`,`name`,`created_at`,`updated_at`) VALUES ('2','Regional Office','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `office_types` (`id`,`name`,`created_at`,`updated_at`) VALUES ('3','Country Office','2015-05-04 09:59:42','2015-05-04 09:59:42');
/*!40000 ALTER TABLE `office_types` ENABLE KEYS */;


--
-- Create Table `organisation_types`
--

DROP TABLE IF EXISTS `organisation_types`;
CREATE TABLE `organisation_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `organisation_types`
--

/*!40000 ALTER TABLE `organisation_types` DISABLE KEYS */;
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('1','Academic & research center','Research/Innovation','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('2','Corporation','Corporation','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('3','Development consulting firm','Development Consulting Firm','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('4','Finance institution','Finance Institution','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('5','Donor & foundation','Donor & foundation','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('6','Government institution','Government Institution','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('7','Information provider','Information provider','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('8','International organisation','International Organisation','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('9','Non government organisation','Non government organisation','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('10','Small & medium enterprise','Small & medium enterprise','2015-05-04 09:59:43','2015-05-04 09:59:43');
INSERT INTO `organisation_types` (`id`,`name`,`display_name`,`created_at`,`updated_at`) VALUES ('12','Trade & union','Trade & union','2015-05-04 09:59:43','2015-05-04 09:59:43');
/*!40000 ALTER TABLE `organisation_types` ENABLE KEYS */;


--
-- Create Table `organisations`
--

DROP TABLE IF EXISTS `organisations`;
CREATE TABLE `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_id` int(10) unsigned NOT NULL,
  `org_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `org_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `office_location` int(10) unsigned DEFAULT NULL,
  `office_type` int(10) unsigned DEFAULT NULL,
  `org_type` int(10) unsigned DEFAULT NULL,
  `top_agency` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8_unicode_ci,
  `org_name_acronym` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_org_name_acronym` tinyint(1) DEFAULT '0',
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `org_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_of_activity` int(10) unsigned DEFAULT NULL,
  `cover_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  PRIMARY KEY (`id`),
  KEY `organisations_cp_id_index` (`cp_id`),
  KEY `organisations_level_of_activity_index` (`level_of_activity`),
  KEY `organisations_office_location_index` (`office_location`),
  KEY `organisations_office_type_index` (`office_type`),
  KEY `organisations_org_type_index` (`org_type`),
  CONSTRAINT `organisations_cp_id_foreign` FOREIGN KEY (`cp_id`) REFERENCES `users` (`id`),
  CONSTRAINT `organisations_level_of_activity_foreign` FOREIGN KEY (`level_of_activity`) REFERENCES `level_of_activities` (`id`),
  CONSTRAINT `organisations_office_location_foreign` FOREIGN KEY (`office_location`) REFERENCES `countries` (`id`),
  CONSTRAINT `organisations_office_type_foreign` FOREIGN KEY (`office_type`) REFERENCES `office_types` (`id`),
  CONSTRAINT `organisations_org_type_foreign` FOREIGN KEY (`org_type`) REFERENCES `organisation_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `organisations`
--

/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('54','105','/uploads/media/105/ADB2.jpg','Asian Development Bank','rogermarkowski@hotmail.com','http://adb.org','170',NULL,'9','1','1','2015-07-23 01:25:36','2015-07-23 08:25:36','The Asian Development Bank aims for an Asia and Pacific free from poverty. Approximately 1.7 billion people in the region are poor and unable to access essential goods, services, assets and opportunities to which every human is entitled. \r\n','','0','http://www.facebook.com/sharer.php?u=http%3A//www.adb.org','http://twitter.com/share?url=http%3A//www.adb.org/','+62 0000','3','/uploads/media/105/ADB.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('55','106','/uploads/media/106/Amnesty1.jpg','Amnesty International','rogermarkowski@hotmail.com','http://www.amnesty.org/en/','224',NULL,'8','1','1','2015-07-23 01:35:56','2015-07-23 08:35:56','Amnesty International is a global movement of more than 7 million people who take injustice personally. We are campaigning for a world where human rights are enjoyed by all.\r\n','','0','http://','http://','+62 000','3','/uploads/media/106/1488503684_Amnesty.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('57','108','/uploads/media/108/AUSAID1.png','Australian Aid','rogermarkowski@hotmail.com','http://dfat.gov.au','13',NULL,'7','1','1','2015-07-23 01:36:13','2015-07-23 08:36:13','The department’s purpose is to help make Australia stronger, safer and more prosperous by promoting and protecting our interests internationally and contributing to global stability and economic growth.\r\n','','0','http://','http://','+62 0000','3','/uploads/media/108/AusAid.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('58','110','/uploads/media/110/ACF.jpg','ACF International','rogermarkowski@hotmail.com','http://www.actionagainsthunger.org/about/acf-international','225',NULL,'8','1','1','2015-07-23 01:36:28','2015-07-23 08:36:28','Action Against Hunger | ACF International, a global humanitarian organization committed to ending world hunger, works to save the lives of malnourished children while providing communities with access to safe water and sustainable solutions to hunger. ','','0','http://','http://','000','3','/uploads/media/110/AAH.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('59','111','/uploads/media/111/CARE.gif','Care International','rogermarkowski@hotmail.com','http://www.care-international.org/','206',NULL,'8','1','1','2015-07-23 01:36:43','2015-07-23 08:36:43','CARE is a leading humanitarian organization fighting global poverty. We place special focus on working alongside poor women because, equipped with the proper resources, women have the power to help whole families and entire communities escape poverty. Women are at the heart of our community-based efforts to improve basic education, prevent the spread of disease, increase access to clean water and sanitation, expand economic opportunity and protect natural resources. We also deliver emergency aid to survivors of war and natural disasters, and help people rebuild their lives.\r\n','','0','http://','http://','000','3','/uploads/media/111/CARE.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('60','112','/uploads/media/112/Caritas.jpg','Caritas Internationalis','rogermarkowski@hotmail.com','http://caritas.org','107',NULL,'8','1','1','2015-07-23 01:37:06','2015-07-23 08:37:06','Caritas shares the mission of the Catholic Church to serve the poor and to promote charity and justice throughout the world. \r\n','','0','http://','http://','000','3','/uploads/media/112/Caritas2.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('61','113','/uploads/media/113/DFID.jpeg','The Department for International Development ','rogermarkowski@hotmail.com','http://gov.uk/government/organisations/department-for-international-development','224',NULL,'7','1','1','2015-07-23 01:37:18','2015-07-23 08:37:18','The Department for International Development (DFID) leads the UK’s work to end extreme poverty. We\'re ending the need for aid by creating jobs, unlocking the potential of girls and women and helping to save lives when humanitarian emergencies hit.\r\n','DFID','1','http://','http://','000','3','/uploads/media/113/DFID.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('62','114','/uploads/media/114/FAO.gif','Food and Agriculture Organization of the United Nations','rogermarkowski@hotmail.com','http://www.fao.org/home/en','107',NULL,'9','1','1','2015-07-23 01:37:34','2015-07-23 08:37:34','Achieving food security for all is at the heart of FAO\'s efforts – to make sure people have regular access to enough high-quality food to lead active, healthy lives.  \r\nOur three main goals are: the eradication of hunger, food insecurity and malnutrition; the elimination of poverty and the driving forward of economic and social progress for all; and, the sustainable management and utilization of natural resources, including land, water, air, climate and genetic resources for the benefit of present and future generations.\r\n','FAO','1','http://','http://','000','3','/uploads/media/114/FAO.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('63','115','/uploads/media/115/foreign-affairs-trade-canada.jpg','Foreign Affairs, Trade and Development Canada','rogermarkowski@hotmail.com','http://www.international.gc.ca/international/index.aspx?lang=eng','38',NULL,'7','1','1','2015-07-24 05:11:11','2015-07-24 12:11:11','The mandate of Foreign Affairs, Trade and Development Canada is to manage Canada\'s diplomatic and consular relations, to encourage the country\'s international trade and to lead Canada’s international development and humanitarian assistance.\r\n','FATDC','1','http://','http://','000','2','/uploads/media/115/FATDC.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('64','116','/uploads/media/116/1828527350_Greenpeace.jpg','Greenpeace','rogermarkowski@hotmail.com','http://www.greenpeace.org/international/en/','152',NULL,'8','1','1','2015-08-06 06:11:36','2015-08-06 13:11:36','Greenpeace exists because this fragile earth deserves a voice. It needs solutions. It needs change. It needs action.\r\nGreenpeace is an independent global campaigning organisation that acts to change attitudes and behaviour, to protect and conserve the environment and to promote peace.\r\n','','0','http://','http://','000','3','/uploads/media/116/greenpeace.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('65','117','/uploads/media/117/ICRC3.jpg','International Committee of the Red Cross','rogermarkowski@hotmail.com','http://www.icrc.org/en','206',NULL,'9','1','1','2015-07-23 01:41:01','2015-07-23 08:41:01','The International Red Cross and Red Crescent Movement is the largest humanitarian network in the world. Its mission is to alleviate human suffering, protect life and health, and uphold human dignity especially during armed conflicts and other emergencies. It is present in every country and supported by millions of volunteers.\r\n','ICRC','1','http://','http://','000','3','/uploads/media/117/ICRC CI.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('66','118','/uploads/media/118/MDM.JPG','Doctors of the World','rogermarkowski@hotmail.com','http://doctorsoftheworld.org/international-network/','225',NULL,'8','1','1','2015-08-06 16:56:50','2015-08-06 23:56:50','Doctors of the World is an international humanitarian organization that provides emergency and long-term medical care to vulnerable populations while fighting for equal access to healthcare worldwide.','','0','http://','http://','000','3','/uploads/media/118/1442009291_MDM.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('67','119','/uploads/media/119/MSF.jpg','Medecins Sans Frontieres International','rogermarkowski@hotmail.com','http://msf.org','206',NULL,'8','1','1','2015-08-06 16:51:08','2015-08-06 23:51:08','Médecins Sans Frontières (MSF) is an international, independent, medical humanitarian organisation that delivers emergency aid to people affected by armed conflict, epidemics, natural disasters and exclusion from healthcare. MSF offers assistance to people based on need, irrespective of race, religion, gender or political affiliation.\r\n','MSF International','1','http://','http://','000','3','/uploads/media/119/MSF.gif');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('68','120','/uploads/media/120/IOM.png','International Organization for Migration','rogermarkowski@hotmail.com','http://www.iom.int/cms/home','206',NULL,'9','1','1','2015-07-23 01:41:46','2015-07-23 08:41:46','IOM is committed to the principle that humane and orderly migration benefits migrants and society.\r\nAs the leading international organization for migration, IOM acts with its partners in the international community to:\r\nAssist in meeting the growing operational challenges of migration management. \r\nAdvance understanding of migration issues. \r\nEncourage social and economic development through migration. \r\nUphold the human dignity and well-being of migrants.','IOM','0','http://','http://','000','3','/uploads/media/120/iom.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('69','121','/uploads/media/121/Oxfam.jpg','Oxfam International','rogermarkowski@hotmail.com','http://oxfam.org','224',NULL,'8','1','1','2015-07-23 01:42:20','2015-07-23 08:42:20','Our vision is a just world without poverty. We want a world where people are valued and treated equally, enjoy their rights as full citizens, and can influence decisions affecting their lives.\r\n\r\nOur purpose is to help create lasting solutions to the injustice of poverty. We are part of a global movement for change, empowering people to create a future that is secure, just, and free from poverty.\r\n','','0','http://','http://','000','3','/uploads/media/121/Oxfam Intternational.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('70','122','/uploads/media/122/PADF.jpg','Pan American Development Foundation','rogermarkowski@hotmail.com','http://padf.org','225',NULL,'8','1','1','2015-07-23 01:43:08','2015-07-23 08:43:08','The Mission of the Pan American Development Foundation is to assist vulnerable and excluded people and communities in the Americas to achieve sustainable economic and social progress, strengthen their communities and civil society, promote democratic participation and inclusion, and prepare for and respond to natural disasters and other humanitarian crises, there by advancing the principles of the Organization of American States and creating a Hemisphere of Opportunity for All.\r\n','PADF','0','http://','http://','000','3','/uploads/media/122/PADF 2.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('71','123','/uploads/media/123/622631929_PLAN.jpg','Plan International','rogermarkowski@hotmail.com','http://plan-international.org/','225',NULL,'8','1','1','2015-08-06 06:13:43','2015-08-06 13:13:43','Plan aims to achieve lasting improvements in the quality of life of deprived children in developing countries, through a process that unites people across cultures and adds meaning and value to their lives.\r\n','','0','http://','http://','000','3','/uploads/media/123/Plan.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('72','124','/uploads/media/124/UNDP.jpg','United Nations Development Programme','rogermarkowski@hotmail.com','http://undp.org','225',NULL,'9','1','1','2015-07-23 01:43:39','2015-07-23 08:43:39','UNDP works in more than 170 countries and territories, helping to achieve the eradication of poverty, and the reduction of inequalities and exclusion. We help countries to develop policies, leadership skills, partnering abilities, institutional capabilities and build resilience in order to sustain development results. \r\n','UNDP','1','https://www.facebook.com/UNDP','https://twitter.com/undp','000','3','/uploads/media/124/Cover image - UNDP.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('73','125','/uploads/media/125/UNICEF.jpg','United Nations Children\'s Fund','rogermarkowski@hotmail.com','http://unicef.org','225',NULL,'9','1','1','2015-07-23 01:44:09','2015-07-23 08:44:09','UNICEF is mandated by the United Nations General Assembly to advocate for the protection of children\'s rights, to help meet their basic needs and to expand their opportunities to reach their full potential. \r\n','UNICEF','1','http://','http://','000','3','/uploads/media/125/Cover image - UNICEF.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('74','126','/uploads/media/126/Unilever.jpeg','Unilever','rogermarkowski@hotmail.com','http://unilever.com','224',NULL,'3','1','1','2015-07-23 01:38:23','2015-07-23 08:38:23','With more than 400 brands focused on health and wellbeing, no company touches so many people’s lives in so many different ways.\r\n','','0','http://','http://','000','3','/uploads/media/126/Unilever.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('75','127','/uploads/media/127/USAID_1.jpg','USAID','rogermarkowski@hotmail.com','http://usaid.gov','225',NULL,'7','1','1','2015-07-23 01:38:44','2015-07-23 08:38:44','USAID is the lead U.S. Government agency that works to end extreme global poverty and enable resilient, democratic societies to realize their potential.\r\n','','0','http://','http://','000','3','/uploads/media/127/USAID.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('76','128','/uploads/media/128/WB3.jpg','World Bank','rogermarkowski@hotmail.com','http://worldbank.org','225',NULL,'9','1','1','2015-07-23 01:39:16','2015-07-23 08:39:16','The World Bank Group has two ambitious goals:\r\nEnd extreme poverty within a generation and boost shared prosperity.\r\n','','0','http://','http://','000','3','/uploads/media/128/World Bank.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('77','129','/uploads/media/129/WHO.png','World Health Organization','rogermarkowski@hotmail.com','http://who.int','206',NULL,'9','1','1','2015-07-23 01:41:37','2015-07-23 08:41:37','WHO is the directing and coordinating authority for health within the United Nations system. It is responsible for providing leadership on global health matters, shaping the health research agenda, setting norms and standards, articulating evidence-based policy options, providing technical support to countries and monitoring and assessing health trends.\r\n','','0','http://','http://','000','3','/uploads/media/129/who.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('78','130','/uploads/media/130/WWF.png','World Wild Fund ','rogermarkowski@hotmail.com','http://pand.org','206',NULL,'8','1','1','2015-07-23 01:42:05','2015-07-23 08:42:05','WWF was born into this world in 1961. It was the product of a deep concern held by a few eminent gentlemen who were worried by what they saw happening in our world at that time. Since those early days WWF has grown up to be one of the largest environmental organizations in the world. \r\n','WWF','0','http://','http://','000','3','/uploads/media/130/WWF.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('79','133','/uploads/media/133/WTO1.jpg','World Trade Organization','rogermarkowski@hotmail.com','http://wto.org','206',NULL,'9','1','1','2015-07-23 01:42:40','2015-07-23 08:42:40','The World Trade Organization (WTO) deals with the global rules of trade between nations. Its main function is to ensure that trade flows as smoothly, predictably and freely as possible.\r\n','','0','http://','http://','000','3','/uploads/media/133/wto.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('80','136','/uploads/media/136/102.png','Fleava','info@fleava.com','http://fleava.com','102',NULL,'3','0','0','2015-07-23 01:43:06','2015-07-23 08:43:06','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulsi recurrant? Confecta res esset. Quod vestri non item. Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Duo Reges: constructio interrete. Et quidem, inquit, vehementer errat;\r\n','','0','','','123','1','/images/noimage2.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('81','138','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-06-25 23:31:28','2015-05-25 11:59:08',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('82','140','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-06-28 21:12:30','2015-06-28 21:12:30',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('83','142','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-06-28 22:48:13','2015-06-28 22:48:13',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('84','146','/uploads/media/146/World Vision2.jpg','World Vision International','rogermarkowski@hotmail.com','http://wvi.org','225',NULL,'8','0','1','2015-07-23 01:45:01','2015-07-23 08:45:01','We have a World Vision for children - where every child grows up healthy and strong, is cared for and protected and has opportunities to flourish.\r\n\r\nWe have a World Vision for change- both on a global scale  and in the lives of individuals through powerful one-to-one connections.\r\n\r\nWe have a World Vision for life– where we care for each other in a world full of promise and free of poverty.\r\n','','0','','','0000','3','/uploads/media/146/World Vision CP.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('85','147','/uploads/media/147/671401978_Amnesty.jpg','ORG Bidon','rogermarkowski@hotmail.com','','2',NULL,'5','0','0','2015-07-23 01:44:58','2015-07-23 08:44:58','bla bla bla','','0','','','0000','3','/uploads/media/147/224628468_Amnesty.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('86','148','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-07-09 08:58:34','2015-07-09 08:58:34',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('87','154','/uploads/media/154/126778336_greenpeace.jpg','ORGTEST','rogermarkowski@hotmail.com','','85',NULL,'12','0','0','2015-07-23 01:45:25','2015-07-23 08:45:25','ORGTEST text of presentation','','0','','','000','2','/uploads/media/154/1915585666_FATDC.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('88','156','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-07-11 05:49:17','2015-07-11 05:49:17',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('89','157','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-07-11 05:52:26','2015-07-11 05:52:26',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('90','158','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-07-11 05:53:30','2015-07-11 05:53:30',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('91','159','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-07-11 07:57:00','2015-07-11 07:57:00',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('92','160','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-07-11 23:30:29','2015-07-11 23:30:29',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('93','165','/uploads/media/165/1481135932_AER.jpg','Action for Economic Reforms','rogermarkow@gmail.com','http://aer.ph/','170',NULL,'8','0','1','2015-07-23 01:47:11','2015-07-23 08:47:11','AER provides timely and sharp policy positions based on a solid grasp of economic, legal and political tools of analysis. Complex economic and governance issues are scrutinized with analytical, technical and political sophistication, carefully handling finer points such as timing, sequencing, policy mix, and tradeoffs.\r\n\r\nAER’s independence and rigor enable it to credibly engage the various branches of government, social movements, academe, media, interest groups and international institutions. The organization employs a variety of forms to influence or change policy—research and analysis, lobby and dialogue, networking and coalition building, legal action, media work, publication, and public education.','AER','1','','','0000','2','/uploads/media/165/2561103_AER2.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('94','166','/uploads/media/166/497266846_Ayala-Foundation.jpg','Ayala Foundation','rogermarkow@gmail.com','http://www.ayalafoundation.org/','170',NULL,'8','0','1','2015-07-23 02:31:26','2015-07-23 09:31:26','AFI envisions communities where people are productive, creative, self-reliant, and proud to be Filipino. As a believer in creating shared value and inclusive business, it has four key program areas—Education, Youth Leadership, Sustainable Livelihood, and Arts and Culture. AFI is a member of the Ayala group of companies.','','0','','','0000','2','/uploads/media/166/1212287755_Ayala.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('95','167','/uploads/media/167/10573753_Asian Farmers.jpg','Asian Farmers\' Association For Sustainable Rural Development','rogermarkow@gmail.com','http://asianfarmers.org/','170',NULL,'8','0','1','2015-08-14 01:21:27','2015-08-14 08:21:27','We are a regional alliance of 17 national federations and organizations of small scale women and men farmers and producers from 13 countries in Asia.','AFA','0','','','000','2','/uploads/media/167/900612753_Asian farmers.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('96','169','/uploads/media/169/545661049_Asian Farmers.jpg','ddd','rogermarkow@gmail.com','','15',NULL,'4','0','0','2015-08-06 06:29:19','2015-08-06 13:29:19','vvv','','0','','','000','1','/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('97','171','/uploads/media/171/453156944_aidpost - connect.png','ORG TEST NAME','rogermarkowski@hotmail.com','','225',NULL,'1','0','0','2015-08-14 03:25:12','2015-08-14 10:25:12','Text of presentation test','cronyme of ORGtest','1','','','000','1','/uploads/media/171/38479549_Aidpost headline image JPEG.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('98','173','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-08-12 23:42:38','2015-08-12 23:42:38',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('99','174','/images/noimage.png','','','',NULL,NULL,NULL,'0','1','2015-08-13 00:31:51','2015-08-13 00:31:51',NULL,NULL,'0',NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('100','175','/uploads/media/175/2012325652_Oxfam.jpg','ORGTEST40','rogermarkowski@hotmail.com','','38',NULL,'1','0','0','2015-08-14 03:25:00','2015-08-14 10:25:00','About our org','','0','','','000','3','/uploads/media/175/1899180159_iom.jpg');
INSERT INTO `organisations` (`id`,`cp_id`,`org_logo`,`org_name`,`org_email`,`org_website`,`office_location`,`office_type`,`org_type`,`top_agency`,`is_active`,`created_at`,`updated_at`,`description`,`org_name_acronym`,`use_org_name_acronym`,`facebook_url`,`twitter_url`,`org_phone`,`level_of_activity`,`cover_image`) VALUES ('101','177','/uploads/media/177/1244925169_20141221_2018213.jpg','Organisation Name','phamvanhieu861992@gmail.com','http://organisation.com','231',NULL,'1','0','1','2015-08-14 18:33:11','2015-08-15 01:33:11','This short text will be displayed on your profile page that visitors can access by clicking on your logo that accompanies all your postings.','NF','1','','','84975425675','1','/images/noimage.png');
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;


--
-- Create Table `password_reminders`
--

DROP TABLE IF EXISTS `password_reminders`;
CREATE TABLE `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pepper` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_username_index` (`username`),
  KEY `password_reminders_token_index` (`token`),
  KEY `password_reminders_pepper_index` (`pepper`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `password_reminders`
--

/*!40000 ALTER TABLE `password_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reminders` ENABLE KEYS */;


--
-- Create Table `platforms`
--

DROP TABLE IF EXISTS `platforms`;
CREATE TABLE `platforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `related_country_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `platforms`
--

/*!40000 ALTER TABLE `platforms` DISABLE KEYS */;
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('1','International','International','0','1','2015-05-18 02:45:23','2015-05-18 08:59:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('2','Indonesia','Indonesia','102','1','2015-05-29 12:58:00','2015-05-29 12:58:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('3','Djibouti','Djibouti','59','0','2015-07-16 17:09:07','2015-07-10 02:02:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('4','Canada','Canada','38','1','2015-07-16 17:11:30','2015-07-11 06:01:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('5','Ghana','Ghana','83','1','2015-07-11 06:09:00','2015-07-11 06:09:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('6','Philippines','Philippines','170','1','2015-07-11 06:10:00','2015-07-11 06:10:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('7','Tanzania, United Republic of','Tanzania, United Republic of','210','1','2015-07-11 06:10:00','2015-07-11 06:10:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('8','United Kingdom','United Kingdom','224','1','2015-07-11 06:11:00','2015-07-11 06:11:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('9','Australia','Australia','13','1','2015-07-11 06:11:00','2015-07-11 06:11:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('10','Greece','Greece','85','0','2015-07-21 23:00:11','2015-07-18 06:02:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('11','Greece','Greece','85','1','2015-07-23 08:21:00','2015-07-23 08:21:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('12','United States','United States','225','1','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('13','Switzerland','Switzerland','206','1','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('14','Italy','Italy','107','1','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('15','Netherlands','Netherlands','152','1','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('16','Albania','Albania','2','0','2015-08-11 17:38:28','2015-07-23 08:44:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('17','Sweden','Sweden','205','1','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('18','Djibouti','Djibouti','59','1','2015-07-23 09:42:00','2015-07-23 09:42:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('19','Belarus','Belarus','20','0','2015-08-11 17:39:17','2015-07-24 22:38:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('20','Azerbaijan','Azerbaijan','15','1','2015-07-24 22:43:00','2015-07-24 22:43:00');
INSERT INTO `platforms` (`id`,`name`,`display_text`,`related_country_id`,`active`,`created_at`,`updated_at`) VALUES ('21','Vietnam','Vietnam','231','1','2015-08-15 01:26:00','2015-08-15 01:26:00');
/*!40000 ALTER TABLE `platforms` ENABLE KEYS */;


--
-- Create Table `platforms_profiles`
--

DROP TABLE IF EXISTS `platforms_profiles`;
CREATE TABLE `platforms_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `profile_type` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `platforms_profiles`
--

/*!40000 ALTER TABLE `platforms_profiles` DISABLE KEYS */;
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('26','134','1','1','2015-05-22 03:11:05','0000-00-00 00:00:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('208','54','2','1','2015-07-23 08:25:00','2015-07-23 08:25:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('209','54','2','6','2015-07-23 08:25:00','2015-07-23 08:25:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('210','55','2','1','2015-07-23 08:35:00','2015-07-23 08:35:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('211','55','2','8','2015-07-23 08:35:00','2015-07-23 08:35:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('212','57','2','1','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('213','57','2','9','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('214','58','2','1','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('215','58','2','12','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('216','59','2','1','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('217','59','2','13','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('218','60','2','1','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('219','60','2','14','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('220','61','2','1','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('221','61','2','8','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('222','62','2','1','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('223','62','2','14','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('226','74','2','1','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('227','74','2','8','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('228','75','2','1','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('229','75','2','12','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('232','76','2','1','2015-07-23 08:39:00','2015-07-23 08:39:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('233','76','2','12','2015-07-23 08:39:00','2015-07-23 08:39:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('238','65','2','1','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('239','65','2','13','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('242','77','2','1','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('243','77','2','13','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('244','68','2','1','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('245','68','2','13','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('246','78','2','1','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('247','78','2','13','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('248','69','2','1','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('249','69','2','8','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('250','79','2','1','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('251','79','2','13','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('252','80','2','2','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('253','70','2','1','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('254','70','2','12','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('257','72','2','1','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('258','72','2','12','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('259','73','2','1','2015-07-23 08:44:00','2015-07-23 08:44:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('260','73','2','12','2015-07-23 08:44:00','2015-07-23 08:44:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('261','85','2','1','2015-07-23 08:44:00','2015-07-23 08:44:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('262','85','2','16','2015-07-23 08:44:00','2015-07-23 08:44:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('263','84','2','1','2015-07-23 08:45:00','2015-07-23 08:45:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('264','84','2','12','2015-07-23 08:45:00','2015-07-23 08:45:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('265','87','2','11','2015-07-23 08:45:00','2015-07-23 08:45:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('266','93','2','6','2015-07-23 08:47:00','2015-07-23 08:47:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('267','94','2','6','2015-07-23 09:31:00','2015-07-23 09:31:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('269','109','1','1','2015-07-23 09:38:00','2015-07-23 09:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('270','109','1','6','2015-07-23 09:38:00','2015-07-23 09:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('271','131','1','1','2015-07-23 09:39:00','2015-07-23 09:39:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('272','131','1','12','2015-07-23 09:39:00','2015-07-23 09:39:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('273','132','1','1','2015-07-23 09:39:00','2015-07-23 09:39:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('274','132','1','12','2015-07-23 09:39:00','2015-07-23 09:39:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('277','145','1','1','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('278','145','1','8','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('279','150','1','1','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('280','150','1','13','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('281','151','1','1','2015-07-23 09:42:00','2015-07-23 09:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('282','151','1','18','2015-07-23 09:42:00','2015-07-23 09:42:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('283','152','1','1','2015-07-23 09:43:00','2015-07-23 09:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('284','152','1','4','2015-07-23 09:43:00','2015-07-23 09:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('285','153','1','1','2015-07-23 09:43:00','2015-07-23 09:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('286','153','1','8','2015-07-23 09:43:00','2015-07-23 09:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('293','95','2','6','2015-07-24 03:03:00','2015-07-24 03:03:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('301','135','1','12','2015-07-24 12:09:00','2015-07-24 12:09:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('302','63','2','4','2015-07-24 12:11:00','2015-07-24 12:11:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('303','168','1','19','2015-07-24 22:38:00','2015-07-24 22:38:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('304','96','2','20','2015-07-24 22:43:00','2015-07-24 22:43:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('305','64','2','1','2015-08-06 13:11:00','2015-08-06 13:11:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('306','64','2','15','2015-08-06 13:11:00','2015-08-06 13:11:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('307','71','2','1','2015-08-06 13:13:00','2015-08-06 13:13:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('308','71','2','12','2015-08-06 13:13:00','2015-08-06 13:13:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('309','67','2','1','2015-08-06 23:51:00','2015-08-06 23:51:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('310','67','2','13','2015-08-06 23:51:00','2015-08-06 23:51:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('311','66','2','1','2015-08-06 23:56:00','2015-08-06 23:56:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('312','66','2','12','2015-08-06 23:56:00','2015-08-06 23:56:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('315','170','1','4','2015-08-11 23:30:00','2015-08-11 23:30:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('316','97','2','12','2015-08-11 23:49:00','2015-08-11 23:49:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('317','172','1','4','2015-08-12 23:48:00','2015-08-12 23:48:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('318','100','2','1','2015-08-13 00:59:00','2015-08-13 00:59:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('319','100','2','4','2015-08-13 00:59:00','2015-08-13 00:59:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('320','176','1','21','2015-08-15 01:26:00','2015-08-15 01:26:00');
INSERT INTO `platforms_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('321','101','2','21','2015-08-15 01:33:00','2015-08-15 01:33:00');
/*!40000 ALTER TABLE `platforms_profiles` ENABLE KEYS */;


--
-- Create Table `platforms_top_profiles`
--

DROP TABLE IF EXISTS `platforms_top_profiles`;
CREATE TABLE `platforms_top_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `profile_type` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `platforms_top_profiles`
--

/*!40000 ALTER TABLE `platforms_top_profiles` DISABLE KEYS */;
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('1','54','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('2','55','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('3','57','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('4','58','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('5','59','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('6','60','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('7','61','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('8','62','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('10','64','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('11','65','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('12','66','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('13','67','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('14','68','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('15','69','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('16','70','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('17','71','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('18','72','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('19','73','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('20','74','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('21','75','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('22','76','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('23','77','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('24','78','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('25','79','2','1','2015-05-21 18:46:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('26','134','1','1','2015-05-22 03:11:05','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('27','131','1','1','2015-05-22 03:12:04','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('29','132','1','1','2015-05-22 03:12:09','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('30','109','1','1','2015-05-22 03:12:30','0000-00-00 00:00:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('31','145','1','1','2015-07-04 13:48:00','2015-07-04 13:48:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('32','85','2','1','2015-07-09 08:47:00','2015-07-09 08:47:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('33','150','1','1','2015-07-10 01:17:00','2015-07-10 01:17:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('34','151','1','3','2015-07-10 02:02:00','2015-07-10 02:02:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('35','152','1','1','2015-07-10 02:49:00','2015-07-10 02:49:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('36','153','1','1','2015-07-10 05:27:00','2015-07-10 05:27:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('38','151','1','1','2015-07-11 03:03:00','2015-07-11 03:03:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('40','93','2','6','2015-07-23 02:12:00','2015-07-23 02:12:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('41','93','2','1','2015-07-23 02:26:00','2015-07-23 02:26:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('42','94','2','6','2015-07-23 04:32:00','2015-07-23 04:32:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('43','95','2','6','2015-07-23 06:30:00','2015-07-23 06:30:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('44','87','2','11','2015-07-23 08:21:00','2015-07-23 08:21:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('45','54','2','6','2015-07-23 08:25:00','2015-07-23 08:25:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('46','55','2','8','2015-07-23 08:35:00','2015-07-23 08:35:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('47','57','2','9','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('48','58','2','12','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('49','59','2','13','2015-07-23 08:36:00','2015-07-23 08:36:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('50','60','2','14','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('51','61','2','8','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('52','62','2','14','2015-07-23 08:37:00','2015-07-23 08:37:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('53','74','2','8','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('54','75','2','12','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('55','64','2','15','2015-07-23 08:38:00','2015-07-23 08:38:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('56','76','2','12','2015-07-23 08:39:00','2015-07-23 08:39:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('57','66','2','12','2015-07-23 08:39:00','2015-07-23 08:39:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('58','77','2','13','2015-07-23 08:39:00','2015-07-23 08:39:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('59','65','2','13','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('60','67','2','13','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('61','68','2','13','2015-07-23 08:41:00','2015-07-23 08:41:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('62','78','2','13','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('63','69','2','8','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('64','79','2','13','2015-07-23 08:42:00','2015-07-23 08:42:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('65','80','2','2','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('66','70','2','12','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('67','71','2','12','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('68','72','2','12','2015-07-23 08:43:00','2015-07-23 08:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('69','73','2','12','2015-07-23 08:44:00','2015-07-23 08:44:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('70','85','2','16','2015-07-23 08:44:00','2015-07-23 08:44:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('71','84','2','1','2015-07-23 08:45:00','2015-07-23 08:45:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('72','84','2','12','2015-07-23 08:45:00','2015-07-23 08:45:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('73','109','1','6','2015-07-23 09:38:00','2015-07-23 09:38:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('74','131','1','12','2015-07-23 09:39:00','2015-07-23 09:39:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('75','132','1','12','2015-07-23 09:39:00','2015-07-23 09:39:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('77','145','1','8','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('78','150','1','13','2015-07-23 09:41:00','2015-07-23 09:41:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('79','151','1','18','2015-07-23 09:42:00','2015-07-23 09:42:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('80','152','1','4','2015-07-23 09:43:00','2015-07-23 09:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('81','153','1','8','2015-07-23 09:43:00','2015-07-23 09:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('87','168','1','19','2015-07-24 22:38:00','2015-07-24 22:38:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('88','96','2','20','2015-07-24 22:43:00','2015-07-24 22:43:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('89','170','1','1','2015-08-11 23:15:00','2015-08-11 23:15:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('90','170','1','4','2015-08-11 23:15:00','2015-08-11 23:15:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('91','97','2','12','2015-08-11 23:49:00','2015-08-11 23:49:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('92','172','1','4','2015-08-12 23:48:00','2015-08-12 23:48:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('93','100','2','1','2015-08-13 00:59:00','2015-08-13 00:59:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('94','100','2','4','2015-08-13 00:59:00','2015-08-13 00:59:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('95','176','1','21','2015-08-15 01:26:00','2015-08-15 01:26:00');
INSERT INTO `platforms_top_profiles` (`id`,`profile_id`,`profile_type`,`platform_id`,`created_at`,`updated_at`) VALUES ('96','101','2','21','2015-08-15 01:33:00','2015-08-15 01:33:00');
/*!40000 ALTER TABLE `platforms_top_profiles` ENABLE KEYS */;


--
-- Create Table `professional_statuses`
--

DROP TABLE IF EXISTS `professional_statuses`;
CREATE TABLE `professional_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `professional_statuses`
--

/*!40000 ALTER TABLE `professional_statuses` DISABLE KEYS */;
INSERT INTO `professional_statuses` (`id`,`name`,`created_at`,`updated_at`) VALUES ('1','Student','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `professional_statuses` (`id`,`name`,`created_at`,`updated_at`) VALUES ('2','Active Professionally','2015-05-04 09:59:42','2015-05-04 09:59:42');
INSERT INTO `professional_statuses` (`id`,`name`,`created_at`,`updated_at`) VALUES ('3','Retired','2015-05-04 09:59:42','2015-05-04 09:59:42');
/*!40000 ALTER TABLE `professional_statuses` ENABLE KEYS */;


--
-- Create Table `publicities`
--

DROP TABLE IF EXISTS `publicities`;
CREATE TABLE `publicities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `a_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_title_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_text` text COLLATE utf8_unicode_ci,
  `a_text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_button_text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noimage.png',
  `a_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noimage.png',
  `b_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noimage.png',
  `c_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `a_show_org_logo` tinyint(1) NOT NULL DEFAULT '1',
  `published_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published_interval` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P0Y7M0DT0H0M0S',
  `published_interval_in_seconds` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `publicities_organisation_id_index` (`organisation_id`),
  KEY `publicities_type_id_index` (`type_id`),
  KEY `publicities_user_id_index` (`user_id`),
  KEY `publicities_user_profile_id_index` (`user_profile_id`),
  CONSTRAINT `publicities_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `publicities_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types_of_publicities` (`id`),
  CONSTRAINT `publicities_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `publicities_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `publicities`
--

/*!40000 ALTER TABLE `publicities` DISABLE KEYS */;
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('51','Adopt a Polar Bear','#222222','As their ice habitat shrinks, skinnier and hungrier bears face a grave challenge to their survival.','#222222','Learn More','#222222','LeftTop','/uploads/media/130/publicities/WWF.png','http://gifts.worldwildlife.org/gift-center/gifts/Species-Adoptions/Polar-Bear.aspx?sc=AWY1302WC922&_ga=1.150197874.599198513.1425506468','','/images/noimage.png','http://','','/images/noimage.png','http://','1','1','0','0','2015-06-24 08:53:48','2015-05-19 23:12:09','78','130','130','Adopt a Polar Bear',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('52','','#222222','','#222222','','#222222','LeftMiddle','/images/noimage.png','http://','Donate','/uploads/media/119/publicities/banner_2.jpg','http://www.msf.org/donate','','/images/noimage.png','http://','2','1','0','0','2015-06-24 08:53:48','2015-05-19 23:49:48','67','119','119','Donate',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('53','','#222222','','#222222','','#222222','LeftMiddle','/images/noimage.png','http://','','/images/noimage.png','http://','Byke for CARE','/uploads/media/111/publicities/CARE UK_Bike Ride.JPG','http://www.care-international.org/take-action.aspx','3','1','0','0','2015-06-24 08:53:48','2015-05-19 23:56:21','59','111','111','Byke for CARE',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('54','South Sudan: new fighting will bring new suffering','#ffffff','Your donation can help deliver urgently needed food and supplies in South Sudan.','#ffffff','Send emergency aid','#222222','LeftMiddle','/uploads/media/117/publicities/ICRC- south-sudan.jpg','http://www.icrc.org/eng/donations/?o=420016','','/images/noimage.png','http://','','/images/noimage.png','http://','1','1','0','0','2015-07-04 19:21:31','2015-07-05 02:21:31','65','117','117','South Sudan','2015-06-06 00:45:05','1','2015-07-10 00:00:00','P0Y1M0DT0H0M0S','2592000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('55','Earthquake Forces Nepal’s Overseas Workers to Face Tough Choices','#ffffff','about how to respond to the April 25 earthquake that could have profound and immediate impacts on household and national economies','#ffffff','How you can help','#222222','LeftBottom','/uploads/media/120/publicities/OIM NepalResponse.jpg','http://https://www.kintera.org/site/c.dnJOKRNkFiG/b.837337/k.F96D/IOM__Make_a_Donation/apps/ka/sd/donor.asp?c=dnJOKRNkFiG&b=837337&en=6pJBKLMnG5JKIVMqF4KHIRMALpJOJPOyGhKPK3MyHaLMKTPAKtE','','/images/noimage.png','http://','','/images/noimage.png','http://','1','1','0','0','2015-06-24 08:53:48','2015-05-21 01:03:19','68','120','120','Earthquake Nepal',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('56','','#222222','','#222222','','#222222','LeftMiddle','/images/noimage.png','http://','Raise your voice ','/uploads/media/126/publicities/Unilever.jpg','http://https://brightfuture.unilever.com/petitions/423601/take-climate-action-now.aspx','','/images/noimage.png','http://','2','0','0','0','2015-07-05 16:24:42','2015-05-20 10:38:42','74','126','126','Raise your voice',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('57','','#222222','','#222222','','#222222','LeftMiddle','/images/noimage.png','http://','.','/uploads/media/126/publicities/Unilever - energy.jpg','http://https://brightfuture.unilever.com/stories/423955/THE-CHUNKINATOR--Turning-ice-cream-into-energy.aspx','','/images/noimage.png','http://','2','0','0','0','2015-07-05 16:24:35','2015-05-20 10:46:49','74','126','126','Meet the chunkinator',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('58','','#222222','','#222222','','#222222','LeftMiddle','/images/noimage.png','http://','','/images/noimage.png','http://','Publication','/uploads/media/128/publicities/World Bank Publication.jpg','http://https://openknowledge.worldbank.org/handle/10986/16608','3','1','0','0','2015-06-24 08:53:48','2015-05-20 10:54:42','76','128','128','Publication',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('59','Adopt a Polar Bear','#222222','Bears face a grave challenge to their survival','#222222','Learn More','#222222','RightBottom','/uploads/media/120/publicities/WWF.png','http://aidweb.info/#','','/images/noimage.png','http://','','/images/noimage.png','http://','1','0','0','0','2015-06-24 08:53:48','2015-05-21 09:51:43','68','120','120','Adopt a Polar Bear',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
INSERT INTO `publicities` (`id`,`a_title`,`a_title_color`,`a_text`,`a_text_color`,`a_button_text`,`a_button_text_color`,`a_position`,`a_image`,`a_url`,`b_title`,`b_image`,`b_url`,`c_title`,`c_image`,`c_url`,`type_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`name`,`published_at`,`a_show_org_logo`,`published_start_date`,`published_interval`,`published_interval_in_seconds`) VALUES ('60','Adopt a Polar Bear','#222222','Bears face a grave challenge to their survival','#222222','Learn More','#222222','RightBottom','/uploads/media/120/publicities/WWF.png','http://aidweb.info/#','','/images/noimage.png','http://','','/images/noimage.png','http://','1','0','0','0','2015-06-24 08:53:48','2015-05-21 09:52:09','68','120','120','Adopt a Polar Bear',NULL,'1','2015-06-24 00:00:00','P0Y7M0DT0H0M0S','18144000');
/*!40000 ALTER TABLE `publicities` ENABLE KEYS */;


--
-- Create Table `publicities_platforms`
--

DROP TABLE IF EXISTS `publicities_platforms`;
CREATE TABLE `publicities_platforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publicity_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `publicities_platforms`
--

/*!40000 ALTER TABLE `publicities_platforms` DISABLE KEYS */;
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('103','51','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('104','52','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('105','53','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('107','55','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('108','56','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('109','57','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('110','58','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('111','59','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('112','60','0','2015-06-24 01:47:19','0000-00-00 00:00:00');
INSERT INTO `publicities_platforms` (`id`,`publicity_id`,`platform_id`,`created_at`,`updated_at`) VALUES ('113','54','0','2015-07-04 19:21:31','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `publicities_platforms` ENABLE KEYS */;


--
-- Create Table `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE `regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `regions`
--

/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Africa','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Asia-Oceania','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Europe','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Latin America','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Middle East','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('6','North America','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
INSERT INTO `regions` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('7','World','0','2015-05-04 09:59:28','2015-05-04 09:59:28');
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;


--
-- Create Table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
CREATE TABLE `registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `registrations`
--

/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('1','102','$2y$10$r4Df/ZFe.jZ3jPpOVzd4uuUrD0dXKy8qBwWc4mgd5z.q76n875rTO');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('2','103','$2y$10$bpTDffX9V7XsC2uEvHVsX.7YbU2bPjcYhiM0TzsixsKT0L.sDNAK6');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('3','104','$2y$10$tEM0Sr44X2GFG/xVeTZ07Oh13ezF5BvAMcdu7ED6.eYlO5RhrWhOi');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('4','105','$2y$10$E/OqyQqHrD9BljOX/A0OMu9pF2vl/TVDKM5JEH0Ad2AeKhD1NvQBe');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('5','106','$2y$10$xmrNma19lvss5Z8VWw0oPe9E3ICF4F/qq0O4Lr2dzVwDl23T0ZV4i');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('6','107','$2y$10$yghJSD9hntZ9at2iVpzSG.OJPeLqah.zCjgkP6ZoiQfxVnL3J3Kka');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('7','108','$2y$10$hqU/kIowRR1RdHRMtfk0m.ubIJ5yVHOlI3KsYJphDO9wU/bRUoJSK');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('8','109','$2y$10$l4M/VQSUzzFn995u/78QCOYIAKmC76nrmKtDLTDmzrlGW9My8PeBO');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('9','110','$2y$10$k.GKdyx/hI1rzPBCVGxEDu3e1LMCWIp2ukJWHkYshiv60zn/T3iS2');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('10','111','$2y$10$.VpUU1MWUFGR2Um4ozNuauRWK3/Mo8mpEnVI/fEnGOEyfaJJhTLF.');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('11','112','$2y$10$8AHI9Of8bF/Mhex2ytTaaujIcvmRWTu.JamIIT3z4OV/hIiao0vzy');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('12','113','$2y$10$p0SYp8QYMrAUcxw38IgaQuM4liDrFI8ujA/VEn0tY5T8MuotvZu8y');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('13','114','$2y$10$BWJhSvBQM0IkW7HvLicsVO7jAgwTIED0fYG28NimeEIQnL6E.HI3W');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('14','115','$2y$10$YNwUJX6ZeKeSkussvzNnVuAnMRvrdPI6mbmYUrbLL/d2aZY0wVB1i');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('15','116','$2y$10$oQ63I91E7zly0TLJfukS0umhXpp0fKhdBXTFMsLa83SyxImjhZfy2');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('16','117','$2y$10$w7RcMkNuQ887Xb3XcHAVXOOOgF913sLB6MsDze9TC7CwM/j4QQuCi');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('17','118','$2y$10$FZGtxHLvj42PqW1eKv/b6.cwyQl5qt8fBleiwzkkqBfniKEQZYMUW');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('18','119','$2y$10$E/ZrUwjkvOG7qMRWnjVh0Om.Osp9ZEBjvqSInGwlbAouuaxZyF8Aq');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('19','120','$2y$10$Xqtr.J7W.Qr.y.JVS/8k9u6HivPUzMr7LjhBNsjtxHOw2IVH.ELsq');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('20','121','$2y$10$Ip.KW3DKI7ResodU8.wYGOB07Xy6MDGmowPdji5RmULoKsOUq7zcG');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('21','122','$2y$10$jwrnZCi9jzwOrkJDmEFjs.W3ybIAPw1ih/DqiBSsyEQtA9PLIsjFe');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('22','123','$2y$10$jSaNNIKiCj271dqlr3qpGeRKAiIeF7rKzlT9h1LhQOM5M2nARA6Dm');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('23','124','$2y$10$1R5b38c5eSBf8gVWkRvJBeePc36/aOnJFIchKlT0nBLOoXSKZDvEm');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('24','125','$2y$10$bcwnH.w1SFZK/QhADMLeQuMiz8Zo.OeZAqSIZk6RFSBNCB1otjMbS');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('25','126','$2y$10$yA1BgOkhWaK6Y7wuYXuWye56lWgCXku84EP7A6TXV0c.b20DCAj0a');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('26','127','$2y$10$hTo7gyt.9YCVWiVzHwAM9eVJytVeKvHY/JBQNVzlm1EDLpqCvDlmG');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('27','128','$2y$10$Q.HbaFaQuWpX2A0CTvxPDuwgLJfwco0R9egIa5X2IKPZryzZxHdaa');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('28','129','$2y$10$jyPtEqNa/.WSOnOHTZCf3.qCZJd0QEXFdXiYnEC3DG.dr/HXv64CG');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('29','130','$2y$10$IRRFLKbvqCCB9G9vn5YCEOzISBFOo9.iZMnsBJnVCEsVyI/7scf1y');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('30','131','$2y$10$bPByGm1Wo/iWoEl.Z6Ta6.Sg.tw.XrAHmq05fS4aYOPaUfS1rYC6a');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('31','132','$2y$10$UMUnZL89D1E20SxLquAxUuggYfhX0HlDCTBwB.fyztuL290YEuAPm');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('32','133','$2y$10$XCVgtLDIL4Ht83Cyh4SgnunocAHi10k5MZ7pyhzMK7aZrYT6vUrKO');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('33','134','$2y$10$xY6k34hsLc9wa6VBV32vYuh1MxU41RV6ExZKG5JDZDlPBnbVjOQEG');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('34','135','$2y$10$Cdm9iEnTwdeDxKW4FhdO.OeDWit.tTJsd/PY7lcEk6xMv/NYPP3/u');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('35','136','$2y$10$p8VvMTQ63TwmUAnTHfzRkuUyfBxpRSK2Bk9vC95Vx/T3S5uGdzu0u');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('36','137','$2y$10$jCILc/JSu/m313fvuGf.F.KS0SMKUgy9DG3Mu/p9GRaOm9rDAV3Je');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('37','138','$2y$10$ZBlGl0UrxOAODAMJNWgcN.bRzkXGyEG97HICIhAxT0LWrTCyvuzgi');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('38','139','$2y$10$.sFKWXWONl1gWZmU55roZu.rlRBMxq3zCAD8I2ORiUqY/FweYWY/i');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('39','140','$2y$10$SpeOdpZHJxytgighVdp81eCgw4uUNdiVcO4aTN33QZgmZgsdPDEre');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('40','141','$2y$10$zDV9YMAKH.hJ/cqyueyDqeQNQK6k.XtlTtLviaMld1.cYXxWwINEm');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('41','142','$2y$10$Y9TR5yPMcB2AqmuOe3eKEOangJ3YB9/PUSHBjk42aNCA4rHD/9Uly');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('42','143','$2y$10$gEk7QOmEFcthG0cQCJduhedVVAzGhrVHFKtUNZhsjqVgkY50BDwPS');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('43','144','$2y$10$TgcAf6ThVHxR8x./Red6sO18QqcqpjSiz.1os5V9wo4ovPBELstw2');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('44','145','$2y$10$i9xEQHjcAIxQGC8EWHIJ/uJMUPKl9OGCF9gpYNRh/HM3jVyiTKjey');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('45','146','$2y$10$OMQ9mSiu4efCXmlWE7POh.tNFcGE1BIuk.XN5ggFiHohkb9Qqq6Ke');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('46','147','$2y$10$1kNYmKqOTpe4DmQBp7xLh.QVt/gAE7lSInBJBiv2ec8mu0KXgDoyq');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('47','148','$2y$10$BTD9GTJy4KaMUZqVNe5XLexRbUCCXK3iXBFlIkdcqoXFyJVE4ZYT6');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('48','149','$2y$10$3yOQaGyJPAy0H8cf.V5dcuR9WmOmydfAZdEyoQpfa5OdDjvByxWFq');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('49','150','$2y$10$.2mJcVUJyf8Yma0jnedqwuVIBGz2v50Gxjz52Ziv5w2VWihUFEX1m');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('50','151','$2y$10$71dJLJgV0L/JT.z9dURtku9FOr6FRKiJcyHpXfa0FepaOzo1yAs1y');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('51','152','$2y$10$8RhXG8H2FkyKI1w4s109j.y9b9.34lN2TnMdms24dnx4U0EUirtBi');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('52','153','$2y$10$Oatb1KpJnMqdRtM0RljkYe3.PiJzCN82Qc.yAtnOFsCI3J5FSdWay');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('53','154','$2y$10$AD2MuHjkFUNUWUDq6jjFLuMW.PFjifP8FWAjPeafQ2U/X9GrFvnIG');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('54','155','$2y$10$5JVEaKPOmLQXS53GKQzGTefMkJn06X4fWthnAf4/duTHX52nyh5m.');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('55','156','$2y$10$3ZNilcYXJ1bBeVrkZlXavOk5P94RMWL/Mc4MZaZ3VtXbO7XnbBFwe');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('56','157','$2y$10$o1qOiTuzL43VUl.F73kol.lopHdg7RA10Muetu/e0.aEqdJZ9CTHm');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('57','158','$2y$10$p8CBvt4PGiui3mf1fvkfdORbtJP9QEPHts8zxYiLrGX6iR1PVYYjy');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('58','159','$2y$10$bSW9uzGPERHeYn5npXmzB.iEt2eUrDqgucF/J/sA1Q6mOYzB54.6.');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('59','160','$2y$10$kvnFWfCiyjePho8VUo0zuOFmrjz8DeBRAzOH.zwIBXPT7fi/x3D8i');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('60','161','$2y$10$2k3eEf7jXsufUrWuOX.A2urFZ8fVWdxK6xn.z1alQ0trlCaD2RMOu');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('61','162','$2y$10$4m2E4irgtgqY7.yB7NXIie5A2TBc9tHz66fCkvdrcgIXbkJKgDd42');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('62','163','$2y$10$AScBLf8IUOvt373gG6i8FOzRXQV463F455AAnbKEfLREHMDLbqq9C');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('63','164','$2y$10$HK25mlBQYK46dE3tKfh1aeWgGSFrFFa.eghu4WWDKf8GunEMVO.W.');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('64','165','$2y$10$hhm.lQeUwpesdpJcmVZ0leQVGuiLr2/4GijPaXStNooENSnCllDNi');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('65','166','$2y$10$ykdhNr4zW4yZrrNk0kEMEuXIjgMB9cGPTYHoSEgH0YDFN9L8NJrgm');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('66','167','$2y$10$QQRu3aM3ktYIBUtxwt.ULuKTyZdp3NAPPRdfLql46c.PbFp9CBRJ6');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('67','168','$2y$10$ZE1e3vSVCV.2PLK8J7zOa.21zW.J3knDyWu78qN/YpVzM85dWNAtq');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('68','169','$2y$10$BgSNYbXUdEyPO5V/6SDen.q1pfUjRPYh/BMW6FletaxlwpQ2.PwpS');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('69','170','$2y$10$OcZyqPGPvIAs9Uxg0hUNUu1ijBX99jO1VJO7EXx9nROKVyyfl5AKO');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('70','171','$2y$10$IIEIYIwGyHkrQnLeTufoCudhjGtopejem/Ty9LA2PjYvxmmyh0iai');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('71','172','$2y$10$WvC7wuD.homwANbIkWi1WOuYy6OFkZMal3hhOIf0pW7YYp./iwH8u');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('72','173','$2y$10$ZzpnXLMoMA4pVNmHZ9KML.GM5YnIDUxA8RAnnP5tG0McmF96RreR2');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('73','174','$2y$10$bX0vUgWHrzp.NGA.ql086.5V2zdWvTphcq8eia4UgzCzEv7hrOq6a');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('74','175','$2y$10$.Jh7AYt1A5He1KxkgvU/yuuUyXhYXPU98BmbRWD0BkmG/LQrjhaxO');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('75','176','$2y$10$9qnrfaNzg516yec9W41q8OIYBlOJf7RaBTM8Jug0pOv.twjWRByP6');
INSERT INTO `registrations` (`id`,`user_id`,`token`) VALUES ('76','177','$2y$10$9CJTi3UIllHU8cgnz9h0X.IokTkR2BvFY2LpxOeloP0nj5kcJjGYu');
/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;


--
-- Create Table `sectors`
--

DROP TABLE IF EXISTS `sectors`;
CREATE TABLE `sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `sectors`
--

/*!40000 ALTER TABLE `sectors` DISABLE KEYS */;
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Agriculture & Fisheries','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Animal Health & Rights','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Cultural','1','2015-05-04 09:59:35','2015-05-19 09:31:00');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Conflict Resolution','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Coordination','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('6','Corporation (CSR)','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('7','Crisis & Emergencies','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('8','Cultural','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('9','Disaster Risk Reduction','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('10','Economy & Trade','0','2015-05-04 09:59:35','2015-05-19 09:32:00');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('11','Endangered Species','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('12','Education','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('13','Energy','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('14','Environment','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('15','Food & Nutrition','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('16','Gender & WID','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('17','Governance','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('18','Habitat, Shelter & NFI','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('19','Health','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('20','International Trade','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('21','IT & Communications','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('22','Law & Legal Affairs','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('23','Livelihoods','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('24','Telecommunications','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('25','MDG','1','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('26','Mental Health','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('27','Microfinance','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('28','Natural Resources','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('29','Population & Settlement','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('30','Protection & Human Rights','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('31','Safety & Security','0','2015-05-04 09:59:35','2015-05-04 09:59:35');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('32','Science & Technology','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('33','Water Sanitation Hygiene','0','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('34','Financing','0','2015-05-19 09:33:00','2015-05-19 09:33:00');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('35','Labour','0','2015-05-19 09:34:00','2015-05-19 09:34:00');
INSERT INTO `sectors` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('36','Peace & Security','0','2015-05-19 09:43:00','2015-05-19 09:43:00');
/*!40000 ALTER TABLE `sectors` ENABLE KEYS */;


--
-- Create Table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `default_value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `settings`
--

/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`,`key`,`value`,`default_value`,`created_at`,`updated_at`) VALUES ('1','urls_blog_blogpost_prefix','blogs','blogs','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `settings` (`id`,`key`,`value`,`default_value`,`created_at`,`updated_at`) VALUES ('2','urls_news_newsitem_prefix','news','news','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `settings` (`id`,`key`,`value`,`default_value`,`created_at`,`updated_at`) VALUES ('3','urls_videos_video_prefix','videos','videos','2015-05-04 09:59:37','2015-05-04 09:59:37');
INSERT INTO `settings` (`id`,`key`,`value`,`default_value`,`created_at`,`updated_at`) VALUES ('4','urls_takeactions_takeaction_prefix','get-involved','take-actions','2015-05-04 09:59:37','2015-05-04 09:59:37');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;


--
-- Create Table `take_actions`
--

DROP TABLE IF EXISTS `take_actions`;
CREATE TABLE `take_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_id` int(10) unsigned DEFAULT NULL,
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `take_actions_type_id_index` (`type_id`),
  KEY `take_actions_organisation_id_index` (`organisation_id`),
  KEY `take_actions_user_profile_id_index` (`user_profile_id`),
  KEY `take_actions_user_id_index` (`user_id`),
  CONSTRAINT `take_actions_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `take_actions_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types_of_take_actions` (`id`),
  CONSTRAINT `take_actions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `take_actions_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions`
--

/*!40000 ALTER TABLE `take_actions` DISABLE KEYS */;
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('51','16AB1471-5E53-44F7-861E-32A7707C2D89','Sponsor a child','sponsor-a-child','Please support Plan&rsquo;s long-term development work by sponsoring a child.','/uploads/media/123/takeactions/Plan.jpg','http://plan-international.org/what-you-can-do/sponsor-a-child/sponsor','1','1','0','2015-05-20 01:45:40','2015-05-20 01:45:40','7','71','123','123');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('52','C71F471E-2FEA-4EDD-93F1-234C1DE0F180','Shop women\'s clothing','shop-womens-clothing','We have 20,000 items of second-hand and new women&#39;s clothing &amp; accessories in Oxfam&#39;s Online Shop. From skirts and dresses to jumpers and coats, we&#39;ve got thousands of unique items for you to make that perfect outfit.','/uploads/media/121/takeactions/Oxfam.png','http://www.oxfam.org.uk/shop/womens-clothing/?intcmp=womens-hp-shop-womens-16-apr-2015','1','1','0','2015-05-20 01:49:54','2015-05-20 01:49:54','6','69','121','121');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('53','043926DB-0432-403A-8A04-221FA995264E','Race Against Hunger','race-against-hunger','Empowering people like you to take a stand against hunger is a vital part of our fight against global malnutrition. Whether you&rsquo;re a student, a teacher, a corporate representative, or a concerned citizen, there&rsquo;s plenty you can do to help put an end to global malnutrition.&nbsp;','/uploads/media/110/takeactions/ACF1.jpg','http://www.actionagainsthunger.org/take-action/race-against-hunger','1','1','0','2015-05-21 02:01:50','2015-05-21 02:01:50','3','58','110','110');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('54','283D72A1-0DB5-4C57-8037-CB6BDD71AF3A','MY BODY MY RIGHTS MANIFESTO','my-body-my-rights-manifesto','My Body My Rights is Amnesty&rsquo;s global campaign to stop the control and criminalization of sexuality and reproduction. Join us in defending sexual and reproductive rights for all. It&rsquo;s your body. Know your rights.','/uploads/media/106/takeactions/Amnesty.png','http://https://www.amnesty.org/en/campaign-my-body-my-rights/','1','1','0','2015-05-21 02:15:49','2015-05-21 02:15:50','1','55','106','106');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('55','C7B44BAD-718F-45E4-BAFB-1A1BC332E141','Join and protect human rights','join-and-protect-human-rights','Our members make change possible. They&rsquo;re the people we call on whenever and wherever human rights are under attack. Their actions, big and small, put pressure on governments, institutions and decision-makers to do the right thing.','/uploads/media/106/takeactions/amnesty 2.jpg','http://https://www.amnesty.org/en/get-involved/join/','1','1','0','2015-05-21 02:18:27','2015-05-21 02:18:27','5','55','106','106');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('56','DFA0D1E2-047F-40BE-B408-7265116AEDB7','Walk In Her Shoes','walk-in-her-shoes','Walk 10,000 steps a day for a week and help improve access to water across the globe, allowing giving women and girls to get an education or work, and have a chance of fulfilling their potential.&nbsp;','/uploads/media/111/takeactions/CARE.jpg','http://https://walkinhershoes.careinternational.org.uk/','1','1','0','2015-05-21 05:31:43','2015-05-21 05:31:43','3','59','111','111');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('57','500DCC1E-CFB3-4F4A-BF1F-5875B941C25D','Several ways to give to Caritas','several-ways-to-give-to-caritas','Giving to Caritas Internationalis allows us to strengthen the global confederation of over 160 national Catholic charities. We serve all poor people, of all faiths, all over the world.','/uploads/media/112/takeactions/Caritas.jpg','http://www.caritas.org/take-action/donate/','1','1','0','2015-05-21 05:37:58','2015-05-21 05:37:58','2','60','112','112');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('58','DCC4B654-202C-46AB-A900-38FECE2093FB','Make Big Polluters Pay','make-big-polluters-pay','The strong and resilient people of the Philippines are ready to take ambitious action, including legal, against the biggest polluters, and they need us all to stand with them.','/uploads/media/116/takeactions/Greenpeace.jpg','http://act.greenpeace.org/ea-action/action?ea.client.id=1844&ea.campaign.id=33046&ea.tracking.id=gpi','1','1','0','2015-05-21 05:48:14','2015-05-21 05:48:14','1','64','116','116');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('59','060C2E1E-164E-4E83-95FD-EAFCC4B1FA6A','Sign up for email updates','sign-up-for-email-updates','We&#39;d love to stay in touch. Whether we&#39;re petitioning politicians or saving lives, sign up for our email updates and we&#39;ll let you know what Oxfam&#39;s up to, and how you can get involved.','/uploads/media/121/takeactions/Oxfam 2.jpg','http://www.oxfam.org.uk/email-newsletter-signup','1','1','0','2015-05-21 05:52:40','2015-05-21 05:53:15','5','69','121','121');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('60','96BD2E1E-D678-43D3-ADD8-61FD1E8A10A1','Make a bequest, leave a lasting legacy','make-a-bequest-leave-a-lasting-legacy','Link your name with the history of the largest humanitarian movement of our time. By making a bequest to the ICRC, you take your commitment to helping victims of war and other situations of violence worldwide to a new level.','/uploads/media/117/takeactions/ICRC.jpg','http://https://www.icrc.org/en/support-us/audience/bequests-and-legacies','1','1','0','2015-05-21 06:06:51','2015-05-21 06:06:51','7','65','117','117');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('61','7D5DF082-F017-4574-96A1-5F2BF790365B','Stay Informed','stay-informed','Subscribe to our mailing list','/uploads/media/118/takeactions/MdM.jpg','http://doctorsoftheworld.org/newsletter/','1','1','0','2015-05-21 06:11:10','2015-05-21 06:11:10','5','66','118','118');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('62','EBCCC2A5-D51F-430D-8FDA-62B212E5C5C5','Donations to MSF help save lives','donations-to-msf-help-save-lives','Every year, our clinical staff provide direct care for common ailments that, left untreated, can prove deadly. When an outbreak or disaster overwhelms existing health services, we bring an emergency medical response. We carry out surgery, provide maternal care, and treat neglected diseases like tuberculosis, kala azar and sleeping sickness, all thanks to the generosity of our donors. ','/uploads/media/119/takeactions/MSF.jpg','http://www.msf.org/donate','1','1','0','2015-05-21 06:31:54','2015-05-21 06:31:54','2','67','119','119');
INSERT INTO `take_actions` (`id`,`name`,`title`,`slug`,`content`,`image`,`url`,`published`,`featured`,`deleted`,`created_at`,`updated_at`,`type_id`,`organisation_id`,`user_profile_id`,`user_id`) VALUES ('63','FE0BAAE6-431B-4648-8B94-A249E590C38C','Help protect their icy home','help-protect-their-icy-home','Sea ice is melting, and industrial development is coming to the Arctic. What does it mean for the mighty tusked walrus?\r You can help WWF better understand walruses and protect their icy home. Gifts for a Living Planet &ndash; Make Them Smile!','/uploads/media/130/takeactions/WWF.jpg','http://shop.panda.org/walrus-mystery.html','1','1','0','2015-05-21 06:38:01','2015-05-21 06:38:01','4','78','130','130');
/*!40000 ALTER TABLE `take_actions` ENABLE KEYS */;


--
-- Create Table `take_actions_beneficiaries`
--

DROP TABLE IF EXISTS `take_actions_beneficiaries`;
CREATE TABLE `take_actions_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_beneficiaries_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_beneficiaries_beneficiary_id_index` (`beneficiary_id`),
  CONSTRAINT `take_actions_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  CONSTRAINT `take_actions_beneficiaries_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_beneficiaries`
--

/*!40000 ALTER TABLE `take_actions_beneficiaries` DISABLE KEYS */;
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('51','51','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('52','52','8','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('53','53','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('54','54','25','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('55','55','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('56','56','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('57','57','17','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('58','58','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('60','59','8','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('61','60','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('62','61','8','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('63','62','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_beneficiaries` (`id`,`takeaction_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('64','63','27','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `take_actions_beneficiaries` ENABLE KEYS */;


--
-- Create Table `take_actions_countries`
--

DROP TABLE IF EXISTS `take_actions_countries`;
CREATE TABLE `take_actions_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_countries_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_countries_country_id_index` (`country_id`),
  CONSTRAINT `take_actions_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `take_actions_countries_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_countries`
--

/*!40000 ALTER TABLE `take_actions_countries` DISABLE KEYS */;
INSERT INTO `take_actions_countries` (`id`,`takeaction_id`,`country_id`,`created_at`,`updated_at`) VALUES ('51','52','224','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_countries` (`id`,`takeaction_id`,`country_id`,`created_at`,`updated_at`) VALUES ('52','53','225','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_countries` (`id`,`takeaction_id`,`country_id`,`created_at`,`updated_at`) VALUES ('53','56','224','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_countries` (`id`,`takeaction_id`,`country_id`,`created_at`,`updated_at`) VALUES ('54','58','170','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `take_actions_countries` ENABLE KEYS */;


--
-- Create Table `take_actions_crisis`
--

DROP TABLE IF EXISTS `take_actions_crisis`;
CREATE TABLE `take_actions_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_crisis_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_crisis_crisis_id_index` (`crisis_id`),
  CONSTRAINT `take_actions_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  CONSTRAINT `take_actions_crisis_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_crisis`
--

/*!40000 ALTER TABLE `take_actions_crisis` DISABLE KEYS */;
/*!40000 ALTER TABLE `take_actions_crisis` ENABLE KEYS */;


--
-- Create Table `take_actions_interventions`
--

DROP TABLE IF EXISTS `take_actions_interventions`;
CREATE TABLE `take_actions_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_interventions_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_interventions_intervention_id_index` (`intervention_id`),
  CONSTRAINT `take_actions_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  CONSTRAINT `take_actions_interventions_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_interventions`
--

/*!40000 ALTER TABLE `take_actions_interventions` DISABLE KEYS */;
/*!40000 ALTER TABLE `take_actions_interventions` ENABLE KEYS */;


--
-- Create Table `take_actions_regions`
--

DROP TABLE IF EXISTS `take_actions_regions`;
CREATE TABLE `take_actions_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_regions_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_regions_region_id_index` (`region_id`),
  CONSTRAINT `take_actions_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`),
  CONSTRAINT `take_actions_regions_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_regions`
--

/*!40000 ALTER TABLE `take_actions_regions` DISABLE KEYS */;
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('51','51','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('52','52','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('53','53','6','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('54','54','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('55','55','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('56','56','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('57','57','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('58','58','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('60','59','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('61','60','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('62','61','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('63','62','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_regions` (`id`,`takeaction_id`,`region_id`,`created_at`,`updated_at`) VALUES ('64','63','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `take_actions_regions` ENABLE KEYS */;


--
-- Create Table `take_actions_sectors`
--

DROP TABLE IF EXISTS `take_actions_sectors`;
CREATE TABLE `take_actions_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_sectors_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_sectors_sector_id_index` (`sector_id`),
  CONSTRAINT `take_actions_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`),
  CONSTRAINT `take_actions_sectors_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_sectors`
--

/*!40000 ALTER TABLE `take_actions_sectors` DISABLE KEYS */;
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('51','51','12','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('52','53','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('53','54','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('54','55','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('55','56','33','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('56','58','14','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('57','60','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('58','62','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_sectors` (`id`,`takeaction_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('59','63','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `take_actions_sectors` ENABLE KEYS */;


--
-- Create Table `take_actions_themes`
--

DROP TABLE IF EXISTS `take_actions_themes`;
CREATE TABLE `take_actions_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_themes_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_themes_theme_id_index` (`theme_id`),
  CONSTRAINT `take_actions_themes_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`),
  CONSTRAINT `take_actions_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `take_actions_themes`
--

/*!40000 ALTER TABLE `take_actions_themes` DISABLE KEYS */;
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('51','51','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('52','52','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('53','53','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('54','54','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('55','55','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('56','56','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('57','57','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('58','58','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('60','59','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('61','60','21','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('62','61','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('63','62','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `take_actions_themes` (`id`,`takeaction_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('64','63','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `take_actions_themes` ENABLE KEYS */;


--
-- Create Table `themes`
--

DROP TABLE IF EXISTS `themes`;
CREATE TABLE `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `themes`
--

/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Advocacy','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Award','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Career','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Challenge','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Crisis','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('6','Disaster','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('7','Finding / Research','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('8','Funding','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('9','Innovation ','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('10','MDG','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('11','New initiative','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('12','Partnerships','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('13','Security','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('14','Success story','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('15','Support request','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('16','Trend','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('17','Vulnerable group','1','2015-05-04 09:59:36','2015-05-04 09:59:36');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('18','Crisis & Relief','0','2015-05-19 09:46:00','2015-05-19 09:46:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('19','CSR & Industry','0','2015-05-19 09:47:00','2015-05-19 09:47:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('20','Ecology','0','2015-05-19 09:47:00','2015-05-19 09:47:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('21','Equality & Rights','0','2015-05-19 09:47:00','2015-05-19 09:47:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('22','Poverty & Solidarity','0','2015-05-19 09:48:00','2015-05-19 09:48:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('23','Public Services','0','2015-05-19 09:48:00','2015-05-19 09:48:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('24','Ways of working','1','2015-05-19 09:48:00','2015-05-19 09:48:00');
INSERT INTO `themes` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('25','Ways of Working','0','2015-07-18 03:05:00','2015-07-18 03:05:00');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;


--
-- Create Table `types_of_publicities`
--

DROP TABLE IF EXISTS `types_of_publicities`;
CREATE TABLE `types_of_publicities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `types_of_publicities`
--

/*!40000 ALTER TABLE `types_of_publicities` DISABLE KEYS */;
INSERT INTO `types_of_publicities` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Full Screen','0','2015-05-04 10:02:12','2015-05-04 10:02:12');
INSERT INTO `types_of_publicities` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Top Banner','0','2015-05-04 10:02:12','2015-05-04 10:02:12');
INSERT INTO `types_of_publicities` (`id`,`name`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Sidebar','0','2015-05-04 10:02:12','2015-05-04 10:02:12');
/*!40000 ALTER TABLE `types_of_publicities` ENABLE KEYS */;


--
-- Create Table `types_of_take_actions`
--

DROP TABLE IF EXISTS `types_of_take_actions`;
CREATE TABLE `types_of_take_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `types_of_take_actions`
--

/*!40000 ALTER TABLE `types_of_take_actions` DISABLE KEYS */;
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('1','Campaign','#1cba93','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('2','Donate','#FF5953','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('3','Fundraise','#2ab3e1','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('4','Give a Gift','#ff9000','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('5','Join','#839061','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('6','Shop','#8978f4','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
INSERT INTO `types_of_take_actions` (`id`,`name`,`color`,`deleted`,`created_at`,`updated_at`) VALUES ('7','Other','#78808c','0','2015-05-04 10:01:12','2015-05-04 10:01:12');
/*!40000 ALTER TABLE `types_of_take_actions` ENABLE KEYS */;


--
-- Create Table `user_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
CREATE TABLE `user_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` int(10) unsigned DEFAULT NULL,
  `based_in` int(10) unsigned DEFAULT NULL,
  `job_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noavatar.png',
  `thought` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_status` int(10) unsigned DEFAULT NULL,
  `professional_status` int(10) unsigned DEFAULT NULL,
  `top_member` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8_unicode_ci,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employer_organisation_type` int(10) unsigned DEFAULT NULL,
  `level_of_responsibility` int(10) unsigned DEFAULT NULL,
  `cover_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  PRIMARY KEY (`id`),
  KEY `user_profiles_based_in_index` (`based_in`),
  KEY `user_profiles_employer_organisation_type_index` (`employer_organisation_type`),
  KEY `user_profiles_level_of_responsibility_index` (`level_of_responsibility`),
  KEY `user_profiles_member_status_index` (`member_status`),
  KEY `user_profiles_nationality_index` (`nationality`),
  KEY `user_profiles_professional_status_index` (`professional_status`),
  KEY `user_profiles_user_id_index` (`user_id`),
  CONSTRAINT `user_profiles_based_in_foreign` FOREIGN KEY (`based_in`) REFERENCES `countries` (`id`),
  CONSTRAINT `user_profiles_employer_organisation_type_foreign` FOREIGN KEY (`employer_organisation_type`) REFERENCES `organisation_types` (`id`),
  CONSTRAINT `user_profiles_level_of_responsibility_foreign` FOREIGN KEY (`level_of_responsibility`) REFERENCES `level_of_responsibilities` (`id`),
  CONSTRAINT `user_profiles_member_status_foreign` FOREIGN KEY (`member_status`) REFERENCES `member_statuses` (`id`),
  CONSTRAINT `user_profiles_nationality_foreign` FOREIGN KEY (`nationality`) REFERENCES `nationalities` (`id`),
  CONSTRAINT `user_profiles_professional_status_foreign` FOREIGN KEY (`professional_status`) REFERENCES `professional_statuses` (`id`),
  CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `user_profiles`
--

/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('101','101','fleava','admin',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-04 09:59:59',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('102','102','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-06 08:07:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('103','103','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-06 08:22:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('104','104','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-14 01:10:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('105','105','John-Adb','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-18 21:39:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('106','106','John-Amnesty','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-18 23:05:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('107','107','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 03:18:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('108','108','John Ausaid','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 04:11:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('109','109','Takehido','Nakao','91','170','President','Asian Development Bank','/uploads/media/109/takehiko-nakao - ADB.jpg','','1','2','1','1','2015-07-04 18:46:43','2015-07-05 01:46:43','Nakao was a career civil servant with the Finance Ministry of Japan which he joined in 1978. During his career, Nakao served variously as Minister at the Embassy of Japan in Washington D.C. and as an economic advisor at the International Monetary Fund in the mid \'90s.As Japan\'s top currency official, Nakao oversaw the Japanese government\'s largest ever currency market intervention following the sharp appreciation of the yen in 2011. He was also the Japanese representative at the G7 and G20 summits in 2012.Before his nomination for the presidency of the ADB, Nakao was Vice Minister of Finance for International Affairs which is the senior most civil service appointment within the country\'s Finance Ministry.\r\n','male','http://','http://','http://','9','3','/uploads/media/109/Nakao.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('110','110','John-ACF','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:06:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('111','111','John-Care','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:11:51',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('112','112','John-Caritas','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:19:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('113','113','John-Dfid','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:22:53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('114','114','John-FAO','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:30:49',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('115','115','John-Fatdc','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:37:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('116','116','John-Greenpeace','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:45:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('117','117','John-ICRC','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 11:48:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('118','118','John-MdM','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:03:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('119','119','John-MSF','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:09:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('120','120','John-Iom','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:23:51',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('121','121','John-Oxfam','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:25:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('122','122','John-Padf','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:38:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('123','123','John-Plan','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:40:24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('124','124','John-Undp','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:45:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('125','125','John-Unicef','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:47:54',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('126','126','John-Unilever','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:55:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('127','127','John-Usaid','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 12:57:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('128','128','John-WB','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 13:05:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('129','129','John-Who','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 13:07:40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('130','130','John-Panda','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-19 22:53:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('131','131','Aaron','Chafetz','4','225','Program Analyst ','USAID','/uploads/media/131/Chafez-USAID.jpg','Test for my your thought of the day','1','2','1','1','2015-07-13 22:12:19','2015-07-14 05:12:19','Aaron Chafetz is a Program Analyst in USAID’s Bureau for Economic Growth, Education and Environment. \r\n','male','http://','http://','http://','9','3','/uploads/media/131/1131954934_Chafetz.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('132','132','Christine','Nesbitt Hills','171','225','Sr. Photo Editor','UNICEF','/uploads/media/132/Nesbitt-Unicef.jpg','Test for my your thought of the day','1','2','1','1','2015-07-08 16:59:13','2015-07-08 23:59:13','Independent photography/video producer & editor with proven record in journalism and documentary Africa-wide, on both long-term and short-term projects. Demonstrated commitment to marginalised people. Strong integrity and ethics for documentation of vulnerable women and children. Interest and knowledge of communication for development studies informs practice, as does a career firmly rooted in journalism. \r\n','female','http://','http://','http://','9','3','/uploads/media/132/710760008_Nesbit.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('133','133','John-Wto','Doe',NULL,NULL,'PR Officer',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-21 05:14:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('134','134','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-21 06:56:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('135','135','Bo Victor','Nylund','35','225','Senior Advisor - Corporate Social Responsibility','UNICEF','/uploads/media/135/Nylund-UNICEF.jpg','','1','2','1','1','2015-07-24 01:07:32','2015-07-24 08:07:32','Bo Viktor Nylund is a protection and legal practitioner who throughout his career has focused on advocacy and engagement with state and non-state actors with a view to driving accountability in the implementation of human rights. Bo Viktor has a PhD in International Law, as well as Master’s degrees in law and political science from Columbia University Law School and Abo Akademi University. \r\n','male','http://','http://','http://','9','2','/uploads/media/135/Nylund-Cover image.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('136','136','Haritian','Pinio',NULL,NULL,'',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-25 09:06:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('137','137','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-25 11:51:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('138','138','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-05-25 11:59:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('139','139','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-25 23:31:45','2015-06-13 08:28:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage2.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('140','140','John','UNDP',NULL,NULL,'whatever',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-28 16:16:15','2015-06-28 23:16:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('141','141','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-28 21:26:05','2015-06-28 21:26:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('142','142','John','Unicef',NULL,NULL,'whatever',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-06-28 15:52:35','2015-06-28 22:52:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('143','143','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-01 07:36:45','2015-07-01 07:36:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('144','144','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-04 13:24:23','2015-07-04 13:24:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('145','145','Sabrina','de Souza','27','224','UK Advocacy Manager ','Action Against Hunger | ACF-UK','/uploads/media/145/Sabrina de Souza.png','Test for my your thought of the day','1','2','0','1','2015-07-04 06:48:16','2015-07-04 13:48:16','I am a driven and experienced advocacy manager with a track record in developing and delivering on UK and international advocacy strategies. I have a detailed understanding and knowledge of the UK Government and its role in development. Over the years I have developed experience in both high-level and grassroots advocacy engagement. \r\n\r\nI am enthusiastic, driven to meet targets, and a naturally social person who enjoys working in challenging and result driven environments. I adapt quickly to new surroundings, as a self-starter with the ability to motivate. I am honest, reliable and conscientious and look forward expanding my experience and building on my previous achievements.\r\n','female','','','','8','3','/uploads/media/145/De Souza.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('146','146','John','WV',NULL,NULL,'whatever',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-04 15:41:18','2015-07-04 22:41:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('147','147','rodge','ORG BIDON',NULL,NULL,'bal',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-09 01:47:16','2015-07-09 08:47:16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('148','148','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-09 08:58:34','2015-07-09 08:58:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('149','149','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-09 09:18:15','2015-07-09 09:18:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('150','150','Wolfgang','Jamann','68','206','Secretary General and CEO','CARE International','/uploads/media/150/804172359_Untitled picture.png','','1','2','0','1','2015-07-09 18:17:00','2015-07-10 01:17:00','Dr. Wolfgang Jamann is responsible for coordinating the work of the CARE International confederation, which is composed of 14 national members engaged in emergency relief and long-term development work across the globe. Prior to his current position at CARE, he was the CEO and Chairman of Welthungerhilfe (German Agro Action; 2009 – 2015), one of the largest international aid organizations in Germany fighting hunger and poverty, with a particular focus on food security and sustainable development. Between January 2014 and February 2015 he was also president of Alliance 2015, a network of eight European development and humanitarian organizations. Before joining Welthungerhilfe, Dr. Jamann had already gained experience with CARE as CEO of CARE Deutschland-Luxembourg (2004 – 2009). Prior to that, he worked in different roles and countries for World Vision as well as the United Nations Development Programme and the German Foundation for International Development.\r\n','male','','','http://www.care-international.org/about-us/global-network/wolfgang-jamann.aspx','8','3','/uploads/media/150/1754557754_W. Jamann.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('151','151','Chiara','Brunelli','88','59','Servizio civile','Caritas Internationalis','/uploads/media/151/405393495_Chiara Brunelli - Caritas.jpg','','1','2','0','1','2015-07-09 19:13:30','2015-07-10 02:13:30','Chiara has done her Civic Service for Caritas Internationalis in Djobouti. Prior to this  field mission, she worked for the  Ministero degli Affari Esteri - Direzione Generale Cooperazione allo Sviluppo, and Caritas Verona.\r\n','female','','','','8','3','/uploads/media/151/1531784190_Chiara.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('152','152','Brian','Blomme','35','38','Communications coordinator ','Greenpeace','/uploads/media/152/1825369588_Brian Blomme.png','','1','2','0','1','2015-07-09 19:49:38','2015-07-10 02:49:38','Brian Blomme is a climate and energy communications manager for Greenpeace International.','male','','','','8','3','/uploads/media/152/594794527_Brian Blomme.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('153','153','Winnie','Byanyima','184','224','Executive Director','Oxfam International','/uploads/media/153/1369135006_Winnie.png','','1','2','0','1','2015-07-09 22:34:29','2015-07-10 05:34:29','Winnie Byanyima, a grass-roots activist, human rights advocate, senior international public servant, and world recognized expert on women’s rights, is currently Executive Director of Oxfam International. Born in Uganda in 1959, Ms. Byanyima earned engineering degrees in the United Kingdom and began her career as an engineer for Uganda Airlines. She was appointed to the diplomatic service in 1989, where she represented Uganda in France and at UNESCO in Paris. She returned to Uganda in 1994 and for the next ten years she served as a member of parliament, created an all-woman parliamentary caucus, and was founding leader of the Forum for Women in Democracy (FOWODE), a national NGO in Uganda to champion women’s equal participation in decision-making.','female','','http://@Winnie_Byanyima','https://blogs.oxfam.org/en/user/profile/winnie-byanyima','8','3','/uploads/media/153/237733637_byanyima.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('154','154','alfred','Nobel',NULL,NULL,'',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-10 18:46:42','2015-07-11 01:46:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('155','155','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-11 01:37:47','2015-07-11 01:37:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('156','156','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-11 05:49:17','2015-07-11 05:49:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('157','157','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-11 05:52:26','2015-07-11 05:52:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('158','158','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-11 05:53:30','2015-07-11 05:53:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('159','159','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-11 07:57:00','2015-07-11 07:57:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('160','160','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-11 23:30:29','2015-07-11 23:30:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('161','161','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-13 00:50:18','2015-07-13 00:50:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('162','162','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-13 23:02:25','2015-07-13 23:02:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('163','163','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-14 05:31:05','2015-07-14 05:31:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('164','164','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-16 21:27:44','2015-07-16 21:27:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('165','165','John','AER',NULL,NULL,'xxx',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-22 19:12:12','2015-07-23 02:12:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('166','166','John','Ayala',NULL,NULL,'sss',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-22 21:32:58','2015-07-23 04:32:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('167','167','John','AFA',NULL,NULL,'ddd',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-22 23:30:24','2015-07-23 06:30:24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('168','168','Frank','Sinatra','19','20','','','/uploads/media/168/1686265199_Asian Farmers.jpg','','2','3','0','0','2015-08-06 06:29:52','2015-08-06 13:29:52','ccc','male','','','',NULL,NULL,'/uploads/media/168/1426446230_Asian Farmers.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('169','169','ccc','ccc',NULL,NULL,'',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-07-24 15:43:57','2015-07-24 22:43:57',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('170','170','First name ','last neame','10','38','Test','Employer Name','/uploads/media/170/447511712_Human Rights Education Associates logo.jpg','','2','3','0','0','2015-08-14 01:22:44','2015-08-14 08:22:44','Test','male','','','','6','1','/uploads/media/170/609424563_News app_sphere_news.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('171','171','First name','Last Name',NULL,NULL,'JT test',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-08-11 16:49:44','2015-08-11 23:49:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('172','172','First Name','Last name','35','38','Job Title','AidPost','/uploads/media/172/302665072_Charles.jpg','','3','1','0','0','2015-08-14 01:22:42','2015-08-14 08:22:42','Text of Intro','male','','','','8','2','/uploads/media/172/719379126_Nakao-ADB.jpg');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('173','173','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-08-12 23:42:38','2015-08-12 23:42:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('174','174','','',NULL,NULL,NULL,NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-08-13 00:31:51','2015-08-13 00:31:51',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('175','175','ddd','ddd',NULL,NULL,'ddd',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-08-12 17:59:56','2015-08-13 00:59:56',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('176','176','PV','Hieu','189','231','This short text will be ','displayed on your profile page.','/uploads/media/176/610864723_20141221_2018213.jpg','','2','2','0','1','2015-08-14 18:26:33','2015-08-15 01:26:33','This short text will be displayed on your profile page.','male','','','','1','1','/images/noimage.png');
INSERT INTO `user_profiles` (`id`,`user_id`,`first_name`,`last_name`,`nationality`,`based_in`,`job_title`,`employer_name`,`avatar`,`thought`,`member_status`,`professional_status`,`top_member`,`is_active`,`created_at`,`updated_at`,`description`,`gender`,`facebook_url`,`twitter_url`,`website_url`,`employer_organisation_type`,`level_of_responsibility`,`cover_image`) VALUES ('177','177','PV','Hieu',NULL,NULL,'This short text will be ',NULL,'/images/noavatar.png',NULL,NULL,NULL,'0','1','2015-08-14 18:33:10','2015-08-15 01:33:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/images/noimage.png');
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;


--
-- Create Table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `profile_type` int(11) DEFAULT '1',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_new` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Data for Table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('101','fleava_admin','fleava.admin@mailinator.com','$2y$10$OD4TNzLAnRgA/upNSCqMW.e0uM7x/rCXVRMo5qcLRROwRuHBRsZ8G','rsLBZr8f3YERYaqdpp3bCNibAcSp9yXRUowJhcYPVoZtJA6Sr7DlwnxmIHdx','2015-08-14 03:25:24','2015-08-14 10:25:24','99','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('102','test','info@fleava.com','$2y$10$wHsoPaidrluyiXNqZCrTpeyMDqQuzWvZNyzWMrHwNU11wclmHVtbu',NULL,'2015-05-06 08:07:00','2015-05-06 08:07:00','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('103','testing','hello@fleava.com','$2y$10$h7uCQrHoN4lcAf4p2sNxSev7BsuO0T/a/bJ3QnydmF9svkY9vsGJe',NULL,'2015-05-06 08:22:09','2015-05-06 08:22:09','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('104','ACF','rogermarkowski@hotmail.com','$2y$10$26hNSOTlYVxaV1p78TvvWu4sy/ZEXIyiUGNwODEsEa.EJIb7IdGYq',NULL,'2015-05-14 01:10:26','2015-05-14 01:10:26','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('105','AsianDevelopmentBank','rogermarkowski@hotmail.com','$2y$10$tTlUlOXIV/tZB12al3.ZleHpXuCm2ADY1u.Hm8W8aRRgOAWBsmByO','G0S2WdDRgQSnBq52JqmpnkDLP6G8KhpgzyIXV7q5n1WtZoQRf0HAW74Y4QeR','2015-07-09 17:29:09','2015-07-10 00:29:09','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('106','Amnesty','rogermarkowski@hotmail.com','$2y$10$yVmL9q0WejAPUMtjxJe0p.x7ibZMsVih/fXOylhEHLjsd/r0QEXGO','g2Sw3EBFCyOSbFpMiuvqSHLMN38CD8tmTvDHsXUVmYEMkJuvJ7RtEjhOj6nJ','2015-07-09 00:28:02','2015-07-09 07:28:02','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('107','AusAid-Demo','rogermarkowski@hotmail.com','$2y$10$liudDwGkOToyU1pVLfAfVOeHeD8kQHprKBLbSpNdrqCpho9te50zu',NULL,'2015-05-19 03:18:25','2015-05-19 03:18:25','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('108','AusAiddemo','rogermarkowski@hotmail.com','$2y$10$U5rpCERVdJ3YFQNtOOVxZ.gIjiZutsV0VMQC/0Wv7GHlSOsT6ST1a','NysGfasxaEKVEFxLxnPRK96VwehH68BMmhR5dtrYcbog1JBvPIFuYxdifrwl','2015-07-09 17:07:23','2015-07-10 00:07:23','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('109','Takehido-demo','rogermarkowski@hotmail.com','$2y$10$XBfkltMhu4u4PKF41miYRuMEBc..s225ZzLYaKqW9I/fu2D6aMhUu','bas6l2ozpoBlvgQJMkY1eyjJeXJWO6RiLaWa1M0C6k9sl2AbASgcIshlOmc8','2015-07-09 16:39:04','2015-07-09 23:39:04','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('110','ACFinternationaldemo','rogermarkowski@hotmail.com','$2y$10$hToxQI5mchnf1RRV1.5aTeUDC4EURacofjqpetlKJs834jEQHQNYK','R0USUYmmCcH7x8DPp3lAoKopmyCkX5x7R99MUNJ9s6cpA8wadnQpfVDPgYAK','2015-07-09 17:37:17','2015-07-10 00:37:17','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('111','CAREinternationaldemo','rogermarkowski@hotmail.com','$2y$10$O7T6Zo55j9b2p2rnrBKQ7.ni.Ztx0sUtzfJr869exGfjE1JAQHhVO','dSgJDC8Dp0JWkFNA0Z14vZmT8tLdvt5nOK7853lTWIYSsj26zjkCFnnV7HvM','2015-07-09 17:06:41','2015-07-10 00:06:41','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('112','Caritasdemo','rogermarkowski@hotmail.com','$2y$10$kE1YYzOlrI4s/11Q.hjoSebPOfP44DKas6iOtG9QnfESCNyEeL0RW','yX2nwTmf0FHbk3GvDkbQB1Jos5RwHrat44uI1k5yHFH8tPhFJJQoQlTftEuV','2015-07-09 17:05:59','2015-07-10 00:05:59','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('113','DFIDdemo','rogermarkowski@hotmail.com','$2y$10$XbSZZkBnGszDAOgMGLvwWOTiB42jfxGWM9IQy135lb1W9o3AjOUvO','Uc3cAOzRhENL01TWXBi8QpWBYdbI6aSMOFuZDHN4dEiRgAxiZxCpXt4WjVxc','2015-07-09 16:57:32','2015-07-09 23:57:32','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('114','FAOdemo','rogermarkowski@hotmail.com','$2y$10$YXvL9Z/e6QK2Zc18QExK7OUC4WQ.SzpxJcHBr./neHmk/Vqf26.YW','OELuIs72PCjh91w9oxJ0qre6ZMMWpibn3j30BusA9yTzr9DidoxbaDnqcfjE','2015-08-14 02:27:12','2015-08-14 09:27:12','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('115','FATDCdemo','rogermarkowski@hotmail.com','$2y$10$HhaabbfVbPuyIAy0UjCVa.dH1W5n36huLHqWhbDx8R41.i1Fjf7uG','5KSERqH8PcR6EIy99eZg5qyJw0IbxtTyTVbyY35Ap2Hnn7sfiOfQX6BRHV1W','2015-07-24 00:49:55','2015-07-24 07:49:55','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('116','Greenpeacedemo','rogermarkowski@hotmail.com','$2y$10$tgnj6QP96VFBuJnLytHTx.m4/H/tyH6U5JhLElWuK70ItT7F443GC','Lkpjd9cYgGJ5Z3eXLLrAtSLEZHwa1CrGVQcKsVqjO375NpAwxqhUgVxNg8Vz','2015-08-06 06:11:47','2015-08-06 13:11:47','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('117','ICRCdemo','rogermarkowski@hotmail.com','$2y$10$QCYX5e3MUKplX.kzmf7k2ewgECt5x4VM9Diqk4.9MBCN56S6UXLea','OC0mLlCCo9DXb8QDgq2tZ5eZ9ipAExAa2t8pHNQxhK7BJkbsDXCrSDjyUA9y','2015-08-14 17:39:00','2015-08-15 00:39:00','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('118','MDMdemo','rogermarkowski@hotmail.com','$2y$10$bIbJcvOCmoQgh.PRPveucufB7bX.awi7gKUltVm6o6auCnbyWHWra','28ECI4sApD5caj0IJWt5jHeV6LauHuTgoKPEyRbLLAN66D1K6bBl2P3faLsE','2015-08-06 17:01:20','2015-08-07 00:01:20','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('119','MSFdemo','rogermarkowski@hotmail.com','$2y$10$OtZ97gBrsLaN.vjVhycZQODLt0D8/aabFw.mfYqWnzIwUYBLSCMJ2','448lvd93CmYDJfX0qCXeQkEOljWpCfP2XR4l9SRVIe5SGu0x3YhdnqJ80E1e','2015-08-06 16:51:33','2015-08-06 23:51:33','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('120','IOMdemo','rogermarkowski@hotmail.com','$2y$10$PaSxW85R5dTg4zHUY0eqIOWP9Jlf9HgMej4MQ/RSmHm1I3c5oxy.O','ROgaI6pe0haHSAbEFBZmD2oi2DXkUxKtMm3dpPuP6ePQKp4Uyj0xyQsS2XWn','2015-07-09 16:58:44','2015-07-09 23:58:44','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('121','Oxfamdemo','rogermarkowski@hotmail.com','$2y$10$DR0IT4WIz9ODmar.aLCf2eRWcvswKF3EPqVra59ygF6wnme13xHdm','bWr6uUDasqCJVgh9Z7TUPzNVWrKwk9MgUuM17eaCw0HqSc5dyhsOsXwXVa9P','2015-07-09 17:01:35','2015-07-10 00:01:35','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('122','PADFdemo','rogermarkowski@hotmail.com','$2y$10$u5dohg.AtfNSDIpDeBlR1.kG3iJlXB6wPKd.UTVelDzWk8afLg1k.','DtQLldz5Ruq68d1HTA9GJ6Smrn0ZD1JNCb50s3JoRd01e9CuUcAJrHcbj39J','2015-07-09 17:00:44','2015-07-10 00:00:44','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('123','PLANdemo','rogermarkowski@hotmail.com','$2y$10$Q8.QmQrGwh2yyXGtzd5.we2Lw7ja5ql1ycgj3I8IkKhNskFlXCEjy','0IHbrhnub6uEQOyRKgUlTm9ovef4hB4LYtE7LCaaWhrwT8InCLmQMD7GGVCy','2015-08-06 06:13:50','2015-08-06 13:13:50','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('124','UNDPdemo','rogermarkowski@hotmail.com','$2y$10$p4HXw3HMp/dbzucUFBU5Ke70aBQKMHsds61FgfnAeJ9eSCb8qHlNC','Gy5qrVNOZMyBMQImt1ZHiot8D9N1oOkgksphyHq8AqB0ZgU30LLgTVqCBz7h','2015-08-14 18:31:40','2015-08-15 01:31:40','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('125','UNICEFdemo','rogermarkowski@hotmail.com','$2y$10$jYkyqlQFCAoGk01mReUVauwMYgB./nReAJq6SfiPfTCs5eH/ObKfO','gGyrMYUzogX8zFAKjfgJcZqTcnaCI060MrzWIiUTWTWbfrhJafo8VjhVYkHq','2015-07-17 22:55:02','2015-07-18 05:55:02','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('126','UNILEVERdemo','rogermarkowski@hotmail.com','$2y$10$qr35J3MnxBC.Nsow5GiowuUGNYN2GRS8wYiAluesxngDhTLF/ivV6','MZhHvsSt2OdHOEa1UHJdX8JiE2oo2Z3ERvwVjYEwZbwy7Pxa8z5e44z41mii','2015-07-09 16:56:35','2015-07-09 23:56:35','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('127','USAIDdemo','rogermarkowski@hotmail.com','$2y$10$4tt0muL0zLCr7v6e3ouScetKChuCEoDR0l3vmJn3iog7x0.HBytMG','Pvyy8eDPWZxCgGuNPQkDAjXlUF371kJvbFCy66q3EhnS4sWiQvuKfBFGtgZV','2015-08-01 23:49:37','2015-08-02 06:49:37','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('128','WorldBankdemo','rogermarkowski@hotmail.com','$2y$10$wted/rNFH.nJCPUR8357peHCPa7lM0Tjo5doX1JoUIVuLpbU68qz2','IspvBNBNvqLJvwUbKnwyIwZG5ASSWlrl1euBkyeupit9dgZslm5vNKHdNCGO','2015-07-16 18:11:11','2015-07-17 01:11:11','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('129','WHOdemo','rogermarkowski@hotmail.com','$2y$10$BbRVW2Yxskiv0jGvwRsoS.mZk7.AwfKmQ5whg9.Bi8l06T06LFxzK','Rp2dJv1paIVsTrn9WAoJrwXywGy68t0ZpKxgVYvUohZCmuHw7hPaeNrhEN59','2015-07-09 16:51:54','2015-07-09 23:51:54','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('130','WWFdemo','rogermarkowski@hotmail.com','$2y$10$A97F/Y.bnuq0XoN1Cqx19u8hc2N9slT7VSylU9MPjencPben9jOOC','5SQvbR7pYzwyfHFiKGPfq7uzbdp8Jxa2aJnkedjtcX9Bssl0wDvvYafR3xG8','2015-07-09 16:49:36','2015-07-09 23:49:36','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('131','Chafetz-demo','rogermarkowski@hotmail.com','$2y$10$pvxkb5p86m4JRwIl2t6wmew089IPt1CmpXmJPO2M4oesUiCd4lK6m','0iUUDYTsjd4b8DkOTapz3ucvOH9pC6rsA3LWvWelPMZhOrxiiXH32b1A1mAf','2015-07-13 16:52:09','2015-07-13 23:52:09','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('132','Nesbitt-demo','rogermarkowski@hotmail.com','$2y$10$detR1De/jOZBNn3dsTzQ2ONRdMaD8ZHZMn5tivP0eHf6f.4fj8CP6','pwCA6sqSqsWqT8Ghb1wN7qprTrfTkM060McQQxmNtrT1tuoobdaKo9tPjuyZ','2015-08-14 01:55:42','2015-08-14 08:55:42','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('133','WTOdemo','rogermarkowski@hotmail.com','$2y$10$cPTSWZkofhHOuIdJUtyLl.ZXgMRRehUDgubZAvHUEQG/wAQGyg.1W','RyuwwhmijrLua45dEHThrR20HkICXo9pwy2kVuKdIbwbM2idUENHXTSiMTk1','2015-07-09 16:50:50','2015-07-09 23:50:50','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('134','GMTdemo','rogermarkowski@hotmail.com','$2y$10$9OGQCHcrgED5FCAYQf0vbeMjFoZpIDvq/vKQJ3Ln0rESHWBv2KUFq',NULL,'2015-05-20 23:58:43','2015-05-21 06:58:43','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('135','NYLUNDdemo','rogermarkowski@hotmail.com','$2y$10$JDD7XYLri7BvT6o4fCLRruqGS9bxBSOnQYidCqT4E3H6PemEHrGBi','HpaN7Es0MejUK6i1xhThqea0OpQuWX6GVnWNUz9ifhAvjettbFH895sLAVrG','2015-08-12 16:23:56','2015-08-12 23:23:56','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('136','fleava','haritianpinio@gmail.com','$2y$10$Y8JmSWuL25ipVOdnAuY33e/G0g6l4wDa2/mJKsCTBVvKSJ1riLGZq','E9Cb6dFovONVsgxtlvJuTXyszgJzlmwHj7J1yZgOVKobCHX5XOAvxEyTEKnH','2015-07-07 02:58:16','2015-07-07 09:58:16','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('137','testing150525','rogermarkowski@hotmail.com','$2y$10$6ZxD7UMJPfwY4rhskQ6Dkubj1pBlG5l6yzq..WuS7wTsIrGahStwO',NULL,'2015-05-25 19:14:02','2015-05-26 02:14:02','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('138','aidwebtesting 5','rogermarkowski@hotmail.com','$2y$10$pMR5ZUBuFP7TbrgJnSATveifeR8wkAN4hUQbjq4RsP5DLwR1QQKc.',NULL,'2015-05-25 19:14:42','2015-05-26 02:14:42','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('139','PopopJojo','rogermarkowski@hotmail.com','$2y$10$Mxte5FvURJaNhggvQc6gPeCCcg0zObBb17NFU40dfHiHYl0kTYwGy',NULL,'2015-06-13 01:51:02','2015-06-13 08:51:02','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('140','Testjune','rogermarkowski@hotmail.com','$2y$10$hLknsCClaRh7H3Mfc/Lb7uJ2As1UNbJvzFwgs8zZXOPi745vEYWzC',NULL,'2015-07-07 03:00:02','2015-06-28 23:16:15','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('141','Testjune2','rogermarkowski@hotmail.com','$2y$10$OqQVNQjeye8yZDcIolS9ge3RBGxIEgb72M9JJcWVcd3t8VOjXom5m',NULL,'2015-06-28 14:27:35','2015-06-28 21:27:35','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('142','UNICEFtest','rogermarkow@gmail.com','$2y$10$e..m.xLKFmZyqiPLNZWHiuiy8rn/2/vTIoE3nvmjvjFPqpEqq9MRW',NULL,'2015-07-07 03:00:06','2015-06-28 22:52:35','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('143','hpinio','haritianpinio@gmail.com','$2y$10$8RWblyvytA3UGL7ZEybgvuEGGlfQ/zjBySMlGn8VPZs22nzv2rTEq',NULL,'2015-07-01 00:37:25','2015-07-01 07:37:25','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('144','deSouza','rogermarkowski@hotmail.com','$2y$10$xbzUvEw/WXVAJHTdatQYY.AeITrxxMRWrZ8lepkZU/EA6NsCC0hp6',NULL,'2015-07-04 13:24:23','2015-07-04 13:24:23','1','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('145','Sabrina','rogermarkow@gmail.com','$2y$10$trxdZsfLEUJSgStmYkKdgOCwznbmpWYPW1UQSIvSzBPgJwhcFYVXW','b4uwyQ9z4NWu1MantaUCiSoNyRjaYL7IwzYiA4ojqVaCbKVqYpaLpmMK6atw','2015-07-09 16:38:53','2015-07-09 23:38:53','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('146','WVdemo','rogermarkow@gmail.com','$2y$10$JnxDBYBRvRJb0F64TW5/s.OtQz52SaGF3guWduxUdrsYVQfnuI2LK','7zzV9q3yOaB2oMMRyrSvHU5w2jDdR2bt2bOelAomuX8JWpHA4UpbcRpc6TeO','2015-07-09 16:48:56','2015-07-09 23:48:56','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('147','ORG bidon test','rogermarkow@gmail.com','$2y$10$YNjMOnKwLD9jEWzVzDfc8.NmGGaN6iQcIvQLehUvnMtOAKEsVxmz2','yee4DHaYwdrEMmwB9POgYGWjJJirQxBCF7YBhQ1tvmNVGD7Gj5rqv5REdbWc','2015-07-09 01:52:20','2015-07-09 08:52:20','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('148','ORG Bidon 2','rogermarkow@gmail.com','$2y$10$dGa9AamKBr0GGSrk4Kbrf.reQEoLI7i/p/CteGG1xrsqhR.L2jWvG',NULL,'2015-07-09 01:59:09','2015-07-09 08:59:09','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('149','IND BIDON','rogermarkow@gmail.com','$2y$10$WDWegb6Oq3.5A5CBc0EnnewoZfFwEUxnYy3LLeMd5uBkmisKkMItS','ULnyB6ZPu06BBrBEWeoP45lKpFZOyb3Lpdcm5cyretbFWnezkcg04iAGvG0A','2015-07-09 02:37:58','2015-07-09 09:37:58','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('150','Jamaandemo','rogermarkow@gmail.com','$2y$10$kbjLGHkXrJLH3oAl8CKIBOEkJ/wnkOHTQ.o2z9rXveacTSvOqP7YO','E41bRZL5PdWfrfT0W3hy5yheCrPnoXUI0mpwo9VkG2pjD3mU5NMEm4jlIy9T','2015-07-09 18:30:50','2015-07-10 01:30:50','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('151','Chiarademo','rogermarkow@gmail.com','$2y$10$X4a3jyN9bawIcjGKetyfcu84przairfoBCHbxH/qC51bik4eltQzq','Ec8iUoep80LqvPQ9WkEldWeaijUfgsfSUlVmLHvHsjXjE1cnh07KIYWbr9hA','2015-07-09 19:19:50','2015-07-10 02:19:50','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('152','Blommedemo','rogermarkow@gmail.com','$2y$10$EmDL3pb38Md/A8wXYELa9.sX3g17YScBW6Mow34AvlSysNTvmhqc.','yP7xyEY0Ro5EISjaKB2yPQfLUR6rDzZDqFYiAj0yk3XxR0YLJWHGlmHBYHDn','2015-07-21 23:10:54','2015-07-22 06:10:54','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('153','Byanyimademo','rogermarkow@gmail.com','$2y$10$kc.C19OXxZI05ys4NzlJuujJZEJxMI12HVx6XKCBuFVXIHAFJDb/K','sNFUbsw6WRCH0YXxSvFaUZkbVX9Axg5hduZMTN5Z885ZX8qwkqLrKCUtsUe5','2015-07-09 22:47:58','2015-07-10 05:47:58','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('154','ORGTESTdemo','rogermarkow@gmail.com','$2y$10$EXgcezJ18U.K5lq1QqD6ReIrNauL2E9l21HVqd3tV.ZEgEauZp8lm','MrbDFe8eYexp1o0SKMq4twj7ysOSSMDCCVxBeWn91V0V3yiYSZotGpensiEN','2015-07-17 23:03:28','2015-07-18 06:03:28','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('155','INDTESTdemo','rogermarkow@gmail.com','$2y$10$lx8Z.KFiqY7WZyNnE6sHA.JT1wYVmdIK.9ywGGcdNCkO8FXLS4N0u','Qpxx7CYI3Zy8D9H2DC1hPTMXFm81jtnnBvw9hbhhHUI8ndpRRycELxDNs6i6','2015-07-10 19:23:41','2015-07-11 02:23:41','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('156','ORGTEST2demo','rogermarkowski@hotmail.com','$2y$10$wgZtrwdaLq/zHpbLyr308eyzLz7s7h8S9TXG8Qipz3WnEh9OZUmni',NULL,'2015-07-11 05:49:17','2015-07-11 05:49:17','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('157','ORGTEST3demo','rogermarkow@gmail.com','$2y$10$W2RFhWs9pJMjRdG/0qIIV.srQtFO0EDNEdg592vZV0RukusB6hrxq',NULL,'2015-07-11 05:52:26','2015-07-11 05:52:26','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('158','ORGTEST5demo','rogermarkow@gmail.com','$2y$10$/4pyMnIWi9b2xufldneal.7tRgK.yesUpyt5dr8exg0B07Nj1u51O',NULL,'2015-07-29 00:00:43','2015-07-29 07:00:43','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('159','ORGTEST6demo','rogermarkow@gmail.com','$2y$10$Enp/.Rib1k1FIG4biXy5Z./8tBI7GMPbgfkF1HCmlGEzItEgRXE/C',NULL,'2015-07-11 07:57:00','2015-07-11 07:57:00','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('160','ORGTEST10demo','rogermarkow@gmail.com','$2y$10$rtR/F7/s9h6nEylFENQWu.nCPsklj2stHpf/OtVCDYGawjtc9w6Pm',NULL,'2015-07-26 05:04:10','2015-07-26 12:04:10','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('161','haritian.pinio','haritianpinio@gmail.com','$2y$10$x8Rmlq6twrrBYeyWCQOMv.t.ooPu.nXKJLgO1P9fk2mZ88jeYdnYK','4vthj0IINfw09Ib5jrQqzoTsc7r8YzgRgVf0bq7TN0u4bumyeMR9YmdPnCjT','2015-07-12 23:38:08','2015-07-13 06:38:08','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('162','INDTEST5demo','rogermarkow@gmail.com','$2y$10$vp3vmPSvRYJLup0z.nj2k.K3pr04AsiqXhXZ3LqmYF4Mo.TQXSr5u',NULL,'2015-07-13 16:02:55','2015-07-13 23:02:55','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('163','haritianpinio','haritianpinio@gmail.com','$2y$10$jOG3O7ECUG6eYaoijlZ7WeQJBU.NpRCz.XUZzqnXBuThc6rljXZZa',NULL,'2015-07-13 22:31:22','2015-07-14 05:31:22','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('164','INDTEST11demo','rogermarkow@gmail.com','$2y$10$B4PS665DXRmf2Fz0xC9G3uWx/OixOoaRSaw03vBh08FMwt72TbGYS','KxSGaiymJGNzdYDv8xl5WLsHJDEsWqzsI2zz7d9bXLhG5xh2z8zkMQBiPHAO','2015-07-26 05:09:30','2015-07-26 12:09:30','1','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('165','AERdemo','rogermarkow@gmail.com','$2y$10$qt9HJHhjXcGHJXUlTjKcIe1ANMScvRnSaX.ZSewluzu236V4gJSxi','Jnt2z7bePJUxZTvwPUuaYO924GB99GwaCBTYLrAmYSbMxm97lduYCbNi7n5Z','2015-07-22 21:14:51','2015-07-23 04:14:51','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('166','AYALAdemo','rogermarkow@gmail.com','$2y$10$55FdYiuCHrcDUK5b5zE7AOkDAHxhgK9mORP2rr7oxIKYPP.IMPw.W','Kuqe3xvSoMksJPjMIPPvlbwQeyXIZ13Y6xMqxpcffk29otAXpKEvrqKFLugP','2015-07-22 21:39:49','2015-07-23 04:39:49','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('167','AFAdemo','rogermarkow@gmail.com','$2y$10$c73lhurXiAYHedOlsGk66.9OXhSYunWOMipBMnh0OEY1ojEkMXKLS','aBV3XWsAM7bTvALyMazF20117n6ExNTya5go4decfYUlK8rL9oCnH33sFOnB','2015-07-23 20:56:05','2015-07-24 03:56:05','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('168','Test20demo','rogermarkow@gmail.com','$2y$10$S6m7yy396ckWinbYo7nu1edftz4QJn1Zq84YhE6Nt8/9QnSb.WUUq',NULL,'2015-07-24 15:38:49','2015-07-24 22:38:49','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('169','TESTORG25demo','rogermarkow@gmail.com','$2y$10$bLOtqkKsU5ApCXxc87efROoRMXXx4mtf3jgHoE/.c4MVQFdHY5ryS','n1qkl1bMGgqK1HPT8u5ggQd65CB9xnGiitgVbvGUqUyLXWfUwxErEikIB3sD','2015-07-29 00:00:25','2015-07-29 07:00:25','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('170','indtest1','rogermarkowski@hotmail.com','$2y$10$uqL0N5Sx9NQBYWehJid24eE89rNOOPdMu2.AOzJ5aAV3CDAno1TJy','eDfzaewcnfpOtcFiZOtBtwL4Ms408Rb3sTqY1oiJM6XOvYId1l9wacjr2ek5','2015-08-12 16:21:52','2015-08-12 23:21:52','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('171','orgtest1','rogermarkowski@hotmail.com','$2y$10$jgmwmbTlxpCBegKb8sqA/edxDamCkLT8rvuqdX0n6VR1tHCRCKH8i','q6IK6V2zbO8aNl6zWENsIb9Z6q8yXwMRH8FCPLnOOYuz8KNosDvkKsmO8gon','2015-08-11 18:06:05','2015-08-12 01:06:05','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('172','Kiddodemo','rogermarkowski@hotmail.com','$2y$10$zUE82yzHS2lqAxmbBCyZ1u6YQkYCfUiGcw5YXiNX2gk1IkuumxcWG','x0FgEYmVusaze3wVDEbBoKL9XneJmBLG6JMpsJgrHWFdVkyjrZzZ8L2uBIjn','2015-08-12 16:54:42','2015-08-12 23:54:42','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('173','ORGTEST20demo','rogermarkowski@hotmil.com','$2y$10$s3IfR4/A2n8SrjFgtQeD.uti65.jHWsxTvkzVodoeAIJDvBYka64m',NULL,'2015-08-12 23:42:38','2015-08-12 23:42:38','2','0','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('174','ORGTEST30demo','rogermarkowski@hotmail.com','$2y$10$YtmACDbILy2JHqmbBWz6bekXqxt8dE3OupqSokBufmcCxMwiXcKxe','l9UhGnKmEQGqhFbxByasbmVv2dZXlCh8vkXPR62e1SShyIRdKX623yYABgBk','2015-08-12 17:46:24','2015-08-13 00:46:24','2','1','1');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('175','ORG40demo','rogermarkowski@hotmail.com','$2y$10$.aM3ZQ20mn8.0OZJS6BZeuP5JNm7PAEjRFNGfjSYTY5CoffF6s/DO',NULL,'2015-08-12 17:59:56','2015-08-13 00:59:56','2','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('176','individual','hieupv.est@gmail.com','$2y$10$h8/N69hkn2Ihz2VuY3cwRuEEr/RTx2f9t40zaDgRMFboK3YxBSO8S',NULL,'2015-08-14 18:26:33','2015-08-15 01:26:33','1','1','0');
INSERT INTO `users` (`id`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`profile_type`,`verified`,`is_new`) VALUES ('177','organisation','phamvanhieu861992@gmail.com','$2y$10$fTdXC2UTLEzyf4Xa06Hs8OJQsVWW1xImFenPj9WXl5VLZ0RbCdSFa',NULL,'2015-08-14 18:33:11','2015-08-15 01:33:11','2','1','0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


--
-- Create Table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `videos_organisation_id_index` (`organisation_id`),
  KEY `videos_user_profile_id_index` (`user_profile_id`),
  KEY `videos_user_id_index` (`user_id`),
  CONSTRAINT `videos_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `videos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `videos_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos`
--

/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('31','Solidarity with Syria ','solidarity-with-syria','Caritas photographers include Evert-Jan Daniels and Jos de Voogd, Patrick Nicholson, Laura Sheahen, plus Caritas Turkey, Caritas Jordan and Caritas Lebanon staff. ','http://img.youtube.com/vi/menGqrnbc2s/hqdefault.jpg','http://https://www.youtube.com/watch?v=menGqrnbc2s','menGqrnbc2s','1','0','1','2015-05-20 01:40:14','2015-05-20 01:40:14','60','112','112','The Caritas confederation responds to the Syria crisis through the work of Caritas Syria, and to members in the region who are helping in Syria, Lebanon, Turkey and Jordan with food, shelter, medical supplies, healthcare, education and finding employment in combined programmes of nearly €15 million. Those programmes have been focused over the last few months on helping Syrians survive the winter, but will be extended as we move into spring and summer. Caritas provides aid to Syrians regardless of their political or religious beliefs.\r\n','PT3M9S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('32','World Food Day 2012: Action Against Hunger in DRC ','world-food-day-2012-action-against-hunger-in-drc','','http://img.youtube.com/vi/79DlGX01b8M/hqdefault.jpg','http://https://www.youtube.com/watch?v=79DlGX01b8M','79DlGX01b8M','1','0','1','2015-05-20 01:57:22','2015-06-29 11:11:17','58','110','110','On world food Day 2012, see how aid from the UK is tackling malnutrition in the heart of Africa, with the NGO Action Against Hunger.','PT5M18S','3');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('33','Action Against Hunger - The Sharing Experiment ','action-against-hunger-the-sharing-experiment','','http://img.youtube.com/vi/zFTspq_nzG4/hqdefault.jpg','http://https://www.youtube.com/watch?v=zFTspq_nzG4','zFTspq_nzG4','1','0','1','2015-05-21 01:56:56','2015-05-21 01:56:56','58','110','110','Just by watching this video you are donating to this non-profit, thanks to our advertisers. So why not support the fight and send it to a few ','PT1M49S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('34','Asian Development Bank Corporate Video 2014 ','asian-development-bank-corporate-video-2014','','http://img.youtube.com/vi/U50S76LNwxQ/hqdefault.jpg','http://https://www.youtube.com/watch?v=U50S76LNwxQ','U50S76LNwxQ','1','0','1','2015-05-21 02:05:10','2015-05-21 02:05:10','54','105','105','Fighting Poverty in Asia and the Pacific. This video serves as an introduction to ADB\'s work and its impact. It highlights continuing challenges faced in the region and Strategy 2020: ADB\'s effort to address these challenges.','PT3M8S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('35','Amnesty International\"Pens\"- Commercial 2014 ','amnesty-internationalpens-commercial-2014','Directed by Onur Senturk and co-produced by Troublemakers.tv and the studio One More','http://img.youtube.com/vi/NbqDjTx3QoA/hqdefault.jpg','http://https://www.youtube.com/watch?v=NbqDjTx3QoA','NbqDjTx3QoA','1','0','1','2015-05-21 02:10:43','2015-07-11 23:45:58','55','106','106','Pens is a full CG film shot in motion capture that epitomises the potential of each signature in the defence of human rights. Highlighting that the voice of a single person can bring about the birth of a social movement, the film concludes with the words \"your signature is more powerful than you think\" and features the song Iron Sky by Paolo Nutini. Through powerful imagery, Amnesty International has encapsulated the essence of its primary objective -- inspiring communities to defend their fundamental human rights.','PT2M','2');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('36','AusAID combats PNG tribal violence ','ausaid-combats-png-tribal-violence','ABC News (Australia)','http://img.youtube.com/vi/qqa6aWqSqgA/hqdefault.jpg','http://https://www.youtube.com/watch?v=qqa6aWqSqgA','qqa6aWqSqgA','1','0','1','2015-05-21 04:05:16','2015-05-21 04:08:42','57','108','108','Former Queensland detective Don Hurrell is establishing peace monitors in Papua New Guinea\'s highlands as part of an AusAID-funded project to reduce tribal fighting.','PT37M30S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('37','Kore Lavi\'s food voucher and school feeding program ','kore-lavis-food-voucher-and-school-feeding-program','','http://img.youtube.com/vi/TdtJaY9sEW4/hqdefault.jpg','http://https://www.youtube.com/watch?v=TdtJaY9sEW4','TdtJaY9sEW4','1','0','1','2015-05-21 04:13:54','2015-05-21 04:13:54','59','111','111','CARE Haiti provides a short video describing the work of the Kore Lavi program.','PT4M36S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('39','Providing not only crops in Africa, but knowledge ','providing-not-only-crops-in-africa-but-knowledge','ROME REPORTS, www.romereports.com, is an independent international TV News Agency based in Rome covering the activity of the Pope, the life of the Vatican and current social, cultural and religious debates. Reporting on the Catholic Church requires proxim','http://img.youtube.com/vi/167W--oeU6o/hqdefault.jpg','http://https://www.youtube.com/watch?v=167W--oeU6o','167W--oeU6o','1','0','1','2015-05-21 04:24:19','2015-06-28 23:52:13','60','112','112','','PT2M19S','2');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('40','DFID\'s Bob Gibbons oversees UK aid being loaded on to a RAF C130 bound for Iraq ','dfids-bob-gibbons-oversees-uk-aid-being-loaded-on-to-a-raf-c130-bound-for-iraq','https://www.youtube.com/watch?v=SWQI-JmA4V0','http://img.youtube.com/vi/SWQI-JmA4V0/hqdefault.jpg','http://https://www.youtube.com/watch?v=SWQI-JmA4V0','SWQI-JmA4V0','1','0','1','2015-05-21 04:50:48','2015-05-21 04:50:48','62','114','114','DFID Humanitarian Programme Manager Bob Gibbons speaks from Brize Norton, one of the Royal Air Force\'s bases in Oxfordshire, where he is helping to load lifesaving aid onto a RAF Hercules C130 aircraft, which is bound for Iraq.','PT1M29S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('41','Food wastage footprint ','food-wastage-footprint','Produced by Nadia El-Hage Scialabba, Art Director Francesca Lucci','http://img.youtube.com/vi/IoCVrkcaH6Q/hqdefault.jpg','http://https://www.youtube.com/watch?v=IoCVrkcaH6Q','IoCVrkcaH6Q','1','0','1','2015-05-21 04:54:54','2015-07-09 09:37:02','62','114','114','Based on the findings of the Food Wastage Footprint project of the Natural Resources Management and Environment Department kindly funded by Germany.','PT3M16S','1');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('42','UNDP\'s Year In Review 2014 ','undps-year-in-review-2014','','http://img.youtube.com/vi/2ByKL5mLBn8/hqdefault.jpg','http://https://www.youtube.com/watch?v=2ByKL5mLBn8','2ByKL5mLBn8','1','0','1','2015-05-21 04:59:04','2015-05-21 04:59:04','72','124','124','2014 was a year of huge humanitarian crises. From the conflict in Syria to the Ebola outbreak in West Africa, from extreme weather events to democratic elections, the United Nations Development Programme was at the forefront of many of the challenges faced by the most vulnerable and exposed people in some of the poorest countries in the world.','PT6M4S','0');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('43','Katy Perry - Unconditionally | UNICEF Goodwill Ambassador | UNICEF ','katy-perry-unconditionally-unicef-goodwill-ambassador-unicef','','http://img.youtube.com/vi/Azma-bPeD7o/hqdefault.jpg','http://https://www.youtube.com/watch?v=Azma-bPeD7o','Azma-bPeD7o','1','0','1','2015-05-21 05:03:02','2015-07-08 23:22:29','73','125','125','Welcome Katy Perry to the UNICEF Family.','PT3M49S','1');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('44','USAID\'s WINNER Project in Hait','usaids-winner-project-in-hait','Janice Laurente','http://img.youtube.com/vi/13WAUvr-8DI/hqdefault.jpg','https://www.youtube.com/watch?v=13WAUvr-8DI','13WAUvr-8DI','1','0','1','2015-05-21 05:05:38','2015-08-15 01:42:10','75','127','127','USAID\'s WINNER Project helps improve the lives of Haitians. The project supports the country\'s agriculture sector which employs approximately 60 percent of Haitians. It also helps protects natural resources and reduces risks from natural disasters like flood control. ','PT14M58S','6');
INSERT INTO `videos` (`id`,`title`,`slug`,`author`,`image`,`url`,`youtube_id`,`published`,`deleted`,`featured`,`created_at`,`updated_at`,`organisation_id`,`user_profile_id`,`user_id`,`description`,`duration`,`view_count`) VALUES ('45','Cambodia\'s International Trade ','cambodias-international-trade','','http://img.youtube.com/vi/DG7n9d5xdB8/hqdefault.jpg','http://https://www.youtube.com/watch?v=DG7n9d5xdB8&index=3&list=PLgNWEgELFnC41ttL9totkDpcRYQxml6lT','DG7n9d5xdB8','1','0','1','2015-05-21 05:22:38','2015-06-26 07:46:28','79','133','133','Dr. Sok Siphana with Dr. Friedrich Von Kirchbach on Cambodia\'s International Trade ','PT30M36S','1');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;


--
-- Create Table `videos_beneficiaries`
--

DROP TABLE IF EXISTS `videos_beneficiaries`;
CREATE TABLE `videos_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_beneficiaries_video_id_index` (`video_id`),
  KEY `videos_beneficiaries_beneficiary_id_index` (`beneficiary_id`),
  CONSTRAINT `videos_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  CONSTRAINT `videos_beneficiaries_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_beneficiaries`
--

/*!40000 ALTER TABLE `videos_beneficiaries` DISABLE KEYS */;
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('31','31','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('32','32','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('33','33','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('34','34','17','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('35','35','19','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('37','36','26','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('38','37','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('40','39','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('41','40','26','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('42','41','17','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('43','42','31','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('44','43','17','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('46','45','8','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_beneficiaries` (`id`,`video_id`,`beneficiary_id`,`created_at`,`updated_at`) VALUES ('47','44','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `videos_beneficiaries` ENABLE KEYS */;


--
-- Create Table `videos_countries`
--

DROP TABLE IF EXISTS `videos_countries`;
CREATE TABLE `videos_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_countries_video_id_index` (`video_id`),
  KEY `videos_countries_country_id_index` (`country_id`),
  CONSTRAINT `videos_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `videos_countries_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_countries`
--

/*!40000 ALTER TABLE `videos_countries` DISABLE KEYS */;
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('31','31','207','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('32','32','50','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('33','33','224','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('35','36','167','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('36','37','94','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('38','40','104','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('40','45','36','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_countries` (`id`,`video_id`,`country_id`,`created_at`,`updated_at`) VALUES ('41','44','94','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `videos_countries` ENABLE KEYS */;


--
-- Create Table `videos_crisis`
--

DROP TABLE IF EXISTS `videos_crisis`;
CREATE TABLE `videos_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_crisis_video_id_index` (`video_id`),
  KEY `videos_crisis_crisis_id_index` (`crisis_id`),
  CONSTRAINT `videos_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  CONSTRAINT `videos_crisis_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_crisis`
--

/*!40000 ALTER TABLE `videos_crisis` DISABLE KEYS */;
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('31','31','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('32','32','32','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('33','34','47','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('35','36','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('36','37','33','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('38','39','47','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('39','40','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('40','41','47','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('42','45','47','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_crisis` (`id`,`video_id`,`crisis_id`,`created_at`,`updated_at`) VALUES ('43','44','33','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `videos_crisis` ENABLE KEYS */;


--
-- Create Table `videos_interventions`
--

DROP TABLE IF EXISTS `videos_interventions`;
CREATE TABLE `videos_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_interventions_video_id_index` (`video_id`),
  KEY `videos_interventions_intervention_id_index` (`intervention_id`),
  CONSTRAINT `videos_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  CONSTRAINT `videos_interventions_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_interventions`
--

/*!40000 ALTER TABLE `videos_interventions` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos_interventions` ENABLE KEYS */;


--
-- Create Table `videos_regions`
--

DROP TABLE IF EXISTS `videos_regions`;
CREATE TABLE `videos_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_regions_video_id_index` (`video_id`),
  KEY `videos_regions_region_id_index` (`region_id`),
  CONSTRAINT `videos_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`),
  CONSTRAINT `videos_regions_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_regions`
--

/*!40000 ALTER TABLE `videos_regions` DISABLE KEYS */;
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('31','31','5','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('32','32','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('33','33','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('34','34','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('35','35','3','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('37','36','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('38','37','4','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('40','39','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('41','40','5','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('42','41','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('43','42','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('44','43','7','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('46','45','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_regions` (`id`,`video_id`,`region_id`,`created_at`,`updated_at`) VALUES ('47','44','4','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `videos_regions` ENABLE KEYS */;


--
-- Create Table `videos_sectors`
--

DROP TABLE IF EXISTS `videos_sectors`;
CREATE TABLE `videos_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_sectors_video_id_index` (`video_id`),
  KEY `videos_sectors_sector_id_index` (`sector_id`),
  CONSTRAINT `videos_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`),
  CONSTRAINT `videos_sectors_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_sectors`
--

/*!40000 ALTER TABLE `videos_sectors` DISABLE KEYS */;
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('31','31','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('32','32','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('33','33','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('34','35','30','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('36','36','36','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('37','37','15','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('38','39','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('39','41','28','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('41','45','10','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_sectors` (`id`,`video_id`,`sector_id`,`created_at`,`updated_at`) VALUES ('42','44','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `videos_sectors` ENABLE KEYS */;


--
-- Create Table `videos_themes`
--

DROP TABLE IF EXISTS `videos_themes`;
CREATE TABLE `videos_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_themes_video_id_index` (`video_id`),
  KEY `videos_themes_theme_id_index` (`theme_id`),
  CONSTRAINT `videos_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`),
  CONSTRAINT `videos_themes_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data for Table `videos_themes`
--

/*!40000 ALTER TABLE `videos_themes` DISABLE KEYS */;
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('31','31','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('32','32','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('33','33','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('34','34','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('35','35','21','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('37','36','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('38','37','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('40','39','22','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('41','40','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('42','41','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('43','42','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('44','43','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('46','45','24','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `videos_themes` (`id`,`video_id`,`theme_id`,`created_at`,`updated_at`) VALUES ('47','44','18','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `videos_themes` ENABLE KEYS */;

SET FOREIGN_KEY_CHECKS=1;
-- EOB

