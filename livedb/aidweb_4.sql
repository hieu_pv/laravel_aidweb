-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 25, 2015 at 10:13 AM
-- Server version: 5.5.41-cll-lve
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aidweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bases`
--

CREATE TABLE IF NOT EXISTS `bases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `beneficiaries`
--

CREATE TABLE IF NOT EXISTS `beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- Dumping data for table `beneficiaries`
--

INSERT INTO `beneficiaries` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Animals', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(2, 'Environment', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(3, 'Children & Youth', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(4, 'Disaster affected', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(5, 'Enterpreneurs', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(6, 'Families', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(7, 'Farmers', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(8, 'General Population', 0, '2015-05-04 16:59:36', '2015-05-19 15:57:00'),
(9, 'Handicapped', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(10, 'Homeless', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(11, 'Virus infected', 0, '2015-05-04 16:59:37', '2015-05-19 15:59:00'),
(12, 'Indigenous People', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(13, 'Internally Displaced', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(14, 'Illiterate & Uneducated', 0, '2015-05-04 16:59:37', '2015-05-19 16:00:00'),
(15, 'Older People', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(16, 'Orphans', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(17, 'Poorest', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(18, 'Refugees', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(19, 'Right-Deprived', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(20, 'Small-scale Farmers', 1, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(21, 'Stateless', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(22, 'Unemployed', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(23, 'Victims of domectic violence', 0, '2015-05-04 16:59:37', '2015-05-19 15:50:00'),
(24, 'War affected', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(25, 'Women', 0, '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(26, 'Victims of armed conflict', 0, '2015-05-19 15:50:00', '2015-05-19 15:50:00'),
(27, 'Fauna & FLora', 0, '2015-05-19 15:57:00', '2015-05-19 15:57:00'),
(28, 'Kidnapped & Missing', 0, '2015-05-19 16:00:00', '2015-05-19 16:00:00'),
(29, 'Prisoners', 0, '2015-05-19 16:01:00', '2015-05-19 16:01:00'),
(30, 'Victims of torture', 0, '2015-05-19 16:02:00', '2015-05-19 16:03:00'),
(31, 'Victims of disaster', 0, '2015-05-19 16:02:00', '2015-05-19 16:02:00'),
(32, 'Peace & Security', 1, '2015-05-19 16:35:00', '2015-05-19 16:35:00'),
(33, 'Population & Settlement', 1, '2015-05-19 16:36:00', '2015-05-19 16:36:00'),
(34, 'Population & Settlement', 1, '2015-05-19 16:36:00', '2015-05-19 16:36:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `image_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_legend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments_count` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `blog_posts_organisation_id_index` (`organisation_id`),
  KEY `blog_posts_user_profile_id_index` (`user_profile_id`),
  KEY `blog_posts_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `name`, `title`, `slug`, `content`, `image`, `image_source`, `image_legend`, `comments_count`, `published`, `deleted`, `featured`, `created_at`, `updated_at`, `organisation_id`, `user_profile_id`, `user_id`, `view_count`) VALUES
(51, '16955ADD-B19F-45BA-A2A0-6D01E0D4132A', 'Solutions for Inclusive, Green, and Resilient Cities', 'solutions-for-inclusive-green-and-resilient-cities', 'Speech by ADB President Takehiko Nakao at the Solutions for Inclusive, Green, and Resilient Cities Delhi Sustainable Development Summit (DSDS) 2015: Sustainable Development Goals and Dealing with Climate Change on 5 February 2015 in Delhi, India.<br />\r\n<br />\r\n<br />\r\nIntroduction<br />\r\n<br />\r\nYour Excellencies, Distinguished Guests, Ladies and Gentlemen:<br />\r\n<br />\r\nI am very pleased to speak to you today about &ldquo;Solutions for Inclusive, Green, and Resilient Cities&rdquo;. Before getting specifically into this topic, I would like to set the stage by talking more broadly about the international development agenda in 2015 &ndash; in particular, the Sustainable Development Goals (SDGs) and the prospects for a new global climate agreement.<br />\r\n<br />\r\n2015 will be a milestone year for international development. An agreement on the SDGs and the post 2015 agenda is expected to be reached at the September United Nations (UN) summit in New York. In December, a new climate deal is also expected to be agreed at COP 21 in Paris.<br />\r\n<br />\r\nIn July 2014, the UN Open Working Group (OWG) on the SDGs delivered its final report. The SDGs place environmental and social sustainability at the core of the new integrated development agenda. At the same time, the SDGs aim to eradicate extreme poverty, and tackle other unfinished business of the Millennium Development Goals (MDGs) by 2030 including for education and health. Economic transformation will be key to achieving many of the goals.<br />\r\n<br />\r\nThe global development community is now finalizing the SDGs. ADB supports key proposals of the SDGs. I want to stress 3 points about the transition from the MDGs to the SDGs. First, we still need to fully achieve the MDGs agenda. Second, poverty and inclusive growth cannot be set aside on environmental grounds. Third, neither can the environment be neglected in the pursuit of growth. Upholding these principles will be essential for sustainable development.<br />\r\n<br />\r\nAlong with the SDGs, the international community has never been so close to agreeing on a global deal on climate change involving all the important players. Intensive negotiations are ongoing in the lead-up to COP 21 in Paris in December.<br />\r\n<br />\r\nThe Lima Call for Climate Action agreed at COP 20 last December provides the framework for several elements of the new agreement and establishes ground rules on how nations can submit their contributions to the climate change agenda. Countries also agreed to raise adaptation to the same level as mitigation action.<br />\r\n<br />\r\nThe issue of climate finance will be critical for the new agreement. Recent pledging for the Green Climate Fund is an encouraging signal. Developed countries need to show leadership in global climate actions including technological innovation and transfer while developing countries will also contribute according to the principle of common but differentiated responsibility.<br />\r\n<br />\r\nI believe ADB and other multilateral development banks should make a key contribution to financing both SDGs and global climate actions. In doing so, special attention should be paid to their role of catalyzing public and private resources.<br />\r\nReshaping Asia&rsquo;s booming cities is the key to meeting the SDG and climate change goals<br />\r\n<br />\r\nNow I would like to go to the main topic of my discussion today, which is &quot;Solutions for Inclusive, Green and Resilient Cities&quot;. Cities are the key to meeting the SDGs and the global climate goals. The proposal for SDGs identified sustainable urban development as a critical area of focus. Several SDGs call for actions mainly in urban areas where most economic activities and investments take place. And cities are where much of the climate mitigation and adaptation action will have to take place.<br />\r\n<br />\r\nWhile cities are engines of growth, poverty and disparities are increasing.<br />\r\n<br />\r\nCities have driven economic growth in Asia &ndash; now producing about 80% of GDP &ndash; and have lifted tens of million out of poverty, especially in the last two decades. Asia is the most rapidly urbanizing region in the world. More than half of the world&rsquo;s largest cities are in Asia. By 2050, 3 billion people &ndash; about 65% of all Asians &ndash; will live in cities. This urbanization process should be in tandem with further economic growth.<br />\r\n<br />\r\nHowever, rapid urbanization also means the urbanization of poverty. Of a total of about 1.6 billion urban people in Asia, more than 500 million live in high density, degraded slums. Asia accounts for about 60% of the world&rsquo;s slum dwellers. Large disparities have emerged in urban areas, and the poor are the most vulnerable to economic and environmental shocks. It is clear that the way cities are developed and managed will heavily influence the effort to eradicate poverty in Asia.<br />\r\n<br />\r\nEnvironmental sustainability remains a major concern. As cities swell in size and numbers, they are under increasing environmental stress. Cities struggle with air and water pollution, traffic congestion, inadequate solid waste management and wastewater treatment. Only about 10% of solid waste ends up in properly managed landfill sites. Current levels of Investment in water, sanitation, housing and other urban infrastructure are not sufficient to cope with such a rapid urbanization process.<br />\r\n<br />\r\nVulnerability to climate change should also be addressed. Rapid and often poorly managed urbanization intensifies climate change risks and amplifies its impacts on infrastructure. Asian cities are especially vulnerable to the hazards caused by climate change: 238 million Asian urban poor are expected to be hit first and hardest by the effects of climate change. Natural disasters routinely erase 1% to 5% of GDP each year and this figure is increasing. Several Asian cities, including Dhaka, Kolkata, Mumbai, Shanghai, Bangkok, Yangon, and Manila, are at risk of coastal flooding as sea levels rise. With the cost of natural disasters set to rise in the near future, cities will become a major battleground in the region&rsquo;s fight against climate change.<br />\r\nPriorities for action: Inclusive, green, and resilient cities<br />\r\n<br />\r\nTo reduce poverty, address environmental challenges and mitigate climate related disasters, we must focus on building inclusive, green, and resilient cities.<br />\r\n<br />\r\nFirst, inclusive growth is key to reduce poverty. Creating inclusive cities means giving the urban poor better access to basic services such as primary health care, education, water, affordable transport and adequate housing. Inclusive cities should also expand quality job opportunities for the poor.<br />\r\n<br />\r\nADB supports such efforts in our developing member countries. For example, In India, we are working with the government to support its National Urban Health Mission (NUHM) efforts to strengthen urban primary health systems. The improved quality of urban health services is expected to benefit about 400 million people, including about 70 million currently living in urban slums. More than half the beneficiaries will be women, since maternal and child health services comprise a large part of urban primary health care.<br />\r\n<br />\r\nADB is also firmly committed to supporting India&rsquo;s 100 Smart Cities Initiatives. The use of technology and intelligent systems will improve urban services for the poor, including sanitation and affordable transportation.<br />\r\n<br />\r\nSecond, to make cities greener, they must become more energy and resource efficient through promotion of low carbon development and smart use of land and water. They need to invest more in mass public transport systems and better waste and wastewater management. Transport systems need to be better integrated. For example, in Vientiane, Laos, ADB is supporting mass public transport systems including Bus Rapid Transit (BRT) linked to cycle and pedestrian pathways. In this way, 700,000 people can easily switch from one mode of transport to another.<br />\r\n<br />\r\nAnother example of ADB&rsquo;s support for &ldquo;green&rdquo; cities is a private sector loan to develop waste-to-energy projects in more than 20 secondary cities across the People&rsquo;s Republic of China. As of June 2014, 12 of these plants are already in operation. Together, they can process about 4.6 million tonnes of household waste annually, generating approximately 1.3 billion kilowatt hours of on-grid electricity each year.<br />\r\n<br />\r\nThird, to make cities more resilient, decision makers should always consider natural hazards and climate change risks when designing cities and urban infrastructure. Relatively small up-front investments can save lives and avoid large scale infrastructure rebuilding and rehabilitation costs later. Asia needs to invest more in climate resilient infrastructure such as enlarged drainage systems in cities, elevated roads, and bigger storm-water retention reservoirs to accommodate variations in rainfall.<br />\r\n<br />\r\nInnovation and knowledge partnerships will be crucial in these efforts. At ADB, all investment projects are screened for climate risks; those at risk undergo climate impact assessments to ensure we can plan, build and manage investments that are more resilient.<br />\r\n<br />\r\nHere, I would like to mention an ADB-supported project in Bangladesh. The Coastal Towns Environmental Infrastructure Improvement Project is designed to reduce the vulnerability of municipal infrastructure &ndash; such as water supply, sanitation, solid waste management and bus terminals &ndash; to climate change and disaster risks. The project also includes investments in cyclone shelters, emergency access roads and bridges, and stronger homes in slums.<br />\r\nImportance of governance and institutions<br />\r\n<br />\r\nFinally, inclusive, green and resilient cities need better governance and more effective institutions. Greater use of integrated urban planning, better use of land, and more timely investments in sustainable and resilient infrastructure, are all essential. Cities also need additional funding beyond traditional sources. For example, they can consider the development of new finance instruments to better mobilize local resources &ndash; such as municipal bond mechanisms and public private partnerships (PPPs).<br />\r\n<br />\r\nOne example of innovative financing is the use of private equity funds to promote PPPs in the Philippines. The Philippine Investment Alliance for Infrastructure, whose investors comprise ADB, the Philippine government pension fund, a Dutch pension fund manager and an Australian private investment bank, is designed to draw other private funds into infrastructure.<br />\r\nClosing<br />\r\n<br />\r\nIn closing, I would like to commit that ADB will continue to play an important role in the development of inclusive, green and resilient cities and the success of the SDGs and the global Climate Change agenda.', '/uploads/media/109/posts/Nakao-ADB.jpg', 'http://holduparchitecture.com/MONT-PARNASSE', '', 0, 1, 0, 1, '2015-05-20 08:05:08', '2015-06-21 12:21:32', NULL, 109, 109, 5),
(52, '258F4F9F-46D6-4863-A563-1E95AD168BB6', 'Photo of the Week: Vanuatu’s ‘10,000 in 10’ campaign', 'photo-of-the-week-vanuatus-10000-in-10-campaign', 'Vanuatu, 2015: Tropical Cyclone Pam, which hit on 13 March, has disrupted access to safe water and sanitation in the South Pacific island nation, increasing children&rsquo;s risk of water- and vector-borne diseases.<br />\r\n<br />\r\nThe &lsquo;10,000 in 10&rsquo; campaign, launched on 18 March, aims to immunize 10,000 children 6 to 59 months of age against measles and rubella, in 21 villages over a period of 10 days.<br />\r\n<br />\r\nIn this photo, children near the shore in Port Vila watch a boat further out that is taking an immunization team to a nearby island.<br />\r\n<br />\r\nTo see more images from UNICEF visit UNICEF Photography (http://www.unicef.org/photography/photo_app.php).', '/uploads/media/132/posts/Nesbitt-Unicef.jpg', '©UNICEF/ NYHQ2015-0523/Sokhin', '', 0, 1, 0, 1, '2015-05-20 08:21:52', '2015-06-21 12:10:32', NULL, 132, 132, 6),
(53, '8135ED9F-4CD4-49B4-B53C-D2AE8D81DEEB', 'Filling the Void: Thinking About Limited Data in the Developing World', 'filling-the-void-thinking-about-limited-data-in-the-developing-world', 'Watching baseball&rsquo;s Opening Day this week reminded me of how the sport sparked my passion for numbers and statistics at an early age. One of my science fair projects way back when was on a (very limited) statistical analysis of whether or not expansion teams to Major League Baseball benefited pitchers more than batters.<br />\r\n<br />\r\nWhile the use of sabermetrics is relatively new to the game (I&rsquo;d highly recommend reading Moneyball if you haven&rsquo;t already), statistics have been a part of baseball since the 19th century and are as much a part of the game as hot dogs and athletic cups.<br />\r\n<br />\r\nThe greats of baseball were defined by their stats, from their tally of home runs or stolen bases to the number of World Series they led their teams to. Listening to or watching baseball games, you&rsquo;ll hear the commentator pull some of the most ridiculous statistics, which makes you think about how each part of the game is tracked in incredible detail &mdash; down to which team&rsquo;s players sport the most facial hair.<br />\r\nStudents participate in a local mapping project in Haiti in 2013. / Kendra Helmer, USAID<br />\r\n<br />\r\nWith all the data that&rsquo;s collected and pored over for baseball and other sports in the United States, you would think data would be as easily accessible across all areas. Unfortunately, that&rsquo;s not the case, especially for our work in developing countries. When working with statistics about the developing world, you find a lot of holes due to a whole host of problems. And for the data we do have access to, much of it is outdated or unreliable, as mentioned in a report released by the Center for Global Development last summer:<br />\r\n<br />\r\n&ldquo;But nowhere in the world is the need for better data more urgent than in sub-Saharan Africa &mdash; the region with perhaps the most potential for progress under a new development agenda. Despite a decade of rapid economic growth in most countries, the accuracy of the most basic data indicators such as GDP, number of kids attending school, and vaccination rates remains low, and improvements have been sluggish.<br />\r\n<br />\r\nThis is a problem especially apparent as the United Nations cultivates the Sustainable Development Goals, the successor to the Millennium Development Goals. In order to determine progress toward the 17 goals, the United Nations needs to collect good data to track a wide range of indicators. In the search for good data, it must accept imperfection as is done with plenty of statistics and data in the developed world.<br />\r\n<br />\r\nAs USAID, other agencies and donors conduct evaluations and analyses to identify critical areas or assess projects effectiveness, the work can often be hindered by the lack of (usable) data.<br />\r\n<br />\r\nTo address the data void, USAID is finding new and innovative ways for collection and measurement. For instance, the USAID GeoCenter has been engaging with university students in the United States and host countries through mapathons to chart unmapped areas of the world, such as Nepal, Bangladesh and the Philippines. The mapping data, openly shared, provide USAID and partners with better baseline information for monitoring projects. When combined with household surveys, the data can improve analyses and understanding of specific areas of vulnerability within a country.<br />\r\n<br />\r\nThe development community is far from reaching the level and reliability of statistics collected on Major League Baseball, which has been allowing general managers, coaches and fantasy baseball fanatics to make more informed decisions to improve their teams for decades.<br />\r\n<br />\r\nBy concentrating efforts to alleviate some of the systematic problems that lead to a lack of data in the first place, the development community would not just be improving access to reliable data, but would be solving some of the underlying problems of developing countries in the first place.<br />\r\n<br />\r\nWith more abundant, reliable and geocoded data about the developing world, USAID and other organizations can make more informed decisions about how to better target poverty, helping us reach the goal of eradicating extreme poverty by 2030.&nbsp;<br />\r\nRELATED LINKS:<br />\r\n<br />\r\nRead more about the Center for Global Development&rsquo;s recommendations for improving data access and quality&nbsp;Check out this video of Geographer Carrie Stokes explaining the USAID GeoCenter&rsquo;s work with Texas Tech to map an unmapped part of Bangladesh<br />\r\nFollow @USAIDEconomic and @GlobalDevLab<br />\r\n', '/uploads/media/131/posts/Chafetz.jpg', '', 'Ryan Zimmerman, third baseman for the Washington Nationals, on his way to homeplate to be mobbed by his ecstatic teammates after hitting a game-winning home run against the Atlanta Braves on opening night at the inaugural game played at Nationals Park in ', 0, 1, 0, 1, '2015-05-20 08:28:51', '2015-06-03 10:13:21', NULL, 131, 131, 2),
(54, '3ED8C23D-9875-40A6-89CF-F48905FC5EB7', 'Tools to integrate children’s rights into business', 'tools-to-integrate-childrens-rights-into-business', 'We received quite a bit of feedback on last month&rsquo;s post about UNICEF&rsquo;s new partnership with the LEGO Group (http://blogs.unicef.org/2015/03/13/unicef-the-lego-group-team-up-for-child-rights/), and several questions asking exactly how UNICEF works with companies and what it means to &lsquo;integrate children&rsquo;s rights,&rsquo; into business practice.<br />\r\n<br />\r\nWhen you ask: &ldquo;How does UNICEF work with companies on these issues?&rdquo; the answer is that we guide them in using four tools to understand: (1) where children&rsquo;s rights fit into their policies, (2) how their activities impact children, (3) how they should engage various stakeholders (from suppliers to employees) during the process of improving respect for children in their business, and (4) how they can monitor and report on their respect for children and other sustainability activities.<br />\r\n<br />\r\nThe 4 corresponding tools are:<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp; The Children&rsquo;s Rights in Policies and Codes of Conduct tool,<br />\r\n&nbsp;&nbsp;&nbsp; The Children&rsquo;s Rights in Impact Assessment tool,<br />\r\n&nbsp;&nbsp;&nbsp; The Engaging Stakeholders on Children&rsquo;s Rights tool, and<br />\r\n&nbsp;&nbsp;&nbsp; The Children&rsquo;s Rights in Sustainability Reporting tool.<br />\r\n<br />\r\n(1) The Children&rsquo;s Rights in Policies and Codes of Conduct tool can assist a company in integrating children&rsquo;s rights elements into its existing policies. Let&rsquo;s use a company&rsquo;s marketing and communications policies as an example. How does the tool work? Well, first, the tool walks the company through a series of questions testing the extent to which existing policies consider children&rsquo;s rights. Then, the tool references the relevant Children&rsquo;s Rights and Business Principle (which in the case of marketing, refers to Principle 6) and goes through a list of relevant considerations.<br />\r\n<br />\r\nFor example: Does the company&rsquo;s existing marketing and product labeling policies ensure that parents and children are empowered to make informed choices? This includes a policy that specifies the minimum age for child-targeted advertisement and what defines child-targeted advertisement for that company.<br />\r\n<br />\r\n(2) The Children&rsquo;s Rights in Impact Assessment tool identifies impact assessment criteria for each of the Children&rsquo;s Rights and Business Principles that a company can use when assessing its impact in each of the business areas. For example, sticking with Principle 6 (related to marketing) the impact assessment criteria proposed by the tool cover three areas of business: (1) policy, (2) due diligence, and (3) remediation.<br />\r\n<br />\r\nGlance at page 37 of the tool and you will find a table that specifically lays out imperative policy criteria for marketing and advertising, as well as the specific actions a company can take to meet that criterion if they currently do not. For instance, the table encourages companies to ask whether or not their current marketing policies take into account the evolving impacts related to the use of digital media, including the use of personalized promotions aimed specifically at children.<br />\r\n<br />\r\n(3) Clearly companies will need to engage different stakeholders when actively working to enhance their standards and practices at both the corporate and site levels. The Engaging Stakeholders on Children&rsquo;s Rights tool aids companies in determining the relevance and appropriate level of engagement with each type of stakeholder. With regards to Principle 6, this tool can be used to identify which stakeholders to engage when developing child-friendly marketing policies. This might include engaging with children directly to study the impact of marketing and advertising on their behavior through focus group interviews, surveys, etc.<br />\r\n<br />\r\n(4) The Children&rsquo;s Rights in Sustainability Reporting tool simply guides business in how to report on children&rsquo;s rights against the reporting framework provided by the Global Reporting Initiative (GRI). The tool includes examples of company information to report on.<br />\r\n<br />\r\nRelated to Principle 6, this means pointing out that a company should report on whether or not they have, for example, formal mechanisms for complaints concerning violations relating to children&rsquo;s rights in the context of marketing and advertising. This tool also suggests indicators that can be used to report on each of the Principles. For Principle 6, a possible indicator is &ldquo;programmes for adherence to laws, standards, and voluntary codes related to marketing communications, including advertising, promotion, and sponsorship.&rdquo;<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp; Full copies of the 4 tools for companies are freely available online (http://www.unicef.org/csr/88.htm).', '/uploads/media/135/posts/Nylund-Unicef.jpg', 'UNICEF/2013/Pirozzi', 'Two small children play with toy xylophones at an ECD center in Bolivia. ', 0, 1, 0, 1, '2015-05-21 14:10:36', '2015-06-14 23:36:26', NULL, 135, 135, 2);

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_beneficiaries`
--

CREATE TABLE IF NOT EXISTS `blog_posts_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_beneficiaries_post_id_index` (`post_id`),
  KEY `blog_posts_beneficiaries_beneficiary_id_index` (`beneficiary_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

--
-- Dumping data for table `blog_posts_beneficiaries`
--

INSERT INTO `blog_posts_beneficiaries` (`id`, `post_id`, `beneficiary_id`, `created_at`, `updated_at`) VALUES
(52, 52, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 51, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 54, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_countries`
--

CREATE TABLE IF NOT EXISTS `blog_posts_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_countries_post_id_index` (`post_id`),
  KEY `blog_posts_countries_country_id_index` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `blog_posts_countries`
--

INSERT INTO `blog_posts_countries` (`id`, `post_id`, `country_id`, `created_at`, `updated_at`) VALUES
(51, 52, 229, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_crisis`
--

CREATE TABLE IF NOT EXISTS `blog_posts_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_crisis_post_id_index` (`post_id`),
  KEY `blog_posts_crisis_crisis_id_index` (`crisis_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `blog_posts_crisis`
--

INSERT INTO `blog_posts_crisis` (`id`, `post_id`, `crisis_id`, `created_at`, `updated_at`) VALUES
(51, 52, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_interventions`
--

CREATE TABLE IF NOT EXISTS `blog_posts_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_interventions_post_id_index` (`post_id`),
  KEY `blog_posts_interventions_intervention_id_index` (`intervention_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_regions`
--

CREATE TABLE IF NOT EXISTS `blog_posts_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_regions_post_id_index` (`post_id`),
  KEY `blog_posts_regions_region_id_index` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

--
-- Dumping data for table `blog_posts_regions`
--

INSERT INTO `blog_posts_regions` (`id`, `post_id`, `region_id`, `created_at`, `updated_at`) VALUES
(52, 52, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 51, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 54, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_sectors`
--

CREATE TABLE IF NOT EXISTS `blog_posts_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_sectors_post_id_index` (`post_id`),
  KEY `blog_posts_sectors_sector_id_index` (`sector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- Dumping data for table `blog_posts_sectors`
--

INSERT INTO `blog_posts_sectors` (`id`, `post_id`, `sector_id`, `created_at`, `updated_at`) VALUES
(52, 52, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 51, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 54, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts_themes`
--

CREATE TABLE IF NOT EXISTS `blog_posts_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blog_posts_themes_post_id_index` (`post_id`),
  KEY `blog_posts_themes_theme_id_index` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

--
-- Dumping data for table `blog_posts_themes`
--

INSERT INTO `blog_posts_themes` (`id`, `post_id`, `theme_id`, `created_at`, `updated_at`) VALUES
(52, 52, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 51, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 54, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `commented_by` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=240 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(2, 'Albania', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(3, 'Algeria', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(4, 'American Samoa', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(5, 'Andorra', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(6, 'Angola', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(7, 'Anguilla', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(8, 'Antarctica', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(9, 'Antigua and Barbuda', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(10, 'Argentina', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(11, 'Armenia', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(12, 'Aruba', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(13, 'Australia', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(14, 'Austria', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(15, 'Azerbaijan', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(16, 'Bahamas', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(17, 'Bahrain', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(18, 'Bangladesh', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(19, 'Barbados', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(20, 'Belarus', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(21, 'Belgium', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(22, 'Belize', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(23, 'Benin', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(24, 'Bermuda', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(25, 'Bhutan', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(26, 'Bolivia', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(27, 'Bosnia and Herzegowina', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(28, 'Botswana', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(29, 'Bouvet Island', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(30, 'Brazil', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(31, 'British Indian Ocean Territory', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(32, 'Brunei Darussalam', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(33, 'Bulgaria', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(34, 'Burkina Faso', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(35, 'Burundi', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(36, 'Cambodia', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(37, 'Cameroon', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(38, 'Canada', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(39, 'Cape Verde', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(40, 'Cayman Islands', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(41, 'Central African Republic', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(42, 'Chad', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(43, 'Chile', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(44, 'China', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(45, 'Christmas Island', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(46, 'Cocos (Keeling) Islands', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(47, 'Colombia', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(48, 'Comoros', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(49, 'Congo', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(50, 'Congo, the Democratic Republic of the', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(51, 'Cook Islands', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(52, 'Costa Rica', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(53, 'Cote d''Ivoire', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(54, 'Croatia (Hrvatska)', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(55, 'Cuba', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(56, 'Cyprus', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(57, 'Czech Republic', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(58, 'Denmark', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(59, 'Djibouti', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(60, 'Dominica', 0, '2015-05-04 16:59:29', '2015-05-04 16:59:29'),
(61, 'Dominican Republic', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(62, 'East Timor', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(63, 'Ecuador', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(64, 'Egypt', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(65, 'El Salvador', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(66, 'Equatorial Guinea', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(67, 'Eritrea', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(68, 'Estonia', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(69, 'Ethiopia', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(70, 'Falkland Islands (Malvinas)', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(71, 'Faroe Islands', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(72, 'Fiji', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(73, 'Finland', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(74, 'France', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(75, 'France Metropolitan', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(76, 'French Guiana', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(77, 'French Polynesia', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(78, 'French Southern Territories', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(79, 'Gabon', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(80, 'Gambia', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(81, 'Georgia', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(82, 'Germany', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(83, 'Ghana', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(84, 'Gibraltar', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(85, 'Greece', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(86, 'Greenland', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(87, 'Grenada', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(88, 'Guadeloupe', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(89, 'Guam', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(90, 'Guatemala', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(91, 'Guinea', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(92, 'Guinea-Bissau', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(93, 'Guyana', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(94, 'Haiti', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(95, 'Heard and Mc Donald Islands', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(96, 'Holy See (Vatican City State)', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(97, 'Honduras', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(98, 'Hong Kong', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(99, 'Hungary', 0, '2015-05-04 16:59:30', '2015-05-04 16:59:30'),
(100, 'Iceland', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(101, 'India', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(102, 'Indonesia', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(103, 'Iran (Islamic Republic of)', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(104, 'Iraq', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(105, 'Ireland', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(106, 'Israel', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(107, 'Italy', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(108, 'Jamaica', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(109, 'Japan', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(110, 'Jordan', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(111, 'Kazakhstan', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(112, 'Kenya', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(113, 'Kiribati', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(114, 'Korea, Democratic People''s Republic of', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(115, 'Korea, Republic of', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(116, 'Kuwait', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(117, 'Kyrgyzstan', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(118, 'Lao, People''s Democratic Republic', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(119, 'Latvia', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(120, 'Lebanon', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(121, 'Lesotho', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(122, 'Liberia', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(123, 'Libyan Arab Jamahiriya', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(124, 'Liechtenstein', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(125, 'Lithuania', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(126, 'Luxembourg', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(127, 'Macau', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(128, 'Macedonia, The Former Yugoslav Republic of', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(129, 'Madagascar', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(130, 'Malawi', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(131, 'Malaysia', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(132, 'Maldives', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(133, 'Mali', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(134, 'Malta', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(135, 'Marshall Islands', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(136, 'Martinique', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(137, 'Mauritania', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(138, 'Mauritius', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(139, 'Mayotte', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(140, 'Mexico', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(141, 'Micronesia, Federated States of', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(142, 'Moldova, Republic of', 0, '2015-05-04 16:59:31', '2015-05-04 16:59:31'),
(143, 'Monaco', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(144, 'Mongolia', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(145, 'Montserrat', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(146, 'Morocco', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(147, 'Mozambique', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(148, 'Myanmar', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(149, 'Namibia', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(150, 'Nauru', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(151, 'Nepal', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(152, 'Netherlands', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(153, 'Netherlands Antilles', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(154, 'New Caledonia', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(155, 'New Zealand', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(156, 'Nicaragua', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(157, 'Niger', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(158, 'Nigeria', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(159, 'Niue', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(160, 'Norfolk Island', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(161, 'Northern Mariana Islands', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(162, 'Norway', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(163, 'Oman', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(164, 'Pakistan', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(165, 'Palau', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(166, 'Panama', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(167, 'Papua New Guinea', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(168, 'Paraguay', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(169, 'Peru', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(170, 'Philippines', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(171, 'Pitcairn', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(172, 'Poland', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(173, 'Portugal', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(174, 'Puerto Rico', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(175, 'Qatar', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(176, 'Reunion', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(177, 'Romania', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(178, 'Russian Federation', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(179, 'Rwanda', 0, '2015-05-04 16:59:32', '2015-05-04 16:59:32'),
(180, 'Saint Kitts and Nevis', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(181, 'Saint Lucia', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(182, 'Saint Vincent and the Grenadines', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(183, 'Samoa', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(184, 'San Marino', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(185, 'Sao Tome and Principe', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(186, 'Saudi Arabia', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(187, 'Senegal', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(188, 'Seychelles', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(189, 'Sierra Leone', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(190, 'Singapore', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(191, 'Slovakia (Slovak Republic)', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(192, 'Slovenia', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(193, 'Solomon Islands', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(194, 'Somalia', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(195, 'South Africa', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(196, 'South Georgia and the South Sandwich Islands', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(197, 'Spain', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(198, 'Sri Lanka', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(199, 'St. Helena', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(200, 'St. Pierre and Miquelon', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(201, 'Sudan', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(202, 'Suriname', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(203, 'Svalbard and Jan Mayen Islands', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(204, 'Swaziland', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(205, 'Sweden', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(206, 'Switzerland', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(207, 'Syrian Arab Republic', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(208, 'Taiwan, Province of China', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(209, 'Tajikistan', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(210, 'Tanzania, United Republic of', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(211, 'Thailand', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(212, 'Togo', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(213, 'Tokelau', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(214, 'Tonga', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(215, 'Trinidad and Tobago', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(216, 'Tunisia', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(217, 'Turkey', 0, '2015-05-04 16:59:33', '2015-05-04 16:59:33'),
(218, 'Turkmenistan', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(219, 'Turks and Caicos Islands', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(220, 'Tuvalu', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(221, 'Uganda', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(222, 'Ukraine', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(223, 'United Arab Emirates', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(224, 'United Kingdom', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(225, 'United States', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(226, 'United States Minor Outlying Islands', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(227, 'Uruguay', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(228, 'Uzbekistan', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(229, 'Vanuatu', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(230, 'Venezuela', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(231, 'Vietnam', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(232, 'Virgin Islands (British)', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(233, 'Virgin Islands (U.S.)', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(234, 'Wallis and Futuna Islands', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(235, 'Western Sahara', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(236, 'Yemen', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(237, 'Yugoslavia', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(238, 'Zambia', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(239, 'Zimbabwe', 0, '2015-05-04 16:59:34', '2015-05-04 16:59:34');

-- --------------------------------------------------------

--
-- Table structure for table `crisis`
--

CREATE TABLE IF NOT EXISTS `crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `crisis`
--

INSERT INTO `crisis` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(2, 'Cameroon', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(3, 'Central African Republic', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(4, 'Chad', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(5, 'Congo', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(6, 'Ebola', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(7, 'Irak', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(8, 'Kenya', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(9, 'Lybia', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(10, 'Mali', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(11, 'Myanmar', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(12, 'Niger', 1, '2015-05-04 16:59:34', '2015-05-04 16:59:34'),
(13, 'Nigeria', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(14, 'Pakistan', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(15, 'Palestine', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(16, 'Sahel', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(17, 'Somalia', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(18, 'South Sudan', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(19, 'Southern Africa Floods', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(20, 'Sudan', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(21, 'Syria', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(22, 'Ukraine', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(23, 'Yemen', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(24, 'Animal Plague', 0, '2015-05-19 16:05:00', '2015-05-19 16:05:00'),
(25, 'Avalanche', 0, '2015-05-19 16:05:00', '2015-05-19 16:05:00'),
(26, 'Biological Hazard', 0, '2015-05-19 16:06:00', '2015-05-19 16:06:00'),
(27, 'Chemical Hazard', 0, '2015-05-19 16:06:00', '2015-05-19 16:06:00'),
(28, 'Civil Unrest', 0, '2015-05-19 16:07:00', '2015-05-19 16:07:00'),
(29, 'Climate Change', 0, '2015-05-19 16:07:00', '2015-05-19 16:07:00'),
(30, 'Armed Conflict', 0, '2015-05-19 16:07:00', '2015-05-19 16:07:00'),
(31, 'Disease Epidemic', 0, '2015-05-19 16:12:00', '2015-05-19 16:12:00'),
(32, 'Drought', 0, '2015-05-19 16:14:00', '2015-05-19 16:14:00'),
(33, 'Earthquake', 0, '2015-05-19 16:14:00', '2015-05-19 16:14:00'),
(34, 'Fire', 0, '2015-05-19 16:17:00', '2015-05-19 16:19:00'),
(35, 'Extreme Temperature', 0, '2015-05-19 16:19:00', '2015-05-19 16:19:00'),
(36, 'Famine', 0, '2015-05-19 16:20:00', '2015-05-19 16:20:00'),
(37, 'Flood', 0, '2015-05-19 16:20:00', '2015-05-19 16:20:00'),
(38, 'Genocide', 0, '2015-05-19 16:21:00', '2015-05-19 16:21:00'),
(39, 'Industrial accident', 0, '2015-05-19 16:22:00', '2015-05-19 16:22:00'),
(40, 'Insect Plague', 0, '2015-05-19 16:22:00', '2015-05-19 16:22:00'),
(41, 'Landslide', 0, '2015-05-19 16:22:00', '2015-05-19 16:22:00'),
(42, 'Pandemic', 0, '2015-05-19 16:22:00', '2015-05-19 16:22:00'),
(43, 'Pollution', 0, '2015-05-19 16:23:00', '2015-05-19 16:23:00'),
(44, 'Species Extinction', 0, '2015-05-19 16:23:00', '2015-05-19 16:23:00'),
(45, 'Storm', 0, '2015-05-19 16:23:00', '2015-05-19 16:23:00'),
(46, 'Transport Accident', 0, '2015-05-19 16:23:00', '2015-05-19 16:23:00'),
(47, 'Under development', 0, '2015-05-19 16:24:00', '2015-05-19 16:24:00'),
(48, 'Unplanned Urbanization', 0, '2015-05-19 16:24:00', '2015-05-19 16:24:00'),
(49, 'Tsunami & Tidal Surge', 0, '2015-05-19 16:27:00', '2015-05-19 16:27:00'),
(50, 'Climate Change', 1, '2015-05-19 16:28:00', '2015-05-19 16:28:00'),
(51, 'Volcanic Eruption', 0, '2015-05-19 16:29:00', '2015-05-19 16:29:00');

-- --------------------------------------------------------

--
-- Table structure for table `interventions`
--

CREATE TABLE IF NOT EXISTS `interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `interventions`
--

INSERT INTO `interventions` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'International Development', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(2, 'National issues', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(3, 'Relief Assistance', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(4, 'Recovery and Rehabilitation', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `level_of_activities`
--

CREATE TABLE IF NOT EXISTS `level_of_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `level_of_responsibilities`
--

CREATE TABLE IF NOT EXISTS `level_of_responsibilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `liked_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `module_id`, `item_id`, `liked_by`, `created_at`, `updated_at`) VALUES
(1, 4, 44, 101, '2015-06-03 14:08:17', '2015-06-03 14:08:17'),
(2, 2, 112, 135, '2015-06-06 07:28:55', '2015-06-06 07:28:55'),
(3, 2, 111, 135, '2015-06-06 07:30:53', '2015-06-06 07:30:53'),
(4, 4, 43, 135, '2015-06-06 07:32:03', '2015-06-06 07:32:03');

-- --------------------------------------------------------

--
-- Table structure for table `member_statuses`
--

CREATE TABLE IF NOT EXISTS `member_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `member_statuses`
--

INSERT INTO `member_statuses` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Non-profit employee', '', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(2, 'Aid Collaborator', 'your work relates closely to the aid or social sector', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(3, 'Aid Supporter', 'you personally support non-profits', '2015-05-04 16:59:42', '2015-05-04 16:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_02_05_045909_create_table_settings', 1),
('2015_02_05_055729_create_lookup_tables', 1),
('2015_02_05_153438_create_filter_tables', 1),
('2015_02_06_160613_create_table_users', 1),
('2015_02_07_055730_create_table_user_profiles', 1),
('2015_02_07_055740_create_table_organisations', 1),
('2015_02_10_073623_create_table_blog_posts', 1),
('2015_03_02_154616_create_table_blog_posts_countries', 1),
('2015_03_02_154623_create_table_blog_posts_regions', 1),
('2015_03_02_154629_create_table_blog_posts_crisis', 1),
('2015_03_02_154636_create_table_blog_posts_sectors', 1),
('2015_03_02_154642_create_table_blog_posts_themes', 1),
('2015_03_02_154648_create_table_blog_posts_beneficiaries', 1),
('2015_03_05_123910_create_table_news_items', 1),
('2015_03_05_153856_create_table_news_items_countries', 1),
('2015_03_05_153903_create_table_news_items_regions', 1),
('2015_03_05_153907_create_table_news_items_crisis', 1),
('2015_03_05_153911_create_table_news_items_sectors', 1),
('2015_03_05_153916_create_table_news_items_themes', 1),
('2015_03_05_153922_create_table_news_items_beneficiaries', 1),
('2015_03_17_150936_create_table_types_of_take_actions', 1),
('2015_03_18_150936_create_table_take_actions', 1),
('2015_03_24_153505_create_table_take_actions_countries', 1),
('2015_03_24_153518_create_table_take_actions_regions', 1),
('2015_03_24_153525_create_table_take_actions_crisis', 1),
('2015_03_24_153530_create_table_take_actions_sectors', 1),
('2015_03_24_153535_create_table_take_actions_themes', 1),
('2015_03_24_153540_create_table_take_actions_beneficiaries', 1),
('2015_03_27_050412_create_table_videos', 1),
('2015_03_27_050445_create_table_videos_countries', 1),
('2015_03_27_050454_create_table_videos_regions', 1),
('2015_03_27_050504_create_table_videos_crisis', 1),
('2015_03_27_050512_create_table_videos_sectors', 1),
('2015_03_27_050522_create_table_videos_themes', 1),
('2015_03_27_050537_create_table_videos_beneficiaries', 1),
('2015_04_06_140325_create_table_password_reminders', 1),
('2015_04_09_015432_alter_table_organisations_add_description_field', 1),
('2015_04_09_015703_alter_table_user_profiles_add_description_field', 1),
('2015_04_09_025412_alter_table_news_items_drop_readmore', 1),
('2015_04_09_025641_alter_table_blog_posts__drop_readmore', 1),
('2015_04_09_032831_alter_table_user_profiles_add_gender_field', 1),
('2015_04_09_041931_alter_table_organisations_add_acronym_field', 1),
('2015_04_09_082126_create_table_filter_interventions', 1),
('2015_04_09_082655_create_table_postings_cross_interventions', 1),
('2015_04_13_031938_create_table_registrations', 1),
('2015_04_14_133957_alter_table_organisations_add_socialmedia_fields', 1),
('2015_04_14_134950_alter_table_user_profiles_add_socialmedia_fields', 1),
('2015_04_15_143439_alter_table_organisations_add_phone', 1),
('2015_04_17_013922_alter_table_videos_add_description_and_duration', 1),
('2015_04_18_135514_create_table_types_of_publicities', 1),
('2015_04_18_135515_create_table_publicities', 1),
('2015_04_26_161640_alter_table_news_items_add_popularity_fields', 1),
('2015_04_26_161927_create_table_likes', 1),
('2015_04_26_173456_alter_table_videos_add_popularity_fields', 1),
('2015_04_27_132131_alter_table_blog_posts_add_popularity_fields', 1),
('2015_04_28_161943_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE IF NOT EXISTS `nationalities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=194 ;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Afghan', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(2, 'Albanian', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(3, 'Algerian', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(4, 'American', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(5, 'Andorran', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(6, 'Angolan', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(7, 'Antiguans', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(8, 'Argentinean', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(9, 'Armenian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(10, 'Australian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(11, 'Austrian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(12, 'Azerbaijani', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(13, 'Bahamian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(14, 'Bahraini', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(15, 'Bangladeshi', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(16, 'Barbadian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(17, 'Barbudans', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(18, 'Batswana', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(19, 'Belarusian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(20, 'Belgian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(21, 'Belizean', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(22, 'Beninese', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(23, 'Bhutanese', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(24, 'Bolivian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(25, 'Bosnian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(26, 'Brazilian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(27, 'British', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(28, 'Bruneian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(29, 'Bulgarian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(30, 'Burkinabe', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(31, 'Burmese', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(32, 'Burundian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(33, 'Cambodian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(34, 'Cameroonian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(35, 'Canadian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(36, 'Cape Verdean', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(37, 'Central African', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(38, 'Chadian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(39, 'Chilean', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(40, 'Chinese', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(41, 'Colombian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(42, 'Comoran', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(43, 'Congolese', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(44, 'Costa Rican', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(45, 'Croatian', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(46, 'Cuban', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(47, 'Cypriot', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(48, 'Czech', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(49, 'Danish', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(50, 'Djibouti', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(51, 'Dominican', '2015-05-04 16:59:38', '2015-05-04 16:59:38'),
(52, 'Dutch', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(53, 'East Timorese', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(54, 'Ecuadorean', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(55, 'Egyptian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(56, 'Emirian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(57, 'Equatorial Guinean', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(58, 'Eritrean', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(59, 'Estonian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(60, 'Ethiopian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(61, 'Fijian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(62, 'Filipino', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(63, 'Finnish', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(64, 'French', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(65, 'Gabonese', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(66, 'Gambian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(67, 'Georgian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(68, 'German', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(69, 'Ghanaian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(70, 'Greek', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(71, 'Grenadian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(72, 'Guatemalan', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(73, 'Guinea-Bissauan', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(74, 'Guinean', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(75, 'Guyanese', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(76, 'Haitian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(77, 'Herzegovinian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(78, 'Honduran', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(79, 'Hungarian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(80, 'I-Kiribati', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(81, 'Icelander', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(82, 'Indian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(83, 'Indonesian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(84, 'Iranian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(85, 'Iraqi', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(86, 'Irish', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(87, 'Israeli', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(88, 'Italian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(89, 'Ivorian', '2015-05-04 16:59:39', '2015-05-04 16:59:39'),
(90, 'Jamaican', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(91, 'Japanese', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(92, 'Jordanian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(93, 'Kazakhstani', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(94, 'Kenyan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(95, 'Kittian and Nevisian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(96, 'Kuwaiti', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(97, 'Kyrgyz', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(98, 'Laotian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(99, 'Latvian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(100, 'Lebanese', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(101, 'Liberian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(102, 'Libyan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(103, 'Liechtensteiner', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(104, 'Lithuanian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(105, 'Luxembourger', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(106, 'Macedonian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(107, 'Malagasy', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(108, 'Malawian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(109, 'Malaysian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(110, 'Maldivan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(111, 'Malian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(112, 'Maltese', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(113, 'Marshallese', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(114, 'Mauritanian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(115, 'Mauritian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(116, 'Mexican', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(117, 'Micronesian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(118, 'Moldovan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(119, 'Monacan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(120, 'Mongolian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(121, 'Moroccan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(122, 'Mosotho', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(123, 'Motswana', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(124, 'Mozambican', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(125, 'Namibian', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(126, 'Nauruan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(127, 'Nepalese', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(128, 'New Zealander', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(129, 'Nicaraguan', '2015-05-04 16:59:40', '2015-05-04 16:59:40'),
(130, 'Nigerian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(131, 'Nigerien', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(132, 'North Korean', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(133, 'Northern Irish', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(134, 'Norwegian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(135, 'Omani', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(136, 'Pakistani', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(137, 'Palauan', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(138, 'Panamanian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(139, 'Papua New Guinean', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(140, 'Paraguayan', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(141, 'Peruvian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(142, 'Polish', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(143, 'Portuguese', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(144, 'Qatari', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(145, 'Romanian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(146, 'Russian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(147, 'Rwandan', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(148, 'Saint Lucian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(149, 'Salvadoran', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(150, 'Samoan', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(151, 'San Marinese', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(152, 'Sao Tomean', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(153, 'Saudi', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(154, 'Scottish', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(155, 'Senegalese', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(156, 'Serbian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(157, 'Seychellois', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(158, 'Sierra Leonean', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(159, 'Singaporean', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(160, 'Slovakian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(161, 'Slovenian', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(162, 'Solomon Islander', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(163, 'Somali', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(164, 'South African', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(165, 'South Korean', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(166, 'Spanish', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(167, 'Sri Lankan', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(168, 'Sudanese', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(169, 'Surinamer', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(170, 'Swazi', '2015-05-04 16:59:41', '2015-05-04 16:59:41'),
(171, 'Swedish', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(172, 'Swiss', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(173, 'Syrian', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(174, 'Taiwanese', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(175, 'Tajik', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(176, 'Tanzanian', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(177, 'Thai', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(178, 'Togolese', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(179, 'Tongan', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(180, 'Trinidadian/Tobagonian', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(181, 'Tunisian', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(182, 'Turkish', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(183, 'Tuvaluan', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(184, 'Ugandan', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(185, 'Ukrainian', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(186, 'Uruguayan', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(187, 'Uzbekistani', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(188, 'Venezuelan', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(189, 'Vietnamese', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(190, 'Welsh', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(191, 'Yemenite', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(192, 'Zambian', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(193, 'Zimbabwean', '2015-05-04 16:59:42', '2015-05-04 16:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `news_items`
--

CREATE TABLE IF NOT EXISTS `news_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `image_legend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photographer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments_count` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `news_items_organisation_id_index` (`organisation_id`),
  KEY `news_items_user_profile_id_index` (`user_profile_id`),
  KEY `news_items_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=114 ;

--
-- Dumping data for table `news_items`
--

INSERT INTO `news_items` (`id`, `name`, `title`, `slug`, `content`, `image`, `image_legend`, `photographer_name`, `comments_count`, `published`, `deleted`, `featured`, `created_at`, `updated_at`, `organisation_id`, `user_profile_id`, `user_id`, `view_count`) VALUES
(104, '75F75519-F0AA-4491-A6F8-1AB536057D80', 'UAE: Three sisters released after three months in secret detention for tweeting', 'uae-three-sisters-released-after-three-months-in-secret-detention-for-tweeting', 'Three sisters were reunited with their family today after spending three months in secret detention after the United Arab Emirates (UAE) authorities subjected them to enforced disappearance, Amnesty International said. They were detained after posting comments on Twitter on behalf of their brother, a prisoner of conscience in the Gulf state.<br />\r\n<br />\r\nAccording to Ahmed Mansoor, a prominent human rights defender, the sisters, Asma Khalifa al-Suwaidi, Mariam Khalifa al-Suwaidi and Dr Alyaziyah Khalifa al-Suwaidi, were dropped off at their family home at close to noon local time today.<br />\r\n<br />\r\nThey had not been heard from since they were summoned for questioning at an Abu Dhabi police station on 15 February and then taken into the custody of the UAE&rsquo;s state security apparatus.\r\n<blockquote>\r\n<p><strong style="line-height: 1.6em;"><q>It is not yet known what pressure the al-Suwaidi sisters were under while in detention, if they were charged with any offence, or if their release carries any conditions. What is clear, however, is that these three women should never have been detained in the first place.</q>&nbsp;</strong><strong style="line-height: 1.6em;">Said Boumedouha, Amnesty International&#39;s Deputy Director for the Middle East and North Africa Programme</strong></p>\r\n</blockquote>\r\n<br />\r\n&ldquo;It is not yet known what pressure the al-Suwaidi sisters were under while in detention, if they were charged with any offence, or if their release carries any conditions,&rdquo; said Said Boumedouha, Amnesty International&#39;s Deputy Director for the Middle East and North Africa programme.<br />\r\n<br />\r\n&ldquo;What is clear, however, is that these three women should never have been detained in the first place. If necessary, we&rsquo;ll continue to campaign on the world stage to call for all charges and conditions to be dropped.<br />\r\n<br />\r\n&ldquo;Enforced disappearance is a crime under international law. It is a chilling act of repression for the state to silence activists&rsquo; families by locking them up for months, with no access to their loved ones or the outside world.<br />\r\n<br />\r\n&quot;While the three sisters&rsquo; release must clearly come as a huge relief for their family and loved ones, the fact of the matter remains that their peaceful tweets have been punished with enforced disappearance. Those responsible must be brought to justice in fair trials.&rdquo;<br />\r\n<br />\r\nPrisoners held in secret detention under the authority of the UAE&rsquo;s state security apparatus are extremely vulnerable and are at particularly high risk of torture or other ill-treatment. Enforced disappearance in itself violates the absolute ban on torture and other ill-treatment. Amnesty International has been campaigning since February for the immediate and unconditional release of the three al-Suwaidi sisters.<br />\r\n<br />\r\nThe organization is calling on the UAE authorities to immediately and unconditionally release all prisoners of conscience.', '/uploads/media/106/news/Amesty.png', '', 'Sophie James / Shutterstock.com', 0, 1, 0, 1, '2015-05-19 07:53:51', '2015-05-21 06:43:07', 55, 106, 106, 3),
(105, 'B9D9311A-BE44-4E57-B7F3-ED9D0D94B9D8', 'ADB, Japan to Help Asia Tap Space Technologies to Counter Disasters ', 'adb-japan-to-help-asia-tap-space-technologies-to-counter-disasters', 'MANILA, PHILIPPINES &ndash; The Asian Development Bank (ADB) and Japan are to help developing countries in Asia and the Pacific tap the latest technologies, including satellite maps, to help them prepare and respond more effectively and quickly to natural disasters.<br />\r\n<br />\r\nA $2 million technical assistance grant from the Japan Fund for Poverty Reduction, administered by ADB, will be used to train government, community officials and local volunteers in Armenia, Bangladesh, Fiji, and the Philippines to use new state-of-the-art space-based technology and other high tech tools for disaster planning. These four countries will act as pilots for the potential wider adoption of these technologies across the region.<br />\r\n<br />\r\nThe use of space-based technology, including satellite-based systems like GPS, for disaster planning and response has been growing in recent years. However many developing countries lack the funds and expertise to adopt new technologies which can supplement their existing early warning and disaster monitoring systems.<br />\r\n<br />\r\n&ldquo;Countries which are vulnerable to catastrophes need more information-based disaster risk management and response tools to prepare better before disasters strike, and to respond better after earthquakes, floods or typhoons hit,&rdquo; said Yusuke Muraki, Infrastructure Specialist with ADB&rsquo;s Regional and Sustainable Development Department. &ldquo;Space-based technology can help these pilot countries improve their resilience in an efficient, sustainable way with reliable and timely disaster-related data.&rdquo;<br />\r\n<br />\r\nThe technical assistance project will train government agencies and local communities in the target countries to use OpenStreetMap, a community-based digital mapping platform, and mobile phone applications, which allow them to collect community-based information for disaster risk planning. They will also get support to use other computer-based and mobile phone applications for managing post-disaster situations more effectively, including for evacuations and the timely delivery of relief goods. &nbsp;Combining satellite-based hazard maps with existing local government maps of vulnerable areas helps identify potential disaster locations more precisely.<br />\r\n<br />\r\nAlong with training, the project will also draw up policy guidelines to guide the use of the new technologies, providing a potential blueprint for their wider adoption in other parts of the region.<br />\r\n<br />\r\nADB, based in Manila, is dedicated to reducing poverty in Asia and the Pacific through inclusive economic growth, environmentally sustainable growth, and regional integration. Established in 1966, it is owned by 67 members &ndash; 48 from the region.&nbsp;', '/uploads/media/101/news/ADB.jpg', 'ADB’s technical assistance will help pilot countries adopt modern technology to supplement their existing early warning and disaster monitoring systems. Devastation caused by Typhoon Haiyan (Yolanda) in the Philippines. ', '', 0, 1, 0, 1, '2015-05-21 06:29:44', '2015-05-21 06:56:20', 54, 105, 105, 1),
(106, '86ED9ACA-AB8B-40FA-9623-8079A73AD5BB', 'Support for Vanuatu''s cyclone recovery', 'support-for-vanuatus-cyclone-recovery', 'Media release - 16 April 2015. Australia will provide additional assistance to Vanuatu to support its recovery from Tropical Cyclone Pam, meeting additional needs and to ensure its long-term recovery.<br />\r\n<br />\r\nEducation, health and food security have been assessed by the Government of Vanuatu and its partners as priority areas for assistance. Australia will provide an extra $5 million to help meet these needs, comprising:<br />\r\n<br />\r\n&nbsp; &nbsp; $2.3 million for urgent school repairs and the replacement of damaged learning materials;<br />\r\n&nbsp; &nbsp; $1.5 million to repair health infrastructure, re-stock pharmaceutical supplies, strengthen immunisation and support the cold storage and transport of medicines; and<br />\r\n&nbsp; &nbsp; $1.2 million to address urgent food needs and restore local food sources.<br />\r\n<br />\r\nThis package builds on the more than $10 million already provided by Australia for emergency relief efforts.<br />\r\n<br />\r\nAustralia&rsquo;s additional assistance marks the transition from our initial emergency response, including the drawdown of Australian Defence Force assets and personnel.', '/uploads/media/108/news/AUSAID.JPG', 'Relief supplies have begun arriving in Port Vila', 'Care Australia', 0, 1, 0, 1, '2015-05-21 06:51:19', '2015-05-21 06:51:19', 57, 108, 108, 0),
(107, 'B033B124-FF8D-425C-9371-9BFF0A1D0296', 'UK leads global crackdown on illegal wildlife trade', 'uk-leads-global-crackdown-on-illegal-wildlife-trade', '<br />\r\nBritain will provide new funding to a global crackdown on the trade in rhino horn, elephant ivory and other illegal wildlife products, International Development Secretary Justine Greening announced today.<br />\r\n<br />\r\nAs well as strengthening law enforcement and reducing demand for illegal products, the &pound;3 million of new support will focus on helping communities affected by the illegal wildlife trade boost their incomes through wildlife conservation.<br />\r\n<br />\r\nJustine Greening said:<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;Poaching threatens endangered animals such as the white rhino, African elephant and snow leopard. It also feeds corruption, undermines global security and costs governments billions in lost revenues every year.&quot;<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;We cannot and will not look the other way while this continues.&quot;<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;This new funding will improve law enforcement, reduce demand for illegal wildlife products and, importantly, help communities find new sources of income and free themselves from this horrific trade.&quot;<br />\r\n<br />\r\nSpeaking from the Illegal Wildlife Trade Conference in Botswana, DEFRA Minister Lord De Mauley said:<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;Wildlife crime has reached unprecedented levels. We must not stand idly by. We must honour the commitments made in London and build on them at Kasane. This issue affects many of our important international partners deeply; and is much more than an environmental issue; it is about tackling corruption, improving security and raising livelihoods.&quot;<br />\r\n<br />\r\n&nbsp; &nbsp; &quot;If we wait any longer then we will wake up to find that all the endangered species living in the wild have been killed. We must send a powerful message to poachers and traffickers wherever they operate: the illegal wildlife trade ends here.&quot;<br />\r\n<br />\r\nThe additional &pound;3 million for the Illegal Wildlife Trade (IWT) challenge fund, financed by the Department for International Development (DFID) and managed by the Department for Environment, Food and Rural Affairs (DEFRA), builds on the &pound;10 million fund announced ahead of the London Conference on the Illegal Wildlife Trade, held in February last year. The first round of Britain&rsquo;s challenge fund is now supporting 19 projects around the world.<br />\r\n<br />\r\nProjects supported in the first call for funding include:\r\n<ul>\r\n	<li>&nbsp; &nbsp; Sending experts to border points in the Horn Of Africa to provide training on wildlife law</li>\r\n	<li>&nbsp; &nbsp; Funding Interpol to improve cooperation between national agencies in Uganda, Kenya and South Africa</li>\r\n	<li>&nbsp; &nbsp; Increasing the number of successful convictions across Africa with a new wildlife forensic network, linking DNA &nbsp; &nbsp;laboratories across the region.</li>\r\n</ul>\r\nThe illegal wildlife trade is booming. Rhino horn is now worth more than gold and is more valuable on the black market than diamonds or cocaine. The ivory trade has more than doubled since 2007. UNEP estimate that the trade costs between $7 and $23 billion every year in lost revenues, primarily for governments in the developing world.<br />\r\n<br />\r\nBut since last year&rsquo;s London Conference there has been solid progress. Over the past 12 months ivory stockpiles have been destroyed in countries from Hong Kong to Chad. There is now tightened security, more training and more arrests across the globe.<br />\r\n<br />\r\nThe second call for proposals to the IWT fund will soon be open for applications. There will be &pound;5 million available in this second round of the Illegal Wildlife Trade Challenge Fund, including &pound;3 million of new funds plus &pound;2 million in existing funding.<br />\r\n<br />\r\nOrganisations can bid for funding for projects in developing countries that address the following themes:\r\n<ul>\r\n	<li>&nbsp; &nbsp; Developing sustainable livelihoods for communities affected by illegal wildlife trade</li>\r\n	<li>&nbsp; &nbsp; Strengthening law enforcement and the role of the criminal justice system</li>\r\n	<li>&nbsp; &nbsp; Reducing demand for the products of the illegal wildlife trade</li>\r\n</ul>\r\n&nbsp;\r\n\r\n<p><strong>Press office</strong></p>\r\n\r\n<div>\r\n<div>Email<br />\r\n<a href="mailto:pressoffice@dfid.gov.uk">pressoffice@dfid.gov.uk</a><br />\r\nTelephone020 7023 0600</div>\r\n\r\n<div class="comments" style="border: none; margin: 10px 0px; padding: 7px 0px 3px; line-height: 1.25; width: 302px; float: left;">Follow the DFID Press office on Twitter - @DFID_Press</div>\r\n</div>\r\n', '/uploads/media/113/news/DFID 2.jpg', 'The UK Border Force cracks down on international trade in endangered species. Grant Miller with some of the items confiscated at Heathrow ', 'Glenn Copus', 0, 1, 0, 1, '2015-05-21 07:18:12', '2015-06-11 16:15:17', 61, 113, 113, 3),
(108, 'A2EB0EB9-E469-49F0-872A-F912FC756F91', 'Harper Government Announces Support to Improve Children’s Access to Quality Education in Developing Countries', 'harper-government-announces-support-to-improve-childrens-access-to-quality-education-in-developing-countries', 'April 16, 2015 - Washington, D.C. - Foreign Affairs, Trade and Development Canada<br />\r\n<br />\r\nToday, the Honourable Christian Paradis, Minister of International Development and La Francophonie, announced that Canada will contribute a total of C$120 million to the Global Partnership for Education, aimed at enhancing education in developing countries. This signals Canada&rsquo;s continued and deep commitment to promoting education for girls and boys in developing countries.<br />\r\n<br />\r\nCanada is also committed to working with key partners, including UN Special Envoy for Global Education Gordon Brown, the Global Partnership for Education, and UNICEF, to ensure that children in crisis situations have opportunities to learn. Minister Paradis announced that Canada is contributing an additional C$10 million to UNICEF for education and child protection in humanitarian crises around the world.<br />\r\n<br />\r\nHe made the announcement following the Safe Schools: Reaching all Children with Education in Lebanon event during the Spring Meetings of the World Bank and International Monetary Fund in Washington, D.C. He was joined by UN Special Envoy for Global Education Gordon Brown, the Global Partnership for Education&rsquo;s Chief Executive Officer, Alice Albright, and UNICEF&rsquo;s Executive Director, Anthony Lake.<br />\r\n<br />\r\nCanada is committed to ensuring the world&rsquo;s poorest and most vulnerable children and youth have access to quality education in safe and secure environments. Canada&#39;s international development programming in basic education is framed by the Securing the Future of Children and Youth strategy, which identifies access to quality basic education as a priority for action for Canada.<br />\r\nQuick Facts<br />\r\n<br />\r\n&nbsp; &nbsp; The Global Partnership for Education comprises close to 60 developing countries, as well as donor governments, international organizations, the private sector, teachers, and local and global civil society organizations. It has achieved significant results in primary school enrolment, literacy, and girls&rsquo; education, particularly in fragile and conflict-affected states.<br />\r\n&nbsp; &nbsp; UNICEF works in 190 countries and territories with a special focus on reaching the most vulnerable and excluded children, to the benefit of all children. In 2014, UNICEF helped provide over 8.6 million children in humanitarian situations around the world with access to basic education.<br />\r\n&nbsp; &nbsp; Improving maternal, newborn and child health is Canada&#39;s top development priority. With our global partners, we have achieved remarkable results: fewer women are dying in pregnancy and childbirth, and millions more children are celebrating their fifth birthday.<br />\r\n<br />\r\nQuotes<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;This will make a real difference in the lives of children in developing countries. It will help to ensure they have access to quality education, which is vital to help them move out of poverty and build prosperous futures. Canada aims to ensure that children not only survive, but have the opportunity to thrive, from their first years of life to adulthood. This is also why we have taken a leadership role worldwide to promote maternal, newborn and child health and to make strides to end child, early and forced marriage.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Christian Paradis, Minister of International Development and La Francophonie<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;In this crucial year as we approach the MDG deadline for achieving universal education, the new announcements today demonstrate Canada&#39;s clear leadership on global education and commitment to the 58 million out-of-school children. I am pleased that Minister Paradis has pledged to do even more to support education for millions of children in conflict-affected areas and humanitarian crises, including Syrian refugees in Lebanon and other neighboring countries.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Gordon Brown, UN Special Envoy for Global Education<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;We are delighted about Canada&rsquo;s contribution and we are grateful for this increased funding for the next four years and for the government&rsquo;s leadership on helping improve education in some of the poorest countries around the world, many of them torn by conflict.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Alice Albright, Chief Executive Officer of the Global Partnership for Education<br />\r\n<br />\r\n&nbsp; &nbsp; &ldquo;A child&rsquo;s right to education is most at risk during humanitarian crises. Yet those are precisely the times when education is most needed, offering hope, protection and a vital sense of normalcy for children whose lives have been devastated by conflict or natural disaster. That&rsquo;s why we are so grateful to the Canadian government and people for their continued, generous support to UNICEF&#39;s education in emergencies programs.&rdquo;<br />\r\n<br />\r\n&nbsp; &nbsp; -Anthony Lake, Executive Director for UNICEF.', '/uploads/media/115/news/FATDC-Tanzanian school children greet Prime Minister Harper.jpg', 'Tanzanian school children greet Prime Minister Harper - Tanzania, Nov. 26, 2007.', 'TOM HANSON / The Canadian Press', 0, 1, 0, 1, '2015-05-21 07:53:20', '2015-06-11 16:07:12', 63, 115, 115, 0),
(109, 'CCE3D778-2CA8-4381-8A7C-E8BA0C76F776', 'Millions of Yemenis face food insecurity amidst escalating conflict. ', 'millions-of-yemenis-face-food-insecurity-amidst-escalating-conflict-more-than-8-million-needed-to-support-farmers-during-crucial-cropping-season', '15 April 2015, Rome - Amidst escalating conflict at a crucial time in the country&rsquo;s cropping season, almost 11 million people in Yemen are severely food insecure and millions more are at risk of not meeting their basic food needs, FAO said today.<br />\r\n<br />\r\nAccording to the organization&#39;s latest assessment, increasing conflict in nearly all major towns across the country is disrupting markets and trade, driving up local food prices and hampering agricultural production, including land preparation and planting for the 2015 maize and sorghum harvests.<br />\r\n<br />\r\n10.6 million Yemenis are now severely food insecure, of which 4.8 million are facing &quot;emergency&quot; conditions, suffering from severe lack of food access, very high and increasing malnutrition, and irreversible destruction of livelihoods.<br />\r\n<br />\r\nAround 850,000 children are acutely malnourished.<br />\r\n<br />\r\nMore than half of Yemen&rsquo;s population &ndash; some 16 million out of a total of 26 million&mdash; is in need of some form of humanitarian aid and has no access to safe water.<br />\r\n<br />\r\nThe latest escalation of conflicts is expected to further increase food insecurity in the poverty-stricken country. Paradoxically, some 2.5 million food producers, including farmers, pastoralists, fishermen and agricultural wage labourers, are among those identified as food insecure.<br />\r\n<br />\r\n&ldquo;We are entering a crucial period for crop production in Yemen and now, more than ever, agriculture cannot be an afterthought if we want to prevent more people from becoming food insecure amidst this crisis,&rdquo; said FAO Representative for Yemen, Salah Hajj Hassan.<br />\r\n<br />\r\nGovernorates in the far Northwest and South are most severely affected by food insecurity.<br />\r\n<br />\r\nMarket disruption<br />\r\n<br />\r\nIn some areas, like the western port city of Hodeidah, food prices have doubled and fuel prices have quadrupled. Further increases are expected as a result of fuel shortages and the impact of civil unrest on imports and transportation networks across Yemen. While agriculture provides the livelihood of nearly two-thirds of Yemenis, the country also relies heavily on imports of staple crops.<br />\r\n<br />\r\nAt the same time, service infrastructure has collapsed and government safety net programs have been suspended, handing an extra blow to millions of poor households.<br />\r\n<br />\r\nCritical work in Yemen<br />\r\n<br />\r\nIn a very challenging field environment, FAO and partners have since 2014 been working to support local farmers and internally displaced people to strengthen their livelihoods by distributing crop production packages, home gardening kits and fisheries inputs. They have also provided vaccinated poultry and goats for backyard livestock production.<br />\r\n<br />\r\nAdditional animal vaccination drives and plant health campaigns have helped farmers protect their agricultural assets, such as livestock and trees, from disease and locust threats.<br />\r\n<br />\r\nSince 2014, more than 90,000 people (13,450 families) have benefited from these FAO programs. Security conditions permitting, the Organization aims to reach nearly 235,000 people through its 2014-15 response plan for Yemen, but more funding is needed. Currently, only $4 million of the required $12 million have been made available for the livelihood programs.<br />\r\n<br />\r\n&ldquo;Even before fighting intensified this spring, Yemenis were in dire need of support to build up their agricultural production,&rdquo; said Abdessalam Ould Ahmed, FAO Assistant Director-General for North Africa and the Near East. &ldquo;The deteriorating situation means we need to double down on our efforts to ensure that as many farmers as possible are able to plant this growing season and strengthen their ability to withstand future shocks.&rdquo;', '/uploads/media/114/news/FAO Food_Insecurity_Yemen.jpg', 'Escalation of conflict is expected to further increase food insecurity in the poverty-stricken Yemen.', '', 0, 1, 0, 1, '2015-05-21 08:00:32', '2015-06-11 16:59:00', 62, 114, 114, 1),
(110, '00C20A2F-9B49-43E8-BA3B-3A917C50E114', 'Didier Drogba to join Ronaldo and Zidane in 12th annual Match Against Poverty', 'didier-drogba-to-join-ronaldo-and-zidane-in-12th-annual-match-against-poverty', 'Proceeds to support Ebola recovery efforts<br />\r\n<br />\r\nGeneva / St-Etienne &ndash; Football superstar and Chelsea striker Didier Drogba has joined a star-studded array of international active and retired players for the 12th Annual Match Against Poverty which will take place on 20 April at the Geoffroy-Guichard Stadium in Saint-Etienne, France. Didier Drogba, who like Ronaldo and Zin&eacute;dine Zidane is also a UNDP Goodwill Ambassador, will team up with the other football stars for a match against an AS St-Etienne All Stars team to help boost Ebola recovery efforts.&nbsp;<br />\r\n<br />\r\nTwo thirds of the match proceeds will support UNDP&rsquo;s work in the hardest-hit countries of Guinea, Liberia and Sierra Leone, helping them to build back better from the epidemic. The remaining third will go to the Club&rsquo;s Association &ldquo;ASSE Coeur-Vert&rdquo;, asd which supports social projects in Saint-Etienne.&nbsp;<br />\r\n<br />\r\n&ldquo;I am honoured to support the people of the three countries who are trying to get through the devastation caused by the Ebola epidemic&rdquo;, said Drogba, &ldquo;and I encourage everyone to pull together to end this crisis, and to prevent it ever happening again.&rdquo; The Ivorian football legend joins the friendly match for the second time. &ldquo;I am also thrilled to be with Ronaldo, Zidane and all the other players to make a difference in the fight against Ebola.&rdquo;&nbsp;<br />\r\n<br />\r\nThe players confirmed up to now in the &ldquo;Drogba, Ronaldo, Zidane, &amp; Friends&rdquo; team are: Cafu, Clarence Seedorf, Edwin van der Sar, &Eacute;ric Abidal, Christian Karembeu, Youri Djorkaeff and Fabien Barthez. Former and current St. Etienne players will comprise the St Etienne All Stars team. Thirteen players have confirmed up to now: Alex, J&eacute;r&eacute;mie Janot, Pascal Feindouno, Dominique Rocheteau, Lionel Potillon, Ľubom&iacute;r Moravč&iacute;k, Laurent Paganelli, Bjorn Kvarme, Efstathios Tavlaridis, S&eacute;bastien Perez, St&eacute;phane P&eacute;dron, Julien Sabl&eacute; and Laurent Batlles. AS St Etienne&rsquo;s coach Christophe Galtier will coach the team. Pierluigi Collina, the legendary six-time World Referee winner, will referee the Match. More top international and AS Saint-Etienne names are expected to confirm their participation in the coming days.&nbsp;<br />\r\n<br />\r\nThe match, which will be televised globally, is supported by football&rsquo;s governing body, the F&eacute;d&eacute;ration Internationale de Football Association (FIFA) and organised in partnership with the Union des Associations Europ&eacute;ennes de Football (UEFA).&nbsp;<br />\r\n<br />\r\nThe 12th Match Against Poverty in St. Etienne will start at 20:00 local time. Ticket prices range from 8 to 12 Euros and are on sale at: http://www.asse.fr and ticket counters of the Stade Geoffroy-Guichard,14, rue Paul et Pierre Guichard, 42000 Saint-Etienne.<br />\r\nContact Information<br />\r\n<br />\r\nAziyad&eacute; Poltier-Mutal, UNDP in Geneva: aziyade.poltier@undp.org ; tel: +41 917 83 68, cell: +41 79 349 16 10&nbsp;', '/uploads/media/124/news/UNDP.jpg', 'Drogba, Ronaldo and Zidane at the Match Against Poverty in 2012', '', 0, 1, 0, 1, '2015-05-21 08:10:20', '2015-06-15 23:00:28', 72, 124, 124, 1),
(111, '7E97D9E2-240A-4130-B76D-75776B77FDF2', 'Uprooted by Boko Haram. Stories of displaced children and women in Nigeria', 'uprooted-by-boko-haram-stories-of-displaced-children-and-women-in-nigeria', 'Violent attacks carried out by members of the Boko Haram rebel group have resulted in unspeakable atrocities in north-eastern Nigeria. Children have seen their loved ones killed and watched as their homes and schools have been damaged or destroyed. Some have walked for days to escape, fleeing with nothing more than the clothes on their backs.<br />\r\nThese are the stories of children and women who have survived the conflict and sought refuge in encampments in Yola, Adamawa State. They are among more than 1.2 million people uprooted by the violence who remain internally displaced.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0474/Esiebo<br />\r\n<br />\r\n(Right) Rose Zeeharrah watched while members of Boko Haram attacked her village and began killing the men who lived there &mdash; including her husband. As she fled into the bush with her nine children, the last sight she saw was her home being set ablaze. &ldquo;We didn&rsquo;t bring anything with us. We just ran,&rdquo; she said. Her 2-year-old son passed away while they were in hiding. &ldquo;He died from the stress,&rdquo; Rose explained. Now she and her children are living in a camp for internally displaced people in Yola, Adamawa State. Even though she knows she has lost her home and cattle, Rose longs to go home. &ldquo;I want to go home and harvest so we can eat,&rdquo; she said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0475/Esiebo<br />\r\n<br />\r\nJohn, 17, was getting ready for church when members of Boko Haram attacked his village. He had already lost his father, a carpenter, and his mother to illness before the violence reached his hometown. Along with his aunt, John managed to escape to a displacement camp in Yola. What he misses most is his father&rsquo;s house, his village and the memories he made there. &ldquo;I want to go back there with my aunt,&rdquo; he said. John is attending a UNICEF-supported school in the camp and is determined to become a pastor and have a positive impact on his community. &ldquo;I want to work for God,&rdquo; he said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0476/Esiebo<br />\r\n<br />\r\nEvelyn was attending church when members of Boko Haram entered her hometown, in the Local Government Area of Michika, and began shooting and killing people, and kidnapping some of the girls from the community. Evelyn grabbed her 1-year-old daughter, Rose (above), and ran, but her 5-year-old son, Wisdom, was in another part of the church made festive with special decorations for children. &ldquo;It was children&rsquo;s day,&rdquo; Evelyn explained in a soft voice. She hid in the mountains with other children, women and men from her community. One week later, she was reunited with Wisdom, whom members of the church brought to her, but she has not seen or heard from her husband since the attack. &ldquo;I don&rsquo;t know if he is alive or dead,&rdquo; she said. With only the clothes on their backs, she and her children stayed in the mountains for one month, barely surviving on berries and swamp water, before making the journey to Yola, where they now live in a camp for displaced people. Evelyn, who studied economics before she married, longs to return home, but she knows it will be difficult. &ldquo;They have taken everything,&rdquo; she said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0477/Esiebo<br />\r\n<br />\r\nWhen members of Boko Haram attacked 13-year-old Aisha&rsquo;s hometown &mdash; Gwozo, in Borno State &mdash; they killed her father and abducted her mother. She managed to escape, fleeing to a displacement camp in Yola with an older sister. The two continue to look after each other. Aisha has received UNICEF-supported counselling. Now strong enough to return to learning, she attends a UNICEF-supported school, which operates in two shifts in the camp to accommodate more children. &ldquo;I enjoy the school here,&rdquo; she said, &ldquo;but I want to go back to my village.&rdquo;<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0479/Esiebo<br />\r\n<br />\r\nWhen Alia, 10, still lived in her hometown, in the Local Government Area of Michika, word came on a Friday that members of Boko Haram had attacked neighbouring villages. The next day, the men arrived in her town. Her father was killed. Alia, together with her mother and other family members, managed to flee to the town of Mubi, leaving all their belongings behind, but three months later, that town also came under attack. They then fled across the border to neighbouring Cameroon before slowly making their way back to Nigeria and ending up in a displacement camp in Yola, where Alia is attending a UNICEF-supported school. Her mother suffered from high blood pressure and diabetes before the attacks, and now she is very sick, adding to Alia&rsquo;s worries. Alia misses her father and her friends, and she is scared that attacks will occur again, but she refuses to give up hope. &ldquo;I want to be a nurse,&rdquo; she said. &ldquo;I want to help people.&rdquo;<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0478/Esiebo<br />\r\n<br />\r\nA few months back, members of Boko Haram arrived in Lydia James&rsquo;s hometown as her husband set out to visit his mother and take food to her. They tied him up, dragged him beneath a tree and shot him before turning their sights on Lydia&rsquo;s house. She and her nine children ran for their lives. &ldquo;I ask God to help me so I can take care of the children,&rdquo; she said. Lydia now shares a cramped dormitory with 78 other women and children in a camp for internally displaced people, in Yola.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0480/Esiebo<br />\r\n<br />\r\nMaryamu Yakudu normally would have been in church when members of Boko Haram attacked her hometown, in the Local Government Area of Michika, but she was home sick that day. When she heard gunshots, she grabbed her daughter, 1-year-old Hyladan Yakudu, and ran. She has not seen her husband since and fears the worst. Her mother, who was too frail to run, remained in the village. &ldquo;I just brought my one wrapper &mdash; nothing else,&rdquo; says Maryamu. She longs to return home, as soon as it is safe &mdash; even though she knows there is little to which to return. &ldquo;There is nothing in our house,&rdquo; she said. &ldquo;They took it all. We had a motorcycle and many cows. It&rsquo;s all gone.&rdquo;<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0481/Esiebo<br />\r\n<br />\r\nA few years ago, Samson, 16, lost his father, who was a member of government forces fighting against Boko Haram. When the news reached his mother, the shock was too much for her to handle &mdash; she became ill, and shortly after, she passed away, leaving Samson to be cared for by his grandmother. But the violence would soon catch up with Samson again. A few months ago, the church in his home village, in the Local Government Area of Michika, was attacked. Many of the men were killed, while women and children fled in panic. &ldquo;We just started running,&rdquo; recalled Samson. He and his grandmother arrived at a displacement camp in Yola, where Samson now attends a UNICEF-supported school, but he dreams of going home. &ldquo;I miss the house and all the people in the community, and I miss playing football with my friends,&rdquo; he said. He also misses feeling safe. Samson already knows that he wants to follow in his father&rsquo;s footsteps. &ldquo;I want to be a soldier,&rdquo; he said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0482/Esiebo<br />\r\n<br />\r\nTwo years ago, Hajja Bello was shopping in a marketplace in the village of Dagu, in Borno State, when members of Boko Haram attacked. &ldquo;It was a Sunday morning, and they assembled everyone and then separated the women from the men,&rdquo; she recalled. &ldquo;They shot the husbands before our eyes.&rdquo; She managed to escape with all five of her children, but she lost five relatives that day, including a brother and four half-brothers. With the support of UNICEF and partners, her children are attending school in the camp, but she misses everything about home &mdash; the farm, her neighbours, the marketplace and the other women there with whom she used to chat. &ldquo;I just want this to end so I can go back,&rdquo; she said.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0483/Esiebo<br />\r\n<br />\r\n&ldquo;When the shooting started, we ran in panic into the mountains,&rdquo; remembered Lydia John, 15, about the day members of Boko Haram attacked her village, Gidel in the Local Government Area of Michika. Residents could see their homes being burned and schools being damaged from the mountains, where they remained trapped for two weeks, occasionally using the cover of night to gather a bit of food. After they managed to leave, they walked for seven straight days to reach the town of Mubi and eventually made it to Yola, where Lydia now lives in a camp for internally displaced people. She misses being home on her family&rsquo;s farm, where there was plenty to eat, and like any adolescent, she misses her friends, who have been scattered across the country, with some fleeing to Abuja, the capital; six of her friends remain missing. Like so many others in the camp, she has lost loved ones in the violence; her uncle and cousin were killed during the attack on her village. &ldquo;I always feel scared when I think of what happened,&rdquo; she said. Lydia is attending school every day in Yola and wants to be a doctor. &ldquo;I want to help my community,&rdquo; she explained. But then the worry returns. With the family&rsquo;s farm lost and their animals, including sheep, stolen or slaughtered, she is unsure how she, her parents and eight siblings will have enough to survive. &ldquo;It&rsquo;s not only what we will eat &mdash; but if all of that is lost, how will we have enough money for me to go to school?&rdquo; she asked.<br />\r\n&copy; UNICEF/NYHQ2015&ndash;0484/Esiebo<br />\r\n<br />\r\nSalamatu Chinaypi used to sell food in a local market to supplement the income her family earned from their farm &mdash; helping feed her nine children and send them all to school. But that, along with the family&rsquo;s home and some of their loved ones, was all lost when members of Boko Haram attacked Salamatu&rsquo;s hometown, Askira Uba, in Borno State. They separated the men from the women and then began killing the men, including Salamatu&rsquo;s brother. &ldquo;That&rsquo;s when everyone started running,&rdquo; she said. She and a surviving brother began to flee, but a shot rang out, and he, too, was killed. &ldquo;They burned our houses. There is nothing left,&rdquo; she said. She, her husband and their children managed to escape, and it took them three weeks and four days to reach Yola, where they now live in a camp for internally displaced people. &ldquo;We came barefooted,&rdquo; she said. &ldquo;We could bring nothing with us.&rdquo;<br />\r\n<br />\r\n<br />\r\nWhat would you miss most if you were forced to flee your home?<br />\r\nJoin the #BringBackOurChildhood campaign.', '/uploads/media/125/news/UNICEF.jpg', '', '', 0, 1, 0, 1, '2015-05-21 08:21:26', '2015-06-15 23:00:45', 73, 125, 125, 4),
(112, '7DE33FE2-9E38-4EAA-8F27-9089EFB40F07', 'USAID Announces $126 Million to Rebuild Life-Saving Health Services in Ebola-Affected Countries', 'usaid-announces-126-million-to-rebuild-life-saving-health-services-in-ebola-affected-countries', 'Saturday, April 18, 2015. Washington, DC: U.S. Agency for International Development (USAID) Associate Administrator Mark Feierstein announced plans to spend $126 million to help rebuild West African health systems impacted by the Ebola outbreak at the Global Citizen Concert on the National Mall. &nbsp;The funds will help Liberia, Sierra Leone, and Guinea restart critical health services that stopped due to the Ebola outbreak, including vaccinations, water and sanitation services, prenatal and maternal health care and nutrition, and programs to prevent and treat malaria and other infectious diseases.<br />\r\n<br />\r\nIn September last year, epidemiologists predicted the Ebola outbreak could result in 1.4 million cases within a few months, and the first case of Ebola landed in the U.S. homeland. &nbsp; The U.S. Government responded by providing $1.4 billion to fund 10,000 civilian responders, provide huge volumes of personal protective equipment, fund and train healthcare workers, deploy laboratories, support epidemiological surveillance and disease tracing, and launch a massive social messaging and education campaign to inform Liberians about practices to protect themselves against infection.<br />\r\n<br />\r\n&ldquo;USAID has helped West African nations by beating back the Ebola outbreak,&rdquo; said Associate Administrator Mark Feierstein. &nbsp;&ldquo;Now we&rsquo;re helping ensure people have food to eat, schools are open and educating children, people are able to communicate through a strong infrastructure network, and families can support themselves by returning to jobs and markets.&rdquo;<br />\r\n<br />\r\nThe U.S. Government and its partners helped Liberia&mdash;once the heart of the epidemic&mdash;bring down new cases of Ebola from more than 50 a day to zero.<br />\r\n', '/uploads/media/127/news/USAID.jpg', 'Health care workers put on personal protective equipment (PPE) before going into the hot zone at Island Clinic in Monrovia, Liberia on Sept. 22 2014.', 'Morgana Wingard, USAID', 0, 1, 0, 1, '2015-05-21 08:38:47', '2015-06-06 07:23:41', 75, 127, 127, 5),
(113, '80497EA6-4EA7-4305-9F2A-019F35645270', 'Caribbean and Central American countries formalize partnership for catastrophe risk insurance', 'caribbean-and-central-american-countries-formalize-partnership-for-catastrophe-risk-insurance', 'Nicaragua is the first Central American country to sign up to the insurance&nbsp;<br />\r\n<br />\r\nWASHINGTON, April 18, 2015 - The Council of Ministers of Finance of Central America, Panama and the Dominican Republic (COSEFIN) and CCRIF SPC (formerly the Caribbean Catastrophe Risk Insurance Facility) signed today a memorandum of understanding that enables Central American countries to formally join the facility to access low cost, high quality sovereign catastrophe risk insurance.<br />\r\n<br />\r\nDuring the ceremony, CCRIF SPC and the Government of Nicaragua also signed a Participation Agreement for Nicaragua to become the first Central American country to formally join the facility. Other member nations of COSEFIN are expected to join CCRIF SPC later this year and in 2016.<br />\r\n<br />\r\n&ldquo;For Nicaragua, it is an honor to be the first member of COSEFIN countries to join the CCRIF. This insurance will allow us to strengthen financial resilience to natural disasters and continue our efforts to reduce poverty and respond to climate change challenges as part of our National Human Development Plan,&rdquo; said Ivan Acosta, Minister of Finance of Nicaragua.<br />\r\n<br />\r\nNine countries in Central America and the Caribbean experienced at least one disaster with an economic impact of more than 50 percent of their annual gross domestic product (GDP) since 1980. The impact of Haiti&rsquo;s earthquake was estimated at 120 percent of GDP. The same year, tropical cyclone Agatha, in Guatemala, had devastating consequences and poverty rates increased by 5.5 percent. Climate change also represents a significant development challenge, with average annual economic losses due to weather-related disasters amounting to 1 percent or more of GDP in ten Caribbean countries and four Central American nations, including Nicaragua.<br />\r\n<br />\r\nEstablished in 2007, CCRIF is the world&rsquo;s first multi-country catastrophe risk pooling mechanism which offers sovereign insurance at affordable rates to its members against hurricanes, earthquakes and excess rainfall. Currently, 16 Caribbean countries are members of CCRIF.<br />\r\n<br />\r\nThe facility enhances the fiscal resilience of its member countries to catastrophes caused by natural hazard events by providing immediate financial resources in the aftermath of a disaster, allowing governments to better respond to the initial needs of their populations and continue providing critical services. Since its inception, CCRIF has made twelve payouts totaling US$35.6 million to eight member governments. All payouts were transferred within two weeks after each event.<br />\r\n<br />\r\n&ldquo;After exploring options for engaging in sovereign disaster risk financing, Central American countries concluded that joining the CCRIF SPC facility was the most efficient and cost-effective insurance mechanism to pool our risk&rdquo;, said Mart&iacute;n Portillo, Executive Secretary of COSEFIN. &ldquo;This will allow us to reduce our countries&rsquo; fiscal vulnerability to the adverse effects associated with earthquakes, tropical cyclones, excess rainfall and other events.&rdquo;<br />\r\n<br />\r\nThis new 23-nation partnership will benefit both existing and new CCRIF members, providing low prices due to more efficient use of capital and insurance market instruments. New members will be able to take advantage of CCRIF&rsquo;s low premium costs and existing members could realize premium reductions due to the increased size of the CCRIF portfolio. This partnership between Caribbean and Central American countries could strengthen economic engagement throughout the greater Caribbean Basin.<br />\r\n<br />\r\n&ldquo;We foresee a range of benefits to both regions emanating from this partnership,&rdquo; said Milo Pearson, Chairman of CCRIF SPC, &ldquo;including the creation of a strategic mechanism in which Caribbean and COSEFIN countries can share best practices in disaster risk management and learn lessons from each other in advancing collaborative approaches to reducing vulnerabilities to hazards, alleviating poverty, enhancing debt and financial sustainability and creating a framework for the sustainable prosperity of the countries of both regions.&rdquo;<br />\r\n<br />\r\nThe World Bank provided the initial finance and technical advisory services to establish CCRIF in 2007 and in 2014 the Bank supplied resources to finance entrance fees to CCRIF for Honduras and Nicaragua, as well as annual insurance premiums for four years.<br />\r\n<br />\r\n&ldquo;This a real example of a regional public good where collective action has clear financial benefits and can help countries tackle the adverse impacts of climate change,&rdquo;, said Jorge Familiar, World Bank Vice President for Latin America and the Caribbean. &ldquo;We look forward to deepening our engagement with COSEFIN, CARICOM and CCRIF as part of this ambitious regional initiative.&rdquo;<br />\r\n<br />\r\nAbout CCRIF SPC<br />\r\n<br />\r\nCCRIF was developed under the technical leadership of the World Bank and with a grant from the Government of Japan. It was capitalized through contributions to a multi-donor Trust Fund by the Government of Canada, the European Union, the World Bank, the governments of the United Kingdom and France, the Caribbean Development Bank and the governments of Ireland and Bermuda, as well as through membership fees paid by participating governments', '/uploads/media/128/news/WB-10-major-natural-disasters-predicted-soon-427002.jpg', '', 'www.gfdrr.org', 0, 1, 0, 1, '2015-05-21 08:51:09', '2015-06-15 14:32:33', 76, 128, 128, 6);

-- --------------------------------------------------------

--
-- Table structure for table `news_items_beneficiaries`
--

CREATE TABLE IF NOT EXISTS `news_items_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_beneficiaries_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_beneficiaries_beneficiary_id_index` (`beneficiary_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=128 ;

--
-- Dumping data for table `news_items_beneficiaries`
--

INSERT INTO `news_items_beneficiaries` (`id`, `newsitem_id`, `beneficiary_id`, `created_at`, `updated_at`) VALUES
(107, 105, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 111, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 112, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 110, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 108, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 107, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 109, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 113, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 106, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 104, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news_items_countries`
--

CREATE TABLE IF NOT EXISTS `news_items_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_countries_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_countries_country_id_index` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=122 ;

--
-- Dumping data for table `news_items_countries`
--

INSERT INTO `news_items_countries` (`id`, `newsitem_id`, `country_id`, `created_at`, `updated_at`) VALUES
(110, 111, 158, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 110, 74, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 107, 224, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 109, 236, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 113, 156, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 106, 229, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 104, 223, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news_items_crisis`
--

CREATE TABLE IF NOT EXISTS `news_items_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_crisis_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_crisis_crisis_id_index` (`crisis_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=123 ;

--
-- Dumping data for table `news_items_crisis`
--

INSERT INTO `news_items_crisis` (`id`, `newsitem_id`, `crisis_id`, `created_at`, `updated_at`) VALUES
(103, 105, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 111, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 112, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 110, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 108, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 107, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 109, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 113, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 106, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news_items_interventions`
--

CREATE TABLE IF NOT EXISTS `news_items_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_interventions_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_interventions_intervention_id_index` (`intervention_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_items_regions`
--

CREATE TABLE IF NOT EXISTS `news_items_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_regions_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_regions_region_id_index` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=129 ;

--
-- Dumping data for table `news_items_regions`
--

INSERT INTO `news_items_regions` (`id`, `newsitem_id`, `region_id`, `created_at`, `updated_at`) VALUES
(108, 105, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 111, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 112, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 110, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 108, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 107, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 109, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 113, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 106, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 104, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news_items_sectors`
--

CREATE TABLE IF NOT EXISTS `news_items_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_sectors_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_sectors_sector_id_index` (`sector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=123 ;

--
-- Dumping data for table `news_items_sectors`
--

INSERT INTO `news_items_sectors` (`id`, `newsitem_id`, `sector_id`, `created_at`, `updated_at`) VALUES
(106, 105, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 112, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 108, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 107, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 109, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 113, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 106, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 104, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news_items_themes`
--

CREATE TABLE IF NOT EXISTS `news_items_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsitem_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_items_themes_newsitem_id_index` (`newsitem_id`),
  KEY `news_items_themes_theme_id_index` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=128 ;

--
-- Dumping data for table `news_items_themes`
--

INSERT INTO `news_items_themes` (`id`, `newsitem_id`, `theme_id`, `created_at`, `updated_at`) VALUES
(107, 105, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 111, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 112, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 110, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 108, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 107, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 109, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 113, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 106, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 104, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `office_locations`
--

CREATE TABLE IF NOT EXISTS `office_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `office_types`
--

CREATE TABLE IF NOT EXISTS `office_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `office_types`
--

INSERT INTO `office_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Headquarter', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(2, 'Regional Office', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(3, 'Country Office', '2015-05-04 16:59:42', '2015-05-04 16:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE IF NOT EXISTS `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_id` int(10) unsigned NOT NULL,
  `org_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `org_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `office_location` int(10) unsigned DEFAULT NULL,
  `office_type` int(10) unsigned DEFAULT NULL,
  `org_type` int(10) unsigned DEFAULT NULL,
  `top_agency` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8_unicode_ci,
  `org_name_acronym` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_org_name_acronym` tinyint(1) DEFAULT '0',
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `org_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_of_activity` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `organisations_cp_id_index` (`cp_id`),
  KEY `organisations_level_of_activity_index` (`level_of_activity`),
  KEY `organisations_office_location_index` (`office_location`),
  KEY `organisations_office_type_index` (`office_type`),
  KEY `organisations_org_type_index` (`org_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=82 ;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `cp_id`, `org_logo`, `org_name`, `org_email`, `org_website`, `office_location`, `office_type`, `org_type`, `top_agency`, `is_active`, `created_at`, `updated_at`, `description`, `org_name_acronym`, `use_org_name_acronym`, `facebook_url`, `twitter_url`, `org_phone`, `level_of_activity`) VALUES
(54, 105, '/uploads/media/105/ADB2.jpg', 'Asian Development Bank', 'rogermarkowski@hotmail.com', 'http://adb.org', 170, NULL, 9, 1, 1, '2015-05-19 04:28:10', '2015-05-19 05:38:45', 'The Asian Development Bank aims for an Asia and Pacific free from poverty. Approximately 1.7 billion people in the region are poor and unable to access essential goods, services, assets and opportunities to which every human is entitled. \r\n', '', 0, 'http://www.facebook.com/sharer.php?u=http%3A//www.adb.org', 'http://twitter.com/share?url=http%3A//www.adb.org/', '+62 0000', NULL),
(55, 106, '/uploads/media/106/Amnesty.jpg', 'Amnesty International', 'rogermarkowski@hotmail.com', 'http://https://www.amnesty.org/en/', 224, NULL, 8, 1, 1, '2015-05-19 05:57:01', '2015-05-19 06:09:35', 'Amnesty International is a global movement of more than 7 million people who take injustice personally. We are campaigning for a world where human rights are enjoyed by all.\r\n', '', 0, 'http://', 'http://', '+62 000', NULL),
(57, 108, '/uploads/media/108/AUSAID1.png', 'Australian Aid', 'rogermarkowski@hotmail.com', 'http://dfat.gov.au', 13, NULL, 7, 1, 1, '2015-05-19 10:30:53', '2015-05-19 17:46:35', 'The department’s purpose is to help make Australia stronger, safer and more prosperous by promoting and protecting our interests internationally and contributing to global stability and economic growth.\r\n', '', 0, 'http://', 'http://', '+62 0000', NULL),
(58, 110, '/uploads/media/110/ACF.jpg', 'ACF International', 'rogermarkowski@hotmail.com', 'http://www.actionagainsthunger.org/about/acf-international', 225, NULL, 8, 1, 1, '2015-05-19 17:49:36', '2015-05-19 18:14:08', 'Action Against Hunger | ACF International, a global humanitarian organization committed to ending world hunger, works to save the lives of malnourished children while providing communities with access to safe water and sustainable solutions to hunger. ', '', 0, 'http://', 'http://', '000', NULL),
(59, 111, '/uploads/media/111/CARE.gif', 'Care International', 'rogermarkowski@hotmail.com', 'http://www.care-international.org/', 206, NULL, 8, 1, 1, '2015-05-19 17:50:55', '2015-05-19 18:24:20', 'CARE is a leading humanitarian organization fighting global poverty. We place special focus on working alongside poor women because, equipped with the proper resources, women have the power to help whole families and entire communities escape poverty. Women are at the heart of our community-based efforts to improve basic education, prevent the spread of disease, increase access to clean water and sanitation, expand economic opportunity and protect natural resources. We also deliver emergency aid to survivors of war and natural disasters, and help people rebuild their lives.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(60, 112, '/uploads/media/112/Caritas.jpg', 'Caritas Internationalis', 'rogermarkowski@hotmail.com', 'http://caritas.org', 107, NULL, 8, 1, 1, '2015-05-19 18:15:35', '2015-05-19 18:24:38', 'Caritas shares the mission of the Catholic Church to serve the poor and to promote charity and justice throughout the world. \r\n', '', 0, 'http://', 'http://', '000', NULL),
(61, 113, '/uploads/media/113/DFID.jpeg', 'The Department for International Development ', 'rogermarkowski@hotmail.com', 'http://gov.uk/government/organisations/department-for-international-development', 224, NULL, 7, 1, 1, '2015-05-19 18:16:46', '2015-05-19 18:24:06', 'The Department for International Development (DFID) leads the UK’s work to end extreme poverty. We''re ending the need for aid by creating jobs, unlocking the potential of girls and women and helping to save lives when humanitarian emergencies hit.\r\n', 'DFID', 1, 'http://', 'http://', '000', NULL),
(62, 114, '/uploads/media/114/FAO.gif', 'Food and Agriculture Organizaion of the United Nations', 'rogermarkowski@hotmail.com', 'http://www.fao.org/home/en', 107, NULL, 9, 1, 1, '2015-05-19 18:25:21', '2015-05-19 18:38:16', 'Achieving food security for all is at the heart of FAO''s efforts – to make sure people have regular access to enough high-quality food to lead active, healthy lives.  \r\nOur three main goals are: the eradication of hunger, food insecurity and malnutrition; the elimination of poverty and the driving forward of economic and social progress for all; and, the sustainable management and utilization of natural resources, including land, water, air, climate and genetic resources for the benefit of present and future generations.\r\n', 'FAO', 1, 'http://', 'http://', '000', NULL),
(63, 115, '/uploads/media/115/FATDC.jpg', 'Foreign Affairs, Trade and Development Canada', 'rogermarkowski@hotmail.com', 'http://www.international.gc.ca/international/index.aspx?lang=eng', 38, NULL, 7, 1, 1, '2015-05-19 18:27:52', '2015-05-19 18:38:19', 'The mandate of Foreign Affairs, Trade and Development Canada is to manage Canada''s diplomatic and consular relations, to encourage the country''s international trade and to lead Canada’s international development and humanitarian assistance.\r\n', 'FATDC', 1, 'http://', 'http://', '000', NULL),
(64, 116, '/uploads/media/116/Greenpeace.jpg', 'Greenpeace', 'rogermarkowski@hotmail.com', 'http://www.greenpeace.org/international/en/', 152, NULL, 8, 1, 1, '2015-05-19 18:42:07', '2015-05-19 18:56:27', 'Greenpeace exists because this fragile earth deserves a voice. It needs solutions. It needs change. It needs action.\r\nGreenpeace is an independent global campaigning organisation that acts to change attitudes and behaviour, to protect and conserve the environment and to promote peace.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(65, 117, '/uploads/media/117/ICRC.jpg', 'International Committee of the Red Cross', 'rogermarkowski@hotmail.com', 'http://www.icrc.org/en', 206, NULL, 9, 1, 1, '2015-05-19 18:42:40', '2015-05-19 18:56:26', 'The International Red Cross and Red Crescent Movement is the largest humanitarian network in the world. Its mission is to alleviate human suffering, protect life and health, and uphold human dignity especially during armed conflicts and other emergencies. It is present in every country and supported by millions of volunteers.\r\n', 'ICRC', 1, 'http://', 'http://', '000', NULL),
(66, 118, '/uploads/media/118/MDM.JPG', 'Doctors of the World', 'rogermarkowski@hotmail.com', 'http://doctorsoftheworld.org/international-network/', 225, NULL, 8, 1, 1, '2015-05-19 18:58:05', '2015-05-19 19:15:39', 'Doctors of the World is an international humanitarian organization that provides emergency and long-term medical care to vulnerable populations while fighting for equal access to healthcare worldwide.', '', 0, 'http://', 'http://', '000', NULL),
(67, 119, '/uploads/media/119/MSF.jpg', 'Medecins Sans Frontieres International', 'rogermarkowski@hotmail.com', 'http://msf.org', 206, NULL, 8, 1, 1, '2015-05-19 18:58:30', '2015-05-19 19:15:35', 'Médecins Sans Frontières (MSF) is an international, independent, medical humanitarian organisation that delivers emergency aid to people affected by armed conflict, epidemics, natural disasters and exclusion from healthcare. MSF offers assistance to people based on need, irrespective of race, religion, gender or political affiliation.\r\n', 'MSF International', 1, 'http://', 'http://', '000', NULL),
(68, 120, '/uploads/media/120/IOM.png', 'International Organization for Migration', 'rogermarkowski@hotmail.com', 'http://www.iom.int/cms/home', 206, NULL, 8, 1, 1, '2015-05-19 19:20:20', '2015-05-19 19:31:43', 'IOM is committed to the principle that humane and orderly migration benefits migrants and society.\r\nAs the leading international organization for migration, IOM acts with its partners in the international community to:\r\nAssist in meeting the growing operational challenges of migration management. \r\nAdvance understanding of migration issues. \r\nEncourage social and economic development through migration. \r\nUphold the human dignity and well-being of migrants.', 'IOM', 0, 'http://', 'http://', '000', NULL),
(69, 121, '/uploads/media/121/Oxfam.jpg', 'Oxfam International', 'rogermarkowski@hotmail.com', 'http://oxfam.org', 224, NULL, 8, 1, 1, '2015-05-19 19:20:58', '2015-05-19 19:33:21', 'Our vision is a just world without poverty. We want a world where people are valued and treated equally, enjoy their rights as full citizens, and can influence decisions affecting their lives.\r\n\r\nOur purpose is to help create lasting solutions to the injustice of poverty. We are part of a global movement for change, empowering people to create a future that is secure, just, and free from poverty.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(70, 122, '/uploads/media/122/PADF.jpg', 'Pan American Development Foundation', 'rogermarkowski@hotmail.com', 'http://padf.org', 225, NULL, 8, 1, 1, '2015-05-19 19:34:41', '2015-05-19 19:41:49', 'The Mission of the Pan American Development Foundation is to assist vulnerable and excluded people and communities in the Americas to achieve sustainable economic and social progress, strengthen their communities and civil society, promote democratic participation and inclusion, and prepare for and respond to natural disasters and other humanitarian crises, there by advancing the principles of the Organization of American States and creating a Hemisphere of Opportunity for All.\r\n', 'PADF', 0, 'http://', 'http://', '000', NULL),
(71, 123, '/uploads/media/123/PLAN.jpg', 'Plan International', 'rogermarkowski@hotmail.com', 'http://plan-international.org/', 225, NULL, 8, 1, 1, '2015-05-19 19:36:02', '2015-05-19 19:41:50', 'Plan aims to achieve lasting improvements in the quality of life of deprived children in developing countries, through a process that unites people across cultures and adds meaning and value to their lives.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(72, 124, '/uploads/media/124/UNDP.jpg', 'United Nations Development Programme', 'rogermarkowski@hotmail.com', 'http://undp.org', 225, NULL, 9, 1, 1, '2015-05-19 19:42:39', '2015-05-19 19:50:40', 'UNDP works in more than 170 countries and territories, helping to achieve the eradication of poverty, and the reduction of inequalities and exclusion. We help countries to develop policies, leadership skills, partnering abilities, institutional capabilities and build resilience in order to sustain development results. \r\n', 'UNDP', 1, 'http://', 'http://', '000', NULL),
(73, 125, '/uploads/media/125/UNICEF.jpg', 'United Nations Children''s Fund', 'rogermarkowski@hotmail.com', 'http://unicef.org', 225, NULL, 9, 1, 1, '2015-05-19 19:43:25', '2015-05-19 19:50:31', 'UNICEF is mandated by the United Nations General Assembly to advocate for the protection of children''s rights, to help meet their basic needs and to expand their opportunities to reach their full potential. \r\n', 'UNICEF', 1, 'http://', 'http://', '000', NULL),
(74, 126, '/uploads/media/126/Unilever.jpeg', 'Unilever', 'rogermarkowski@hotmail.com', 'http://unilever.com', 224, NULL, 3, 1, 1, '2015-05-19 19:52:15', '2015-05-19 19:57:48', 'With more than 400 brands focused on health and wellbeing, no company touches so many people’s lives in so many different ways.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(75, 127, '/uploads/media/127/USAID_1.jpg', 'USAID', 'rogermarkowski@hotmail.com', 'http://usaid.gov', 225, NULL, 7, 1, 1, '2015-05-19 19:52:56', '2015-05-19 19:58:18', 'USAID is the lead U.S. Government agency that works to end extreme global poverty and enable resilient, democratic societies to realize their potential.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(76, 128, '/uploads/media/128/WB3.jpg', 'World Bank', 'rogermarkowski@hotmail.com', 'http://worldbank.org', 225, NULL, 9, 1, 1, '2015-05-19 20:02:00', '2015-05-19 20:08:25', 'The World Bank Group has two ambitious goals:\r\nEnd extreme poverty within a generation and boost shared prosperity.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(77, 129, '/uploads/media/129/WHO.png', 'World Health Organization', 'rogermarkowski@hotmail.com', 'http://who.int', 206, NULL, 9, 1, 1, '2015-05-19 20:02:36', '2015-05-19 20:08:24', 'WHO is the directing and coordinating authority for health within the United Nations system. It is responsible for providing leadership on global health matters, shaping the health research agenda, setting norms and standards, articulating evidence-based policy options, providing technical support to countries and monitoring and assessing health trends.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(78, 130, '/uploads/media/130/WWF.png', 'World Wild Fund ', 'rogermarkowski@hotmail.com', 'http://pand.org', 206, NULL, 8, 1, 1, '2015-05-20 05:50:04', '2015-05-20 05:55:29', 'WWF was born into this world in 1961. It was the product of a deep concern held by a few eminent gentlemen who were worried by what they saw happening in our world at that time. Since those early days WWF has grown up to be one of the largest environmental organizations in the world. \r\n', 'WWF', 0, 'http://', 'http://', '000', NULL),
(79, 133, '/uploads/media/133/WTO1.jpg', 'World Trade Organization', 'rogermarkowski@hotmail.com', 'http://wto.org', 206, NULL, 9, 1, 1, '2015-05-21 12:08:11', '2015-05-21 12:19:17', 'The World Trade Organization (WTO) deals with the global rules of trade between nations. Its main function is to ensure that trade flows as smoothly, predictably and freely as possible.\r\n', '', 0, 'http://', 'http://', '000', NULL),
(80, 136, '/uploads/media/136/fleava.png', 'Fleava', 'info@fleava.com', 'http://fleava.com', 102, NULL, 3, 0, 1, '2015-05-25 16:04:01', '2015-05-25 16:10:55', '', '', 0, 'http://', 'http://', '123', NULL),
(81, 138, '/images/noimage.png', '', '', '', NULL, NULL, NULL, 0, 1, '2015-05-25 18:59:08', '2015-05-25 18:59:08', NULL, NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organisation_types`
--

CREATE TABLE IF NOT EXISTS `organisation_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `organisation_types`
--

INSERT INTO `organisation_types` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'Academic, training, and research institution', 'Research/Innovation', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(2, 'Civil society organisation', 'Civil Society Organisation', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(3, 'Corporation', 'Corporation', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(4, 'Development consulting firm', 'Development Consulting Firm', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(5, 'Finance institution', 'Finance Institution', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(6, 'Foundation', 'Foundation', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(7, 'Government institution', 'Government Institution', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(8, 'International non governmental organisation', 'INGO', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(9, 'International organisation (e.g. UN agencies, multilateral donors)', 'International Organisation', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(10, 'Media & other information provider', 'Media & Other Information Provider', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(11, 'National non governmental organisation', 'LNGO', '2015-05-04 16:59:43', '2015-05-04 16:59:43'),
(12, 'Private sector support organisation', 'Private Sector Support Organisation', '2015-05-04 16:59:43', '2015-05-04 16:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pepper` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_username_index` (`username`),
  KEY `password_reminders_token_index` (`token`),
  KEY `password_reminders_pepper_index` (`pepper`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `platforms`
--

CREATE TABLE IF NOT EXISTS `platforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `related_country_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data for table `platforms`
--

INSERT INTO `platforms` (`id`, `name`, `display_text`, `related_country_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 'International', 'International', 0, 1, '2015-05-18 09:45:23', '2015-05-18 15:59:00'),
(2, 'Indonesia', 'Indonesia', 102, 1, '2015-05-29 19:58:00', '2015-05-29 19:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `platforms_top_profiles`
--

CREATE TABLE IF NOT EXISTS `platforms_top_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `profile_type` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=31 ;

--
-- Dumping data for table `platforms_top_profiles`
--

INSERT INTO `platforms_top_profiles` (`id`, `profile_id`, `profile_type`, `platform_id`, `created_at`, `updated_at`) VALUES
(1, 54, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(2, 55, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(3, 57, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(4, 58, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(5, 59, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(6, 60, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(7, 61, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(8, 62, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(9, 63, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(10, 64, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(11, 65, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(12, 66, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(13, 67, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(14, 68, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(15, 69, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(16, 70, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(17, 71, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(18, 72, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(19, 73, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(20, 74, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(21, 75, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(22, 76, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(23, 77, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(24, 78, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(25, 79, 2, 1, '2015-05-22 01:46:30', '0000-00-00 00:00:00'),
(26, 134, 1, 1, '2015-05-22 10:11:05', '0000-00-00 00:00:00'),
(27, 131, 1, 1, '2015-05-22 10:12:04', '0000-00-00 00:00:00'),
(28, 135, 1, 1, '2015-05-22 10:12:06', '0000-00-00 00:00:00'),
(29, 132, 1, 1, '2015-05-22 10:12:09', '0000-00-00 00:00:00'),
(30, 109, 1, 1, '2015-05-22 10:12:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `professional_statuses`
--

CREATE TABLE IF NOT EXISTS `professional_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `professional_statuses`
--

INSERT INTO `professional_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Student', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(2, 'Active Professionally', '2015-05-04 16:59:42', '2015-05-04 16:59:42'),
(3, 'Retired', '2015-05-04 16:59:42', '2015-05-04 16:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `publicities`
--

CREATE TABLE IF NOT EXISTS `publicities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `a_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_title_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_text` text COLLATE utf8_unicode_ci,
  `a_text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_button_text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noimage.png',
  `a_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noimage.png',
  `b_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noimage.png',
  `c_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `a_show_org_logo` tinyint(1) NOT NULL DEFAULT '1',
  `published_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published_interval` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P0Y7M0DT0H0M0S',
  `published_interval_in_seconds` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `publicities_organisation_id_index` (`organisation_id`),
  KEY `publicities_type_id_index` (`type_id`),
  KEY `publicities_user_id_index` (`user_id`),
  KEY `publicities_user_profile_id_index` (`user_profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=61 ;

--
-- Dumping data for table `publicities`
--

INSERT INTO `publicities` (`id`, `a_title`, `a_title_color`, `a_text`, `a_text_color`, `a_button_text`, `a_button_text_color`, `a_position`, `a_image`, `a_url`, `b_title`, `b_image`, `b_url`, `c_title`, `c_image`, `c_url`, `type_id`, `published`, `deleted`, `featured`, `created_at`, `updated_at`, `organisation_id`, `user_profile_id`, `user_id`, `name`, `published_at`, `a_show_org_logo`, `published_start_date`, `published_interval`, `published_interval_in_seconds`) VALUES
(51, 'Adopt a Polar Bear', '#222222', 'As their ice habitat shrinks, skinnier and hungrier bears face a grave challenge to their survival.', '#222222', 'Learn More', '#222222', 'LeftTop', '/uploads/media/130/publicities/WWF.png', 'http://gifts.worldwildlife.org/gift-center/gifts/Species-Adoptions/Polar-Bear.aspx?sc=AWY1302WC922&_ga=1.150197874.599198513.1425506468', '', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 1, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-20 06:12:09', 78, 130, 130, 'Adopt a Polar Bear', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(52, '', '#222222', '', '#222222', '', '#222222', 'LeftMiddle', '/images/noimage.png', 'http://', 'Donate', '/uploads/media/119/publicities/banner_2.jpg', 'http://www.msf.org/donate', '', '/images/noimage.png', 'http://', 2, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-20 06:49:48', 67, 119, 119, 'Donate', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(53, '', '#222222', '', '#222222', '', '#222222', 'LeftMiddle', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 'Byke for CARE', '/uploads/media/111/publicities/CARE UK_Bike Ride.JPG', 'http://www.care-international.org/take-action.aspx', 3, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-20 06:56:21', 59, 111, 111, 'Byke for CARE', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(54, 'South Sudan: new fighting will bring new suffering', '#ffffff', 'Your donation can help deliver urgently needed food and supplies in South Sudan.', '#ffffff', 'Send emergency aid', '#222222', 'LeftMiddle', '/uploads/media/117/publicities/ICRC- south-sudan.jpg', 'http://www.icrc.org/eng/donations/?o=420016', '', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 1, 1, 0, 0, '2015-06-24 15:53:48', '2015-06-06 07:42:40', 65, 117, 117, 'South Sudan', '2015-06-06 00:45:05', 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(55, 'Earthquake Forces Nepal’s Overseas Workers to Face Tough Choices', '#ffffff', 'about how to respond to the April 25 earthquake that could have profound and immediate impacts on household and national economies', '#ffffff', 'How you can help', '#222222', 'LeftBottom', '/uploads/media/120/publicities/OIM NepalResponse.jpg', 'http://https://www.kintera.org/site/c.dnJOKRNkFiG/b.837337/k.F96D/IOM__Make_a_Donation/apps/ka/sd/donor.asp?c=dnJOKRNkFiG&b=837337&en=6pJBKLMnG5JKIVMqF4KHIRMALpJOJPOyGhKPK3MyHaLMKTPAKtE', '', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 1, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-21 08:03:19', 68, 120, 120, 'Earthquake Nepal', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(56, '', '#222222', '', '#222222', '', '#222222', 'LeftMiddle', '/images/noimage.png', 'http://', 'Raise your voice ', '/uploads/media/126/publicities/Unilever.jpg', 'http://https://brightfuture.unilever.com/petitions/423601/take-climate-action-now.aspx', '', '/images/noimage.png', 'http://', 2, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-20 17:38:42', 74, 126, 126, 'Raise your voice', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(57, '', '#222222', '', '#222222', '', '#222222', 'LeftMiddle', '/images/noimage.png', 'http://', '.', '/uploads/media/126/publicities/Unilever - energy.jpg', 'http://https://brightfuture.unilever.com/stories/423955/THE-CHUNKINATOR--Turning-ice-cream-into-energy.aspx', '', '/images/noimage.png', 'http://', 2, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-20 17:46:49', 74, 126, 126, 'Meet the chunkinator', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(58, '', '#222222', '', '#222222', '', '#222222', 'LeftMiddle', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 'Publication', '/uploads/media/128/publicities/World Bank Publication.jpg', 'http://https://openknowledge.worldbank.org/handle/10986/16608', 3, 1, 0, 0, '2015-06-24 15:53:48', '2015-05-20 17:54:42', 76, 128, 128, 'Publication', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(59, 'Adopt a Polar Bear', '#222222', 'Bears face a grave challenge to their survival', '#222222', 'Learn More', '#222222', 'RightBottom', '/uploads/media/120/publicities/WWF.png', 'http://aidweb.info/#', '', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 1, 0, 0, 0, '2015-06-24 15:53:48', '2015-05-21 16:51:43', 68, 120, 120, 'Adopt a Polar Bear', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000),
(60, 'Adopt a Polar Bear', '#222222', 'Bears face a grave challenge to their survival', '#222222', 'Learn More', '#222222', 'RightBottom', '/uploads/media/120/publicities/WWF.png', 'http://aidweb.info/#', '', '/images/noimage.png', 'http://', '', '/images/noimage.png', 'http://', 1, 0, 0, 0, '2015-06-24 15:53:48', '2015-05-21 16:52:09', 68, 120, 120, 'Adopt a Polar Bear', NULL, 1, '2015-06-24 00:00:00', 'P0Y7M0DT0H0M0S', 18144000);

-- --------------------------------------------------------

--
-- Table structure for table `publicities_platforms`
--

CREATE TABLE IF NOT EXISTS `publicities_platforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publicity_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=113 ;

--
-- Dumping data for table `publicities_platforms`
--

INSERT INTO `publicities_platforms` (`id`, `publicity_id`, `platform_id`, `created_at`, `updated_at`) VALUES
(103, 51, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(104, 52, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(105, 53, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(106, 54, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(107, 55, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(108, 56, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(109, 57, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(110, 58, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(111, 59, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00'),
(112, 60, 0, '2015-06-24 08:47:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Africa', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(2, 'Asia-Oceania', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(3, 'Europe', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(4, 'Latin America', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(5, 'Middle East', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(6, 'North America', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28'),
(7, 'World', 0, '2015-05-04 16:59:28', '2015-05-04 16:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

CREATE TABLE IF NOT EXISTS `registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `registrations`
--

INSERT INTO `registrations` (`id`, `user_id`, `token`) VALUES
(1, 102, '$2y$10$r4Df/ZFe.jZ3jPpOVzd4uuUrD0dXKy8qBwWc4mgd5z.q76n875rTO'),
(2, 103, '$2y$10$bpTDffX9V7XsC2uEvHVsX.7YbU2bPjcYhiM0TzsixsKT0L.sDNAK6'),
(3, 104, '$2y$10$tEM0Sr44X2GFG/xVeTZ07Oh13ezF5BvAMcdu7ED6.eYlO5RhrWhOi'),
(4, 105, '$2y$10$E/OqyQqHrD9BljOX/A0OMu9pF2vl/TVDKM5JEH0Ad2AeKhD1NvQBe'),
(5, 106, '$2y$10$xmrNma19lvss5Z8VWw0oPe9E3ICF4F/qq0O4Lr2dzVwDl23T0ZV4i'),
(6, 107, '$2y$10$yghJSD9hntZ9at2iVpzSG.OJPeLqah.zCjgkP6ZoiQfxVnL3J3Kka'),
(7, 108, '$2y$10$hqU/kIowRR1RdHRMtfk0m.ubIJ5yVHOlI3KsYJphDO9wU/bRUoJSK'),
(8, 109, '$2y$10$l4M/VQSUzzFn995u/78QCOYIAKmC76nrmKtDLTDmzrlGW9My8PeBO'),
(9, 110, '$2y$10$k.GKdyx/hI1rzPBCVGxEDu3e1LMCWIp2ukJWHkYshiv60zn/T3iS2'),
(10, 111, '$2y$10$.VpUU1MWUFGR2Um4ozNuauRWK3/Mo8mpEnVI/fEnGOEyfaJJhTLF.'),
(11, 112, '$2y$10$8AHI9Of8bF/Mhex2ytTaaujIcvmRWTu.JamIIT3z4OV/hIiao0vzy'),
(12, 113, '$2y$10$p0SYp8QYMrAUcxw38IgaQuM4liDrFI8ujA/VEn0tY5T8MuotvZu8y'),
(13, 114, '$2y$10$BWJhSvBQM0IkW7HvLicsVO7jAgwTIED0fYG28NimeEIQnL6E.HI3W'),
(14, 115, '$2y$10$YNwUJX6ZeKeSkussvzNnVuAnMRvrdPI6mbmYUrbLL/d2aZY0wVB1i'),
(15, 116, '$2y$10$oQ63I91E7zly0TLJfukS0umhXpp0fKhdBXTFMsLa83SyxImjhZfy2'),
(16, 117, '$2y$10$w7RcMkNuQ887Xb3XcHAVXOOOgF913sLB6MsDze9TC7CwM/j4QQuCi'),
(17, 118, '$2y$10$FZGtxHLvj42PqW1eKv/b6.cwyQl5qt8fBleiwzkkqBfniKEQZYMUW'),
(18, 119, '$2y$10$E/ZrUwjkvOG7qMRWnjVh0Om.Osp9ZEBjvqSInGwlbAouuaxZyF8Aq'),
(19, 120, '$2y$10$Xqtr.J7W.Qr.y.JVS/8k9u6HivPUzMr7LjhBNsjtxHOw2IVH.ELsq'),
(20, 121, '$2y$10$Ip.KW3DKI7ResodU8.wYGOB07Xy6MDGmowPdji5RmULoKsOUq7zcG'),
(21, 122, '$2y$10$jwrnZCi9jzwOrkJDmEFjs.W3ybIAPw1ih/DqiBSsyEQtA9PLIsjFe'),
(22, 123, '$2y$10$jSaNNIKiCj271dqlr3qpGeRKAiIeF7rKzlT9h1LhQOM5M2nARA6Dm'),
(23, 124, '$2y$10$1R5b38c5eSBf8gVWkRvJBeePc36/aOnJFIchKlT0nBLOoXSKZDvEm'),
(24, 125, '$2y$10$bcwnH.w1SFZK/QhADMLeQuMiz8Zo.OeZAqSIZk6RFSBNCB1otjMbS'),
(25, 126, '$2y$10$yA1BgOkhWaK6Y7wuYXuWye56lWgCXku84EP7A6TXV0c.b20DCAj0a'),
(26, 127, '$2y$10$hTo7gyt.9YCVWiVzHwAM9eVJytVeKvHY/JBQNVzlm1EDLpqCvDlmG'),
(27, 128, '$2y$10$Q.HbaFaQuWpX2A0CTvxPDuwgLJfwco0R9egIa5X2IKPZryzZxHdaa'),
(28, 129, '$2y$10$jyPtEqNa/.WSOnOHTZCf3.qCZJd0QEXFdXiYnEC3DG.dr/HXv64CG'),
(29, 130, '$2y$10$IRRFLKbvqCCB9G9vn5YCEOzISBFOo9.iZMnsBJnVCEsVyI/7scf1y'),
(30, 131, '$2y$10$bPByGm1Wo/iWoEl.Z6Ta6.Sg.tw.XrAHmq05fS4aYOPaUfS1rYC6a'),
(31, 132, '$2y$10$UMUnZL89D1E20SxLquAxUuggYfhX0HlDCTBwB.fyztuL290YEuAPm'),
(32, 133, '$2y$10$XCVgtLDIL4Ht83Cyh4SgnunocAHi10k5MZ7pyhzMK7aZrYT6vUrKO'),
(33, 134, '$2y$10$xY6k34hsLc9wa6VBV32vYuh1MxU41RV6ExZKG5JDZDlPBnbVjOQEG'),
(34, 135, '$2y$10$Cdm9iEnTwdeDxKW4FhdO.OeDWit.tTJsd/PY7lcEk6xMv/NYPP3/u'),
(35, 136, '$2y$10$p8VvMTQ63TwmUAnTHfzRkuUyfBxpRSK2Bk9vC95Vx/T3S5uGdzu0u'),
(36, 137, '$2y$10$jCILc/JSu/m313fvuGf.F.KS0SMKUgy9DG3Mu/p9GRaOm9rDAV3Je'),
(37, 138, '$2y$10$ZBlGl0UrxOAODAMJNWgcN.bRzkXGyEG97HICIhAxT0LWrTCyvuzgi'),
(38, 139, '$2y$10$.sFKWXWONl1gWZmU55roZu.rlRBMxq3zCAD8I2ORiUqY/FweYWY/i');

-- --------------------------------------------------------

--
-- Table structure for table `sectors`
--

CREATE TABLE IF NOT EXISTS `sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `sectors`
--

INSERT INTO `sectors` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Agriculture & Fisheries', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(2, 'Animal Health & Rights', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(3, 'Cultural', 1, '2015-05-04 16:59:35', '2015-05-19 16:31:00'),
(4, 'Conflict Resolution', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(5, 'Coordination', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(6, 'Corporation (CSR)', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(7, 'Crisis & Emergencies', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(8, 'Cultural', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(9, 'Disaster Risk Reduction', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(10, 'Economy & Trade', 0, '2015-05-04 16:59:35', '2015-05-19 16:32:00'),
(11, 'Endangered Species', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(12, 'Education', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(13, 'Energy', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(14, 'Environment', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(15, 'Food & Nutrition', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(16, 'Gender & WID', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(17, 'Governance', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(18, 'Habitat, Shelter & NFI', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(19, 'Health', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(20, 'International Trade', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(21, 'IT & Communications', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(22, 'Law & Legal Affairs', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(23, 'Livelihoods', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(24, 'Telecommunications', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(25, 'MDG', 1, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(26, 'Mental Health', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(27, 'Microfinance', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(28, 'Natural Resources', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(29, 'Population & Settlement', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(30, 'Protection & Human Rights', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(31, 'Safety & Security', 0, '2015-05-04 16:59:35', '2015-05-04 16:59:35'),
(32, 'Science & Technology', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(33, 'Water Sanitation Hygiene', 0, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(34, 'Financing', 0, '2015-05-19 16:33:00', '2015-05-19 16:33:00'),
(35, 'Labour', 0, '2015-05-19 16:34:00', '2015-05-19 16:34:00'),
(36, 'Peace & Security', 0, '2015-05-19 16:43:00', '2015-05-19 16:43:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `default_value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `default_value`, `created_at`, `updated_at`) VALUES
(1, 'urls_blog_blogpost_prefix', 'blogs', 'blogs', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(2, 'urls_news_newsitem_prefix', 'news', 'news', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(3, 'urls_videos_video_prefix', 'videos', 'videos', '2015-05-04 16:59:37', '2015-05-04 16:59:37'),
(4, 'urls_takeactions_takeaction_prefix', 'get-involved', 'take-actions', '2015-05-04 16:59:37', '2015-05-04 16:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `take_actions`
--

CREATE TABLE IF NOT EXISTS `take_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type_id` int(10) unsigned DEFAULT NULL,
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `take_actions_type_id_index` (`type_id`),
  KEY `take_actions_organisation_id_index` (`organisation_id`),
  KEY `take_actions_user_profile_id_index` (`user_profile_id`),
  KEY `take_actions_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=64 ;

--
-- Dumping data for table `take_actions`
--

INSERT INTO `take_actions` (`id`, `name`, `title`, `slug`, `content`, `image`, `url`, `published`, `featured`, `deleted`, `created_at`, `updated_at`, `type_id`, `organisation_id`, `user_profile_id`, `user_id`) VALUES
(51, '16AB1471-5E53-44F7-861E-32A7707C2D89', 'Sponsor a child', 'sponsor-a-child', 'Please support Plan&rsquo;s long-term development work by sponsoring a child.', '/uploads/media/123/takeactions/Plan.jpg', 'http://plan-international.org/what-you-can-do/sponsor-a-child/sponsor', 1, 1, 0, '2015-05-20 08:45:40', '2015-05-20 08:45:40', 7, 71, 123, 123),
(52, 'C71F471E-2FEA-4EDD-93F1-234C1DE0F180', 'Shop women''s clothing', 'shop-womens-clothing', 'We have 20,000 items of second-hand and new women&#39;s clothing &amp; accessories in Oxfam&#39;s Online Shop. From skirts and dresses to jumpers and coats, we&#39;ve got thousands of unique items for you to make that perfect outfit.', '/uploads/media/121/takeactions/Oxfam.png', 'http://www.oxfam.org.uk/shop/womens-clothing/?intcmp=womens-hp-shop-womens-16-apr-2015', 1, 1, 0, '2015-05-20 08:49:54', '2015-05-20 08:49:54', 6, 69, 121, 121),
(53, '043926DB-0432-403A-8A04-221FA995264E', 'Race Against Hunger', 'race-against-hunger', 'Empowering people like you to take a stand against hunger is a vital part of our fight against global malnutrition. Whether you&rsquo;re a student, a teacher, a corporate representative, or a concerned citizen, there&rsquo;s plenty you can do to help put an end to global malnutrition.&nbsp;', '/uploads/media/110/takeactions/ACF1.jpg', 'http://www.actionagainsthunger.org/take-action/race-against-hunger', 1, 1, 0, '2015-05-21 09:01:50', '2015-05-21 09:01:50', 3, 58, 110, 110),
(54, '283D72A1-0DB5-4C57-8037-CB6BDD71AF3A', 'MY BODY MY RIGHTS MANIFESTO', 'my-body-my-rights-manifesto', 'My Body My Rights is Amnesty&rsquo;s global campaign to stop the control and criminalization of sexuality and reproduction.<br />\r\n<br />\r\nJoin us in defending sexual and reproductive rights for all.<br />\r\n<br />\r\nIt&rsquo;s your body. Know your rights.', '/uploads/media/106/takeactions/Amnesty.png', 'http://https://www.amnesty.org/en/campaign-my-body-my-rights/', 1, 1, 0, '2015-05-21 09:15:49', '2015-05-21 09:15:50', 1, 55, 106, 106),
(55, 'C7B44BAD-718F-45E4-BAFB-1A1BC332E141', 'Join and protect human rights', 'join-and-protect-human-rights', 'Our members make change possible. They&rsquo;re the people we call on whenever and wherever human rights are under attack. Their actions, big and small, put pressure on governments, institutions and decision-makers to do the right thing.', '/uploads/media/106/takeactions/amnesty 2.jpg', 'http://https://www.amnesty.org/en/get-involved/join/', 1, 1, 0, '2015-05-21 09:18:27', '2015-05-21 09:18:27', 5, 55, 106, 106),
(56, 'DFA0D1E2-047F-40BE-B408-7265116AEDB7', 'Walk In Her Shoes', 'walk-in-her-shoes', 'Walk 10,000 steps a day for a week and help improve access to water across the globe, allowing giving women and girls to get an education or work, and have a chance of fulfilling their potential.&nbsp;', '/uploads/media/111/takeactions/CARE.jpg', 'http://https://walkinhershoes.careinternational.org.uk/', 1, 1, 0, '2015-05-21 12:31:43', '2015-05-21 12:31:43', 3, 59, 111, 111),
(57, '500DCC1E-CFB3-4F4A-BF1F-5875B941C25D', 'Several ways to give to Caritas', 'several-ways-to-give-to-caritas', 'Giving to Caritas Internationalis allows us to strengthen the global confederation of over 160 national Catholic charities. We serve all poor people, of all faiths, all over the world.', '/uploads/media/112/takeactions/Caritas.jpg', 'http://www.caritas.org/take-action/donate/', 1, 1, 0, '2015-05-21 12:37:58', '2015-05-21 12:37:58', 2, 60, 112, 112),
(58, 'DCC4B654-202C-46AB-A900-38FECE2093FB', 'Make Big Polluters Pay', 'make-big-polluters-pay', 'The strong and resilient people of the Philippines are ready to take ambitious action, including legal, against the biggest polluters, and they need us all to stand with them.', '/uploads/media/116/takeactions/Greenpeace.jpg', 'http://act.greenpeace.org/ea-action/action?ea.client.id=1844&ea.campaign.id=33046&ea.tracking.id=gpi', 1, 1, 0, '2015-05-21 12:48:14', '2015-05-21 12:48:14', 1, 64, 116, 116),
(59, '060C2E1E-164E-4E83-95FD-EAFCC4B1FA6A', 'Sign up for email updates', 'sign-up-for-email-updates', 'We&#39;d love to stay in touch. Whether we&#39;re petitioning politicians or saving lives, sign up for our email updates and we&#39;ll let you know what Oxfam&#39;s up to, and how you can get involved.', '/uploads/media/121/takeactions/Oxfam 2.jpg', 'http://www.oxfam.org.uk/email-newsletter-signup', 1, 1, 0, '2015-05-21 12:52:40', '2015-05-21 12:53:15', 5, 69, 121, 121),
(60, '96BD2E1E-D678-43D3-ADD8-61FD1E8A10A1', 'Make a bequest, leave a lasting legacy', 'make-a-bequest-leave-a-lasting-legacy', 'Link your name with the history of the largest humanitarian movement of our time. By making a bequest to the ICRC, you take your commitment to helping victims of war and other situations of violence worldwide to a new level.', '/uploads/media/117/takeactions/ICRC.jpg', 'http://https://www.icrc.org/en/support-us/audience/bequests-and-legacies', 1, 1, 0, '2015-05-21 13:06:51', '2015-05-21 13:06:51', 7, 65, 117, 117),
(61, '7D5DF082-F017-4574-96A1-5F2BF790365B', 'Stay Informed', 'stay-informed', 'Subscribe to our mailing list', '/uploads/media/118/takeactions/MdM.jpg', 'http://doctorsoftheworld.org/newsletter/', 1, 1, 0, '2015-05-21 13:11:10', '2015-05-21 13:11:10', 5, 66, 118, 118),
(62, 'EBCCC2A5-D51F-430D-8FDA-62B212E5C5C5', 'Donations to MSF help save lives', 'donations-to-msf-help-save-lives', 'Every year, our clinical staff provide direct care for common ailments that, left untreated, can prove deadly.<br />\r\n<br />\r\nWhen an outbreak or disaster overwhelms existing health services, we bring an emergency medical response. We carry out surgery, provide maternal care, and treat neglected diseases like tuberculosis, kala azar and sleeping sickness, all thanks to the generosity of our donors.<br />\r\n', '/uploads/media/119/takeactions/MSF.jpg', 'http://www.msf.org/donate', 1, 1, 0, '2015-05-21 13:31:54', '2015-05-21 13:31:54', 2, 67, 119, 119),
(63, 'FE0BAAE6-431B-4648-8B94-A249E590C38C', 'Help protect their icy home', 'help-protect-their-icy-home', 'Sea ice is melting, and industrial development is coming to the Arctic. What does it mean for the mighty tusked walrus?<br />\r\n<br />\r\nYou can help WWF better understand walruses and protect their icy home.<br />\r\n<br />\r\nGifts for a Living Planet &ndash; Make Them Smile!', '/uploads/media/130/takeactions/WWF.jpg', 'http://shop.panda.org/walrus-mystery.html', 1, 1, 0, '2015-05-21 13:38:01', '2015-05-21 13:38:01', 4, 78, 130, 130);

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_beneficiaries`
--

CREATE TABLE IF NOT EXISTS `take_actions_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_beneficiaries_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_beneficiaries_beneficiary_id_index` (`beneficiary_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `take_actions_beneficiaries`
--

INSERT INTO `take_actions_beneficiaries` (`id`, `takeaction_id`, `beneficiary_id`, `created_at`, `updated_at`) VALUES
(51, 51, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 52, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 53, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 54, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 55, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 56, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 57, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 58, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 59, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 60, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 61, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 62, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 63, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_countries`
--

CREATE TABLE IF NOT EXISTS `take_actions_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_countries_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_countries_country_id_index` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `take_actions_countries`
--

INSERT INTO `take_actions_countries` (`id`, `takeaction_id`, `country_id`, `created_at`, `updated_at`) VALUES
(51, 52, 224, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 53, 225, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 56, 224, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 58, 170, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_crisis`
--

CREATE TABLE IF NOT EXISTS `take_actions_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_crisis_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_crisis_crisis_id_index` (`crisis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_interventions`
--

CREATE TABLE IF NOT EXISTS `take_actions_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_interventions_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_interventions_intervention_id_index` (`intervention_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_regions`
--

CREATE TABLE IF NOT EXISTS `take_actions_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_regions_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_regions_region_id_index` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `take_actions_regions`
--

INSERT INTO `take_actions_regions` (`id`, `takeaction_id`, `region_id`, `created_at`, `updated_at`) VALUES
(51, 51, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 52, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 53, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 54, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 55, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 56, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 57, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 58, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 59, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 60, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 61, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 62, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 63, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_sectors`
--

CREATE TABLE IF NOT EXISTS `take_actions_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_sectors_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_sectors_sector_id_index` (`sector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=60 ;

--
-- Dumping data for table `take_actions_sectors`
--

INSERT INTO `take_actions_sectors` (`id`, `takeaction_id`, `sector_id`, `created_at`, `updated_at`) VALUES
(51, 51, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 53, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 54, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 55, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 56, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 58, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 60, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 62, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 63, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `take_actions_themes`
--

CREATE TABLE IF NOT EXISTS `take_actions_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `takeaction_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `take_actions_themes_takeaction_id_index` (`takeaction_id`),
  KEY `take_actions_themes_theme_id_index` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `take_actions_themes`
--

INSERT INTO `take_actions_themes` (`id`, `takeaction_id`, `theme_id`, `created_at`, `updated_at`) VALUES
(51, 51, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 52, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 53, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 54, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 55, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 56, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 57, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 58, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 59, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 60, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 61, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 62, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 63, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Advocacy', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(2, 'Award', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(3, 'Career', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(4, 'Challenge', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(5, 'Crisis', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(6, 'Disaster', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(7, 'Finding / Research', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(8, 'Funding', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(9, 'Innovation ', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(10, 'MDG', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(11, 'New initiative', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(12, 'Partnerships', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(13, 'Security', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(14, 'Success story', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(15, 'Support request', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(16, 'Trend', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(17, 'Vulnerable group', 1, '2015-05-04 16:59:36', '2015-05-04 16:59:36'),
(18, 'Crisis & Relief', 0, '2015-05-19 16:46:00', '2015-05-19 16:46:00'),
(19, 'CSR & Industry', 0, '2015-05-19 16:47:00', '2015-05-19 16:47:00'),
(20, 'Ecology', 0, '2015-05-19 16:47:00', '2015-05-19 16:47:00'),
(21, 'Equality & Rights', 0, '2015-05-19 16:47:00', '2015-05-19 16:47:00'),
(22, 'Poverty & Solidarity', 0, '2015-05-19 16:48:00', '2015-05-19 16:48:00'),
(23, 'Public Services', 0, '2015-05-19 16:48:00', '2015-05-19 16:48:00'),
(24, 'Ways of working', 0, '2015-05-19 16:48:00', '2015-05-19 16:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `types_of_publicities`
--

CREATE TABLE IF NOT EXISTS `types_of_publicities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `types_of_publicities`
--

INSERT INTO `types_of_publicities` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Full Screen', 0, '2015-05-04 17:02:12', '2015-05-04 17:02:12'),
(2, 'Top Banner', 0, '2015-05-04 17:02:12', '2015-05-04 17:02:12'),
(3, 'Sidebar', 0, '2015-05-04 17:02:12', '2015-05-04 17:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `types_of_take_actions`
--

CREATE TABLE IF NOT EXISTS `types_of_take_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `types_of_take_actions`
--

INSERT INTO `types_of_take_actions` (`id`, `name`, `color`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Campaign', '#1cba93', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12'),
(2, 'Donate', '#FF5953', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12'),
(3, 'Fundraise', '#2ab3e1', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12'),
(4, 'Give a Gift', '#ff9000', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12'),
(5, 'Join', '#839061', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12'),
(6, 'Shop', '#8978f4', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12'),
(7, 'Other', '#78808c', 0, '2015-05-04 17:01:12', '2015-05-04 17:01:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `profile_type` int(11) DEFAULT '1',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_new` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=140 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `profile_type`, `verified`, `is_new`) VALUES
(101, 'fleava_admin', 'fleava.admin@mailinator.com', '$2y$10$OD4TNzLAnRgA/upNSCqMW.e0uM7x/rCXVRMo5qcLRROwRuHBRsZ8G', 'RFA3qd1cF1RtezpEO9OtvWqWF9ZMMwaAa6JrAmeoA6Bk0zQH1S1MIjag3Pyc', '2015-06-14 20:56:57', '2015-06-15 03:56:57', 99, 1, 0),
(102, 'test', 'info@fleava.com', '$2y$10$wHsoPaidrluyiXNqZCrTpeyMDqQuzWvZNyzWMrHwNU11wclmHVtbu', NULL, '2015-05-06 15:07:00', '2015-05-06 15:07:00', 2, 0, 1),
(103, 'testing', 'hello@fleava.com', '$2y$10$h7uCQrHoN4lcAf4p2sNxSev7BsuO0T/a/bJ3QnydmF9svkY9vsGJe', NULL, '2015-05-06 15:22:09', '2015-05-06 15:22:09', 2, 0, 1),
(104, 'ACF', 'rogermarkowski@hotmail.com', '$2y$10$26hNSOTlYVxaV1p78TvvWu4sy/ZEXIyiUGNwODEsEa.EJIb7IdGYq', NULL, '2015-05-14 08:10:26', '2015-05-14 08:10:26', 2, 0, 1),
(105, 'AsianDevelopmentBank', 'rogermarkowski@hotmail.com', '$2y$10$WkJR3o3qTzD2rwl5xUeRZufJcO.uptJ0nT00kokh11pZ88heixG7C', 'Nt7voW7a1Ovko1ZECLSEzYfnxRlX6sxB6xoiH1eZwuOp7FDGisqwewhLd0hk', '2015-05-21 02:05:31', '2015-05-21 09:05:31', 2, 1, 0),
(106, 'Amnesty', 'rogermarkowski@hotmail.com', '$2y$10$yVmL9q0WejAPUMtjxJe0p.x7ibZMsVih/fXOylhEHLjsd/r0QEXGO', 'rynt8PmPlyVoVOzZvCoB2dPuMnTLtWJCTnyBoKYXcUEp3AwfrnXa0fos5e9Y', '2015-06-11 10:25:04', '2015-06-11 17:25:04', 2, 1, 0),
(107, 'AusAid-Demo', 'rogermarkowski@hotmail.com', '$2y$10$liudDwGkOToyU1pVLfAfVOeHeD8kQHprKBLbSpNdrqCpho9te50zu', NULL, '2015-05-19 10:18:25', '2015-05-19 10:18:25', 2, 0, 1),
(108, 'AusAiddemo', 'rogermarkowski@hotmail.com', '$2y$10$U5rpCERVdJ3YFQNtOOVxZ.gIjiZutsV0VMQC/0Wv7GHlSOsT6ST1a', 'k97JngPHmnlWd8pZRJjuhkJETkaxiCNFTELvABFqU86NXja3g4EzJE2Fe8mj', '2015-06-11 10:22:20', '2015-06-11 17:22:20', 2, 1, 0),
(109, 'Takehido-demo', 'rogermarkowski@hotmail.com', '$2y$10$XBfkltMhu4u4PKF41miYRuMEBc..s225ZzLYaKqW9I/fu2D6aMhUu', 'wSKtQ6qMj8oxNF5Bld1s1aM2c8bG5IASmTHNiRr5qlPrkwWWMqtb25n1HE9s', '2015-05-20 02:16:04', '2015-05-20 09:16:04', 1, 1, 0),
(110, 'ACFinternationaldemo', 'rogermarkowski@hotmail.com', '$2y$10$hToxQI5mchnf1RRV1.5aTeUDC4EURacofjqpetlKJs834jEQHQNYK', 'opSAupc7G4soykq9T3jiT2oiYOTqmt0EhOqspIvRmYvXDHP8D0Kno0qMDiH5', '2015-05-21 02:02:31', '2015-05-21 09:02:31', 2, 1, 0),
(111, 'CAREinternationaldemo', 'rogermarkowski@hotmail.com', '$2y$10$O7T6Zo55j9b2p2rnrBKQ7.ni.Ztx0sUtzfJr869exGfjE1JAQHhVO', '95Mqz2NXg4HbGfheFy78KERLg8P3DUsc6CfZgGvNidRDsmjcesW87K7hNyez', '2015-05-21 05:35:20', '2015-05-21 12:35:20', 2, 1, 0),
(112, 'Caritasdemo', 'rogermarkowski@hotmail.com', '$2y$10$kE1YYzOlrI4s/11Q.hjoSebPOfP44DKas6iOtG9QnfESCNyEeL0RW', '3PAJ9DpycMjRoXxcTocdpxlPDqdoAgBOs21csX0UN93ximZ1vNV5gIFbeKHb', '2015-06-06 00:37:23', '2015-06-06 07:37:23', 2, 1, 0),
(113, 'DFIDdemo', 'rogermarkowski@hotmail.com', '$2y$10$XbSZZkBnGszDAOgMGLvwWOTiB42jfxGWM9IQy135lb1W9o3AjOUvO', 'yDfC9MtCDpm4A9LpVTeJf51RtM0ygFxCFEW9pUjmuIhQ0Z2drel341bZ0rGz', '2015-06-11 09:15:31', '2015-06-11 16:15:31', 2, 1, 0),
(114, 'FAOdemo', 'rogermarkowski@hotmail.com', '$2y$10$YXvL9Z/e6QK2Zc18QExK7OUC4WQ.SzpxJcHBr./neHmk/Vqf26.YW', 'gsosxwuRbatl3TsBGWttNNFgaSSO7deLAtZJBYZBBsdvdbq06ruV8kstFwk6', '2015-06-11 09:59:10', '2015-06-11 16:59:10', 2, 1, 0),
(115, 'FATDCdemo', 'rogermarkowski@hotmail.com', '$2y$10$HhaabbfVbPuyIAy0UjCVa.dH1W5n36huLHqWhbDx8R41.i1Fjf7uG', 'TZf1Ooe1pMv1apynosW8qGNMXf99a1nkkGLv4yvitToKkyZrGrvstyUcuKXk', '2015-06-11 09:07:34', '2015-06-11 16:07:34', 2, 1, 0),
(116, 'Greenpeacedemo', 'rogermarkowski@hotmail.com', '$2y$10$tgnj6QP96VFBuJnLytHTx.m4/H/tyH6U5JhLElWuK70ItT7F443GC', 'ZvzWAEeKVkz3sM8CDnxIOZCX6fyK7bPOhoWStI8ROZjdq3aee3G6YlhjWZlx', '2015-05-21 05:48:30', '2015-05-21 12:48:30', 2, 1, 0),
(117, 'ICRCdemo', 'rogermarkowski@hotmail.com', '$2y$10$QCYX5e3MUKplX.kzmf7k2ewgECt5x4VM9Diqk4.9MBCN56S6UXLea', 'GZb1iFmcCZpr8bgXyS9aSUcfsKvRv8SVULLnfUkjlLOhdUFhuxVHDZmo38HK', '2015-06-06 00:42:55', '2015-06-06 07:42:55', 2, 1, 0),
(118, 'MDMdemo', 'rogermarkowski@hotmail.com', '$2y$10$bIbJcvOCmoQgh.PRPveucufB7bX.awi7gKUltVm6o6auCnbyWHWra', 'C3MevGv9K6LNDhhgmYf7X7MaIOJ0L0wfVeWaglYRdEX84ZlOqDk8ydIOWNjp', '2015-05-21 06:12:31', '2015-05-21 13:12:31', 2, 1, 0),
(119, 'MSFdemo', 'rogermarkowski@hotmail.com', '$2y$10$OtZ97gBrsLaN.vjVhycZQODLt0D8/aabFw.mfYqWnzIwUYBLSCMJ2', 'ZZyqZwU3vtiKNz82kvCLshFVAfKX4ZQYySlyZbyifUapFaOGbKtM2R0Vg2R5', '2015-05-21 06:35:08', '2015-05-21 13:35:08', 2, 1, 0),
(120, 'IOMdemo', 'rogermarkowski@hotmail.com', '$2y$10$PaSxW85R5dTg4zHUY0eqIOWP9Jlf9HgMej4MQ/RSmHm1I3c5oxy.O', 'xwdswnrP9rKsWnAFV4TVuD7kB0xkWtKhA5JRUoS45KPPnFAy6wvAZJfRkFcV', '2015-06-14 20:21:34', '2015-06-15 03:21:34', 2, 1, 0),
(121, 'Oxfamdemo', 'rogermarkowski@hotmail.com', '$2y$10$DR0IT4WIz9ODmar.aLCf2eRWcvswKF3EPqVra59ygF6wnme13xHdm', 'fvQDN5NSPJx9Df1MoJlvlAI22MhsuRTyTXNdW6vkGPcKZqvXC961rnzKtwpe', '2015-05-21 05:55:01', '2015-05-21 12:55:01', 2, 1, 0),
(122, 'PADFdemo', 'rogermarkowski@hotmail.com', '$2y$10$u5dohg.AtfNSDIpDeBlR1.kG3iJlXB6wPKd.UTVelDzWk8afLg1k.', NULL, '2015-05-19 12:38:28', '2015-05-19 19:38:28', 2, 1, 0),
(123, 'PLANdemo', 'rogermarkowski@hotmail.com', '$2y$10$Q8.QmQrGwh2yyXGtzd5.we2Lw7ja5ql1ycgj3I8IkKhNskFlXCEjy', 'T4MwxEDrqTrWD6Fw0w7wYT8xmvEexDJWDbJZBt7RevZK6PbJlkKcGnpueSId', '2015-05-20 01:45:52', '2015-05-20 08:45:52', 2, 1, 0),
(124, 'UNDPdemo', 'rogermarkowski@hotmail.com', '$2y$10$p4HXw3HMp/dbzucUFBU5Ke70aBQKMHsds61FgfnAeJ9eSCb8qHlNC', 'T05z3xcBU0o59hnLRAxx8Bhqov283VrbeYnHQeQuOw0TTAy9Y0oZfqDPUjL8', '2015-06-11 10:12:26', '2015-06-11 17:12:26', 2, 1, 0),
(125, 'UNICEFdemo', 'rogermarkowski@hotmail.com', '$2y$10$jYkyqlQFCAoGk01mReUVauwMYgB./nReAJq6SfiPfTCs5eH/ObKfO', 'DaQjPflCYJKAfbw62RN83EQdVX6TVzqqK91K2NUMg9WHM2W2WU6tAjJVUd2X', '2015-05-21 05:03:10', '2015-05-21 12:03:10', 2, 1, 0),
(126, 'UNILEVERdemo', 'rogermarkowski@hotmail.com', '$2y$10$qr35J3MnxBC.Nsow5GiowuUGNYN2GRS8wYiAluesxngDhTLF/ivV6', NULL, '2015-05-19 12:55:10', '2015-05-19 19:55:10', 2, 1, 0),
(127, 'USAIDdemo', 'rogermarkowski@hotmail.com', '$2y$10$4tt0muL0zLCr7v6e3ouScetKChuCEoDR0l3vmJn3iog7x0.HBytMG', 'NNA1Vq9xshcdy1sfc2n2cjCgg5lKeAPxZgV4dP1dsH5pmGJFWggbUc1iRUsV', '2015-06-14 19:53:04', '2015-06-15 02:53:04', 2, 1, 0),
(128, 'WorldBankdemo', 'rogermarkowski@hotmail.com', '$2y$10$wted/rNFH.nJCPUR8357peHCPa7lM0Tjo5doX1JoUIVuLpbU68qz2', 'KiyjoRg1JbZU5SatUb1qiW4rWFDlj45jlN9sWZu9yaJgvgUx72sfIwAsQn2d', '2015-06-11 10:05:57', '2015-06-11 17:05:57', 2, 1, 0),
(129, 'WHOdemo', 'rogermarkowski@hotmail.com', '$2y$10$BbRVW2Yxskiv0jGvwRsoS.mZk7.AwfKmQ5whg9.Bi8l06T06LFxzK', NULL, '2015-05-19 13:07:40', '2015-05-19 20:07:40', 2, 1, 0),
(130, 'WWFdemo', 'rogermarkowski@hotmail.com', '$2y$10$A97F/Y.bnuq0XoN1Cqx19u8hc2N9slT7VSylU9MPjencPben9jOOC', 'CgekvYP1uIgmHMKEPNfUrW2FQDdadFUspG6F3tcpSn0eENEhxtOhCN53yuRI', '2015-05-21 06:38:09', '2015-05-21 13:38:09', 2, 1, 0),
(131, 'Chafetz-demo', 'rogermarkowski@hotmail.com', '$2y$10$pvxkb5p86m4JRwIl2t6wmew089IPt1CmpXmJPO2M4oesUiCd4lK6m', '52xSBq7HN5kYhQDKnwdUzScZrWbPSdgSHN5GNQl9X7KuLgF5hVVM955c17VY', '2015-05-20 01:29:25', '2015-05-20 08:29:25', 1, 1, 0),
(132, 'Nesbitt-demo', 'rogermarkowski@hotmail.com', '$2y$10$detR1De/jOZBNn3dsTzQ2ONRdMaD8ZHZMn5tivP0eHf6f.4fj8CP6', 'ewfPivJ1vFrRVAPuhdfyLUOmuAWB71b3g7HQxiaxozmQijteVi8WRHgwNj22', '2015-05-20 01:22:02', '2015-05-20 08:22:02', 1, 1, 0),
(133, 'WTOdemo', 'rogermarkowski@hotmail.com', '$2y$10$cPTSWZkofhHOuIdJUtyLl.ZXgMRRehUDgubZAvHUEQG/wAQGyg.1W', 'JISrses6Zj8liotuaHIsc7Bsac7fsyEKEeMNhepiZUqvofjdQL1OgvXzdqjw', '2015-05-21 05:22:47', '2015-05-21 12:22:47', 2, 1, 0),
(134, 'GMTdemo', 'rogermarkowski@hotmail.com', '$2y$10$9OGQCHcrgED5FCAYQf0vbeMjFoZpIDvq/vKQJ3Ln0rESHWBv2KUFq', NULL, '2015-05-21 06:58:43', '2015-05-21 13:58:43', 1, 1, 1),
(135, 'NYLUNDdemo', 'rogermarkowski@hotmail.com', '$2y$10$JDD7XYLri7BvT6o4fCLRruqGS9bxBSOnQYidCqT4E3H6PemEHrGBi', 'n6MrkdkU9pbperv9K8N84Z6ziM0Lh7NBqkSYWtU17aurponhqW5kBtV7vlZo', '2015-06-14 19:20:59', '2015-06-15 02:20:59', 1, 1, 0),
(136, 'fleava', 'haritianpinio@gmail.com', '$2y$10$Y8JmSWuL25ipVOdnAuY33e/G0g6l4wDa2/mJKsCTBVvKSJ1riLGZq', '3KKNLhXhEVNTlS2jTl9LuGdSXMwDn65usxUG4X5hOsK7KGqf10gyFU5MNk6Q', '2015-06-20 03:36:22', '2015-06-20 10:36:22', 2, 1, 0),
(137, 'testing150525', 'rogermarkowski@hotmail.com', '$2y$10$6ZxD7UMJPfwY4rhskQ6Dkubj1pBlG5l6yzq..WuS7wTsIrGahStwO', NULL, '2015-05-26 02:14:02', '2015-05-26 09:14:02', 1, 1, 1),
(138, 'aidwebtesting 5', 'rogermarkowski@hotmail.com', '$2y$10$pMR5ZUBuFP7TbrgJnSATveifeR8wkAN4hUQbjq4RsP5DLwR1QQKc.', NULL, '2015-05-26 02:14:42', '2015-05-26 09:14:42', 2, 1, 1),
(139, 'PopopJojo', 'rogermarkowski@hotmail.com', '$2y$10$Mxte5FvURJaNhggvQc6gPeCCcg0zObBb17NFU40dfHiHYl0kTYwGy', NULL, '2015-06-13 08:51:02', '2015-06-13 15:51:02', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` int(10) unsigned DEFAULT NULL,
  `based_in` int(10) unsigned DEFAULT NULL,
  `job_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/images/noavatar.png',
  `thought` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_status` int(10) unsigned DEFAULT NULL,
  `professional_status` int(10) unsigned DEFAULT NULL,
  `top_member` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8_unicode_ci,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employer_organisation_type` int(10) unsigned DEFAULT NULL,
  `level_of_responsibility` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_profiles_based_in_index` (`based_in`),
  KEY `user_profiles_employer_organisation_type_index` (`employer_organisation_type`),
  KEY `user_profiles_level_of_responsibility_index` (`level_of_responsibility`),
  KEY `user_profiles_member_status_index` (`member_status`),
  KEY `user_profiles_nationality_index` (`nationality`),
  KEY `user_profiles_professional_status_index` (`professional_status`),
  KEY `user_profiles_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=140 ;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `first_name`, `last_name`, `nationality`, `based_in`, `job_title`, `employer_name`, `avatar`, `thought`, `member_status`, `professional_status`, `top_member`, `is_active`, `created_at`, `updated_at`, `description`, `gender`, `facebook_url`, `twitter_url`, `website_url`, `employer_organisation_type`, `level_of_responsibility`) VALUES
(101, 101, 'fleava', 'admin', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-04 16:59:59', '2015-05-04 16:59:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 102, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-06 15:07:00', '2015-05-06 15:07:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 103, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-06 15:22:09', '2015-05-06 15:22:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 104, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-14 08:10:26', '2015-05-14 08:10:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 105, 'John-Adb', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-18 21:39:38', '2015-05-19 04:39:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 106, 'John-Amnesty', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-18 23:05:47', '2015-05-19 06:05:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 107, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 10:18:25', '2015-05-19 10:18:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 108, 'John Ausaid', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 04:11:38', '2015-05-19 11:11:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 109, 'Takehido', 'Nakao', NULL, 170, 'President', 'Asian Development Bank', '/uploads/media/109/takehiko-nakao - ADB.jpg', '', 1, 2, 1, 1, '2015-05-20 01:52:08', '2015-05-20 08:52:08', 'Nakao was a career civil servant with the Finance Ministry of Japan which he joined in 1978. During his career, Nakao served variously as Minister at the Embassy of Japan in Washington D.C. and as an economic advisor at the International Monetary Fund in the mid ''90s.As Japan''s top currency official, Nakao oversaw the Japanese government''s largest ever currency market intervention following the sharp appreciation of the yen in 2011. He was also the Japanese representative at the G7 and G20 summits in 2012.Before his nomination for the presidency of the ADB, Nakao was Vice Minister of Finance for International Affairs which is the senior most civil service appointment within the country''s Finance Ministry.\r\n', 'male', 'http://', 'http://', 'http://', NULL, NULL),
(110, 110, 'John-ACF', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:06:10', '2015-05-19 18:06:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 111, 'John-Care', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:11:51', '2015-05-19 18:11:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 112, 'John-Caritas', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:19:41', '2015-05-19 18:19:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 113, 'John-Dfid', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:22:53', '2015-05-19 18:22:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 114, 'John-FAO', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:30:49', '2015-05-19 18:30:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 115, 'John-Fatdc', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:37:23', '2015-05-19 18:37:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 116, 'John-Greenpeace', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:45:27', '2015-05-19 18:45:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 117, 'John-ICRC', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 11:48:38', '2015-05-19 18:48:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 118, 'John-MdM', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:03:28', '2015-05-19 19:03:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 119, 'John-MSF', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:09:11', '2015-05-19 19:09:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 120, 'John-Iom', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:23:51', '2015-05-19 19:23:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 121, 'John-Oxfam', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:25:47', '2015-05-19 19:25:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 122, 'John-Padf', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:38:28', '2015-05-19 19:38:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 123, 'John-Plan', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:40:24', '2015-05-19 19:40:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 124, 'John-Undp', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:45:38', '2015-05-19 19:45:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 125, 'John-Unicef', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:47:54', '2015-05-19 19:47:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 126, 'John-Unilever', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:55:10', '2015-05-19 19:55:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 127, 'John-Usaid', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 12:57:06', '2015-05-19 19:57:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 128, 'John-WB', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 13:05:31', '2015-05-19 20:05:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 129, 'John-Who', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 13:07:40', '2015-05-19 20:07:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 130, 'John-Panda', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-19 22:53:19', '2015-05-20 05:53:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 131, 'Aaron', 'Chafetz', NULL, 225, 'Program Analyst ', 'USAID', '/uploads/media/131/Chafez-USAID.jpg', '', 1, 2, 1, 1, '2015-05-20 01:51:32', '2015-05-20 08:51:32', 'Aaron Chafetz is a Program Analyst in USAID’s Bureau for Economic Growth, Education and Environment. \r\n', 'male', 'http://', 'http://', 'http://', NULL, NULL),
(132, 132, 'Christine', 'Nesbitt Hills', NULL, 225, 'Sr. Photo Editor', 'UNICEF', '/uploads/media/132/Nesbitt-Unicef.jpg', '', 1, 2, 1, 1, '2015-05-20 01:51:47', '2015-05-20 08:51:47', 'Independent photography/video producer & editor with proven record in journalism and documentary Africa-wide, on both long-term and short-term projects. Demonstrated commitment to marginalised people. Strong integrity and ethics for documentation of vulnerable women and children. Interest and knowledge of communication for development studies informs practice, as does a career firmly rooted in journalism. \r\n', 'female', 'http://', 'http://', 'http://', NULL, NULL),
(133, 133, 'John-Wto', 'Doe', NULL, NULL, 'PR Officer', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-21 05:14:35', '2015-05-21 12:14:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 134, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-21 13:56:04', '2015-05-21 13:56:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 135, 'Bo Victor', 'Nylund', 171, 205, 'Senior Advisor - Corporate Social Responsibility', 'UNICEF', '/uploads/media/135/Nylund-UNICEF.jpg', '', 1, 2, 1, 1, '2015-05-21 16:11:52', '2015-05-21 23:11:52', 'Bo Viktor Nylund is a protection and legal practitioner who throughout his career has focused on advocacy and engagement with state and non-state actors with a view to driving accountability in the implementation of human rights. Bo Viktor has a PhD in International Law, as well as Master’s degrees in law and political science from Columbia University Law School and Abo Akademi University. \r\n', 'male', 'http://', 'http://', 'http://', NULL, NULL),
(136, 136, 'Haritian', 'Pinio', NULL, NULL, '', NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-25 09:06:04', '2015-05-25 16:06:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 137, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-25 18:51:23', '2015-05-25 18:51:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 138, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-05-25 18:59:08', '2015-05-25 18:59:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 139, '', '', NULL, NULL, NULL, NULL, '/images/noavatar.png', NULL, NULL, NULL, 0, 1, '2015-06-13 15:28:46', '2015-06-13 15:28:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/images/noimage.png',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `organisation_id` int(10) unsigned DEFAULT NULL,
  `user_profile_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `videos_organisation_id_index` (`organisation_id`),
  KEY `videos_user_profile_id_index` (`user_profile_id`),
  KEY `videos_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `slug`, `author`, `image`, `url`, `youtube_id`, `published`, `deleted`, `featured`, `created_at`, `updated_at`, `organisation_id`, `user_profile_id`, `user_id`, `description`, `duration`, `view_count`) VALUES
(31, 'Solidarity with Syria ', 'solidarity-with-syria', 'Caritas photographers include Evert-Jan Daniels and Jos de Voogd, Patrick Nicholson, Laura Sheahen, plus Caritas Turkey, Caritas Jordan and Caritas Lebanon staff. ', 'http://img.youtube.com/vi/menGqrnbc2s/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=menGqrnbc2s', 'menGqrnbc2s', 1, 0, 1, '2015-05-20 08:40:14', '2015-05-20 08:40:14', 60, 112, 112, 'The Caritas confederation responds to the Syria crisis through the work of Caritas Syria, and to members in the region who are helping in Syria, Lebanon, Turkey and Jordan with food, shelter, medical supplies, healthcare, education and finding employment in combined programmes of nearly €15 million. Those programmes have been focused over the last few months on helping Syrians survive the winter, but will be extended as we move into spring and summer. Caritas provides aid to Syrians regardless of their political or religious beliefs.\r\n', 'PT3M9S', 0),
(32, 'World Food Day 2012: Action Against Hunger in DRC ', 'world-food-day-2012-action-against-hunger-in-drc', '', 'http://img.youtube.com/vi/79DlGX01b8M/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=79DlGX01b8M', '79DlGX01b8M', 1, 0, 1, '2015-05-20 08:57:22', '2015-05-20 08:57:22', 58, 110, 110, 'On world food Day 2012, see how aid from the UK is tackling malnutrition in the heart of Africa, with the NGO Action Against Hunger.', 'PT5M18S', 0),
(33, 'Action Against Hunger - The Sharing Experiment ', 'action-against-hunger-the-sharing-experiment', '', 'http://img.youtube.com/vi/zFTspq_nzG4/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=zFTspq_nzG4', 'zFTspq_nzG4', 1, 0, 1, '2015-05-21 08:56:56', '2015-05-21 08:56:56', 58, 110, 110, 'Just by watching this video you are donating to this non-profit, thanks to our advertisers. So why not support the fight and send it to a few ', 'PT1M49S', 0),
(34, 'Asian Development Bank Corporate Video 2014 ', 'asian-development-bank-corporate-video-2014', '', 'http://img.youtube.com/vi/U50S76LNwxQ/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=U50S76LNwxQ', 'U50S76LNwxQ', 1, 0, 1, '2015-05-21 09:05:10', '2015-05-21 09:05:10', 54, 105, 105, 'Fighting Poverty in Asia and the Pacific. This video serves as an introduction to ADB''s work and its impact. It highlights continuing challenges faced in the region and Strategy 2020: ADB''s effort to address these challenges.', 'PT3M8S', 0),
(35, 'Amnesty International"Pens"- Commercial 2014 ', 'amnesty-internationalpens-commercial-2014', 'Directed by Onur Senturk and co-produced by Troublemakers.tv and the studio One More', 'http://img.youtube.com/vi/NbqDjTx3QoA/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=NbqDjTx3QoA', 'NbqDjTx3QoA', 1, 0, 1, '2015-05-21 09:10:43', '2015-05-21 09:10:43', 55, 106, 106, 'Pens is a full CG film shot in motion capture that epitomises the potential of each signature in the defence of human rights. Highlighting that the voice of a single person can bring about the birth of a social movement, the film concludes with the words "your signature is more powerful than you think" and features the song Iron Sky by Paolo Nutini. Through powerful imagery, Amnesty International has encapsulated the essence of its primary objective -- inspiring communities to defend their fundamental human rights.', 'PT2M', 0),
(36, 'AusAID combats PNG tribal violence ', 'ausaid-combats-png-tribal-violence', 'ABC News (Australia)', 'http://img.youtube.com/vi/qqa6aWqSqgA/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=qqa6aWqSqgA', 'qqa6aWqSqgA', 1, 0, 1, '2015-05-21 11:05:16', '2015-05-21 11:08:42', 57, 108, 108, 'Former Queensland detective Don Hurrell is establishing peace monitors in Papua New Guinea''s highlands as part of an AusAID-funded project to reduce tribal fighting.', 'PT37M30S', 0),
(37, 'Kore Lavi''s food voucher and school feeding program ', 'kore-lavis-food-voucher-and-school-feeding-program', '', 'http://img.youtube.com/vi/TdtJaY9sEW4/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=TdtJaY9sEW4', 'TdtJaY9sEW4', 1, 0, 1, '2015-05-21 11:13:54', '2015-05-21 11:13:54', 59, 111, 111, 'CARE Haiti provides a short video describing the work of the Kore Lavi program.', 'PT4M36S', 0),
(39, 'Providing not only crops in Africa, but knowledge ', 'providing-not-only-crops-in-africa-but-knowledge', 'ROME REPORTS, www.romereports.com, is an independent international TV News Agency based in Rome covering the activity of the Pope, the life of the Vatican and current social, cultural and religious debates. Reporting on the Catholic Church requires proxim', 'http://img.youtube.com/vi/167W--oeU6o/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=167W--oeU6o', '167W--oeU6o', 1, 0, 1, '2015-05-21 11:24:19', '2015-05-21 11:24:19', 60, 112, 112, '', 'PT2M19S', 0),
(40, 'DFID''s Bob Gibbons oversees UK aid being loaded on to a RAF C130 bound for Iraq ', 'dfids-bob-gibbons-oversees-uk-aid-being-loaded-on-to-a-raf-c130-bound-for-iraq', 'https://www.youtube.com/watch?v=SWQI-JmA4V0', 'http://img.youtube.com/vi/SWQI-JmA4V0/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=SWQI-JmA4V0', 'SWQI-JmA4V0', 1, 0, 1, '2015-05-21 11:50:48', '2015-05-21 11:50:48', 62, 114, 114, 'DFID Humanitarian Programme Manager Bob Gibbons speaks from Brize Norton, one of the Royal Air Force''s bases in Oxfordshire, where he is helping to load lifesaving aid onto a RAF Hercules C130 aircraft, which is bound for Iraq.', 'PT1M29S', 0),
(41, 'Food wastage footprint ', 'food-wastage-footprint', 'Produced by Nadia El-Hage Scialabba, Art Director Francesca Lucci', 'http://img.youtube.com/vi/IoCVrkcaH6Q/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=IoCVrkcaH6Q', 'IoCVrkcaH6Q', 1, 0, 1, '2015-05-21 11:54:54', '2015-05-21 11:54:54', 62, 114, 114, 'Based on the findings of the Food Wastage Footprint project of the Natural Resources Management and Environment Department kindly funded by Germany.', 'PT3M16S', 0),
(42, 'UNDP''s Year In Review 2014 ', 'undps-year-in-review-2014', '', 'http://img.youtube.com/vi/2ByKL5mLBn8/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=2ByKL5mLBn8', '2ByKL5mLBn8', 1, 0, 1, '2015-05-21 11:59:04', '2015-05-21 11:59:04', 72, 124, 124, '2014 was a year of huge humanitarian crises. From the conflict in Syria to the Ebola outbreak in West Africa, from extreme weather events to democratic elections, the United Nations Development Programme was at the forefront of many of the challenges faced by the most vulnerable and exposed people in some of the poorest countries in the world.', 'PT6M4S', 0),
(43, 'Katy Perry - Unconditionally | UNICEF Goodwill Ambassador | UNICEF ', 'katy-perry-unconditionally-unicef-goodwill-ambassador-unicef', '', 'http://img.youtube.com/vi/Azma-bPeD7o/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=Azma-bPeD7o', 'Azma-bPeD7o', 1, 0, 1, '2015-05-21 12:03:02', '2015-05-21 12:03:02', 73, 125, 125, 'Welcome Katy Perry to the UNICEF Family.', 'PT3M49S', 0),
(44, 'USAID''s WINNER Project in Hait', 'usaids-winner-project-in-hait', 'Janice Laurente', 'http://img.youtube.com/vi/SWQI-JmA4V0/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=SWQI-JmA4V0', 'SWQI-JmA4V0', 1, 0, 1, '2015-05-21 12:05:38', '2015-05-21 12:05:38', 75, 127, 127, 'USAID''s WINNER Project helps improve the lives of Haitians. The project supports the country''s agriculture sector which employs approximately 60 percent of Haitians. It also helps protects natural resources and reduces risks from natural disasters like flood control. ', 'PT1M29S', 0),
(45, 'Cambodia''s International Trade ', 'cambodias-international-trade', '', 'http://img.youtube.com/vi/DG7n9d5xdB8/hqdefault.jpg', 'http://https://www.youtube.com/watch?v=DG7n9d5xdB8&index=3&list=PLgNWEgELFnC41ttL9totkDpcRYQxml6lT', 'DG7n9d5xdB8', 1, 0, 1, '2015-05-21 12:22:38', '2015-05-21 12:22:38', 79, 133, 133, 'Dr. Sok Siphana with Dr. Friedrich Von Kirchbach on Cambodia''s International Trade ', 'PT30M36S', 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos_beneficiaries`
--

CREATE TABLE IF NOT EXISTS `videos_beneficiaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `beneficiary_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_beneficiaries_video_id_index` (`video_id`),
  KEY `videos_beneficiaries_beneficiary_id_index` (`beneficiary_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `videos_beneficiaries`
--

INSERT INTO `videos_beneficiaries` (`id`, `video_id`, `beneficiary_id`, `created_at`, `updated_at`) VALUES
(31, 31, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 32, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 33, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 34, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 35, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 36, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 37, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 39, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 40, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 41, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 42, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 43, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 44, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 45, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos_countries`
--

CREATE TABLE IF NOT EXISTS `videos_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_countries_video_id_index` (`video_id`),
  KEY `videos_countries_country_id_index` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `videos_countries`
--

INSERT INTO `videos_countries` (`id`, `video_id`, `country_id`, `created_at`, `updated_at`) VALUES
(31, 31, 207, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 32, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 33, 224, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 36, 167, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 37, 94, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 40, 104, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 44, 94, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 45, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos_crisis`
--

CREATE TABLE IF NOT EXISTS `videos_crisis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `crisis_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_crisis_video_id_index` (`video_id`),
  KEY `videos_crisis_crisis_id_index` (`crisis_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Dumping data for table `videos_crisis`
--

INSERT INTO `videos_crisis` (`id`, `video_id`, `crisis_id`, `created_at`, `updated_at`) VALUES
(31, 31, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 32, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 34, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 36, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 37, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 39, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 40, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 41, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 44, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos_interventions`
--

CREATE TABLE IF NOT EXISTS `videos_interventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `intervention_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_interventions_video_id_index` (`video_id`),
  KEY `videos_interventions_intervention_id_index` (`intervention_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `videos_regions`
--

CREATE TABLE IF NOT EXISTS `videos_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_regions_video_id_index` (`video_id`),
  KEY `videos_regions_region_id_index` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `videos_regions`
--

INSERT INTO `videos_regions` (`id`, `video_id`, `region_id`, `created_at`, `updated_at`) VALUES
(31, 31, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 32, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 33, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 34, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 35, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 36, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 37, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 39, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 40, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 41, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 42, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 43, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 44, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 45, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos_sectors`
--

CREATE TABLE IF NOT EXISTS `videos_sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `sector_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_sectors_video_id_index` (`video_id`),
  KEY `videos_sectors_sector_id_index` (`sector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `videos_sectors`
--

INSERT INTO `videos_sectors` (`id`, `video_id`, `sector_id`, `created_at`, `updated_at`) VALUES
(31, 31, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 32, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 33, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 35, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 36, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 37, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 39, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 41, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 44, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 45, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `videos_themes`
--

CREATE TABLE IF NOT EXISTS `videos_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `videos_themes_video_id_index` (`video_id`),
  KEY `videos_themes_theme_id_index` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `videos_themes`
--

INSERT INTO `videos_themes` (`id`, `video_id`, `theme_id`, `created_at`, `updated_at`) VALUES
(31, 31, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 32, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 33, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 34, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 35, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 36, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 37, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 39, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 40, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 41, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 42, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 43, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 44, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 45, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD CONSTRAINT `blog_posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `blog_posts_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  ADD CONSTRAINT `blog_posts_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`);

--
-- Constraints for table `blog_posts_beneficiaries`
--
ALTER TABLE `blog_posts_beneficiaries`
  ADD CONSTRAINT `blog_posts_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  ADD CONSTRAINT `blog_posts_beneficiaries_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`);

--
-- Constraints for table `blog_posts_countries`
--
ALTER TABLE `blog_posts_countries`
  ADD CONSTRAINT `blog_posts_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `blog_posts_countries_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`);

--
-- Constraints for table `blog_posts_crisis`
--
ALTER TABLE `blog_posts_crisis`
  ADD CONSTRAINT `blog_posts_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  ADD CONSTRAINT `blog_posts_crisis_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`);

--
-- Constraints for table `blog_posts_interventions`
--
ALTER TABLE `blog_posts_interventions`
  ADD CONSTRAINT `blog_posts_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  ADD CONSTRAINT `blog_posts_interventions_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`);

--
-- Constraints for table `blog_posts_regions`
--
ALTER TABLE `blog_posts_regions`
  ADD CONSTRAINT `blog_posts_regions_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`),
  ADD CONSTRAINT `blog_posts_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`);

--
-- Constraints for table `blog_posts_sectors`
--
ALTER TABLE `blog_posts_sectors`
  ADD CONSTRAINT `blog_posts_sectors_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`),
  ADD CONSTRAINT `blog_posts_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`);

--
-- Constraints for table `blog_posts_themes`
--
ALTER TABLE `blog_posts_themes`
  ADD CONSTRAINT `blog_posts_themes_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `blog_posts` (`id`),
  ADD CONSTRAINT `blog_posts_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`);

--
-- Constraints for table `news_items`
--
ALTER TABLE `news_items`
  ADD CONSTRAINT `news_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `news_items_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  ADD CONSTRAINT `news_items_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`);

--
-- Constraints for table `news_items_beneficiaries`
--
ALTER TABLE `news_items_beneficiaries`
  ADD CONSTRAINT `news_items_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  ADD CONSTRAINT `news_items_beneficiaries_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`);

--
-- Constraints for table `news_items_countries`
--
ALTER TABLE `news_items_countries`
  ADD CONSTRAINT `news_items_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `news_items_countries_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`);

--
-- Constraints for table `news_items_crisis`
--
ALTER TABLE `news_items_crisis`
  ADD CONSTRAINT `news_items_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  ADD CONSTRAINT `news_items_crisis_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`);

--
-- Constraints for table `news_items_interventions`
--
ALTER TABLE `news_items_interventions`
  ADD CONSTRAINT `news_items_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  ADD CONSTRAINT `news_items_interventions_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`);

--
-- Constraints for table `news_items_regions`
--
ALTER TABLE `news_items_regions`
  ADD CONSTRAINT `news_items_regions_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`),
  ADD CONSTRAINT `news_items_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`);

--
-- Constraints for table `news_items_sectors`
--
ALTER TABLE `news_items_sectors`
  ADD CONSTRAINT `news_items_sectors_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`),
  ADD CONSTRAINT `news_items_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`);

--
-- Constraints for table `news_items_themes`
--
ALTER TABLE `news_items_themes`
  ADD CONSTRAINT `news_items_themes_newsitem_id_foreign` FOREIGN KEY (`newsitem_id`) REFERENCES `news_items` (`id`),
  ADD CONSTRAINT `news_items_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`);

--
-- Constraints for table `organisations`
--
ALTER TABLE `organisations`
  ADD CONSTRAINT `organisations_org_type_foreign` FOREIGN KEY (`org_type`) REFERENCES `organisation_types` (`id`),
  ADD CONSTRAINT `organisations_cp_id_foreign` FOREIGN KEY (`cp_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `organisations_level_of_activity_foreign` FOREIGN KEY (`level_of_activity`) REFERENCES `level_of_activities` (`id`),
  ADD CONSTRAINT `organisations_office_location_foreign` FOREIGN KEY (`office_location`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `organisations_office_type_foreign` FOREIGN KEY (`office_type`) REFERENCES `office_types` (`id`);

--
-- Constraints for table `publicities`
--
ALTER TABLE `publicities`
  ADD CONSTRAINT `publicities_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`),
  ADD CONSTRAINT `publicities_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  ADD CONSTRAINT `publicities_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types_of_publicities` (`id`),
  ADD CONSTRAINT `publicities_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `take_actions`
--
ALTER TABLE `take_actions`
  ADD CONSTRAINT `take_actions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `take_actions_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  ADD CONSTRAINT `take_actions_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types_of_take_actions` (`id`),
  ADD CONSTRAINT `take_actions_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`);

--
-- Constraints for table `take_actions_beneficiaries`
--
ALTER TABLE `take_actions_beneficiaries`
  ADD CONSTRAINT `take_actions_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  ADD CONSTRAINT `take_actions_beneficiaries_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`);

--
-- Constraints for table `take_actions_countries`
--
ALTER TABLE `take_actions_countries`
  ADD CONSTRAINT `take_actions_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `take_actions_countries_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`);

--
-- Constraints for table `take_actions_crisis`
--
ALTER TABLE `take_actions_crisis`
  ADD CONSTRAINT `take_actions_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  ADD CONSTRAINT `take_actions_crisis_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`);

--
-- Constraints for table `take_actions_interventions`
--
ALTER TABLE `take_actions_interventions`
  ADD CONSTRAINT `take_actions_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  ADD CONSTRAINT `take_actions_interventions_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`);

--
-- Constraints for table `take_actions_regions`
--
ALTER TABLE `take_actions_regions`
  ADD CONSTRAINT `take_actions_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`),
  ADD CONSTRAINT `take_actions_regions_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`);

--
-- Constraints for table `take_actions_sectors`
--
ALTER TABLE `take_actions_sectors`
  ADD CONSTRAINT `take_actions_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`),
  ADD CONSTRAINT `take_actions_sectors_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`);

--
-- Constraints for table `take_actions_themes`
--
ALTER TABLE `take_actions_themes`
  ADD CONSTRAINT `take_actions_themes_takeaction_id_foreign` FOREIGN KEY (`takeaction_id`) REFERENCES `take_actions` (`id`),
  ADD CONSTRAINT `take_actions_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`);

--
-- Constraints for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_profiles_based_in_foreign` FOREIGN KEY (`based_in`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `user_profiles_employer_organisation_type_foreign` FOREIGN KEY (`employer_organisation_type`) REFERENCES `organisation_types` (`id`),
  ADD CONSTRAINT `user_profiles_level_of_responsibility_foreign` FOREIGN KEY (`level_of_responsibility`) REFERENCES `level_of_responsibilities` (`id`),
  ADD CONSTRAINT `user_profiles_member_status_foreign` FOREIGN KEY (`member_status`) REFERENCES `member_statuses` (`id`),
  ADD CONSTRAINT `user_profiles_nationality_foreign` FOREIGN KEY (`nationality`) REFERENCES `nationalities` (`id`),
  ADD CONSTRAINT `user_profiles_professional_status_foreign` FOREIGN KEY (`professional_status`) REFERENCES `professional_statuses` (`id`);

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `videos_organisation_id_foreign` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  ADD CONSTRAINT `videos_user_profile_id_foreign` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profiles` (`id`);

--
-- Constraints for table `videos_beneficiaries`
--
ALTER TABLE `videos_beneficiaries`
  ADD CONSTRAINT `videos_beneficiaries_beneficiary_id_foreign` FOREIGN KEY (`beneficiary_id`) REFERENCES `beneficiaries` (`id`),
  ADD CONSTRAINT `videos_beneficiaries_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Constraints for table `videos_countries`
--
ALTER TABLE `videos_countries`
  ADD CONSTRAINT `videos_countries_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `videos_countries_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Constraints for table `videos_crisis`
--
ALTER TABLE `videos_crisis`
  ADD CONSTRAINT `videos_crisis_crisis_id_foreign` FOREIGN KEY (`crisis_id`) REFERENCES `crisis` (`id`),
  ADD CONSTRAINT `videos_crisis_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Constraints for table `videos_interventions`
--
ALTER TABLE `videos_interventions`
  ADD CONSTRAINT `videos_interventions_intervention_id_foreign` FOREIGN KEY (`intervention_id`) REFERENCES `interventions` (`id`),
  ADD CONSTRAINT `videos_interventions_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Constraints for table `videos_regions`
--
ALTER TABLE `videos_regions`
  ADD CONSTRAINT `videos_regions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`),
  ADD CONSTRAINT `videos_regions_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Constraints for table `videos_sectors`
--
ALTER TABLE `videos_sectors`
  ADD CONSTRAINT `videos_sectors_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`),
  ADD CONSTRAINT `videos_sectors_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Constraints for table `videos_themes`
--
ALTER TABLE `videos_themes`
  ADD CONSTRAINT `videos_themes_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`),
  ADD CONSTRAINT `videos_themes_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
