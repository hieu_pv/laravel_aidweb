
      /*
      * Script created by Quest Schema Compare at 26/06/2015 2:20:18 PM.
      * Please back up your database before running this script.
      *
      * Synchronizing objects from aidweb to aidweb.
      */
    USE `aidweb`;

-- Drop foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`blog_posts` DROP FOREIGN KEY `blog_posts_organisation_id_foreign`;

-- Drop foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`news_items` DROP FOREIGN KEY `news_items_organisation_id_foreign`;

-- Drop foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`publicities` DROP FOREIGN KEY `publicities_organisation_id_foreign`;

-- Drop foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`take_actions` DROP FOREIGN KEY `take_actions_organisation_id_foreign`;

-- Drop foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`videos` DROP FOREIGN KEY `videos_organisation_id_foreign`;

-- Drop foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`blog_posts` DROP FOREIGN KEY `blog_posts_user_profile_id_foreign`;

-- Drop foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`news_items` DROP FOREIGN KEY `news_items_user_profile_id_foreign`;

-- Drop foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`publicities` DROP FOREIGN KEY `publicities_user_profile_id_foreign`;

-- Drop foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`take_actions` DROP FOREIGN KEY `take_actions_user_profile_id_foreign`;

-- Drop foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`videos` DROP FOREIGN KEY `videos_user_profile_id_foreign`;

-- Drop foreign keys, referenced to users
ALTER TABLE `aidweb`.`blog_posts` DROP FOREIGN KEY `blog_posts_user_id_foreign`;

-- Drop foreign keys, referenced to users
ALTER TABLE `aidweb`.`news_items` DROP FOREIGN KEY `news_items_user_id_foreign`;

-- Drop foreign keys, referenced to users
ALTER TABLE `aidweb`.`publicities` DROP FOREIGN KEY `publicities_user_id_foreign`;

-- Drop foreign keys, referenced to users
ALTER TABLE `aidweb`.`take_actions` DROP FOREIGN KEY `take_actions_user_id_foreign`;

-- Drop foreign keys, referenced to users
ALTER TABLE `aidweb`.`videos` DROP FOREIGN KEY `videos_user_id_foreign`;

ALTER TABLE `aidweb`.`organisations` DROP FOREIGN KEY `organisations_cp_id_foreign`;

ALTER TABLE `aidweb`.`organisations` DROP FOREIGN KEY `organisations_level_of_activity_foreign`;

ALTER TABLE `aidweb`.`organisations` DROP FOREIGN KEY `organisations_office_location_foreign`;

ALTER TABLE `aidweb`.`organisations` DROP FOREIGN KEY `organisations_office_type_foreign`;

ALTER TABLE `aidweb`.`organisations` DROP FOREIGN KEY `organisations_org_type_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_based_in_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_employer_organisation_type_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_level_of_responsibility_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_member_status_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_nationality_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_professional_status_foreign`;

ALTER TABLE `aidweb`.`user_profiles` DROP FOREIGN KEY `user_profiles_user_id_foreign`;

/* Header line. Object: level_of_activities. Script date: 26/06/2015 2:20:18 PM. */
DROP TABLE IF EXISTS `aidweb`.`_temp_level_of_activities`;

CREATE TABLE `aidweb`.`_temp_level_of_activities` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	`created_at` timestamp NOT NULL,
	`updated_at` timestamp NOT NULL,
	PRIMARY KEY  ( `id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
AUTO_INCREMENT = 4
ROW_FORMAT = Compact
;

INSERT INTO `aidweb`.`_temp_level_of_activities`
( `created_at`, `id`, `name`, `updated_at` )
SELECT
`created_at`, `id`, `name`, `updated_at`
FROM `aidweb`.`level_of_activities`;

DROP TABLE `aidweb`.`level_of_activities`;

ALTER TABLE `aidweb`.`_temp_level_of_activities` RENAME `level_of_activities`;

/* Header line. Object: level_of_responsibilities. Script date: 26/06/2015 2:20:18 PM. */
DROP TABLE IF EXISTS `aidweb`.`_temp_level_of_responsibilities`;

CREATE TABLE `aidweb`.`_temp_level_of_responsibilities` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	`created_at` timestamp NOT NULL,
	`updated_at` timestamp NOT NULL,
	PRIMARY KEY  ( `id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
AUTO_INCREMENT = 4
ROW_FORMAT = Compact
;

INSERT INTO `aidweb`.`_temp_level_of_responsibilities`
( `created_at`, `id`, `name`, `updated_at` )
SELECT
`created_at`, `id`, `name`, `updated_at`
FROM `aidweb`.`level_of_responsibilities`;

DROP TABLE `aidweb`.`level_of_responsibilities`;

ALTER TABLE `aidweb`.`_temp_level_of_responsibilities` RENAME `level_of_responsibilities`;

/* Header line. Object: organisations. Script date: 26/06/2015 2:20:18 PM. */
DROP TABLE IF EXISTS `aidweb`.`_temp_organisations`;

CREATE TABLE `aidweb`.`_temp_organisations` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`cp_id` int(10) unsigned NOT NULL,
	`org_logo` varchar(255) NOT NULL default '/images/noimage.png',
	`org_name` varchar(255) NOT NULL,
	`org_email` varchar(255) NOT NULL,
	`org_website` varchar(255) NOT NULL,
	`office_location` int(10) unsigned default NULL,
	`office_type` int(10) unsigned default NULL,
	`org_type` int(10) unsigned default NULL,
	`top_agency` tinyint(1) NOT NULL default '0',
	`is_active` tinyint(1) NOT NULL default '1',
	`created_at` timestamp NOT NULL,
	`updated_at` timestamp NOT NULL,
	`description` text default NULL,
	`org_name_acronym` varchar(255) default NULL,
	`use_org_name_acronym` tinyint(1) default '0',
	`facebook_url` varchar(255) default NULL,
	`twitter_url` varchar(255) default NULL,
	`org_phone` varchar(255) default NULL,
	`level_of_activity` int(10) unsigned default NULL,
	`cover_image` varchar(255) NOT NULL default '/images/noimage.png',
	KEY `organisations_cp_id_index` ( `cp_id` ),
	KEY `organisations_level_of_activity_index` ( `level_of_activity` ),
	KEY `organisations_office_location_index` ( `office_location` ),
	KEY `organisations_office_type_index` ( `office_type` ),
	KEY `organisations_org_type_index` ( `org_type` ),
	PRIMARY KEY  ( `id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
AUTO_INCREMENT = 82
ROW_FORMAT = Compact
;

INSERT INTO `aidweb`.`_temp_organisations`
( `cp_id`, `created_at`, `description`, `facebook_url`, `id`, `is_active`, `level_of_activity`, `office_location`, `office_type`, `org_email`, `org_logo`, `org_name`, `org_name_acronym`, `org_phone`, `org_type`, `org_website`, `top_agency`, `twitter_url`, `updated_at`, `use_org_name_acronym` )
SELECT
`cp_id`, `created_at`, `description`, `facebook_url`, `id`, `is_active`, `level_of_activity`, `office_location`, `office_type`, `org_email`, `org_logo`, `org_name`, `org_name_acronym`, `org_phone`, `org_type`, `org_website`, `top_agency`, `twitter_url`, `updated_at`, `use_org_name_acronym`
FROM `aidweb`.`organisations`;

DROP TABLE `aidweb`.`organisations`;

ALTER TABLE `aidweb`.`_temp_organisations` RENAME `organisations`;

/* Header line. Object: user_profiles. Script date: 26/06/2015 2:20:18 PM. */
DROP TABLE IF EXISTS `aidweb`.`_temp_user_profiles`;

CREATE TABLE `aidweb`.`_temp_user_profiles` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`user_id` int(10) unsigned NOT NULL,
	`first_name` varchar(255) NOT NULL,
	`last_name` varchar(255) NOT NULL,
	`nationality` int(10) unsigned default NULL,
	`based_in` int(10) unsigned default NULL,
	`job_title` varchar(255) default NULL,
	`employer_name` varchar(255) default NULL,
	`avatar` varchar(255) default '/images/noavatar.png',
	`thought` varchar(255) default NULL,
	`member_status` int(10) unsigned default NULL,
	`professional_status` int(10) unsigned default NULL,
	`top_member` tinyint(1) NOT NULL default '0',
	`is_active` tinyint(1) NOT NULL default '1',
	`created_at` timestamp NOT NULL,
	`updated_at` timestamp NOT NULL,
	`description` text default NULL,
	`gender` varchar(255) default NULL,
	`facebook_url` varchar(255) default NULL,
	`twitter_url` varchar(255) default NULL,
	`website_url` varchar(255) default NULL,
	`employer_organisation_type` int(10) unsigned default NULL,
	`level_of_responsibility` int(10) unsigned default NULL,
	`cover_image` varchar(255) NOT NULL default '/images/noimage.png',
	PRIMARY KEY  ( `id` ),
	KEY `user_profiles_based_in_index` ( `based_in` ),
	KEY `user_profiles_employer_organisation_type_index` ( `employer_organisation_type` ),
	KEY `user_profiles_level_of_responsibility_index` ( `level_of_responsibility` ),
	KEY `user_profiles_member_status_index` ( `member_status` ),
	KEY `user_profiles_nationality_index` ( `nationality` ),
	KEY `user_profiles_professional_status_index` ( `professional_status` ),
	KEY `user_profiles_user_id_index` ( `user_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
AUTO_INCREMENT = 140
ROW_FORMAT = Compact
;

INSERT INTO `aidweb`.`_temp_user_profiles`
( `avatar`, `based_in`, `created_at`, `description`, `employer_name`, `employer_organisation_type`, `facebook_url`, `first_name`, `gender`, `id`, `is_active`, `job_title`, `last_name`, `level_of_responsibility`, `member_status`, `nationality`, `professional_status`, `thought`, `top_member`, `twitter_url`, `updated_at`, `user_id`, `website_url` )
SELECT
`avatar`, `based_in`, `created_at`, `description`, `employer_name`, `employer_organisation_type`, `facebook_url`, `first_name`, `gender`, `id`, `is_active`, `job_title`, `last_name`, `level_of_responsibility`, `member_status`, `nationality`, `professional_status`, `thought`, `top_member`, `twitter_url`, `updated_at`, `user_id`, `website_url`
FROM `aidweb`.`user_profiles`;

DROP TABLE `aidweb`.`user_profiles`;

ALTER TABLE `aidweb`.`_temp_user_profiles` RENAME `user_profiles`;

/* Header line. Object: users. Script date: 26/06/2015 2:20:18 PM. */
DROP TABLE IF EXISTS `aidweb`.`_temp_users`;

CREATE TABLE `aidweb`.`_temp_users` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`username` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	`password` varchar(255) NOT NULL,
	`remember_token` varchar(100) default NULL,
	`created_at` timestamp NOT NULL,
	`updated_at` timestamp NOT NULL,
	`profile_type` int(11) default '1',
	`verified` tinyint(1) NOT NULL default '0',
	`is_new` tinyint(1) NOT NULL default '1',
	PRIMARY KEY  ( `id` ),
	UNIQUE INDEX `users_username_unique` ( `username` )
)
ENGINE = InnoDB
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
AUTO_INCREMENT = 140
ROW_FORMAT = Compact
;

INSERT INTO `aidweb`.`_temp_users`
( `created_at`, `email`, `id`, `is_new`, `password`, `profile_type`, `remember_token`, `updated_at`, `username`, `verified` )
SELECT
`created_at`, `email`, `id`, `is_new`, `password`, `profile_type`, `remember_token`, `updated_at`, `username`, `verified`
FROM `aidweb`.`users`;

DROP TABLE `aidweb`.`users`;

ALTER TABLE `aidweb`.`_temp_users` RENAME `users`;

-- Restore foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`blog_posts` ADD CONSTRAINT `blog_posts_organisation_id_foreign`
	FOREIGN KEY ( `organisation_id` ) REFERENCES `organisations` ( `id` );

-- Restore foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`news_items` ADD CONSTRAINT `news_items_organisation_id_foreign`
	FOREIGN KEY ( `organisation_id` ) REFERENCES `organisations` ( `id` );

-- Restore foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`publicities` ADD CONSTRAINT `publicities_organisation_id_foreign`
	FOREIGN KEY ( `organisation_id` ) REFERENCES `organisations` ( `id` );

-- Restore foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`take_actions` ADD CONSTRAINT `take_actions_organisation_id_foreign`
	FOREIGN KEY ( `organisation_id` ) REFERENCES `organisations` ( `id` );

-- Restore foreign keys, referenced to organisations
ALTER TABLE `aidweb`.`videos` ADD CONSTRAINT `videos_organisation_id_foreign`
	FOREIGN KEY ( `organisation_id` ) REFERENCES `organisations` ( `id` );

-- Restore foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`blog_posts` ADD CONSTRAINT `blog_posts_user_profile_id_foreign`
	FOREIGN KEY ( `user_profile_id` ) REFERENCES `user_profiles` ( `id` );

-- Restore foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`news_items` ADD CONSTRAINT `news_items_user_profile_id_foreign`
	FOREIGN KEY ( `user_profile_id` ) REFERENCES `user_profiles` ( `id` );

-- Restore foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`publicities` ADD CONSTRAINT `publicities_user_profile_id_foreign`
	FOREIGN KEY ( `user_profile_id` ) REFERENCES `user_profiles` ( `id` );

-- Restore foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`take_actions` ADD CONSTRAINT `take_actions_user_profile_id_foreign`
	FOREIGN KEY ( `user_profile_id` ) REFERENCES `user_profiles` ( `id` );

-- Restore foreign keys, referenced to user_profiles
ALTER TABLE `aidweb`.`videos` ADD CONSTRAINT `videos_user_profile_id_foreign`
	FOREIGN KEY ( `user_profile_id` ) REFERENCES `user_profiles` ( `id` );

-- Restore foreign keys, referenced to users
ALTER TABLE `aidweb`.`blog_posts` ADD CONSTRAINT `blog_posts_user_id_foreign`
	FOREIGN KEY ( `user_id` ) REFERENCES `users` ( `id` );

-- Restore foreign keys, referenced to users
ALTER TABLE `aidweb`.`news_items` ADD CONSTRAINT `news_items_user_id_foreign`
	FOREIGN KEY ( `user_id` ) REFERENCES `users` ( `id` );

-- Restore foreign keys, referenced to users
ALTER TABLE `aidweb`.`publicities` ADD CONSTRAINT `publicities_user_id_foreign`
	FOREIGN KEY ( `user_id` ) REFERENCES `users` ( `id` );

-- Restore foreign keys, referenced to users
ALTER TABLE `aidweb`.`take_actions` ADD CONSTRAINT `take_actions_user_id_foreign`
	FOREIGN KEY ( `user_id` ) REFERENCES `users` ( `id` );

-- Restore foreign keys, referenced to users
ALTER TABLE `aidweb`.`videos` ADD CONSTRAINT `videos_user_id_foreign`
	FOREIGN KEY ( `user_id` ) REFERENCES `users` ( `id` );

-- Update foreign keys of organisations
ALTER TABLE `aidweb`.`organisations` ADD CONSTRAINT `organisations_cp_id_foreign`
	FOREIGN KEY ( `cp_id` ) REFERENCES `users` ( `id` );

ALTER TABLE `aidweb`.`organisations` ADD CONSTRAINT `organisations_level_of_activity_foreign`
	FOREIGN KEY ( `level_of_activity` ) REFERENCES `level_of_activities` ( `id` );

ALTER TABLE `aidweb`.`organisations` ADD CONSTRAINT `organisations_office_location_foreign`
	FOREIGN KEY ( `office_location` ) REFERENCES `countries` ( `id` );

ALTER TABLE `aidweb`.`organisations` ADD CONSTRAINT `organisations_office_type_foreign`
	FOREIGN KEY ( `office_type` ) REFERENCES `office_types` ( `id` );

ALTER TABLE `aidweb`.`organisations` ADD CONSTRAINT `organisations_org_type_foreign`
	FOREIGN KEY ( `org_type` ) REFERENCES `organisation_types` ( `id` );

-- Update foreign keys of user_profiles
ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_based_in_foreign`
	FOREIGN KEY ( `based_in` ) REFERENCES `countries` ( `id` );

ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_employer_organisation_type_foreign`
	FOREIGN KEY ( `employer_organisation_type` ) REFERENCES `organisation_types` ( `id` );

ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_level_of_responsibility_foreign`
	FOREIGN KEY ( `level_of_responsibility` ) REFERENCES `level_of_responsibilities` ( `id` );

ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_member_status_foreign`
	FOREIGN KEY ( `member_status` ) REFERENCES `member_statuses` ( `id` );

ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_nationality_foreign`
	FOREIGN KEY ( `nationality` ) REFERENCES `nationalities` ( `id` );

ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_professional_status_foreign`
	FOREIGN KEY ( `professional_status` ) REFERENCES `professional_statuses` ( `id` );

ALTER TABLE `aidweb`.`user_profiles` ADD CONSTRAINT `user_profiles_user_id_foreign`
	FOREIGN KEY ( `user_id` ) REFERENCES `users` ( `id` );

