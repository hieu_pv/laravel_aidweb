// let fleavaAdminServices throw undefined if not found
fleavaAdminServices.factory('videosService', ['$http',
  function ($http) {
    var service = {};
    var dt_table = null;

    var filters = {
      uploader: 0,
      published: -1
    };
    service.filters = filters;

    var options = {
      uploaders: []
    };
    service.options = options;

    $(service).on('filter_changed', function () {
      redrawDataTable();
    });

    function setUploaders () {
      $http.get(window.libraries.ws_urls.videos.authors()).success(function (data) {
        data.authors.splice(0, 0, { id: 0, org_name: "-- All" });
        options.uploaders = data.authors;
        $(service).trigger('options_uploaders_changed');
      });
    }
    service.setUploaders = setUploaders;

    function showDeletedMessage (title) {
      $.gritter.add({
        title: 'Video Deleted!',
        text: 'You have successfully deleted Video: <br/>' + title,
        sticky: false,
        time: ''
      });
    }

    function setupDeleteConfirmation () {
      $('.btnDestroyVideo').confirmation({
        trigger: 'click',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        onConfirm: function () {
          var e = this;
          if (dt_table) {
            var pos = dt_table.fnGetPosition($(e).parents('tr').get(0));
            var data = dt_table.fnGetData($(e).parents('tr').get(0));
            $.post(window.libraries.ws_urls.videos.destroy(data.id), function (d) {
              if (d.deleted) {
                $(e).parents('tr').fadeOut(500, function  () {
                  dt_table.fnDeleteRow(pos);
                });
                showDeletedMessage(data.title);
              }
            });
          }
        }
      });
    }

    function setPublishedSwitches () {
      $('.transform-switchable').bootstrapSwitch();
      $('.transform-switchable').bootstrapSwitch('setOnLabel', 'Yes');
      $('.transform-switchable').bootstrapSwitch('setOffLabel', 'No');
      $('.transform-switchable').bootstrapSwitch('setOnClass', 'success');
      $('.transform-switchable').bootstrapSwitch('setOffClass', 'danger');
      $('.transform-switchable').on('switch-change', function(e, data) {
        if (dt_table) {
          var pos = dt_table.fnGetPosition($(this).parents('tr').get(0));
          var tbData = dt_table.fnGetData($(this).parents('tr').get(0));
          if (data.value) {
            $.post(window.libraries.ws_urls.videos.publish(tbData.id), function (ping) {
              if (ping.published) {
                $.gritter.add({
                  title: 'Video Published!',
                  text: 'You have successfully published Video: ' + tbData.title
                });
              } else {
                $.gritter.add({
                  title: 'Cannot Publish Video!',
                  text: 'Something is not right. Please try again.'
                });
              }
            });
          } else {
            $.post(window.libraries.ws_urls.videos.unpublish(tbData.id), function (ping) {
              if (ping.unpublished) {
                $.gritter.add({
                  title: 'Video Un-Published!',
                  text: 'You have successfully un-published Video: ' + tbData.title
                });
              } else {
                $.gritter.add({
                  title: 'Cannot Un-Publish Video!',
                  text: 'Something is not right. Please try again.'
                });
              }
            });
          }
        }
      });
    }

    var loadstate = function () {
      var datatable = false;
      var o = {};
      o.setDataTableState = function (value) {
        datatable = value;
        $(o).trigger('state_changed');
      };
      o.getDataTableState = function () {
        return datatable;
      };
      o.loaded = function () {
        return datatable;
      };
      return o;
    } ();

    $(loadstate).on('state_changed', function () {
      if (loadstate.loaded()) {
        showDataArea();
      }
    });

    function showDataArea () {
      $('.list-area-loader').slideUp();
      $('.list-area').fadeTo(400, 1);
    }

    function setDataTable () {
      dt_table = $('#table_videos').dataTable({
        "aaSorting": [],
        "bProcessing": true,
        "iDisplayLength": 10,
        "bServerSide": true,
        "sAjaxSource": window.libraries.ws_urls.videos.getData(),
        "fnServerParams": function (aoData) {
          var published = filters.published;
          if (!$.isNumeric(published)) {
            published = -1;
          }
          var uploader = filters.uploader;
          if (!$.isNumeric(uploader)) {
            uploader = 0;
          }
          aoData.push({
            "name": "publishedFilter",
            "value": published
          });
          aoData.push({
            "name": "uploaderFilter",
            "value": uploader
          });
        },
        "aoColumns": [
          { "mData": "id" },
          { "mData": "title" },
          { "mData": "organisation_name" },
          { "mData": "created_at" },
          { "mData": "updated_at" },
          { "mData": "published" },
          { "mData": null }
        ],
        "aoColumnDefs": [
          { "sClass": "title", "aTargets": [1] },
          { "bSearchable": false, "bSortable": false, "aTargets": [2] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [5] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [6] }
        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var delMsg = 'Remove Video: <br/> ' + aData.title.replace(/"/g, '&quot;') + '.<br/>Are you sure?';
          var actionHtml = '';
          if (window._ouser.current.isOrganisation) {
            actionHtml += '<a href="' + window.libraries.ws_urls.videos.edit(aData.id) + '" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>&nbsp;';
          }
          if (window._ouser.current.isAdmin) {
            actionHtml += '<a href="' + window.libraries.ws_urls.videos.preview(aData.id) + '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-search"></i></a>&nbsp;';
          }
          var delAttr = ' data-title="' + delMsg + '" data-placement="left" data-singleton="true" data-popout="false" ';
          actionHtml += '<a href="#!" class="btn btn-danger btn-xs btnDestroyVideo"' + delAttr + '><i class="fa fa-times"></i></a>';
          $('td:eq(6)', nRow).append(actionHtml);

          var publishedHtml = '';
          var publishedAttr = '';
          if (window._ouser.current.isOrganisation) {
            publishedAttr = 'read-only disabled';
          }
          if (aData.published) {
            publishedHtml = '<input type="checkbox" checked class="transform-switchable switch-mini" ' + publishedAttr + '>';
          } else {
            publishedHtml = '<input type="checkbox" class="transform-switchable switch-mini" ' + publishedAttr + '>';
          }
          $('td:eq(5)', nRow).html(publishedHtml);

          // title
          var videoThumb = aData.image;
          var titleHtml = '';
          titleHtml += '<img width="48" height="auto" src="' + videoThumb + '">';
          titleHtml += '<span>' + aData.title + '</span>';
          //titleHtml += '<span>' + aData.youtube_id + '</span>';
          $('td:eq(1)', nRow).html($(titleHtml));
        },
        "fnDrawCallback": function (settings) {
          $('ins.dark-tooltip').remove();
          setupDeleteConfirmation();
          setPublishedSwitches();
          if (window._ouser.current.isOrganisation) {
            $('#table_videos tr td:nth-child(3)').css('display', 'none');
            $('#table_videos tr th:nth-child(3)').css('display', 'none');
          }
        },
        "fnInitComplete": function () {
          loadstate.setDataTableState(true);
        }
      });
    }

    function redrawDataTable () {
      dt_table.fnDraw();
    }

    service.onDocumentReady = function () {
      setDataTable();
    };

    // CREATE UPDATE 
    service.createupdate = {};
    service.createupdate.videoDetails = {};
    service.createupdate.getVideoDetails = function (url) {
      var d = $.Deferred();
      $('.video-cu-dynamic-generated-area').block({ message: '', css: { display: 'none' } });
      $http.get(window.libraries.ws_urls.common.getVideoDetails(url))
        .success(function (data) {
          $('.video-cu-dynamic-generated-area').unblock();
          if (data.details) {
            service.createupdate.videoDetails = data.details;
            d.resolve();
          } else {
            d.reject();
          }
        })
        .error(function () {
          $('.video-cu-dynamic-generated-area').unblock();
          d.reject();
        });
      return d.promise();
    };
    service.createupdate.title = {
      get: function () {
        return $('#title').val();
      },
      set: function (value) {
        $('#title').val(value);
      }
    };
    service.createupdate.slug = {
      get: function () {
        return $('#slug').val();
      },
      set: function (value) {
        $('#slug').val(value);
      }
    };
    // service.createupdate.authors = {
    //   get: function () {
    //     // var authors = $('.select-author').select2('data');
    //     // return authors;
    //     return $('#author').val();
    //   },
    //   set: function (value) {
    //     //$('.select-author').select2('val', value);
    //     $('#author').val(value);
    //   }
    // };
    // service.createupdate.setAuthorsForPost = function () {
    //   $('#author_obj').val(JSON.stringify(service.createupdate.authors.get()));
    // };
    service.createupdate.onDocumentReady = function () {
      $(document).on('blur', '#title', function (e) {
        e.preventDefault();
        $('.btnGenerateSlug').trigger('click');
      });
      $(document).on('click', '.btnGenerateSlug', function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.find('.fa-refresh').addClass('fa-spin');
        $this.find('.fa-refresh').prev().remove();
        $this.find('.fa-refresh').before('<span>generating..&nbsp;&nbsp;&nbsp;</span>');
        $this.attr('disabled', 'disabled');
        $this.prop('disabled', true);
        $.get(window.libraries.ws_urls.common.getSlug(service.createupdate.title.get()), function (data) {
          if (data) {
            if (data.slug) {
              service.createupdate.slug.set(data.slug);
            }
          }
        }).always(function () {
          $this.find('.fa-refresh').removeClass('fa-spin');
          $this.find('.fa-refresh').prev().remove();
          $this.removeAttr('disabled');
          $this.prop('disabled', false);
        });
      });

      // $('.select-author').select2({
      //   tags: [],
      //   tokenSeparators: [','],
      //   minimumResultsForSearch: 0
      // });

      $('#yes_featured, #no_featured').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    };

    return service;
  }
]);
