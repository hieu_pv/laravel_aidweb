/*jshint multistr: true */

fleavaAdminApp.directive('ngDeactivatePlatformConfirmation', function () {
  return {
    link: function (scope, element, attrs) {
      var options = scope.$eval(attrs.ngDeactivatePlatformConfirmation);
      var platformName = options.platformName;
      var platformId = options.platformId;
      jQuery(element).confirmation({
        trigger: 'manual',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        singleton: true,
        placement: 'left',
        title: 'You are about to deactivate platform ' + platformName + '. Are you sure?',
        onConfirm: function () {
          jQuery.post(window.libraries.ws_urls.common.deactivatePlatform(platformId), null, function (response, textStatus, jqXhr) {
            if (response.ok) {
              window.location.reload();
            }
          });
        },
        onCancel: function () {
          jQuery(element).confirmation('hide');
        }
      });
      jQuery(element).click(function (e) {
        jQuery(element).confirmation('show');
        e.preventDefault();
        e.stopPropagation();
      });
    }
  };
});

fleavaAdminApp.directive('ngActivatePlatformConfirmation', function () {
  return {
    link: function (scope, element, attrs) {
      var options = scope.$eval(attrs.ngActivatePlatformConfirmation);
      var platformName = options.platformName;
      var platformId = options.platformId;
      jQuery(element).confirmation({
        trigger: 'manual',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        singleton: true,
        placement: 'left',
        title: 'You are about to activate platform ' + platformName + '. Are you sure?',
        onConfirm: function () {
          jQuery.post(window.libraries.ws_urls.common.activatePlatform(platformId), null, function (response, textStatus, jqXhr) {
            if (response.ok) {
              window.location.reload();
            }
          });
        },
        onCancel: function () {
          jQuery(element).confirmation('hide');
        }
      });
      jQuery(element).click(function (e) {
        jQuery(element).confirmation('show');
        e.preventDefault();
        e.stopPropagation();
      });
    }
  };
});

fleavaAdminController.controller('ManagePlatformsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  function ($rootScope, $scope, $http) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.getCountries = function () {
      $http.get(window.libraries.ws_urls.common.getCountriesForPlatform()).success(function(data, status, headers, config) {
        $scope.countries = data.countries;
        $scope.safeApply();
      }).error(function(data, status, headers, config) {
      });
    };
    $scope.selected_country = 0;
    $scope.createPlatform = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      if ($scope.selected_country) {
        var l = Ladda.create($event.currentTarget);
        l.start();
        jQuery.post(window.libraries.ws_urls.common.createPlatform(), { selected_country: $scope.selected_country }, function (response, text, jqXhr) {
          if (response.ok) {
            window.location.reload();
          } else {
            l.stop();
          }
        }).fail(function () {
          l.stop();
        }).always(function () {
          
        });
      }
    };
    angular.element(document).ready(function () {
      $scope.getCountries();
    });
  }
]);