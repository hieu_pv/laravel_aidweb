(function ($) {
  $(function () {

    $('#resetPasswordModal').on('show.bs.modal', function (e) {
      clearResetPasswordMessages();
      clearResetPasswordForm();
    });

    var clearResetPasswordForm = function () {
      $('#resetPasswordModal input').val('');
    };

    var clearResetPasswordMessages = function () {
      $('#resetPasswordModal .messages-area .alert-danger').addClass('hide');
      $('#resetPasswordModal .messages-area .alert-success').addClass('hide');
    };

    var showResetPasswordErrorMessages = function (messages) {
      clearResetPasswordMessages();
      $('#resetPasswordModal .messages-area .alert-danger').removeClass('hide');
      var html = '<ul>';
      for (var i = 0; i < messages.length; i++) {
        html+= '<li>' + messages[i] + '</li>';
      }
      html += '</ul>';
      $('#resetPasswordModal .messages-area .alert-danger').empty();
      $('#resetPasswordModal .messages-area .alert-danger').append('<i class="fa fa-times-circle"></i> <strong>Error!</strong>');
      $('#resetPasswordModal .messages-area .alert-danger').append(html);
    };

    var showResetPasswordSuccessMessages = function (messages) {
      clearResetPasswordMessages();
      $('#resetPasswordModal .messages-area .alert-success').removeClass('hide');
      var html = '<ul>';
      for (var i = 0; i < messages.length; i++) {
        html+= '<li>' + messages[i] + '</li>';
      }
      html += '</ul>';
      $('#resetPasswordModal .messages-area .alert-success').empty();
      $('#resetPasswordModal .messages-area .alert-success').append('<i class="fa fa-check-circle"></i> <strong>Success!</strong>');
      $('#resetPasswordModal .messages-area .alert-success').append(html);
    };

    $('#btn_reset_password').click(function (e) {
      var dt = {
        email_to_reset_password: $('#email_to_reset_password').val(),
        username_to_reset_password: $('#username_to_reset_password').val()
      };
      var l = Ladda.create(document.querySelector('#btn_reset_password'));
      l.start();
      $.post(window.libraries.ws_urls.common.resetPassword(), dt, function (ping) {
        if (ping.reset) {
          $('#hiddenUsernameToResetPassword').val(ping.username);
          $('#hiddenEmailToResetPassword').val(ping.email);
          showResetPasswordSuccessMessages(['An Email has been sent to you providing instructions to reset your password. If you do not receive the instructions yet, try <a href="#" class="resend-reminder"><strong>re-sending the instructions</strong></a>. <span class="resend-reminder-sending" style="white-space:nowrap;"></span>']);
        } else {
          showResetPasswordErrorMessages(ping.messages);
        }
      })
      .fail(function () {
        showResetPasswordErrorMessages(['System cannot perform steps to reset your password due to network issues or internal system error. Please refresh the page and try again or contact AidPost administrator.']);
      })
      .always(function () {
        l.stop();
      });
      e.preventDefault();
    });

    $(document).on('click', '.resend-reminder', function (e) {
      var data = {
        username_to_reset_password: $('#hiddenUsernameToResetPassword').val(),
        email_to_reset_password: $('#hiddenEmailToResetPassword').val()
      };
      $('.resend-reminder-sending').html('<i class="fa fa-spinner fa-spin"></i> Sending email..');
      $.post(window.libraries.ws_urls.common.resendReminder(), data, function (ping) {
        if (ping.ok) {

        } else {
          // what to do here?
        }
      })
      .done(function () {
        $('.resend-reminder-sending').html('<i class="fa fa-check"></i> Email sent.');
      })
      .fail(function () {
        $('.resend-reminder-sending').html('<i class="fa fa-times"></i> Something wrong. Please refresh page and try again.');
      })
      .always(function () {
        //$('.resend-reminder-sending').html('');
      });
      e.preventDefault();
    });

    $(document).on('click', '.resend-verification', function (e) {
      e.preventDefault();
      var dis = this;
      var target = $(this).attr('data-url');
      $('.alert.alert-danger').after('<div style="margin-bottom: 5px !important;"><strong><i class="fa fa-spinner fa-spin"></i> sending mail..</strong></div>');
      $.get(target)
      .always(function () {
        
      })
      .done(function () {
        $('.alert.alert-danger').next().remove();
        $('.alert.alert-danger').after('<div style="margin-bottom: 5px !important;"><strong>Email Sent!</strong></div>');
        window.setTimeout(function () {
          $('.alert.alert-danger').next().remove();
        }, 5000);
      });
    });

    /**************************************************************************************************************************************/

    $('#resendVerificationModal').on('show.bs.modal', function (e) {
      clearResendVerificationForm();
      clearResendVerificationMessages();
    });

    var clearResendVerificationForm = function () {
      $('#resendVerificationModal input').val('');
    };

    var clearResendVerificationMessages = function () {
      $('#resendVerificationModal .messages-area .alert-danger').addClass('hide');
      $('#resendVerificationModal .messages-area .alert-success').addClass('hide');
    };

    var showResendVerificationSuccessMessages = function (messages) {
      clearResendVerificationMessages();
      $('#resendVerificationModal .messages-area .alert-success').removeClass('hide');
      var html = '<ul>';
      for (var i = 0; i < messages.length; i++) {
        html+= '<li>' + messages[i] + '</li>';
      }
      html += '</ul>';
      $('#resendVerificationModal .messages-area .alert-success').empty();
      $('#resendVerificationModal .messages-area .alert-success').append('<i class="fa fa-check-circle"></i> <strong>Success!</strong>');
      $('#resendVerificationModal .messages-area .alert-success').append(html);
    };

    $('#btn_resend_verification').click(function (e) {
      var dt = {
        email_to_resend_verification: $('#email_to_resend_verification').val(),
        username_to_resend_verification: $('#username_to_resend_verification').val()
      };
      var l = Ladda.create(document.querySelector('#btn_resend_verification'));
      l.start();
      $.post(window.libraries.ws_urls.common.resendVerification(), dt, function (ping) {
        if (ping.ok) {
          $('#hiddenUsernameToResendVerification').val(ping.username);
          $('#hiddenEmailToResendVerification').val(ping.email);
          showResendVerificationSuccessMessages(['An Email has been sent to you providing instructions to verify your account.']);
        } else {
        }
      })
      .fail(function () {
      })
      .always(function () {
        l.stop();
      });
      e.preventDefault();
    });

  });
} (jQuery));