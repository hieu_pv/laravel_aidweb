fleavaAdminController.controller('NewsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'newsService',
  function ($rootScope, $scope, $http, newsService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.newsService = newsService;
    $scope.selectedPublishedFilter = -1;
    $scope.selectedAuthor = {};
    $(newsService).on('options_authors_changed', function () {
      $scope.selectedAuthor = newsService.options.authors[0];
      $scope.$apply();
    });
    $scope.filterChanged = function () {
      newsService.filters.author = $scope.selectedAuthor.id;
      newsService.filters.published = $scope.selectedPublishedFilter;
      $(newsService).trigger('filter_changed');
    };
    angular.element(document).ready(function () {
      newsService.onDocumentReady();
      $scope.safeApply();
    });
  }
]);

fleavaAdminController.controller('NewsItemCreateUpdateCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'newsService',
  function ($rootScope, $scope, $http, newsService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    angular.element(document).ready(function () {
      newsService.createupdate.onDocumentReady();
      $scope.safeApply();
    });
  }
]);