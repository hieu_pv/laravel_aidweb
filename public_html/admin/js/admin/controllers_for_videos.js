fleavaAdminController.controller('VideosCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'videosService',
  function ($rootScope, $scope, $http, videosService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.videosService = videosService;
    $scope.selectedPublishedFilter = -1;
    $scope.selectedUploader = {};
    $(videosService).on('options_uploaders_changed', function () {
      $scope.selectedUploader = videosService.options.uploaders[0];
      $scope.safeApply();
    });
    $scope.filterChanged = function () {
      videosService.filters.uploader = $scope.selectedUploader.id;
      videosService.filters.published = $scope.selectedPublishedFilter;
      $(videosService).trigger('filter_changed');
    };
    angular.element(document).ready(function () {
      videosService.onDocumentReady();
      videosService.setUploaders();
      $scope.safeApply();
    });
  }
]);

fleavaAdminController.controller('VideoCreateUpdateCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'videosService',
  function ($rootScope, $scope, $http, videosService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.videoUrl = '';
    $scope.videoYoutubeId = '';
    $scope.videoThumbnail = '/images/noimage.png';
    $scope.videoDuration = '';
    $scope.urlBlurred = function () {
      var url = $scope.videoUrl;
      var p = videosService.createupdate.getVideoDetails(url);
      p.done(function () {
        $scope.videoYoutubeId = videosService.createupdate.videoDetails.youtube_id;
        $scope.videoThumbnail = videosService.createupdate.videoDetails.thumbnail;
        $scope.videoDuration = videosService.createupdate.videoDetails.duration;
        $scope.safeApply();
      });
    };
    $scope.formSubmitted = function () {
      //videosService.createupdate.setAuthorsForPost();
    };
    angular.element(document).ready(function () {
      videosService.createupdate.onDocumentReady();
      //videosService.createupdate.authors.set(window._video_cu_authors);
      $scope.videoUrl = window._video_cu_url;
      $scope.urlBlurred();
      $scope.safeApply();
    });
  }
]);
