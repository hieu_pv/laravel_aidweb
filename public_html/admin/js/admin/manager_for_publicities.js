fleavaAdminServices.factory('publicitiesService', ['$http',
  function ($http) {

    var service = {};
    var dt_table = null;

    var filters = {
      author: 0,
      published: -1,
      type: 0
    };
    service.filters = filters;

    $(service).on('filter_changed', function () {
      redrawDataTable();
    });

    function redrawDataTable () {
      dt_table.fnDraw();
    }

    var options = {
      types: [],
      authors: []
    };
    service.options = options;

    function setTypes () {
      $http.get(window.libraries.ws_urls.publicities.types()).success(function (data) {
        data.types.splice(0, 0, { id: 0, name: "-- All" });
        options.types = data.types;
        $(service).trigger('options_types_changed');
      });
    }
    service.setTypes = setTypes;

    function setAuthors () {
      $http.get(window.libraries.ws_urls.publicities.authors()).success(function (data) {
        data.authors.splice(0, 0, { id: 0, org_name: "-- All" });
        options.authors = data.authors;
        $(service).trigger('options_authors_changed');
      });
    }
    service.setAuthors = setAuthors;

    function showDeletedMessage (str) {
      $.gritter.add({
        title: 'Publicity Deleted!',
        text: 'You have successfully deleted Publicity: <br/>' + str,
        sticky: false,
        time: ''
      });
    }

    function setupDeleteConfirmation () {
      $('.btnDestroyPublicity').confirmation({
        trigger: 'click',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        onConfirm: function () {
          var e = this;
          if (dt_table) {
            var pos = dt_table.fnGetPosition($(e).parents('tr').get(0));
            var data = dt_table.fnGetData($(e).parents('tr').get(0));
            $.post(window.libraries.ws_urls.publicities.destroy(data.id), function (d) {
              if (d.deleted) {
                $(e).parents('tr').fadeOut(500, function  () {
                  dt_table.fnDeleteRow(pos);
                });
                showDeletedMessage(data.name);
              }
            });
          }
        }
      });
    }

    function setPublishedSwitches() {
      $('.transform-switchable').bootstrapSwitch();
      $('.transform-switchable').bootstrapSwitch('setOnLabel', 'Yes');
      $('.transform-switchable').bootstrapSwitch('setOffLabel', 'No');
      $('.transform-switchable').bootstrapSwitch('setOnClass', 'success');
      $('.transform-switchable').bootstrapSwitch('setOffClass', 'danger');
      $('.transform-switchable').on('switch-change', function(e, data) {
        if (dt_table) {
          var pos = dt_table.fnGetPosition($(this).parents('tr').get(0));
          var tbData = dt_table.fnGetData($(this).parents('tr').get(0));
          if (data.value) {
            $.post(window.libraries.ws_urls.publicities.publish(tbData.id), function (ping) {
              if (ping.published) {
                $.gritter.add({
                  title: 'Publicity Published!',
                  text: 'You have successfully published Publicity: ' + tbData.name
                });
              } else {
                $.gritter.add({
                  title: 'Cannot Publish Publicity!',
                  text: 'Something is not right. Please try again.'
                });
              }
            });
          } else {
            $.post(window.libraries.ws_urls.publicities.unpublish(tbData.id), function (ping) {
              if (ping.unpublished) {
                $.gritter.add({
                  title: 'Publicity Un-Published!',
                  text: 'You have successfully un-published Publicity: ' + tbData.name
                });
              } else {
                $.gritter.add({
                  title: 'Cannot Un-Publish Publicity!',
                  text: 'Something is not right. Please try again.'
                });
              }
            });
          }
        }
      });
    }

    function setDataTable () {
      dt_table = $('#table_publicities').dataTable({
        "aaSorting": [],
        "bProcessing": true,
        "iDisplayLength": 100,
        "bServerSide": true,
        "sAjaxSource": window.libraries.ws_urls.publicities.getData(),
        "fnServerParams": function (aoData) {
          var published = filters.published;
          if (!$.isNumeric(published)) {
            published = -1;
          }
          var author = filters.author;
          if (!$.isNumeric(author)) {
            author = 0;
          }
          var type = filters.type;
          if (!$.isNumeric(type)) {
            type = 0;
          }
          aoData.push({
            "name": "publishedFilter",
            "value": published
          });
          aoData.push({
            "name": "authorFilter",
            "value": author
          });
          aoData.push({
            "name": "typeFilter",
            "value": type
          });
        },
        "aoColumns": [
          { "mData": "id" },
          { "mData": "name" },
          { "mData": "author" },
          { "mData": "type_of_publicity.name" },
          { "mData": "created_at" },
          //{ "mData": "published_at" },
          { "mData": "published_start_date" },
          { "mData": "published" },
          { "mData": null }
        ],
        "aoColumnDefs": [
          { "sClass": "title", "aTargets": [1] },
          { "bSearchable": false, "bSortable": false, "aTargets": [2] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-left", "aTargets": [3] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [6] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [7] }
        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var name = aData.name,
              image = aData.image,
              url = aData.url;

          var delMsg = 'Remove Publicity: <br/> ' + name + '.<br/>Are you sure?';
          var actionHtml = '';
          if (window._ouser.current.isOrganisation) {
            actionHtml += '<a href="' + window.libraries.ws_urls.publicities.edit(aData.id) + '" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>&nbsp;';
          }
          if (window._ouser.current.isAdmin) {
            actionHtml += '<a href="' + window.libraries.ws_urls.publicities.preview(aData.id) + '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-search"></i></a>&nbsp;';
          }
          var delAttr = ' data-name="' + delMsg + '" data-placement="left" data-singleton="true" data-popout="false" ';
          actionHtml += '<a href="#!" class="btn btn-danger btn-xs btnDestroyPublicity"' + delAttr + '><i class="fa fa-times"></i></a>';
          $('td:eq(7)', nRow).append(actionHtml);

          var publishedHtml = '';
          var publishedAttr = '';
          if (window._ouser.current.isOrganisation) {
            publishedAttr = 'read-only disabled';
          }
          if (aData.published) {
            publishedHtml = '<input type="checkbox" checked class="transform-switchable switch-mini" ' + publishedAttr + '>';
          } else {
            publishedHtml = '<input type="checkbox" class="transform-switchable switch-mini" ' + publishedAttr + '>';
          }
          $('td:eq(6)', nRow).html(publishedHtml);

          var html1 = '';
          html1 += '<img width="48" height="auto" src="/imagecache/r_100_auto_c' + image + '">';
          html1 += '<span>' + name + '</span>';
          $('td:eq(1)', nRow).html($(html1));

          // type
          var typeHtml = $('<strong class=""></strong>');
          $(typeHtml).text(aData.type_of_publicity.name);
          $('td:eq(3)', nRow).html(typeHtml);

          // start date and status
          var statusHtml = '<strong class="published_start_date">' + aData.published_start_date + '</strong>';
          statusHtml += '<span style="display:inline-block;">&nbsp;-&nbsp;' + aData.num_of_months + ' month(s)</span>';
          if (aData.publicly_published) {
            statusHtml += '<span class="status-published"><i class="fa fa-check"></i> Published</span>';
          } else if (aData.overdue) {
            statusHtml += '<span class="status-overdue"><i class="fa fa-level-down"></i> Overdue</span>';
          } else if (aData.upcoming) {
            statusHtml += '<span class="status-upcoming"><i class="fa fa-clock-o"></i> Upcoming</span>';
          } else if (!aData.published) {
            statusHtml += '<span class="status-notapproved"><i class="fa fa-ban"></i> Not Approved</span>';
          }
          $('td:eq(5)', nRow).html(statusHtml);

          var createdAtHtml = '<span>' + aData.created_at2 + '</span>';
          $('td:eq(4)', nRow).html(createdAtHtml);
        },
        "fnDrawCallback": function (settings) {
          $('ins.dark-tooltip').remove();
          setupDeleteConfirmation();
          setPublishedSwitches();
          if (window._ouser.current.isOrganisation) {
            $('#table_publicities tr td:nth-child(3)').css('display', 'none');
            $('#table_publicities tr th:nth-child(3)').css('display', 'none');
          }
        },
        "fnInitComplete": function () {
          loadstate.setDataTableState(true);
        }
      });
    }

    var loadstate = function () {
      var datatable = false;
      var o = {};
      o.setDataTableState = function (value) {
        datatable = value;
        $(o).trigger('state_changed');
      };
      o.getDataTableState = function () {
        return datatable;
      };
      o.loaded = function () {
        return datatable;
      };
      return o;
    } ();

    $(loadstate).on('state_changed', function () {
      if (loadstate.loaded()) {
        showDataArea();
      }
    });

    function showDataArea () {
      $('.list-area-loader').slideUp();
      $('.list-area').fadeTo(400, 1);
    }

    service.onDocumentReady = function () {
      setDataTable();
    };

    /******************************************************************************************************/

    var _rules = [
      {
        'name': { required: true },
        //'a_title': { required: true },
        //'a_title_color': { required: true },
        //'a_text': { required: true },
        //'a_text_color': { required: true },
        'a_button_text': { required: true },
        //'a_button_text_color': { required: true },
        'a_position': { required: true },
        //'a_image': { required: true, accept: 'image/*' },
        'a_url': { required: true }
      },
      {
        'name': { required: true },
        //'b_title': { required: true },
        //'b_image': { required: true, accept: 'image/*' },
        'b_url': { required: true }
      },
      {
        'name': { required: true },
        //'c_title': { required: true },
        //'c_image': { required: true, accept: 'image/*' },
        'c_url': { required: true }
      }
    ];

    function setFormValidator () {
      for (var item in service.createupdate.activeRules) {
        if ($('#' + item).length > 0) {
          $('#' + item).rules('remove');
        }
      }
      service.createupdate.activeRules = _rules[service.createupdate.selectedType - 1];
      for (var item2 in service.createupdate.activeRules) {
        if ($('#' + item2).length > 0) {
          $('#' + item2).rules('add', service.createupdate.activeRules[item2]);
        }
      }
    }

    service.createupdate = {};
    service.createupdate.activeRules = {};
    service.createupdate.selectedType = 1;
    service.createupdate.onDocumentReady = function () {
      $('input[id^="choose_pub_type_"], input[id^="choose_title_color"], input[id^="choose_text_color"], input[id^="choose_button_text_color"], input[id^="choose_position_"], input[name="a_show_org_logo"]').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });

      $('input[id^="choose_pub_type_"], input[id^="choose_title_color"], input[id^="choose_text_color"], input[id^="choose_button_text_color"], input[id^="choose_position_"], input[name="a_show_org_logo"]').on('ifChecked', function (e) {
        var targetId = $(this).attr('id');
        $('#' + targetId).trigger('click');
      });

      service.createupdate.activeRules = _rules[service.createupdate.selectedType - 1];
      $('#form_cu_publicity').validate({
        //debug: true,
        rules: service.createupdate.activeRules,
        errorPlacement: function (error, element) {
          var $target = $(element).closest('.form-group').find('label').next();
          error.appendTo($target);
        }
      });

      $('.dp_published_start_date').datepicker();
      $('#selSelectedPlatforms').select2();
      $('#selSelectedPlatforms').val(eval($('#hdnSelectedPlatforms').val())).trigger("change");
    };

    $(service.createupdate).on('changed.pub_type', function (e) {
      $('input[id^="choose_pub_type_"]').iCheck('update');
      $('#pub_type_tabs [href="#pub_type_' + service.createupdate.selectedType + '"]').tab('show');
      setFormValidator();
    });

    return service;

  }
]);
