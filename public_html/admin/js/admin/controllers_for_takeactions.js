fleavaAdminController.controller('TakeActionsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'takeActionsService',
  function ($rootScope, $scope, $http, takeActionsService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.takeActionsService = takeActionsService;
    $scope.selectedPublishedFilter = -1;
    $scope.selectedType = {};
    $(takeActionsService).on('options_types_changed', function () {
      $scope.selectedType = takeActionsService.options.types[0];
      $scope.safeApply();
    });
    $scope.selectedAuthor = {};
    $(takeActionsService).on('options_authors_changed', function () {
      $scope.selectedAuthor = takeActionsService.options.authors[0];
      $scope.safeApply();
    });
    $scope.filterChanged = function () {
      takeActionsService.filters.author = $scope.selectedAuthor.id;
      takeActionsService.filters.type = $scope.selectedType.id;
      takeActionsService.filters.published = $scope.selectedPublishedFilter;
      $(takeActionsService).trigger('filter_changed');
    };
    angular.element(document).ready(function () {
      takeActionsService.onDocumentReady();
      takeActionsService.setTypes();
      takeActionsService.setAuthors();
      $scope.safeApply();
    });
  }
]);

fleavaAdminController.controller('TakeActionCreateUpdateCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'takeActionsService',
  function ($rootScope, $scope, $http, takeActionsService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.selectedType = 0;
    $scope.selectedTypeColor = '#a1a1a1';
    $scope.typeChanged = function () {
      var found = null;
      for (var i = 0; i < $scope.types.length; i++) {
        var item = $scope.types[i];
        if (item.id === $scope.selectedType) {
          found = item;
          break;
        }
      }
      if (found) {
        $scope.selectedTypeColor = found.color;
      } else {
        $scope.selectedTypeColor = '#a1a1a1';
      }
    };
    angular.element(document).ready(function () {
      takeActionsService.createupdate.onDocumentReady();
      $scope.types = window._types_of_take_actions || {};
      $scope.selectedType = window._selected_take_action_type_id;
      $scope.typeChanged();
      $scope.safeApply();
    });
  }
]);