/*jshint multistr: true */
fleavaAdminApp.directive('ngAddFilter', function () {
  return {
    link: function (scope, element, attrs) {
      var options = scope.$eval(attrs.ngAddFilter);
      var filterCategory = options.filterCategory;
      jQuery(element).find('.ok').click(function (e) {
        e.preventDefault();
        var n = scope.addFilterInputText[filterCategory];
        if (!n) {
          return false;
        }
        var d = {
          filterName: filterCategory,
          itemName: n
        };
        jQuery(element).find('.ok').html('<i class="fa fa-spinner fa-spin"></i>');
        jQuery.post(window.libraries.ws_urls.common.createFilter(), d, function (response, textStatus, jqXhr) {
          if (response.ok) {
            var id = response.id;
            scope.filters[filterCategory].push({ id: id, name: n });
            jQuery(element).find('.cancel').trigger('click');
            scope.safeApply();
            scope.relayout();
          }
        }).always(function () {
          jQuery(element).find('.ok').html('<i class="fa fa-check fa-fw"></i>');
        });
      });

      jQuery(element).find('.cancel').click(function (e) {
        e.preventDefault();
        scope.modes[filterCategory] = 'anything';
        scope.safeApply();
        scope.relayout();
      });
    }
  };
});

fleavaAdminApp.directive('ngDeleteConfirmation', function () {
  return {
    link: function (scope, element, attrs) {
      var options = scope.$eval(attrs.ngDeleteConfirmation);
      var filterItem = options.filterItem;
      var filterCategory = options.filterCategory;
      jQuery(element).confirmation({
        trigger: 'manual',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        singleton: true,
        placement: 'left',
        title: 'You are about to delete filter ' + filterItem.name + '. Are you sure?',
        onConfirm: function () {
          jQuery.post(window.libraries.ws_urls.common.deleteFilter(), { filterName: filterCategory, itemId: filterItem.id }, function (response, textStatus, jqXhr) {
            if (response.ok) {
              var idx = scope.findFilterById(filterCategory, filterItem.id);
              if (idx > -1) {
                scope.filters[filterCategory].splice(idx, 1);
                scope.safeApply();
                scope.relayout();
              }
            }
          });
        },
        onCancel: function () {
          jQuery(element).confirmation('hide');
        }
      });
      jQuery(element).click(function (e) {
        jQuery(element).confirmation('show');
        e.preventDefault();
        e.stopPropagation();
      });
    }
  };
});

fleavaAdminApp.directive('ngInlineEdit', function () {
  return {
    link: function (scope, element, attrs) {
      var options = scope.$eval(attrs.ngInlineEdit);
      var filterItem = options.filterItem;
      var filterCategory = options.filterCategory;

      // cancel button clicked
      jQuery(element).closest('.has-inline-editable').on('click', '.cancel', function (e) {
        e.preventDefault();
        var editable2 = jQuery(element).closest('.has-inline-editable').find('.inline-editable');
        var idx = scope.findFilterById(filterCategory, filterItem.id);
        var n = '';
        if (idx > -1) {
          n = scope.filters[filterCategory][idx].name;
        } else {
          n = filterItem.name;
        }
        var span = '<span class="inline-editable">' + n + '</span>';
        jQuery(editable2).replaceWith(span);
        jQuery(element).show();
        scope.relayout();
      });

      // ok button clicked
      jQuery(element).closest('.has-inline-editable').on('click', '.ok', function (e) {
        e.preventDefault();
        var n = jQuery(element).closest('.has-inline-editable').find('input[type="text"]').val();
        if (!n) {
          return false;
        }
        var d = {
          filterName: filterCategory,
          itemId: filterItem.id,
          itemName: n
        };
        jQuery(element).closest('.has-inline-editable').find('.ok').html('<i class="fa fa-spinner fa-spin"></i>');
        jQuery.post(window.libraries.ws_urls.common.updateFilter(), d, function (response, textStatus, jqXhr) {
          if (response.ok) {
            var idx = scope.findFilterById(filterCategory, filterItem.id);
            if (idx > -1) {
              scope.filters[filterCategory][idx].name = n;
              scope.safeApply();
            }
            jQuery(element).closest('.has-inline-editable').find('.cancel').trigger('click');
          }
        }).always(function () {
          jQuery(element).closest('.has-inline-editable').find('.ok').html('<i class="fa fa-check fa-fw"></i>');
        });
      });

      // edit button click
      jQuery(element).click(function (e) {
        // replace with input
        var editable = jQuery(element).closest('.has-inline-editable').find('.inline-editable');
        var input = '<div class="input-group inline-editable"> \
                     <input type="text" class="form-control" value="' + filterItem.name + '" /> \
                     <span class="input-group-btn"> \
                     <button class="btn btn-white ok text-success" type="button"><i class="fa fa-check fa-fw"></i></button> \
                     <button class="btn btn-white cancel text-danger" type="button"><i class="fa fa-times fa-fw"></i></button> \
                     </span> \
                     </div>';
        jQuery(editable).replaceWith(input);
        // hide the edit button
        jQuery(element).hide();
        scope.relayout();
        e.preventDefault();
        e.stopPropagation();
      });
    }
  };
});

fleavaAdminController.controller('ManageFiltersCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  function ($rootScope, $scope, $http) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.addFilterInputText = {};
    $scope.modes = {};
    $scope.filterCategories = [
      { tableName: 'beneficiaries', displayText: 'Beneficiary' },
      { tableName: 'crisis', displayText: 'Crisis/Disaster' },
      // { tableName: 'interventions', displayText: 'Intervention' },
      { tableName: 'themes', displayText: 'Theme' },
      { tableName: 'sectors', displayText: 'Sector' }
    ];
    $scope.findFilterById = function (name, id) {
      var filters = $scope.filters[name];
      var idx = -1;
      for (var i = 0; i < filters.length; i++) {
        if (filters[i].id === id) {
          idx = i;
          break;
        }
      }
      return idx;
    };
    $scope.getData = function () {
      $http.get(window.libraries.ws_urls.common.getAllFilters()).success(function(data, status, headers, config) {
        $scope.filters = data.filters;
        $scope.safeApply();
        $scope.relayout();
      }).error(function(data, status, headers, config) {
      });
    };
    $scope.iso = null;
    $scope.relayout = function () {
      if ($scope.iso) {
        setTimeout(function () {
          jQuery('.manage-filters .row').isotope('layout');
        }, 100);
      } else {
        setTimeout(function () {
          $scope.iso = jQuery('.manage-filters .row').isotope({
            itemSelector: '.col-md-12.col-lg-6',
          });
        }, 100);
      }
    };
    angular.element(document).ready(function () {
      $scope.getData();
    });
  }
]);