fleavaAdminController.controller('IndividualsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'individualsService',
  function ($rootScope, $scope, $http, individualsService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.individualsService = individualsService;
    $scope.selectedMembershipStatusFilter = -1;
    $scope.selectedTopStatusFilter = -1;
    $scope.selectedNationalityFilter = {};
    $scope.selectedBasedInFilter = {};
    $scope.selectedMemberStatusFilter = {};
    $scope.selectedProfessionalStatusFilter = {};
    $scope.filterChanged = function () {
      individualsService.filters.topStatus = $scope.selectedTopStatusFilter;
      individualsService.filters.membershipStatus = $scope.selectedMembershipStatusFilter;
      individualsService.filters.nationality = $scope.selectedNationalityFilter.id;
      individualsService.filters.memberStatus = $scope.selectedMemberStatusFilter.id;
      individualsService.filters.professionalStatus = $scope.selectedProfessionalStatusFilter.id;
      individualsService.filters.basedIn = $scope.selectedBasedInFilter.id;
      $(individualsService).trigger('filter_changed');
    };
    $(individualsService).on('options_nationalities_changed', function () {
      $scope.selectedNationalityFilter = individualsService.options.nationalities[0];
      $scope.safeApply();
    });
    $(individualsService).on('options_memberstatuses_changed', function () {
      $scope.selectedMemberStatusFilter = individualsService.options.memberStatuses[0];
      $scope.safeApply();
    });
    $(individualsService).on('options_professionalstatuses_changed', function () {
      $scope.selectedProfessionalStatusFilter = individualsService.options.professionalStatuses[0];
      $scope.safeApply();
    });
    $(individualsService).on('options_bases_changed', function () {
      $scope.selectedBasedInFilter = individualsService.options.bases[0];
      $scope.safeApply();
    });
    angular.element(document).ready(function () {
      individualsService.onDocumentReady();
      individualsService.setNationalities();
      individualsService.setMemberStatuses();
      individualsService.setProfessionalStatuses();
      individualsService.setBases();
      $scope.safeApply();
    });
  }
]);
