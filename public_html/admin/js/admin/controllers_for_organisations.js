fleavaAdminController.controller('OrganisationsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'organisationsService',
  function ($rootScope, $scope, $http, organisationsService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.organisationsService = organisationsService;
    $scope.selectedMembershipStatusFilter = -1;
    $scope.selectedTopStatusFilter = -1;
    $scope.selectedOrgTypeFilter = {};
    $scope.selectedOfficeLocationFilter = {};
    $scope.filterChanged = function () {
      organisationsService.filters.membershipStatus = $scope.selectedMembershipStatusFilter;
      organisationsService.filters.topStatus = $scope.selectedTopStatusFilter;
      organisationsService.filters.orgType = $scope.selectedOrgTypeFilter.id;
      organisationsService.filters.officeLocation = $scope.selectedOfficeLocationFilter.id;
      $(organisationsService).trigger('filter_changed');
    };
    $(organisationsService).on('options_orgtypes_changed', function () {
      $scope.selectedOrgTypeFilter = organisationsService.options.orgTypes[0];
      $scope.safeApply();
    });
    $(organisationsService).on('options_officelocations_changed', function () {
      $scope.selectedOfficeLocationFilter = organisationsService.options.officeLocations[0];
      $scope.safeApply();
    });
    angular.element(document).ready(function () {
      organisationsService.onDocumentReady();
      organisationsService.setOrgTypes();
      organisationsService.setOfficeLocations();
      $scope.safeApply();
    });
  }
]);