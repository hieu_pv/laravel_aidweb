// let fleavaAdminServices throw undefined if not found
fleavaAdminServices.factory('organisationsService', ['$http',
  function ($http) {
    var service = {};
    var dt_table = null;

    var filters = {
      orgType: 0,
      topStatus: -1,
      membershipStatus: -1,
      officeLocation: 0,
    };
    service.filters = filters;

    var options = {
      orgTypes: [],
      officeLocations: [],
    };
    service.options = options;

    $(service).on('filter_changed', function () {
      dt_table.fnDraw();
    });

    service.setOrgTypes = function () {
      $http.get(window.libraries.ws_urls.organisations.types()).success(function (data, status, headers, config) {
        data.orgTypes.splice(0, 0, { id: 0, name: "-- All" });
        options.orgTypes = data.orgTypes;
        $(service).trigger('options_orgtypes_changed');
      });
    };

    service.setOfficeLocations = function () {
      $http.get(window.libraries.ws_urls.common.getCountries()).success(function (data, status, headers, config) {
        data.countries.splice(0, 0, { id: 0, name: "-- All" });
        options.officeLocations = data.countries;
        $(service).trigger('options_officelocations_changed');
      });
    };
    
    function setTopAgencySwitches () {
      $('.is-top-switch').bootstrapSwitch();
      $('.is-top-switch').bootstrapSwitch('setOnLabel', 'Yes');
      $('.is-top-switch').bootstrapSwitch('setOffLabel', 'No');
      $('.is-top-switch').bootstrapSwitch('setOnClass', 'success');
      $('.is-top-switch').bootstrapSwitch('setOffClass', 'danger');
      $('.is-top-switch').on('switch-change', function (e, data) {
        //var platformId = $(e.target).attr('data-platform-id');
        //var orgId = $(e.target).attr('data-org-id');
        var platform = $(e.target).data('platform');
        var org = $(e.target).data('org');
        var jd = { platformId: platform.id, orgId: org.id };
        if (data.value) {
          $http.post(window.libraries.ws_urls.common.markOrgAsTopAtPlatform(), jd).success(function (response, status, headers, config) {
            if (response.ok) {
              $.gritter.add({
                title: 'Organisation set as Top Agency',
                text: 'You have successfully set organisation ' + org.org_name + ' as Top Agency in platform ' + platform.name + '. An email has been sent to the organisation.'
              });
            }
          }).error(function (response, status, headers, config) {
          });
        } else {
          $http.post(window.libraries.ws_urls.common.unmarkOrgFromTopAtPlatform(), jd).success(function (response, status, headers, config) {
            if (response.ok) {
              $.gritter.add({
                title: 'Organisation unset from Top Agency',
                text: 'You have successfully unset organisation ' + org.org_name + ' from Top Agency in platform ' + platform.name + '. An email has been sent to the organisation.'
              });
            }
          }).error(function (response, status, headers, config) {
          });
        }
      });
    }

    var removeExistingConfirmation = function (toggleSwitch) {
      if ($('#is_active_switch_confirm').length) {
        $('#is_active_switch_confirm').confirmation('destroy');
        if (toggleSwitch) {
          $('#is_active_switch_confirm').closest('td').find('.is-active-switch').bootstrapSwitch('toggleState', true);
        }
        $('#is_active_switch_confirm').remove();
      }
    };

    function setIsActiveSwitches () {
      $('.is-active-switch').bootstrapSwitch();
      $('.is-active-switch').bootstrapSwitch('setOnLabel', 'Yes');
      $('.is-active-switch').bootstrapSwitch('setOffLabel', 'No');
      $('.is-active-switch').bootstrapSwitch('setOnClass', 'success');
      $('.is-active-switch').bootstrapSwitch('setOffClass', 'danger');
      $('.is-active-switch').on('switch-change', function (e, data) {
        var dis = this;
        if (dt_table) {
          var pos = dt_table.fnGetPosition($(this).parents('tr').get(0));
          var aData = dt_table.fnGetData($(this).parents('tr').get(0));

          // e. does not work, use the GLOBAL "event" object to stop the click
          if (typeof event !== 'undefined') {
            event.preventDefault(); //e.preventDefault();
            event.stopPropagation(); //e.stopPropagation();
          } else {
            e.preventDefault();
            e.stopPropagation();
          }

          // remove existing confirmation box
          removeExistingConfirmation(true);

          var rmaHtml = '<a href="#" id="is_active_switch_confirm" class="is-active-switch-confirm">&nbsp;</a>';
          $(dis).closest('td').prepend(rmaHtml);

          if (data.value) {
            $('#is_active_switch_confirm').confirmation({
              trigger: 'manual',
              btnOkClass: 'btn-xs btn-primary-original',
              btnCancelClass: 'btn-xs btn-default-original',
              singleton: true,
              placement: 'left',
              title: 'You are about to activate membership of organisation: ' + aData.org_name + '.<br/>Are you sure?',
              onConfirm: function () {
                removeExistingConfirmation(false);
                $http.post(window.libraries.ws_urls.organisations.activate(), { org_id: aData.id })
                .success(function (d, status, headers, config) {
                  if (d.activated) {
                    $.gritter.add({
                      title: 'Organisation has been activated!',
                      text: 'You have successfully actived the membership of organisation ' + aData.org_name + '. An email has been sent to the organisation.'
                    });
                  }
                })
                .error(function (d, status, headers, config) {
                });
              },
              onCancel: function () {
                removeExistingConfirmation(true);
              }
            });
            $('#is_active_switch_confirm').confirmation('show');
          } else {
            $('#is_active_switch_confirm').confirmation({
              trigger: 'manual',
              btnOkClass: 'btn-xs btn-primary-original',
              btnCancelClass: 'btn-xs btn-default-original',
              singleton: true,
              placement: 'left',
              title: 'You are about to cancel membership (de-activate) of organisation: ' + aData.org_name + '.<br/>Are you sure?',
              onConfirm: function () {
                removeExistingConfirmation(false);
                $http.post(window.libraries.ws_urls.organisations.revoke(), { org_id: aData.id })
                .success(function (d, status, headers, config) {
                  if (d.revoked) {
                    $.gritter.add({
                      title: 'Organisation has been revoked from membership!',
                      text: 'You have successfully de-actived the membership of organisation ' + aData.org_name + '. An email has been sent to the organisation.'
                    });
                  }
                })
                .error(function (d, status, headers, config) {
                });
              },
              onCancel: function () {
                removeExistingConfirmation(true);
              }
            });
            $('#is_active_switch_confirm').confirmation('show');
          }
        }
      });
    }

    function showDataArea () {
      $('.list-area-loader').slideUp();
      $('.list-area').fadeTo(400, 1);
    }

    function setDataTable () {
      dt_table = $('#table_organisations').dataTable({
        "aaSorting": [],
        "bProcessing": true,
        "iDisplayLength": 100,
        "bServerSide": true,
        "sAjaxSource": window.libraries.ws_urls.organisations.getData(),
        "fnServerParams": function (aoData) {
          var membershipStatus = filters.membershipStatus;
          if (!$.isNumeric(membershipStatus)) {
            membershipStatus = -1;
          }
          var topStatus = filters.topStatus;
          if (!$.isNumeric(topStatus)) {
            topStatus = -1;
          }
          var orgType = filters.orgType;
          if (!$.isNumeric(orgType)) {
            orgType = 0;
          }
          aoData.push({
            "name": "membershipStatusFilter",
            "value": membershipStatus
          });
          aoData.push({
            "name": "topStatusFilter",
            "value": topStatus
          });
          aoData.push({
            "name": "orgTypeFilter",
            "value": orgType
          });
          var officeLocation = filters.officeLocation;
          if (!$.isNumeric(officeLocation)) {
            officeLocation = 0;
          }
          aoData.push({
            "name": "officeLocationFilter",
            "value": officeLocation
          });
        },
        "aoColumns": [
          { "mData": "id" },
          { "mData": "org_name" },
          { "mData": "related_organisation_type_name" },
          { "mData": "created_at" },
          { "mData": null },
          { "mData": null },
          { "mData": null },
        ],
        "aoColumnDefs": [
          { "bSearchable": false, "bSortable": true, "sClass": "", "aTargets": [0] },
          { "sClass": "title", "aTargets": [1] },
          { "bSearchable": false, "bSortable": false, "sClass": "", "aTargets": [2] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [4] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [5] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [6] }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
          var isActiveHtml = '';
          if (aData.is_active) {
            isActiveHtml += '<input type="checkbox" checked class="transform-switchable is-active-switch switch-mini">';
          } else {
            isActiveHtml += '<input type="checkbox" class="transform-switchable is-active-switch switch-mini">';
          }
          $('td:eq(4)', nRow).html(isActiveHtml);

          // var isTopHtml = '';
          // if (aData.top_agency) {
          //   isTopHtml = '<input type="checkbox" checked class="transform-switchable is-top-switch switch-mini">';
          // } else {
          //   isTopHtml = '<input type="checkbox" class="transform-switchable is-top-switch switch-mini">';
          // }
          // $('td:eq(5)', nRow).html(isTopHtml);
          var assignTopAgencyHtml = '<a href="#" class="assign-top-agency text-primary" data-toggle="modal" data-target="#assignTopAgencyModal"><i class="fa fa-check-square"></i> Manage</a>';
          $assignTopAgencyHtml = $(assignTopAgencyHtml).attr('data-org-id', aData.id).data('org', aData);
          $('td:eq(5)', nRow).html($assignTopAgencyHtml);

          //console.log(aData);
          var titleHtml = '';
          titleHtml += '<img width="48" height="auto" src="/imagecache/r_100_auto_c' + aData.org_logo + '">';
          if (aData.org_name) {
            titleHtml += '<span>' + aData.org_name + '</span>';
          } else {
            titleHtml += '<span class="label label-info">NEW ACCOUNT</span>';
            titleHtml += '<span class="label label-danger">PROFILE NOT COMPLETED</span>';
            $(nRow).addClass('new-account');
          }
          $('td:eq(1)', nRow).html($(titleHtml));

          var actionHtml = '<a href="' + window.libraries.ws_urls.organisations.preview(aData.id) + '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-search"></i></a>';
          actionHtml += ' <a href="#" class="btn btn-danger btn-xs" target="_blank"><i class="fa fa-times"></i></a>';
          $('td:eq(6)', nRow).html($(actionHtml));
        },
        "fnDrawCallback": function (settings) {
          setIsActiveSwitches();
          //setTopAgencySwitches();
        },
        "fnInitComplete": function () {
          showDataArea();
        }
      });
    }

    var platformsLoaded = false;
    var platforms = [];

    function loadPlatforms (orgId) {
      var deferred = $.Deferred();
      $http.get(window.libraries.ws_urls.common.getOrgAssignedPlatforms(orgId)).success(function (data, status, headers, config) {
        if (data.platforms) {
          platformsLoaded = true;
          platforms = data.platforms;
          deferred.resolve();
        } else {
          deferred.reject();
        }
      });
      return deferred.promise();
    }

    function assigned (platformId, platforms_having_top_status) {
      for (var i = 0; i < platforms_having_top_status.length; i++) {
        if (platformId === platforms_having_top_status[i].platform_id) {
          return true;
        }
      }
      return false;
    }

    function drawPlatforms (platforms_having_top_status, org) {
      if (platforms) {
        for (var i = 0; i < platforms.length; i++) {
          var platform = platforms[i];
          var c = '';
          if (assigned(platform.id, platforms_having_top_status)) {
            c = 'checked';
          }
          var h = '<li></li>';
          var $input = $('<input type="checkbox" class="transform-switchable is-top-switch switch-mini" ' + c + '/>');
          var $strong = $('<strong>' + platform.name + '</strong>');
          $('#assignTopAgencyModal .platform-list').append($(h).append($input).append($strong));
          $input.data('platform', platform);
          $input.data('org', org);
        }
      }
    }

    function getOrgPlatformsHavingTopStatus(orgId) {
      var deferred = $.Deferred();
      $http.get(window.libraries.ws_urls.common.getOrgPlatformsHavingTopStatus(orgId)).success(function (data, status, headers, config) {
        if (data.platforms_having_top_status) {
          deferred.resolve(data.platforms_having_top_status);
        } else {
          deferred.reject();
        }
      });
      return deferred.promise();
    }

    $('#assignTopAgencyModal').on('show.bs.modal', function (e) {
      var anchor = e.relatedTarget;
      var orgId = parseInt($(anchor).attr('data-org-id'), 10);
      var org = $(anchor).data('org');
      $('#assignTopAgencyModal .modal-title').html('');
      $('#assignTopAgencyModal .modal-title').html('Top Agency status for organisation: <br/> <strong>' + org.org_name + '</strong>');
      $('#assignTopAgencyModal .platform-list').empty();
      loadPlatforms(orgId).always(function () {
        if (platformsLoaded) {
          $('#assignTopAgencyModal .loader').show();
          getOrgPlatformsHavingTopStatus(orgId).done(function (platforms_having_top_status) {
            drawPlatforms(platforms_having_top_status, org);
            setTopAgencySwitches();
          }).always(function () {
            $('#assignTopAgencyModal .loader').hide();
          });
          // reset
          platformsLoaded = false;
        }
      });
    });

    $('#assignTopAgencyModal').on('hide.bs.modal', function (e) {
      
    });

    service.onDocumentReady = function () {
      setDataTable();
    };

    return service;
  }
]);