/**
 * Created by westilian on 1/19/14.
 */

(function(){
    var t;
    function size(animate){
        if (animate == undefined){
            animate = false;
        }
        clearTimeout(t);
        t = setTimeout(function(){
            $("canvas").each(function(i,el){
                $(el).attr({
                    "width":$(el).parent().width(),
                    "height":$(el).parent().outerHeight()
                });
            });
            redraw(animate);
            var m = 0;
            $(".chartJS").height("");
            $(".chartJS").each(function(i,el){ m = Math.max(m,$(el).height()); });
            $(".chartJS").height(260);
        }, 30);
    }
    $(window).on('resize', function(){ size(false); });


    function redraw(animation){
        var options = {};
        if (!animation){
            options.animation = false;
        } else {
            options.animation = true;
        }


        //var barChartData = {
        //    labels : ["January","February","March","April","May","June","July"],
        //    datasets : [
        //        {
        //            fillColor : "#E67A77",
        //            strokeColor : "#E67A77",
        //            data : [65,59,90,81,56,55,40]
        //        },
        //        {
        //            fillColor : "#79D1CF",
        //            strokeColor : "#79D1CF",
        //            data : [28,48,40,19,96,27,100]
        //        }
        //    ]
        //}

        //var myLine = new Chart(document.getElementById("bar-chart-js").getContext("2d")).Bar(barChartData);


        var Linedata = {
            labels : ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct"],
            datasets : [
                {
                    fillColor : "rgba(230,122,119,.6)",
                    strokeColor : "rgba(230,122,119,0)",
                    pointColor : "rgba(230,122,119,.6)",
                    pointStrokeColor : "#fff",
                    data : [10,1778,4912,3767,6810,5670,4820,25073,10687,1000]
                },
                {
                    fillColor : "rgba(217,221,129,.6)",
                    strokeColor : "rgba(217,221,129,0)",
                    pointColor : "rgba(217,221,129,.6)",
                    pointStrokeColor : "#fff",
                    data : [10,7294,12969,3597,1914,4293,3795,5967,34460,5713]
                },
                {   fillColor : "rgba(121,209,207,.6)",
                    strokeColor : "rgba(121,209,207,0)",
                    pointColor : "rgba(121,209,207,.6)",
                    pointStrokeColor : "#fff",
                    data : [10,18441,3501,5689,2293,1881,1588,5175,22028,1791]
                }

            ]
        }

        var lineChartArea = document.getElementById("line-chart-js");
        if (lineChartArea) {
          var myLineChart = new Chart(lineChartArea.getContext("2d")).Line(Linedata);
        }


        //var pieData = [
        //    {
        //        value: 30,
        //        color:"#E67A77"
        //    },
        //    {
        //        value : 50,
        //        color : "#D9DD81"
        //    },
        //    {
        //        value : 100,
        //        color : "#79D1CF"
        //    }
        //];
       // var myPie = new Chart(document.getElementById("pie-chart-js").getContext("2d")).Pie(pieData);
       // var donutData = [
       //     {
       //         value: 30,
       //         color:"#E67A77"
       //     },
       //     {
       //         value : 50,
       //         color : "#D9DD81"
       //     },
       //     {
       //         value : 100,
       //         color : "#79D1CF"
       //     },
       //     {
       //         value : 40,
       //         color : "#95D7BB"
       //     },
       //     {
       //         value : 120,
       //         color : "#4D5360"
       //     }
       // ]
       // var myDonut = new Chart(document.getElementById("donut-chart-js").getContext("2d")).Doughnut(donutData);
    }




    size(true);

}());
