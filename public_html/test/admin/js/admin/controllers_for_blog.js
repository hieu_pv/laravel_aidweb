fleavaAdminController.controller('BlogCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'blogService',
  function ($rootScope, $scope, $http, blogService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.blogService = blogService;
    $scope.selectedPublishedFilter = -1;
    $scope.selectedAuthor = {};
    $(blogService).on('options_authors_changed', function () {
      $scope.selectedAuthor = blogService.options.authors[0];
      $scope.$apply();
    });
    $scope.filterChanged = function () {
      blogService.filters.author = $scope.selectedAuthor.id;
      blogService.filters.published = $scope.selectedPublishedFilter;
      $(blogService).trigger('filter_changed');
    };
    angular.element(document).ready(function () {
      blogService.onDocumentReady();
      $scope.safeApply();
    });
  }
]);


fleavaAdminController.controller('BlogPostCreateUpdateCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'blogService',
  function ($rootScope, $scope, $http, blogService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    angular.element(document).ready(function () {
      blogService.createupdate.onDocumentReady();
      $scope.safeApply();
    });
  }
]);