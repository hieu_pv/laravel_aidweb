/*jshint multistr: true */
fleavaAdminController.controller('ManagePlatformsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  function ($rootScope, $scope, $http) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.getCountries = function () {
      $http.get(window.libraries.ws_urls.common.getCountriesForPlatform()).success(function(data, status, headers, config) {
        $scope.countries = data.countries;
        $scope.safeApply();
      }).error(function(data, status, headers, config) {
      });
    };
    $scope.selected_country = 0;
    $scope.createPlatform = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      if ($scope.selected_country) {
        var l = Ladda.create($event.currentTarget);
        l.start();
        jQuery.post(window.libraries.ws_urls.common.createPlatform(), { selected_country: $scope.selected_country }, function (response, text, jqXhr) {
          if (response.ok) {
            window.location.reload();
          } else {
            l.stop();
          }
        }).fail(function () {
          l.stop();
        }).always(function () {
          
        });
      }
    };
    angular.element(document).ready(function () {
      $scope.getCountries();
    });
  }
]);