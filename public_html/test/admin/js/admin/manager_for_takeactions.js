// let fleavaAdminServices throw undefined if not found
fleavaAdminServices.factory('takeActionsService', ['$http',
  function ($http) {
    var service = {};
    var dt_table = null;

    var filters = {
      author: 0,
      published: -1,
      type: 0
    };
    service.filters = filters;

    var options = {
      types: [],
      authors: []
    };
    service.options = options;

    $(service).on('filter_changed', function () {
      redrawDataTable();
    });

    function setTypes () {
      $http.get(window.libraries.ws_urls.takeactions.types()).success(function (data) {
        data.types.splice(0, 0, { id: 0, name: "-- All" });
        options.types = data.types;
        $(service).trigger('options_types_changed');
      });
    }
    service.setTypes = setTypes;

    function setAuthors () {
      $http.get(window.libraries.ws_urls.takeactions.authors()).success(function (data) {
        data.authors.splice(0, 0, { id: 0, org_name: "-- All" });
        options.authors = data.authors;
        $(service).trigger('options_authors_changed');
      });
    }
    service.setAuthors = setAuthors;

    function showDeletedMessage (title) {
      $.gritter.add({
        title: 'Get Involved Deleted!',
        text: 'You have successfully deleted Get Involved: <br/>' + title,
        sticky: false,
        time: ''
      });
    }

    function setupDeleteConfirmation () {
      $('.btnDestroyTakeAction').confirmation({
        trigger: 'click',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        onConfirm: function () {
          var e = this;
          if (dt_table) {
            var pos = dt_table.fnGetPosition($(e).parents('tr').get(0));
            var data = dt_table.fnGetData($(e).parents('tr').get(0));
            $.post(window.libraries.ws_urls.takeactions.destroy(data.id), function (d) {
              if (d.deleted) {
                $(e).parents('tr').fadeOut(500, function  () {
                  dt_table.fnDeleteRow(pos);
                });
                showDeletedMessage(data.title);
              }
            });
          }
        }
      });
    }

    function setPublishedSwitches () {
      $('.transform-switchable').bootstrapSwitch();
      $('.transform-switchable').bootstrapSwitch('setOnLabel', 'Yes');
      $('.transform-switchable').bootstrapSwitch('setOffLabel', 'No');
      $('.transform-switchable').bootstrapSwitch('setOnClass', 'success');
      $('.transform-switchable').bootstrapSwitch('setOffClass', 'danger');
      $('.transform-switchable').on('switch-change', function(e, data) {
        if (dt_table) {
          var pos = dt_table.fnGetPosition($(this).parents('tr').get(0));
          var tbData = dt_table.fnGetData($(this).parents('tr').get(0));
          if (data.value) {
            $.post(window.libraries.ws_urls.takeactions.publish(tbData.id), function (ping) {
              if (ping.published) {
                $.gritter.add({
                  title: 'Get Involved Published!',
                  text: 'You have successfully published Get Involved: ' + tbData.title
                });
              } else {
                $.gritter.add({
                  title: 'Cannot Publish Get Involved!',
                  text: 'Something is not right. Please try again.'
                });
              }
            });
          } else {
            $.post(window.libraries.ws_urls.takeactions.unpublish(tbData.id), function (ping) {
              if (ping.unpublished) {
                $.gritter.add({
                  title: 'Get Involved Un-Published!',
                  text: 'You have successfully un-published Get Involved: ' + tbData.title
                });
              } else {
                $.gritter.add({
                  title: 'Cannot Un-Publish Get Involved!',
                  text: 'Something is not right. Please try again.'
                });
              }
            });
          }
        }
      });
    }

    var loadstate = function () {
      var datatable = false;
      var o = {};
      o.setDataTableState = function (value) {
        datatable = value;
        $(o).trigger('state_changed');
      };
      o.getDataTableState = function () {
        return datatable;
      };
      o.loaded = function () {
        return datatable;
      };
      return o;
    } ();

    $(loadstate).on('state_changed', function () {
      if (loadstate.loaded()) {
        showDataArea();
      }
    });

    function showDataArea () {
      $('.list-area-loader').slideUp();
      $('.list-area').fadeTo(400, 1);
    }

    function setDataTable () {
      dt_table = $('#table_takeactions').dataTable({
        "aaSorting": [],
        "bProcessing": true,
        "iDisplayLength": 10,
        "bServerSide": true,
        "sAjaxSource": window.libraries.ws_urls.takeactions.getData(),
        "fnServerParams": function (aoData) {
          var published = filters.published;
          if (!$.isNumeric(published)) {
            published = -1;
          }
          var author = filters.author;
          if (!$.isNumeric(author)) {
            author = 0;
          }
          var type = filters.type;
          if (!$.isNumeric(type)) {
            type = 0;
          }
          aoData.push({
            "name": "publishedFilter",
            "value": published
          });
          aoData.push({
            "name": "authorFilter",
            "value": author
          });
          aoData.push({
            "name": "typeFilter",
            "value": type
          });
        },
        "aoColumns": [
          { "mData": "id" },
          { "mData": "title" },
          { "mData": "author" },
          { "mData": "type_of_take_action.name" },
          { "mData": "created_at" },
          { "mData": "updated_at" },
          { "mData": "published" },
          { "mData": null }
        ],
        "aoColumnDefs": [
          { "sClass": "title", "aTargets": [1] },
          { "bSearchable": false, "bSortable": false, "aTargets": [2] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-left", "aTargets": [3] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [6] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [7] }
        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          var delMsg = 'Remove Get Involved: <br/> ' + aData.title + '.<br/>Are you sure?';
          var actionHtml = '';
          if (window._ouser.current.isOrganisation) {
            actionHtml += '<a href="' + window.libraries.ws_urls.takeactions.edit(aData.id) + '" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>&nbsp;';
          }
          if (window._ouser.current.isAdmin) {
            actionHtml += '<a href="' + window.libraries.ws_urls.takeactions.preview(aData.id) + '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-search"></i></a>&nbsp;';
          }
          var delAttr = ' data-title="' + delMsg + '" data-placement="left" data-singleton="true" data-popout="false" ';
          actionHtml += '<a href="#!" class="btn btn-danger btn-xs btnDestroyTakeAction"' + delAttr + '><i class="fa fa-times"></i></a>';
          $('td:eq(7)', nRow).append(actionHtml);

          var publishedHtml = '';
          var publishedAttr = '';
          if (window._ouser.current.isOrganisation) {
            publishedAttr = 'read-only disabled';
          }
          if (aData.published) {
            publishedHtml = '<input type="checkbox" checked class="transform-switchable switch-mini" ' + publishedAttr + '>';
          } else {
            publishedHtml = '<input type="checkbox" class="transform-switchable switch-mini" ' + publishedAttr + '>';
          }
          $('td:eq(6)', nRow).html(publishedHtml);

          // title
          var titleHtml = '';
          titleHtml += '<img width="48" height="auto" src="/imagecache/r_100_auto_c' + aData.image + '">';
          titleHtml += '<span>' + aData.title + '</span>';
          $('td:eq(1)', nRow).html($(titleHtml));

          // type
          var typeHtml = $('<span class=""></span>');
          $(typeHtml).text(aData.type_of_take_action.name);
          $(typeHtml).css('color', aData.type_of_take_action.color);
          $(typeHtml).css('font-weight', 'bold');
          $('td:eq(3)', nRow).html(typeHtml);
        },
        "fnDrawCallback": function (settings) {
          $('ins.dark-tooltip').remove();
          setupDeleteConfirmation();
          setPublishedSwitches();
          if (window._ouser.current.isOrganisation) {
            $('#table_takeactions tr td:nth-child(3)').css('display', 'none');
            $('#table_takeactions tr th:nth-child(3)').css('display', 'none');
          }
        },
        "fnInitComplete": function () {
          loadstate.setDataTableState(true);
        }
      });
    }

    function redrawDataTable () {
      dt_table.fnDraw();
    }

    service.onDocumentReady = function () {
      setDataTable();
    };


    service.createupdate = {};
    service.createupdate.title = {
      get: function () {
        return $('#title').val();
      },
      set: function (value) {
        $('#title').val(value);
      }
    };
    service.createupdate.slug = {
      get: function () {
        return $('#slug').val();
      },
      set: function (value) {
        $('#slug').val(value);
      }
    };
    service.createupdate.onDocumentReady = function () {
      $(document).on('blur', '#title', function (e) {
        e.preventDefault();
        $('.btnGenerateSlug').trigger('click');
      });
      $(document).on('click', '.btnGenerateSlug', function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.find('.fa-refresh').addClass('fa-spin');
        $this.find('.fa-refresh').prev().remove();
        $this.find('.fa-refresh').before('<span>generating..&nbsp;&nbsp;&nbsp;</span>');
        $this.attr('disabled', 'disabled');
        $this.prop('disabled', true);
        $.get(window.libraries.ws_urls.common.getSlug(service.createupdate.title.get()), function (data) {
          if (data) {
            if (data.slug) {
              service.createupdate.slug.set(data.slug);
            }
          }
        }).always(function () {
          $this.find('.fa-refresh').removeClass('fa-spin');
          $this.find('.fa-refresh').prev().remove();
          $this.removeAttr('disabled');
          $this.prop('disabled', false);
        });
      });

      $('#yes_featured, #no_featured').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    };

    return service;
  }
]);
