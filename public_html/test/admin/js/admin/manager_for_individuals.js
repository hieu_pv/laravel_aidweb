// let fleavaAdminServices throw undefined if not found
fleavaAdminServices.factory('individualsService', ['$http',
  function ($http) {
    var service = {};
    var dt_table = null;

    var filters = {
      topStatus: -1,
      membershipStatus: -1,
      nationality: 0,
      memberStatus: 0,
      professionalStatus: 0,
      basedIn: 0,
    };
    service.filters = filters;

    var options = {
      nationalities: [],
      memberStatuses: [],
      professionalStatuses: [],
      bases: []
    };
    service.options = options;

    $(service).on('filter_changed', function () {
      dt_table.fnDraw();
    });

    service.setNationalities = function () {
      $http.get(window.libraries.ws_urls.common.getNationalities()).success(function (data, status, headers, config) {
        data.nationalities.splice(0, 0, { id: 0, name: "-- All" });
        options.nationalities = data.nationalities;
        $(service).trigger('options_nationalities_changed');
      });
    };

    service.setMemberStatuses = function () {
      $http.get(window.libraries.ws_urls.common.getMemberStatuses()).success(function (data, status, headers, config) {
        data.memberStatuses.splice(0, 0, { id: 0, name: "-- All" });
        options.memberStatuses = data.memberStatuses;
        $(service).trigger('options_memberstatuses_changed');
      });
    };

    service.setProfessionalStatuses = function () {
      $http.get(window.libraries.ws_urls.common.getProfessionalStatuses()).success(function (data, status, headers, config) {
        data.professionalStatuses.splice(0, 0, { id: 0, name: "-- All" });
        options.professionalStatuses = data.professionalStatuses;
        $(service).trigger('options_professionalstatuses_changed');
      });
    };

    service.setBases = function () {
      $http.get(window.libraries.ws_urls.common.getCountries()).success(function (data, status, headers, config) {
        data.countries.splice(0, 0, { id: 0, name: "-- All" });
        options.bases = data.countries;
        $(service).trigger('options_bases_changed');
      });
    };

    function setTopMemberSwitches() {
      $('.is-top-switch').bootstrapSwitch();
      $('.is-top-switch').bootstrapSwitch('setOnLabel', 'Yes');
      $('.is-top-switch').bootstrapSwitch('setOffLabel', 'No');
      $('.is-top-switch').bootstrapSwitch('setOnClass', 'success');
      $('.is-top-switch').bootstrapSwitch('setOffClass', 'danger');
      $('.is-top-switch').on('switch-change', function (e, data) {
        var platform = $(e.target).data('platform');
        var ind = $(e.target).data('ind');
        var jd = { platformId: platform.id, indId: ind.id };
        if (data.value) {
          $http.post(window.libraries.ws_urls.common.assignIndToPlatform(), jd).success(function (response, status, headers, config) {
            if (response.ok) {
              $.gritter.add({
                title: 'Individual set as Top Member',
                text: 'You have successfully set ' + ind.full_name + ' as Top Member in platform ' + platform.name + '. An email has been sent to the individual.'
              });
            }
          }).error(function (response, status, headers, config) {
          });
        } else {
          $http.post(window.libraries.ws_urls.common.unassignIndFromPlatform(), jd).success(function (response, status, headers, config) {
            if (response.ok) {
              $.gritter.add({
                title: 'Individual unset from Top Member',
                text: 'You have successfully unset ' + ind.full_name + ' from Top Member in platform ' + platform.name + '. An email has been sent to the individual.'
              });
            }
          }).error(function (response, status, headers, config) {
          });
        }
      });
    }

    var removeExistingConfirmation = function (toggleSwitch) {
      if ($('#is_active_switch_confirm').length) {
        $('#is_active_switch_confirm').confirmation('destroy');
        if (toggleSwitch) {
          $('#is_active_switch_confirm').closest('td').find('.is-active-switch').bootstrapSwitch('toggleState', true);
        }
        $('#is_active_switch_confirm').remove();
      }
    };

    function setIsActiveSwitches() {
      $('.is-active-switch').bootstrapSwitch();
      $('.is-active-switch').bootstrapSwitch('setOnLabel', 'Yes');
      $('.is-active-switch').bootstrapSwitch('setOffLabel', 'No');
      $('.is-active-switch').bootstrapSwitch('setOnClass', 'success');
      $('.is-active-switch').bootstrapSwitch('setOffClass', 'danger');
      $('.is-active-switch').on('switch-change', function (e, data) {
        var dis = this;
        if (dt_table) {
          var pos = dt_table.fnGetPosition($(this).parents('tr').get(0));
          var aData = dt_table.fnGetData($(this).parents('tr').get(0));

          // e. does not work, use the GLOBAL "event" object to stop the click
          event.preventDefault(); //e.preventDefault();
          event.stopPropagation(); //e.stopPropagation();

          // remove existing confirmation box
          removeExistingConfirmation(true);

          var rmaHtml = '<a href="#" id="is_active_switch_confirm" class="is-active-switch-confirm">&nbsp;</a>';
          $(dis).closest('td').prepend(rmaHtml);

          if (data.value) {
            $('#is_active_switch_confirm').confirmation({
              trigger: 'manual',
              btnOkClass: 'btn-xs btn-primary-original',
              btnCancelClass: 'btn-xs btn-default-original',
              singleton: true,
              placement: 'left',
              title: 'You are about to activate membership of person: ' + aData.full_name + '.<br/>Are you sure?',
              onConfirm: function () {
                removeExistingConfirmation(false);
                $http.post(window.libraries.ws_urls.individuals.activate(), { ind_id: aData.id })
                .success(function (d, status, headers, config) {
                  if (d.activated) {
                    $.gritter.add({
                      title: 'Individual has been activated!',
                      text: 'You have successfully actived the membership of person ' + aData.full_name + '. An email has been sent to the person.'
                    });
                  }
                })
                .error(function (d, status, headers, config) {
                });
              },
              onCancel: function () {
                removeExistingConfirmation(true);
              }
            });
            $('#is_active_switch_confirm').confirmation('show');
          } else {
            $('#is_active_switch_confirm').confirmation({
              trigger: 'manual',
              btnOkClass: 'btn-xs btn-primary-original',
              btnCancelClass: 'btn-xs btn-default-original',
              singleton: true,
              placement: 'left',
              title: 'You are about to cancel membership (de-activate) of person: ' + aData.full_name + '.<br/>Are you sure?',
              onConfirm: function () {
                removeExistingConfirmation(false);
                $http.post(window.libraries.ws_urls.individuals.revoke(), { ind_id: aData.id })
                .success(function (d, status, headers, config) {
                  if (d.revoked) {
                    $.gritter.add({
                      title: 'Individual has been revoked from membership!',
                      text: 'You have successfully de-actived the membership of person ' + aData.full_name + '. An email has been sent to the person.'
                    });
                  }
                })
                .error(function (d, status, headers, config) {
                });
              },
              onCancel: function () {
                removeExistingConfirmation(true);
              }
            });
            $('#is_active_switch_confirm').confirmation('show');
          }
        }
      });
    }

    function showDataArea () {
      $('.list-area-loader').slideUp();
      $('.list-area').fadeTo(400, 1);
    }

    function setDataTable () {
      dt_table = $('#table_individuals').dataTable({
        "aaSorting": [],
        "bProcessing": true,
        "iDisplayLength": 100,
        "bServerSide": true,
        "sAjaxSource": window.libraries.ws_urls.individuals.getData(),
        "fnServerParams": function (aoData) {
          var membershipStatus = filters.membershipStatus;
          if (!$.isNumeric(membershipStatus)) {
            membershipStatus = -1;
          }
          var topStatus = filters.topStatus;
          if (!$.isNumeric(topStatus)) {
            topStatus = -1;
          }
          aoData.push({
            "name": "membershipStatusFilter",
            "value": membershipStatus
          });
          aoData.push({
            "name": "topStatusFilter",
            "value": topStatus
          });
          var nationality = filters.nationality;
          if (!$.isNumeric(nationality)) {
            nationality = 0;
          }
          aoData.push({
            "name": "nationalityFilter",
            "value": nationality
          });
          var memberStatus = filters.memberStatus;
          if (!$.isNumeric(memberStatus)) {
            memberStatus = 0;
          }
          aoData.push({
            "name": "memberStatusFilter",
            "value": memberStatus
          });
          var professionalStatus = filters.professionalStatus;
          if (!$.isNumeric(professionalStatus)) {
            professionalStatus = 0;
          }
          aoData.push({
            "name": "professionalStatusFilter",
            "value": professionalStatus
          });
          var basedIn = filters.basedIn;
          if (!$.isNumeric(basedIn)) {
            basedIn = 0;
          }
          aoData.push({
            "name": "basedInFilter",
            "value": basedIn
          });
        },
        "aoColumns": [
          { "mData": "id" },
          { "mData": "full_name" },
          { "mData": "created_at" },
          { "mData": null },
          { "mData": null },
          { "mData": null },
        ],
        "aoColumnDefs": [
          { "bSearchable": false, "bSortable": true, "sClass": "", "aTargets": [0] },
          { "sClass": "title", "aTargets": [1] },
          { "bSearchable": false, "bSortable": false, "sClass": "", "aTargets": [2] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [3] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [4] },
          { "bSearchable": false, "bSortable": false, "sClass": "text-center", "aTargets": [5] }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
          var isActiveHtml = '';
          if (aData.is_active) {
            isActiveHtml += '<input type="checkbox" checked class="transform-switchable is-active-switch switch-mini">';
          } else {
            isActiveHtml += '<input type="checkbox" class="transform-switchable is-active-switch switch-mini">';
          }
          $('td:eq(3)', nRow).html(isActiveHtml);

          // var isTopHtml = '';
          // if (aData.top_member) {
          //   isTopHtml = '<input type="checkbox" checked class="transform-switchable is-top-switch switch-mini">';
          // } else {
          //   isTopHtml = '<input type="checkbox" class="transform-switchable is-top-switch switch-mini">';
          // }
          // $('td:eq(4)', nRow).html(isTopHtml);
          var assignTopHtml = '<a href="#" class="assign-top-member text-primary" data-toggle="modal" data-target="#assignTopMemberModal"><i class="fa fa-check-square"></i> Manage</a>';
          $assignTopHtml = $(assignTopHtml).attr('data-ind-id', aData.id).data('ind', aData);
          $('td:eq(4)', nRow).html($assignTopHtml);

          var titleHtml = '';
          titleHtml += '<img width="48" height="auto" src="/imagecache/r_100_auto_c' + aData.avatar + '">';
          if (aData.full_name) {
            titleHtml += '<span>' + aData.full_name + '</span>';
          } else {
            titleHtml += '<span class="label label-info">NEW ACCOUNT</span>';
            titleHtml += '<span class="label label-danger">PROFILE NOT COMPLETED</span>';
            $(nRow).addClass('new-account');
          }
          $('td:eq(1)', nRow).html($(titleHtml));

          //console.log(aData);
          var actionHtml = '<a href="' + window.libraries.ws_urls.individuals.preview(aData.id) + '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-search"></i></a>';
          //console.log(actionHtml);
          $('td:eq(5)', nRow).html($(actionHtml));
        },
        "fnDrawCallback": function (settings) {
          setIsActiveSwitches();
          //setTopMemberSwitches();
        },
        "fnInitComplete": function () {
          showDataArea();
        }
      });
    }

    var platformsLoaded = false;
    var platforms = [];

    function loadPlatforms () {
      $http.get(window.libraries.ws_urls.common.getAllPlatforms()).success(function (data, status, headers, config) {
        if (data.platforms) {
          platformsLoaded = true;
          platforms = data.platforms;
        }
      });
    }

    function assigned (platformId, assigned_platforms) {
      for (var i = 0; i < assigned_platforms.length; i++) {
        if (platformId === assigned_platforms[i].platform_id) {
          return true;
        }
      }
      return false;
    }

    function drawPlatforms (assigned_platforms, ind) {
      if (platforms) {
        for (var i = 0; i < platforms.length; i++) {
          var platform = platforms[i];
          var c = '';
          if (assigned(platform.id, assigned_platforms)) {
            c = 'checked';
          }
          var h = '<li></li>';
          var $input = $('<input type="checkbox" class="transform-switchable is-top-switch switch-mini" ' + c + '/>');
          var $strong = $('<strong>' + platform.name + '</strong>');
          $('#assignTopMemberModal .platform-list').append($(h).append($input).append($strong));
          $input.data('platform', platform);
          $input.data('ind', ind);
        }
      }
    }

    function getAssignedPlatforms(indId) {
      var deferred = $.Deferred();
      $http.get(window.libraries.ws_urls.common.getIndAssignedPlatforms(indId)).success(function (data, status, headers, config) {
        if (data.assigned_platforms) {
          deferred.resolve(data.assigned_platforms);
        } else {
          deferred.reject();
        }
      });
      return deferred.promise();
    }

    $('#assignTopMemberModal').on('show.bs.modal', function (e) {
      var anchor = e.relatedTarget;
      var indId = parseInt($(anchor).attr('data-ind-id'), 10);
      var ind = $(anchor).data('ind');
      $('#assignTopMemberModal .modal-title').html('');
      $('#assignTopMemberModal .modal-title').html('Top Member status for individual: <br/> <strong>' + ind.full_name + '</strong>');
      if (platformsLoaded) {
        $('#assignTopMemberModal .loader').show();
        $('#assignTopMemberModal .platform-list').empty();
        getAssignedPlatforms(indId).done(function (assigned_platforms) {
          drawPlatforms(assigned_platforms, ind);
          setTopMemberSwitches();
        }).always(function () {
          $('#assignTopMemberModal .loader').hide();
        });
      }
    });

    service.onDocumentReady = function () {
      setDataTable();
      loadPlatforms();
    };

    return service;
  }
]);