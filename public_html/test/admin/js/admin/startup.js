(function ($) {
  var setImgCss = function (elem3) {
    var imgW = $(elem3).width();
    var imgH = $(elem3).height();
    if (imgW > imgH) {
      $(elem3).css('width', '200px').css('height', 'auto');
    } else {
      $(elem3).css('height', '200px').css('width', 'auto');
    }
  };

  var forNewInterval = null;
  var forPreviewInterval = null;
  var forNewIntervalCount = 0;
  var forPreviewIntervalCount = 0;

  var clearPrevIntervals = function () {
    window.clearInterval(forNewInterval);
    window.clearInterval(forPreviewInterval);
  };

  var forNew = function (elem) {
    //console.log('forNew');
    var imgs1 = $(elem).find('.fileupload-new img');
    if (imgs1.length > 0 || forNewIntervalCount > 10) {
      window.clearInterval(forNewInterval);
    }
    $(imgs1).each(function (idx3, elem3) {
      setImgCss(elem3);
    });
    forNewIntervalCount++;
  };

  var forPreview = function (elem) {
    var imgs2 = $(elem).find('.fileupload-preview img');
    if (imgs2.length > 0 || forPreviewIntervalCount > 10) {
      window.clearInterval(forPreviewInterval);
    }
    $(imgs2).each(function (idx3, elem3) {
      setImgCss(elem3);
    });
    forPreviewIntervalCount++;
  };

  var makeSureSquareImg1 = function (elem) {
    forNewInterval = window.setInterval(function () { forNew(elem); }, 100);
  };

  var makeSureSquareImg2 = function (elem) {
    forPreviewInterval = window.setInterval(function () { forPreview(elem); }, 100);
  };

  $(function () {

    $('.upload-avatar .fileupload').each(function (idx, elem) {
      var containers = $(elem).find('.fileupload-new, .fileupload-preview');
      $(containers).each(function (idx2, elem2) {
        $(elem2).css('width', '210px').css('height', '210px');
      });

      $(elem).find(':file').on('change', function (e, mode) {
        clearPrevIntervals();
        if (mode === 'clear') {
          makeSureSquareImg1(elem);
        } else {
          makeSureSquareImg1(elem);
          makeSureSquareImg2(elem);
        }
      });

      makeSureSquareImg1(elem);
      makeSureSquareImg2(elem);
    });

    // $('#sidebar').affix({
    //   offset: {
    //     top: 80,
    //     // bottom: function () {
    //     //   return (this.bottom = $('.footer').outerHeight(true))
    //     // }
    //   }
    // });

  });
} (jQuery));
