fleavaAdminController.controller('ChangePasswordCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  function ($rootScope, $scope, $http) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.cp_clean = {
      oldPassword: '',
      newPassword: '',
      newPasswordConfirmation: '',
      confirmed: function () {
        return this.newPasswordConfirmation === this.newPassword;
      }
    };
    $scope.cp = {
      oldPassword: '',
      newPassword: '',
      newPasswordConfirmation: '',
      confirmed: function () {
        return this.newPasswordConfirmation === this.newPassword;
      }
    };
    $scope.reset = function (form) {
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      } else if ($scope.form) {
        $scope.form.$setPristine();
        $scope.form.$setUntouched();
      }
      $scope.cp = angular.copy($scope.cp_clean);
    };
    $scope.changed = false;
    $scope.custom_messages = [];
    $scope.changePassword = function () {
      if ($scope.form.$valid) {
        if ($scope.cp) {
          if ($scope.cp.confirmed()) {
            var toPost = {
              old_password: $scope.cp.oldPassword,
              new_password: $scope.cp.newPassword,
              new_password_confirmation: $scope.cp.newPasswordConfirmation
            };
            var l = Ladda.create(document.querySelector('.submit-change-password'));
            l.start();
            $http.post(window.libraries.ws_urls.common.changePassword(), toPost)
            .success(function (data, status, headers, config) {
              l.stop();
              $scope.custom_messages = [];
              if (data.changed) {
                $scope.changed = true;
                $scope.reset();
              } else {
                for (var key in data.messages) {
                  $scope.custom_messages.push(data.messages[key]);
                }
              }
            })
            .error(function (data, status, headers, config) {
              l.stop();
              $scope.custom_messages = ['Cannot change password due to network issues or server issues. Please refresh the page and try again.'];
            });
          }
        }
      }
    };
    $rootScope.$on('changePasswordModal.show.bs.modal', function () {
      $scope.reset();
      $scope.custom_messages = [];
    });
  }
]);