fleavaAdminController.controller('PublicitiesCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'publicitiesService',
  function ($rootScope, $scope, $http, publicitiesService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.publicitiesService = publicitiesService;
    $scope.selectedPublishedFilter = -1;
    $scope.selectedType = {};
    $(publicitiesService).on('options_types_changed', function () {
      $scope.selectedType = publicitiesService.options.types[0];
      $scope.safeApply();
    });
    $scope.selectedAuthor = {};
    $(publicitiesService).on('options_authors_changed', function () {
      $scope.selectedAuthor = publicitiesService.options.authors[0];
      $scope.safeApply();
    });
    $scope.filterChanged = function () {
      publicitiesService.filters.author = $scope.selectedAuthor.id;
      publicitiesService.filters.type = $scope.selectedType.id;
      publicitiesService.filters.published = $scope.selectedPublishedFilter;
      $(publicitiesService).trigger('filter_changed');
    };
    angular.element(document).ready(function () {
      publicitiesService.onDocumentReady();
      publicitiesService.setTypes();
      publicitiesService.setAuthors();
      $scope.safeApply();
    });
  }
]);

fleavaAdminController.controller('PublicityCreateUpdateCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  'publicitiesService',
  function ($rootScope, $scope, $http, publicitiesService) {
    // REFACTOR THIS TO BASE CONTROLLER
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.selectedType = 0;
    $scope.selectedTitleColor = '#ffffff';
    $scope.selectedTextColor = '#ffffff';
    $scope.selectedButtonTextColor = '#ffffff';
    $scope.selectedPosition = 'LeftTop';
    $scope.showOrgLogo = '1';
    $scope.typeChanged = function () {
      publicitiesService.createupdate.selectedType = $scope.selectedType;
      $(publicitiesService.createupdate).trigger('changed.pub_type');
    };
    angular.element(document).ready(function () {
      $scope.selectedType = window._selected_publicity_type;
      //$scope.types = window._types_of_publicities || {};
      $scope.selectedTitleColor = window._selected_title_color;
      $scope.selectedTextColor = window._selected_text_color;
      $scope.selectedButtonTextColor = window._selected_button_text_color;
      $scope.selectedPosition = window._selected_position;
      $scope.showOrgLogo = window._show_org_logo.toString();
      $scope.safeApply();
      publicitiesService.createupdate.onDocumentReady();
      $scope.typeChanged();
    });
  }
]);
