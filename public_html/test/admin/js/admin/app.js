var fleavaAdminApp = angular.module('fleavaAdminApp', ['fleavaAdminController', 'fleavaAdminServices'])
  .config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
  }]);


fleavaAdminApp.directive('integer', function () {
  return {
    require: 'ngModel',
    link: function (scope, ele, attr, ctrl) {
      ctrl.$parsers.unshift(function (viewValue) {
        return parseInt(viewValue, 10);
      });
    }
  };
});

fleavaAdminApp.directive('compareTo', function () {
  return {
    require: 'ngModel',
    scope: {
      otherModelValue: '=compareTo'
    },
    link: function(scope, element, attributes, ngModel) {
      ngModel.$validators.compareTo = function (modelValue) {
        // if empty, fall to required validator
        if (modelValue) {
          return modelValue === scope.otherModelValue;
        }
        return true;
      };
      scope.$watch('otherModelValue', function () {
        ngModel.$validate();
      });
    }
  };
});

var fleavaAdminController = angular.module('fleavaAdminController', []);

var fleavaAdminServices = angular.module('fleavaAdminServices', []);


