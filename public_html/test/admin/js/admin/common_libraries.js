(function () {
  window.libraries.showImagePreview = function ($fileElement, $previewElement) {
    var felem = $($fileElement).get(0);
    var fileReader = new FileReader();
    fileReader.readAsDataURL(felem.files[0]);

    fileReader.onload = function (fileReaderEvent) {
      //console.log(fileReaderEvent);
      $($previewElement).get(0).src = fileReaderEvent.target.result;
    };
  };

  window.libraries.ws_urls = {
    posts: {
      destroy: function (id) {
        return '/oadmin/posts/' + id + '/destroy';
      },
      publish: function (id) {
        return '/oadmin/posts/' + id + '/publish';
      },
      unpublish: function (id) {
        return '/oadmin/posts/' + id + '/unpublish';
      },
      getData: function () {
        return '/oadmin/posts/getdata';
      },
      edit: function (id) {
        return '/oadmin/posts/' + id + '/edit';
      },
      authors: function () {
        return '/oadmin/posts/authors';
      },
      preview: function (id) {
        return '/oadmin/posts/' + id + '/readonly';
      }
    },
    news: {
      destroy: function (id) {
        return '/oadmin/news/' + id + '/destroy';
      },
      getData: function () {
        return '/oadmin/news/getdata';
      },
      edit: function (id) {
        return '/oadmin/news/' + id + '/edit';
      },
      publish: function (id) {
        return '/oadmin/news/' + id + '/publish';
      },
      unpublish: function (id) {
        return '/oadmin/news/' + id + '/unpublish';
      },
      authors: function () {
        return '/oadmin/news/authors';
      },
      preview: function (id) {
        return '/oadmin/news/' + id + '/readonly';
      }
    },
    takeactions: {
      getData: function () {
        return '/oadmin/takeactions/getdata';
      },
      edit: function (id) {
        return '/oadmin/takeactions/' + id + '/edit';
      },
      publish: function (id) {
        return '/oadmin/takeactions/' + id + '/publish';
      },
      unpublish: function (id) {
        return '/oadmin/takeactions/' + id + '/unpublish';
      },
      destroy: function (id) {
        return '/oadmin/takeactions/' + id + '/destroy';
      },
      authors: function () {
        return '/oadmin/takeactions/authors';
      },
      types: function () {
        return '/oadmin/takeactions/types';
      },
      preview: function (id) {
        return '/oadmin/takeactions/' + id + '/readonly';
      }
    },
    videos: {
      getData: function () {
        return '/oadmin/videos/getdata';
      },
      edit: function (id) {
        return '/oadmin/videos/' + id + '/edit';
      },
      publish: function (id) {
        return '/oadmin/videos/' + id + '/publish';
      },
      unpublish: function (id) {
        return '/oadmin/videos/' + id + '/unpublish';
      },
      destroy: function (id) {
        return '/oadmin/videos/' + id + '/destroy';
      },
      authors: function () {
        return '/oadmin/videos/authors';
      },
      types: function () {
        return '/oadmin/videos/types';
      },
      preview: function (id) {
        return '/oadmin/videos/' + id + '/readonly';
      }
    },
    media_gallery: {
      getFiles: function (folder) {
        return '/oadmin/gallery/getfiles?folder=' + folder;
      },
      createFolder: function () {
        return '/oadmin/gallery/createfolder';
      },
      rename: function () {
        return '/oadmin/gallery/quickrename';
      },
      destroy: function () {
        return '/oadmin/gallery/destroy';
      }
    },
    publicities: {
      getData: function () {
        return '/oadmin/publicities/getdata';
      },
      edit: function (id) {
        return '/oadmin/publicities/' + id + '/edit';
      },
      publish: function (id) {
        return '/oadmin/publicities/' + id + '/publish';
      },
      unpublish: function (id) {
        return '/oadmin/publicities/' + id + '/unpublish';
      },
      destroy: function (id) {
        return '/oadmin/publicities/' + id + '/destroy';
      },
      authors: function () {
        return '/oadmin/publicities/authors';
      },
      types: function () {
        return '/oadmin/publicities/types';
      },
      preview: function (id) {
        return '/oadmin/publicities/' + id + '/readonly';
      }
    },
    organisations: {
      getData: function () {
        return '/oadmin/organisations/getdata';
      },
      revoke: function () {
        return '/oadmin/organisations/revoke';
      },
      activate: function () {
        return '/oadmin/organisations/activate';
      },
      setTop: function () {
        return '/oadmin/organisations/settop';
      },
      unsetTop: function () {
        return '/oadmin/organisations/unsettop';
      },
      types: function () {
        return '/oadmin/organisations/types';
      },
      preview: function (orgId) {
        return '/oadmin/profile/org/' + orgId + '/readonly';
      }
    },
    individuals: {
      getData: function () {
        return '/oadmin/individuals/getdata';
      },
      revoke: function () {
        return '/oadmin/individuals/revoke';
      },
      activate: function () {
        return '/oadmin/individuals/activate';
      },
      setTop: function () {
        return '/oadmin/individuals/settop';
      },
      unsetTop: function () {
        return '/oadmin/individuals/unsettop';
      },
      preview: function (profileId) {
        return '/oadmin/profile/ind/' + profileId + '/readonly';
      }
    },
    common: {
      getSlug: function (title) {
        return '/oadmin/methods/getslug?title=' + title;
      },
      getVideoDetails: function (url) {
        return '/oadmin/methods/getvideodetails?url=' + url;
      },
      getCountries: function () {
        return '/oadmin/methods/getcountries';
      },
      getNationalities: function () {
        return '/oadmin/methods/getnationalities';
      },
      getMemberStatuses: function () {
        return '/oadmin/methods/getmemberstatuses';
      },
      getProfessionalStatuses: function () {
        return '/oadmin/methods/getprofessionalstatuses';
      },
      changePassword: function () {
        return '/oadmin/account/changepassword';
      },
      resetPassword: function () {
        return '/resetpassword';
      },
      resendReminder: function () {
        return '/resendreminder';
      },
      resendVerification: function () {
        return '/resendverification2';
      },
      getAllFilters: function () {
        return '/oadmin/filters/getall';
      },
      createFilter: function () {
        return '/oadmin/filters/create';
      },
      deleteFilter: function () {
        return '/oadmin/filters/delete';
      },
      updateFilter: function () {
        return '/oadmin/filters/update';
      },
      getCountriesForPlatform: function () {
        return '/oadmin/platforms/countries';
      },
      getAllPlatforms: function () {
        return '/oadmin/platforms/getall';
      },
      createPlatform: function () {
        return '/oadmin/platforms/create';
      },
      getOrgAssignedPlatforms: function (orgId) {
        return '/oadmin/platforms/getorgassignedplatforms&orgid=' + orgId;
      },
      assignOrgToPlatform: function () {
        return '/oadmin/platforms/assignorgtoplatform';
      },
      unassignOrgFromPlatform: function () {
        return '/oadmin/platforms/unassignorgfromplatform';
      },
      getIndAssignedPlatforms: function (indId) {
        return '/oadmin/platforms/getindassignedplatforms&indid=' + indId;
      },
      assignIndToPlatform: function () {
        return '/oadmin/platforms/assignindtoplatform';
      },
      unassignIndFromPlatform: function () {
        return '/oadmin/platforms/unassignindfromplatform';
      },
      lastPosts: function () {
        return '/oadmin/last-posts';
      }
    }
  };

}(window.libraries = window.libraries || {}));


