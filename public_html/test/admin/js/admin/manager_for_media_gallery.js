(function($) {

  $.fn.editableform.buttons = '' +
    '<button type="submit" class="btn btn-primary btn-xs editable-submit"><i class="glyphicon glyphicon-ok"></i></button>' +
    '<button type="button" class="btn btn-default btn-xs editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>' +
    '';

  var mediaGallery = {
    files: [],
    getFileById: function (fileId) {
      var file = _.find(this.files, function (f) {
        return f.fileId === fileId;
      });
      return file;
    },
    folder: '',
    getFolder: function () {
      return this.folder;
    },
    addFolder: function (partial) {
      this.folder = ''.concat(this.folder, '/', partial);
    },
    navigateToFolder: function () {
      $(this).trigger('call_navigateToFolder');
    },
    getBreadcrumb: function () {
      if (this.folder) {
        var splits = this.folder.split('/');
        var splits2 = splits.filter(function (el) {
          return el.length > 0;
        });
        return splits2;
      } else {
        return [];
      }
    },
    setFolderFromQS: function () {
      var uri = new URI(document.URL);
      var qs = uri.search(true);
      if (qs) {
        if (qs.fdr) {
          this.folder = qs.fdr;
        }
      }
    }
  };

  window.__mediaGallery = mediaGallery;

  $(mediaGallery).on('call_navigateToFolder', function () {
    var uri = new URI(document.URL);
    uri.search({
      fdr: this.folder
    });
    if (window.history) {
      window.history.replaceState({}, "", uri.href());
    } else {
      window.location.href = uri.href();
    }
    getFiles();
  });

  function blockArea () {
    $('.panel-body').block({ css: {
        padding: '10px',
        'z-index': 1001
      },
      message: '<h4><i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;&nbsp;loading...</h4>'
    });
  }

  function unblockArea () {
    $('.panel-body').unblock();
  }

  function init () {
    mediaGallery.setFolderFromQS();
  }

  function getFiles () {
    blockArea();
    $.get(window.libraries.ws_urls.media_gallery.getFiles(mediaGallery.folder), function (data) {
      mediaGallery.files = data.files;
      renderFiles();
      renderBreadcrumb();
      // editable file name
      $('.filename').editable({
        url: function (params) {
          var d = $.Deferred();
          var newName = params.value; //''.concat(mediaGallery.folder, '/', params.value);
          var oldName = params.pk; //''.concat(mediaGallery.folder, '/', params.pk);
          if (newName.length === 0) {
            setTimeout(function () { d.reject(oldName); }, 100);
            return d;
          }
          $.post(window.libraries.ws_urls.media_gallery.rename(), { folder: mediaGallery.folder, newName: newName, oldName: oldName }, function (ping) {
            d.resolve(ping);
          }).fail(function() {
            d.reject();
          });
          return d;
        },
        success: function (response, newValue) {
          if (response.success) {
            var editedid = parseInt($(this).closest('.item').attr('data-fileid'), 10);
            var editedfile = mediaGallery.getFileById(editedid);
            editedfile.fileName = newValue;
            editedfile.url = response.model.url;
            $(this).attr('data-name', newValue);
            $(this).closest('.item').find('[data-foldername]').attr('data-foldername', newValue);
          }
        },
        error: function (response, newValue) {
          if (response) {
            $(this).attr('data-name', response);
            $(this).closest('.item').find('[data-foldername]').attr('data-foldername', response);
            $(this).closest('.item').find('.editable-input input[type="text"]').val(response);
          }
        },
        pk: function () {
          return $(this).attr('data-name');
        }
      });
    }).always(function () {
      setTimeout(function () {
        unblockArea();
      }, 1000);
    });
  }

  function renderBreadcrumb () {
    // reset breadcrumb
    $('#gallery_breadcrumb').empty();
    // root
    var li = '<li><a href="javascript:;" data-foldername="" class="open-folder"><span class="label label-info">root</span></a></li>';
    $('#gallery_breadcrumb').append(li);
    // put traces :D
    var traces = mediaGallery.getBreadcrumb();
    for (var i = 0; i < traces.length; i++) {
      var item = traces[i];
      li = '<li><a href="javascript:;" data-foldername="' + item + '" class="open-folder"><span class="label label-info">' + item + '</span></a></li>';
      $('#gallery_breadcrumb').append(li);
    }
  }

  function renderFiles () {
    // reset area
    $('#gallery').empty();
    // render items
    for (var i = 0; i < mediaGallery.files.length; i++) {
      var item = mediaGallery.files[i];
      var template = $('#template_file_item').html();
      var compiled = _.template(template);
      var toa = compiled(item);
      $('#gallery').append(toa);
    }
    // post render activities - isotope layout for media items
    var $container = $('#gallery');
    var imgLoad = imagesLoaded($container);
    imgLoad.on('done', function () {
      if($container.hasClass('isotope')) {
        $container.isotope('destroy');
      }
      $container.isotope({
        itemSelector: '.item',
        animationOptions: {
          duration: 500,
          easing: 'linear',
          queue: false
        },
        layoutMode: 'fitRows'
      });
    });
    // post render activities - icheck for media item checkbox
    $('.tool-overlay .icheck input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-red',
      radioClass: 'iradio_flat-red'
    });
  }

  function deleteFiles (ids) {
    var failedCount = 0;
    var successCount = 0;
    var d = $.Deferred();
    for (var i = 0; i < ids.length; i++) {
      var id = ids[i];
      var file = mediaGallery.getFileById(id);
      $.post(window.libraries.ws_urls.media_gallery.destroy(), { folder: mediaGallery.folder, fileName: file.fileName }, function (ret) {
        //if (i === ids.length) {
        //  d.resolve();
        //}
        successCount++;
      }).fail(function () {
        failedCount++;
      }).always(function () {
        if (i === ids.length) {
          if (successCount > 0) {
            // partial success, just let it go :D
            d.resolve();
          } else if (failedCount === ids.length) {
            // fail all, drop
            d.reject();
          }
        }
      });
    }
    return d.promise();
  }

  // handler for clicking anywhere in document
  var clickOnAnyWhereHandler = function () {
    if (!window.mouse_inside_add_folder_modal) {
      if ($('.add-folder-modal').is(':visible')) {
        $('.add-folder-modal').addClass('hide');
        document.removeEventListener('click', clickOnAnyWhereHandler);
      }
    }
  };

  var getSelectedFiles = function () {
    var selectedFiles = [];
    if ($('.media-gal .item .tool-overlay input[type="checkbox"]:checked').length > 0) {
      $('.media-gal .item .tool-overlay input[type="checkbox"]:checked').each(function (idx, elem) {
        var item = $(elem).closest('.item');
        var fileId = parseInt($(item).attr('data-fileid'), 10);
        var file = mediaGallery.getFileById(fileId);
        selectedFiles.push(file);
      });
    } else {

    }
    return selectedFiles;
  };

  var fileInfoModal = {
    fileName: {
      set: function (value) { $('#fileInfoModal #fileinfo_filename').text(value); }
    },
    fileType: {
      set: function (value) { $('#fileInfoModal #fileinfo_fileext').text(value); }
    },
    fileResolution: {
      set: function (value) { $('#fileInfoModal #fileinfo_filedim').text(value); }
    },
    fileSize: {
      set: function (value) { $('#fileInfoModal #fileinfo_filesize').text(value); }
    },
    fileUrl: {
      set: function (value) { $('#fileInfoModal #fileinfo_url').text(value); }
    },
    image: {
      set: function (value) { $('#fileInfoModal #fileinfo_imgsrc').attr('src', value); }
    },
    fileId: {
      set: function (value) { $('#fileInfoModal [data-fileid]').attr('data-fileid', value); }
    },
    show: function (file) {
      var dis = this;
      //$('#fileInfoModal').on('shown.bs.modal', function () {
      dis.renderData(file);
      //});
      $('#fileInfoModal').modal('show');
    },
    clear: function () {
      this.fileName.set('');
      this.fileType.set('');
      this.fileSize.set('');
      this.fileUrl.set('');
      this.fileId.set('');
      this.fileResolution.set('');
      this.image.set('');
    },
    renderData: function (file) {
      this.clear();
      this.fileName.set(file.fileName);
      this.fileType.set(file.fileExtension);
      this.fileSize.set(file.fileSize);
      this.fileUrl.set(file.url);
      this.fileId.set(file.fileId);
      if (file.fileType === 'images') {
        this.fileResolution.set(file.imageSize[0] + ' x ' + file.imageSize[1]);
        this.image.set(file.url);
      }
    }
  };

  var uploadFilesModal = {
    show: function () {
      $('#uploadModal').on('hidden.bs.modal', function () {
        if ($('#uploadModal tbody.files').children().length > 0) {
          getFiles();
        }
      });
      $('#uploadModal').on('shown.bs.modal', function () {
        $('#uploadModal tbody.files').empty();
      });
      $('#uploadModal').modal('show');
      $('#uploadModal #upload_destination_folder').text(mediaGallery.folder);
    }
  };

  var setupEvents = {
    onFilterMedia: function () {
      // filter items when filter link is clicked
      $(document).on('click', '#filters a', function (e) {
        var selector = $(this).attr('data-filter');
        var $container = $('#gallery');
        $container.isotope({ filter: selector });
        e.preventDefault();
      });
    },
    onClickBreadcrumbItem: function () {
      // breadcrumb event
      $(document).on('click', '#gallery_breadcrumb li a.open-folder', function (e) {
        // get all previous siblings (li > a), get the attr data-foldername, concat to path /a/b/c
        // add current cliked anchor to last path /a/b/c/d
        var fn = $(this).attr('data-foldername');
        var fns = [];
        $(this).closest('li').prevAll().find('a.open-folder').each(function (idx, elem) {
          fns.push($(elem).attr('data-foldername'));
        });
        fns.push(fn);
        mediaGallery.folder = fns.join('/').trimLeft();
        mediaGallery.navigateToFolder();
        e.preventDefault();
      });
    },
    onOpenFolder: function () {
      // post render events
      $(document).on('click', '#gallery .item a.open-folder', function (e) {
        var fn = $(this).attr('data-foldername');
        mediaGallery.addFolder(fn);
        mediaGallery.navigateToFolder();
        e.preventDefault();
      });
    },
    onAddNewFolder: function () {
      // add new folder button
      $(document).on('click', '.btn-add-folder', function (e) {
        if ($('.add-folder-modal').length === 0) {
          var compiled = _.template($('#template_add_folder').html());
          $('body').append(compiled());
        }
        var pos = $(this).offset();
        $('.add-folder-modal').css('top', pos.top + $(this).outerHeight());
        $('.add-folder-modal').css('right', $(window).width() - pos.left - $(this).outerWidth() + 1);
        $('.add-folder-modal').removeClass('hide');
        $('.add-folder-modal #inputFolderName').val('');
        $('.add-folder-modal #inputFolderName').focus();
        document.addEventListener('click', clickOnAnyWhereHandler);
        e.preventDefault();
        e.stopPropagation();
      });
    },
    onClickAnyWhere: function () {
      // document click everywhere
      $(document).on('mouseenter', '.add-folder-modal', function () {
        window.mouse_inside_add_folder_modal = true;
      });
      $(document).on('mouseleave', '.add-folder-modal', function () {
        window.mouse_inside_add_folder_modal = false;
      });
    },
    onSaveNewFolder: function () {
      // submit/save new folder
      $(document).on('click', '.add-folder-modal [type="submit"]', function (e) {
        var currentFolder = mediaGallery.folder;
        var createdFolder = currentFolder + '/' + $('.add-folder-modal #inputFolderName').val();
        var l = Ladda.create(this);
        l.start();
        $.post(window.libraries.ws_urls.media_gallery.createFolder(), { folder: createdFolder }, function (ping) {
          //window.location.reload();
          getFiles();
          // hide the add new folder popup
          window.mouse_inside_add_folder_modal = false;
          clickOnAnyWhereHandler();
        }).always(function () {
          l.stop();
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
      });
    },
    onSelectAllMediaItem: function () {
      // select all
      $(document).on('click', '.btn-select-all', function (e) {
        // all checked or not?
        if ($('.media-gal .item .tool-overlay input[type="checkbox"]').length === $('.media-gal .item .tool-overlay input[type="checkbox"]:checked').length) {
          $('.media-gal .item .tool-overlay input[type="checkbox"]').iCheck('uncheck');
          $('.media-gal .item .tool-overlay input[type="checkbox"]').removeAttr('checked', 'checked');
          $('.media-gal .item .tool-overlay input[type="checkbox"]').prop('checked', false);
        } else {
          $('.media-gal .item .tool-overlay input[type="checkbox"]').iCheck('check');
          $('.media-gal .item .tool-overlay input[type="checkbox"]').attr('checked', 'checked');
          $('.media-gal .item .tool-overlay input[type="checkbox"]').prop('checked', true);
        }
        e.preventDefault();
      });
    },
    onShowFileInfo: function () {
      $(document).on('click', '.media-gal .item a.file-action', function (e) {
        // var filetype = $(this).closest('.item').attr('data-filetype');
        var fileId = parseInt($(this).closest('.item').attr('data-fileid'), 10);
        var file = mediaGallery.getFileById(fileId);
        if (file.fileType !== 'folder') {
          fileInfoModal.show(file);
        }
        e.preventDefault();
      });
    },
    onDeleteFile: function () {
      $('#fileInfoModal #fileinfo_delete').confirmation({
        trigger: 'click',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        title: 'File will be deleted. Are you sure?',
        singleton: true,
        placement: 'left',
        onConfirm: function () {
          var e = this;
          var fileId = parseInt($(e).attr('data-fileid'), 10);
          var l = Ladda.create($(e).get(0));
          l.start();
          var promise = deleteFiles([fileId]);
          promise.done(function () {
            getFiles();
          });
          promise.always(function () {
            l.stop();
            $('#fileInfoModal').modal('hide');
          });
        }
      });
      $(document).on('click', '#fileInfoModal #fileinfo_delete', function (e) {
        e.preventDefault();
      });
      $('.media-toolbox .btn-delete').confirmation({
        trigger: 'click',
        btnOkClass: 'btn-xs btn-primary-original',
        btnCancelClass: 'btn-xs btn-default-original',
        title: 'Selected files and directories will be deleted. Files inside directories will be deleted also. Are you sure?',
        singleton: true,
        placement: 'left',
        onConfirm: function () {
          var e = this;
          var selectedFiles = getSelectedFiles();
          if (selectedFiles.length > 0) {
            var ids = _.map(selectedFiles, function (f) {
              return f.fileId;
            });
            var l = Ladda.create($(e).get(0));
            l.start();
            var promise = deleteFiles(ids);
            promise.done(function () {
              getFiles();
            });
            promise.always(function () {
              l.stop();
            });
          }
        }
      });
      $(document).on('click', '.media-toolbox .btn-delete', function (e) {
        e.preventDefault();
      });
    },
    onUploadFiles: function () {
      $(document).on('click', '.media-toolbox .btn-upload-files', function (e) {
        uploadFilesModal.show();
        e.preventDefault();
      });
    },
    onSetupFileUpload: function () {
      $('#fileupload').fileupload({
        url: '/oadmin/gallery/uploads'
        //formData: { folder: mediaGallery.getFolder() }
      });
      $('#fileupload').bind('fileuploadsubmit', function (e, data) {
        data.formData = { folder: mediaGallery.getFolder() };
      });
    }
  };

  $(function () {

    init();
    getFiles();

    setupEvents.onFilterMedia();
    setupEvents.onClickBreadcrumbItem();
    setupEvents.onOpenFolder();
    setupEvents.onAddNewFolder();
    setupEvents.onClickAnyWhere();
    setupEvents.onSaveNewFolder();
    setupEvents.onSelectAllMediaItem();
    setupEvents.onShowFileInfo();
    setupEvents.onDeleteFile();
    setupEvents.onUploadFiles();
    setupEvents.onSetupFileUpload();

    // $(document).on('click', '.media-gal .item a.overlay-action', function (e) {
    //   e.preventDefault();
    // });

  });

} (jQuery));