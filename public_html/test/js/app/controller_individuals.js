newsController.controller('AllIndividualsCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  '$timeout',
  function ($rootScope, $scope, $http, $timeout) {
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.info_text = {
      filters_text: {
        nationality_text: '',
        member_status_text: '',
        professional_status_text: '',
        employer_organisation_type_text: '',
        get: function () {
          var str = '';
          if (this.nationality_text && this.nationality_text.length > 0) {
            str += this.nationality_text;
          }
          if (this.member_status_text && this.member_status_text.length > 0) {
            str += this.member_status_text;
          }
          if (this.professional_status_text && this.professional_status_text.length > 0) {
            str += '/' + this.professional_status_text;
          }
          if (this.employer_organisation_type_text && this.employer_organisation_type_text.length > 0) {
            str += '/' + this.employer_organisation_type_text;
          }
          if (str[0] === '/') {
            str = str.substr(1);
          }
          str = str.replace(/\//gi, ' / ');
          return str;
        }
      },
    };
    $scope.query = {
      filters: {
        nationality: -1,
        member_status: -1,
        professional_status: -1,
        employer_organisation_type: -1,
      },
      pageSize: 18,
      pageNumber: 1,
      //sortBy: 'name',
      sortBy: 'recent',
    };
    $scope.hasFilter = false;
    $scope.searchQuery = '';
    $scope.items = [];
    $scope.record_count = 0;
    $scope.page_count = 1;
    $scope.based_in_count = 0;
    $scope.filters = {};
    $scope.getData = function (append) {
      $http.post('/api/individuals/_getdata', { query: $scope.query, searchQuery: $scope.searchQuery }).success(function (data, status, headers, config) {
        if (append) {
          for (var i = 0; i < data.result.data.length; i++) {
            $scope.items.push(data.result.data[i]);
          }
        } else {
          $scope.items = data.result.data;
        }
        $scope.record_count = data.result.record_count;
        $scope.page_count = data.result.page_count;
        $scope.based_in_count = data.result.based_in_count;
        $timeout(function () {
          window.AIDWEB._doClamp();
          window.AIDWEB._doLazyLoad();
          $("#list_of_organisations .portfolio-image img.lazy").lazyload({
            effect : "fadeIn", 
            load: function () {
              var pih = jQuery(this).closest('.portfolio-image').height();
              var ih = jQuery(this).height();
              if (ih < pih) {
                jQuery(this).css('height', pih);
                jQuery(this).css('width', 'auto');
                jQuery(this).css('max-width', 'none');
                //jQuery(this).css('margin-left', '-25%');
              }
            }
          });
        }, 0, false);
        //console.log($scope.items);
      }).error(function (data, status, headers, config) {

      });
    };
    $scope.resetPagination = function () {
      $scope.query.pageNumber = 1;
    };
    $scope.doSearch = function () {
      $scope.resetPagination();
      $scope.getData();
    };
    $scope.watchSortBy = function () {
      $scope.$watch('query.sortBy', function (newValue, oldValue) {
        if (newValue !== oldValue) {
          $scope.resetPagination();
          $scope.getData();
        }
      }, true);
    };
    $scope.watchFilters = function () {
      $scope.$watch('query.filters', function (newValue, oldValue) {
        if (newValue !== oldValue) {
          $scope.hasFilter = $scope.query.filters.nationality > -1 ||
                             $scope.query.filters.member_status > -1 ||
                             $scope.query.filters.professional_status > -1 ||
                             $scope.query.filters.employer_organisation_type > -1;
          window.setTimeout(function () {
            window.AIDWEB._removeBreadcrumbSlash();
          }, 100);
          $scope.resetPagination();
          $scope.getData();
        }
      }, true);
    };
    $scope.loadMore = function () {
      $scope.query.pageNumber = $scope.query.pageNumber + 1;
      $scope.getData(true);
    };
    $scope.positionDescription = function (item) {
      //console.log(item);
      var pd = '';
      if (item.job_title) {
        pd += item.job_title;
        if (item.employer_name) {
          pd += ' at ' + item.employer_name;
        }
      } else if (item.employer_name) {
        pd += item.employer_name;
      }
      return pd;
    };

    angular.element(document).ready(function () {
      $http.get('/api/individuals/_getfilters').success(function (data, status, headers, config) {
        $scope.filters = data.filters;
        window.__renderCountsForIndividuals(data.counts);
      }).error(function (data, status, headers, config) {
        
      });
      $scope.watchSortBy();
      $scope.watchFilters();
      $scope.getData();
      $scope.safeApply();
    });
  }
]);