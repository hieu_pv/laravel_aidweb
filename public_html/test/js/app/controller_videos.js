videosController.controller('AllVideosCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  '$timeout',
  function ($rootScope, $scope, $http, $timeout) {
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.info_text = {
      filters_text: {
        region_text: '',
        country_text: '',
        crisis_text: '',
        intervention_text: '',
        theme_text: '',
        sector_text: '',
        beneficiary_text: '',
        organisationType_text: '',
        get: function () {
          var str = '';
          if (this.region_text && this.region_text.length > 0) {
            str += this.region_text;
          }
          if (this.country_text && this.country_text.length > 0) {
            str += this.country_text;
          }
          if (this.crisis_text && this.crisis_text.length > 0) {
            str += '/' + this.crisis_text;
          }
          if (this.intervention_text && this.intervention_text.length > 0) {
            str += '/' + this.intervention_text;
          }
          if (this.theme_text && this.theme_text.length > 0) {
            str += '/' + this.theme_text;
          }
          if (this.sector_text && this.sector_text.length > 0) {
            str += '/' + this.sector_text;
          }
          if (this.beneficiary_text && this.beneficiary_text.length > 0) {
            str += '/' + this.beneficiary_text;
          }
          if (this.organisationType_text && this.organisationType_text.length > 0) {
            str += '/' + this.organisationType_text;
          }
          if (str[0] === '/') {
            str = str.substr(1);
          }
          str = str.replace(/\//gi, ' / ');
          return str;
        }
      },
    };
    $scope.query = {
      filters: {
        region: -1,
        country: -1,
        crisis: -1,
        intervention: -1,
        theme: -1,
        sector: -1,
        beneficiary: -1,
        organisationType: -1,
      },
      pageSize: 18,
      pageNumber: 1,
      sortBy: 'recent', // choose 'recent' or 'popularity'
    };
    $scope.hasFilter = false;
    $scope.searchQuery = '';
    $scope.items = [];
    $scope.record_count = 0;
    $scope.org_count = 0;
    $scope.page_count = 1;
    $scope.filters = {};
    $scope.getData = function (append) {
      $http.post('/api/videos/_getdata', { query: $scope.query, searchQuery: $scope.searchQuery }).success(function (data, status, headers, config) {
        if (append) {
          for (var i = 0; i < data.result.data.length; i++) {
            $scope.items.push(data.result.data[i]);
          }
        } else {
          $scope.items = data.result.data;
        }
        $scope.record_count = data.result.record_count;
        $scope.org_count = data.result.org_count;
        $scope.page_count = data.result.page_count;
        $timeout(function () {
          window.AIDWEB._doClamp();
          window.AIDWEB._doLazyLoad();
        }, 0, false);
      }).error(function (data, status, headers, config) {

      });
    };
    $scope.resetPagination = function () {
      $scope.query.pageNumber = 1;
    };
    $scope.doSearch = function () {
      $scope.resetPagination();
      $scope.getData();
    };
    $scope.watchSortBy = function () {
      $scope.$watch('query.sortBy', function (newValue, oldValue) {
        if (newValue !== oldValue) {
          $scope.resetPagination();
          $scope.getData();
        }
      }, true);
    };
    $scope.watchFilters = function () {
      $scope.$watch('query.filters', function (newValue, oldValue) {
        if (newValue !== oldValue) {
          $scope.hasFilter = $scope.query.filters.region > -1 ||
                             $scope.query.filters.country > -1 ||
                             $scope.query.filters.crisis > -1 ||
                             $scope.query.filters.theme > -1 ||
                             $scope.query.filters.sector > -1 ||
                             $scope.query.filters.beneficiary > -1 ||
                             $scope.query.filters.organisationType > -1;
          window.setTimeout(function () {
            window.AIDWEB._removeBreadcrumbSlash();
          }, 100);
          $scope.resetPagination();
          $scope.getData();
        }
      }, true);
    };
    $scope.loadMore = function () {
      $scope.query.pageNumber = $scope.query.pageNumber + 1;
      $scope.getData(true);
    };
    angular.element(document).ready(function () {
      $http.get('/api/_filters/4').success(function (data, status, headers, config) {
        $scope.filters = data.filters;
        window.__renderCounts(data.counts);
      }).error(function (data, status, headers, config) {
        
      });
      $scope.watchSortBy();
      $scope.watchFilters();
      $scope.getData();
      $scope.safeApply();
    });
  }
]);