profileController.controller('ProfileIndCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  function ($rootScope, $scope, $http) {
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.posts = [];
    $scope.userProfileId = -1;
    $scope.pageNumber = 1;
    $scope.recordCount = 0;
    $scope.pageCount = 1;
    $scope.getData = function () {
      var d = {
        userProfileId: $scope.userProfileId,
        pageNumber: $scope.pageNumber
      };
      $http.post('/api/blogs/_getpostsbyuserprofileid', d).success(function (data, status, headers, config) {
        if (data.result) {
          if ($scope.pageNumber === 1) {
            $scope.posts = data.result;
            $scope.recordCount = data.record_count;
            $scope.pageCount = data.page_count;
          } else {
            for (var i = 0; i < data.result.length; i++) {
              $scope.posts.push(data.result[i]);
            }
          }
        }
      }).error(function (data, status, headers, config) {

      });
    };
    $scope.showMore = function ($event) {
      $scope.pageNumber++;
      $scope.getData();
      $event.preventDefault();
    };
    angular.element(document).ready(function () {
      $scope.getData();
    });
  }
]);
