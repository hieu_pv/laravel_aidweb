/* Polyfills */
// (function() {
//   window.spawn = window.spawn || function(gen) {
//     function continuer(verb, arg) {
//       var result;
//       try {
//         result = generator[verb](arg);
//       } catch (err) {
//         return Promise.reject(err);
//       }
//       if (result.done) {
//         return result.value;
//       } else {
//         return Promise.resolve(result.value).then(onFulfilled, onRejected);
//       }
//     }
//     var generator = gen();
//     var onFulfilled = continuer.bind(continuer, 'next');
//     var onRejected = continuer.bind(continuer, 'throw');
//     return onFulfilled();
//   };
//   window.showModalDialog = window.showModalDialog || function(url, arg, opt) {
//     url = url || ''; //URL of a dialog
//     arg = arg || null; //arguments to a dialog
//     opt = opt || 'dialogWidth:300px;dialogHeight:200px'; //options: dialogTop;dialogLeft;dialogWidth;dialogHeight or CSS styles
//     var caller = showModalDialog.caller.toString();
//     var dialog = document.body.appendChild(document.createElement('dialog'));
//     dialog.setAttribute('style', opt.replace(/dialog/gi, ''));
//     dialog.innerHTML = '<a href="#" id="dialog-close" style="position: absolute; top: 0; right: 4px; font-size: 20px; color: #000; text-decoration: none; outline: none;">&times;</a><iframe id="dialog-body" src="' + url + '" style="border: 0; width: 100%; height: 100%;"></iframe>';
//     document.getElementById('dialog-body').contentWindow.dialogArguments = arg;
//     document.getElementById('dialog-close').addEventListener('click', function(e) {
//       e.preventDefault();
//       dialog.close();
//     });
//     dialog.showModal();
//     //if using yield
//     if(caller.indexOf('yield') >= 0) {
//       return new Promise(function(resolve, reject) {
//         dialog.addEventListener('close', function() {
//           var returnValue = document.getElementById('dialog-body').contentWindow.returnValue;
//           document.body.removeChild(dialog);
//           resolve(returnValue);
//         });
//       });
//     }
//     //if using eval
//     var isNext = false;
//     var nextStmts = caller.split('\n').filter(function(stmt) {
//       if(isNext || stmt.indexOf('showModalDialog(') >= 0)
//         return isNext = true;
//       return false;
//     });
//     dialog.addEventListener('close', function() {
//       var returnValue = document.getElementById('dialog-body').contentWindow.returnValue;
//       document.body.removeChild(dialog);
//       nextStmts[0] = nextStmts[0].replace(/(window\.)?showModalDialog\(.*\)/g, JSON.stringify(returnValue));
//       eval('{\n' + nextStmts.join('\n'));
//     });
//     throw 'Execution stopped until showModalDialog is closed';
//   };
// })();


window.AIDWEB = {};

(function (aw, $) {

  aw._doClamp = function () {
    $('[data-clamp-height]').each(function (idx, elem) {
      var height = parseInt($(elem).attr('data-clamp-height'), 10);
      $(elem).css('height', '' + height + 'px');
      window.setTimeout(function () {
        //$clamp(elem, {clamp: '' + height + 'px'});
        $(elem).dotdotdot();
      }, 0);
    });
  };

  aw._doLazyLoad = function () {
    $("img.lazy").lazyload({
      effect : "fadeIn"
    });
  };

  aw._removeBreadcrumbSlash = function () {
    jQuery('.news-filters .breadcrumb li').removeClass('no-before');
    var one = jQuery('.news-filters .breadcrumb li:not(.ng-hide)').get(0);
    var pa = 0,
        pah = 0;
    if (one) {
      pa = jQuery(one).prevAll().length;
      pah = jQuery(one).prevAll('.ng-hide').length;
      if (pa === pah) {
        jQuery(one).addClass('no-before');
      } else {

      }
    }
  };

  $(function () {
    aw._doClamp();
    aw._doLazyLoad();
  });

} (window.AIDWEB, jQuery));


/* angular part */

var aidwebApp = angular.module('aidwebApp', ['newsController', 'blogsController', 'videosController', 'takeactionsController', 'profileController', 'ngSanitize'])
  .config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
  }]);

aidwebApp.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
});

aidwebApp.filter('newlines', function () {
  return function(text) {
    return text.replace(/\r\n/g, '<br/>')
               .replace(/\n/g, '<br/>')
               .replace(/"/g, '&quot;');
  };
});

var blogsController = angular.module('blogsController', []);
var newsController = angular.module('newsController', []);
var takeactionsController = angular.module('takeactionsController', []);
var videosController = angular.module('videosController', []);
var profileController = angular.module('profileController', []);

/*-------------------------------------------------------------------------------------------------*/
window.__byId = function (list, id) {
  var total = 0;
  for (var a = 0; a < list.length; a++) {
    total += list[a].record_count;
  }
  if (id === 0 || id === -1) {
    //return total;
    //console.log(list);
    //console.log(total);
    return '';
  }
  for (var i = 0; i < list.length; i++) {
    if (list[i].item_id === id) {
      return list[i].record_count;
    }
  }
  return 0;
};

window.__renderCountsForOrganisationsExecuted = false;
window.__renderCountsForOrganisations = function (counts) {
  window.setTimeout(function () {
    $('.filter-item-count-label').closest('li').show();
    $('.filter-item-count-label').remove();
    if (!window.__renderCountsForOrganisationsExecuted) {
      $('ul.dropdown-menu.scrollable-dropdown-menu').each(function (idx, elem) {
        $(elem).css('width', $(elem).width() + 40);
      });
      window.__renderCountsForOrganisationsExecuted = true;
    }

    $('.filter-countries-org li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.countries, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-countries-org').css('width', $('.filter-countries-org').width() + 40);

    $('.filter-levelofactivities-org li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.levelofactivities, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-levelofactivities-org').css('width', $('.filter-levelofactivities-org').width() + 40);

    $('.filter-organisationtypes-org li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.organisationtypes, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-organisationtypes-org').css('width', $('.filter-organisationtypes-org').width() + 40);
    
    $('.filter-item-count-label').each(function (idx, elem) {
      if ($(elem).text() === '0') {
        $(elem).closest('li').hide();
      }
    });
  }, 100);
};

window.__renderCountsForIndividualsExecuted = false;
window.__renderCountsForIndividuals = function (counts) {
  window.setTimeout(function () {
    $('.filter-item-count-label').closest('li').show();
    $('.filter-item-count-label').remove();
    if (!window.__renderCountsForIndividualsExecuted) {
      $('ul.dropdown-menu.scrollable-dropdown-menu').each(function (idx, elem) {
        $(elem).css('width', $(elem).width() + 40);
      });
      window.__renderCountsForIndividualsExecuted = true;
    }

    $('.filter-nationalities-ind li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.nationalities, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-nationalities-ind').css('width', $('.filter-nationalities-ind').width() + 40);

    $('.filter-professionalstatuses-ind li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.professionalstatuses, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-professionalstatuses-ind').css('width', $('.filter-professionalstatuses-ind').width() + 40);

    $('.filter-memberstatuses-ind li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.memberstatuses, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-memberstatuses-ind').css('width', $('.filter-memberstatuses-ind').width() + 40);

    $('.filter-employertypes-ind li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.employertypes, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-employertypes-ind').css('width', $('.filter-employertypes-ind').width() + 40);
    
    $('.filter-item-count-label').each(function (idx, elem) {
      if ($(elem).text() === '0') {
        $(elem).closest('li').hide();
      }
    });
  }, 100);
};

window.__renderCountsExecuted = false;
window.__renderCounts = function (counts) {
  window.setTimeout(function () {
    // reset first
    $('.filter-item-count-label').closest('li').show();
    $('.filter-item-count-label').remove();
    if (!window.__renderCountsExecuted) {
      $('ul.dropdown-menu.scrollable-dropdown-menu').each(function (idx, elem) {
        $(elem).css('width', $(elem).width() + 40);
      });
      window.__renderCountsExecuted = true;
    }
    
    // country/region
    $('.filter-countries li[data-filter-type="region"] > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.regions, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    $('.filter-countries li[data-filter-type="country"] > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.countries, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-countries').css('width', $('.filter-countries').width() + 40);

    // crisis
    $('.filter-crisis li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.crisis, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-crisis').css('width', $('.filter-crisis').width() + 40);

    // themes
    $('.filter-themes li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.themes, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-themes').css('width', $('.filter-themes').width() + 40);

    // sector
    $('.filter-sectors li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.sectors, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-sectors').css('width', $('.filter-sectors').width() + 40);

    // beneficiary
    $('.filter-beneficiaries li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.beneficiaries, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-beneficiaries').css('width', $('.filter-beneficiaries').width() + 40);

    // nationality
    $('.filter-nationalities li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.nationalities, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-nationalities').css('width', $('.filter-nationalities').width() + 40);

    // org types
    $('.filter-organisationtypes li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.organisationtypes, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-organisationtypes').css('width', $('.filter-organisationtypes').width() + 40);

    // action types
    $('.filter-actiontypes li > a').each(function (idx, elem) {
      var id = parseInt($(elem).attr('data-value'), 10);
      var count = __byId(counts.actiontypes, id);
      if (count > -1) {
        $(elem).html($(elem).html() + '<span class="filter-item-count-label">' + count + '</span>');
      }
    });
    //$('.filter-actiontypes').css('width', $('.filter-actiontypes').width() + 40);

    $('.filter-item-count-label').each(function (idx, elem) {
      if ($(elem).text() === '0') {
        $(elem).closest('li').hide();
      }
    });
  }, 100);
};