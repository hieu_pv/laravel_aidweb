profileController.controller('ProfileOrgCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  function ($rootScope, $scope, $http) {
    var tab = function (title, organisationId, dataUrl) {
      return {
        title: title,
        organisationId: organisationId,
        getDataUrl: dataUrl,
        items: [],
        pageNumber: 1,
        recordCount: 0,
        pageCount: 1,
        onStage: function () {
          // no data, get
          // if (this.items.length === 0) {
          //   return this.getData();
          // }
          return { always: function (callback) { if (typeof callback === 'function') callback(); } };
        },
        getData: function () {
          var dis = this;
          var d = {
            organisationId: dis.organisationId,
            pageNumber: dis.pageNumber
          };
          var df = $.Deferred();
          $http.post(dis.getDataUrl, d).success(function (data, status, headers, config) {
            if (data.result) {
              if (dis.pageNumber === 1) {
                dis.items = data.result;
                dis.recordCount = data.record_count;
                dis.pageCount = data.page_count;
              } else {
                for (var i = 0; i < data.result.length; i++) {
                  dis.items.push(data.result[i]);
                }
              }
              $scope.safeApply();
              df.resolve();
            } else {
              df.reject();
            }
          }).error(function (data, status, headers, config) {
            df.reject();
          });
          return df;
        },
        showMore: function ($event) {
          this.pageNumber++;
          this.getData();
          $event.preventDefault();
        }
      };
    };
    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $scope.organisationId = -1;
    $scope.tabs = {};
    $scope.initTabs = function () {
      $scope.tabs.news = tab('News', $scope.organisationId, '/api/news/_getnewsitemsbyorganisationid');
      $scope.tabs.videos = tab('Videos', $scope.organisationId, '/api/videos/_getvideosbyorganisationid');
      $scope.tabs.getinvolved = tab('Get Involved',$scope.organisationId, '/api/takeactions/_gettakeactionsbyorganisationid');
    };
    angular.element(document).ready(function () {
      $scope.initTabs();
      $scope.tabs.news.getData();
      $scope.tabs.videos.getData();
      $scope.tabs.getinvolved.getData();
      jQuery('#tab_nav_org_modules a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        jQuery(e.relatedTarget).closest('li').removeClass('ui-tabs-active').removeClass('ui-state-active');
        jQuery(e.target).closest('li').addClass('ui-tabs-active').addClass('ui-state-active');
        var key = jQuery(e.target).attr('data-mkey');
        var href = jQuery(e.target).attr('href');
        $scope.tabs[key].onStage().always(function () {
          $(href).find('.horiz-post-wrapper').fadeIn();
        });
      });
      jQuery('#tab_nav_org_modules li:first-child a[data-toggle="tab"]').tab('show');
    });
  }
]);
