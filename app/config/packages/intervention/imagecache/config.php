<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */
   
    'route' => 'imagecache',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => array(
        // public_path('upload'),
        // public_path('images'),
        // public_path('uploads'),
        public_path()
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */
   
    'templates' => array(
        // 'small' => 'Intervention\Image\Templates\Small',
        // 'medium' => 'Intervention\Image\Templates\Medium',
        // 'large' => 'Intervention\Image\Templates\Large',
        '360_211' => 'intervention_360_211',
        '370_216' => 'intervention_370_216',
        '370_278_c' => 'intervention_370_278_c',
        '380_222_c' => 'intervention_380_222_c',
        '680_160_c' => 'intervention_680_160_c',
        '760_400' => 'intervention_760_400',
        '1333_600' => 'intervention_1333_600',
        'r_57_c' => 'intervention_r_57_c',
        'r_255_c' => 'intervention_r_255_c',
        'r_100_auto_c' => 'intervention_r_100_auto_c',
        'r_211_auto_c' => 'intervention_r_211_auto_c',
        'r_370_auto_c' => 'intervention_r_370_auto_c',
        'r_760_auto_c' => 'intervention_r_760_auto_c',
        'r_1366_auto_c' => 'intervention_r_1366_auto_c',
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */
   
    'lifetime' => 43200,

);
