<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="">
    <title>404</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('admin/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('admin/css/bootstrap-reset.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ URL::asset('admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('admin/css/style-responsive.css') }}" rel="stylesheet" />
</head>
  <body>
    <div class="container">

      <div class="jumbotron" style="background-color: transparent">
        <div class="text-center">
          <img src="{{ URL::asset('images/logo.png') }}" alt="" style="" class="login-logo">
          <br><br>
          <h1 class="text-center">404</h1>
          <h2 class="text-center" style="">Page not found</h2>
          <p>The resource you requested was not found.</p>
          <br>
          <p><a class="btn btn-default btn-lg" href="/" role="button">Back to Home Page</a></p>
        </div>
      </div>

    </div>

  </body>
</html>



