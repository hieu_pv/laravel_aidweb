@extends('layouts.master')

@section('title', 'AidPost | Fleava Admin | Advanced Website Backend')

@section('head')
  @include('admin.common._styles')
@stop

@section('styles')
@show

@section('body')

<body>

  <section id="container">

    @include('admin.header')

    @include('admin.sidebar')

    <!--main content start-->
    <section id="main-content">

      <section class="wrapper">

        @section('content')
        @show

      </section>

    </section>
    <!--main content end-->

    {{--@include('admin.sidebar_right')--}}
    <div class="clearfix"></div>

  </section>

  <!-- Placed js at the end of the document so the pages load faster -->
  @include('admin.common._scripts')

  <script>
    (function (ouser) {
      ouser.current = {
        id: {{ Auth::user()->id }},
        isAdmin: {{ Auth::user()->isAdmin() ? 'true' : 'false' }},
        isIndividual: {{ Auth::user()->isIndividual() ? 'true' : 'false' }},
        isOrganisation: {{ Auth::user()->isOrganisation() ? 'true' : 'false' }}
      };
    } (window._ouser = window._ouser || {}));
  </script>

  @section('scripts')
  @show

</body>

@stop