@extends('layouts.master')

@section('title', 'AidPost')

@section('head')
  @include('admin.common._styles')
@stop

@section('styles')
@show

@section('body')

<body class="stretched no-transition">

  <section id="" class="container">

    <section class="">

      @section('content')
      @show

    </section>
    
  </section>

  @include('admin.common._scripts')

  @section('scripts')
  @show

</body>

@stop