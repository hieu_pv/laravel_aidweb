<!DOCTYPE html>
<html lang="en" ng-app="fleavaAdminApp">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
  <meta name="description" content="@yield('description', 'AidPost')">
  <meta name="author" content="Fleava">
  <link rel="shortcut icon" href="">
  <title>@yield('title', 'AidPost')</title>
  {{-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" type="text/css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic" type="text/css"> --}}
  <link href="{{ URL::asset('admin/css/fonts.css') }}" rel="stylesheet">
  @section('head')
  @show
</head>
@section('body')
@show
</html>
