<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="fleava" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>AidPost</title>
  @section('head')
  @show
  @include('pages.common._styles')
  @include('pages.common._scripts_head')
</head>
<body class="stretched no-transition" ng-app="aidwebApp">
  <!-- Document Wrapper ============================================= -->
  <div id="wrapper" class="clearfix">
    @include('pages.header')

    @section('before_content')
    @show

    <section id="content">
      @section('content')
      @show
    </section><!-- #content end -->

    @section('after_content')
    @show

    @include('pages.footer')
  </div><!-- #wrapper end -->
  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  @include('pages.common._scripts_foot')
  @section('scripts')
  @show

  <?php
  // use DebugBar\StandardDebugBar;
  // $debugbar = new StandardDebugBar();
  // $debugbarRenderer = $debugbar->getJavascriptRenderer();
  // $debugbar["messages"]->addMessage("hello world!");
  ?>
  <?php //echo $debugbarRenderer->renderHead() ?>
  <?php //echo $debugbarRenderer->render() ?>

</body>
</html>