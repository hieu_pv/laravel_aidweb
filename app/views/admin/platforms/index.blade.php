@extends('layouts.admin')

@section('content')
  
  <div ng-controller="ManagePlatformsCtrl" class="manage-platforms">

    <ul class="breadcrumb">
      <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ul>

    <div class="row">
      
      <div class="col-md-12 col-lg-12">
        <section class="panel">
          <header class="panel-heading clearfix">
            <span class="pull-left">All Platforms</span>
          </header>
          <div class="panel-body">

            @include('flash::message')

            <div class="clearfix">
              {{-- <div class="pull-left">
                <a href="#" class="btn btn-primary" ng-click="mode = (mode === 'create' ? 'anything' : 'create')"><i class="fa fa-plus"></i> Add Platform</a>
              </div> --}}
            </div>

            <div class="well"> {{--  ng-show="mode === 'create'"> --}}
              {{-- <form action="/oadmin/platforms/create" method="POST" class="clearfix"> --}}
                <h4>To create a platform choose a country from the options and click 'Create' button.</h4>
                <div class="form-group">
                  <select name="selected_country" class="form-control" ng-model="selected_country" ng-init="selected_country = 0" ng-options="country.name for country in countries"></select>
                </div>
                <div class="clearfix">
                  <div class="pull-right">
                    <a href="#" class="btn btn-success ladda-button" data-style="expand-right" ng-click="createPlatform($event);"><span class="ladda-label">Create</span></a>
                  </div>
                </div>
              {{-- </form> --}}
            </div>

            <h3>Platforms</h3>
            <ul>
              <li>Item with Blue background is Active platforms.</li>
              <li>Item with Red background is Inactive platforms.</li>
              <li>Use the thumbs-up / thumbs-down to activate / deactivate platforms.</li>
            </ul>
            {{-- <div class="row"> --}}
            <ul class="list-inline">
              @foreach($platforms as $platform)
                {{-- <div class="col-md-3"> --}}
                <li style="vertical-align: middle">
                  <div class="panel {{$platform->active ? 'panel-primary' : 'panel-danger'}}">
                    <div class="panel-heading clearfix">
                      {{$platform->display_text}}
                      <div class="pull-right" style="margin-left: 15px;">
                        @if ($platform->id !== Modules::platform_international())
                          @if ($platform->active) 
                            <a href="#" class="" style="color:#fff; cursor: pointer;" ng-deactivate-platform-confirmation="{ platformName: '{{$platform->display_text}}', platformId: {{$platform->id}} }"><i class="fa fa-thumbs-down fa-lg"></i></a>
                          @else
                            <a href="#" class="" style="color:#fff; cursor: pointer;" ng-activate-platform-confirmation="{ platformName: '{{$platform->display_text}}', platformId: {{$platform->id}} }"><i class="fa fa-thumbs-up fa-lg"></i></a>
                          @endif
                        @endif
                      </div>
                    </div>
                  </div>
                {{-- </div> --}}
                </li>
              @endforeach
            </ul>
            {{-- </div> --}}
          </div><!-- panel-body -->
        </section>
      </div>
    
    </div>

  </div><!-- controller -->

@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_manage_platforms.js') }}"></script>
@stop 

