@extends('layouts.admin')

@include('admin.common._pilot_phase')

@section('content')

  <div class="dashboard-container">
    
    @if (Auth::user()->isIndividual())
      <div>
        <p>Hello {{Auth::user()->user_profile->fullname}},</p>
        <p>You are a member of the <strong>{{$model->platformText}}</strong> platform.</p>
        <p>Your voice and visibility count.</p>
        <p>You can edit or delete your postings at any time.</p>
      </div>
    @elseif (Auth::user()->isOrganisation())
      <div>
        <p>Hello, {{Auth::user()->organisation->org_name}},</p>
        <p>You are a member of the <strong>{{$model->platformText}}</strong> platform.</p>
        <p>Your voice and visibility count.</p>
        <p>Ready to post? Please select a category. You can edit or delete your postings at any time.</p>
      </div>
    @elseif (Auth::user()->isAdmin())
      <div>
        <p>Hello Admin,</p>
      </div>
    @endif

    <p>&nbsp;</p>

    <div class="row">
      
      @if (Auth::user()->isIndividual() || Auth::user()->isAdmin())
        <div class="col-md-4 col-lg-3">
          <div class="panel">
            <div class="panel-body">
              <a href="{{ URL::route('admin_all_posts') }}"><h3><i class="fa fa-book"></i> Blogs</h3></a>
              <p><span class="text-info"><span class="count">{{$model->postsTotalRecords}}</span> posts</span></p>
              <div class="buttons">
                <a href="{{ URL::route('admin_all_posts') }}" class="btn btn-primary btn-xs"><i class="fa fa-list v-align"></i> Manage</a>
                @if (Auth::user()->isIndividual())
                  <a href="{{ URL::route('admin_create_post') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus v-align"></i> Add</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endif

      @if (Auth::user()->isOrganisation() || Auth::user()->isAdmin())
        <div class="col-md-4 col-lg-3">
          <div class="panel">
            <div class="panel-body">
              <a href="{{ URL::route('admin_all_newsitems') }}"><h3><i class="fa fa-rss"></i> News</h3></a>
              <p><span class="text-info"><span class="count">{{$model->newsTotalRecords}}</span> news</span></p>
              <div class="buttons">
                <a href="{{ URL::route('admin_all_newsitems') }}" class="btn btn-primary btn-xs"><i class="fa fa-list v-align"></i> Manage</a>
                @if (Auth::user()->isOrganisation())
                  <a href="{{ URL::route('admin_create_newsitem') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus v-align"></i> Add</a>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4 col-lg-3">
          <div class="panel">
            <div class="panel-body">
              <a href="{{ URL::route('admin_all_takeactions') }}"><h3><i class="fa fa-exclamation-circle"></i> Get involved</h3></a>
              <p><span class="text-info"><span class="count">{{$model->takeActionsTotalRecords}}</span> actions</span></p>
              <div class="buttons">
                <a href="{{ URL::route('admin_all_takeactions') }}" class="btn btn-primary btn-xs"><i class="fa fa-list v-align"></i> Manage</a>
                @if (Auth::user()->isOrganisation())
                  <a href="{{ URL::route('admin_create_takeaction') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus v-align"></i> Add</a>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4 col-lg-3">
          <div class="panel">
            <div class="panel-body">
              <a href="{{ URL::route('admin_all_videos') }}"><h3><i class="fa fa-camera"></i> Videos</h3></a>
              <p><span class="text-info"><span class="count">{{$model->videosTotalRecords}}</span> videos</span></p>
              <div class="buttons">
                <a href="{{ URL::route('admin_all_videos') }}" class="btn btn-primary btn-xs"><i class="fa fa-list v-align"></i> Manage</a>
                @if (Auth::user()->isOrganisation())
                  <a href="{{ URL::route('admin_create_video') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus v-align"></i> Add</a>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4 col-lg-3">
          <div class="panel">
            <div class="panel-body">
              <a href="{{ URL::route('admin_all_publicities') }}"><h3><i class="fa fa-picture-o"></i> Publicities</h3></a>
              <p><span class="text-info"><span class="count">{{$model->publicitiesTotalRecords}}</span> publicities</span></p>
              <div class="buttons">
                <a href="{{ URL::route('admin_all_publicities') }}" class="btn btn-primary btn-xs"><i class="fa fa-list v-align"></i> Manage</a>
                @if (Auth::user()->isOrganisation())
                  <a href="{{ URL::route('admin_create_publicity') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus v-align"></i> Add</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endif

    </div>

    @if (Auth::user()->isIndividual())
      <div>
        <p><strong>3 simple posting mechanisms:</strong></p>
        <ul>
          <li>Postings from all members are displayed in the all results section of the International platform.</li>
          <li>Your posting will also be displayed on the home page and featured area of your platform(s).</li>
          <li>Postings from members of the International platform are displayed on the national platform of the country selected as filter.</li>
        </ul>
        <p>
          <strong>Important notice:</strong><br>
          <span>All postings must relate to aid and societal issues. Inappropriate postings will be deleted and may eventually result in the termination of your account.</span>
        </p>
      </div>
    @elseif (Auth::user()->isOrganisation())
      <div>
        <p><strong>3 simple posting mechanisms:</strong></p>
        <ul>
          <li>Postings from all members are displayed in the all results section of the International platform.</li>
          <li>Your posting will also be displayed on the home page and featured area of your platform(s).</li>
          <li>Postings from members of the International platform are displayed on the national platform of the country selected as filter.</li>
        </ul>
        <p>
          <strong>Important notice:</strong><br>
          <span>All postings must relate to aid and societal issues. Inappropriate postings will be deleted and may eventually result in the termination of your account.</span>
        </p>
      </div>
    @elseif (Auth::user()->isAdmin())
      <div>
        <p>AidPost Admin</p>
      </div>
    @endif

  </div>

@stop