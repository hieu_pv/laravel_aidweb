@if(isset($activeLinkKey) && !is_null($activeLinkKey))
<input type="hidden" id="hdnActiveLinkKey" value="{{ $activeLinkKey }}">
@endif

<!--sidebar start-->
<aside>
  <div id="sidebar" class="nav-collapse"> {{-- data-spy="affix" data-offset-top="80" --}}
    <!-- sidebar menu start-->
    <div class="leftside-navigation">
      <ul class="sidebar-menu" id="nav-accordion">

        @if (Auth::check())

          <!-- dashboard -->
          <li>
            <a data-linkkey="dashboard" class="" href="{{ URL::route('admin_dashboard') }}">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>

          {{-- ====================================================================================== --}}
          {{-- ADMIN --}}
          {{-- ====================================================================================== --}}
          @if (Auth::user()->isAdmin())
            <!-- blog -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="blog">
                <i class="fa fa-book"></i>
                <span>Blog</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allposts"><a href="{{ URL::route('admin_all_posts') }}">All Posts</a></li>
              </ul>
            </li>
            <!-- news -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="news">
                <i class="fa fa-rss"></i>
                <span>News</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allnewsitems"><a href="{{ URL::route('admin_all_newsitems') }}">All News</a></li>
              </ul>
            </li>
            <!-- take actions -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="takeactions">
                <i class="fa fa-exclamation-circle"></i>
                <span>Get Involved</span>
              </a>
              <ul class="sub">
                <li data-linkkey="alltakeactions"><a href="{{ URL::route('admin_all_takeactions') }}">All Get Involved</a></li>
              </ul>
            </li>
            <!-- videos -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="videos">
                <i class="fa fa-video-camera"></i>
                <span>Videos</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allvideos"><a href="{{ URL::route('admin_all_videos') }}">All Videos</a></li>
              </ul>
            </li>
            <!-- publications -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="publicities">
                <i class="fa fa-picture-o"></i>
                <span>Publicities</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allpublicities"><a href="{{ URL::route('admin_all_publicities') }}">All Publicities</a></li>
              </ul>
            </li>
          @endif

          {{-- ====================================================================================== --}}
          {{-- INDIVIDUALS --}}
          {{-- ====================================================================================== --}}
          @if (Auth::user()->isIndividual())
            <!-- blog -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="blog">
                <i class="fa fa-book"></i>
                <span>Blog</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allposts"><a href="{{ URL::route('admin_all_posts') }}">All Posts</a></li>
                <li data-linkkey="addpost"><a href="{{ URL::route('admin_create_post') }}">Add Post</a></li>
              </ul>
            </li>
          @endif
    
          {{-- ====================================================================================== --}}
          {{-- ORGANISATIONS --}}
          {{-- ====================================================================================== --}}
          @if (Auth::user()->isOrganisation())
            <!-- news -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="news">
                <i class="fa fa-rss"></i>
                <span>News</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allnewsitems"><a href="{{ URL::route('admin_all_newsitems') }}">All News</a></li>
                <li data-linkkey="addnewsitem"><a href="{{ URL::route('admin_create_newsitem') }}">Add News</a></li>
              </ul>
            </li>
            <!-- take actions -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="takeactions">
                <i class="fa fa-exclamation-circle"></i>
                <span>Get Involved</span>
              </a>
              <ul class="sub">
                <li data-linkkey="alltakeactions"><a href="{{ URL::route('admin_all_takeactions') }}">All Get Involved</a></li>
                <li data-linkkey="addtakeaction"><a href="{{ URL::route('admin_create_takeaction') }}">Add Get Involved</a></li>
              </ul>
            </li>
            <!-- videos -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="videos">
                <i class="fa fa-video-camera"></i>
                <span>Videos</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allvideos"><a href="{{ URL::route('admin_all_videos') }}">All Videos</a></li>
                <li data-linkkey="addvideo"><a href="{{ URL::route('admin_create_video') }}">Add Video</a></li>
              </ul>
            </li>
            <!-- publications -->
            <li class="sub-menu">
              <a href="#!" class="" data-linkkey="publicities">
                <i class="fa fa-picture-o"></i>
                <span>Publicities</span>
              </a>
              <ul class="sub">
                <li data-linkkey="allpublicities"><a href="{{ URL::route('admin_all_publicities') }}">All Publicities</a></li>
                <li data-linkkey="addpublicity"><a href="{{ URL::route('admin_create_publicity') }}">Add Publicity</a></li>
              </ul>
            </li>
          @endif

          {{-- ====================================================================================== --}}
          {{-- COMMON --}}
          {{-- ====================================================================================== --}}
          <!-- media -->
          <li class="sub-menu">
            <a href="#!"  data-linkkey="media">
              <i class="fa fa-camera"></i>
              <span>Media</span>
            </a>
            <ul class="sub">
              <li><a href="{{ URL::route('admin_media_gallery') }}" data-linkkey="gallery">Media Gallery</a></li>
            </ul>
          </li>
    
          {{-- ====================================================================================== --}}
          {{-- ADMIN --}}
          {{-- ====================================================================================== --}}
          @if (Auth::user()->isAdmin())
          <!-- organisations -->
          <li class="sub-menu">
            <a href="#!" class="" data-linkkey="organisations">
              <i class="fa fa-building-o"></i>
              <span>Organisations</span>
            </a>
            <ul class="sub">
              <li data-linkkey="allorganisations"><a href="{{ URL::route('admin_all_organisations') }}">All Organisations</a></li>
            </ul>
          </li>
          <!-- individuals -->
          <li class="sub-menu">
            <a href="#!" class="" data-linkkey="individuals">
              <i class="fa fa-users"></i>
              <span>Individuals</span>
            </a>
            <ul class="sub">
              <li data-linkkey="allindividuals"><a href="{{ URL::route('admin_all_individuals') }}">All Individuals</a></li>
            </ul>
          </li>
          <!-- manage filters -->
          <li class="sub-menu">
            <a href="#!" class="" data-linkkey="managefilters">
              <i class="fa fa-filter"></i>
              <span>Filters</span>
            </a>
            <ul class="sub">
              <li data-linkkey="index"><a href="{{ URL::route('admin_manage_filters') }}">All Filters</a></li>
            </ul>
          </li>
          <!-- manage platforms -->
          <li class="sub-menu">
            <a href="#!" class="" data-linkkey="manageplatforms">
              <i class="fa fa-adjust"></i>
              <span>Platforms</span>
            </a>
            <ul class="sub">
              <li data-linkkey="index"><a href="{{ URL::route('admin_manage_platforms') }}">All Platforms</a></li>
            </ul>
          </li>
          {{-- <!-- settings -->
          <li class="sub-menu">
            <a href="#!" data-linkkey="settings">
              <i class="fa fa-gear"></i>
              <span>Settings</span>
            </a>
            <ul class="sub">
              <li><a data-linkkey="urls" href="{{ URL::route('admin_settings_urls') }}">URLS</a></li>
              <li><a href="#!"></a></li>
            </ul>
          </li> --}}
          @endif

        @else
          {{-- not authenticated --}}
          <li></li>
        @endif

      </ul>
    </div>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->
