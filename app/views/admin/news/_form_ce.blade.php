@include('admin.common._form_errors')

@include('admin.common._posting_disclaimer')

@if ($model->id === 0)
{{ Form::open(array('action' => 'AdminNewsItemController@insert', 'files' => true, 'class' => 'form-horizontal', 'id' => 'form_ce_newsitem')) }}
@else 
{{ Form::open(array('action' => 'AdminNewsItemController@update', 'files' => true, 'class' => 'form-horizontal', 'id' => 'form_ce_newsitem')) }}
@endif

  {{ Form::token() }}
  {{ Form::hidden('id', $model->id) }}

  @include('admin.common._form_filters', array('module' => Modules::news()))

  <div class="form-group hide" style="">
    {{ Form::label('name', 'Name', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::text('name', $model->name, array('class' => 'form-control')) }}
      <span class="help-block">Unique name for the news, not displayed anywhere.</span>
    </div>
  </div>

  <div class="form-group">
    {{ Form::rawLabel('title', 'Title <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::text('title', $model->title, array('class' => 'form-control', 'required' => 'required')) }}
      {{-- <span class="help-block">Title of the news, displayed as news heading.</span> --}}
    </div>
  </div>

  <div class="form-group hide">
    {{ Form::rawLabel('slug', 'Slug <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      <div class="input-group">
        <span class="input-group-btn">
          <a class="btn btn-primary btnGenerateSlug" href="#">&nbsp;<i class="fa fa-refresh"></i>&nbsp;</a>
        </span>
        {{ Form::text('slug', $model->slug, array('class' => 'form-control', 'readonly' => 'readonly', 'required' => 'required')) }}
      </div>
      <span class="help-block">Slug is generated from Title. Click the refresh button to regenerate Slug.</span>
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('image_legend', 'Photo Legend', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::text('image_legend', $model->image_legend, array('class' => 'form-control')) }}
      {{-- <span class="help-block">Photo legend, displayed when user hover at the photo.</span> --}}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('photographer_name', 'Photo Source', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::text('photographer_name', $model->photographer_name, array('class' => 'form-control')) }}
      {{-- <span class="help-block">Photographer Name, displayed when user hover at the photo.</span> --}}
    </div>
  </div>

  @include('admin.common._form_photo')

  <div class="form-group">
    {{ Form::rawLabel('content', 'Text <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::textarea('content', $model->content, array('class' => 'form-control wysihtml5 ckeditor', 'rows' => '30')) }}
    </div>
  </div>

  {{-- <div class="m-bot15">&nbsp;</div> --}}

  {{-- <div class="well">
    <p><strong>Your posting will be instantly displayed on NEWS page (filter section), and possibly on Home Page in the "Most Popular News".</strong></p>
  </div> --}}
 
  <div class="pull-right m-bot15">
    <a href="{{ URL::route('admin_all_newsitems') }}" title="" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Cancel</a>
    <a href="#" class="btn btn-info btn-getpreviewurl ladda-button" data-style="expand-right"><span class="ladda-label"><i class="fa fa-desktop"></i> Preview</span></a>
    {{-- Form::submit('Submit', array('class' => 'btn btn-success')) --}}
    <button type="submit" class="btn btn-success" {{Session::has('PostingLimitation_news') ? 'disabled': ''}}><i class="fa fa-check"></i> Submit</button>
  </div>

{{ Form::close() }}  

