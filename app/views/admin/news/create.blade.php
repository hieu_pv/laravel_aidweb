@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@include('admin.common._pilot_phase')

@section('content')
  <div ng-controller="NewsItemCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-rss"></i> News Manager</a></li>
    <li class="active">Post a News</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      Post a News
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.common._posting_within_24hours', array('category' => 'news'))
      @if(Session::get('PostingLimitation_news'))
        @include('admin.common._posting_limitation', ['category' => 'news', 'time_diff' => Session::get('PostingLimitation_news')])
      @endif
      @if (Session::get('canPostMoreThanOneIn24Hours_'.'news', true))
        @include('admin.news._form_ce')
      @endif
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        
      });
    } (jQuery));
  </script>
@stop 