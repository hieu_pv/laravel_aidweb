@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@section('content')

  <div ng-controller="NewsItemCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-rss"></i> News Manager</a></li>
    <li class="active">View News : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      View News : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      <p><strong>URL: </strong> <a target="_blank" href="{{PermalinkEngine::getPermalink(Modules::news(), $model)}}" title="{{$model->title}}">{{PermalinkEngine::getPermalink(Modules::news(), $model)}}</a></p>
      <hr>
      @include('admin.news._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        // hacks
        $('#form_ce_newsitem [type="submit"]').remove();
        $('#form_ce_newsitem .btn-getpreviewurl').remove();
        $('#form_ce_newsitem').prev('.well').remove();
        $('#form_ce_newsitem').attr('action', '#');
        $('#form_ce_newsitem').find('input, select, textarea').each(function (idx, elem) {
          $(elem).attr('disabled', 'disabled');
          $(elem).attr('readonly', 'readonly');
          $(elem).prop('disabled', true);
          $(elem).prop('readonly', true);
        });
        CKEDITOR.on('instanceReady', function (evt) {
          CKEDITOR.instances['content'].setReadOnly(true);
        });
      });
    } (jQuery));
  </script>
@stop 