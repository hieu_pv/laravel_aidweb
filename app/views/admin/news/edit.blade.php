@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@section('content')

  <div ng-controller="NewsItemCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-rss"></i> News Manager</a></li>
    <li class="active">Edit News : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      Edit News : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.news._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        
      });
    } (jQuery));
  </script>
@stop 