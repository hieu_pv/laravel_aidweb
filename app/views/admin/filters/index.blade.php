@extends('layouts.admin')

@section('content')
  
  <div ng-controller="ManageFiltersCtrl" class="manage-filters">

    <ul class="breadcrumb">
      <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ul>

    <div class="row">
      
      <div class="col-md-12 col-lg-6" ng-repeat="category in filterCategories">
        <section class="panel">
          <header class="panel-heading clearfix">
            <span class="pull-left"><%category.displayText%></span>
            {{-- <span class="tools pull-right">
              <a href="#!" class="fa fa-chevron-down"></a>
            </span> --}}
            <div class="pull-left" style="margin-left: 20px;">
              <a href="#" class="text-primary" ng-click="modes[category.tableName] = 'add';addFilterInputText[category.tableName] = '';relayout();$event.preventDefault();$event.stopPropagation();"><i class="fa fa-plus"></i> Add</a>
            </div>
          </header>
          <div class="panel-body">
            <div style="margin-bottom: 5px; background-color: #f8f9fa; padding: 10px;" ng-show="modes[category.tableName] === 'add'">
              <div class="input-group" ng-add-filter="{ filterCategory: category.tableName }">
                <input type="text" class="form-control" placeholder="insert name here" ng-model="addFilterInputText[category.tableName]"/>
                <span class="input-group-btn">
                  <button class="btn btn-white ok text-success" type="button"><i class="fa fa-check fa-fw"></i></button>
                  <button class="btn btn-white cancel text-danger" type="button"><i class="fa fa-times fa-fw"></i></button>
                </span>
              </div>
            </div>
            <ul class="filter-list">
              <li class="has-inline-editable clearfix" ng-repeat="item in filters[category.tableName] | orderBy: 'name'">
                <div class="pull-left">
                  <span class="inline-editable"><% item.name %></span>
                </div>
                <div class="pull-right">
                  <a href="#" class="close text-danger" ng-delete-confirmation="{ filterItem: item, filterCategory: category.tableName }"><i class="fa fa-trash-o fa-fw"></i></a>
                  <a href="#" class="close text-info" ng-inline-edit="{ filterItem: item, filterCategory: category.tableName }"><i class="fa fa-pencil fa-fw"></i></a>
                </div>
              </li>
            </ul>
          </div><!-- panel-body -->
        </section>
      </div>
    
    </div>

  </div><!-- controller -->

@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('admin/js/isotope.pkgd.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_manage_filters.js') }}"></script>
@stop 

