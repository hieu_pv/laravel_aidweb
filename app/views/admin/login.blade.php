@extends('layouts.master')

@section('title', 'AidPost | Connect')

@section('head')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/font-awesome/css/font-awesome.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('style.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/dark.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/font-icons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda-themeless.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/css/login.css') }}" type="text/css"/>
@stop

@section('body')

<body class="stretched no-transition">
  <div id="wrapper" class="clearfix">
    <section id="content">
      <div class="content-wrap nopadding">
        <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: #f1f2f7;"></div>
        <div class="section nobg full-screen nopadding nomargin">
          <div class="container divcenter clearfix" style="margin-top:30px;">
            <div class="row center">
              <a href="/" class="retina-logo"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo" style="height:30px; margin-bottom:30px;"></a>
            </div>
            <div class="panel panel-default divcenter noradius noborder" style="max-width: 400px;">
              <div class="panel-body" style="padding: 30px;">
                {{ Form::open(array('action' => 'AdminRegistrationController@postLogin', 'class' => 'nobottommargin', 'id' => 'login-form')) }}
                  <h3 class="nomargin" style="margin-bottom: 20px !important;">Login to your Account</h3>
                  @if (Session::has('login_errors')) 
                    <div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 5px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('login_errors') }}
                    </div>
                  @endif
                  <div class="col_full">
                    <input type="text" placeholder="username" value="" class="form-control not-dark" autofocus required id="username" name="username" />
                  </div>
                  <div class="col_full">
                    <input type="password" placeholder="password" value="" class="form-control not-dark" required id="secret" name="secret" />
                  </div>
                  <div class="col_full nobottommargin">
                    <button type="submit" class="btn btn-danger btn-long nomargin" id="login-form-submit" name="login-form-submit" value="login">LOGIN</button>
                    <a data-toggle="modal" href="#resetPasswordModal" class="fright underline">Forgot Password?</a>
                  </div>
                  <input type="hidden" name="previousUrl" value="{{URL::previous()}}">
                {{ Form::close() }} <!-- </form> --> 
                <div class="line line-xs"></div>
                <p class="mdfadecolor">
                  Don't have an account?<br>
                  <a href="/register/organisation" class="fleft underline">Register as Organisation</a>
                  <span class="fleft fadecolor"> &nbsp; | &nbsp; </span>
                  <a href="/register/individual" class="fleft underline">Register as Individual</a>
                </p>
              </div>
            </div>
            @include('admin.common._copy_center')
          </div>
        </div>
      </div>
    </section><!-- #content end -->
  </div><!-- #wrapper end -->

  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- Modal -->
  <div aria-hidden="true" aria-labelledby="resetPasswordModalLabel" role="dialog" tabindex="-1" id="resetPasswordModal" class="modal fade">
    <input type="hidden" id="hiddenUsernameToResetPassword">
    <input type="hidden" id="hiddenEmailToResetPassword">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><strong>Forgot Your Password ?</strong></h4>
        </div>
        <div class="modal-body">
          <div class="messages-area">
            <div class="alert alert-danger hide" role="alert"></div>
            <div class="alert alert-success hide" role="alert"></div>
          </div>
          <p class="nomargin">Enter your username and email address below and we will send you password reset instructions.</p>
          <br>
          <div>
            <p class="nomargin" style="margin-bottom: 5px !important;"><strong>Enter your username</strong></p>
            <input type="text" name="username_to_reset_password" placeholder="Username" autcomplete="off" class="form-control placeholder-no-fix" id="username_to_reset_password">
          </div>
          <br>
          <div>
            <p class="nomargin" style="margin-bottom: 5px !important;"><strong>Enter your e-mail address</strong></p>
            <input type="text" name="email_to_reset_password" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix" id="email_to_reset_password">
          </div>
          <br>
          <div class="clearfix">
            <div class="pull-right">
              <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
              <button class="btn btn-primary ladda-button" type="button" id="btn_reset_password" data-style="expand-right"><span class="ladda-label">Submit</span></button>
            </div>
          </div>
          <hr>
          <p>If you don't find our email, check your junk or spam folder to see if it landed there.</p>
        </div>
        {{-- <div class="modal-footer">
        </div> --}}
      </div>
    </div>
  </div><!-- // modal -->

  <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/functions2.js') }}"></script>
  {{-- <script type="text/javascript" src="{{ URL::asset('admin/js/site-alt.js') }}"></script> --}}
  <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/spin.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/common_libraries.js') }}"></script>
  {{-- <script type="text/javascript" src="{{ URL::asset('admin/js/admin/app.js') }}"></script> --}}
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/login.js') }}"></script>
</body>

@stop