@extends('layouts.admin')

@section('content')
  
  <div ng-controller="OrganisationsCtrl">

    <ul class="breadcrumb">
      <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#!"><i class="fa fa-video-camera"></i> Organisations Manager</a></li>
      <li class="active">All Organisations</li>
    </ul>

    <section class="panel">
      <header class="panel-heading">
        All Organisations
        <span class="tools pull-right">
          <a href="#!" class="fa fa-chevron-down"></a>
          <a href="#!" class="fa fa-cog"></a>
        </span>
      </header>
      <div class="panel-body">
        <div class="list-area-loader">
          <i class="fa fa-refresh fa-lg fa-spin"></i>&nbsp;&nbsp;&nbsp;fetching data..
        </div>
        <div class="list-area" style="opacity:0;">
          @include('admin.common._session_message')
          
          @if (!Auth::user()->isAdmin())
          @endif

          @if (Auth::user()->isOrganisation())
          @endif

          @if (Auth::user()->isAdmin())
          {{-- table for admin --}}
          @endif

          <div class="adv-table">
            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#dataFilters" aria-expanded="false" aria-controls="collapseExample">
              Filter By
            </button>
            <div id="dataFilters" class="collapse table-filters-area clearfix">
              <div class="">
                <div class="">
                  <div class="form-group">
                    <label>Membership Status:</label>
                    <select id="selFilterMembershipStatus" class="form-control" ng-model="selectedMembershipStatusFilter" ng-change="filterChanged()" integer>
                      <option value="-1">-- All</option>
                      <option value="0">Not Active</option>
                      <option value="1">Active</option>
                    </select>
                  </div>  
                </div>
                {{-- <div class="">
                  <div class="form-group">
                    <label>Top Status:</label>
                    <select id="selFilterTopStatus" class="form-control" ng-model="selectedTopStatusFilter" ng-change="filterChanged()" integer>
                      <option value="-1">-- All</option>
                      <option value="0">Non Top Agencies</option>
                      <option value="1">Top Agencies</option>
                    </select>
                  </div>
                </div> --}}
                <div class="">
                  <div class="form-group">
                    <label>Org. Type:</label>
                    <select id="selFilterOrgType" class="form-control" ng-model="selectedOrgTypeFilter" ng-change="filterChanged()" ng-options="orgtype as orgtype.name for orgtype in organisationsService.options.orgTypes">
                    </select>
                  </div>
                </div>
                <div class="">
                  <div class="form-group">
                    <label>Office Location:</label>
                    <select id="selFilterOfficeLocation" class="form-control" ng-model="selectedOfficeLocationFilter" ng-change="filterChanged()" ng-options="officelocation as officelocation.name for officelocation in organisationsService.options.officeLocations">
                    </select>
                  </div>
                </div>
                <div class="">
                </div>
                <div class="">
                </div>
              </div>
              <div class="form-inline">
                
              </div>
            </div>
            <table id="table_organisations" class="table table-striped table-condensed">
              <thead>
                <tr>
                  <th width="1%">Id</th>
                  <th width="">Name</th>
                  <th width="20%">Org. Type</th>
                  <th width="16%" class="text-left">Registered At</th>
                  <th width="1%" class="text-center">Active</th>
                  <th width="14%" class="text-center">Top Agency</th>
                  <th width="1%" class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><!-- // end table -->
        </div>
      </div><!-- panel-body -->
    </section>

    <div class="modal fade" id="assignTopAgencyModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <p>Toggle to set/unset organisation as Top Agency</p>
            <small>The list below shows all platforms this Organisation belongs to.</small>
            <div class="loader text-primary"><i class="fa fa-spin fa-spinner"></i> loading data..</div>
            <ul class="platform-list list-unstyled">
              
            </ul>
          </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  </div><!-- controller -->

@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_organisations.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_organisations.js') }}"></script>
  <script type="text/javascript">
    
  </script>
@stop 

