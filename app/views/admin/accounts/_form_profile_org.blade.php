@include('admin.common._form_errors')

@if (Session::has('flash_msg')) 
  <div class="alert alert-info alert-dismissible" role="alert" style="margin-bottom:0">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ Session::get('flash_msg') }}
  </div>
@endif

<p><strong>My Account</strong></p>

{{ Form::open(array('action' => 'AdminUserController@postProfile', 'files' => true, 'class' => '')) }}

  {{ Form::token() }}
  {{ Form::hidden('profile_type', '2') }}
  {{ Form::hidden('id', $model->id) }}

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('username', 'Username', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('username', $model->username, array('class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled', 'readonly' => 'readonly')) }}
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
      <div class="form-group">
        <label>&nbsp;</label>
        <div>
          <a href="#" class="btn btn-primary form-control" id="change_password" data-toggle="modal" data-target="#changePasswordModal">Change Password</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="form-group">
            {{ Form::label('first_name', 'First Name', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('first_name', $model->first_name, array('class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled', 'readonly' => 'readonly')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="form-group">
            {{ Form::label('last_name', 'Last Name', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('last_name', $model->last_name, array('class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled', 'readonly' => 'readonly')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            {{ Form::label('job_title', 'Job Title', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('job_title', $model->job_title, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('email', 'Email', array('class' => 'control-label')) }}
            <div>
              {{ Form::email('email', $model->email, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('email_confirmation', 'Retype Email', array('class' => 'control-label')) }}
            <div>
              {{ Form::email('email_confirmation', $model->email_confirmation, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="form-horizontal">
    <div class="form-group">
      <label for="organisation_logo" class="control-label col-md-2">Organisation's Logo</label>
      <div class="col-md-10">
        <div class="fileupload fileupload-new" data-provides="fileupload">
          <div class="fileupload-new thumbnail">
            {{-- <img src="/images/noimage.png" alt=""/> --}}
            <img src="{{ $model->organisation_logo }}" alt=""/>
          </div>
          <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
          <div>
            <span class="btn btn-white btn-file">
              <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select photo</span>
              <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
              <input type="file" class="default" id="organisation_logo" name="organisation_logo">
            </span>
            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
          </div>
        </div>
        <span class="label label-danger" style="display:inline-block; margin-bottom: 4px;">NOTE!</span><br/>
        <span>Attached photo thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only</span>
        <br><br>
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_name', 'Organisation Name', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::text('organisation_name', $model->organisation_name, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_email', 'Organisation Email', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::email('organisation_email', $model->organisation_email, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_website', 'Organisation Website', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::text('organisation_website', $model->organisation_website, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('office_location', 'Office Location', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::select('office_location', $lookups->office_locations, $model->office_location, array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('office_type', 'Office Location', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::select('office_type', $lookups->office_types, $model->office_type, array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_types', 'Organisation Type', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        @foreach ($lookups->organisation_types as $key => $value)
          @if ($model->organisation_types === $key) 
            {{ Form::radio('organisation_types', $key, true) }} {{ $value }}  
          @else
            {{ Form::radio('organisation_types', $key, false) }} {{ $value }}  
          @endif
          <br>
        @endforeach
      </div>
    </div>
  </div>

  <div class="pull-right">
    {{ Form::submit('Submit', array('class' => 'btn btn-success')) }}
  </div>

{{ Form::close() }}

{{-- MODALS --}}
<div id="changePasswordModal" class="modal fade" ng-controller="ChangePasswordCtrl">
  <form novalidate class="simple-form" name="form">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Change Password</h4>
        </div>
        <div class="modal-body">

          <div ng-show="custom_messages.length > 0">
            <div class="alert alert-danger" role="alert">
              <ul>
                <li ng-repeat="custom_message in custom_messages">
                  <span ng-bind="custom_messages[$index]"></span>
                </li>
              </ul>
            </div>
          </div>
          <div ng-show="changed">
            <div class="alert alert-success" role="alert">You have successfully changed your password.</div>
          </div>

          <div class="form-horizontal">

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-4 col-md-4">Current Password:</label>
              <div class="col-xs-12 col-sm-8 col-md-8">
                <input type="password" class="form-control" id="old_password" name="old_password" required ng-model="cp.oldPassword">
                <div ng-show="form.$submitted || form.old_password.$touched">
                  <small class="help-block" ng-show="form.old_password.$error.required" style="color: red">please enter current password</small>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-4 col-md-4">New Password:</label>
              <div class="col-xs-12 col-sm-8 col-md-8">
                <input type="password" class="form-control" id="new_password" name="new_password" required ng-model="cp.newPassword">
                <div ng-show="form.$submitted || form.new_password.$touched">
                  <small class="help-block" ng-show="form.new_password.$error.required" style="color: red">please enter new password</small>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-4 col-md-4">New Password (Confirm):</label>
              <div class="col-xs-12 col-sm-8 col-md-8">
                <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" required ng-model="cp.newPasswordConfirmation" compare-to="cp.newPassword">
                <div ng-show="form.$submitted || form.new_password_confirmation.$touched">
                  <small class="help-block" ng-show="form.new_password_confirmation.$error.required" style="color: red">please confirm new password</small>
                  <small class="help-block" ng-show="form.new_password_confirmation.$error.compareTo" style="color: red">password do not match.</small>
                </div>
              </div>
            </div>

          </div>
        </div><!-- / modal-body -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success ladda-button submit-change-password" data-style="expand-right" ng-click="changePassword()"><span class="ladda-label">Save changes</span></button>
        </div>
      </div>
    </div>
  </form>
</div>

