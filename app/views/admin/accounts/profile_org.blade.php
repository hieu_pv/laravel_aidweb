@extends('layouts.admin')

@section('styles')
@stop

@section('content')

  <div class="panel">
    <div class="panel-body">

      <div id="form_profile_organisation" class="form-profile">
        @if (Session::has('ok_updateProfile'))
          <div class="clearfix">
            <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="text-left">
                <p><i class="fa fa-check-circle"></i> <strong>Success!</strong> Your profile has been updated. You can see your profile page <a target="_blank" href="{{PermalinkEngine::resolveProfileUrl($model->organisation_id, Modules::profile_type_organisation())}}"><strong>here</strong></a></p>
              </div>
            </div>
          </div>
        @endif

        <div class="text-center" style="position: relative;">
          <div class="" style="position: absolute; top: 0; left: 0;">
            <a href="#" class="btn btn-primary" id="change_password" data-toggle="modal" data-target="#changePasswordModal">Change Password</a>
          </div>
          <h3>Edit Your Profile</h3>
          <p>You can edit your profile using the form below. <br> <span class="red-text">* Required fields</span></p>
        </div>

        @include('admin.common._form_errors')

        @include('admin.accounts._profile_org_form', array('show_org_confirmation' => false))

        <div style="height: 100px;"></div>

        @include('admin.accounts._change_password_modal')

      </div>

    </div>
  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {

      $(function () {

        if (typeof window._profile_org_form !== 'undefined') {
          window._profile_org_form.init();
        }
        
        $('#changePasswordModal').on('show.bs.modal', function () {
          var $rootScope = $('body').scope();
          $rootScope.$apply(function () {
            $rootScope.$broadcast('changePasswordModal.show.bs.modal');
          });
        }); 

      });

    } (jQuery));
  </script>
@stop 