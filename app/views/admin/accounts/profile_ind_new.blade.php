@extends('layouts.guests')

@section('styles')
@stop

@section('content')

  <div style="margin-top: 30px;"></div>

  <div class="text-center" style="position: relative;">
    <div class="" style="position: absolute; left: 0; top: 0;">
      <a href="/" class="btn btn-default" target="_blank"><i class="fa fa-home"></i> Go to Home Page</a>
    </div>
    <a href="/" class="retina-logo"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo" style="height:30px; margin-bottom:50px;"></a>
  </div>

  @if (Session::has('ok_updateProfile'))
    <div class="clearfix">
      <div class="alert alert-dismissible" role="alert">
        {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
        <div class="text-center">
          <h4>Congratulation, you have successfully completed your registration. <br/> From now on, you can upload contents on AidPost. To do so, you must go to "My Account"</h4>
          <p>Go to <a href="/"><strong>Home Page</strong></a> or <a href="/oadmin"><strong>My account</strong></a></p>
        </div>
      </div>
    </div>
  @endif
  
  @if (!Session::has('hideUpdateProfileForm')) 
    <div class="panel panel-default divcenter noradius noborder clearfix">
      <div class="panel-body">
        <div class="form-profile" style="padding: 0 15px;">

          <div class="text-center">
            <h2>Welcome to AidPost! Your account has been activated.</h2>
            <p>By filling out the short form below, you are creating your profile page from where AidPost visitors can be redirected to your social media pages. <br> You can edit it at any time from your Account. Once completed, you will also be set to post contents and to connect with the aid community. <br> <span class="red-text">* Required fields</span></p>
          </div>
          
          @include('admin.common._form_errors')

          @include('admin.accounts._profile_ind_form')

          <div style="height: 100px;"></div>

        </div>
      </div>
    </div>
  @endif

@stop

@section('scripts')

  <script type="text/javascript">
    (function ($) {

      $(function () {

        if (typeof window._profile_ind_form !== 'undefined') {
          window._profile_ind_form.init();
        }

      });

    } (jQuery));
  </script>

@stop 