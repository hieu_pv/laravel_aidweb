@extends('layouts.admin')

@section('styles')
@stop

@section('content')

  <div class="panel">
    <div class="panel-body">

      <div id="form_profile_organisation" class="form-profile">

        <div class="text-center" style="position: relative;">
          <h3>View Profile</h3>
        </div>

        @include('admin.accounts._profile_org_form', array('show_org_confirmation' => false))

        <div style="height: 100px;"></div>

      </div>

    </div>
  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {

      $(function () {

        if (typeof window._profile_org_form !== 'undefined') {
          window._profile_org_form.init();
        }

        $('#form_profile_organisation [type="submit"]').remove();
        $('#form_profile_organisation').attr('action', '#');
        $('#form_profile_organisation').find('input, select, textarea, radio').each(function (idx, elem) {
          $(elem).attr('disabled', 'disabled');
          $(elem).attr('readonly', 'readonly');
          $(elem).prop('disabled', true);
          $(elem).prop('readonly', true);
        });

      });

    } (jQuery));
  </script>
@stop 