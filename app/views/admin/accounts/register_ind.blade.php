@extends('layouts.master')

@section('title', 'AidPost | Connect')

@section('head')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/font-awesome/css/font-awesome.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('style.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/dark.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/font-icons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda-themeless.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/css/login.css') }}" type="text/css"/>
@stop

@section('body')

<body class="stretched grey-bg">
  <div id="wrapper" class="clearfix nobg">
    <section id="content nobg">
      <div class="content-wrap nopadding nobg">
        <div class="section nobg nomargin nopadding" style="padding-top: 30px !important;">
          <div class="container clearfix">
            <div class="row center">
              <a href="/" class="retina-logo"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo" style="height:30px; margin-bottom:30px;"></a>
            </div>
            <div class="panel panel-default divcenter noradius noborder" style="max-width: 940px">
              <div class="panel-body" style="padding: 40px;">
                <div class="row">
                  <div class="col-md-4">
                    {{ Form::open(array('action' => 'AdminRegistrationController@postRegister', 'class' => 'nobottommargin')) }}
                      {{ Form::token() }}
                      {{ Form::hidden('profile_type', Modules::profile_type_individual()) }}
                      <h3>Create an individual account.</h3>

                      <div class="messages-area">
                        @if (isset($errors) && !is_null($errors) && count($errors) > 0) 
                          <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ HTML::ul($errors->all()) }}
                          </div>
                        @endif
                      </div>

                      <div class="col_full">
                        <input type="email" class="form-control not-dark" placeholder="Email Address" autofocus required id="email" name="email" value="{{Input::old('email')}}">
                      </div>
                      <div class="col_full">
                        {{-- <input type="text"  placeholder="first name" name="login-form-username" value="" class="form-control not-dark" /> --}}
                        <input type="text" class="form-control not-dark" placeholder="Username" required id="username" name="username" value="{{Input::old('username')}}">
                      </div>
                      {{-- <div class="col_full">
                        <input type="text"  placeholder="last name" name="login-form-username" value="" class="form-control not-dark" />
                      </div> --}}
                      <div class="col_full">
                        <input type="password" class="form-control not-dark" placeholder="Password" required id="password" name="password">
                        <!-- <small><i class="fa fa-info-circle"></i> Good password should be case sensitive, contains number and symbol.</small> -->
                      </div>
                      <div class="col_full nobottommargin">
                        <button class="btn btn-danger btn-long nomargin" id="login-form-submit" name="login-form-submit" value="register">REGISTER</button><br><br>
                        <small class="mdfadecolor">By clicking this button, you agree to AidPost's <a href="/user-agreement" class="underline">User Agreement</a> and <a href="/privacy-policy" class="underline">Privacy Policy.</a></small>
                      </div>
                    </form>
                    <!-- <div class="line line-sm"></div> -->
                    <!-- <small class="mdfadecolor">
                      Not an individual? <a href="/register/organisation" class=" underline"> Register as organisation.</a><br>
                      Already have an account? <a href="/oadmin/login" class=" underline"> Sign in here.</a>
                    </small> -->
                  </div>
                  <div class="col-md-7 col-md-offset-1">
                    <h3>Connect with the aid community and its supporters worldwide.</h3>
                    <h4>Use your AidPost individual account to:</h4>
                    <ul class="iconlist">
                      <li class="list-group-item"><i class="icon-ok"></i> Publish Blogs. Write comments on postings from other members.</li>
                      <li class="list-group-item"><i class="icon-ok"></i> Elect the most popular News, Blogs, Videos to be displayed on home page.</li>
                      <li class="list-group-item"><i class="icon-ok"></i> Share your Profile. Redirect visitors to your website and social media pages.</li>
                      <li class="list-group-item"><i class="icon-ok"></i> Find peers, enlarge your professional network and build new relationships.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            @include('admin.common._copy_center')
          </div>
        </div>
      </div>
    </section><!-- #content end -->
  </div><!-- #wrapper end -->

  <div id="gotoTop" class="icon-angle-up"></div>

  <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/functions2.js') }}"></script>
  {{-- <script type="text/javascript" src="{{ URL::asset('admin/js/site-alt.js') }}"></script> --}}
  <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/spin.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/common_libraries.js') }}"></script>
  {{-- <script type="text/javascript" src="{{ URL::asset('admin/js/admin/app.js') }}"></script> --}}
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/login.js') }}"></script>

</body>

@stop