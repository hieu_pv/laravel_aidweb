<div class="form-group">
  <label for="organisation_logo" class="control-label">Organisation's logo <span class="red-text">*</span></label>
  <div class="">
    <div class="upload-avatar clearfix">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          {{-- <img src="/images/noimage.png" alt=""/> --}}
          <img src="{{ $model->organisation_logo }}" alt=""/>
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div class="fileupload-actions">
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Upload your logo</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="organisation_logo" name="organisation_logo">
          </span>
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div class="">
        <span class="label label-danger photo-note">NOTE</span><br/>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Please upload a square photo image for your logo.</span></li>
          <li><span>Minimun size: 500 x 500 pixels</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="form-group">
  <label class="control-label">Cover image</label>
  <div class="">
    <div class="upload-avatar upload-cover-image">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          <img src="{{ $model->cover_image }}" alt=""/>
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div>
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Upload cover image</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="cover_image" name="cover_image">
          </span>
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div>
        <span class="label label-danger photo-note">NOTE</span><br/>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Please upload a good resolution landscape photo for nicer presentation.</span></li>
          <li><span>Minimun size: 1400 x 600 pixels</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>