@extends('layouts.master')

@section('title', 'AidPost | Connect')

@section('head')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/font-awesome/css/font-awesome.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('style.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/dark.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/font-icons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda-themeless.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ URL::asset('admin/css/login.css') }}" type="text/css"/>
@stop

@section('body')

<body class="stretched no-transition">
  <div id="wrapper" class="clearfix">
    <section id="content">
      <div class="content-wrap nopadding">
        <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: #f1f2f7;"></div>
        <div class="section nobg full-screen nopadding nomargin">
          <div class="container vertical-middle divcenter clearfix">
            <div class="row center">
              <a href="/" class="retina-logo"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo" style="height:30px; margin-bottom:50px;"></a>
            </div>
            <div class="panel panel-default divcenter noradius noborder" style="max-width: 600px;">
              <div class="panel-body" style="padding: 40px;">
                <div class="feature-box fbox-effect">
                  <div class="fbox-icon">
                    <a href="#"><i class="icon-ok i-alt"></i></a>
                  </div>
                  <h3>Thanks for Signing Up.</h3>
                  <p>Please open the email we just sent to your email address and click <strong>Activate Account</strong>.
                    If you don't find our activation email, check your junk or spam folder to see if it landed there.<br><br>
                    Once your account is activated, we'll direct you to your profile page that needs to be filled for you to connect with the aid community and your potential supporters.</p>
                    <br>
                  </div>
                </div>
              </div>
              @include('admin.common._copy_center')
            </div>
          </div>
        </div>
      </section><!-- #content end -->
    </div><!-- #wrapper end -->

    <div id="gotoTop" class="icon-angle-up"></div>
    
    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/functions2.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ URL::asset('admin/js/site-alt.js') }}"></script> --}}
    <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/spin.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/js/admin/common_libraries.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ URL::asset('admin/js/admin/app.js') }}"></script> --}}
    <script type="text/javascript" src="{{ URL::asset('admin/js/admin/login.js') }}"></script>

  </body>

@stop