@include('admin.common._form_errors')

<div>
  <p>
    <strong>User your aidweb individual account to:</strong><br>
    <span>Post blogs and tweets. React to news, blogs, photos, and videos. Participate in polls. Share your profile. Connect with peers.</span>
  </p>
</div>

{{ Form::open(array('action' => 'AdminUserController@postRegister', 'files' => true, 'class' => '')) }}

  {{ Form::token() }}
  {{ Form::hidden('profile_type', '1') }}
  {{ Form::hidden('id', $model->id) }}

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('username', 'Username', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('username', $model->username, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('password', 'Password', array('class' => 'control-label')) }}
        <div>
          {{ Form::password('password', array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('password_confirmation', 'Retype Password', array('class' => 'control-label')) }}
        <div>
          {{ Form::password('password_confirmation', array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('first_name', 'First Name', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('first_name', $model->first_name, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('last_name', 'Last Name', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('last_name', $model->last_name, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('nationality', 'Nationality', array('class' => 'control-label')) }}
            <div>
              {{ Form::select('nationality', $lookups->nationalities, $model->nationality, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('bases', 'Based In', array('class' => 'control-label')) }}
            <div>
              {{ Form::select('bases', $lookups->bases, $model->based_in, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('email', 'Email', array('class' => 'control-label')) }}
            <div>
              {{ Form::email('email', $model->email, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('email_confirmation', 'Retype Email', array('class' => 'control-label')) }}
            <div>
              {{ Form::email('email_confirmation', $model->email_confirmation, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <strong>If Currently Employed</strong>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        {{ Form::label('job_title', 'Job Title', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('job_title', $model->job_title, array('class' => 'form-control')) }}
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        {{ Form::label('employer_name', 'Employer Name', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('employer_name', $model->employer_name, array('class' => 'form-control')) }}
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {{-- <label for="avatar" class="control-label col-md-2"></label> --}}
        <div class="col-md-10">
          <p>Please upload a high quality logo for a nicer presentation.</p>
          <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail">
              {{-- <img src="/images/noimage.png" alt=""/> --}}
              <img src="{{ $model->avatar }}" alt=""/>
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
            <div>
              <span class="btn btn-white btn-file">
                <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select photo</span>
                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                <input type="file" class="default" id="avatar" name="avatar">
              </span>
              <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
            </div>
          </div>
          <span class="label label-danger" style="display:inline-block; margin-bottom: 4px;">NOTE!</span><br/>
          <span>Attached photo thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only</span>
          <br><br>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        {{ Form::label('thought', 'Your thought of the day', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('thought', $model->thought, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        {{ Form::label('member_statuses', 'Member Status', array('class' => 'control-label')) }}
        <div>
          @foreach ($lookups->member_statuses as $key => $value)
            @if ($model->member_statuses === $key) 
              {{ Form::radio('member_statuses', $key, true) }} {{ $value }}  
            @else 
              {{ Form::radio('member_statuses', $key, false) }} {{ $value }}  
            @endif
            <br>
          @endforeach
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        {{ Form::label('professional_statuses', 'Professional Status', array('class' => 'control-label')) }}
        <div>
          @foreach ($lookups->professional_statuses as $key => $value)
            @if ($model->professional_statuses === $key) 
              {{ Form::radio('professional_statuses', $key, true) }} {{ $value }}
            @else
              {{ Form::radio('professional_statuses', $key, false) }} {{ $value }}
            @endif
            <br>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="g-recaptcha" data-sitekey="6LfGcAMTAAAAAPvWCV7dhPG-25mHyVMxwTFWRMgo"></div>

{{--   <p>
    <strong>We take your privacy seriously. By clicking FINISH REGISTRATION you hereby acknowledge that your use of aidweb.com is governed by aidweb.com Terms of Service and Privacy Policy, and you agree to be bound by the terms thereof.</strong>
  </p> --}}

  <div class="pull-right">
    {{ Form::submit('Finish Registration', array('class' => 'btn btn-success')) }}
  </div>

{{ Form::close() }}  