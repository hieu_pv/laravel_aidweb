@extends('layouts.master')

@section('title', 'AidWeb | Connect')

@section('head')
  @include('admin.common._styles')
  <link href="{{ URL::asset('admin/css/login.css') }}" rel="stylesheet"/>
  <style type="text/css" rel="stylesheet">
    a.btn-primary,
    a.btn-primary:hover {
      color: #fff;
      text-decoration: none;
    }
  </style>
@stop

@section('body')

<body>

  <div class="container">

    <div class="text-center">
      <img src="{{ URL::asset('images/logo.png') }}" alt="" style="" class="login-logo">
    </div>

    <div class="account-container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-signin">
            <div class="login-wrap">
              <div style="">
                <div class="alert alert-success text-center" role="alert">
                  <i class="fa fa-check-circle"></i> <strong>Success!</strong> Your account is verified. 
                </div>
              </div>

              <div class="text-center">
                <a href="/oadmin" class="btn btn-primary">Log In</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          
        </div>
      </div><!-- /row -->
    </div><!-- / account-container -->

  </div><!-- / container -->

</body>

@stop