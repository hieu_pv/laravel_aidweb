@include('admin.common._form_errors')

@if (Session::has('flash_msg')) 
<div class="alert alert-info alert-dismissible" role="alert" style="margin-bottom:0">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{ Session::get('flash_msg') }}
</div>
@endif

<div>
  <p>
    <strong>User your aidweb organisation account to:</strong><br>
    <span>Post news, polls, photos, take action, tweets, videos.</span>
  </p>
</div>

{{ Form::open(array('action' => 'AdminUserController@postRegister', 'files' => true, 'class' => '')) }}

  {{ Form::token() }}
  {{ Form::hidden('profile_type', '2') }}
  {{ Form::hidden('id', '0') }}

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('username', 'Username', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('username', $model->username, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('password', 'Password', array('class' => 'control-label')) }}
        <div>
          {{ Form::password('password', array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {{ Form::label('password_confirmation', 'Retype Password', array('class' => 'control-label')) }}
        <div>
          {{ Form::password('password_confirmation', array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="form-group">
            {{ Form::label('first_name', 'First Name', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('first_name', $model->first_name, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="form-group">
            {{ Form::label('last_name', 'Last Name', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('last_name', $model->last_name, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            {{ Form::label('job_title', 'Job Title', array('class' => 'control-label')) }}
            <div>
              {{ Form::text('job_title', $model->job_title, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('email', 'Email', array('class' => 'control-label')) }}
            <div>
              {{ Form::email('email', $model->email, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            {{ Form::label('email_confirmation', 'Retype Email', array('class' => 'control-label')) }}
            <div>
              {{ Form::email('email_confirmation', $model->email_confirmation, array('class' => 'form-control', 'required' => 'required')) }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="form-horizontal">
    <div class="form-group">
      <label for="organisation_logo" class="control-label col-md-2">Organisation's Logo</label>
      <div class="col-md-10">
        <p>Please upload a high quality logo for a nicer presentation.</p>
        <div class="fileupload fileupload-new" data-provides="fileupload">
          <div class="fileupload-new thumbnail">
            {{-- <img src="/images/noimage.png" alt=""/> --}}
            <img src="{{ $model->organisation_logo }}" alt=""/>
          </div>
          <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
          <div>
            <span class="btn btn-white btn-file">
              <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select photo</span>
              <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
              <input type="file" class="default" id="organisation_logo" name="organisation_logo">
            </span>
            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
          </div>
        </div>
        <span class="label label-danger" style="display:inline-block; margin-bottom: 4px;">NOTE!</span><br/>
        <span>Attached photo thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only</span>
        <br><br>
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_name', 'Organisation Name', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::text('organisation_name', $model->organisation_name, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_email', 'Organisation Email', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::email('organisation_email', $model->organisation_email, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_website', 'Organisation Website', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::text('organisation_website', $model->organisation_website, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('office_location', 'Office Location', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::select('office_location', $lookups->office_locations, $model->office_location, array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('office_type', 'Office Location', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        {{ Form::select('office_type', $lookups->office_types, $model->office_type, array('class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('organisation_types', 'Organisation Type', array('class' => 'control-label col-md-2')) }}
      <div class="col-md-10">
        @foreach ($lookups->organisation_types as $key => $value)
          @if ($model->organisation_types === $key) 
            {{ Form::radio('organisation_types', $key, true) }} {{ $value }}  
          @else
            {{ Form::radio('organisation_types', $key, false) }} {{ $value }}  
          @endif
          <br>
        @endforeach
      </div>
    </div>
  </div>

  <hr>

  <div class="clearfix">
    <div class="icheck">
      <div class="checkbox">
        <input type="checkbox" id="isVerified">
        <label>I certify being authorized by the organisation above to create an account on its behalf, and authorize aidweb to verify this statement with my employer.</label>
        <br>
        <span id="error_isVerified" class="" style="display: none;color:red">Please verify that you're authorized to create the account.</span>
      </div>
    </div>
  </div>

  <div class="g-recaptcha" data-sitekey="6LfGcAMTAAAAAPvWCV7dhPG-25mHyVMxwTFWRMgo"></div>
  
{{--   <p>
    <strong>We take your privacy seriously. By clicking FINISH REGISTRATION you hereby acknowledge that your use of aidweb.com is governed by aidweb.com Terms of Service and Privacy Policy, and you agree to be bound by the terms thereof.</strong>
  </p> --}}

  <div class="pull-right">
    {{ Form::submit('Finish Registration', array('class' => 'btn btn-success')) }}
  </div>

{{ Form::close() }}  