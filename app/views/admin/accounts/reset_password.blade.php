@extends('layouts.guests')

@section('styles')
@stop

@section('content')

  <div style="margin-top: 30px;"></div>

  <div class="text-center" style="position: relative; max-width: 600px; margin: 0 auto;">
    <div class="" style="position: absolute; left: 0; top: 0;">
      <a href="/" class="btn btn-default" target="_blank"><i class="fa fa-home"></i> Go to Home Page</a>
    </div>
    <a href="/" class="retina-logo"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo" style="height:30px; margin-bottom:50px;"></a>
  </div>

  @if (Session::has('afterReset'))

    <div class="panel" style="max-width: 600px; margin: 0 auto;">
      <div class="panel-body" style="position: relative;">
        <div class="text-center">
          <i class="fa fa-check-circle"></i> <strong>Success!</strong> <span>Your Password has been reset.</span>
          <br><br>
          <a href="/oadmin/login" class="btn btn-primary">Login</a>
        </div>
      </div>
    </div>

  @else
    @if ($ok)

      <div class="panel" style="max-width: 600px; margin: 0 auto;">
        <div class="panel-body" style="position: relative;">
          <div class="text-center">
            <h3>Reset Your Password</h3>
            <p></p>
          </div>

          @include('admin.common._form_errors')

          {{ Form::open(array('action' => 'AdminRegistrationController@doResetPassword2', 'class' => 'form-horizontal')) }}

            <input type="hidden" name="username" value="{{$passwordReminder->username}}">
            <input type="hidden" name="email" value="{{$passwordReminder->email}}">
            <input type="hidden" name="token" value="{{$passwordReminder->token}}">

            <p>&nbsp;</p>

            <div class="form-group">
              {{ Form::label('new_password', 'New Password', array('class' => 'control-label col-md-5')) }}
              <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                {{ Form::password('new_password', array('class' => 'form-control')) }}
              </div>
            </div>

            <div class="form-group">
              {{ Form::label('new_password_confirmation', 'New Password Confirmation', array('class' => 'control-label col-md-5')) }}
              <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                {{ Form::password('new_password_confirmation', array('class' => 'form-control')) }}
              </div>
            </div>
      
            <div class="pull-right">
              <button class="btn btn-success ladda-button" type="submit" id="btn_reset_password" data-style="expand-right"><span class="ladda-label">Submit</span></button>
            </div>

          {{ Form::close() }}
        </div>
      </div>

    @else

      <div class="alert alert-danger" role="alert">
        <i class="fa fa-times-circle"></i> <strong>Error!</strong>
        <ul>
        @foreach ($messages as $msg)
          <li>{{$msg}}</li>
        @endforeach
        </ul>
      </div>

    @endif
  @endif

@stop

@section('scripts')
  <script src='https://www.google.com/recaptcha/api.js'></script> 
@stop 