{{ Form::open(array('action' => 'AdminUserController@postProfile', 'files' => true, 'class' => '')) }}

  {{ Form::token() }}
  {{ Form::hidden('profile_type', Modules::profile_type_individual()) }}
  {{ Form::hidden('id', $model->id) }}

  <p>&nbsp;</p>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        {{ Form::rawLabel('first_name', 'First Name', array('class' => 'control-label')) }} <span class="red-text">*</span>
        <div>
          {{ Form::text('first_name', $model->first_name, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        {{ Form::rawLabel('last_name', 'Last Name', array('class' => 'control-label')) }} <span class="red-text">*</span>
        <div>
          {{ Form::text('last_name', $model->last_name, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        {{ Form::rawLabel('email', 'Email', array('class' => 'control-label')) }} <span class="red-text">*</span>
        <div>
          {{ Form::email('email', $model->email, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
    {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="form-group">
        {{ Form::rawLabel('email_confirmation', 'Retype Email', array('class' => 'control-label')) }}
        <div>
          {{ Form::email('email_confirmation', $model->email_confirmation, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div> --}}
     
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        {{ Form::rawLabel('nationality', 'Nationality', array('class' => 'control-label')) }} <span class="red-text">*</span>
        <div>
          {{ Form::select('nationality', $lookups->nationalities, $model->nationality, array('class' => 'form-control')) }}
          {{ Form::hidden('hdnNationality', $model->nationality) }}
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        {{ Form::rawLabel('bases', 'Based In', array('class' => 'control-label')) }} <span class="red-text">*</span>
        <div>
          {{ Form::select('bases', $lookups->bases, $model->based_in, array('class' => 'form-control')) }}
          {{ Form::hidden('hdnBasedIn', $model->based_in) }}
        </div>
      </div>
    </div>
      
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        {{ Form::rawLabel('gender', 'Gender', array('class' => 'control-label')) }} <span class="red-text">*</span>
        <div>
          {{ Form::select('gender', ['male' => 'Male', 'female' => 'Female'], $model->gender, array('class' => 'form-control')) }}
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none;">
      <div class="form-group">
        {{ Form::label('thought', 'Your thought of the day', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('thought', $model->thought, array('class' => 'form-control')) }}
        </div>
      </div>
    </div>
  </div><!-- /row -->

  <hr>

  <div class="clearfix">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @include('admin.accounts._profile_ind_photo')
      </div>
    </div><!-- /row -->
  </div>

  <hr>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="form-group">
        {{ Form::rawLabel('description', 'About You <span class="red-text">*</span>', array('class' => 'control-label')) }}
        <p>This short text will be displayed on your profile page.</p>
        <div>
          {{ Form::textarea('description', $model->description, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>
    </div>
  </div><!-- /row -->

  <hr>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="form-group">
        <p><label class="control-label">Your presence on Social Media</label></p>
        <div>
          <div class="input-group m-bot15">
            <span class="input-group-addon btn-white" style="padding:0;border:none;background:transparent;"><i class="fa fa-globe fa-2x fa-fw"></i></span>
            {{ Form::text('website_url', $model->website_url, array('class' => 'form-control')) }}
          </div>
          <div class="input-group m-bot15">
            <span class="input-group-addon btn-white" style="padding:0;border:none;background:transparent;"><i class="fa fa-facebook-square fa-2x fa-fw"></i></span>
            {{ Form::text('facebook_url', $model->facebook_url, array('class' => 'form-control')) }}
          </div>
          <div class="input-group m-bot15">
            <span class="input-group-addon btn-white" style="padding:0;border:none;background:transparent;"><i class="fa fa-twitter-square fa-2x fa-fw"></i></span>
            {{ Form::text('twitter_url', $model->twitter_url, array('class' => 'form-control')) }}
          </div>
        </div>
      </div>
    </div>
  </div><!-- /row -->

  <hr>

  <!-- <h5><strong>Desired Status</strong></h5> -->

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        {{ Form::rawLabel('member_statuses', 'Member Status <span class="red-text">*</span>', array('class' => 'control-label')) }}
        <div>
          @foreach ($lookups->member_statuses as $key => $value)
            <div class="icheck" style="margin-bottom: 5px;">
              {{ Form::radio('member_statuses', $key, ($model->member_statuses === $key), array('id' => 'choose_member_status_'.$key)) }}
              <label for="choose_member_status_{{$key}}">{{$value}}</label>
            </div>
          @endforeach
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        {{ Form::rawLabel('professional_statuses', 'Professional Status <span class="red-text">*</span>', array('class' => 'control-label')) }}
        <div>
          @foreach ($lookups->professional_statuses as $key => $value)
            <div class="icheck" style="margin-bottom: 5px;">
              {{ Form::radio('professional_statuses', $key, ($model->professional_statuses === $key), array('id' => 'choose_professional_status_'.$key)) }} 
              <label for="choose_professional_status_{{$key}}">{{ $value }}</label>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div><!-- /row -->

  <hr>

  <h5><strong>If Currently Employed</strong></h5>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        {{ Form::rawLabel('job_title', 'Job Title', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('job_title', $model->job_title, array('class' => 'form-control')) }}
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        {{ Form::rawLabel('employer_name', 'Employer Name', array('class' => 'control-label')) }}
        <div>
          {{ Form::text('employer_name', $model->employer_name, array('class' => 'form-control')) }}
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        {{ Form::rawLabel('employer_organisation_types', 'Type of Employer', array('class' => 'control-label')) }}
        <div class="">
          @foreach ($lookups->organisation_types as $key => $value)
            <div class="icheck" style="margin-bottom: 5px;">
              {{ Form::radio('employer_organisation_types', $key, ($model->employer_organisation_types === $key), array('id' => 'choose_organisation_type_'.$key)) }}
              <label for="choose_organisation_type_{{$key}}">{{ $value }}</label>
            </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        {{ Form::rawLabel('level_of_responsibilities', 'Level of Responsibility', array('class' => 'control-label')) }}
        <div class="">
          @foreach ($lookups->level_of_responsibilities as $key => $value)
            <div class="icheck" style="margin-bottom: 5px;">
              {{ Form::radio('level_of_responsibilities', $key, ($model->level_of_responsibilities === $key), array('id' => 'choose_level_of_responsibility_'.$key)) }}
              <label for="choose_level_of_responsibility_{{$key}}">{{ $value }}</label>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div><!-- /row -->     

  <p>&nbsp;</p> 

  <div class="">
    <div class="text-center">
      <button type="submit" class="btn btn-success btn-lg ladda-button submit-profile" data-style="expand-right"><span class="ladda-label">Submit</span></button>
      {{-- Form::submit('Submit', array('class' => 'btn btn-success btn-lg')) --}}
    </div>
  </div>

  <input type="hidden" id="hdnOldInputIsEmpty" value="{{ Form::oldInputIsEmpty() }}">

{{ Form::close() }}
<script type="text/javascript">
  
  window._profile_ind_form = {
    init: function () {
      $('.form-profile [name="member_statuses"], .form-profile [name="professional_statuses"], .form-profile [name="employer_organisation_types"], .form-profile [name="level_of_responsibilities"]').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });

      if ($('#hdnOldInputIsEmpty').val() === '1') {
        var n = parseInt($('input[type="hidden"][name="hdnNationality"]').val());
        if (isNaN(n) || n === 0) {
          $('select[name="nationality"]').prepend('<option selected disabled hidden value=""></option>');
        }
        var b = parseInt($('input[type="hidden"][name="hdnBasedIn"]').val());
        if (isNaN(b) || b === 0) {
          $('select[name="bases"]').prepend('<option selected disabled hidden value=""></option>');
        }
      }
    }
  };

</script>