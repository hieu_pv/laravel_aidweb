@if (Auth::check())
  <!-- user login dropdown start-->
  <li class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
      @if (Auth::user()->isIndividual())
        <img alt="" src="{{ Auth::user()->userProfile->avatar }}" class="avatar-mini">
        <span class="username">{{ Auth::user()->userProfile->fullname() }}</span>
      @elseif (Auth::user()->isOrganisation())
        <img alt="" src="{{ Auth::user()->organisation->org_logo }}" class="avatar-mini">
        <span class="username">{{ Auth::user()->organisation->org_name }}</span>
      @else 
        <img alt="" src="{{ URL::asset('images/avatar-admin.png') }}" class="avatar-mini">
        <span class="username">{{ Auth::user()->userProfile->fullname() }}</span>
      @endif
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu extended logout">
      <li><a href="{{ URL::route('admin_profile') }}"><i class=" fa fa-suitcase"></i>Profile</a></li>
      {{-- <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li> --}}
      <li><a href="{{ URL::route('admin_logout') }}"><i class="fa fa-key"></i> Log Out</a></li>
    </ul>
  </li>
  <!-- user login dropdown end -->
@else 
  <a href="/oadmin/login" class="btn btn-primary">Login</a>
@endif