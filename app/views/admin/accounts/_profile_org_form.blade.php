{{ Form::open(array('action' => 'AdminUserController@postProfile', 'files' => true, 'class' => '')) }}

  {{ Form::token() }}
  {{ Form::hidden('profile_type', Modules::profile_type_organisation()) }}
  {{ Form::hidden('id', $model->id) }}

  <div class="section">
    <div class="section-title">
      <strong class="">1. Organisation Identity</strong>
    </div>

    <div class="form-horizontal">

      <div class="form-group">
        {{ Form::rawLabel('organisation_name', 'Organisation Name <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          {{ Form::text('organisation_name', $model->organisation_name, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>

      <div class="form-group">
        {{ Form::rawLabel('organisation_email', 'Organisation Email <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          {{ Form::email('organisation_email', $model->organisation_email, array('class' => 'form-control', 'required' => 'required', 'placeholder' => 'For general enquiry')) }}
        </div>
      </div>

      <div class="form-group">
        {{ Form::rawLabel('organisation_website', 'Organisation Website', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          {{ Form::text('organisation_website', $model->organisation_website, array('class' => 'form-control')) }}
        </div>
      </div>

      <div class="form-group">
        {{ Form::rawLabel('organisation_phone', 'Organisation Phone <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          {{ Form::text('organisation_phone', $model->organisation_phone, array('class' => 'form-control', 'required' => 'required')) }}
        </div>
      </div>

      <div class="form-group">
        {{ Form::rawLabel('office_location', 'Office Location <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          {{ Form::select('office_location', $lookups->office_locations, $model->office_location, array('class' => 'form-control')) }}
          {{ Form::hidden('hdnOfficeLocation', $model->office_location) }}
        </div>
      </div>

      {{-- <div class="form-group">
        {{ Form::rawLabel('office_type', 'Office Type <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          {{ Form::select('office_type', $lookups->office_types, $model->office_type, array('class' => 'form-control')) }}
        </div>
      </div> --}}

      <div class="form-group">
        {{ Form::rawLabel('organisation_types', 'Organisation Type <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          @foreach ($lookups->organisation_types as $key => $value)
            <div class="icheck" style="margin-bottom: 5px;">
              {{ Form::radio('organisation_types', $key, ($model->organisation_types === $key), array('id' => 'choose_organisation_type_'.$key)) }}
              <label for="choose_organisation_type_{{$key}}">{{ $value }}</label>
            </div>
          @endforeach
        </div>
      </div>

      <div class="form-group">
        {{ Form::rawLabel('level_of_activities', 'Level of Activity <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-md-3')) }}
        <div class="col-xs-12 col-md-9">
          @foreach ($lookups->level_of_activities as $key => $value)
            <div class="icheck" style="margin-bottom: 5px;">
              {{ Form::radio('level_of_activities', $key, ($model->level_of_activities === $key), array('id' => 'choose_level_of_activity_'.$key)) }}
              <label for="choose_level_of_activity_{{$key}}">{{ $value }}</label>
            </div>
          @endforeach
        </div>
      </div>

    </div><!-- /form-horizontal -->
  </div><!-- /section -->

  <div class="section">
    <div class="section-title">
      <strong>2. Information &amp; material to be displayed on AidPost website</strong>
    </div>

    <div class="form-inline clearfix">
      <div class="form-group">
        <label>Organisation's name</label>
        <div class="icheck">
          <div class="checkbox">
            <span>I want AidPost to display the organisation</span>
            {{ Form::radio('use_org_name_acronym', 0, $model->use_org_name_acronym !== 1, array('id' => 'useFullname')) }}
            <span>full name or</span>
            {{ Form::radio('use_org_name_acronym', 1, $model->use_org_name_acronym === 1, array('id' => 'useAcronym')) }}
            <span>the following acronyme</span>
            {{-- <span>&nbsp;</span> --}}
            {{ Form::text('org_name_acronym', $model->org_name_acronym, array('class' => 'form-control')) }}
          </div>
        </div>
      </div>
    </div>
    <span class="red-text">Full names are at risk to be amputated, please see  the space allocated  to  the organisation names on  computer, tablets and  smart phones.</span>
    
    <hr>

    <div class="clearfix">
      @include('admin.accounts._profile_org_photo')
    </div>

    <hr>

    <div class="form-group">
      {{ Form::rawLabel('description', 'About your organisation <span class="red-text">*</span>', array('class' => 'control-label')) }}
      <p>This short text will be displayed on your profile page that visitors can access by clicking on your logo that accompanies  all your postings.</p>
      <div>
        {{ Form::textarea('description', $model->description, array('class' => 'form-control', 'required' => 'required')) }}
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <p><label class="control-label">Your presence on Social Media</label></p>
          <div>
            <div class="input-group m-bot15">
              <span class="input-group-addon btn-white" style="padding:0;border:none;background:transparent;"><i class="fa fa-facebook-square fa-2x fa-fw"></i></span>
              {{ Form::text('facebook_url', $model->facebook_url, array('class' => 'form-control')) }}
            </div>
            <div class="input-group m-bot15">
              <span class="input-group-addon btn-white" style="padding:0;border:none;background:transparent;"><i class="fa fa-twitter-square fa-2x fa-fw"></i></span>
              {{ Form::text('twitter_url', $model->twitter_url, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
      </div>
    </div><!-- /row -->

  </div><!-- /section -->

  <div class="section">
    <div class="section-title">
      <strong>3. Contact Person</strong>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
          {{ Form::label('first_name', 'First Name', array('class' => 'control-label')) }} <span class="red-text">*</span>
          <div>
            {{ Form::text('first_name', $model->first_name, array('class' => 'form-control', 'required' => 'required')) }}
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
          {{ Form::label('last_name', 'Last Name', array('class' => 'control-label')) }} <span class="red-text">*</span>
          <div>
            {{ Form::text('last_name', $model->last_name, array('class' => 'form-control', 'required' => 'required')) }}
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
          {{ Form::label('email', 'Email', array('class' => 'control-label')) }} <span class="red-text">*</span>
          <div>
            {{ Form::email('email', $model->email, array('class' => 'form-control', 'required' => 'required')) }}
          </div>
        </div>
      </div>

      {{-- <div class="col-xs-12 col-sm-12 col-md-4 hide">
        <div class="form-group">
          {{ Form::label('email_confirmation', 'Retype Email', array('class' => 'control-label')) }}
          <div>
            {{ Form::email('email_confirmation', $model->email_confirmation, array('class' => 'form-control', 'required' => 'required')) }}
          </div>
        </div>
      </div> --}}

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          {{ Form::label('job_title', 'Job Title', array('class' => 'control-label')) }}
          <div>
            {{ Form::text('job_title', $model->job_title, array('class' => 'form-control')) }}
          </div>
        </div>
      </div>

      @if ($show_org_confirmation) 
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="clearfix">
            <div class="icheck">
              <div class="checkbox padding-left-0">
                <input type="checkbox" id="isVerified">
                <label for="isVerified">I am an authorize representative of the organisation. Please tick the box if you are.</label>
                <br>
                <span id="error_isVerified" class="" style="display: none;color:red">Please verify that you're an authorized representative of the organisation.</span>
              </div>
            </div>
          </div>
        </div>
      @endif
    </div>
  </div><!-- /section -->

  <div class="">
    <div class="text-center">
      <button type="submit" class="btn btn-success btn-lg ladda-button submit-profile" data-style="expand-right"><span class="ladda-label">Submit</span></button>
      {{-- Form::submit('Submit', array('class' => 'btn btn-success btn-lg')) --}}
    </div>
  </div>

  <input type="hidden" id="hdnOldInputIsEmpty" value="{{ Form::oldInputIsEmpty() }}">

{{ Form::close() }}

<script type="text/javascript">

  window._profile_org_form = {
    init: function () {
      $('#form_profile_organisation #useAcronym, #form_profile_organisation #useFullname').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });

      $('#form_profile_organisation [name="organisation_types"], #form_profile_organisation [name="level_of_activities"]').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });

      $('#form_profile_organisation #isVerified').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });

      $('#form_profile_organisation form').submit(function (e) {
        if ($('#form_profile_organisation #isVerified').is(':visible')) {
          $('#form_profile_organisation #error_isVerified').hide();
          if ($('#form_profile_organisation #isVerified').is(':checked')) {
            return true;
          } else {
            $('#form_profile_organisation #error_isVerified').show();
            return false;
          }
        } else {
          return true;
        }
      });

      if ($('#hdnOldInputIsEmpty').val() === '1') {
        var o = parseInt($('input[type="hidden"][name="hdnOfficeLocation"]').val());
        if (isNaN(o) || o === 0) {
          $('select[name="office_location"]').prepend('<option selected disabled hidden value=""></option>');
        }
      }
    }
  }; 

</script>