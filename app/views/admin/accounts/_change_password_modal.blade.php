<div id="changePasswordModal" class="modal fade" ng-controller="ChangePasswordCtrl">
  <form novalidate class="simple-form" name="form">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Change Password</h4>
        </div>

        <div class="modal-body">

          <div ng-show="custom_messages.length > 0">
            <div class="alert alert-danger" role="alert">
              <i class="fa fa-times-circle"></i> <strong>Error!</strong>
              <ul>
                <li ng-repeat="custom_message in custom_messages">
                  <span ng-bind="custom_messages[$index]"></span>
                </li>
              </ul>
            </div>
          </div>

          <div ng-show="changed">
            <div class="alert alert-success" role="alert"><i class="fa fa-check-circle"></i> <strong>Success!</strong> You have successfully changed your password.</div>
          </div>

          <div class="form-horizontal">

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-4 col-md-4">Current Password:</label>
              <div class="col-xs-12 col-sm-8 col-md-8">
                <input type="password" class="form-control" id="old_password" name="old_password" required ng-model="cp.oldPassword">
                <div ng-show="form.$submitted || form.old_password.$touched">
                  <small class="help-block" ng-show="form.old_password.$error.required" style="color: red">please enter current password</small>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-4 col-md-4">New Password:</label>
              <div class="col-xs-12 col-sm-8 col-md-8">
                <input type="password" class="form-control" id="new_password" name="new_password" required ng-model="cp.newPassword">
                <div ng-show="form.$submitted || form.new_password.$touched">
                  <small class="help-block" ng-show="form.new_password.$error.required" style="color: red">please enter new password</small>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-4 col-md-4">New Password (Confirm):</label>
              <div class="col-xs-12 col-sm-8 col-md-8">
                <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" required ng-model="cp.newPasswordConfirmation" compare-to="cp.newPassword">
                <div ng-show="form.$submitted || form.new_password_confirmation.$touched">
                  <small class="help-block" ng-show="form.new_password_confirmation.$error.required" style="color: red">please confirm new password</small>
                  <small class="help-block" ng-show="form.new_password_confirmation.$error.compareTo" style="color: red">password do not match.</small>
                </div>
              </div>
            </div>

          </div><!--/form horizontal-->

        </div><!-- / modal-body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success ladda-button submit-change-password" data-style="expand-right" ng-click="changePassword()"><span class="ladda-label">Save changes</span></button>
        </div>
      </div><!-- /modal-content -->
    </div><!-- /modal-dialog -->
  </form>
</div><!-- /#changePasswordModal -->