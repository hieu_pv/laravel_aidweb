<div class="form-group">
  <label class="control-label">Your Photo <span class="red-text">*</span></label>
  <div class="">
    <div class="upload-avatar">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          <img src="{{ $model->avatar }}" alt=""/>
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div>
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Upload your photo</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="avatar" name="avatar">
          </span>
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div>
        <span class="label label-danger photo-note">NOTE</span><br/>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Please upload a square photo, with a light color background and a good resolution for nicer presentation.</span></li>
          <li><span>Minimun size: 500 x 500 pixels</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<p>&nbsp;</p>

<!-- <div class="form-group">
  <label class="control-label">Cover image</label>
  <div class="">
    <div class="upload-avatar upload-cover-image">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          <img src="{{ $model->cover_image }}" alt=""/>
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div>
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Upload cover image</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="cover_image" name="cover_image">
          </span>
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div>
        <span class="label label-danger photo-note">NOTE</span><br/>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Please upload a landscape photo, a good resolution for nicer presentation.</span></li>
          <li><span>The window shows what your photo will look like on the website.</span></li>
          <li><span>Image preview is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10+ only</span></li>
          <li><span>Maximum file size: 5MB</span></li>
        </ul>
      </div>
    </div>
  </div>
</div> -->