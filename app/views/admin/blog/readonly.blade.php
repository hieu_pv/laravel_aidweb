@extends('layouts.admin')

@section('styles')
  
@stop

@section('content')

  <div ng-controller="BlogPostCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-book"></i> Blog Manager</a></li>
    <li class="active">View Blog : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      View Blog : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      <p><strong>URL: </strong> <a target="_blank" href="{{PermalinkEngine::getPermalink(Modules::blog(), $model)}}" title="{{$model->title}}">{{PermalinkEngine::getPermalink(Modules::blog(), $model)}}</a></p>
      <hr>
      @include('admin.blog._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        // hacks
        $('#form_ce_blogpost [type="submit"]').remove();
        $('#form_ce_blogpost .btn-getpreviewurl').remove();
        $('#form_ce_blogpost').prev('.well').remove();
        $('#form_ce_blogpost').attr('action', '#');
        $('#form_ce_blogpost').find('input, select, textarea').each(function (idx, elem) {
          $(elem).attr('disabled', 'disabled');
          $(elem).attr('readonly', 'readonly');
          $(elem).prop('disabled', true);
          $(elem).prop('readonly', true);
        });
        CKEDITOR.on('instanceReady', function (evt) {
          CKEDITOR.instances['content'].setReadOnly(true);
        });
      });
    } (jQuery));
  </script>
@stop 