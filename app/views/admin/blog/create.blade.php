@extends('layouts.admin')

@section('styles')
  
@stop

@include('admin.common._pilot_phase')

@section('content')

  <div ng-controller="BlogPostCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-book"></i> Blog Manager</a></li>
    <li class="active">Post a Blog</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      Post a Blog
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.common._posting_within_24hours', array('category' => 'posts'))
      @if(Session::get('PostingLimitation_posts'))
        @include('admin.common._posting_limitation', ['category' => 'posts', 'time_diff' => Session::get('PostingLimitation_posts')])
      @endif
      @if (Session::get('canPostMoreThanOneIn24Hours_'.'posts', true))
        @include('admin.blog._form_ce')
      @endif
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
      });
    } (jQuery));
  </script>
@stop 