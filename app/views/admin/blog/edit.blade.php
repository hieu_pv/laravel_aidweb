@extends('layouts.admin')

@section('styles')

@stop

@section('content')

  <div ng-controller="BlogPostCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-book"></i> Blog Manager</a></li>
    <li class="active">Edit Blog : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      Edit Blog : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.blog._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
      });
    } (jQuery));
  </script>
@stop 