@extends('layouts.admin')

@section('content')
  
  <div ng-controller="IndividualsCtrl">

    <ul class="breadcrumb">
      <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#!"><i class="fa fa-video-camera"></i> Individuals Manager</a></li>
      <li class="active">All Individuals</li>
    </ul>

    <section class="panel">
      <header class="panel-heading">
        All Individuals
        <span class="tools pull-right">
          <a href="#!" class="fa fa-chevron-down"></a>
          <a href="#!" class="fa fa-cog"></a>
        </span>
      </header>
      <div class="panel-body">
        <div class="list-area-loader">
          <i class="fa fa-refresh fa-lg fa-spin"></i>&nbsp;&nbsp;&nbsp;fetching data..
        </div>
        <div class="list-area" style="opacity:0;">
          @include('admin.common._session_message')
          
          @if (!Auth::user()->isAdmin())
          @endif

          @if (Auth::user()->isOrganisation())
          @endif

          @if (Auth::user()->isAdmin())
          {{-- table for admin --}}
          @endif

          <div class="adv-table">
            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#dataFilters" aria-expanded="false" aria-controls="collapseExample">
              Filter By
            </button>
            <div id="dataFilters" class="collapse table-filters-area clearfix">
              <div class="">
                <div class="">
                  <div class="form-group">
                    <label>Membership Status:</label>
                    <select id="selFilterMembershipStatus" class="form-control" ng-model="selectedMembershipStatusFilter" ng-change="filterChanged()" integer>
                      <option value="-1">-- All</option>
                      <option value="0">Not Active</option>
                      <option value="1">Active</option>
                    </select>
                  </div>
                </div>
                {{-- <div class="">
                  <div class="form-group">
                    <label>Top Status:</label>
                    <select id="selFilterTopStatus" class="form-control" ng-model="selectedTopStatusFilter" ng-change="filterChanged()" integer>
                      <option value="-1">-- All</option>
                      <option value="0">Non Top Members</option>
                      <option value="1">Top Members</option>
                    </select>
                  </div>
                </div> --}}
                <div class="">
                  <div class="form-group">
                    <label>Nationality:</label>
                    <select id="selFilterNationality" class="form-control" ng-model="selectedNationalityFilter" ng-change="filterChanged()" ng-options="nationality as nationality.name for nationality in individualsService.options.nationalities">
                    </select>
                  </div>
                </div>
                <div class="">
                  <div class="form-group">
                    <label>Based In:</label>
                    <select id="selFilterBasedIn" class="form-control" ng-model="selectedBasedInFilter" ng-change="filterChanged()" ng-options="basedin as basedin.name for basedin in individualsService.options.bases">
                    </select>
                  </div>
                </div>
                <div class="">
                  <div class="form-group">
                    <label>Member Status:</label>
                    <select id="selFilterMemberStatus" class="form-control" ng-model="selectedMemberStatusFilter" ng-change="filterChanged()" ng-options="memberstatus as memberstatus.name for memberstatus in individualsService.options.memberStatuses">
                    </select>
                  </div>
                </div>
                <div class="">
                  <div class="form-group">
                    <label>Professional Status:</label>
                    <select id="selFilterProfessionalStatus" class="form-control" ng-model="selectedProfessionalStatusFilter" ng-change="filterChanged()" ng-options="professionalstatus as professionalstatus.name for professionalstatus in individualsService.options.professionalStatuses">
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <table id="table_individuals" class="table table-striped table-condensed">
              <thead>
                <tr>
                  <th width="1%">Id</th>
                  <th width="">Name</th>
                  {{-- <th width="20%">Org. Type</th> --}}
                  <th width="16%" class="text-left">Registered At</th>
                  <th width="1%" class="text-center">Active</th>
                  <th width="14%" class="text-center">Top Member</th>
                  <th width="1%" class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><!-- // end table -->
        </div>
      </div><!-- panel-body -->
    </section>

    <div class="modal fade" id="assignTopMemberModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <p>Toggle to set/unset individual as Top Member</p>
            <small>The list below shows all platforms this Member belongs to.</small>
            <div class="loader text-primary"><i class="fa fa-spin fa-spinner"></i> loading data..</div>
            <ul class="platform-list list-unstyled">
              
            </ul>
          </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  </div><!-- controller -->

@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_individuals.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_individuals.js') }}"></script>
  <script type="text/javascript">
    
  </script>
@stop 

