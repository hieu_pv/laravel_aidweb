@if (!Session::get('canPostMoreThanOneIn24Hours_'.$category, true))
  <div>
    <h2>Sorry...</h2>
    <p>You cannot post more than 1 posting per category (Blogs, News, Videos, Get Involved) within 24 hours.</p>
    <br>
    <?php
      $lastPost = Session::get('canPostMoreThanOneIn24Hours_lastpost_'.$category, null);
    ?>
    @if ($lastPost === null) 
      
    @else
      <span>Your last post in this category:</span> <br><br>
      <span>Id: {{$lastPost->id}}</span> <br><br>
      <span>Title: {{$lastPost->title}}</span> <br><br>
      <span>Posted: {{$lastPost->created_at}} - <strong>{{Carbon::createFromTimestamp(strtotime($lastPost->created_at))->diffForHumans()}}</strong></span>
    @endif
  </div>
@endif