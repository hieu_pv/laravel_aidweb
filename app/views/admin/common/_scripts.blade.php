<script type="text/javascript" src="{{ URL::asset('admin/js/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/site.js') }}"></script>

<!-- production -->
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/script.js') }}"></script>

<!-- dev -->
{{-- <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_publicities.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_publicities.js') }}"></script> --}}

<!-- TEMP move to file later on -->
<script type="text/javascript">
  (function ($) {
    $(function () {
      var form_names = [
        'form_ce_newsitem',
        'form_ce_blogpost',
        'form_ce_takeaction',
        'form_ce_video',
        'form_cu_publicity'
      ];
      var form_urls = [
        '/oadmin/news/getpreviewurl',
        '/oadmin/posts/getpreviewurl',
        '/oadmin/takeactions/getpreviewurl',
        '/oadmin/videos/getpreviewurl',
        '/oadmin/publicities/getpreviewurl',
      ];
      for (var i = 0; i < form_names.length; i++) {
        $(document).on('click', '#' + form_names[i] + ' .btn-getpreviewurl', function (formName, formUrl) {
          return function (e) {
            e.preventDefault();
            e.stopPropagation();
            var dis = this;
            var fd = new FormData();
            var $inputs = $('#' + formName).find(':input:not(:checkbox, :radio)');
            $inputs.each(function () {
              if (this.type !== 'file') {
                fd.append(this.name, $(this).val());
              } else {
              }
            });
            var $inputs2 = $('#' + formName).find(':checkbox:checked, :radio:checked');
            $inputs2.each(function () {
              fd.append(this.name, $(this).val());
            });
            var $files = $('#' + formName + ' :file');
            $files.each(function () {
              if (this.files.length > 0) {
                fd.append(this.name, this.files[0]);
              }
            });
            var $editors = $('#' + formName + ' textarea.ckeditor');
            $editors.each(function () {
              var inst = CKEDITOR.instances[this.id];
              if (inst) {
                fd.append(this.name, inst.getData());
              }
            });
            var l = Ladda.create(this);
            l.start();
            $.ajax({
              url: formUrl, // Url to which the request is send
              type: "POST",             // Type of request to be send, called as method
              data: fd, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
              contentType: false,       // The content type used when sending data to the server.
              // cache: false,             // To unable request pages to be cached
              processData:false,        // To send DOMDocument or non processed data file it is set to false
              success: function (response) {  // A function to be called if request succeeds
                if (response.ok) {
                  window.open(response.url, '_blank');
                }
                l.stop();
              },
              error: function () {
                l.stop();
              }
            });
            return false;
          };
        } (form_names[i], form_urls[i]));
      }
    });
  } (jQuery));
</script>

{{-- <script src="{{ URL::asset('admin/js/admin/startup.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/common_libraries.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/app.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_change_password.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_blog.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_news.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_takeactions.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_videos.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_organisations.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_individuals.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_blog.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_news.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_takeactions.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_videos.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_organisations.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_individuals.js') }}"></script> --}}