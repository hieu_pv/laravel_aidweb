{{-- <div class="form-group">
  <label for="current_image" class="control-label col-md-2">Current Photo</label>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <img src="{{ URL::asset($model->image) }}" class="img-thumbnail preview-image" id="current_image" style="width: 210px; height: auto;">
    <span class="help-block">Current post photo. Display in post listing and post details.</span>
  </div>
</div> --}}

<div class="form-group">
  <label for="image" class="control-label col-md-2">Upload Photo <span class="red-text">*</span></label>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="upload-posting-photo">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          <img src="{{ URL::asset($model->image) }}" alt=""/>
          <input type="hidden" name="original_image" value="{{$model->image}}">
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div>
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="image" name="image">
          </span>
          {{-- <a href="#!" class="btn btn-primary fileupload-browse" disabled><i class="fa fa-search"></i> Browse</a> --}}
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div>
        <span class="label label-danger photo-note">NOTE</span><br>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Image preview is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10+ only</span></li>
          <li><span>Maximum file size: 5MB</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>