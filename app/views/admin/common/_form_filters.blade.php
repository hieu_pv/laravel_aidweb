<div class="form-group clearfix">
  <label for="filters" class="control-label col-md-2">Filters</label>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="row">

      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        @if ($module === Modules::takeactions())
          <label for="region" class="control-label">Region</label>
        @else
          <label for="region" class="control-label">Region <span class="red-text">*</span></label>
        @endif
        {{ Form::select('region', $lookups->regions, $filters->originalRegion, array('class' => 'form-control')) }}
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <label for="country" class="control-label">Country</label>
        {{ Form::select('country', $lookups->countries, $filters->originalCountry, array('class' => 'form-control')) }}
      </div>

      @if ($module === Modules::takeactions())
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
          {{ Form::rawLabel('type_id', 'Action type <span class="red-text">*</span>', array('class' => 'control-label')) }}
          <div class="input-group">
            <span class="input-group-addon" ng-style="{'background-color': selectedTypeColor}">&nbsp;&nbsp;&nbsp;</span>
            {{ Form::select('type_id', $types, $model->type_id, array('class' => 'form-control', 'ng-model' => 'selectedType', 'ng-change' => 'typeChanged()', 'integer' => '')) }}
          </div>
          <script type="text/javascript">window._types_of_take_actions = {{$jtypes}}; </script>
          <script type="text/javascript">window._selected_take_action_type_id = {{$seltypeid}}; </script>
        </div>
      @else
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <label for="crisis" class="control-label">Disaster</label>
          {{ Form::select('crisis', $lookups->crisis, $filters->originalCrisis, array('class' => 'form-control')) }}
        </div>
      @endif

      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <label for="sector" class="control-label">Sector</label>
        {{ Form::select('sector', $lookups->sectors, $filters->originalSector, array('class' => 'form-control')) }}
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        @if ($module === Modules::takeactions())
          <label for="theme" class="control-label">Theme</label>
        @else
          <label for="theme" class="control-label">Theme <span class="red-text">*</span></label>
        @endif
        {{ Form::select('theme', $lookups->themes, $filters->originalTheme, array('class' => 'form-control')) }}
      </div>

      {{-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <label for="intervention" class="control-label">Intervention</label>
        {{ Form::select('intervention', $lookups->interventions, $filters->originalIntervention, array('class' => 'form-control')) }}
      </div> --}}

      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <label for="beneficiary" class="control-label">Beneficiary</label>
        {{ Form::select('beneficiary', $lookups->beneficiaries, $filters->originalBeneficiary, array('class' => 'form-control')) }}
      </div>

    </div>
  </div>
</div>