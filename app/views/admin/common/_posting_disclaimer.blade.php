<div class="well">
  <ul>
    <li>Fields with a red asterix (<span style="color: red;">*</span>) are required.</li>
    <li>Only postings in English are accepted at this point.</li>
    <li>No more than 1 posting per category per 24 hours is allowed.</li>
  </ul>
</div>