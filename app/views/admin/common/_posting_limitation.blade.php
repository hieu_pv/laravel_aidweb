<?php
	switch ($category) {
		case 'videos':
			$category = 'video';
			break;
		case 'takeactions':
			$category = 'invitation';
			break;
		case 'posts':
			$category = 'post';
			break;
		case 'publicities':
			$category = 'publicity';
			break;
		default:
			# code...
			break;
	}
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <p><strong>Sorry!</strong> <span>you cannot post more than 1 {{$category}} per week (7 days). </span></p>
  <p>You last posting was published {{($time_diff->days > 0) ? $time_diff->days.' day': ''}} {{($time_diff->days > 1) ? 's' : ''}} {{ ($time_diff->days > 0) ? ',' : '' }} {{ ($time_diff->h > 1) ? $time_diff->h.' hours' : '1 hour' }} ago. </p>
</div>