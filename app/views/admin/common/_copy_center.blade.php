<div class="row center"><small>Copyrights &copy; <span id="copyyear"></span> <a href="/">AidPost</a>. All Rights Reserved.</small></div>
<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(event) { 
    var year = new Date();
    document.querySelector('#copyyear').textContent = year.getFullYear();
  });
</script>