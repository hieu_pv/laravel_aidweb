@if (Config::get('app.pilotphase')) 
  <div class="pilot-phase-info">
    <div class="alert alert-info alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <p><strong>Pilot Phase</strong> You are in pilot phase of AidPost website. During pilot phase, you can only post one item per category (blogs, news, videos, get involved) in 24 hours.</p>
      <br>
      <strong><span>Your last posts: </span></strong>
      <ul id="last_posts">
        
      </ul>
    </div>
  </div>
  <script>
    document.addEventListener("DOMContentLoaded", function(event) { 
      $.get(window.libraries.ws_urls.common.lastPosts(), function (data) {
        for (var i = 0; i < data.lastposts.length; i++) {
          var item = data.lastposts[i];
          if (item.humandatetime === '-') {
            var html2 = '<li>' +
                       '<strong>' + item.kind + '</strong>&nbsp;:&nbsp;No record yet' +
                       '</li>';
            $('#last_posts').append(html2);
          } else {
            var html = '<li>' +
                       '<strong>' + item.kind + '</strong>&nbsp;:&nbsp;' +
                       '<span>' + item.title + '</span>&nbsp;:&nbsp;' +
                       '<span>' + item.datetime + '</span>&nbsp;:&nbsp;' +
                       '<strong>' + item.humandatetime + '</strong>' +
                       '</li>';
            $('#last_posts').append(html);
          }
        }
        $('.pilot-phase-info').fadeIn();
      });
    });
  </script>
@endif