@extends('layouts.admin')

@section('styles')
  <link href="{{ URL::asset('admin/js/bootstrap3-editable/css/bootstrap-editable.css') }}" rel="stylesheet">
  <!-- blueimp Gallery styles -->
  {{-- <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"> --}}
  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="{{ URL::asset('admin/js/file-uploader/css/jquery.fileupload.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('admin/js/file-uploader/css/jquery.fileupload-ui.css') }}">
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript>
    <link rel="stylesheet" href="{{ URL::asset('admin/js/file-uploader/css/jquery.fileupload-noscript.css') }}">
  </noscript>
  <noscript>
    <link rel="stylesheet" href="{{ URL::asset('admin/js/file-uploader/css/jquery.fileupload-ui-noscript.css') }}">
  </noscript>
@stop

@section('content')

  <section class="panel">
    <header class="panel-heading">
      Media Manager
      <span class="tools pull-right">
        <a href="javascript:;" class="fa fa-chevron-down"></a>
        <a href="javascript:;" class="fa fa-cog"></a>
        <a href="javascript:;" class="fa fa-times"></a>
      </span>
    </header>
    <div class="panel-body">

      <div class="clearfix m-bot15">
        <ul id="filters" class="media-filter">
          <li><a href="#" data-filter="*"> All</a></li>
          <li><a href="#" data-filter=".images">Images</a></li>
          <li><a href="#" data-filter=".audio">Audio</a></li>
          <li><a href="#" data-filter=".video">Video</a></li>
          <li><a href="#" data-filter=".documents">Documents</a></li>
          <li><a href="#" data-filter=".folder">Folders</a></li>
        </ul>
        <div class="btn-group pull-right media-toolbox">
          <!-- <button type="button" class="btn btn-white btn-sm"><i class="fa fa-check-square-o"></i> Select all</button> -->
          <button type="button" class="btn btn-white btn-sm btn-upload-files"><i class="fa fa-upload"></i> Upload File(s)</button>
          <button type="button" class="btn btn-white btn-sm btn-select-all"><i class="fa fa-check-square-o"></i> Select all</button>
          <button type="button" class="btn btn-white btn-sm btn-add-folder"><i class="fa fa-folder-open"></i> Add Folder</button>
          <button type="button" class="btn btn-white btn-sm btn-delete ladda-button" data-style="expand-right" data-spinner-color="#428bca" data-size="xs" data-tooltip="Selected files and directories will be deleted. Files inside directories will be deleted also. Are you sure?"><span class="ladda-label"><i class="fa fa-trash-o"></i> Delete</span></button>
        </div>
        {{-- <a href="#" type="button" class="btn pull-right btn-sm"><i class="fa fa-upload"></i> Upload New File</a> --}}
      </div>
      
      <div class="clearfix m-bot15">
        {{-- <strong class="inline-block">Path : </strong>
        <strong class="inline-block gallery-path"></strong> --}}
        <strong class="inline-block">Path:</strong>
        <ul class="breadcrumb inline-block" style="margin: 0;" id="gallery_breadcrumb">
        </ul>
        <br/>
        <small>click path to navigate / show files inside the path</small>
      </div>

      <div id="gallery" class="media-gal">
      </div>

      {{-- <div class="col-md-12 text-center clearfix">
        <ul class="pagination">
          <li><a href="#">«</a></li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">»</a></li>
        </ul>
      </div> --}}

      @include('admin.mediagallery._fileInfo_modal')
      @include('admin.mediagallery._upload_modal')

    </div>
  </section>

  <script type="text/template" id="template_add_folder">
    <div class="add-folder-modal hide">
      <form action="javascript:;" method="POST" class="form-inline text-center">
        <div class="form-group">
          <!--<label for="inputFolderName">Folder Name:</label>-->
          <input type="text" class="form-control" id="inputFolderName" placeholder="enter folder name">
        </div>
        <button type="submit" class="btn btn-success ladda-button" data-style="zoom-out"><span class="ladda-label">Save</span></button>
      </form>
    </div>
  </script>

  <script type="text/template" id="template_file_item">
    <div class="<%= fileType %> item" data-filetype="<%= fileType %>" data-fileid="<%= fileId %>">
      <% if (fileType === "images") { %>
        <a href="#!" style="background-image: url('/imagecache/r_211_auto_c<%= url %>');" class="file-action"></a>
      <% } else if (fileType === "audio") { %>
        <a href="#!" style="background-image: url(/images/icons/audio.png);" class="file-action"></a>
      <% } else if (fileType === "video") { %>
        <a href="#!" style="background-image: url(/images/icons/video.png);" class="file-action"></a>
      <% } else if (fileType === "documents") { %>
        <a href="#!" style="background-image: url(/images/icons/document.png);" class="file-action"></a>
      <% } else if (fileType === "folder") { %>
        <a href="#!" style="background-image: url(/images/icons/folder.png);" class="file-action open-folder" data-foldername="<%= fileName %>"></a>
      <% } %>
      <p><a href="javascript:;" data-type="text" data-title="edit name" data-rows="3" data-name="<%= fileName %>" class="filename"><%= fileName %></a></p>
      <div class="tool-overlay">
        <div class="icheck">
          <div class="checkbox">
            <input type="checkbox">
          </div>
        </div>
      </div>
    </div>
  </script>

@stop

@section('scripts')
  <script src="{{ URL::asset('admin/js/jquery.isotope.js') }}"></script>
  <script src="{{ URL::asset('admin/js/imagesloaded/imagesloaded.pkgd.js') }}"></script>
  <script src="{{ URL::asset('admin/js/bootstrap3-editable/js/bootstrap-editable.js') }}"></script>

  <!--Multiuple File Uploader-->
  <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/vendor/jquery.ui.widget.js') }}"></script>
  <!-- The Templates plugin is included to render the upload/download listings -->
  <script src="{{ URL::asset('admin/js/blueimp/tmpl.min.js') }}"></script>
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <script src="{{ URL::asset('admin/js/blueimp/load-image.all.min.js') }}"></script>
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <script src="{{ URL::asset('admin/js/blueimp/canvas-to-blob.min.js') }}"></script>
  <!-- blueimp Gallery script -->
  {{-- <script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script> --}}
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.iframe-transport.js') }}"></script>
  <!-- The basic File Upload plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload.js') }}"></script>
  <!-- The File Upload processing plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload-process.js') }}"></script>
  <!-- The File Upload image preview & resize plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload-image.js') }}"></script>
  <!-- The File Upload audio preview plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload-audio.js') }}"></script>
  <!-- The File Upload video preview plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload-video.js') }}"></script>
  <!-- The File Upload validation plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload-validate.js') }}"></script>
  <!-- The File Upload user interface plugin -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/jquery.fileupload-ui.js') }}"></script>
  <!-- The main application script -->
  <script src="{{ URL::asset('admin/js/file-uploader/js/main.js') }}"></script>
  <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
  <!--[if (gte IE 8)&(lt IE 10)]>
  <script src="{{ URL::asset('admin/js/file-uploader/js/cors/jquery.xdr-transport.js') }}"></script>
  <![endif]-->

  <script src="{{ URL::asset('admin/js/admin/manager_for_media_gallery.js') }}"></script>
  <script type="text/javascript">

  </script>
@stop
