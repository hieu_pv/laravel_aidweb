<!-- File Info Modal -->
<div class="modal fade" id="fileInfoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Media Info</h4>
      </div>
      <div class="modal-body row">
        <div class="col-md-12 img-modal">
          {{-- <a href="#" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit Image</a>
          <a href="#" class="btn btn-white btn-sm"><i class="fa fa-eye"></i> View Full Size</a> --}}
          <p class="mtop10"><strong>File Name:</strong> <span id="fileinfo_filename"></span></p>
          <p><strong>File Type:</strong> <span id="fileinfo_fileext"></span></p>
          <p><strong>Resolution:</strong> <span id="fileinfo_filedim"></span></p>
          <p><strong>Size:</strong> <span id="fileinfo_filesize"></span></p>
          <p><strong>URL:</strong> <span id="fileinfo_url"></span></p>
          {{-- <p><strong>Uploaded By:</strong> <a href="#">John Doe</a></p> --}}
        </div>
        <div class="col-md-12">
          <img src="" alt="" id="fileinfo_imgsrc" style="width:100%;height:auto;">
        </div>
        <div class="col-md-12 mtop10">
          <div class="pull-right">
            <button class="btn btn-danger ladda-button" data-style="expand-right" type="button" id="fileinfo_delete" data-fileid="" data-tooltip="File will be deleted. Are you sure?"><span class="ladda-label">Delete</span></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- // file info modal -->