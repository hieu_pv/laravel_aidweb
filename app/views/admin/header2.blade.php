<!--header start-->
<header class="header fixed-top clearfix">
  <!--logo start-->
  <div class="brand">
    <a href="/oadmin" class="logo"><img src="{{ URL::asset('images/logo.png') }}" alt=""></a>
    {{-- <div class="sidebar-toggle-box"><div class="fa fa-bars"></div></div> --}}
  </div>
  <!--logo end-->

  <div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
      <a href="/oadmin/login" class="btn btn-primary">Sign In</a>
    </ul>
    <!--search & user info end-->
  </div>
</header>
<!--header end-->