@include('admin.common._form_errors')

@include('admin.common._posting_disclaimer')

@if ($model->id === 0)
{{ Form::open(array('action' => 'AdminVideoController@insert', 'files' => true, 'class' => 'form-horizontal', 'ng-submit' => 'formSubmitted()', 'id' => 'form_ce_video')) }}
@else 
{{ Form::open(array('action' => 'AdminVideoController@update', 'files' => true, 'class' => 'form-horizontal', 'ng-submit' => 'formSubmitted()', 'id' => 'form_ce_video')) }}
@endif

  {{ Form::token() }}
  {{ Form::hidden('id', $model->id) }}

  @include('admin.common._form_filters', array('module' => Modules::videos()))

  <div class="form-group">
    {{ Form::rawLabel('title', 'Video Title <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::text('title', $model->title, array('class' => 'form-control', 'required' => 'required')) }}
      {{-- <span class="help-block">Video Title, displayed in the heading.</span> --}}
    </div>
  </div>

  <div class="form-group hide">
    {{ Form::rawLabel('slug', 'Slug <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      <div class="input-group">
        <span class="input-group-btn">
          <a class="btn btn-primary btnGenerateSlug" href="#!">&nbsp;<i class="fa fa-refresh"></i>&nbsp;</a>
        </span>
        {{ Form::text('slug', $model->slug, array('class' => 'form-control', 'readonly' => 'readonly', 'required' => 'required')) }}
      </div>
      <span class="help-block">Slug is generated from Title. Click the refresh button to regenerate Slug.</span>
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('description', 'Video Description', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::textarea('description', $model->description, array('class' => 'form-control', 'rows' => '5')) }}
    </div>
  </div>

  {{-- <div class="form-group">
    <label class="control-label col-md-2">Author</label>
    <div class="col-md-10 video-cu-author-area">
      <input type="hidden" name="author_obj" id="author_obj">
      <input type="text" class="select-author form-control">
      <span class="help-block">Author(s) of the video. Separate each name with comma (,).</span>
      <script type="text/javascript">window._video_cu_authors = {{$authorNames}};</script>
    </div>
  </div> --}}

  <div class="form-group">
    {{ Form::rawLabel('author', 'Author', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::textarea('author', $model->author, array('class' => 'form-control', 'rows' => '3')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::rawLabel('url', 'URL <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      <input type="text" class="form-control" ng-model="videoUrl" ng-blur="urlBlurred()" name="url" required="required">
      <span class="help-block">URL of the Video (only supports Youtube URL at the moment). Format: https://www.youtube.com/watch?v=[YOUTUBE_ID]</span>
      <script type="text/javascript">window._video_cu_url = '{{$ping->url}}'</script>
    </div>
  </div>

  <div class="form-group">
    {{ Form::rawLabel('youtube_id_DONT_POST', 'Youtube ID <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-md-10 video-cu-youtubeid-area video-cu-dynamic-generated-area">
      {{ Form::text('youtube_id_DONT_POST', $model->youtube_id, array('class' => 'form-control', 'readonly' => 'readonly', 'ng-model' => 'videoYoutubeId', 'required' => 'required')) }}
      <span class="help-block">Youtube ID, auto generated from video URL.</span>
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('displayed_duration_DONT_POST', 'Duration', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-md-10 video-cu-dynamic-generated-area">
      {{ Form::text('displayed_duration_DONT_POST', $model->displayed_duration, array('class' => 'form-control', 'readonly' => 'readonly', 'ng-model' => 'videoDuration')) }}
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-2">Video Thumbnail</label>
    <div class="col-md-10 video-cu-thumbnail-area video-cu-dynamic-generated-area">
      <span class="help-block">Video image, auto generated from video URL.</span>
      <img src="/images/noimage.png" alt="" id="video_cu_thumbnail" class="img-responsive" ng-src="<%videoThumbnail%>" style="max-width: 480px;">
    </div>
  </div>

  {{-- <div class="well">
    <p><strong>Your posting will be instantly displayed in VIDEOS section.</strong></p>
  </div> --}}

  <div class="pull-right m-bot15">
    <a href="{{ URL::route('admin_all_videos') }}" title="" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Cancel</a>
    <a href="#" class="btn btn-info btn-getpreviewurl ladda-button" data-style="expand-right"><span class="ladda-label"><i class="fa fa-desktop"></i> Preview</span></a>
    {{-- Form::submit('Submit', array('class' => 'btn btn-success')) --}}
    <button type="submit" class="btn btn-success" {{Session::has('PostingLimitation_videos') ? 'disabled' : ''}}><i class="fa fa-check"></i> Submit</button>
  </div>

{{ Form::close() }}  

