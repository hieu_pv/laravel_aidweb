@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@section('content')

  <div ng-controller="VideoCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-video-camera"></i> Videos Manager</a></li>
    <li class="active">View Video : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      View Video : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.videos._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        // hacks
        $('#form_ce_video [type="submit"]').remove();
        $('#form_ce_video .btn-getpreviewurl').remove();
        $('#form_ce_video').prev('.well').remove();
        $('#form_ce_video').attr('action', '#');
        $('#form_ce_video').find('input, select, textarea').each(function (idx, elem) {
          $(elem).attr('disabled', 'disabled');
          $(elem).attr('readonly', 'readonly');
          $(elem).prop('disabled', true);
          $(elem).prop('readonly', true);
        });
      });
    } (jQuery));
  </script>
@stop 