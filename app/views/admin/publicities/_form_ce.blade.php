@include('admin.common._form_errors')

<div class="well">
  <p><strong>Please note that:</strong></p>
  <ul>
    <li>Your publicity must be submitted at least 48 hours before the required publishing date.</li>
    <li>All publicities are displayed for 30 days and must be related to aid or societal issues.</li>
    <li>A maximum of 5 publicities (per type) are published at any given time. They are displayed in rotation, changing each time a new web page is opened.</li>
    <li>AidPost is charging a fee for publicity. After submission you will receive an invoice by email where we will ask you to confirm your placement.</li>
  </ul>
</div>

@if ($model->id === 0)
{{ Form::open(array('action' => 'AdminPublicityController@insert', 'files' => true, 'class' => 'form-horizontal', 'id' => 'form_cu_publicity')) }}
@else 
{{ Form::open(array('action' => 'AdminPublicityController@update', 'files' => true, 'class' => 'form-horizontal', 'id' => 'form_cu_publicity')) }}
@endif

<input type="hidden" name="id" value="{{$model->id}}">
{{ Form::hidden('a_image_x', $model->a_image_x) }}
{{ Form::hidden('b_image_x', $model->b_image_x) }}
{{ Form::hidden('c_image_x', $model->c_image_x) }}

<div class="">
  <div class="form-group" style="margin-right:0;margin-left:0;">
    <div class="m-bot15 col-md-2">
      {{ Form::rawLabel('type_id', 'Type of Publicity <span class="red-text">*</span> (choose one)', array('class' => 'control-label')) }}
    </div>
    <div class="col-md-10 clearfix">
      <div class="row">
      @foreach ($types as $type)
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="icheck clearfix">
            <div class="checkbox">
              @if ($type->id === $ping->type_id)
                <input type="radio" id="choose_pub_type_{{$type->id}}" name="type_id" ng-model="selectedType" value="{{$type->id}}" ng-change="typeChanged()">
              @else
                <input type="radio" id="choose_pub_type_{{$type->id}}" name="type_id" ng-model="selectedType" value="{{$type->id}}" ng-change="typeChanged()">
              @endif
              <label for="choose_pub_type_{{$type->id}}"><strong class="text-uppercase">{{$type->name}}</strong></label>
              {{-- &nbsp;
              <a href="#"><i class="fa fa-question-circle"></i></a> --}}
            </div>
          </div>
          <div>
            <a href="#!"><img src="{{URL::asset('admin/images/pub_type_')}}{{$type->id}}.jpg" alt="" class="thumbnail img-responsive"></a>
          </div>
        </div>
      @endforeach
      </div>
    </div>
    <script type="text/javascript">window._selected_publicity_type = {{$ping->type_id}};</script>
    {{-- <script type="text/javascript">window._types_of_publicities = {{$jtypes}}; </script> --}}
  </div>
{{--   <div>Title Color: <% selectedTitleColor %></div>
  <div>Text Color: <% selectedTextColor %></div>
  <div>Button Text Color: <% selectedButtonTextColor %></div> --}}
</div>

<div role="tabpanel">
  <ul class="nav nav-tabs hide" role="tablist" id="pub_type_tabs">
    <li role="presentation"><a href="#pub_type_1" aria-controls="" role="tab" data-toggle="tab"></a></li>
    <li role="presentation"><a href="#pub_type_2" aria-controls="" role="tab" data-toggle="tab"></a></li>
    <li role="presentation"><a href="#pub_type_3" aria-controls="" role="tab" data-toggle="tab"></a></li>
  </ul>

  <div class="form-group">
    {{ Form::rawLabel('name', 'Name <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      {{ Form::text('name', $model->name, array('class' => 'form-control')) }}
      <span class="help-block">The name of the publicity, for system purpose (not displayed to website visitors).</span>
    </div>
  </div>

  <div class="form-group">
    <!-- Start published date -->
    {{ Form::rawLabel('published_start_date', 'Define first day of publication <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2')) }}
    <div class="col-xs-12 col-sm-12 col-md-2">
      <div class="date dp_published_start_date" data-date-format="dd-mm-yyyy" data-date="{{ $model->published_start_date }}">
        {{ Form::text('published_start_date', $model->published_start_date, array('class' => 'form-control', 'readonly' => 'readonly', 'required' => 'required')) }}
        <span class="input-group-btn add-on">
          <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>

    <!-- Num of Months -->
    {{ Form::rawLabel('num_of_months', 'Number of months <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-md-offset-1')) }}
    <div class="col-xs-12 col-sm-12 col-md-2">
      {{ Form::select('num_of_months', [1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], $model->num_of_months, array('class' => 'form-control', 'required' => 'required')) }}
    </div>
  </div>

  <div class="form-group">
    <!-- selected platforms -->
    <label for="selSelectedPlatforms" class="control-label col-xs-12 col-sm-12 col-md-2">
      Select platforms <span class="red-text">*</span> 
    </label>
    <div class="col-xs-12 col-sm-12 col-md-10">
      <input type="hidden" value="{{'['.implode(",", $ping->selected_platforms).']'}}" id="hdnSelectedPlatforms">
      <select id="selSelectedPlatforms" name="selected_platforms[]" class="form-control" multiple="multiple" required="required">
        <option value="{{Modules::all_platforms()}}">All Platforms</option>
        @foreach ($platforms as $platform)
          <option value="{{$platform->id}}">{{$platform->display_text}}</option>
        @endforeach
      </select>
    </div>
    {{-- <div class="col-xs-12 col-sm-12 col-md-2">
      <span>Selection: </span>
      <span class="platform-selection">N/A</span>
    </div> --}}
  </div>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade" id="pub_type_1">
      @include('admin.publicities._pub_type_1')
    </div>

    <div role="tabpanel" class="tab-pane fade" id="pub_type_2">
      @include('admin.publicities._pub_type_2')
    </div>

    <div role="tabpanel" class="tab-pane fade" id="pub_type_3">
      @include('admin.publicities._pub_type_3')
    </div>
  </div>
</div>

{{-- <div class="well">
  <p><strong>Your Publicity will be displayed after all the prerequisites are met.</strong></p>
</div> --}}

<div class="pull-right m-bot15">
  <a href="{{ URL::route('admin_all_publicities') }}" title="" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Cancel</a>
  <a href="#" class="btn btn-info btn-getpreviewurl ladda-button" data-style="expand-right"><span class="ladda-label"><i class="fa fa-desktop"></i> Preview</span></a>
  {{-- Form::submit('Submit', array('class' => 'btn btn-success')) --}}
  <button type="submit" class="btn btn-success" {{Session::has('PostingLimitation_publicities') ? 'disabled': ''}}><i class="fa fa-check"></i> Submit</button>
</div>

{{ Form::close() }}