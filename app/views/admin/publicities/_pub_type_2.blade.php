<div class="form-group" style="display: none !important;">
  {{ Form::rawLabel('b_title', 'Title <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    {{ Form::text('b_title', $model->b_title, array('class' => 'form-control', 'dummy_required' => 'dummy_required')) }}
  </div>
</div>

<div class="form-group">
  {{ Form::rawLabel('b_url', 'URL <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    {{ Form::text('b_url', $model->b_url, array('class' => 'form-control', 'dummy_required' => 'dummy_required')) }}
    <span class="help-block">Your website page where you want to redirect visitors.</span>
  </div>
</div>

<div class="form-group">
  <label for="b_image" class="control-label col-md-2">Upload Photo <span class="red-text">*</span></label>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="upload-posting-photo">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          <img src="{{ URL::asset($model->b_image) }}" alt=""/>
          <input type="hidden" name="b_image_original_image" value="{{ $model->b_image }}">
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div>
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select photo</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="b_image" name="b_image">
          </span>
          {{-- <a href="#!" class="btn btn-primary fileupload-browse" disabled><i class="fa fa-search"></i> Browse</a> --}}
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div>
        <span class="label label-danger photo-note">NOTE</span><br/>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Image preview is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10+ only</span></li>
          <li><span>Maximum file size: 5MB</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>