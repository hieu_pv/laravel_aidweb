@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@section('content')

  <div ng-controller="PublicityCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-picture-o"></i> Publicities Manager</a></li>
    <li class="active">Create Publicity</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      Create Publicity
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @if(Session::has('PostingLimitation_publicities')) 
        @include('admin.common._posting_limitation', ['category' => 'publicities', 'time_diff' => Session::get('PostingLimitation_publicities')])
      @endif
      @include('admin.publicities._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_publicities.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_publicities.js') }}"></script>
  <script type="text/javascript">
    (function ($) {
      $(function () {
        
      });
    } (jQuery));
  </script>
@stop 