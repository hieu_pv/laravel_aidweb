<div class="form-group">
  {{ Form::rawLabel('a_title', 'Title', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    {{ Form::text('a_title', $model->a_title, array('class' => 'form-control', 'dummy_required' => 'dummy_required')) }}
    <span class="help-block">The big text displayed to website visitors.</span>
  </div>
</div>

<div class="form-group" style="display: none !important;">
  {{ Form::rawLabel('a_title_color', 'Title Color <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-md-10 clearfix">
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_title_color_1" name="a_title_color" ng-model="selectedTitleColor" value="#ffffff">
        <label for="choose_title_color_1"><strong>White</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_title_color_2" name="a_title_color" ng-model="selectedTitleColor" value="#222222">
        <label for="choose_title_color_2"><strong>Black</strong></label>
      </div>
    </div>
  </div>
  <script type="text/javascript">window._selected_title_color = '{{$ping->a_title_color}}';</script>
</div>

<div class="form-group">
  {{ Form::rawLabel('a_text', 'Text', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    {{ Form::textarea('a_text', $model->a_text, array('class' => 'form-control', 'dummy_required' => 'dummy_required', 'rows' => '5')) }}
    <span class="help-block">The small text (paragraph) displayed to website visitors.</span>
  </div>
</div>

<div class="form-group">
  {{ Form::rawLabel('a_text_color', 'Text Color', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-md-10 clearfix">
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_text_color_1" name="a_text_color" ng-model="selectedTextColor" value="#ffffff">
        <label for="choose_text_color_1"><strong>White</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_text_color_2" name="a_text_color" ng-model="selectedTextColor" value="#222222">
        <label for="choose_text_color_2"><strong>Black</strong></label>
      </div>
    </div>
    <div class="clearfix"></div>
    <span class="help-block">The color of text in the publicity (for both big and small text).</span>
  </div>
  <script type="text/javascript">window._selected_text_color = '{{$ping->a_text_color}}';</script>
</div>

<div class="form-group">
  {{ Form::rawLabel('a_button_text', 'Button Text <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    {{ Form::text('a_button_text', $model->a_button_text, array('class' => 'form-control', 'dummy_required' => 'dummy_required')) }}
    <span class="help-block">The text inside the button.</span>
  </div>
</div>

<div class="form-group" style="display: none !important;">
  {{ Form::rawLabel('a_button_text_color', 'Button Text Color', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-md-10 clearfix">
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_button_text_color_1" name="a_button_text_color" ng-model="selectedButtonTextColor" value="#ffffff">
        <label for="choose_button_text_color_1"><strong>White</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_button_text_color_2" name="a_button_text_color" ng-model="selectedButtonTextColor" value="#222222">
        <label for="choose_button_text_color_2"><strong>Black</strong></label>
      </div>
    </div>
    <div class="clearfix"></div>
    <span class="help-block">The color of text inside the button.</span>
  </div>
  <script type="text/javascript">window._selected_button_text_color = '{{$ping->a_button_text_color}}';</script>
</div>

<div class="form-group">
  {{ Form::rawLabel('a_url', 'URL <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    {{ Form::text('a_url', $model->a_url, array('class' => 'form-control', 'dummy_required' => 'dummy_required')) }}
    <span class="help-block">Your website page where you want to redirect visitors.</span>
  </div>
</div>

<div class="form-group">
  {{ Form::rawLabel('a_show_org_logo', 'Show Logo <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-md-10 clearfix">
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_show_org_logo" name="a_show_org_logo" ng-model="showOrgLogo" value="1">
        <label for="choose_show_org_logo"><strong>Yes</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_hide_org_logo" name="a_show_org_logo" ng-model="showOrgLogo" value="0">
        <label for="choose_hide_org_logo"><strong>No</strong></label>
      </div>
    </div>
    <div class="clearfix"></div>
    <span class="help-block">Show or hide organisation's logo.</span>
  </div>
  <script type="text/javascript">window._show_org_logo = '{{$ping->a_show_org_logo}}';</script>
</div>

<div class="form-group">
  {{ Form::rawLabel('a_position', 'Content Position <span class="red-text">*</span>', array('class' => 'control-label col-xs-12 col-sm-12 col-md-2 col-lg-2')) }}
  <div class="col-md-10 clearfix">
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_position_1" name="a_position" ng-model="selectedPosition" value="LeftTop">
        <label for="choose_position_1"><strong>Left Top</strong></label>
      </div>
    </div>
    <div class="icheck pull-left" style="display: none !important;">
      <div class="checkbox">
        <input type="radio" id="choose_position_2" name="a_position" ng-model="selectedPosition" value="LeftMiddle">
        <label for="choose_position_2"><strong>Left Middle</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_position_3" name="a_position" ng-model="selectedPosition" value="LeftBottom">
        <label for="choose_position_3"><strong>Left Bottom</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_position_4" name="a_position" ng-model="selectedPosition" value="RightTop">
        <label for="choose_position_4"><strong>Right Top</strong></label>
      </div>
    </div>
    <div class="icheck pull-left" style="display: none !important;">
      <div class="checkbox">
        <input type="radio" id="choose_position_5" name="a_position" ng-model="selectedPosition" value="RightMiddle">
        <label for="choose_position_5"><strong>Right Middle</strong></label>
      </div>
    </div>
    <div class="icheck pull-left">
      <div class="checkbox">
        <input type="radio" id="choose_position_6" name="a_position" ng-model="selectedPosition" value="RightBottom">
        <label for="choose_position_6"><strong>Right Bottom</strong></label>
      </div>
    </div>
    <div class="clearfix"></div>
    <span class="help-block">The location of the text block.</span>
  </div>
  <script type="text/javascript">window._selected_position = '{{$ping->a_position}}';</script>
</div>

<div class="form-group">
  <label for="a_image" class="control-label col-md-2">Upload Photo <span class="red-text">*</span></label>
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="upload-posting-photo">
      <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail">
          <img src="{{ URL::asset($model->a_image) }}" alt=""/>
          <input type="hidden" name="a_image_original_image" value="{{ $model->a_image }}">
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 20px;"></div>
        <div>
          <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select photo</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
            <input type="file" class="default" id="a_image" name="a_image">
          </span>
          {{-- <a href="#!" class="btn btn-primary fileupload-browse" disabled><i class="fa fa-search"></i> Browse</a> --}}
          <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
        </div>
      </div>
      <div>
        <span class="label label-danger photo-note">NOTE</span><br/>
        <ul class="inline-block" style="max-width: 600px;">
          <li><span>Image preview is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10+ only</span></li>
          <li><span>Maximum file size: 5MB</span></li>
        </ul>
      </div>
    </div>
  </div>
</div>