@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@section('content')

  <div ng-controller="PublicityCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-picture-o"></i> Publicities Manager</a></li>
    <li class="active">View Publicity : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      View Publicity : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.publicities._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/controllers_for_publicities.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/admin/manager_for_publicities.js') }}"></script>
  <script type="text/javascript">
    (function ($) {
      $(function () {
        // hacks
        $('#form_cu_publicity [type="submit"]').remove();
        $('#form_cu_publicity .btn-getpreviewurl').remove();
        $('#form_cu_publicity').prev('.well').remove();
        $('#form_cu_publicity').attr('action', '#');
        $('#form_cu_publicity').find('input, select, textarea').each(function (idx, elem) {
          $(elem).attr('disabled', 'disabled');
          $(elem).attr('readonly', 'readonly');
          $(elem).prop('disabled', true);
          $(elem).prop('readonly', true);
        });
      });
    } (jQuery));
  </script>
@stop 