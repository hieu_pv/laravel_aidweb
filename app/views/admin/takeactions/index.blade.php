@extends('layouts.admin')

@section('content')
  
  <div ng-controller="TakeActionsCtrl">

    <ul class="breadcrumb">
      <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#!"><i class="fa fa-exclamation-circle"></i> Get Involved Manager</a></li>
      <li class="active">All Get Involved</li>
    </ul>

    <section class="panel">
      <header class="panel-heading">
        All Get Involved
        <span class="tools pull-right">
          <a href="#!" class="fa fa-chevron-down"></a>
          <a href="#!" class="fa fa-cog"></a>
        </span>
      </header>
      <div class="panel-body">
        <div class="list-area-loader">
          <i class="fa fa-refresh fa-lg fa-spin"></i>&nbsp;&nbsp;&nbsp;fetching data..
        </div>
        <div class="list-area" style="opacity:0;">
          @include('admin.common._session_message')
          
          <div class="clearfix">
            @if (!Auth::user()->isAdmin())
            <div class="btn-group">
              <a id="btn_add_takeaction" class="btn btn-primary" href="{{ URL::route('admin_create_takeaction') }}"><i class="fa fa-plus v-align"></i> Add Get Involved Invitation</a>
            </div>
            @endif
            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#dataFilters" aria-expanded="false" aria-controls="collapseExample">
              Filter By
            </button>
          </div><!-- // end toolbar -->

          @if (Auth::user()->isOrganisation())
          @endif

          @if (Auth::user()->isAdmin())
          {{-- table for admin --}}
          @endif

          <div class="adv-table">
            <div id="dataFilters" class="collapse table-filters-area clearfix">
              <div class="">
                @if (Auth::user()->isAdmin())
                <div class="">
                  <div class="form-group">
                    <label>Author:</label>
                    <select id="selFilterAuthor" class="form-control" ng-model="selectedAuthor" ng-change="filterChanged()" ng-options="author as author.org_name for author in takeActionsService.options.authors">
                    </select>
                  </div>
                </div>
                @endif
                <div class="">
                  <div class="form-group">
                    <label>Published:</label>
                    <select id="selFilterPublished" class="form-control" ng-model="selectedPublishedFilter" ng-change="filterChanged()" integer>
                      <option value="-1">-- All</option>
                      <option value="0">Not Published</option>
                      <option value="1">Published</option>
                    </select>
                  </div>
                </div>
                <div class="">
                  <div class="form-group">
                    <label>Type:</label>
                    <select id="selFilterType" class="form-control" ng-model="selectedType" ng-change="filterChanged()" ng-options="type as type.name for type in takeActionsService.options.types">
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <table id="table_takeactions" class="table table-striped table-condensed">
              <thead>
                <tr>
                  <th width="1%">Id</th>
                  <th width="">Title</th>
                  <th width="16%">Author</th>
                  <th width="12%">Type</th>
                  <th width="16%" class="text-left">Created</th>
                  <th width="16%" class="text-left">Updated</th>
                  <th width="1%" class="text-center">Published</th>
                  <th width="1%" class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><!-- // end table -->
        </div>
      </div><!-- panel-body -->
    </section>

  </div><!-- controller -->

@stop

@section('scripts')
  <script type="text/javascript">
    
  </script>
@stop 

