@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@section('content')

  <div ng-controller="TakeActionCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-exclamation-circle"></i> Take Actions (Get Involved) Manager</a></li>
    <li class="active">View Get Involved Invitation : {{ $model->title }}</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      View Get Involved Invitation : {{ $model->title }}
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      {{-- <p><strong>Generated Permalink: </strong> <a target="_blank" href="{{PermalinkEngine::getPermalink(Modules::takeactions(), $model)}}" title="{{$model->title}}">{{PermalinkEngine::getPermalink(Modules::takeactions(), $model)}}</a></p>
      <hr> --}}
      @include('admin.takeactions._form_ce')
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        // hacks
        $('#form_ce_takeaction [type="submit"]').remove();
        $('#form_ce_takeaction .btn-getpreviewurl').remove();
        $('#form_ce_takeaction').prev('.well').remove();
        $('#form_ce_takeaction').attr('action', '#');
        $('#form_ce_takeaction').find('input, select, textarea').each(function (idx, elem) {
          $(elem).attr('disabled', 'disabled');
          $(elem).attr('readonly', 'readonly');
          $(elem).prop('disabled', true);
          $(elem).prop('readonly', true);
        });
      });
    } (jQuery));
  </script>
@stop 