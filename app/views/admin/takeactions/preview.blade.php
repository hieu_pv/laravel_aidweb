@extends('layouts.admin')

@section('content')
  <div class="">

    <div class="panel">

      <div class="panel-heading">
        <strong>Posting Preview</strong>
      </div>

      <div class="panel-body">
        <div class="pull-left">
          @if ($model->published)
            {{ Form::open(array('action' => array('AdminTakeActionController@fpUnpublish', $model->id), 'files' => true, 'class' => 'form-horizontal')) }}
            <button type="submit" class="btn btn-warning unpublish-this">Un-Publish Get Involved</button>
            {{ Form::close() }}
          @else
            {{ Form::open(array('action' => array('AdminTakeActionController@fpPublish', $model->id), 'files' => true, 'class' => 'form-horizontal')) }}
            <button type="submit" class="btn btn-primary publish-this">Publish Get Involved</button>
            {{ Form::close() }}
          @endif
        </div>
        <div class="clearfix"></div>
        <hr>

        <h3>Posting Information<strong></strong></h3>
        <p>
          <strong>Status: </strong>
          @if ($model->published)
            <h5 class="text-success"><strong><i class="fa fa-arrow-up"></i> Published</strong></h5>
          @else
            <h5 class="text-warning"><strong><i class="fa fa-arrow-down"></i> Not Published</strong></h5>
          @endif
        </p>
        <p>
          <strong>Author: </strong>
          <span>{{$model->author}}</span>
        </p>
        <p><strong>Created At: </strong> <span>{{$model->created_at}}</span></p>
        <p><strong>Updated At: </strong> <span>{{$model->updated_at}}</span></p>
        <p><strong>Type: <span style="color: {{$model->typeOfTakeAction->color}}">{{$model->typeOfTakeAction->name}}</span></strong></p>
        <hr>

        <h3>Title<strong></strong></h3>
        <h1>{{$model->title}}</h1>
        <hr>

        <h3>URL</h3>
        {{-- <p><strong>Generated Slug: </strong> <span>{{$model->slug}}</span></p> --}}
        {{-- <p><strong>Generated Permalink: </strong> <a target="_blank" href="{{PermalinkEngine::getPermalink(Modules::takeactions(), $model)}}" title="{{$model->title}}">{{PermalinkEngine::getPermalink(Modules::takeactions(), $model)}}</a></p> --}}
        <p><strong>Target URL: </strong> <span><a href="{{$model->url}}" target="_blank">{{$model->url}}</a></span></p>
        <hr>

        <h3>Image</h3>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <img src="{{$model->image}}" alt="{{$model->image_legend}}" class="img-responsive">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            
          </div>
        </div>
        <hr>

        <h3>Content</h3>
        <div>
          {{$model->content}}
        </div>

        <hr>
        <div class="pull-left">
          @if ($model->published)
            {{ Form::open(array('action' => array('AdminTakeActionController@fpUnpublish', $model->id), 'files' => true, 'class' => 'form-horizontal')) }}
            <button type="submit" class="btn btn-warning unpublish-this">Un-Publish Get Involved</button>
            {{ Form::close() }}
          @else
            {{ Form::open(array('action' => array('AdminTakeActionController@fpPublish', $model->id), 'files' => true, 'class' => 'form-horizontal')) }}
            <button type="submit" class="btn btn-primary publish-this">Publish Get Involved</button>
            {{ Form::close() }}
          @endif
        </div>
        <div class="clearfix"></div>
      </div><!-- /panel-body -->

    </div><!-- /panel -->

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
  </script>
@stop 

