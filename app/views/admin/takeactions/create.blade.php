@extends('layouts.admin')

@section('styles')
  <style type="text/css">

  </style>
@stop

@include('admin.common._pilot_phase')

@section('content')

  <div ng-controller="TakeActionCreateUpdateCtrl">

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-exclamation-circle"></i> Get Involved Manager</a></li>
    <li class="active">Post a Get Involved Invitation</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">
      Post a Get Involved Invitation
      <span class="tools pull-right">
        <a href="#!" class="fa fa-chevron-down"></a>
        <a href="#!" class="fa fa-cog"></a>
      </span>
    </header>
    <div class="panel-body">
      @include('admin.common._posting_within_24hours', array('category' => 'takeactions'))
      @if(Session::get('PostingLimitation_takeactions'))
        @include('admin.common._posting_limitation', ['category' => 'takeactions', 'time_diff' => Session::get('PostingLimitation_takeactions')])
      @endif
      @if (Session::get('canPostMoreThanOneIn24Hours_'.'takeactions', true))
        @include('admin.takeactions._form_ce')
      @endif
    </div>
  </section>

  </div>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {
      $(function () {
        
      });
    } (jQuery));
  </script>
@stop 