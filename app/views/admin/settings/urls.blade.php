@extends('layouts.admin')

@section('content')

  <ul class="breadcrumb">
    <li><a href="{{ URL::route('admin_dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#!"><i class="fa fa-gear"></i> Settings</a></li>
    <li class="active">URLS</li>
  </ul>

  <section class="panel">
    <header class="panel-heading">URLS</header>
    
    <div class="panel-body">
      @include('admin.common._session_message')

      {{ Form::open(array('action' => 'AdminSettingsController@store', 'class' => 'form-horizontal')) }}
        @foreach ($urls as $url)
          <div class="form-group">
            <label for="{{ $url->key }}" class="control-label col-md-2"><strong>{{ $url->key }}</strong></label>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
              <div class="detect-changes">
                <input type="text" class="form-control" id="{{ $url->key }}" name="{{ $url->key }}" value="{{ $url->value }}" required>
                <span class="help-block">URL prefix to view a post. example: value "journal" will generate URL "://host/journal/(slug)"</span>
                <span class="changed-info label label-danger">changed</span>
              </div>
            </div>
          </div>
        @endforeach

        <div class="pull-right m-bot15">
          {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
        </div>
      </form>
    </div>
  </section>

@stop

@section('scripts')
  <script type="text/javascript">
    (function ($) {

      $(function () {

        $('.detect-changes input[type="text"]').each(function (idx, elem) {
          var $elem = $(elem);
          $elem.data('oldVal', $elem.val());
          $elem.bind('propertychange change click keyup input paste', function (e) {
            $elem.closest('.detect-changes').removeClass('changed');
            if ($elem.data('oldVal') !== $elem.val()) {
              $elem.closest('.detect-changes').addClass('changed');
           }
          });
        });

      });

    } (jQuery));
  </script>
@stop 