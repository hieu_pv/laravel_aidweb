<div class="row">
  <div class="col-md-8">
    <!--earning graph start-->
    <section class="panel">
      <header class="panel-heading">
        Site Visitors
        <span class="tools pull-right">
          <a href="javascript:;" class="fa fa-chevron-down"></a>
          <a href="javascript:;" class="fa fa-cog"></a>
          <a href="javascript:;" class="fa fa-times"></a>
        </span>
      </header>
      <div class="panel-body">
        <div class="chartJS">
          <canvas id="line-chart-js" height="250" width="800" ></canvas>
        </div>
        <br>
      </div>
    </section>
    
    <!--earning graph end-->
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <!--widget start-->
    <aside class="profile-nav alt">
      <section class="panel">
        <div class="user-heading alt gray-bg">
          <a href="#">
            <img alt="" src="images/lock_thumb.jpg">
          </a>
          <h1>Jim Doe</h1>
          <p>Project Manager</p>
        </div>

        <ul class="nav nav-pills nav-stacked">
          <li><a href="javascript:;"> <i class="fa fa-envelope-o"></i> Mail Inbox <span class="badge label-success pull-right r-activity">10</span></a></li>
          <li><a href="javascript:;"> <i class="fa fa-tasks"></i> Recent Activity <span class="badge label-danger pull-right r-activity">15</span></a></li>
          <li><a href="javascript:;"> <i class="fa fa-bell-o"></i> Notification <span class="badge label-success pull-right r-activity">11</span></a></li>
          <li><a href="javascript:;"> <i class="fa fa-comments-o"></i> Message <span class="badge label-warning pull-right r-activity">03</span></a></li>
        </ul>

      </section>
    </aside>
    <!--widget end-->
    
    
  </div>
</div>

<br> 
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading">
        General Table
        <span class="tools pull-right">
          <a href="javascript:;" class="fa fa-chevron-down"></a>
          <a href="javascript:;" class="fa fa-cog"></a>
          <a href="javascript:;" class="fa fa-times"></a>
        </span>
      </header>
      <div class="panel-body">
        <table class="table  table-hover general-table">
          <thead>
            <tr>
              <th> Company</th>
              <th class="hidden-phone">Descrition</th>
              <th>Profit</th>
              <th>Status</th>
              <th>Progress</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">Graphics</a></td>
              <td class="hidden-phone">Lorem Ipsum dorolo imit</td>
              <td>1320.00$ </td>
              <td><span class="label label-info label-mini">Due</span></td>
              <td>
                <div class="progress progress-striped progress-xs">
                  <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success">
                    <span class="sr-only">40% Complete (success)</span>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#">
                  ThemeBucket
                </a>
              </td>
              <td class="hidden-phone">Lorem Ipsum dorolo</td>
              <td>556.00$ </td>
              <td><span class="label label-warning label-mini">Due</span></td>
              <td>
                <div class="progress progress-striped progress-xs">
                  <div style="width: 70%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-danger">
                    <span class="sr-only">70% Complete (success)</span>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#">
                  XYZ
                </a>
              </td>
              <td class="hidden-phone">Lorem Ipsum dorolo</td>
              <td>13240.00$ </td>
              <td><span class="label label-success label-mini">Paid</span></td>
              <td>
                <div class="progress progress-striped progress-xs">
                  <div style="width: 55%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-warning">
                    <span class="sr-only">55% Complete (success)</span>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#">
                  BCSE
                </a>
              </td>
              <td class="hidden-phone">Lorem Ipsum dorolo</td>
              <td>3455.50$ </td>
              <td><span class="label label-danger label-mini">Paid</span></td>
              <td>
                <div class="progress progress-striped progress-xs">
                  <div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-info">
                    <span class="sr-only">90% Complete (success)</span>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td><a href="#">AVC Ltd</a></td>
              <td class="hidden-phone">Lorem Ipsum dorolo imit</td>
              <td>110.00$ </td>
              <td><span class="label label-primary label-mini">Due</span></td>
              <td>
                <div class="progress progress-striped progress-xs">
                  <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success">
                    <span class="sr-only">60% Complete (success)</span>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#">
                  Themeforest
                </a>
              </td>
              <td class="hidden-phone">Lorem Ipsum dorolo</td>
              <td>456.00$ </td>
              <td><span class="label label-warning label-mini">Due</span></td>
              <td>
                <div class="progress progress-striped progress-xs">
                  <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-danger">
                    <span class="sr-only">40% Complete (success)</span>
                  </div>
                </div>
              </td>
            </tr>

          </tbody>
        </table>
      </div>
    </section>
  </div>
</div>

<br><hr><br>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <div class="profile-nav alt">
      <section class="panel text-center">
        <div class="user-heading alt wdgt-row terques-bg">
          <i class="fa fa-user"></i>
        </div>

        <div class="panel-body">
          <div class="wdgt-value">
            <h1 class="count">1,225</h1>
            <p>New Users</p>
          </div>
        </div>

      </section>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <div class="profile-nav alt">
      <section class="panel text-center">
        <div class="user-heading alt wdgt-row red-bg">
          <i class="fa fa-tags"></i>
        </div>

        <div class="panel-body">
          <div class="wdgt-value">
            <h1 class="count">3,295</h1>
            <p>Sales</p>
          </div>
        </div>

      </section>
    </div>
  </div>
  <div class="col-md-6">
    <div class="feed-box text-center">
      <section class="panel">
        <div class="panel-body">
          <div class="corner-ribon blue-ribon">
            <i class="fa fa-twitter"></i>
          </div>
          <a href="#">
            <img alt="" src="images/lock_thumb.jpg">
          </a>
          <h1>Kanye West</h1>
          <p>Just got a pretty neat project via <a href="#">@ooomf</a> - Give it a try <a href="#">http://t.co/e02DwGEeOJ</a></p>
        </div>
      </section>
    </div>
  </div>
</div>

<br>
