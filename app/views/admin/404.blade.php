<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="">
    <title>404</title>
    {{-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" type="text/css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic" type="text/css"> --}}
    <link href="{{ URL::asset('admin/css/fonts.css') }}" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('admin/css/bootstrap-reset.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ URL::asset('admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('admin/css/style-responsive.css') }}" rel="stylesheet" />
</head>
  <body class="body-404">

    <div class="error-head" style="height: 160px;"> </div>

    <div class="container ">

      <section class="error-wrapper text-center">
          <h1><img src="{{ URL::asset('admin/images/404.png') }}" alt=""></h1>
          <div class="error-desk">
              <h2>page not found</h2>
              <p class="nrml-txt">We Couldn’t Find This Page</p>
          </div>
          <a href="/oadmin/dashboard" class="back-btn" style="background-color:#fff;color:#35bcb5;"><i class="fa fa-home"></i> <strong>Go to Dashboard</strong></a>
          <a href="javascript:history.go(-1);" class="back-btn" style="background-color:#fff;color:#35bcb5;"><i class="fa fa-backward"></i> <strong>Back to Previous Page</strong></a>
          <a href="/" class="back-btn" style="background-color:#fff;color:#35bcb5;"><i class="fa fa-home"></i> <strong>Go to Home Page</strong></a>
      </section>

    </div>


  </body>
</html>
