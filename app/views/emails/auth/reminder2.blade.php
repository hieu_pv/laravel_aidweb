<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    {{-- <h2>Password Reset</h2> --}}
    <div>
      <p>Hi {{$user->userProfile->fullname}},</p>
      <p>You recently requested AidPost to request your password. Please follow the link below and set your new password.</p>
      <p><a href="{{ URL::route('account_do_resetpassword', array('token' => $token)) }}">{{ URL::route('account_do_resetpassword', array('token' => $token)) }}</a></p>
      <p>This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.</p>
      <br>
      <p>Regards,<br>AidPost Team</p>
    </div>
  </body>
</html>
