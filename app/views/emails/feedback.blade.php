<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>A new message has been submitted</p>
		<div>
			<strong>Subject</strong> : <span>{{$subject}}</span> <br>
			<strong>Name</strong> : <span>{{$model->name}}</span> <br>
			<strong>Email</strong> : <span>{{$model->email}}</span> <br>
			<strong>Message</strong> : <span>{{$model->message}}</span> <br>
		</div>
	</body>
</html>
