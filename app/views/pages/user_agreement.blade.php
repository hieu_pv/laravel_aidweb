@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="bottompadding static-page">
    <div class="container clearfix">

      <div class="nobottommargin clearfix col-md-12">
        <div class="single-post nobottommargin">
          <div class="entry clearfix">
            <div class="entry-title">
              <h1>User Agreement</h1>
            </div>
            <div class="entry-content">
              <ol start="1" class="notopmargin">
                <li><h3 class="">Introduction</h3></li>
              </ol>
              <h4>1.1. Purpose</h4>
              <p class="justify">AidPost is a media and network for aid and social actors. We aim at building a global community where organizations and individuals alike have an increased capacity to connect with the public and their peers, in order to improve the overall performance of the civil society as a whole.</p>
              
              <h4>1.2. Agreement</h4>
              <p class="justify">When you use our Service, you are entering into a legal agreement and you agree to all of these terms. You also agree to our <a href="https://www.linkedin.com/legal/privacy-policy">Privacy Policy</a>, which covers how we collect, use, share, and store your personal information.</p>
              <p class="justify">You agree that by clicking &ldquo;Join Now&rdquo;, &ldquo;Sign Up&rdquo; or similar, registering, accessing or using our service, you are entering into a legally binding agreement, even if you are using our Service on behalf of a company. Your agreement is with AidPost, registered in Canada.</p>
              <p class="justify">This &ldquo;Agreement&rdquo; includes this User Agreement and the <a href="https://www.linkedin.com/legal/privacy-policy">Privacy Policy</a>. If you do not agree to this Agreement, do NOT click &ldquo;Join Now&rdquo; (or similar) and do not access or otherwise use our Service.</p>
              <p class="justify">Registered users of our Service are &ldquo;Members&rdquo; and unregistered users are &ldquo;Visitors&rdquo;. This Agreement applies to both.</p>
              
              <ol start="2">
                <li><h3>Obligations</h3></li>
              </ol>
              
              <h4>2.1. Service Eligibility</h4>
              <p class="justify">Here are some promises you make to us in this Agreement:</p>
              <p class="justify">You're eligible to enter into this Agreement and you are at least our &ldquo;Minimum Age.&rdquo;</p>
              <p class="justify">To use the Service, you agree that: (1) you must be the &ldquo;Minimum Age&rdquo; (defined below) or older; (2) you will only have one AidPost account, which must be in your real name; and (3) you are not already restricted by AidPost from using the Service.</p>
              <p class="justify">&ldquo;Minimum Age&rdquo; means (a) 18 years old for the People's Republic of China, (b) 16 years old for the Netherlands, (c) 14 years old for the United States, Canada, Germany, Spain, Australia and South Korea, and (d) 13 years old for all other countries. However, if law requires that you must be older in order for AidPost to lawfully provide the Service to you (including the collection, storage and use of your information) then the Minimum Age is such older age. The Service is not for use by anyone under the age of 13.</p>
              
              <h4>2.2. Your Membership</h4>
              <p class="justify">You'll keep your password a secret.</p>
              <p class="justify">You will not share an account with anyone else and will follow our rules and the law.</p>
              <p class="justify">As between you and others, your account belongs to you. You agree to: (1) try to choose a strong and secure password; (2) keep your password secure and confidential; and (3) follow the law and the Dos and Don'ts below. You are responsible for anything that happens through your account unless you close it or report misuse.</p>
              
              <h4>2.3 Payment</h4>
              <p class="justify">If you purchase publicity space(s), you agree to pay us the applicable fees and taxes. Failure to pay these fees may result in the termination of your subscription.</p>
              
              <h4>2.4. Notifices and Service Messages</h4>
              <p class="justify">You're okay with us using our website and email to provide you with important notices.</p>
              <p class="justify">If the contact information you provide isn't up to date, you may miss out on these notices.</p>
              <p class="justify">You agree that we may provide notices to you in the following ways: (1) a banner notice on the Service, or (2) an email sent to an address you provided, or (3) through other means including mobile number, telephone, or mail. You agree to keep your <a href="https://help.linkedin.com/app/answers/detail/a_id/34987/loc/na/trk/NoPageKey/">contact information</a> up to date.</p>
              
              <h4>2.5. Messages and Sharing</h4>
              <p class="justify">When you share information, others can see, copy and use that information.</p>
              <p class="justify">Our Service allow sharing of information in many ways, such as your profile, links to your website and social media, and job postings. Information and content that you share or post may be seen by other Members or by Visitors.</p>
              <p class="justify">We are not obligated to publish any information or content on our Service and can remove it in our sole discretion, with or without notice.</p>
              
              <ol start="3">
                <li><h3>Rights and Limits</h3></li>
              </ol>
              
              <h4>3.1. Your License to AidPost </h4>
              <p class="justify">You own all of the content, feedback, and personal information you provide to us, but you also grant us a non-exclusive license to it.</p>
              <p class="justify">You promise to only provide information and content that you have the right to share, and that your AidPost profile will be truthful.</p>
              <p class="justify">As between you and AidPost, you own the content and information that you submit or post to the Service and you are only granting AidPost the following non-exclusive license: A worldwide, transferable and sublicensable right to use, copy, modify, distribute, publish, and process, information and content that you provide through our Service, without any further consent, notice and/or compensation to you or others. These rights are limited in the following ways:</p>
              
              <ol>
                <li>You can end this license for specific content by deleting such content from the Service, or generally by closing your account, except (a) to the extent you shared it with others as part of the Service and they copied or stored it and (b) for the reasonable time it takes to remove from backup and other systems.</li>
                <li>We will not include your content in advertisements for the products and services of others (including sponsored content) to others without your separate consent. However, we have the right, without compensation to you or others, to serve ads near your content and information, and your comments on sponsored content may be visible.</li>
                <li>We will get your consent if we want to give others the right to publish your posts beyond the Service. However, other Members and/or Visitors may access and share your content and information.</li>
              </ol>
              
              <p class="justify">You agree that we may access, store and use any information that you provide in accordance with the terms of the <a href="https://www.linkedin.com/legal/privacy-policy">Privacy Policy</a> and your privacy settings.</p>
              <p class="justify">By submitting suggestions or other feedback regarding our Service to AidPost, you agree that AidPost can use and share (but does not have to) such feedback for any purpose without compensation to you.</p>
              <p class="justify">You agree to only provide content or information if that does not violate the law nor anyone's rights (e.g., without violating any intellectual property rights or breaching a contract). You also agree that your profile information will be truthful. AidPost may be required by law to remove certain information or content in certain countries.</p>
              
              <h4>3.2. Service Availability</h4>
              <p class="justify">We may change or discontinue our Service. We can't promise to store or keep showing any information and content you've posted.</p>
              <p class="justify">We may change, suspend or end our Service, or change and modify prices prospectively in our discretion. To the extent allowed under law, these changes may be effective upon notice provided to you.</p>
              <p class="justify">AidPost is not a storage service. You agree that we have no obligation to store, maintain or provide you a copy of any content or information that you or others provide, except to the extent required by applicable law.</p>
              
              <h4>3.3. Other Content, Sites and apps</h4>
              <p class="justify">When you see or use others' content and information posted on our Service, it's at your own risk.</p>
              <p class="justify">Third parties may offer their own products and services through AidPost, and we aren't responsible for those third-party activities.</p>
              <p class="justify">By using the Service, you may encounter content or information that might be inaccurate, incomplete, delayed, misleading, illegal, offensive or otherwise harmful. AidPost does not systematically review content provided by our Members. You agree that we are not responsible for third parties' (including other Members') content or information or for any damages as result of your use of or reliance on it.</p>
              <p class="justify">You are responsible for deciding if you want to access or use third party apps or sites that link from our Service.</p>
              
              <h4>3.4. Limits</h4>
              <p class="justify">We have the right to limit how you connect and interact on our Service.</p>
              <p class="justify">We're providing you notice about our intellectual property rights.</p>
              <p class="justify">AidPost reserves the right to restrict, suspend, or terminate your account if AidPost believes that you may be in breach of this Agreement or law or are misusing the Service (e.g. violating any Do and Don'ts).</p>
              <p class="justify">AidPost reserves all of its intellectual property rights in the Service. For example, AidPost service marks, graphics, and logos used in connection with AidPost are trademarks or registered trademarks of AidPost. Other trademarks and logos used in connection with the Service may be the trademarks of their respective owners.</p>
              
              <ol start="4">
                <li><h3> Disclaimer and Limit of Liability</h3></li>
              </ol>
              
              <h4>4.1. No Warranty</h4>
              <p class="justify">This is our disclaimer of legal liability for the quality, safety, or reliability of our Service.</p>
              <p class="justify">To the extend allowed under law, AidPost (and those that AidPost works with to provide the service) (a) disclaim all implied warranties and representations (e.g. warranties of merchantability, fitness for a particualr purpose, accuracy of data, and noninfringement); (b) do not guarantee that the service will function without interruption or errors, and (c) provide the service (including content and information) on an &ldquo;as is&rdquo; and &ldquo;as available basis.</p>
              <p class="justify">Some laws do not allow certain disclaimers, so some or all these disclaimers may not apply to you.</p>
              
              <h4>4.2. Exclusion of Liability</h4>
              <p class="justify">These are the limits of legal liability we may have to you.</p>
              <p class="justify">To the extent permitted under law (and unless AidPost has entered into a separate written agreement that supersedes this agreement), AidPost (and those that AidPost works with to provide the Service) shall not be liable to you or others for any indirect, incidental, special, consequencial or punitive damages, or any loss of data, opportunities, reputation, profits or revenues, related to the services (e.g. offensive or defamatory statements, down time or loss, use or changes to your information or content).</p>
              <p class="justify">In no event shall the liability of AidPost (and those that AidPost works with to provide the Service) exceed, in the aggregate for all claims, an amount above US $1000.</p>
              <p class="justify">This limitation of liability is part of the basis of the bargain between you and AidPost and shall apply to all claims of liability (e.g. warranty, tort, negligence, contract, law) and even if AidPost has been told of the possibility of any such damage, and even if these remedies fail their essential purpose.</p>
              <p class="justify">Some laws do not allow the limitation or exclusion of liability, so these limits may not apply to you.</p>
              
              <ol start="5">
                <li><h3> Termination</h3></li>
              </ol>
              <p class="justify">We can each end this Agreement anytime we want.</p>
              <p class="justify">AidPost or You may terminate this Agreement at any time with notice to the other. On termination, you lose the right to access or use the Service. The following shall survive termination:</p>
              
              <ul>
                <li>Our rights to use and disclose your feedback;</li>
                <li>Members and/or Visitors'r rights to further re-share content and information you shared through the Service to the extent copied or re-shared prior to termination;</li>
                <li>Sections 4, 6 and 7 of this Agreement;</li>
                <li>Any amounts owed by either party prior to termination remain owed after termination.</li>
              </ul>
              
              <ol start="6">
                <li><h3> Dispute Resolution</h3></li>
              </ol>
              <p class="justify">In the unlikely event we end up in a legal dispute, it will take place in the province of Quebec courts, applying Canadian law.</p>
              
              <ol start="7">
                <li><h3> General Terms</h3></li>
              </ol>
              <p class="justify">Here are some important details about how to read the Agreement.</p>
              <p class="justify">If a court with authority over this Agreement finds any part of it not enforceable, you and us agree that the court should modify the terms to make that part enforceable while still achieving its intent. If the court cannot do that, you and us agree to ask the court to remove that unenforceable part and still enforce the rest of this Agreement. To the extent allowed by law, the English version of this Agreement is binding and other translations are for convenience only. This Agreement (including additional terms that may be provided by us when you engage with a feature of the Service) is the only agreement between us regarding the Service and supersedes all prior agreements for the Service.</p>
              <p class="justify">If we don't act to enforce a breach of this Agreement, that does not mean that AidPost has waived its right to enforce this Agreement. You may not assign or transfer this Agreement (or your membership or use of Service) to anyone without our consent. However, you agree that AidPost may assign this Agreement to its affiliates or a party that buys it without your consent. There are no third party beneficiaries to this Agreement.</p>
              <p class="justify">We reserve the right to change the terms of this Agreement and will provide you notice if we do and we agree that changes cannot be retroactive. If you don't agree to these changes, you must stop using the Service.</p>
              <p class="justify">You agree that the only way to provide us legal notice is at the email address provided in Section 10.</p>
              
              <ol start="8">
                <li><h3> AidPost &ldquo;DOs&rdquo; and &ldquo;DON&rsquo;Ts.&rdquo;</h3></li>
              </ol>
              
              <h4>8.1. Dos. You agree that you will:</h4>
              <ul>
                <li>Comply with all applicable laws, including, without limitation, privacy laws, intellectual property laws, anti-spam laws, export control laws, tax laws, and regulatory requirements;</li>
                <li>Provide accurate information to us and keep it updated;</li>
                <li>Use your real name on your profile;</li>
                <li>Use the Service in a professional manner.</li>
              </ul>
              
              <h4>8.2. Don'ts. You agree that you will not:</h4>
              <ul>
                <li>Act dishonestly or unprofessionally, including by posting inappropriate, inaccurate, or objectionable content;</li>
                <li>Add content that is not intended for, or inaccurate for, a designated field (e.g. submitting a telephone number in the &ldquo;title&rdquo; or any other field, or including telephone numbers, email addresses, street addresses or any personally identifiable information for which there is not a field provided by AidPost);</li>
                <li>Use an image that is not your likeness or a head-shot photo for your profile;</li>
                <li>Create a false identity on AidPost;</li>
                <li>Misrepresent your current or previous positions and qualifications;</li>
                <li>Misrepresent your identity, including but not limited to the use of a pseudonym;</li>
                <li>Create a Member profile for anyone other than yourself (a real person);</li>
                <li>Use or attempt to use another's account;</li>
                <li>Harass, abuse or harm another person;</li>
                <li>Scrape or copy profiles and information of others through any means (including crawlers, browser plugins and add-ons, and any other technology or manual work);</li>
                <li>Act in an unlawful, libelous, abusive, obscene, discriminatory or otherwise objectionable manner;</li>
                <li>Disclose information that you do not have the right to disclose (such as confidential information of others (including your employer));</li>
                <li>Violate intellectual property rights of others, including patents, trademarks, trade secrets, copyrights or other proprietary rights;</li>
                <li>Violate the intellectual property or other rights of AidPost;</li>
                <li>Post any unsolicited or unauthorized advertising or any other form of solicitation unauthorized by AidPost;</li>
                <li>Post anything that contains software viruses, worms, or any other harmful code;</li>
                <li>Manipulate identifiers in order to disguise the origin of any message or post transmitted through the Service;</li>
                <li>Create profiles or provide content that promotes escort services or prostitution.</li>
                <li>Creating or operate a pyramid scheme, fraud or other similar practice;</li>
                <li>Copy or use the information, content or data of others available on the Service (except as expressly authorized);</li>
                <li>Copy or use the information, content or data on AidPost &nbsp;in connection with a competitive service (as determined by AidPost);</li>
                <li>Copy, modify or create derivative works of Aidweb, the Service or any related technology (except as expressly authorized by AidPost);</li>
                <li>Reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for the Services or any related technology, or any part thereof;</li>
                <li>Imply or state that you are affiliated with or endorsed by AidPost without our express consent</li>
                <li>Rent, lease, loan, trade, sell/re-sell access to the Service or related any information or data;</li>
                <li>Remove any copyright, trademark or other proprietary rights notices contained in or on our Service;</li>
                <li>Remove, cover or obscure any advertisement included on the Service;</li>
                <li>Collect, use, copy, or transfer any information obtained from Aidweb without the consent of AidPost;</li>
                <li>Share or disclose information of others without their express consent;</li>
                <li>Use manual or automated software, devices, scripts robots, other means or processes to access, &ldquo;scrape,&rdquo; &ldquo;crawl&rdquo; or &ldquo;spider&rdquo; the Service or any related data or information;</li>
                <li>Use bots or other automated methods to access the Service;</li>
                <li>Monitor the Service availability, performance or functionality for any competitive purpose;</li>
                <li>Engage in &ldquo;framing,&rdquo; &ldquo;mirroring,&rdquo; or otherwise simulating the appearance or function of the Service;</li>
                <li>Access the Service except through the interfaces expressly provided by AidPost;</li>
                <li>Override any security feature of the Services;</li>
                <li>Interfere with the operation of, or place an unreasonable load on, the Services (e.g., spam, denial of service attack, viruses, gaming algorithms.</li>
              </ul>
              
              <ol start="9">
                <li><h3> Complaints Regarding Content</h3></li>
              </ol>
              
              <h4>9.1 Notice of Copyring Infringement</h4>
              <p class="justify">We respect the intellectual property rights of others. We require that information posted by Members be accurate and not in violation of the intellectual property rights or other rights of third parties.</p>
              <p class="justify">If you believe in good faith that your copyright has been infringed, you may provide a written communication which contains:</p>
              <ol>
                <li>An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest;</li>
                <li>A description of the copyrighted work that you claim has been infringed;</li>
                <li>A description specifying the location on our website of the material that you claim is infringing;</li>
                <li>Your telephone number and e-mail address;</li>
                <li>A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and</li>
                <li>A statement by you, made under penalty of perjury, that the information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner&rsquo;s behalf.</li>
              </ol>
              <strong>Please submit your notice by email at: <a href="mailto:info@aidpost.org">info@aidpost.org</a>.</strong>
              
              <h4>9.2 Reporting Inappropriate Content, Messages, or Safety Concerns</h4>
              <p class="justify">If you come across a concern you'd like to report, please do not hesitate to share it with us at: <a href="mailto:info@aidpost.org">info@aidpost.org</a>.</p>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
@stop

@section('after_content')
  
@stop