@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="container">
    <div id="" class="portfolio-carousel row list-of-videos">

      <div class="oc-item zoomhover col-md-offset-4 col-md-4 smallpadding col-sm-offset-6 col-sm-6">
        <div class="ipost card-grey clearfix">
          <div class="portfolio-image">
            <a href="#" class="zoomeffect"> 
              <img src="{{$model->image}}" alt="{{$model->title}}">
            </a>
            <div class="portfolio-overlay flat">
              <a href="{{$model->watchUrl()}}" class="left-icon topleft" data-lightbox="aidweb"><i class="icon-line-play"></i></a>
              <div class="portfolio-desc">
                <span style="border:none; padding-bottom:15px;">
                  {{$model->description}}
                </span>
                <h3 class="videoattr"><span><strong>{{$model->present()->timeAgo()}}</strong> <strong><i class="icon-thumbs-up"></i> {{$model->num_of_likes}}</strong></span></h3>
              </div>
            </div>
            <span class="duration"><i class="icon-stopwatch"></i> {{$model->displayed_duration}}</span>
          </div>
          <div class="news-desc"><a href="{{$model->watchUrl()}}" data-lightbox="aidweb">{{$model->title}}</a></div>
          <div class="portfolio-desc">
            <a href="{{$model->present()->organisationInfo()->profile_url}}"><img class="org-logo" src="{{$model->present()->organisationInfo()->logo}}" /></a>
            <div class="desc-content">
              <h3><strong>{{$model->present()->organisationInfo()->type}}</strong></h3>
              <span><a href="{{$model->present()->organisationInfo()->profile_url}}">{{$model->present()->organisationInfo()->name}}</a></span>
            </div><div class="clear"></div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_videos.js') }}"></script>
@stop