<div class="container topmargin bottommargin hidden-xs hidden-sm">
  <div class="fancy-title title-dotted-border title-center">
    <h3 class="">Featured Videos</h3>
  </div>
  <div class="hidden-xs">
    <ul class="tab-nav clearfix countries" id="top_videos_tabs">
      @foreach ($tabsOfTopVideos->tabs as $item)
        <li><a href="#topvideos_tab_{{$item->id}}" data-toggle="tab" data-id="{{$item->id}}">{{$item->name}}</a></li>
      @endforeach
    </ul>
  </div>
  <div class="clear"></div>
  <div class="tab-content featured-news">
    @foreach ($tabsOfTopVideos->tabs as $item)
      <div role="tabpanel" class="tab-pane fade" id="topvideos_tab_{{$item->id}}">
        <div class="col-md-12 hidden-xs nomargin nopadding">
          <section id="" class="swiper_wrapper video-slider clearfix">
            <div class="swiper-container swiper-video-parent swiper-topvideos-parent-{{$item->id}}">
              <div class="swiper-wrapper">
                @foreach ($item->top_videos as $video)
                  <div class="swiper-slide">
                    <div class="slideimg col-md-8 nomargin nopadding" style="background-image: url('{{$video->image}}'); background-position: center center;"></div>
                    <div class="slidecontent col-md-4 nomargin">
                      <div class="slider-caption">
                        <a href="{{$video->org_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$video->org_info->logo}}" data-swiper-parallax="-150" /></a>
                        <p data-swiper-parallax="-150">
                          <strong>{{$video->sector_name}}</strong><br>
                          <a href="{{$video->org_info->profile_url}}">{{$video->org_info->name}}</a>
                        </p>
                        <div class="clear"></div>
                        <p>&nbsp;</p>
                        <div class="clear"></div>
                        <h2 data-swiper-parallax="-200"><a href="{{$video->watch_url}}" data-lightbox="aidweb" data-id="{{$video->id}}"><strong>{{$video->title}}</strong></a></h2>
                        <p data-swiper-parallax="-100" data-clamp-height="94">{{str_ireplace('</p>','',str_ireplace('<p>','',nl2br($video->teaser)))}}</p>
                        <div class="clear"></div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
            {{-- <div id="slider-video-arrow-left"><i class="icon-angle-left"></i></div>
            <div id="slider-video-arrow-right" class="pushed"><i class="icon-angle-right"></i></div> --}}
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-{{$item->id}}"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-next-{{$item->id}}"></div>
            <div class="swiper-button-prev swiper-button-prev-{{$item->id}}"></div>
          </section>
        </div>
        <div class="clear"></div>
        {{-- <div class="hidden-sm hidden-xs news-slider-thumbs nomargin">
          <div class="gallery-thumbs gallery-thumbs-news gallery-thumbs-topvideos-{{$item->id}} hidden-sm hidden-xs">
            <div class="swiper-wrapper">
              @foreach ($item->top_videos as $video)
                <div class="swiper-slide"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$video->org_info->logo}}" /> <p data-clamp-height="50" style="padding-top: 0; padding-bottom: 0;">{{$video->title}}</p></div>
              @endforeach
            </div>
          </div>
        </div> --}}
        <div class="clear"></div>
      </div>
    @endforeach
  </div>

  <script>
    function setupSlider (id) {
      if ($('.swiper-topvideos-parent-' + id).length > 0) {
        var existing1 = $('.swiper-topvideos-parent-' + id)[0].swiper;
        if (existing1) {
          existing1.slideTo(0);
        }
        // var existing2 = $('.gallery-thumbs-topvideos-' + id)[0].swiper;
        // if (existing2) {
        //   existing2.slideTo(0);
        // }
      }
      var swiperSlider = new Swiper('.swiper-topvideos-parent-' + id, {
        parallax:true,
        autoplay: 5000,
        speed: 300,
        effect: 'fade',
        // nextButton: '#slider-video-arrow-right',
        // prevButton: '#slider-video-arrow-left',
        pagination: '.swiper-pagination-' + id,
        paginationClickable: true,
        nextButton: '.swiper-button-next-' + id,
        prevButton: '.swiper-button-prev-' + id,
      });
      // var galleryThumbs = new Swiper('.gallery-thumbs-topvideos-' + id, {
      //   spaceBetween: 0,
      //   direction: 'horizontal',
      //   centeredSlides: true,
      //   slidesPerView: 4,
      //   slideToClickedSlide: true
      // });
      // swiperSlider.params.control = galleryThumbs;
      // galleryThumbs.params.control = swiperSlider;
    }

    jQuery(document).ready(function($){
      $('#top_videos_tabs li:first-child a').click();
      $('#top_videos_tabs li:first-child a').closest('li').addClass('ui-tabs-active ui-state-active').click();
      setupSlider($('#top_videos_tabs li:first-child a').attr('data-id'));

      $('#top_videos_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(e.relatedTarget).closest('li').removeClass('ui-tabs-active ui-state-active');
        $(e.target).closest('li').addClass('ui-tabs-active ui-state-active');
        var id = $(e.target).attr('data-id');
        setupSlider(id);
      });
    });
  </script>
</div>

<!-- <div class="container topmargin bottommargin hidden-md hidden-lg">
  <div id="" class="portfolio-carousel row list-of-videos">
    @foreach ($tabsOfTopVideos->tabs as $item)
      @foreach ($item->top_videos as $video)
        <div class="oc-item zoomhover col-md-4 smallpadding col-sm-6">
          <div class="ipost card-grey clearfix">
            <div class="portfolio-image">
              <a href="#" class="zoomeffect"><img src="/images/placeholder.gif" data-original="{{$video->image}}" alt="{{$video->title}}" class="lazy"></a>
              <div class="portfolio-overlay flat">
                <a href="{{$video->watch_url}}" class="left-icon topleft" data-lightbox="aidweb" data-desc="{{$video->description}}" data-likes="{{$video->num_of_likes}}" data-id="{{$video->id}}"><i class="icon-line-play"></i></a>
                <div class="portfolio-desc">
                  <span style="border:none; margin-bottom:15px; padding-top: 0; padding-bottom: 0;" data-clamp-height="150">
                    <span>{{nl2br($video->description)}}</span>
                  </span>
                  <h3 class="videoattr"><span><strong>{{$video->time_ago}}</strong> <strong><i class="icon-thumbs-up"></i> {{$video->num_of_likes}}</strong></span></h3>
                </div>
              </div>
              <span class="duration"><i class="icon-stopwatch"></i> {{$video->duration}}</span>
            </div>
            <div class="news-desc"><a href="{{$video->watch_url}}" data-lightbox="aidweb" data-clamp-height="70" data-desc="{{$video->description}}" data-likes="{{$video->num_of_likes}}" data-id="{{$video->id}}">{{$video->title}}</a></div>
            <div class="portfolio-desc">
              <a href="{{$video->org_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$video->org_info->logo}}" /></a>
              <div class="desc-content">
                <h3><strong>{{$video->sector_name}}</strong></h3>
                <span><a href="{{$video->org_info->profile_url}}">{{$video->org_info->name}}</a></span>
              </div><div class="clear"></div>
            </div>
          </div>
        </div>
      @endforeach
    @endforeach
  </div>
  <div class="clear"></div>
</div> -->