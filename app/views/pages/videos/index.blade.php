@extends('layouts.site')

@section('before_content')
  @include('pages.common._pub_big', array('model' => Publicities::getBigPublicity()))
  <div class="clear"></div>
@stop

@section('content')
  @include('pages.videos._top_videos')
  @include('pages.videos._all_videos')
@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_videos.js') }}"></script>
@stop