@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="bottompadding static-page">
    <div class="container clearfix">
      
      <div class="nobottommargin clearfix col-md-12">
        <div class="single-post nobottommargin">
          <div class="entry clearfix">
            <div class="entry-title">
              <h1>About Us</h1>
            </div>
            <div class="entry-content justify">
              <h3 class="notopmargin">What is AidPost?</h3>
              <p class="justify">AidPost is a website where postings from aid organizations and their staff are automatically displayed on intertwined national and international platforms, so as to maximize the interaction between aid actors and potential supporters worldwide. It offers the aid community a modern channel to connect with their peers and present their activities and needs to a larger audience.</p>

              <h3>AidPost objective</h3>
              <p class="justify">AidPost supports the communication, fundraising and networking needs of NGOs, and their staff. It aims at building an aid community where nonprofits of all sizes and place of origin, can increase their visibility, influence, resources, knowledge, network, and efficiency by:</p>
              
              <ul>
                <li>Sharing and accessing useful information on development issues</li>
                <li>Facilitating resource mobilization from the public and the private sector</li>
                <li>Making it easy to identify and connect with members of the aid community</li>
                <li>Stimulating open discourse between nonprofits, corporations, and governments</li>
                <li>Fostering innovation and spreading new ideas and technologies</li>
                <li>Providing a valuable alternative - and a springboard - to mainstream media.</li>
                <li>Supporting advancement of freedom of expression worldwide.</li>
              </ul>

              <h3>What problem AidPost is addressing?</h3>

              <p class="justify">NGOs must be noticed on internet and the media to engage the general public for resource mobilization. The people they need to influence for policy change and major donations are also increasingly influenced by the internet.</p>
                
              <p class="justify">But internet is not an easy fit for all: it requires investment of time, money and technical skills - resources that are not available to the vast majority of nonprofits around the world. And in the societies which are still striving towards further democratization, the relationship between NGOs and the media is often problematic - as a result, NGOs seek to establish alternative means of communicating their message.</p>
              
              <p class="justify">Most NGOs see value in <b>networks</b> as a way to enhance capacities, to access and share knowledge, and to gain more attention. But the viability of national networks poses a challenge, and their connectivity with the outside world is often limited. (<em>A Networked Response? Exploring National Humanitarian Networks in Asia, ALNAP, 2013</em>).</p>

              <h3>New trend to empower local NGOs</h3>

              <p class="justify">The International aid community face many challenges; among these, empowering local organizations is at the top of the list. According to Foreign Affairs (Barnett and Walker, issue of July/August 2015), recent studies show that in disaster responses: </p>
              
              <ul>
                <li>local NGOs - and the victims - see very little of the funds provided by the Western governments;</li>
                <li>these shortfalls do more than create a bad reputation for international crisis responders; they also cost lives.</li>
                <li>working relationships with local groups greatly increases the effectiveness of aid and can, in fact, lower the price tag;</li>
              </ul>

              <p class="justify">In 2013, the gap between the funds available for humanitarian aid and estimated global needs reached $ 4,5 billion, leaving at least one third of the demand unmet. These data underscore the urgency of empowering national organizations and disaster-affected populations. In this respect, new actors and technologies could soon accomplish just that.</p>

              <p class="justify">Prominent donor organizations from developing countries, corporate players, faith-based institutions and a few massive philanthropic foundations are inclined  to plow money into regional associations that are responsive to local needs, rapidly growing NGOs based in developing countries point the way to a more effective and legitimate system, and social media platforms such as Facebook and Twitter are helping aid recipients organize and speak more assertively.</p>
              
              <p class="justify">In its own way, with the collaboration of international aid agencies who have an interest in empowering local NGOs, AidPost could also improve the accountability towards beneficiaries and performance of the aid community by connecting NGOs and aid supporters from both hemispheres.</p>
              
              <h3>Approach</h3>

              <p class="justify">AidPost is implementing this initiative along the Minimum Viable Product approach, which is part of an iterative Build-Measure-Learn process. The idea is to start simple and let the Service grow in complexity based on the needs of the aid community. As such, AidPost and its partners will continuously seek and act upon feedback from members to improve and further develop the service. </p>
              
              <h3>Sustainability & Implementing Partners</h3>

              <p class="justify"></p>The platforms in developing countries will be managed by national associations of journalists or NGOs. For instance, in Indonesia, a partnership was established with Aliansi Jurnalis Independen (AJI), a member of the International Federation of Journalists.</p>
 
              <p class="justify"></p>To ensure the viability of each national platform, AidPost implementing partners will rely on sponsors and on the sale of add and featured spaces. To this effect, AidPost has integrated its own publicity platform - members can create and submit their adds directly from their account.</p>

              <h3>Historic</h3>

              <p class="justify">Senior aid professionals and web developers from various countries have teamed up, on a volunteer basis, to create AidPost.  The roots of this initiative can be traced back to the relief operations of the 2006 tsunami in Aceh and the 2010 earthquake in Haiti, where the people who have conceptualized AidPost were involved as senior aid workers. These two large scale disasters have been an eye opener for those deployed in the field on some of the biggest challenges faced by the aid community. </p>

              <h3>Board of Advisors</h3>
              <p class="justify">AidPost is supported by a Board of Advisors composed of senior aid professionals, information technology specialists and communication experts from various countries. By alphabetic order:</p>
              
              <div class="row">
                <div class="col-md-3">Michael Andreini <br> United States</div>
                <div class="col-md-9">Director <br> Rocky Mountains Epidemiology Center, USA</div>
                <div class="clearfix"><br></div>

                <div class="col-md-3">Farook Adrien Doomun <br> Switzerland</div>
                <div class="col-md-9">Regional Head of Support Service <br> UNOPS, Senegal</div>
                <div class="clearfix"><br></div>


                <div class="col-md-3">Yegana Guliyeva-Baigorri <br> Azerbaidjan</div>
                <div class="col-md-9">Adviser on International Cooperation <br> BILTEK Turkey, Spain</div>
                <div class="clearfix"><br></div>
                
                <div class="col-md-3">Raymond Lachappelle <br> Canada</div>
                <div class="col-md-9">Communication Adviser <br> Cellonis Biotechnologies, China</div>
                <div class="clearfix"><br></div>

                <div class="col-md-3">Pierre Markowski <br> Canada</div>
                <div class="col-md-9">Regional Director <br> Developpement International Desjardins, Senegal</div>
              </div>
              
              <h3>Team</h3>

              <p class="justify">AidPost is thankful to the following people who have also contributed, most of them on a volunteer basis, to the creation of Aidpost. By alphabetic order:</p>
               <div class="row">
                <div class="col-md-3">Charles Caesien <br> Indonesia</div>
                <div class="col-md-9">Research <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Diyah Perwitosri <br> Indonesia</div>
                <div class="col-md-9">Research <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Haritian Pinio <br> Indonesia</div>
                <div class="col-md-9">Web Programming <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Ken Vo Dinh Thinh <br> Vietnam</div>
                <div class="col-md-9">Web programmer<br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Lewis Webber <br> United Kingdom</div>
                <div class="col-md-9">Web Design <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Nana Srihasanah <br> Indonesia</div>
                <div class="col-md-9">Administration <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Roger Markowski <br> Canada</div>
                <div class="col-md-9">Coordination <br> &nbsp;</div>

              </div>

              <h3>Consultancy</h3>
              <p class="justify">The following senior freelancers have also provided free guidance during the development phase of AidPost:</p>
              <div class="row">
                <div class="col-md-3">Abhijit Agarwal <br> India</div>
                <div class="col-md-9">Web developer<br> &nbsp;</div>
                <div class="clearfix"><br></div>

                <div class="col-md-3">Adrien Motsch <br> France</div>
                <div class="col-md-9">Website Developer<br> &nbsp;</div>
                <div class="clearfix"><br></div>

                <div class="col-md-3">Ajay Kumar <br> India</div>
                <div class="col-md-9">Web developer<br> &nbsp;</div>
                <div class="clearfix"><br></div>
                
                <div class="col-md-3">Ousmane Wilane<br> Senegal</div>
                <div class="col-md-9">Website Developer<br> &nbsp;</div>
                <div class="clearfix"><br></div>

                <div class="col-md-3">Rosaleen Cunningham <br> UK</div>
                <div class="col-md-9">Communication Specialist<br> &nbsp;</div>
                <div class="clearfix"><br></div>

                <div class="col-md-3">Tibo<br> France</div>
                <div class="col-md-9">Website Developer<br> &nbsp;</div>
              </div>

              <br>
              <p class="justify">AidPost is a social enterprise registered in Canada since May 2013. It was founded by a group of senior aid professionals.</p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@stop

@section('after_content')
  
@stop