<div class="toppadding-sm bottompadding">

  <div ng-controller="AllNewsCtrl">

    <div class="fancy-title title-dotted-border title-center">
      <h3 class="">All News</h3>
    </div>

    <div class="container">
      {{-- <p style="word-wrap: break-word;"><% query %></p> --}}
      {{-- <p style="word-wrap: break-word;"><% info_text.filters_text.get() %></p> --}}
      <div class="news-filters row smallpadding hidden-sm">
        <div class="col-md-12 nopadding bottommargin">
          <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="country" data-toggle="dropdown">
              Country / Region
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu filter-countries" role="menu" aria-labelledby="country">
              <li role="presentation" ng-repeat="item in filters.regions" data-filter-type="region">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.country = -1; info_text.filters_text.country_text = ''; query.filters.region = item.id; info_text.filters_text.region_text = item.name"><% item.name %></a>
              </li>
              <li class="divider"></li>
              <li role="presentation" ng-repeat="item in filters.countries" data-filter-type="country">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.region = -1; info_text.filters_text.region_text = ''; query.filters.country = item.id; info_text.filters_text.country_text = item.name"><% item.name %></a>
              </li>
            </ul>
          </div>

          <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="crisis" data-toggle="dropdown">
              Crisis / Disaster
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu filter-crisis" role="menu" aria-labelledby="crisis">
              <li role="presentation" ng-repeat="item in filters.crisis">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.crisis = item.id; info_text.filters_text.crisis_text = item.name"><% item.name %></a>
              </li>
            </ul>
          </div>

          {{-- <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="intervention" data-toggle="dropdown">
              Intervention
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu" role="menu" aria-labelledby="intervention">
              <li role="presentation" ng-repeat="item in filters.interventions">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.intervention = item.id; info_text.filters_text.intervention_text = item.name"><% item.name %></a>
              </li>
            </ul>
          </div> --}}

          <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="theme" data-toggle="dropdown">
              Theme
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu filter-themes" role="menu" aria-labelledby="theme">
              <li role="presentation" ng-repeat="item in filters.themes">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.theme = item.id; info_text.filters_text.theme_text = item.name"><% item.name %></a>
              </li>
            </ul>
          </div>

          <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="sector" data-toggle="dropdown">
              Sector
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu filter-sectors" role="menu" aria-labelledby="sector">
              <li role="presentation" ng-repeat="item in filters.sectors">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.sector = item.id; info_text.filters_text.sector_text = item.name"><% item.name %></a>
              </li>
            </ul>
          </div>
          
          <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="beneficiary" data-toggle="dropdown">
              Beneficiary
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu filter-beneficiaries" role="menu" aria-labelledby="beneficiary">
              <li role="presentation" ng-repeat="item in filters.beneficiaries">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.beneficiary = item.id; info_text.filters_text.beneficiary_text = item.name"><% item.name %></a>
              </li>
            </ul>
          </div>

          <div class="dropdown col-md-2 xsmallpadding">
            <button class="btn btn-default dropdown-toggle" type="button" id="organisation-type" data-toggle="dropdown">
              Organisation Type
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-dropdown-menu filter-organisationtypes" role="menu" aria-labelledby="organisation-type" style="left: auto; right: 0;">
              <li role="presentation" ng-repeat="item in filters.organisation_types">
                <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.organisationType = item.id; info_text.filters_text.organisationType_text = item.display_name"><% item.display_name %></a>
              </li>
            </ul>
          </div>
            
          <div class="clear"></div>

          @include('pages.common._filter_info')
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 grey-bg clearfix filter-result-bar">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <strong><% record_count %></strong> <strong>News</strong> from <strong><% org_count %></strong> <strong>organisations</strong>
          </div>

          <div class="col-lg-3 col-md-3 col-lg-push-5 col-md-push-5 col-sm-3 col-sm-push-5 col-xs-12">
            <div class="input-group">
              <input type="text" class="form-control input-sm" placeholder="Enter Keywords" ng-enter="doSearch()" ng-model="searchQuery">
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="button" ng-click="doSearch()"><i class="icon-search" style="display:block; font-weight:600"></i></button>
              </span>
            </div><!-- /input-group -->
          </div><!-- / -->

          <div class="col-lg-5 col-md-5 col-lg-pull-3 col-md-pull-3 col-sm-5 col-sm-pull-3 col-xs-12">
            <span class="pull-left sort-by visible-xs">Sort By:</span>
            <ul class="nav nav-pills pull-right pull-left-on-xs">
              <li ng-class="{'active': query.sortBy === 'popularity'}"><a href="javascript:;" ng-click="query.sortBy = 'popularity'">Popularity</a></li>
              <li ng-class="{'active': query.sortBy === 'recent'}"><a href="javascript:;" ng-click="query.sortBy = 'recent'">Recent</a></li>
            </ul>
            <span class="pull-right sort-by hidden-xs">Sort By:</span>
          </div>  
        </div>

        <div class="clear"></div>
      </div>

      <div id="" class="portfolio-carousel row">

        <div class="oc-item zoomhover col-md-4 smallpadding col-sm-6" ng-repeat="item in items">
          <div class="ipost card-grey clearfix">
            <div class="portfolio-image">
              <a href="#" class="zoomeffect"><img src="/images/placeholder.gif" data-original="/imagecache/370_216<% item.image %>" alt="<% item.title %>" class="lazy"></a>
              <div class="portfolio-overlay gradient soft">
                {{-- <div class="label label-danger label-lg" ng-show="item.org_info.top_agency">Top Agency</div> --}}
                <div class="portfolio-desc">
                  <h3><span><strong><% item.time_ago %></strong> <strong><i class="icon-thumbs-up"></i> <% item.num_of_likes %></strong>  <strong><i class="icon-eye"></i> <% item.view_count %></strong></span></h3>
                </div>
              </div>
            </div>
            <div class="news-desc"><a href="<% item.view_url %>" data-clamp-height="70"><% item.title %></a></div>
            <div class="portfolio-desc">
              <a href="<% item.org_info.profile_url %>"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="<% item.org_info.logo %>" /></a>
              <div class="desc-content">
                <h3 data-clamp-height="20"><strong>{{-- <% item.org_info.type %> --}}<% item.sector_name %></strong></h3> 
                <span data-clamp-height="45" ng-show="!item.org_info.use_org_name_acronym"><a href="<% item.org_info.profile_url %>"><% item.org_info.name %></a></span>
                <span data-clamp-height="45" ng-show="item.org_info.use_org_name_acronym"><a href="<% item.org_info.profile_url %>"><% item.org_info.org_name_acronym %></a></span>
              </div><div class="clear"></div>
            </div>
          </div>
        </div>

        <div class="col-md-12 text-center topmargin-sm">
          <a href="javascript:;" class="btn btn-warning" style="text-align:center; padding:10px 60px;" ng-click="loadMore()" ng-hide="query.pageNumber >= page_count">Show More</a>
        </div>
        
      </div>

      <div class="clear"></div>

    </div>

  </div>

</div><!-- /ng-app -->