@extends('layouts.site')

@section('before_content')
  @include('pages.common._pub_big', array('model' => Publicities::getBigPublicity()))
  <div class="clear"></div>
@stop

@section('content')
  @include('pages.news._latest_news')
  @include('pages.news._top_news')
  @include('pages.news._all_news')
@stop

@section('scripts')
	<script type="text/javascript" src="{{ URL::asset('js/app/controller_news.js') }}"></script>
@stop