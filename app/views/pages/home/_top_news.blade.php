<!-- Top Headline Slider ============================================= -->

<section id="slider" class="swiper_wrapper clearfix">
  <div class="swiper-container swiper-parent">
    <div class="swiper-wrapper">

      @foreach ($topNews as $item)
        <div class="swiper-slide dark" style="background-image: url('/imagecache/1333_600{{$item->image}}'); background-position: center center;">
          <div class="slider-overlay"></div> <div class="slider-gradient"></div>
          <div class="container clearfix">
            <div class="slider-caption">
              <h2 data-swiper-parallax="-200"><span>Headline</span><a href="{{$item->present()->viewUrl()}}" class="aw-view-newsitem"><strong>{{$item->title}}</strong></a></h2>
              <a href="{{$item->present()->organisationInfo()->profile_url}}" class="aw-view-profile">
                <img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$item->present()->organisationInfo()->logo}}" data-swiper-parallax="-150" />
              </a>
              <p data-swiper-parallax="-150">
                <strong>{{$item->sector_name}}</strong><br>
                <a href="{{$item->present()->organisationInfo()->profile_url}}" class="aw-view-profile">{{$item->present()->organisationInfo()->name}}</a>
              </p>
            </div>
          </div>
        </div>
      @endforeach

    </div><!-- / swiper-wrapper -->
  </div><!-- / swiper-container swiper-parent -->

  <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
  <div id="slider-arrow-right" class="pushed"><i class="icon-angle-right"></i></div>

  <div class="gallery-thumbs gallery-thumbs-news hidden-sm hidden-xs">
    <div class="swiper-wrapper">
      @foreach ($topNews as $item)
        <div class="swiper-slide">
          <img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$item->present()->organisationInfo()->logo}}" /> <p data-clamp-height="50" style="padding-top: 0; padding-bottom: 0;">{{$item->title}}</p>
        </div>
      @endforeach
    </div>
  </div>
  <style>
    #slider .container {
      display: table;
    }
    #slider .slider-caption {
      display: table-cell;
      vertical-align: middle;
      position: relative;
    }
    #slider .slider-caption a strong {
      max-width: 420px;
    }
  </style>
  <script>
    jQuery(document).ready(function($){
      
      var swiperSlider = new Swiper('.swiper-parent',{
        parallax:true,
        autoplay: 5000,
        speed: 300,
        effect: 'fade',
        nextButton: '#slider-arrow-right',
        prevButton: '#slider-arrow-left',
        onTouchEnd: function(swiper, event) {
        }
      });
      
      var galleryThumbs = new Swiper('.gallery-thumbs-news', {
        spaceBetween: 1,
        direction: 'vertical',
        centeredSlides: true,
        slidesPerView: 7,
        slideToClickedSlide: true,
        onTouchEnd: function(swiper, event) {
        }
      });

      swiperSlider.params.control = galleryThumbs;
      galleryThumbs.params.control = swiperSlider;

    });
  </script>
</section>