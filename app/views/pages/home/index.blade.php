@extends('layouts.site')

@section('before_content')
  @include('pages.home._top_news')
@stop

@section('content')
  @include('pages.home._get_involved')
  @include('pages.home._featured_videos')
  @include('pages.home._most_popular')
@stop

@section('after_content')
  @include('pages.common._pub_big', array('model' => Publicities::getBigPublicity()))
@stop