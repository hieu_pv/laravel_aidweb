<!-- Featured Videos ============================================= -->

<div class="darkergrey-bg toppadding bottompadding">
  
  <div class="container">
    <div class="fancy-title title-dotted-border title-center">
      <h3 class="darkergrey-bg">Videos</h3>
    </div>

    <div id="featuredvideos" class="owl-carousel portfolio-carousel ">

      @foreach ($featuredVideos as $item)
        <div class="oc-item video-item zoomhover card-bright">
          <div class="iportfolio video-portfolio">
            <div class="portfolio-image home-video-item-image">
              <a href="#" class="zoomeffect"><img data-original="{{$item->image}}" src="/images/placeholder.gif" alt="video title" class="lazy"></a>
              <div class="portfolio-overlay flat">
                <a href="{{$item->watchUrl()}}" class="left-icon topleft" data-lightbox="aidweb" data-desc="{{nl2br(htmlspecialchars($item->description))}}" data-likes="{{$item->num_of_likes}}" data-id="{{$item->id}}"><i class="icon-line-play"></i></a>
                <div class="portfolio-desc">
                  <span style="border:none; margin-bottom:30px; padding-top: 0; padding-bottom: 0;" data-clamp-height="100">
                    {{ nl2br(htmlspecialchars($item->description)) }}
                  </span>
                </div>
              </div>
              <span class="duration"><i class="icon-stopwatch"></i> {{$item->displayed_duration}}</span>
            </div>
            <div class="news-desc"><a href="{{$item->watchUrl()}}" data-lightbox="aidweb" data-clamp-height="70" data-desc="{{nl2br(htmlspecialchars($item->description))}}" data-likes="{{$item->num_of_likes}}" data-id="{{$item->id}}">{{$item->title}}</a></div>
            <div class="portfolio-desc">
              <a href="{{$item->present()->organisationInfo()->profile_url}}"><img class="org-logo lazy" data-original="{{$item->present()->organisationInfo()->logo}}" src="/images/placeholder.gif"/></a>
              <div class="desc-content">
                <h3><a><strong>{{$item->sector_name}}</strong></a></h3>
                <span><a href="{{$item->present()->organisationInfo()->profile_url}}">{{ ($item->related_organisation->use_org_name_acronym) ? $item->related_organisation->org_name_acronym : $item->related_organisation->org_name }}</a></span>
              </div>
            </div>
          </div>
        </div>
      @endforeach

    </div>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
        function findBootstrapEnvironment() {
            var envs = ['xs', 'sm', 'md', 'lg'];

            $el = $('<div>');
            $el.appendTo($('body'));

            for (var i = envs.length - 1; i >= 0; i--) {
                var env = envs[i];

                $el.addClass('hidden-'+env);
                if ($el.is(':hidden')) {
                    $el.remove();
                    return env
                }
            };
        }
        if(findBootstrapEnvironment() != 'xs') {
          var ocNews = $("#featuredvideos");
          ocNews.owlCarousel({
            margin: 15,
            nav: false,
            navText: ['<i class="icon-angle-left alt"></i>','<i class="icon-angle-right alt"></i>'],
            autoplay: false,
            autoplayHoverPause: false,
            dots: false,
            responsive:{
              0:{ items:1 },
              600:{ items:2 },
              1000:{ items:3 },
              1200:{ items:3 }
            }
          });
        } else {
          $('#featuredvideos').removeClass('owl-carousel');
        }
      });
    </script>

    <div class="clear"></div>
  </div>
</div>

