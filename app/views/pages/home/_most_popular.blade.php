<!-- Most Popular ============================================= -->
<section id="most-popular" class="hidden-xs">
  <div class="nobottommargin clearfix" id="">

    <div class="container toppadding darktab text-center">
      <h3 class="text-white">Most Popular:</h3>
      <ul class="nav nav-tabs tab-nav clearfix text-center">
        <li role="presentation" class="active"><a href="#tabs-1" aria-controls="home" role="tab" data-toggle="tab" class="text-white">Blogs</a></li>
        <li role="presentation"><a href="#tabs-2" aria-controls="home" role="tab" data-toggle="tab" class="text-white">News</a></li>
        <li role="presentation"><a href="#tabs-3" aria-controls="home" role="tab" data-toggle="tab" class="text-white">Videos</a></li>
      </ul>
    </div>

    <div class="bottompadding">
      <div class="tab-container container tab-content">

        <!-- Popular Blogs ============================================= -->
        <div class="tab-pane active clearfix" id="tabs-1">
          <div id="oc-portfolio-1" class="owl-carousel owl-carousel-full portfolio-carousel">
            @foreach ($mostPopularBlogPosts as $item)
              <div class="oc-item zoomhover card-white">
                <div class="iportfolio">
                  <div class="portfolio-image">
                    <a href="{{$item->view_url}}" class="zoomeffect">
                      <img src="/imagecache/380_222_c{{$item->image}}" alt="{{$item->title}}">
                    </a>
                    <div class="portfolio-overlay gradient soft">
                      <div class="portfolio-desc">
                        <h3>
                          <span><strong>{{$item->time_ago}}</strong>  <strong><i class="icon-thumbs-up"></i> {{$item->num_of_likes}}</strong>  <strong><i class="icon-eye"></i> {{$item->view_count}}</strong></span>
                        </h3>
                      </div>
                    </div>
                  </div>
                  <div class="news-desc"><a href="{{$item->view_url}}" data-clamp-height="70">{{$item->title}}</a> </div>
                  <div class="portfolio-desc">
                    <a href="{{$item->user_profile_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$item->user_profile_info->avatar}}" /></a>
                    <div class="desc-content">
                      <h3><a href="{{$item->user_profile_info->profile_url}}">{{$item->user_profile_info->full_name}}</a></h3>
                      @if (strlen($item->user_profile_info->employer_name) > 0)
                        <span>{{$item->user_profile_info->employer_name}}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="clear"></div>
        </div>

        <!-- Popular News ============================================= -->
        <div class="tab-pane clearfix" id="tabs-2">
          <div id="oc-portfolio-2" class="owl-carousel owl-carousel-full portfolio-carousel">
            @foreach ($mostPopularNews as $item)
              <div class="oc-item zoomhover card-white" data-id="{{ $item->id }}">
                <div class="iportfolio">
                  <div class="portfolio-image">
                    <a href="#" class="zoomeffect">
                      <img src="/imagecache/380_222_c{{ $item->image }}" alt="{{ $item->title }}">
                    </a>
                    <div class="portfolio-overlay gradient soft">
                      <div class="portfolio-desc">
                        <h3>
                          <span><strong>{{ $item->time_ago }}</strong>  <strong><i class="icon-thumbs-up"></i> {{ $item->num_of_likes }}</strong>  <strong><i class="icon-eye"></i> {{ $item->view_count }}</strong></span>
                        </h3>
                      </div>
                    </div>
                  </div>
                  <div class="news-desc"><a href="{{ $item->view_url }}" data-clamp-height="70">{{ $item->title }}</a></div>
                  <div class="portfolio-desc">
                    <a href="{{ $item->org_info->profile_url }}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{ $item->org_info->logo }}"/></a>
                    <div class="desc-content">
                      <h3><a href="javascript:;"><strong>{{ $item->sector_name }}</strong></a></h3>
                      <span><a href="{{ $item->org_info->profile_url }}">{{ ($item->org_info->use_org_name_acronym) ? $item->org_info->org_name_acronym : $item->org_info->name }}</a></span>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="clear"></div>
        </div>

        <!-- Popular Videos ============================================= -->
        <div class="tab-pane clearfix" id="tabs-3">
          <div id="oc-portfolio-3" class="owl-carousel owl-carousel-full portfolio-carousel">
            @foreach ($mostPopularVideos as $item)
              <div class="oc-item video-item zoomhover card-white">
                <div class="iportfolio">
                  <div class="portfolio-image home-video-item-image">
                    <a href="#" class="zoomeffect">
                      <img src="{{ $item->image }}" alt="{{ $item->title }}">
                    </a>
                    <div class="portfolio-overlay flat">
                      <a href="{{ $item->watch_url }}" class="left-icon topleft" data-lightbox="aidweb" data-desc="{{nl2br(htmlspecialchars($item->description))}}" data-likes="{{$item->num_of_likes}}" data-id="{{$item->id}}"><i class="icon-line-play"></i></a>
                      <div class="portfolio-desc">
                        <span style="border:none; margin-bottom:30px; padding-top: 0; padding-bottom: 0; overflow: hidden" data-clamp-height="110">
                          {{ nl2br(htmlspecialchars($item->description)) }}
                        </span>
                        <h3 class="videoattr"><span><strong>{{ $item->time_ago }}</strong> <strong><i class="icon-thumbs-up"></i> {{ $item->num_of_likes }}</strong>  <strong><i class="icon-eye"></i> {{ $item->view_count }}</strong></span></h3>
                      </div>
                    </div>
                    <span class="duration"><i class="icon-stopwatch"></i> {{ $item->duration }}</span>
                  </div>
                  <div class="news-desc"><a href="{{ $item->watch_url }}" data-lightbox="aidweb" data-clamp-height="70" data-desc="{{nl2br(htmlspecialchars($item->description))}}" data-likes="{{$item->num_of_likes}}" data-id="{{$item->id}}">{{ $item->title }}</a> </div>
                  <div class="portfolio-desc">
                    <a href="{{ $item->org_info->profile_url }}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{ $item->org_info->logo }}" /></a>
                    <div class="desc-content">
                      <h3><a><strong>{{ $item->sector_name }}</strong></a></h3>
                      <span><a href="{{ $item->org_info->profile_url }}">{{ ($item->org_info->use_org_name_acronym) ? $item->org_info->org_name_acronym : $item->org_info->name }}</a></span>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="clear"></div>
        </div>

      </div><!-- / tab-container container -->

      <script type="text/javascript">
        jQuery(document).ready(function($) {
          var ocPortfolio = $("#oc-portfolio-1");
          ocPortfolio.owlCarousel({
            margin: 15,
            nav: false,
            navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
            autoplay: false,
            autoplayHoverPause: false,
            dots: false,
            responsive:{
              0:{ items:1 },
              600:{ items:2 },
              1000:{ items:3 },
              1200:{ items:3 }
            }
          });
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var id = $(e.target).attr('href');
            var index = id.substr(6,1);
            console.log(index);
            var ocPortfolio = $("#oc-portfolio-"+index);
            ocPortfolio.owlCarousel({
              margin: 15,
              nav: false,
              navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
              autoplay: false,
              autoplayHoverPause: false,
              dots: false,
              responsive:{
                0:{ items:1 },
                600:{ items:2 },
                1000:{ items:3 },
                1200:{ items:3 }
              }
            });
          })
        });
      </script>

    </div><!-- /bottompadding -->

  </div><!-- #sidebar-tabs -->
</section><!-- #most-popular -->

<section id="most-popular-mobile" class="hidden-lg hidden-md hidden-sm">
  <!-- Most Popular Blogs -->
  <div id="popular-blogs" class="darkergrey-bg toppadding bottompadding">
    <div class="container">
      <div class="fancy-title title-dotted-border title-center">
        <h3 class="darkergrey-bg">Popular Blogs</h3>
      </div>
       @foreach ($mostPopularBlogPosts as $item)
        <div class="oc-item zoomhover card-white">
          <div class="iportfolio">
            <div class="portfolio-image">
              <a href="{{$item->view_url}}" class="zoomeffect">
                <img src="/images/placeholder.gif" data-original="/imagecache/380_222_c{{$item->image}}" alt="{{$item->title}}" class="lazy">
              </a>
              <div class="portfolio-overlay gradient soft">
                <div class="portfolio-desc">
                  <h3>
                    <span><strong>{{$item->time_ago}}</strong>  <strong><i class="icon-thumbs-up"></i> {{$item->num_of_likes}}</strong>  <strong><i class="icon-eye"></i> {{$item->view_count}}</strong></span>
                  </h3>
                </div>
              </div>
            </div>
            <div class="news-desc"><a href="{{$item->view_url}}" data-clamp-height="70">{{$item->title}}</a> </div>
            <div class="portfolio-desc">
              <a href="{{$item->user_profile_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$item->user_profile_info->avatar}}" /></a>
              <div class="desc-content">
                <h3><a href="{{$item->user_profile_info->profile_url}}">{{$item->user_profile_info->full_name}}</a></h3>
                @if (strlen($item->user_profile_info->employer_name) > 0)
                  <span>{{$item->user_profile_info->employer_name}}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  <!-- Most Popular News -->
  <div id="popular-news" class="darkergrey-bg toppadding bottompadding">
    <div class="container">
      <div class="fancy-title title-dotted-border title-center">
        <h3 class="darkergrey-bg">Popular News</h3>
      </div>
      @foreach ($mostPopularNews as $item)
        <div class="oc-item zoomhover card-white" data-id="{{ $item->id }}">
          <div class="iportfolio">
            <div class="portfolio-image">
              <a href="#" class="zoomeffect">
                <img src="/images/placeholder.gif" data-original="/imagecache/380_222_c{{ $item->image }}" alt="{{ $item->title }}" class="lazy">
              </a>
              <div class="portfolio-overlay gradient soft">
                <div class="portfolio-desc">
                  <h3>
                    <span><strong>{{ $item->time_ago }}</strong>  <strong><i class="icon-thumbs-up"></i> {{ $item->num_of_likes }}</strong>  <strong><i class="icon-eye"></i> {{ $item->view_count }}</strong></span>
                  </h3>
                </div>
              </div>
            </div>
            <div class="news-desc"><a href="{{ $item->view_url }}" data-clamp-height="70">{{ $item->title }}</a></div>
            <div class="portfolio-desc">
              <a href="{{ $item->org_info->profile_url }}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{ $item->org_info->logo }}"/></a>
              <div class="desc-content">
                {{-- <h3><a href="javascript:;"><strong>{{ $item->org_info->type }}</strong></a></h3> --}}
                <h3><a href="javascript:;"><strong>{{ $item->sector_name }}</strong></a></h3>
                <span><a href="{{ $item->org_info->profile_url }}">{{ $item->org_info->name }}</a></span>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  <!-- Most Popular Videos -->
  <div id="popular-videos" class="darkergrey-bg toppadding bottompadding">
    <div class="container">
      <div class="fancy-title title-dotted-border title-center">
        <h3 class="darkergrey-bg">Popular Videos</h3>
      </div>
      @foreach ($mostPopularVideos as $item)
        <div class="oc-item video-item zoomhover card-white">
          <div class="iportfolio">
            <div class="portfolio-image home-video-item-image">
              <a href="#" class="zoomeffect">
                <img src="/images/placeholder.gif" data-original="{{ $item->image }}" alt="{{ $item->title }}" class="lazy">
              </a>
              <div class="portfolio-overlay flat">
                <a href="{{ $item->watch_url }}" class="left-icon topleft" data-lightbox="aidweb" data-desc="{{nl2br(htmlspecialchars($item->description))}}" data-likes="{{$item->num_of_likes}}" data-id="{{$item->id}}"><i class="icon-line-play"></i></a>
                <div class="portfolio-desc">
                  <span style="border:none; margin-bottom:30px; padding-top: 0; padding-bottom: 0;" data-clamp-height="80">
                    {{ nl2br(htmlspecialchars($item->description)) }}
                  </span>
                  <h3 class="videoattr"><span><strong>{{ $item->time_ago }}</strong> <strong><i class="icon-thumbs-up"></i> {{ $item->num_of_likes }}</strong>  <strong><i class="icon-eye"></i> {{ $item->view_count }}</strong></span></h3>
                </div>
              </div>
              <span class="duration"><i class="icon-stopwatch"></i> {{ $item->duration }}</span>
            </div>
            <div class="news-desc"><a href="{{ $item->watch_url }}" data-lightbox="aidweb" data-clamp-height="70" data-desc="{{nl2br(htmlspecialchars($item->description))}}" data-likes="{{$item->num_of_likes}}" data-id="{{$item->id}}">{{ $item->title }}</a> </div>
            <div class="portfolio-desc">
              <a href="{{ $item->org_info->profile_url }}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{ $item->org_info->logo }}" /></a>
              <div class="desc-content">
                <h3><a><strong>{{ $item->sector_name }}</strong></a></h3>
                <span><a href="{{ $item->org_info->profile_url }}">{{ $item->org_info->name }}</a></span>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</section>