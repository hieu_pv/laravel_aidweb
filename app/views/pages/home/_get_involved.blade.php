<!-- Get Involved ============================================= -->

<div class="toppadding-lg bottompadding-lg">
  <div class="container">
    <div class="fancy-title title-dotted-border title-center">
      <h3>Get Involved</h3>
    </div>

    <div id="oc-action-1" class="owl-carousel portfolio-carousel get-involved-home">
      
      @foreach ($topOneTakeActionPerType as $item)
        <div class="oc-item card zoomhover">
          {{--strtolower($item->typeOfTakeAction->name)--}}
          <span class="action" style="background-color: {{strtolower($item->typeOfTakeAction->color)}};">{{strtoupper($item->typeOfTakeAction->name)}}</span>
          <div class="action-desc">
            <h3><a href="{{$item->url}}">{{$item->title}}</a></h3>
            <p data-clamp-height="70">{{$item->content}}</p>
          </div>
          <div class="portfolio-desc">
            <a href="{{$item->present()->organisationInfo()->profile_url}}"><img class="org-logo" src="{{$item->present()->organisationInfo()->logo}}" /></a>
            <div class="desc-content">
              <h3><strong>{{$item->sector_name}}</strong></h3>
              <span class="desc"><a href="{{$item->present()->organisationInfo()->profile_url}}">{{ ($item->related_organisation->use_org_name_acronym) ? $item->related_organisation->org_name_acronym : $item->related_organisation->org_name }}</a></span>
            </div>
          </div>
          <div class="zoomeffect home-get-involved-item-image">
            <img src="/imagecache/360_211{{$item->image}}" alt="">
          </div>
        </div>
      @endforeach

    </div>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
        var ocNews = $("#oc-action-1");
        ocNews.owlCarousel({
          margin: 10,
          nav: true,
          navText: ['<i class="icon-angle-left alt"></i>','<i class="icon-angle-right alt"></i>'],
          autoplay: false,
          autoplayHoverPause: false,
          dots: false,
          responsive:{
            0:{ items:1 },
            600:{ items:2 },
            1000:{ items:3 },
            1200:{ items:3 }
          }
        });
      });
    </script>

    <div class="clear"></div>
  </div>
</div>
