@extends('layouts.site')

@section('before_content')
  @include('pages.common._pub_big', array('model' => Publicities::getBigPublicity()))
  <div class="clear"></div>
@stop

@section('content')
  @include('pages.takeactions._top_records')
  @include('pages.takeactions._all_records')
@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_takeactions.js') }}"></script>
@stop