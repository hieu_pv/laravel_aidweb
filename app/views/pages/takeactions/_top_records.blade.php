@include('pages.takeactions._top_records_style')
<div class="container topmargin bottommargin hidden-xs hidden-sm">
  <div class="fancy-title title-dotted-border title-center">
    <h3 class="">Featured Ways to Help</h3>
  </div>
  <div class="hidden-xs">
    <ul class="tab-nav clearfix countries" id="type_for_top_tabs">
      @foreach ($typesForTop->types as $item)
        <li data-ta-type="{{$item->name}}">
          <a href="#toprecords_region_{{$item->id}}" data-toggle="tab" data-id="{{$item->id}}" style="background-color: {{$item->color}}; border-bottom-color: {{$item->color}}; color: #fff;">{{$item->name}}</a>
        </li>
      @endforeach
    </ul>
  </div>
  <div class="clear"></div>
  <div class="tab-content featured-news">
    @foreach ($typesForTop->types as $item)
      <div role="tabpanel" class="tab-pane fade" id="toprecords_region_{{$item->id}}">
        <div class="col-md-12 hidden-xs nomargin nopadding">
          <section id="" class="swiper_wrapper video-slider clearfix">
            <div class="swiper-container swiper-video-parent swiper-toprecords-parent-{{$item->id}}">
              <div class="swiper-wrapper">
                @foreach ($item->top_records as $record)
                  <div class="swiper-slide">
                    <div class="slideimg col-md-8 nomargin nopadding" style="background-image: url('/imagecache/r_760_auto_c{{$record->image}}'); background-position: center center;"></div>
                    <div class="slidecontent col-md-4 nomargin">
                      <div class="slider-caption">
                        <a href="{{$record->org_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$record->org_info->logo}}" data-swiper-parallax="-150" /></a>
                        <p data-swiper-parallax="-150">
                          <strong>{{$record->sector_name}}</strong><br>
                          <a href="{{$record->org_info->profile_url}}">{{$record->org_info->name}}</a>
                        </p>
                        <div class="clear"></div>
                        <p>&nbsp;</p>
                        <div class="clear"></div>
                        <h2 data-swiper-parallax="-200"><a href="{{$record->view_url}}"><strong>{{$record->title}}</strong></a></h2>
                        <p data-swiper-parallax="-100" data-clamp-height="94">{{str_ireplace('</p>','',str_ireplace('<p>','',nl2br($record->teaser)))}}</p>
                        <div class="clear"></div>
                        <a href="{{$record->view_url}}" target="_blank" class="btn btn-default" data-swiper-parallax="-180" style="transform: translate3d(180px, 0px, 0px); -webkit-transform: translate3d(180px, 0px, 0px); transition-duration: 0ms; -webkit-transition-duration: 0ms;">Take Action Now!</a>
                        <div class="clear"></div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
            {{-- <div id="slider-video-arrow-left"><i class="icon-angle-left"></i></div>
            <div id="slider-video-arrow-right" class="pushed"><i class="icon-angle-right"></i></div> --}}
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-{{$item->id}}"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-next-{{$item->id}}"></div>
            <div class="swiper-button-prev swiper-button-prev-{{$item->id}}"></div>
          </section>
        </div>
        <div class="clear"></div>
        {{-- <div class="hidden-sm hidden-xs news-slider-thumbs nomargin">
          <div class="gallery-thumbs gallery-thumbs-news gallery-thumbs-toprecords-{{$item->id}} hidden-sm hidden-xs">
            <div class="swiper-wrapper">
              @foreach ($item->top_records as $record)
                <div class="swiper-slide"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$record->org_info->logo}}" /> <p data-clamp-height="50" style="padding-top: 0; padding-bottom: 0;">{{$record->title}}</p></div>
              @endforeach
            </div>
          </div>
        </div> --}}
        <div class="clear"></div>
      </div>
    @endforeach
  </div>

  <script>
    function setupSlider (id) {
      if ($('.swiper-toprecords-parent-' + id).length > 0) {
        var existing1 = $('.swiper-toprecords-parent-' + id)[0].swiper;
        if (existing1) {
          existing1.slideTo(0);
        }
        // var existing2 = $('.gallery-thumbs-toprecords-' + id)[0].swiper;
        // if (existing2) {
        //   existing2.slideTo(0);
        // }
      }
      var swiperSlider = new Swiper('.swiper-toprecords-parent-' + id, {
        parallax:true,
        autoplay: 5000,
        speed: 300,
        effect: 'fade',
        // nextButton: '#slider-video-arrow-right',
        // prevButton: '#slider-video-arrow-left',
        pagination: '.swiper-pagination-' + id,
        paginationClickable: true,
        nextButton: '.swiper-button-next-' + id,
        prevButton: '.swiper-button-prev-' + id,
      });
      // var galleryThumbs = new Swiper('.gallery-thumbs-toprecords-' + id, {
      //   spaceBetween: 0,
      //   direction: 'horizontal',
      //   centeredSlides: true,
      //   slidesPerView: 4,
      //   slideToClickedSlide: true
      // });
      // swiperSlider.params.control = galleryThumbs;
      // galleryThumbs.params.control = swiperSlider;
    }

    jQuery(document).ready(function($){
      $('#type_for_top_tabs li:first-child a').click();
      $('#type_for_top_tabs li:first-child a').closest('li').addClass('ui-tabs-active ui-state-active').click();
      setupSlider($('#type_for_top_tabs li:first-child a').attr('data-id'));

      $('#type_for_top_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(e.relatedTarget).closest('li').removeClass('ui-tabs-active ui-state-active');
        $(e.target).closest('li').addClass('ui-tabs-active ui-state-active');
        var id = $(e.target).attr('data-id');
        setupSlider(id);
      });
    });
  </script>
</div>
<!-- 
<div class="container topmargin bottommargin hidden-md hidden-lg">
  <div class="fancy-title title-dotted-border title-center">
    <h3 class="">Featured Ways to Help</h3>
  </div>
  <div id="" class="portfolio-carousel row">
    @foreach ($typesForTop->types as $type)
      @foreach ($type->top_records as $item)
        <div class="oc-item zoomhover col-md-4 smallpadding col-sm-6 get-involed">
          <div class="ipost card-grey clearfix">
            <div class="portfolio-image">
              <a href="#" class="zoomeffect"><img src="/images/placeholder.gif" data-original="/imagecache/370_216{{$item->image}}" alt="{{$item->title}}" class="lazy"></a>
              <div class="portfolio-overlay flat">
                <div class="portfolio-desc">
                  <h3><a href="#">{{$item->type_name}}</a></h3>
                  <span>
                    <span data-clamp-height="65">{{nl2br($item->content)}}</span>
                    <a href="{{$item->view_url}}" class="btn action btn-default hidden-md">Take Action</a>
                  </span>
                </div>
              </div>
            </div>
            <div class="portfolio-desc">
              <a href="{{$item->org_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$item->org_info->logo}}" /></a>
              <div class="desc-content">
                <h3><a href="{{$item->view_url}}"><strong>{{$item->title}}</strong></a></h3>
                <span><a href="{{$item->org_info->profile_url}}">{{$item->org_info->name}}</a></span>
              </div><div class="clear"></div>
            </div>
          </div>
        </div>
      @endforeach
    @endforeach
  </div>
  <div class="clear"></div>
</div> -->