@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="container">
    <div id="" class="portfolio-carousel row">

      <div class="oc-item zoomhover col-md-offset-4 col-md-4 smallpadding col-sm-offset-6 col-sm-6">
        <div class="ipost card-grey clearfix">
          <div class="portfolio-image">
            <a href="#" class="zoomeffect">
              @if (GenericUtility::startsWith($model->image, 'data:'))
                <img src="{{$model->image}}" alt="{{$model->title}}">
              @else
                <img src="/imagecache/370_216{{$model->image}}" alt="{{$model->title}}">
              @endif
            </a>
            <div class="portfolio-overlay flat">
              <div class="portfolio-desc">
                <h3><a href="#">{{$model->action_type_name}}</a></h3>
                <span>{{$model->content}}
                  <br><a href="{{$model->url}}" class="btn action btn-default hidden-md">Take Action</a>
                </span>
              </div>
            </div>
          </div>
          <div class="portfolio-desc" style="min-height:105px;">
            <a href="{{$model->present()->organisationInfo()->profile_url}}"><img class="org-logo" src="{{$model->present()->organisationInfo()->logo}}" /></a>
            <div class="desc-content">
              <h3><a href="{{$model->url}}"><strong>{{$model->title}}</strong></a></h3>
              <span><a href="{{$model->present()->organisationInfo()->profile_url}}">{{$model->present()->organisationInfo()->name}}</a></span>
            </div><div class="clear"></div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
@stop

@section('scripts')
  
@stop