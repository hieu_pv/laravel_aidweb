@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
<div ng-controller="ProfileOrgCtrl">
  <input type="hidden" ng-init="organisationId = {{$model->present()->organisationInfo()->id}}">

  <div class="grey-bg toppadding bottompadding">
    <div class="container">
      <div class="col-sm-3">
        <img src="{{$model->present()->organisationInfo()->logo_big}}" class="profpic" />
        <div class="si-share text-center">
          @if (isset($model->present()->organisationInfo()->website_url) && strlen($model->present()->organisationInfo()->website_url) > 0 && strtolower($model->present()->organisationInfo()->website_url) !== 'http://') 
            <a href="{{GenericUtility::prefixWithHttpIfNeeded($model->present()->organisationInfo()->website_url)}}" class="social-icon si-borderless si-cc" target="_blank">
              <i class="icon-globe"></i>
              <i class="icon-globe"></i>
            </a>
          @endif

          @if (isset($model->present()->organisationInfo()->facebook_url) && strlen($model->present()->organisationInfo()->facebook_url) > 0 && strtolower($model->present()->organisationInfo()->facebook_url) !== 'http://') 
            <a href="{{$model->present()->organisationInfo()->facebook_url}}" class="social-icon si-borderless si-facebook" target="_blank">
              <i class="icon-facebook"></i>
              <i class="icon-facebook"></i>
            </a>
          @endif 

          @if (isset($model->present()->organisationInfo()->twitter_url) && strlen($model->present()->organisationInfo()->twitter_url) > 0 && strtolower($model->present()->organisationInfo()->twitter_url) !== 'http://') 
            <a href="{{$model->present()->organisationInfo()->twitter_url}}" class="social-icon si-borderless si-twitter" target="_blank">
              <i class="icon-twitter"></i>
              <i class="icon-twitter"></i>
            </a>
          @endif
        </div>
      </div>
      <div class="col-sm-9 org-info">
        {{--$model->present()->organisationInfo()->office_location_name--}}
          <p class="org-name">{{$model->present()->organisationInfo()->name}}</p>
          <p class="org-location"><span>{{$model->present()->organisationInfo()->type_full_name}}, </span><span>{{$model->present()->organisationInfo()->office_location_name}}</span></p>
        <div class="org-desc">
          <div class="col-md-12">
            <p>{{$model->present()->organisationInfo()->about}}</p>

          </div>
          {{-- <div class="col-md-5">
            <ul class="portfolio-meta">
              <!-- <li class=""><div><span>Top Blogger Platform:</span> International</div></li> -->
              <li class=""><div><span style="vertical-align:top;">Organisation Type:</span> <span style="font-weight:normal;">{{$model->present()->organisationInfo()->type_full_name}}</span></div></li>
              <li class=""><div><span style="vertical-align:top;">News Posted:</span> <span style="font-weight:normal;"><%tabs['news'].recordCount%></span></div></li>
              <li class=""><div><span style="vertical-align:top;">Videos Posted:</span> <span style="font-weight:normal;"><%tabs['videos'].recordCount%></span></div></li>
              <li class=""><div><span style="vertical-align:top;">Get Involved Posted:</span> <span style="font-weight:normal;"><%tabs['getinvolved'].recordCount%></span></div></li>
            </ul>
          </div> --}}
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>

  <div class="toppadding bottompadding">
    <div class="container">
      <div class="bottommargin">
        <ul class="tab-nav clearfix countries" id="tab_nav_org_modules">
          <li class="ui-tabs-active ui-state-active"><a href="#tab_news" data-toggle="tab" data-mkey="news">News <span class="">(<%tabs['news'].recordCount%>)</span></a></li>
          <li><a href="#tab_videos" data-toggle="tab" data-mkey="videos">Videos <span class="">(<%tabs['videos'].recordCount%>)</span></a></li>
          <li><a href="#tab_getinvolved" data-toggle="tab" data-mkey="getinvolved">Get Involved <span class="">(<%tabs['getinvolved'].recordCount%>)</span></a></li>
        </ul>
      </div>
      <br>
      <div id="org-profile-tabpanel" class="tab-content">

        <div role="tabpanel" class="tab-pane fade" id="tab_news">
          <div class="horiz-post-wrapper" style="display:none;">
            <div id="posts" class="horiz-post row">
              <div class="entry clearfix row zoomhover" ng-repeat="item in tabs['news'].items">
                <div class="col-md-4 org-profile-news">
                  <div class="zoomeffect">
                    <img class="image_fade" ng-src="<%item.image%>" alt="<%item.title%>">
                    <div class="portfolio-desc">
                      <h3><span><strong><% item.time_ago %></strong> <strong><i class="icon-thumbs-up"></i> <% item.num_of_likes %></strong>  <strong><i class="icon-eye"></i> <% item.view_count %></strong></span></h3>
                    </div>
                  </div>
                </div>
                <div class="entry-c col-md-8">
                  <ul class="entry-meta">
                    <li><%item.time_ago%></li>
                    <li><i class="icon-thumbs-up"></i> <%item.num_of_likes%></li>
                    <li><i class="icon-eye"></i> <%item.view_count%></li>
                  </ul>
                  <div class="clear"></div>
                  <div class="entry-title clearfix">
                    <h3><a href="<%item.view_url%>"><%item.title%></a></h3>
                  </div>
                  <div class="entry-content">
                    <p ng-bind-html="item.teaser"></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 text-center topmargin-sm">
              <a href="#" ng-click="tabs['news'].showMore($event)" class="btn btn-warning" style="text-align:center; padding:10px 60px;" ng-hide="tabs['news'].pageNumber === tabs['news'].pageCount">Show More</a>
            </div>
            <div class="clear"></div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_videos">
          <div class="horiz-post-wrapper" style="display:none;">
            <div id="posts2" class="horiz-post row">
              <div class="oc-item video-item zoomhover col-md-4 smallpadding col-sm-6" ng-repeat="item in tabs['videos'].items">
                <div class="ipost card-grey clearfix">
                  <div class="portfolio-image home-video-item-image">
                    <a href="#" class="zoomeffect"><img ng-src="<% item.image %>" alt="<% item.title %>"></a>
                    <div class="portfolio-overlay flat">
                      <a href="<% item.watch_url %>" class="left-icon topleft" data-lightbox="aidweb" data-id="<% item.id %>"><i class="icon-line-play"></i></a>
                      <div class="portfolio-desc">
                        <span style="border:none; padding-bottom:15px; max-height: 120px; overflow: hidden">
                          <% item.description %>
                        </span>
                        <h3 class="videoattr"><span><strong><% item.time_ago %></strong> <strong><i class="icon-thumbs-up"></i> <% item.num_of_likes %></strong></span></h3>
                      </div>
                    </div>
                    <span class="duration"><i class="icon-stopwatch"></i> <% item.duration %></span>
                  </div>
                  <div class="news-desc"><a href="<% item.watch_url %>" data-lightbox="aidweb" data-id="<% item.id %>"><% item.title %></a></div>
                  <div class="portfolio-desc">
                    <a href="<% item.org_info.profile_url %>"><img class="org-logo" ng-src="<% item.org_info.logo %>" /></a>
                    <div class="desc-content">
                      <h3><strong><% item.org_info.type %></strong></h3>
                      <span><a href="<% item.org_info.profile_url %>"><% item.org_info.name %></a></span>
                    </div><div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 text-center topmargin-sm">
              <a href="#" ng-click="tabs['videos'].showMore($event)" class="btn btn-warning" style="text-align:center; padding:10px 60px;" ng-hide="tabs['videos'].pageNumber === tabs['videos'].pageCount">Show More</a>
            </div>
            <div class="clear"></div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_getinvolved">
          <div class="horiz-post-wrapper" style="display:none;">
            <div id="posts3" class="horiz-post row">
              <div class="oc-item zoomhover col-md-4 smallpadding get-involed col-sm-6" ng-repeat="item in tabs['getinvolved'].items">
                <div class="ipost card-grey clearfix">
                  <div class="portfolio-image">
                    <a href="#" class="zoomeffect"><img ng-src="/imagecache/360_211<% item.image %>" alt="<% item.title %>"></a>
                    <div class="portfolio-overlay flat">
                      <div class="portfolio-desc">
                        <h3><a href="#"><% item.type_name %></a></h3>
                        <span><% item.content %>
                          <br><a href="<% item.view_url %>" class="btn action btn-default">Take Action</a>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="portfolio-desc">
                    <a href="<% item.org_info.profile_url %>"><img class="org-logo" ng-src="<% item.org_info.logo %>" /></a>
                    <div class="desc-content">
                      <h3><a href="<% item.view_url %>"><strong><% item.title %></strong></a></h3>
                      <span><a href="<% item.org_info.profile_url %>"><% item.org_info.name %></a></span>
                    </div><div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 text-center topmargin-sm">
              <a href="#" ng-click="tabs['getinvolved'].showMore($event)" class="btn btn-warning" style="text-align:center; padding:10px 60px;" ng-hide="tabs['getinvolved'].pageNumber === tabs['getinvolved'].pageCount">Show More</a>
            </div>
            <div class="clear"></div>
          </div>
        </div>

      </div><!-- /tab-content -->
    </div>
  </div>

  {{-- <div class="toppadding bottompadding">
    <div class="container">
      <div class="fancy-title title-dotted-border title-center">
        <h3>Blogs from this Author</h3>
      </div>
      <br>
      <div id="posts" class="horiz-post row">
        <div class="entry clearfix row zoomhover" ng-repeat="item in posts">
          <div class="col-md-4">
            <a href="<%item.view_url%>" class="zoomeffect"><img class="image_fade" ng-src="<%item.image%>" alt="<%item.title%>"></a>
          </div>
          <div class="entry-c col-md-8">
            <ul class="entry-meta clearfix">
              <li><%item.time_ago%></li>
              <li><i class="icon-thumbs-up"></i> <%item.num_of_likes%></li>
              <li><i class="icon-eye"></i> <%item.view_count%></li>
            </ul>
            <div class="clear"></div>
            <div class="entry-title clearfix">
              <h3><a href="<%item.view_url%>"><%item.title%></a></h3>
            </div>
            <div class="entry-content">
              <p ng-bind-html="item.teaser"></p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 text-center topmargin-sm"><a href="#" ng-click="showMore($event)" class="btn btn-warning" style="text-align:center; padding:10px 60px;">Show More</a></div>
    </div>
  </div> --}}
</div>
@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_profile_org.js') }}"></script>
@stop
