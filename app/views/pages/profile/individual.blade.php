@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
<div ng-controller="ProfileIndCtrl">
  <input type="hidden" ng-init="userProfileId = {{$model->present()->userProfileInfo()->id}}">

  <div class="grey-bg toppadding bottompadding">
    <div class="container">
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <img src="{{$model->present()->userProfileInfo()->avatar_big}}" class="profpic" />
        <div class="si-share noborder clearfix text-center">
          <div class="clearfix" style="float: none; display: inline-block;">
            @if (isset($model->present()->userProfileInfo()->website_url) && strlen($model->present()->userProfileInfo()->website_url) > 0 && strtolower($model->present()->organisationInfo()->website_url) !== 'http://')
              <a href="{{$model->present()->userProfileInfo()->website_url}}" class="social-icon si-borderless si-cc" target="_blank">
                <i class="icon-globe"></i>
                <i class="icon-globe"></i>
              </a>
            @endif

            @if (isset($model->present()->userProfileInfo()->facebook_url) && strlen($model->present()->userProfileInfo()->facebook_url) > 0 && strtolower($model->present()->organisationInfo()->facebook_url) !== 'http://')
              <a href="{{$model->present()->userProfileInfo()->facebook_url}}" class="social-icon si-borderless si-facebook" target="_blank">
                <i class="icon-facebook"></i>
                <i class="icon-facebook"></i>
              </a>
            @endif

            @if (isset($model->present()->userProfileInfo()->twitter_url) && strlen($model->present()->userProfileInfo()->twitter_url) > 0 && strtolower($model->present()->organisationInfo()->twitter_url) !== 'http://')
              <a href="{{$model->present()->userProfileInfo()->twitter_url}}" class="social-icon si-borderless si-twitter" target="_blank">
                <i class="icon-twitter"></i>
                <i class="icon-twitter"></i>
              </a>
            @endif
          </div>
        </div>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
        <h2>{{$model->present()->userProfileInfo()->full_name}} <small>{{$model->present()->userProfileInfo()->job_title}} – {{$model->present()->userProfileInfo()->employer_name}}, {{$model->present()->userProfileInfo()->based_in_name}}</small></h2>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p>{{$model->present()->userProfileInfo()->about}}</p>
            {{-- Visit {{$model->present()->userProfileInfo()->first_name}}'s Website --}}
          </div>
          <!-- <div class="col-md-5">
            <ul class="portfolio-meta">
              <li class=""><div><span>Top Blogger Platform:</span> International</div></li>
              <li class=""><div><span style="vertical-align:top;">Nationality:</span> <span style="font-weight:normal;">{{$model->present()->userProfileInfo()->nationality_name}}</span></div></li>
              <li class=""><div><span style="vertical-align:top;">Status AidPost:</span> <span style="font-weight:normal;">{{$model->present()->userProfileInfo()->member_status_name}}</span></div></li>
              <li class=""><div><span style="vertical-align:top;">Career Status:</span> <span style="font-weight:normal;">{{$model->present()->userProfileInfo()->professional_status_name}}</span></div></li>
              <li class=""><div><span style="vertical-align:top;">Blogs Posted:</span> <%recordCount%></div></li>
            </ul>
          </div> -->
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>

  <div class="toppadding bottompadding">
    <div class="container">
      <div class="fancy-title title-dotted-border title-center">
        <h3>Blogs from this Author</h3>
      </div>
      <br>
      <div id="posts" class="horiz-post row">
        <div class="entry clearfix row zoomhover" ng-repeat="item in posts">
          <div class="col-md-4">
            <a href="<%item.view_url%>" class="zoomeffect"><img class="image_fade" ng-src="<%item.image%>" alt="<%item.title%>"></a>
          </div>
          <div class="entry-c col-md-8">
            <ul class="entry-meta">
              <li><%item.time_ago%></li>
              <li><i class="icon-thumbs-up"></i> <%item.num_of_likes%></li>
              <li><i class="icon-eye"></i> <%item.view_count%></li>
            </ul>
            <div class="clear"></div>
            <div class="entry-title clearfix">
              <h3><a href="<%item.view_url%>"><%item.title%></a></h3>
            </div>
            <div class="entry-content">
              <p ng-bind-html="item.teaser"></p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 text-center topmargin-sm">
        <a href="#" ng-click="showMore($event)" class="btn btn-warning" style="text-align:center; padding:10px 60px;" ng-hide="pageNumber === pageCount">Show More</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_profile_ind.js') }}"></script>
@stop
