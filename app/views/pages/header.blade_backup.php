<!-- Header ============================================= -->
<header id="header" class="sticky-style-2 bigger">

  <div class="container clearfix">

    <div id="logo">
      <a href="/" class="standard-logo" data-dark-logo="{{ URL::asset('images/logo-dark.png') }}"><img src="{{ URL::asset('images/logo.png') }}" alt="AidPost Logo"></a>
      <a href="/" class="retina-logo" data-dark-logo="{{ URL::asset('images/logo-dark@2x.png') }}"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo"></a>
      <h2>Connect the aid community</h2>
      <div class="dropdown homedropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="action-type" data-toggle="dropdown">
          {{ GenericUtility::selectedPlatform()->display_text }}
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu change-platform" role="menu" aria-labelledby="action-type">
          @foreach ($platforms as $platform)
            <li role="presentation">
              <form action="/changeplatform" method="POST">
                <input type="hidden" value="{{$platform->id}}" name="platform_id">
                <button type="submit" role="menuitem" tabindex="-1" data-id="{{$platform->id}}" data-country-id="{{$platform->related_country_id}}" class="change-platform-submit">{{$platform->display_text}}</button>
              </form>
            </li>
          @endforeach
          {{-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#">International</a></li> --}}
          {{-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Africa</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Asia</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Europe</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Latin America</a></li> --}}
        </ul>
      </div>
    </div>

    @include('pages.common._pub_small', array('model' => Publicities::getSmallPublicity()))

  </div>

  <div id="header-wrap">
    <nav id="primary-menu" class="style-2">
      <div class="container clearfix">
        
        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
        <ul>
          {{-- class="current" --}}
          <li class="menu_0"><a href="/"><div>Home</div></a></li>
          <li class="menu_1"><a href="/news"><div>News</div></a></li>
          <li class="menu_2"><a href="/blogs"><div>Blogs</div></a></li>
          <li class="menu_3"><a href="/videos"><div>Videos</div></a></li>
          <li class="menu_4"><a href="/get-involved"><div>Get Involved</div></a></li>
          <li class="menu_5"><a href="javascript:;">Members</a>
            <ul>
              <li><a href="/organisations"><div>Organisations</div></a></li>
              <li><a href="/individuals"><div>Individuals</div></a></li>
            </ul>
          </li>
          {{-- <li><a href="#"><div>Jobs</div></a></li>
          <li><a href="#"><div>Events</div></a></li> --}}
        </ul>
        <input type="hidden" id="hdn_active_menu" value="{{$active_menu or 'menu_0'}}">
        
        @if (Auth::check()) 
          <ul class="pull-right">
            <li><a href="javascript:;" class="btn user-menu"><div>Hello, {{Auth::user()->userProfile->first_name}}</div><span class="caret"></span></a>
              <ul>
                <li><a href="/oadmin"><div>My account</div></a></li>
                @if (Auth::user()->isOrganisation())
                  <li><a href="{{Auth::user()->organisation->profile_url}}" target="_blank"><div>View my profile</div></a></li>
                @elseif (Auth::user()->isIndividual())
                  <li><a href="{{Auth::user()->userProfile->profile_url}}" target="_blank"><div>View my profile</div></a></li>
                @endif
                <li><a href="{{ URL::route('admin_logout') }}"><div>Log out</div></a></li>
              </ul>
            </li>
          </ul>
        @else
          <ul class="pull-right">
            <li><a href="javascript:;" class="btn btn-danger"><div>Connect</div><span class="caret"></span></a>
              <ul>
                <li><a href="/oadmin/login" target="_blank"><div>Login</div></a></li>
                <li><a href="/register/individual" target="_blank"><div>Register as Individual</div></a></li>
                <li><a href="/register/organisation" target="_blank"><div>Register as Organisation</div></a></li>
              </ul>
            </li>
          </ul>
        @endif

      </div>
    </nav>
  </div>

</header>