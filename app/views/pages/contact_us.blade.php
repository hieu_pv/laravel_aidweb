@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="bottompadding toppadding-sm static-page">
    <div class="container clearfix">
      <div class="nobottommargin clearfix col-md-8">
        @include('flash::message')
        <h3>Contact Us</h3>
        {{ Form::open(array('action' => 'CmsMethodsController@postContact', 'files' => false, 'class' => '', 'id' => '')) }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                {{ Form::rawLabel('subject', 'Subject <span style="color:red">*</span>', array('class' => 'control-label')) }}
                <div>
                  {{ Form::select('subject', ['1' => 'Complaints', '2' => 'Feedback', '3' => 'Questions'], null, array('class' => 'form-control', 'required' => 'required')) }}
                </div>
              </div>
              <div class="form-group">
                {{ Form::rawLabel('name', 'Your Name <span style="color:red">*</span>', array('class' => 'control-label')) }}
                <div>
                  {{ Form::text('name', null, array('class' => 'form-control', 'required' => 'required')) }}
                </div>
              </div>
              <div class="form-group">
                {{ Form::rawLabel('email', 'Your Email Address <span style="color:red">*</span>', array('class' => 'control-label')) }}
                <div>
                  {{ Form::text('email', null, array('class' => 'form-control', 'required' => 'required')) }}
                </div>
              </div>
              <div class="form-group">
                {{ Form::rawLabel('message', 'Message <span style="color:red">*</span>', array('class' => 'control-label')) }}
                <div>
                  {{ Form::textarea('message', null, array('class' => 'form-control', 'rows' => '5', 'required' => 'required')) }}
                </div>
              </div>
              <div class="">
                <div class="text-left">
                  <button type="submit" class="btn btn-primary ladda-button submit-contact" data-style="expand-right"><span class="ladda-label">Send message</span></button>
                </div>
              </div>
            </div>
          </div>
        {{ Form::close() }}
      </div>
      <div class="nobottommargin col_last clearfix col-md-4 sidecol">
        <div class="sidebar-widgets-wrap">
          @include('pages.common._popularnews')

          @include('pages.common._pub_side', array('model' => Publicities::getSidePublicity()))

          @include('pages.common._popularblogs')
        </div>
      </div>
    </div>
  </div>
  <script>
    $(function () {
      $('select[name="subject"]').prepend('<option selected disabled hidden value="">-- Select a category</option>');
    });
  </script>
@stop

@section('after_content')
  
@stop