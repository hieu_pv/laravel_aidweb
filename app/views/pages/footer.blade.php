<!-- Footer
============================================= -->
<footer id="footer" class="dark">

  <div class="container">
    <div class="footer-widgets-wrap clearfix">

      <div class="col_one_third">
        <div class="widget clearfix">
          <img src="{{ URL::asset('images/footer-widget-logo.png') }}" alt="" class="footer-logo">
          <p>AidPost connects humanitarian, development and social actors - individuals and organizations alike. All contents on AidPost are uploaded by members and delivered in real time.</p>
          <div class="widget clearfix">
            <a href="https://www.facebook.com/pages/AidPost/734345110007561" class="social-icon si-small si-rounded si-facebook" target="_blank">
              <i class="icon-facebook"></i>
              <i class="icon-facebook"></i>
            </a>
            <a href="http://www.twitter.com/aidpostnews" class="social-icon si-small si-rounded si-twitter" target="_blank">
              <i class="icon-twitter"></i>
              <i class="icon-twitter"></i>
            </a>
            <a href="https://plus.google.com/u/0/109899192171420633936" class="social-icon si-small si-rounded si-gplus" target="_blank">
              <i class="icon-gplus"></i>
              <i class="icon-gplus"></i>
            </a>
            <a href="https://www.pinterest.com/aidpost/" class="social-icon si-small si-rounded si-pinterest" target="_blank">
              <i class="icon-pinterest"></i>
              <i class="icon-pinterest"></i>
            </a>
            <a href="https://www.linkedin.com/company/aidpost" class="social-icon si-small si-rounded si-linkedin" target="_blank">
              <i class="icon-linkedin"></i>
              <i class="icon-linkedin"></i>
            </a>
          </div>
        </div>
      </div><!-- /col_one_third -->

      <div class="col_one_third">

        <div class="widget clearfix">
          <h4>Latest Tweets</h4>

          <div>
            <a class="twitter-timeline" href="https://twitter.com/search?q=%40aidpostnews" data-widget-id="611031381045411841" data-chrome="noborders noscrollbar transparent nofooter noheader" data-tweet-limit="3">Tweets about @aidpostnews</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          </div>

          {{-- <div class="fslider testimonial no-image nobg noborder noshadow nopadding" data-animation="slide" data-arrows="false">
            <div class="flexslider">
              <div class="slider-wrap">
                <div class="slide">
                  <div class="testi-content">
                    <p>RT @<a href="#" target="_blank">JohnDoe</a>: CMagni impedit quae consectetur consequuntur adipisci veritatis modi a, officia cum</p>
                    <div class="testi-meta"><span><a href="#" target="_blank">about an hour ago</a></span></div>
                  </div>
                  <div class="testi-content">
                    <p>RT @<a href="#" target="_blank">JohnDoe</a>: CMagni impedit quae consectetur consequuntur adipisci veritatis modi a, officia cum</p>
                    <div class="testi-meta"><span><a href="#" target="_blank">about an hour ago</a></span></div>
                  </div>
                  
                </div>
                <div class="slide">
                  <div class="testi-content">
                    <p>RT @<a href="#" target="_blank">JohnDoe</a>: CMagni impedit quae consectetur consequuntur adipisci veritatis modi a, officia cum</p>
                    <div class="testi-meta"><span><a href="#" target="_blank">about an hour ago</a></span></div>
                  </div>
                  <div class="testi-content">
                    <p>RT @<a href="#" target="_blank">JohnDoe</a>: CMagni impedit quae consectetur consequuntur adipisci veritatis modi a, officia cum</p>
                    <div class="testi-meta"><span><a href="#" target="_blank">about an hour ago</a></span></div>
                  </div>
                  
                </div>
                <div class="slide">
                  <div class="testi-content">
                    <p>RT @<a href="#" target="_blank">JohnDoe</a>: CMagni impedit quae consectetur consequuntur adipisci veritatis modi a, officia cum</p>
                    <div class="testi-meta"><span><a href="#" target="_blank">about an hour ago</a></span></div>
                  </div>
                  <div class="testi-content">
                    <p>RT @<a href="#" target="_blank">JohnDoe</a>: CMagni impedit quae consectetur consequuntur adipisci veritatis modi a, officia cum</p>
                    <div class="testi-meta"><span><a href="#" target="_blank">about an hour ago</a></span></div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div> --}}

        </div>
      </div><!-- /col_one_third -->

      <div class="col_one_third col_last">

        <div class="widget quick-contact-widget clearfix">
          <h4>Contact Us</h4>
          <div id="quick-contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>
          {{ Form::open(array('action' => 'CmsMethodsController@postContact2', 'files' => false, 'class' => 'quick-contact-form nobottommargin', 'id' => 'quick-contact-form', 'name' => 'quick-contact-form')) }}
            <div id="quick-contact-form-message"></div><br>
            <div class="form-process"></div>
            <div class="input-group divcenter">
              <span class="input-group-addon"><i class="icon-tag"></i></span>
              <select name="quick-contact-form-subject" id="quick-contact-form-subject" class="required form-control input-block-level" required>
                <option selected="" disabled="" hidden="" value="">-- Select a category</option>
                <option value="1">Complaints</option>
                <option value="2">Feedback</option>
                <option value="3">Questions</option>
              </select>
            </div>
            <div class="input-group divcenter">
              <span class="input-group-addon"><i class="icon-user"></i></span>
              <input type="text" class="required form-control input-block-level" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Full Name" required/>
            </div>
            <div class="input-group divcenter">
              <span class="input-group-addon"><i class="icon-email2"></i></span>
              <input type="text" class="required form-control email input-block-level" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Email Address" required/>
            </div>
            <textarea class="required form-control input-block-level short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="4" cols="30" placeholder="Message" required></textarea>
            <input type="text" class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />
            <button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="btn btn-danger nomargin ladda-button" value="submit" data-style="expand-right"><span class="ladda-label">Send Email</span></button>
          {{ Form::close() }}
        </div>

      </div><!-- /col_one_third -->

    </div><!-- /footer-widgets-wrap -->
  </div><!-- /container -->

  <!-- Copyrights
  ============================================= -->
  <div id="copyrights">
    <div class="container clearfix">
      <div class="col_half">
        Copyrights &copy; <span id="copyyear"></span> <a href="#">AidPost</a>. All Rights Reserved. <br>
        <!-- Design by <a href="http://fleava.com" target="_blank">Fleava</a>. -->
      </div>
      <div class="col_half col_last tright">
        <div class="fright clearfix">
          <div class="copyrights-menu copyright-links nobottommargin">
            <a href="/">Home</a>/<a href="/about">About</a>/<a href="/user-agreement">User Agreement</a>/<a href="/privacy-policy">Privacy Policy</a>/<a href="/feedback">Feedback</a>
          </div>
        </div>
      </div>
    </div>
  </div><!-- /#copyrights -->

</footer>