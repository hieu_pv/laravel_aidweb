@extends('layouts.master')

@section('content')

  @foreach ($posts as $post)
    <p>
      <a href="{{ $post->present()->url }}" title="" target="_blank">{{ $post->title }}</a>
      <p>Categories</p>
      <ul>
        @foreach ($post->categories as $category)
          <li>{{ $category->name }}</li>
        @endforeach
      </ul>
      <p>Tags</p>
      <ul>
        @foreach ($post->tags as $tag)
          <li>{{ $tag->name }}</li>
        @endforeach
      </ul>
    </p>
    <hr>
  @endforeach

@stop