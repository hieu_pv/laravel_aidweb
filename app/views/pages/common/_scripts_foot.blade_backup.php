<!-- Footer Scripts
============================================= -->

<div class="modal fade" id="modal_like_not_authorized" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Not Authorized</h4>
      </div>
      <div class="modal-body">
        <p>You Need to Login or Register to Use this Feature</p>
        <div class="text-center">
          <a href="/oadmin/login" class="btn btn-primary">Login</a>
          <a href="/oadmin/login" class="btn btn-info">Register</a>
        </div>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
      -->
    </div>
  </div>
</div>

{{-- <script type="text/javascript" src="{{ URL::asset('js/plugins/clamp.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ URL::asset('js/plugins/jquery.dotdotdot.js') }}"></script> --}}
<script type="text/javascript" src="{{ URL::asset('js/aidweb.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app/core.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/functions2.js') }}"></script>
<script type="text/javascript">
  (function ($) {
    $(function () {

      $(document).on('click', 'a[data-like-posting]', function (event) {
        event.preventDefault();
        var dis = this;
        var module = $(this).attr('data-module');
        var id = $(this).attr('data-id');
        $.post('/api/_like', { module: module, id: id }, function (response) {
          if (response.ok) {
            $(dis).addClass('liked');
            $(dis).html('<i class="icon-thumbs-up"></i> Post Liked');
            $(dis).attr('disabled', 'disabled');
          }
        }).fail(function () {
          if (arguments[0].status === 401) {
            $('#modal_like_not_authorized').modal({ show: true });
          }
        });
      });

      $(document).on('click', 'button[data-submit-comment]', function (event) {
        event.preventDefault();
        var dis = this;
        var module = $(this).attr('data-module');
        var id = $(this).attr('data-id');
        var content = $(this).closest('.aw-give-comment').find('textarea[name="comment"]').val();
        if (content.length === 0) {
          return false;
        }
        var dt = { module: module, id: id, content: content };
        // var l = Ladda.create(this);
        // l.start();
        $.post('/api/_givecomment', dt, function (response) {
          if (response.ok) {
            $(dis).closest('.aw-give-comment').find('textarea[name="comment"]').val('');
            $(dis).after('<div class="alert alert-success alert-dismissible fade in" role="alert" id="success_comment"> \
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> \
            <strong>Success!</strong> Your Comment has been submitted. \
            </div>');
            if (typeof window.aw_comments_list !== 'undefined') {
              $(window.aw_comments_list).trigger('get.newest.comments');
            }
          }
        }).fail(function () {
          if (arguments[0].status === 401) {
            $('#modal_like_not_authorized').modal({ show: true });
          }
        }).always(function () {
          // l.stop();
        });
      });

      $('.' + $('#hdn_active_menu').val()).addClass('current');

      $(document).on('click', 'a[data-lightbox="iframe"]', function (e) {
        e.preventDefault();
        $(this).magnificPopup({
          disableOn: 600,
          type: 'iframe',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });
        $(this).magnificPopup('open');
      });

      $(document).on('click', 'a[data-lightbox="aidweb"]', function (e) {
        e.preventDefault();
        var markup3 = '<div class="mfp-iframe-scaler">'+
                      '<div class="mfp-close"></div>'+
                      '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                      '<div class="mfp-overlay1"><a href="#" class="mfp-like-link" data-like-posting="true"><i class="icon-thumbs-up"></i> <span class="mfp-like-count">Like Post</span></a></div>'+
                      '</div>'+
                      '<div class="mfp-desc"></div>';
        $(this).magnificPopup({
          disableOn: 0,
          type: 'iframe',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false,
          mainClass: 'aidweb-mfp-content',
          iframe: {
            markup: markup3,
            patterns: {
              aidweb: {
                index: 'youtube.com/',
                id: 'v=',
                src: '//www.youtube.com/embed/%id%?autoplay=1'
              }
            }
          }
        });
        $(this).on('mfpOpen', function (e) {
          var desc = $($.magnificPopup.instance.st.el[0]).attr('data-desc');
          var likes = $($.magnificPopup.instance.st.el[0]).attr('data-likes');
          var id = $($.magnificPopup.instance.st.el[0]).attr('data-id');
          $('.aidweb-mfp-content .mfp-content .mfp-desc').html(desc);
          $('.aidweb-mfp-content .mfp-content .mfp-like-link').attr('data-id', id);
          $('.aidweb-mfp-content .mfp-content .mfp-like-link').attr('data-module', 4);

          // send viewed request
          var postData = { id: id };
          $.post('/videos/viewed', postData);
        });
        $(this).magnificPopup('open');
      });

      $(document).on('click', '#most-popular .tab-nav li a', function (e) {
        window.AIDWEB._doClamp();
      });

      // $(document).on('click', '#top_posts_tabs li a, #top_news_tabs li a, #top_videos_tabs li a, #type_for_top_tabs li a', function (e) {
      //   window.AIDWEB._doClamp();
      // });

    });
  }(jQuery));
</script>
<script type="text/javascript">
  $(function () {
    var year = new Date();
    document.querySelector('#copyyear').textContent = year.getFullYear();

    $('#quick-contact-form-submit').click(function (event) {
      if ($('#quick-contact-form').valid()) {

      } else {
        return false;
      }
      var action = $('#quick-contact-form').attr('action');
      var formData = {
        'subject': $('[name=quick-contact-form-subject]').val(),
        'name': $('input[name=quick-contact-form-name]').val(),
        'email': $('input[name=quick-contact-form-email]').val(),
        'message': $('[name=quick-contact-form-message]').val()
      };
      var l = Ladda.create(document.querySelector('#quick-contact-form-submit'));
      l.start();
      $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : action, // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
      })
      .done(function(data) {
        //console.log(data);
        $('#quick-contact-form-message').html(data.message);
      })
      .always(function(data) {
        l.stop();
      });
      event.preventDefault();
    });
  });
</script>