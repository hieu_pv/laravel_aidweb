<div class="widget clearfix">
  <h4>Popular News</h4>
  <div id="post-list-footer">
    @foreach ($mostPopularNews as $item)
      <div class="spost clearfix zoomhover">
        <a href="#" class="zoomeffect"><img src="{{$item->image}}" alt="{{$item->title}}"></a><br>
        <div class="entry-title">
          <a href="{{$item->org_info->profile_url}}" class="nobg"><img class="org-logo" src="{{$item->org_info->logo}}" alt=""></a>
          <h4><a href="{{$item->view_url}}">{{$item->title}}</a></h4>
        </div>
        <div class="clear"></div>
      </div>
    @endforeach
  </div>
</div>