@if (Auth::check())
  <div class="aw-give-comment">
    Hello, <strong>{{Auth::user()->entityName}}</strong>!
    <ol class="commentlist clearfix">
      <li class="comment even thread-even depth-1" id="li-comment-1">
        <div id="comment-1" class="comment-wrap clearfix">
          <div class="comment-meta">
            <div class="comment-author vcard">
              <span class="comment-avatar clearfix">
                <a href="{{Auth::user()->profile_url}}"><img alt="" src="{{Auth::user()->image}}" class="avatar avatar-60 photo avatar-default" height="60" width="60" /></a>
              </span>
            </div>
          </div>
          <div class="comment-content clearfix">
            <textarea name="comment" cols="58" rows="4" tabindex="4" class="sm-form-control" placeholder="Write a comment..."></textarea>
          </div>
          <button name="" type="button" id="submit_comment" tabindex="5" value="" class="btn btn-default ladda-button" data-style="expand-right" style="margin: 15px;" data-submit-comment="true" data-module="{{$fc_module}}" data-id="{{$fc_id}}" data-uid="{{Auth::user()->id}}"><span class="ladda-label">Submit</span></button>
          <div class="clear"></div>
        </div>
      </li>
    </ol>
  </div>
@endif