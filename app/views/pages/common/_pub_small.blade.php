@if ($model !== null) 
  <div class="top-advert bigger">
    <a href="{{$model->url}}" target="_blank">
      @if (GenericUtility::startsWith($model->image, 'data:'))
        <img src="/images/placeholder.gif" data-original="{{$model->image}}" alt="" class="lazy">
      @else
        <img src="/images/placeholder.gif" data-original="/imagecache/680_160_c{{$model->image}}" alt="" class="lazy">
      @endif
    </a>
  </div>
@endif