<script type="text/template" id="template_for_comment_item">
  <li class="comment even thread-even depth-1" id="" data-comment-id="<%= id %>">
    <div id="" class="comment-wrap clearfix">
      <div class="comment-meta">
        <div class="comment-author vcard">
          <span class="comment-avatar clearfix">
            <a href="<%= profile_url %>"><img alt="" src="<%= profile_image %>" class="avatar avatar-60 photo avatar-default" height="60" width="60" /></a>
          </span>
        </div>
      </div>
      <div class="comment-content clearfix">
        <div class="comment-author"><a href="<%= profile_url %>"><%= profile_name %></a><span><a href="#" title="Permalink to this comment"><%= time_ago %></a></span></div>
        <p><%= content %></p>
      </div>
      <div class="clear"></div>
    </div>
  </li>
</script>

<div class="aw-comments-list">
  <input type="hidden" value="{{$cl_module}}" id="aw_comments_list_module_id">
  <input type="hidden" value="{{$cl_id}}" id="aw_comments_list_item_id">
  <h3 id="comments-title"><span class="c"></span> Comments</h3>
  <ol class="commentlist clearfix">
  </ol>
  <script>
    window.aw_comments_list = {};
    $(window.aw_comments_list).on('get.newest.comments', function () {
      var nid = $('.aw-comments-list .commentlist li.comment:first-child').attr('data-comment-id') || 0;
      var dt = {
        module: $('#aw_comments_list_module_id').val(),
        id: $('#aw_comments_list_item_id').val(),
        pivot_id: nid,
      };
      $.post('/api/_getnewestcomments', dt, function (response) {
        $('.aw-comments-list #comments-title .c').text(parseInt($('.aw-comments-list #comments-title .c').text(), 10) + response.length);
        for (var i = 0; i < response.length; i++) {
          var rendered = _.template($('#template_for_comment_item').html())(response[i]);
          $('.aw-comments-list .commentlist').prepend($(rendered));
        }
      }).fail(function () {

      });
    });
    $(function () {
      var dt = {
        module: $('#aw_comments_list_module_id').val(),
        id: $('#aw_comments_list_item_id').val(),
      };
      $.post('/api/_getcomments', dt, function (response) {
        $('.aw-comments-list #comments-title .c').text(response.length);
        for (var i = 0; i < response.length; i++) {
          var rendered = _.template($('#template_for_comment_item').html())(response[i]);
          $('.aw-comments-list .commentlist').append($(rendered));
        }
      }).fail(function () {

      });
    });
  </script>
</div>