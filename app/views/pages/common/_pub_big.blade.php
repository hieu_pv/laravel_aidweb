<div class="clear"></div>
@if ($model !== null)
  @if (GenericUtility::startsWith($model->image, 'data:'))
  <div class="section nobottommargin notopmargin bottom-advert {{ $model->a_text_color === '#222222' ? 'light' : 'dark' }} big-pub" style="background-image: url('{{$model->image}}');">
  @else
  <div class="section nobottommargin notopmargin bottom-advert {{ $model->a_text_color === '#222222' ? 'light' : 'dark' }} big-pub" style="background-image: url('/imagecache/r_1366_auto_c{{$model->image}}');">
  @endif
    
    @if (starts_with($model->a_position, 'Right'))
      <div class="slider-gradient invert"></div>
    @else
      <div class="slider-gradient"></div>
    @endif

    <div class="container clearfix" style="display: table">
      <div class="slider-caption"> <!-- {{$model->a_position}} -->
        <h2><span class="smaller">Publicity</span></h2>

        @if ($model->a_title !== '')
          <h2><strong><a href="{{$model->url}}" style="color: {{$model->a_text_color}} !important;" target="_blank">{{$model->a_title}}</a></strong></h2>
        @endif

        @if (starts_with($model->a_position, 'Right'))
          @if ($model->a_show_org_logo)
            <a href="{{$model->present()->organisationInfo()->profile_url}}">
              <img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$model->present()->organisationInfo()->logo}}" style="float:right" />
            </a>
          @endif
          @if ($model->a_text !== '')
            <p class="desc hidden-xs" style="color: {{$model->a_text_color}} !important; padding-left: 0px; {{ $model->a_show_org_logo ? 'padding-right: 70px;' : '' }}">{{$model->a_text}}</p>
          @endif
        @else
          @if ($model->a_show_org_logo)
            <a href="{{$model->present()->organisationInfo()->profile_url}}">
              <img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$model->present()->organisationInfo()->logo}}" />
            </a>
          @endif
          @if ($model->a_text !== '')
            <p class="desc hidden-xs" style="color: {{$model->a_text_color}} !important; {{ !$model->a_show_org_logo ? 'padding-left: 0;' : '' }}">{{$model->a_text}}</p>
          @endif
        @endif
        
        <div class="clear"></div>

        @if ($model->a_button_text !== '')
          <a href="{{$model->url}}" class="btn btn-grey" style="text-transform: uppercase;" target="_blank">{{$model->a_button_text}}</a>
        @endif

      </div>
    </div>
  </div>
@endif