<ol class="breadcrumb" ng-show="!hasFilter">
  <li ng-hide="hasFilter">
    <span>No filter selected</span>
  </li>
</ol>

<ol class="breadcrumb" ng-show="hasFilter">
  <li ng-show="info_text.filters_text.region_text.length > 0">
    <a href="#" ng-click="query.filters.region = -1; info_text.filters_text.region_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.region_text %></span>
  </li>
  <li ng-show="info_text.filters_text.country_text.length > 0">
    <a href="#" ng-click="query.filters.country = -1; info_text.filters_text.country_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.country_text %></span>
  </li>
  <li ng-show="info_text.filters_text.actiontype_text.length > 0">
    <a href="#" ng-click="query.filters.actiontype = -1; info_text.filters_text.actiontype_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.actiontype_text %></span>
  </li>
  <li ng-show="info_text.filters_text.crisis_text.length > 0">
    <a href="#" ng-click="query.filters.crisis = -1; info_text.filters_text.crisis_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.crisis_text %></span>
  </li>
  <li ng-show="info_text.filters_text.theme_text.length > 0">
    <a href="#" ng-click="query.filters.theme = -1; info_text.filters_text.theme_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.theme_text %></span>
  </li>
  <li ng-show="info_text.filters_text.sector_text.length > 0">
    <a href="#" ng-click="query.filters.sector = -1; info_text.filters_text.sector_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.sector_text %></span>
  </li>
  <li ng-show="info_text.filters_text.beneficiary_text.length > 0">
    <a href="#" ng-click="query.filters.beneficiary = -1; info_text.filters_text.beneficiary_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.beneficiary_text %></span>
  </li>
  <li ng-show="info_text.filters_text.nationality_text.length > 0">
    <a href="#" ng-click="query.filters.nationality = -1; info_text.filters_text.nationality_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.nationality_text %></span>
  </li>
  <li ng-show="info_text.filters_text.organisationType_text.length > 0">
    <a href="#" ng-click="query.filters.organisationType = -1; info_text.filters_text.organisationType_text = ''; $event.preventDefault();">&times;</a>
    <span><% info_text.filters_text.organisationType_text %></span>
  </li>
</ol>