@if ($model !== null) 
  <div class="widget clearfix">
    <a href="{{$model->url}}" target="_blank">
      @if (GenericUtility::startsWith($model->image, 'data:'))
        <img style="width:100%" src="/images/placeholder.gif" data-original="{{$model->image}}" alt="" class="lazy">
      @else
        <img style="width:100%" src="/images/placeholder.gif" data-original="/imagecache/r_760_auto_c{{$model->image}}" alt="" class="lazy">
      @endif
    </a>
  </div>
@endif