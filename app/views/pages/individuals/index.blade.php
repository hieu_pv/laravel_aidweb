@extends('layouts.site')

@section('before_content')
  @include('pages.common._pub_big', array('model' => Publicities::getBigPublicity()))
  <div class="clear"></div>
@stop

@section('content')

  <div class="toppadding-sm bottompadding">
    <div ng-controller="AllIndividualsCtrl">

      <div class="fancy-title title-dotted-border title-center">
        <h3 class="">All Individuals</h3>
      </div>

      <div class="container">
        <div class="news-filters row smallpadding" >

          <div class="col-md-12 nopadding bottommargin">
            <div class="dropdown col-md-2 xsmallpadding">
              <button class="btn btn-default dropdown-toggle" type="button" id="country" data-toggle="dropdown">
                Nationality
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu scrollable-dropdown-menu filter-nationalities-ind" role="menu" aria-labelledby="country">
                <li role="presentation" ng-repeat="item in filters.nationalities" data-filter-type="country">
                  <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.nationality = item.id; info_text.filters_text.nationality_text = item.name"><% item.name %></a>
                </li>
              </ul>
            </div>
            <div class="dropdown col-md-2 xsmallpadding">
              <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                Professional Status
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu scrollable-dropdown-menu filter-professionalstatuses-ind" role="menu" aria-labelledby="">
                <li role="presentation" ng-repeat="item in filters.professional_statuses">
                  <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.professional_status = item.id; info_text.filters_text.professional_status_text = item.name"><% item.name %></a>
                </li>
              </ul>
            </div>
            <div class="dropdown col-md-2 xsmallpadding">
              <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                Member Status
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu scrollable-dropdown-menu filter-memberstatuses-ind" role="menu" aria-labelledby="">
                <li role="presentation" ng-repeat="item in filters.member_statuses">
                  <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.member_status = item.id; info_text.filters_text.member_status_text = item.name"><% item.name %></a>
                </li>
              </ul>
            </div>
            <div class="dropdown col-md-2 xsmallpadding">
              <button class="btn btn-default dropdown-toggle" type="button" id="" data-toggle="dropdown">
                Employer Type
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu scrollable-dropdown-menu filter-employertypes-ind" role="menu" aria-labelledby="">
                <li role="presentation" ng-repeat="item in filters.organisation_types">
                  <a role="menuitem" tabindex="-1" href="javascript:;" data-value="<% item.id %>" ng-click="query.filters.employer_organisation_type = item.id; info_text.filters_text.employer_organisation_type_text = item.display_name"><% item.display_name %></a>
                </li>
              </ul>
            </div>
            <div class="clear"></div>
            <ol class="breadcrumb" ng-show="!hasFilter">
              <li ng-hide="hasFilter">
                <span>No filter selected</span>
              </li>
            </ol>
            <ol class="breadcrumb" ng-show="hasFilter">
              <li ng-show="info_text.filters_text.nationality_text.length > 0">
                <a href="#" ng-click="query.filters.nationality = -1; info_text.filters_text.nationality_text = ''; $event.preventDefault();">&times;</a>
                <span><% info_text.filters_text.nationality_text %></span>
              </li>
              <li ng-show="info_text.filters_text.professional_status_text.length > 0">
                <a href="#" ng-click="query.filters.professional_status = -1; info_text.filters_text.professional_status_text = ''; $event.preventDefault();">&times;</a>
                <span><% info_text.filters_text.professional_status_text %></span>
              </li>
              <li ng-show="info_text.filters_text.member_status_text.length > 0">
                <a href="#" ng-click="query.filters.member_status = -1; info_text.filters_text.member_status_text = ''; $event.preventDefault();">&times;</a>
                <span><% info_text.filters_text.member_status_text %></span>
              </li>
              <li ng-show="info_text.filters_text.employer_organisation_type_text.length > 0">
                <a href="#" ng-click="query.filters.employer_organisation_type = -1; info_text.filters_text.employer_organisation_type_text = ''; $event.preventDefault();">&times;</a>
                <span><% info_text.filters_text.employer_organisation_type_text %></span>
              </li>
            </ol>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 grey-bg clearfix filter-result-bar">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <strong><% record_count %></strong> <strong>Individuals</strong> from <strong><% based_in_count %></strong> <strong>countries</strong>
            </div>

            <div class="col-lg-3 col-md-3 col-lg-push-5 col-md-push-5 col-sm-3 col-sm-push-5 col-xs-12">
              <div class="input-group">
                <input type="text" class="form-control input-sm" placeholder="Enter Keywords" ng-enter="doSearch()" ng-model="searchQuery">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-sm" type="button" ng-click="doSearch()"><i class="icon-search" style="display:block; font-weight:600"></i></button>
                </span>
              </div><!-- /input-group -->
            </div><!-- / -->

            <div class="col-lg-5 col-md-5 col-lg-pull-3 col-md-pull-3 col-sm-5 col-sm-pull-3 col-xs-12">
              <span class="pull-left sort-by visible-xs">Sort By:</span>
              <ul class="nav nav-pills pull-right pull-left-on-xs">
                <li ng-class="{'active': query.sortBy === 'name'}"><a href="javascript:;" ng-click="query.sortBy = 'name'">Name</a></li>
                <li ng-class="{'active': query.sortBy === 'recent'}"><a href="javascript:;" ng-click="query.sortBy = 'recent'">Recent</a></li>
              </ul>
              <span class="pull-right sort-by hidden-xs">Sort By:</span>
            </div>  
          </div>

          <div class="clear"></div>
        </div>

        <div id="list_of_organisations" class="portfolio-carousel row">
          <div class="oc-item zoomhover col-md-4 smallpadding col-sm-6" ng-repeat="item in items">
            <div class="ipost card-grey clearfix">
              <div class="portfolio-desc">
                <a href="<% item.profile_url %>"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="<% item.avatar %>" /></a>
                <div class="desc-content">
                  <a href="<% item.profile_url %>"><strong><% item.full_name %></strong></a>
                  <br>
                  <span class="ind-desc"><% positionDescription(item) %></span>
                </div>
                <div class="clear"></div>
              </div>
<!--               <div class="portfolio-image">
                <a href="<% item.profile_url %>" class="zoomeffect" style="width: 100%; height: 100%;">
                  <img src="/images/blank.gif" alt="<% item.org_name %>" class="" style="width: 100%; height: 100%; background: url('/imagecache/r_370_auto_c<% item.cover_image %>') center center / cover no-repeat;">
                </a>
              </div> -->
            </div>
          </div>

          <div class="col-md-12 text-center topmargin-sm">
            <a href="javascript:;" class="btn btn-warning" style="text-align:center; padding:10px 60px;" ng-click="loadMore()" ng-hide="query.pageNumber >= page_count">Show More</a>
          </div>
        </div>

        <div class="clear"></div>
      </div>

    </div>
  </div>

@stop

@section('scripts')
  <script type="text/javascript">var _selectedPlatformCountryId = {{ GenericUtility::selectedPlatform()->related_country_id }};</script>
  <script type="text/javascript">var _selectedPlatformCountryText = '{{ GenericUtility::selectedPlatform()->display_text }}';</script>
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_individuals.js') }}"></script>
@stop