<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="fleava" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>AidPost</title>
  @section('head')
  @show
  @include('pages.common._styles')
  @include('pages.common._scripts_head')
</head>
<body class="stretched no-transition" ng-app="aidwebApp">
  <!-- Document Wrapper ============================================= -->
  <div id="wrapper" class="clearfix">
    <header id="header" class="sticky-style-2 bigger">
      <div class="container clearfix">
        <div id="logo">
          <a href="/" class="standard-logo" data-dark-logo="{{ URL::asset('images/logo-dark.png') }}"><img src="{{ URL::asset('images/logo.png') }}" alt="AidPost Logo"></a>
          <a href="/" class="retina-logo" data-dark-logo="{{ URL::asset('images/logo-dark@2x.png') }}"><img src="{{ URL::asset('images/logo@2x.png') }}" alt="AidPost Logo"></a>
          <h2>Connect the aid community</h2>
        </div>
        @if ($model->type_id === Modules::publicity_type_small())
          @include('pages.common._pub_small', array('model' => $model))
        @endif
      </div>
    </div>

    @if ($model->type_id === Modules::publicity_type_big())
      @include('pages.common._pub_big', array('model' => $model))
    @endif

    <div class="clear"></div>

    <section id="content">
      <div class="toppadding bottompadding">
        <div class="container clearfix">
          <div class="nobottommargin clearfix col-md-8">
          </div>
          <div class="nobottommargin col_last clearfix col-md-4 sidecol">
            <div class="sidebar-widgets-wrap">
              @if ($model->type_id === Modules::publicity_type_side())
                @include('pages.common._pub_side', array('model' => $model))
              @endif
            </div>
          </div>
        </div>
      </div>
    </section><!-- #content end -->

    @section('after_content')
    @show

    @include('pages.footer')
  </div><!-- #wrapper end -->
  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  @include('pages.common._scripts_foot')
  @section('scripts')
  @show
</body>
</html>


