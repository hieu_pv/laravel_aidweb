@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="bottompadding static-page">
    <div class="container clearfix">
      
      <div class="nobottommargin clearfix col-md-8">
        <div class="single-post nobottommargin">
          <div class="entry clearfix">
            <div class="entry-title">
              <h1>About Us</h1>
            </div>
            <div class="entry-content">
              <h3 class="notopmargin">What is AidPost?</h3>
              <p>It is a media, a networking platform, and an inter-agency website for development and social actors &ndash; individuals and organizations alike. All contents on AidPost are uploaded by members.</p>
              <p>AidPost connects the civil society: aid workers and supporters, development agencies, government and academic institutions, the private sector delivering products and services to the aid community or pursuing corporate social responsibilities.</p>
              <p>For the general public, AidPost proposes a one-stop gateway to interact with the aid community and to access &lsquo;unfiltered&rsquo; information in real time.</p>
              <p>For the aid community, it offers a user-friendly channel using the latest information technologies to connect with their peers and present their activities and needs to a larger audience.</p>

              <h3>Who is AidPost for?</h3>
              <p>For people interested in societal issues, and those who want to bring about change in their neigborhood or elsewhere on the planet.</p>
              <p>For development and social agencies who need the attention of national and international audiences, or to establish new alliances with &nbsp;potential partners, donors, suppliers.</p>
              <p>For employee of not-profit organizations, scolars and civil servants willing to share their expertise, opinions and ideas on development issues, or looking to expand their profesionnal network.</p>

              <h3>Mission</h3>
              <p>AidPost aims at building a global community where all development actors have an equal opportunity and increased capacity to interact with the general public and their peers, in order to improve the overall performance of the civil society as a whole.</p>
              
              <h3>Communication Trends</h3>
              <p>Nowadays, aid agencies and civil society organizations need to be present in the media and noticed on the internet. They need to engage:</p>
              <ul>
              <li>the general public for resource mobilization and to bring about change;</li>
              <li>the private sector to foster partnerships, innovations and social responsibilities;</li>
              <li>governments to broaden country-level policy dialogue;</li>
              <li>their peers to build relationships, share knowledge, and amplify their voices.</li>
              </ul>
              <p>Remarkably NGOs &ldquo;are becoming increasingly involved in the gathering and delivery of international news&rdquo; (Price, Morgan, Klinkforth), they have &ldquo;turned themselves into reporters for the mainstream media&rdquo; (Cooper) and have become the most trusted source of information globally, before businesses, media, and governments (see annual surveys of Edelman Trust Barometer). Some NGOs have also developed websites that can genuinely involve their audiences and keep up with the latest trends.</p>
              <p>But the above achievements are only realizable by a small number of organizations because servicing efficiently the media and the internet requires investment of time, money and technical skills - resources that are not available to the vast majority of NGOs and CSOs. It also requires efforts that conflict with their core mission.</p>
              
              <h3>Networking trends</h3>
              <p>Development organizations see value in networks as a way to enhance capacities, to access and share knowledge, to gain more attention, to increase their visibility, credibility, influence and access to resources.</p>
              <p>But in developing countries national networks are often dominated by international organizations operating at the national level. &ldquo;For many national</p>
              <p>organizations this creates a relationship with international agencies described as &lsquo;both a blessing and a curse&rsquo; &ndash; providing vital financial and technical support, but also undermining&nbsp; their independence and national character.&rdquo; (Scriven, ALNAP)</p>
              <p>Weather fully owned by national actors or not, the long term viability of national networks poses a challenge, given the cyclical nature of humanitarian funding, and the challenges facing national organizations in accessing institutional funds. More often than not, they also fail to connect with an international audience.</p>
              
              <h3>AidPost solution</h3>
              <p>Aidpost tries to tackle problems faced by the aid community across the globe in its effort to improve its performance through networking and communication strategies.</p>
              <p>The website strives to be of as good quality as those produced by other media for sophisticated consumers, offering a no-cost and simple solution to be noticed on the World Wide Web. By increasing the visibility of the aid community in the public arena, AidPost provides hopefully a valuable alternative - as well as a springboard - to national and international mainstream media.</p>
              <p>Another key feature of Aidweb is the display of <strong>national</strong> platforms. The national level is paramount, it is where needs are identified; where aid and social programmes are designed and implemented; where partnerships are built; where local service providers and suppliers are called upon and local staffs recruited and trained; where debates for policy change take place; where lifes and livelihoods are at stake.</p>
              <p>On AidPost, national platforms from Southern and Northern countries are intertwined so as to enhance the visibility, interaction and performance of the aid community as a whole.</p>
              
              <h3>Historic</h3>
              <p>The roots of AidPost can be traced back to the relief operations of the 2006 tsunami in Aceh and the 2010 earthquake in Haiti, where the people who have conceptualized AidPost were involved as senior aid workers. These two large scale disasters have been an eye opener for those deployed in the field on some of the biggest challenges faced by the aid community.</p>
              
              <h3>Approach</h3>
              <p>AidPost proposes to implement this project along the Minimum Viable Product approach, which is part of an iterative Build-Measure-Learn process. The idea is to start simple and let the system grow in complexity, according to the needs of the aid community. As such, AidPost will constantly welcome, actively seek, and act upon feedback from members to improve and further develop the service.</p>
              <p>As it stands, the MVP allows posting of Blogs (by Individual members), News, Videos, Get involved, Tweets, and displays a profile page of members that can redirect visitors to their website and social media pages. It is anticipated that the number of services, of national platforms and of autorized languages will grow over time.</p>
              <p>To ensure its long term financial viability, AidPost will mainly rely on sponsorships and on sale of publicity and featured spaces to its more fortunated members.</p>
              
              <h3>Board of Advisors</h3>
              <p>AidPost is supported by a Board of Advisors composed of aid professionals and information technology specialists, who have generously accepted to provide sound advises during the research and development phase of AidPost. By alphabetic order:</p>
              <div class="row">
                <div class="col-md-3">Michael Andreini <br> United States</div>
                <div class="col-md-9">Director <br> Rocky Mountains Epidemiology Center, USA</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Farook Adrien Doomun <br> Switzerland</div>
                <div class="col-md-9">Regional Head of Support Service <br> UNOPS, Senegal</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Yegana Guliyeva-Baigorri <br> Azerbaidjan</div>
                <div class="col-md-9">Adviser on International Cooperation <br> BILTEK Turkey, Spain</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Raymond Lachappelle <br> Canada</div>
                <div class="col-md-9">Communication Adviser <br> Cellonis Biotechnologies, China</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Sebastien Lemarquis <br> France</div>
                <div class="col-md-9">Co-Founder and Director <br> Tiketik, indonesia</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Pierre Markowski <br> Canada</div>
                <div class="col-md-9">Regional Director <br> Developpement International Desjardins, Senegal</div>
              </div>
              
              <h3>Team</h3>
              <p>AidPost is thankful to the following people who have also contributed, some of them on a volunteer basis, with their guidance or work, to the creation of the website you can now access on-line. By alphabetic order:</p>
              <div class="row">
                <div class="col-md-3">Charles Caesien <br> Indonesia</div>
                <div class="col-md-9">Research <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Haritian Pinio <br> Indonesia</div>
                <div class="col-md-9">Web Programming <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Roger Markowski <br> Canada</div>
                <div class="col-md-9">Coordination <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Adrien Motsch <br> France</div>
                <div class="col-md-9">Web Programming (Consultation) <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Diyah Perwitosri <br> Indonesia</div>
                <div class="col-md-9">Research <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Nana Srihasanah <br> Indonesia</div>
                <div class="col-md-9">Administration <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Lewis Webber <br> United Kingdom</div>
                <div class="col-md-9">Web Design <br> &nbsp;</div>
                <div class="clearfix"><br></div>
                <div class="col-md-3">Ousmane Wilane <br> Senegal</div>
                <div class="col-md-9">Web Programming (Consultation) <br> &nbsp;</div>
              </div>
              <br>
              <p>AidPost is a social enterprise registered in Canada since May 2013.</p>
            </div>
          </div>
        </div>
      </div>

      <div class="nobottommargin col_last clearfix col-md-4 sidecol">
        <div class="sidebar-widgets-wrap">
          @include('pages.common._popularnews')

          @include('pages.common._pub_side', array('model' => Publicities::getSidePublicity()))

          @include('pages.common._popularblogs')
        </div>
      </div>

    </div>
  </div>
@stop

@section('after_content')
  
@stop