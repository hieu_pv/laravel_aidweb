<div class="container topmargin bottommargin hidden-xs hidden-sm">
  <div class="fancy-title title-dotted-border title-center">
    <h3 class="">Featured Posts</h3>
  </div>
  <div class="hidden-xs">
    <ul class="tab-nav clearfix countries" id="top_posts_tabs">
      @foreach ($tabsOfTopPosts->tabs as $item)
        <li><a href="#topposts_tab_{{$item->id}}" data-toggle="tab" data-id="{{$item->id}}">{{$item->name}}</a></li>
      @endforeach
    </ul>
  </div>
  <div class="clear"></div>
  <div class="tab-content featured-news">
    @foreach ($tabsOfTopPosts->tabs as $item)
      <div role="tabpanel" class="tab-pane fade" id="topposts_tab_{{$item->id}}">
        <div class="col-md-12 hidden-xs nomargin nopadding">
          <section id="" class="swiper_wrapper video-slider clearfix">
            <div class="swiper-container swiper-video-parent swiper-topposts-parent-{{$item->id}}">
              <div class="swiper-wrapper">
                @foreach ($item->top_posts as $blogPost)
                  <div class="swiper-slide">
                    <div class="slideimg col-md-8 nomargin nopadding" style="background-image: url('/imagecache/r_760_auto_c{{$blogPost->image}}'); background-position: center center;"></div>
                    <div class="slidecontent col-md-4 nomargin">
                      <div class="slider-caption">
                        <a href="{{$blogPost->user_profile_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$blogPost->user_profile_info->avatar}}" data-swiper-parallax="-150" /></a>
                        <p data-swiper-parallax="-150">
                          <strong><a href="{{$blogPost->user_profile_info->profile_url}}">{{$blogPost->user_profile_info->full_name}}</a></strong><br>
                          {{$blogPost->user_profile_info->employer_name}}
                        </p>
                        <div class="clear"></div>
                        <p>&nbsp;</p>
                        <div class="clear"></div>
                        <h2 data-swiper-parallax="-200"><a href="{{$blogPost->view_url}}"><strong>{{$blogPost->title}}</strong></a></h2>
                        <p data-swiper-parallax="-100" data-clamp-height="94">{{str_ireplace('</p>','',str_ireplace('<p>','',$blogPost->teaser))}}</p>
                        <div class="clear"></div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
            {{-- <div id="slider-video-arrow-left"><i class="icon-angle-left"></i></div>
            <div id="slider-video-arrow-right" class="pushed"><i class="icon-angle-right"></i></div> --}}
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-{{$item->id}}"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-next-{{$item->id}}"></div>
            <div class="swiper-button-prev swiper-button-prev-{{$item->id}}"></div>
          </section>
        </div>
        <div class="clear"></div>
        {{-- <div class="hidden-sm hidden-xs news-slider-thumbs nomargin">
          <div class="gallery-thumbs gallery-thumbs-news gallery-thumbs-topposts-{{$item->id}} hidden-sm hidden-xs">
            <div class="swiper-wrapper">
              @foreach ($item->top_posts as $blogPost)
                <div class="swiper-slide"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$blogPost->user_profile_info->avatar}}" /> <p data-clamp-height="50" style="padding-top: 0; padding-bottom: 0;">{{$blogPost->title}}</p></div>
              @endforeach
            </div>
          </div>
        </div> --}}
        <div class="clear"></div>
      </div>
    @endforeach
  </div>

  <script>
    function setupSlider (id) {
      if ($('.swiper-topposts-parent-' + id).length > 0) {
        var existing1 = $('.swiper-topposts-parent-' + id)[0].swiper;
        if (existing1) {
          existing1.slideTo(0);
        }
        // var existing2 = $('.gallery-thumbs-topposts-' + id)[0].swiper;
        // if (existing2) {
        //   existing2.slideTo(0);
        // }
      }
      var swiperSlider = new Swiper('.swiper-topposts-parent-' + id, {
        parallax:true,
        autoplay: 5000,
        speed: 300,
        effect: 'fade',
        // nextButton: '#slider-video-arrow-right',
        // prevButton: '#slider-video-arrow-left',
        pagination: '.swiper-pagination-' + id,
        paginationClickable: true,
        nextButton: '.swiper-button-next-' + id,
        prevButton: '.swiper-button-prev-' + id,
      });
      // var galleryThumbs = new Swiper('.gallery-thumbs-topposts-' + id, {
      //   spaceBetween: 0,
      //   direction: 'horizontal',
      //   centeredSlides: true,
      //   slidesPerView: 4,
      //   slideToClickedSlide: true
      // });
      //swiperSlider.params.control = galleryThumbs;
      //galleryThumbs.params.control = swiperSlider;
    }

    jQuery(document).ready(function($){
      $('#top_posts_tabs li:first-child a').click();
      $('#top_posts_tabs li:first-child a').closest('li').addClass('ui-tabs-active ui-state-active').click();
      setupSlider($('#top_posts_tabs li:first-child a').attr('data-id'));

      $('#top_posts_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(e.relatedTarget).closest('li').removeClass('ui-tabs-active ui-state-active');
        $(e.target).closest('li').addClass('ui-tabs-active ui-state-active');
        var id = $(e.target).attr('data-id');
        setupSlider(id);
      });
    });
  </script>
</div>

<!-- <div class="container topmargin bottommargin hidden-md hidden-lg">
  <div id="" class="portfolio-carousel row">
    @foreach ($tabsOfTopPosts->tabs as $item)
      @foreach ($item->top_posts as $blogPost)
        <div class="oc-item zoomhover col-md-4 smallpadding col-sm-6">
          <div class="ipost card-grey clearfix">
            <div class="portfolio-image">
              <a href="#" class="zoomeffect"><img src="/images/placeholder.gif" data-original="/imagecache/370_216{{$blogPost->image}}" alt="{{$blogPost->title}}" class="lazy"></a>
              <div class="portfolio-overlay gradient soft">
                <div class="portfolio-desc">
                  <h3><span><strong>{{$blogPost->time_ago}}</strong> <strong><i class="icon-thumbs-up"></i> {{$blogPost->num_of_likes}}</strong>  <strong><i class="icon-eye"></i> {{$blogPost->view_count}}</strong></span></h3>
                </div>
              </div>
            </div>
            <div class="news-desc"><a href="{{$blogPost->view_url}}">{{$blogPost->title}}</a> </div>
            <div class="portfolio-desc">
              <a href="{{$blogPost->user_profile_info->profile_url}}"><img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$blogPost->user_profile_info->avatar}}" /></a>
              <div class="desc-content">
                <a href="{{$blogPost->user_profile_info->profile_url}}"><h3><strong>{{$blogPost->user_profile_info->full_name}}</strong></h3></a> <span>{{$blogPost->user_profile_info->employer_name}}</span>
              </div><div class="clear"></div>
            </div>
          </div>
        </div>
      @endforeach
    @endforeach
    <div class="clear"></div>
  </div>
</div> -->