@extends('layouts.site')

@section('head')
  <style type="text/css" rel="stylesheet" href="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda-themeless.min.css') }}"></style>
  <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/spin.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/js/ladda-bootstrap/dist/ladda.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/underscore-min.js') }}"></script>
  {{-- <link rel="canonical" href="{{PermalinkEngine::getPermalink(Modules::blog(), $model)}}"> --}}
@stop

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="toppadding bottompadding">
    <div class="container clearfix">

      <div class="nobottommargin clearfix col-md-8">
        <div class="single-post nobottommargin">
          
          <div class="entry clearfix">
            <div class="entry-title">
              <h2>{{$model->title}}</h2>
            </div><!-- .entry-title end -->

            <ul class="entry-meta clearfix">
              <li>{{$model->present()->displayDateInDetails()}}</li>
              <li>Views: {{$model->view_count}}</li>
              <li><a href="#"><i class="icon-thumbs-up"></i> {{$model->num_of_likes}} Likes</a></li>
              <li><a href="#"><i class="icon-comments"></i> {{$model->comments_count}} Comments</a></li>
            </ul>

            <div class="entry-image">
              <a href="#">
                @if (GenericUtility::startsWith($model->image, 'data:'))
                    <img src="{{$model->image}}" alt="{{$model->title}}">
                @else
                  <img src="/imagecache/r_1366_auto_c{{$model->image}}" alt="{{$model->title}}">
                @endif
              </a>
              <p class="legend">{{$model->image_legend}}<br> Photo by: {{$model->image_source}}</p class="legend">
            </div>

            <div class="panel panel-default">
              <div class="panel-body">
                <div class="author-image">
                  <a href="{{$model->present()->userProfileInfo()->profile_url}}"><img src="{{$model->present()->userProfileInfo()->avatar}}" alt=""></a>
                </div>
                {{-- <div style="height:15px; width:100%;"></div> --}}
                <strong>{{$model->present()->userProfileInfo()->full_name}}</strong><br>
                <p class="nomargin">{{$model->present()->userProfileInfo()->job_title}} at {{$model->present()->userProfileInfo()->employer_name}}</p>
                <p class="nomargin">{{$model->present()->userProfileInfo()->based_in_name}}</p>
              </div>
            </div><!-- Post Single - Author End -->

            <div class="entry-content">
              <div>
                {{$model->content}}
              </div>
              <div class="clear"></div>
              <div class="row">
                <div class="col-sm-4">
                  @if ($liked)
                    <a href="#" class="btn btn-warning btn-like-posting" data-like-posting="true" data-module="{{Modules::blog()}}" data-id="{{$model->id}}" disabled="disabled"><i class="icon-thumbs-up"></i> Post Liked</a>
                  @else
                    <a href="#" class="btn btn-warning btn-like-posting" data-like-posting="true" data-module="{{Modules::blog()}}" data-id="{{$model->id}}"><i class="icon-thumbs-up"></i> Like this Post</a>
                  @endif
                </div>
                <div class="si-share noborder clearfix col-sm-8">
                  <div class="pull-right">
                    <?php 
                      $urlToShare = urlencode(PermalinkEngine::getPermalink(Modules::blog(), $model));
                      //$urlToShare = urlencode('http://aidpost.org/blog-posts/solutions-for-inclusive-green-and-resilient-cities-51');
                      //https://www.facebook.com/dialog/share?app_id=735289673264401&display=popup&href={{$urlToShare}}&redirect_uri={{$urlToShare}}
                      //{{-- https://www.facebook.com/sharer/sharer.php?u={{$urlToShare}} --}}
                    ?>
                    <a href="javascript:;" class="social-icon si-borderless si-facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{$urlToShare}}', '', 'scrollbars=0,resizable=0,Width=555px,Height=300px')">
                      <i class="icon-facebook"></i>
                      <i class="icon-facebook"></i>
                    </a>
                    <a href="https://twitter.com/intent/tweet?text={{urlencode($model->title)}}&url={{$urlToShare}}" class="social-icon si-borderless si-twitter" target="_blank">
                      <i class="icon-twitter"></i>
                      <i class="icon-twitter"></i>
                    </a>
                    <a href="mailto:?subject={{$model->title}}&body={{$urlToShare}}" class="social-icon si-borderless si-email3">
                      <i class="icon-email3"></i>
                      <i class="icon-email3"></i>
                    </a>
                  </div>
                  <span class="pull-right">Share this Post:</span>
                </div>
              </div>
            </div><!-- /entry-content -->
          </div><!-- /entry -->

          <!-- Comments
          ============================================= -->
          <div id="comments" class="clearfix">
            @include('pages.common._give_comment', array('fc_module' => Modules::blog(), 'fc_id' => $model->id))
            @include('pages.common._comments_list', array('cl_module' => Modules::blog(), 'cl_id' => $model->id))
            <div class="clear"></div>
          </div><!-- #comments end -->

        </div><!-- /single-post -->
      </div><!-- nobottommargin clearfix col-md-8 end -->

      <!-- Sidebar
      ============================================= -->
      <div class="nobottommargin col_last clearfix col-md-4 sidecol">
        <div class="sidebar-widgets-wrap">
          @include('pages.common._popularnews')

          @include('pages.common._pub_side', array('model' => Publicities::getSidePublicity()))

          @include('pages.common._popularblogs')

          {{-- <div class="widget clearfix">
            <h4>Top Blogs</h4>
            <div id="post-list-footer">
              @foreach ($topPosts as $item)
                <div class="spost clearfix zoomhover">
                  <a href="#" class="zoomeffect"><img src="{{$item->image}}" alt="{{$item->title}}"></a><br>
                  <div class="entry-title">
                    <a href="{{$item->user_profile_info->profile_url}}" class="nobg"><img class="org-logo" src="{{$item->user_profile_info->avatar}}" alt=""></a>
                    <h4><a href="{{$item->view_url}}">{{$item->title}}</a></h4>
                  </div>
                  <div class="clear"></div>
                </div>
              @endforeach
            </div>
          </div> --}}
        </div>
      </div><!-- .sidebar end -->

    </div><!-- /container clearfix end -->
  </div><!-- /toppadding bottompadding end -->
@stop
