@extends('layouts.site')

@section('before_content')
  @include('pages.common._pub_big', array('model' => Publicities::getBigPublicity()))
  <div class="clear"></div>
@stop

@section('content')
  @include('pages.blogs._latest_blogs')
  @include('pages.blogs._top_blogs')
  @include('pages.blogs._all_blogs')
@stop

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('js/app/controller_blogs.js') }}"></script>
@stop