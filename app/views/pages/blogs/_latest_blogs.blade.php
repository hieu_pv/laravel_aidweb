<div class="header-stick clearfix breaking-news hidden-xs" style="padding: 10px 0;">
  <div>
    <div class="container clearfix">
      <h4 class="pull-left nobottommargin">LATEST:</h4>
      <div class="fslider bnews-slider nobottommargin" data-speed="800" data-pause="6000" data-arrows="true" data-pagi="false">
        <div class="flexslider">
          <div class="slider-wrap">
            @foreach ($latestPosts as $item)
              <div class="slide">
                <a href="{{$item->user_profile_info->profile_url}}" class="inline-block">
                  <img class="org-logo lazy" src="/images/placeholder.gif" data-original="{{$item->user_profile_info->avatar}}" />
                </a>
                <a href="{{$item->view_url}}" class="inline-block">
                  <p><strong>{{$item->title}}</strong>{{$item->user_profile_info->full_name}}</p>
                </a>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

