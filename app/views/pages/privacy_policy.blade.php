@extends('layouts.site')

@section('before_content')
  <div class="clear"></div>
@stop

@section('content')
  <div class="bottompadding static-page">
    <div class="container clearfix">

      <div class="nobottommargin clearfix col-md-12">
        <div class="single-post nobottommargin">
          <div class="entry clearfix">
            <div class="entry-title">
              <h1>Privacy Policy</h1>
            </div>
            <div class="entry-content">
              <ol start="1" class="notopmargin">
                <li><h3>Principles</h3></li>
              </ol>

              <p class="justify">AidPost protect your personal information using industry-standard safeguards.We adhere to the following principles to protect your privacy:</p>
              <ul>
                <li>We protect your personal information and will only provide it to third parties: (1) with your consent; (2) where it is necessary to carry out your instructions; (3) as reasonably necessary in order to provide our features and functionality to you; (4) when we reasonably believe it is required by law, subpoena or other legal process; or (5) as necessary to enforce our User Agreement or protect the rights, property, or safety of AidPost, our Members and Visitors, and the public.</li>
                <li>We have implemented appropriate security safeguards designed to protect your information in accordance with industry standards.</li>
                <li>We don&rsquo;t share any of your personal information with third parties for direct marketing.</li>
              </ul>
              <p class="justify">We may modify this Privacy Policy from time to time, and if we make material changes to it, we will provide notice through our Service, or by other means so that you may review the changes before you continue to use our Services.&nbsp;<strong>If you object to any changes, you may close your account. Continuing to use our Services after we publish or communicate a notice about any changes to this Privacy Policy means that you are consenting to the changes.</strong></p>

              <ol start="2">
                <li><h3>What information we collect</h3></li>
              </ol>
              <p class="justify">Our Privacy Policy applies to any Member or Visitor. We collect information when you use our Service to offer you a personalized and relevant experience. The personal information provided to or collected by our Service is controlled by AidPost registered in Canada. <strong>If you have any concern about providing information to us or having such information displayed on our Service or otherwise used in any manner permitted in this Privacy Policy and the User Agreement, you should not become a Member, visit our website, or otherwise use our Service.</strong> If you have already registered, you can close your accounts.</p>
              <p class="justify">We collect your personal information in the following ways:</p>
              <div class="leftmargin-sm">
                <h4>2.1 Registration</h4>
                <p class="justify">When you create an account with us, we collect information (including your email address, and password). To create an account on&nbsp; AidPost, you must provide us with at least your email address, a username, and a password and agree to our&nbsp;<a href="/user-agreement">User Agreement</a>&nbsp;and this Privacy Policy, which governs how we treat your information. You may provide additional information during the registration flow to help you build your profile.. You understand that, by creating an account, we and others will be able to identify you by your AidPost profile.</p>
                
                <h4>2.2 Profile Information</h4>
                <p class="justify">We collect information when you fill out a profile that helps you get found by other people and organisations for opportunities. Most of your profile is publicly displayed so please do not provide personal information you would not want to be public.</p>
                
                <h4>2.3 Using AidPost Site</h4>
                <p class="justify">We collect information when you use (whether as a Member or a Visitor) our website. For example, if you are logged in on AidPost.org, one of our cookies on your device identifies you, your usage information, and your IP address will be associated by us with your account. Even if you&rsquo;re not logged into a Service, we log information about devices used to access our Service, including IP address.</p>
                
                <h4>2.4 Cookies</h4>
                <p class="justify">We use cookies and similar technologies, including mobile application identifiers, to help us learn about your interests, improve your experience, increase security, and measure use and effectiveness of our Service. You can control cookies through your browser settings and other tools.&nbsp;By visiting our Service, you consent to the placement of cookies and beacons in your browser and HTML-based emails in accordance with this Privacy Policy.</p>
                
                <h4>2.5 Data Retention</h4>
                <p class="justify">We keep your information for as long as your account is active or as needed. For example, we may keep certain information even after you close your account if it is necessary to comply with our legal obligations, meet regulatory requirements, resolve disputes, prevent fraud and abuse, or enforce this agreement.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@stop

@section('after_content')
  
@stop