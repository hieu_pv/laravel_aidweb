<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTakeActionsCountries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('take_actions_countries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('takeaction_id')->unsigned()->index();
      $table->foreign('takeaction_id')->references('id')->on('take_actions');
      $table->integer('country_id')->unsigned()->index();
      $table->foreign('country_id')->references('id')->on('countries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('take_actions_countries')) {
      Schema::drop('take_actions_countries');
    }
  }

}
