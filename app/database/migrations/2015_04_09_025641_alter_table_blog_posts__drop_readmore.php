<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBlogPostsDropReadmore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('blog_posts')) {
			if (Schema::hasColumn('blog_posts', 'read_more'))
			{
				Schema::table('blog_posts', function($table)
				{
						$table->dropColumn('read_more');
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// no rollback, the intention is to delete / clean up this column
	}

}
