<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilterTables extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('countries', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });

    Schema::create('regions', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });

    Schema::create('crisis', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });

    Schema::create('sectors', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });

    Schema::create('themes', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });

    Schema::create('beneficiaries', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('countries')) {
      Schema::drop('countries');
    }
    if (Schema::hasTable('regions')) {
      Schema::drop('regions');
    }
    if (Schema::hasTable('crisis')) {
      Schema::drop('crisis');
    }
    if (Schema::hasTable('sectors')) {
      Schema::drop('sectors');
    }
    if (Schema::hasTable('themes')) {
      Schema::drop('themes');
    }
    if (Schema::hasTable('beneficiaries')) {
      Schema::drop('beneficiaries');
    }
  }

}
