<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlogPostsBeneficiaries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('blog_posts_beneficiaries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('post_id')->unsigned()->index();
      $table->foreign('post_id')->references('id')->on('blog_posts');
      $table->integer('beneficiary_id')->unsigned()->index();
      $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('blog_posts_beneficiaries');
    if (Schema::hasTable('blog_posts_beneficiaries')) {
      Schema::drop('blog_posts_beneficiaries');
    }
  }

}
