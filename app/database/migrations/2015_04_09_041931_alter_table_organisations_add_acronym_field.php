<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrganisationsAddAcronymField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->string('org_name_acronym')->nullable();
				$table->boolean('use_org_name_acronym')->nullable()->default(false);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->dropColumn('org_name_acronym');
				$table->dropColumn('use_org_name_acronym');
			});
		}
	}

}
