<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTakeActionsThemes extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('take_actions_themes', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('takeaction_id')->unsigned()->index();
      $table->foreign('takeaction_id')->references('id')->on('take_actions');
      $table->integer('theme_id')->unsigned()->index();
      $table->foreign('theme_id')->references('id')->on('themes');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('take_actions_themes')) {
      Schema::drop('take_actions_themes');
    }
  }

}
