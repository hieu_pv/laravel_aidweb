<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePublicitiesPlatforms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('publicities_platforms', function ($table)
		{
			$table->increments('id');
			$table->integer('publicity_id');
			$table->integer('platform_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('publicities_platforms')) {
			Schema::drop('publicities_platforms');
		}
	}

}
