<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePublicitiesAddPublishedIntervalInSeconds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('publicities')) {
			Schema::table('publicities', function($table)
			{
				$table->integer('published_interval_in_seconds')->default(0);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('publicities')) {
			Schema::table('publicities', function($table)
			{
				$table->dropColumn('published_interval_in_seconds');
			});
		}
	}

}
