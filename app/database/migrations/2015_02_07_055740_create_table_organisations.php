<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrganisations extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('organisations', function ($table) 
    {
      $table->increments('id');
      
      $table->integer('cp_id')->unsigned()->index();
      $table->foreign('cp_id')->references('id')->on('users');
      
      $table->string('org_logo')->default('/images/noimage.png');
      $table->string('org_name');
      $table->string('org_email');
      $table->string('org_website');

      $table->integer('office_location')->unsigned()->index()->nullable();
      $table->foreign('office_location')->references('id')->on('countries');
      
      $table->integer('office_type')->unsigned()->index()->nullable();
      $table->foreign('office_type')->references('id')->on('office_types');
      
      $table->integer('org_type')->unsigned()->index()->nullable();
      $table->foreign('org_type')->references('id')->on('organisation_types');

      $table->boolean('top_agency')->default(false);

      $table->boolean('is_active')->default(true);
      
      $table->timestamps();
      
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('organisations')) {
      Schema::drop('organisations');
    }
  }

}
