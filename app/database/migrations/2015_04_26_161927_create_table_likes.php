<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLikes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('likes', function ($table)
		{
			$table->increments('id');
			$table->integer('module_id');
			$table->integer('item_id');
			$table->integer('liked_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('likes')) {
			Schema::drop('likes');
		}
	}

}
