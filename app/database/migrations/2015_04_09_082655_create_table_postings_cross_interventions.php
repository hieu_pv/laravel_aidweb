<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostingsCrossInterventions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_posts_interventions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->unsigned()->index();
			$table->foreign('post_id')->references('id')->on('blog_posts');
			$table->integer('intervention_id')->unsigned()->index();
			$table->foreign('intervention_id')->references('id')->on('interventions');
			$table->timestamps();
		});

		Schema::create('news_items_interventions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('newsitem_id')->unsigned()->index();
			$table->foreign('newsitem_id')->references('id')->on('news_items');
			$table->integer('intervention_id')->unsigned()->index();
			$table->foreign('intervention_id')->references('id')->on('interventions');
			$table->timestamps();
		});

		Schema::create('take_actions_interventions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('takeaction_id')->unsigned()->index();
			$table->foreign('takeaction_id')->references('id')->on('take_actions');
			$table->integer('intervention_id')->unsigned()->index();
			$table->foreign('intervention_id')->references('id')->on('interventions');
			$table->timestamps();
		});

		Schema::create('videos_interventions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('video_id')->unsigned()->index();
			$table->foreign('video_id')->references('id')->on('videos');
			$table->integer('intervention_id')->unsigned()->index();
			$table->foreign('intervention_id')->references('id')->on('interventions');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('blog_posts_interventions')) {
			Schema::drop('blog_posts_interventions');
		}

		if (Schema::hasTable('news_items_interventions')) {
			Schema::drop('news_items_interventions');
		}

		if (Schema::hasTable('take_actions_interventions')) {
			Schema::drop('take_actions_interventions');
		}

		if (Schema::hasTable('videos_interventions')) {
			Schema::drop('videos_interventions');
		}
	}

}
