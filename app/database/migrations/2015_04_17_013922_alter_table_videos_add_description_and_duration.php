<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVideosAddDescriptionAndDuration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('videos')) {
			Schema::table('videos', function($table)
			{
				$table->text('description')->nullable();
				$table->string('duration')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('videos')) {
			Schema::table('videos', function($table)
			{
				$table->dropColumn('description');
				$table->dropColumn('duration');
			});
		}
	}

}
