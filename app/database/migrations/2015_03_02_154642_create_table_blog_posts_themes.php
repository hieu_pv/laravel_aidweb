<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlogPostsThemes extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('blog_posts_themes', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('post_id')->unsigned()->index();
      $table->foreign('post_id')->references('id')->on('blog_posts');
      $table->integer('theme_id')->unsigned()->index();
      $table->foreign('theme_id')->references('id')->on('themes');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('blog_posts_themes');
    if (Schema::hasTable('blog_posts_themes')) {
      Schema::drop('blog_posts_themes');
    }
  }

}
