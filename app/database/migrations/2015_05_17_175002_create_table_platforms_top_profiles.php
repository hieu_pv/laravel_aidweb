<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlatformsTopProfiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('platforms_top_profiles', function ($table)
		{
			$table->increments('id');
			$table->integer('profile_id');
			// type == 1 : individual
			// type == 2 : organisation
			// type == 99 : admin
			$table->integer('profile_type');
			$table->integer('platform_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('platforms_top_profiles')) {
			Schema::drop('platforms_top_profiles');
		}
	}

}
