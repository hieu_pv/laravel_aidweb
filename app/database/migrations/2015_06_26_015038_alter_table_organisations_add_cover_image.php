<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrganisationsAddCoverImage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->string('cover_image')->default('/images/noimage2.jpg');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->dropColumn('cover_image');
			});
		}
	}

}
