<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTakeActions extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('take_actions', function ($table) {
      $table->increments('id');
      $table->string('name');
      $table->string('title');
      $table->string('slug');
      $table->longtext('content');
      $table->string('image')->default('/images/noimage.png');
      $table->string('url');

      $table->boolean('published')->default(true);
      $table->boolean('featured')->default(false);
      $table->boolean('deleted')->default(false);
      
      $table->timestamps();

      $table->integer('type_id')->nullable()->unsigned()->index();
      $table->foreign('type_id')->references('id')->on('types_of_take_actions');

      $table->integer('organisation_id')->nullable()->unsigned()->index();
      $table->foreign('organisation_id')->references('id')->on('organisations');
      
      $table->integer('user_profile_id')->nullable()->unsigned()->index();
      $table->foreign('user_profile_id')->references('id')->on('user_profiles');

      $table->integer('user_id')->nullable()->unsigned()->index();
      $table->foreign('user_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('take_actions');
    if (Schema::hasTable('take_actions')) {
      Schema::drop('take_actions');
    }
  }

}
