<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTypesOfPublicities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('types_of_publicities', function ($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->boolean('deleted')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('types_of_publicities')) {
			Schema::drop('types_of_publicities');
		}
	}

}
