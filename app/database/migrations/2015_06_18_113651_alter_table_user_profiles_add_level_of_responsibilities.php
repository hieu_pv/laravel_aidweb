<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserProfilesAddLevelOfResponsibilities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('user_profiles')) {
			Schema::table('user_profiles', function($table)
			{
				$table->integer('level_of_responsibility')->unsigned()->index()->nullable();
				$table->foreign('level_of_responsibility')->references('id')->on('level_of_responsibilities');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('user_profiles')) {
			Schema::table('user_profiles', function($table)
			{
				$table->dropForeign('user_profiles_level_of_responsibility_foreign');
				$table->dropColumn('level_of_responsibility');
			});
		}
	}

}
