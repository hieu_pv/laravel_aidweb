<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsItemsCountries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_items_countries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('newsitem_id')->unsigned()->index();
      $table->foreign('newsitem_id')->references('id')->on('news_items');
      $table->integer('country_id')->unsigned()->index();
      $table->foreign('country_id')->references('id')->on('countries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('news_items_countries');
    if (Schema::hasTable('news_items_countries')) {
      Schema::drop('news_items_countries');
    }
  }

}
