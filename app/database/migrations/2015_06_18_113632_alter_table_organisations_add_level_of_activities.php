<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrganisationsAddLevelOfActivities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->integer('level_of_activity')->unsigned()->index()->nullable();
				$table->foreign('level_of_activity')->references('id')->on('level_of_activities');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->dropForeign('organisations_level_of_activity_foreign');
				$table->dropColumn('level_of_activity');
			});
		}
	}

}
