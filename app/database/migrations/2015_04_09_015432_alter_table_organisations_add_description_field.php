<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrganisationsAddDescriptionField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->text('description')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->dropColumn('description');
			});
		}
	}

}
