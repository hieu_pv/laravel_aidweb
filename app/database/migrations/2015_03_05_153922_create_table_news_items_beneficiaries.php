<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsItemsBeneficiaries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_items_beneficiaries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('newsitem_id')->unsigned()->index();
      $table->foreign('newsitem_id')->references('id')->on('news_items');
      $table->integer('beneficiary_id')->unsigned()->index();
      $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('news_items_beneficiaries');
    if (Schema::hasTable('news_items_beneficiaries')) {
      Schema::drop('news_items_beneficiaries');
    }
  }

}
