<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlogPostsRegions extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('blog_posts_regions', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('post_id')->unsigned()->index();
      $table->foreign('post_id')->references('id')->on('blog_posts');
      $table->integer('region_id')->unsigned()->index();
      $table->foreign('region_id')->references('id')->on('regions');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('blog_posts_regions');
    if (Schema::hasTable('blog_posts_regions')) {
      Schema::drop('blog_posts_regions');
    }
  }

}
