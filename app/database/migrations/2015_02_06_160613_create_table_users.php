<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function ($table) 
    {
      $table->increments('id');
      $table->string('username')->unique();
      $table->string('email'); //->unique();
      $table->string('password');
      $table->rememberToken();
      $table->timestamps();
      // type == 1 : individual
      // type == 2 : organisation
      // type == 99 : admin
      $table->integer('profile_type')->nullable()->default(1);
      
      $table->boolean('verified')->default(false);
      $table->boolean('is_new')->default(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('users')) {
      Schema::drop('users');
    }
  }

}
