<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTakeActionsSectors extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('take_actions_sectors', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('takeaction_id')->unsigned()->index();
      $table->foreign('takeaction_id')->references('id')->on('take_actions');
      $table->integer('sector_id')->unsigned()->index();
      $table->foreign('sector_id')->references('id')->on('sectors');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('take_actions_sectors')) {
      Schema::drop('take_actions_sectors');
    }
  }

}
