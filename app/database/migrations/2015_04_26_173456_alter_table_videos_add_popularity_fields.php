<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVideosAddPopularityFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		if (Schema::hasTable('videos')) {
			Schema::table('videos', function($table)
			{
				$table->integer('view_count')->default(0);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('videos')) {
			Schema::table('videos', function($table)
			{
				$table->dropColumn('view_count');
			});
		}
	}

}
