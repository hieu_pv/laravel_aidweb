<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlogPostsSectors extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('blog_posts_sectors', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('post_id')->unsigned()->index();
      $table->foreign('post_id')->references('id')->on('blog_posts');
      $table->integer('sector_id')->unsigned()->index();
      $table->foreign('sector_id')->references('id')->on('sectors');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('blog_posts_sectors');
    if (Schema::hasTable('blog_posts_sectors')) {
      Schema::drop('blog_posts_sectors');
    }
  }
}
