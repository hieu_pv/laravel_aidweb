<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserProfiles extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('user_profiles', function ($table) 
    {
      $table->increments('id');
      
      $table->integer('user_id')->unsigned()->index();
      $table->foreign('user_id')->references('id')->on('users');
      
      $table->string('first_name');
      $table->string('last_name');

      $table->integer('nationality')->unsigned()->index()->nullable();
      $table->foreign('nationality')->references('id')->on('nationalities');
      
      $table->integer('based_in')->unsigned()->index()->nullable();
      $table->foreign('based_in')->references('id')->on('countries');
      
      $table->string('job_title')->nullable();
      $table->string('employer_name')->nullable();
      $table->string('avatar')->nullable()->default('/images/noavatar.png');
      $table->string('thought')->nullable();
      
      $table->integer('member_status')->unsigned()->index()->nullable();
      $table->foreign('member_status')->references('id')->on('member_statuses');
      
      $table->integer('professional_status')->unsigned()->index()->nullable();
      $table->foreign('professional_status')->references('id')->on('professional_statuses');
      
      $table->boolean('top_member')->default(false);

      $table->boolean('is_active')->default(true);

      $table->timestamps();
      
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('user_profiles')) {
      Schema::drop('user_profiles');
    }
  }

}
