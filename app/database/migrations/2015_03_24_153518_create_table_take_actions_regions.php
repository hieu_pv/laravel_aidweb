<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTakeActionsRegions extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('take_actions_regions', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('takeaction_id')->unsigned()->index();
      $table->foreign('takeaction_id')->references('id')->on('take_actions');
      $table->integer('region_id')->unsigned()->index();
      $table->foreign('region_id')->references('id')->on('regions');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('take_actions_regions')) {
      Schema::drop('take_actions_regions');
    }
  }

}
