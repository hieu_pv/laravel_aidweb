<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsItemsThemes extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_items_themes', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('newsitem_id')->unsigned()->index();
      $table->foreign('newsitem_id')->references('id')->on('news_items');
      $table->integer('theme_id')->unsigned()->index();
      $table->foreign('theme_id')->references('id')->on('themes');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('news_items_themes');
    if (Schema::hasTable('news_items_themes')) {
      Schema::drop('news_items_themes');
    }
  }

}
