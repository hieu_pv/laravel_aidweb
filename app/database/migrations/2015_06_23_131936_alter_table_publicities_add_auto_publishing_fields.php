<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePublicitiesAddAutoPublishingFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('publicities')) {
			Schema::table('publicities', function($table)
			{
				$table->dateTime('published_start_date')->default('0000-00-00 00:00:00');

				// ISO 8601 interval P[n]Y[n]M[n]DT[n]H[n]M[n]S
				$table->string('published_interval')->default('P0Y7M0DT0H0M0S');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('publicities')) {
			Schema::table('publicities', function($table)
			{
				$table->dropColumn('published_start_date');
				$table->dropColumn('published_interval');
			});
		}
	}

}
