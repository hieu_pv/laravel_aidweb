<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlogPostsCountries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('blog_posts_countries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('post_id')->unsigned()->index();
      $table->foreign('post_id')->references('id')->on('blog_posts');
      $table->integer('country_id')->unsigned()->index();
      $table->foreign('country_id')->references('id')->on('countries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('blog_posts_countries');
    if (Schema::hasTable('blog_posts_countries')) {
      Schema::drop('blog_posts_countries');
    }
  }

}
