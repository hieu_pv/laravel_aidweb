<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserProfilesAddGenderField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('user_profiles')) {
			Schema::table('user_profiles', function($table)
			{
				$table->string('gender')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('user_profiles')) {
			Schema::table('user_profiles', function($table)
			{
				$table->dropColumn('gender');
			});
		}
	}

}
