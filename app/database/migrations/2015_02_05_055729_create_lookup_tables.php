<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookupTables extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('nationalities', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->timestamps();
      
    });

    Schema::create('bases', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->timestamps();

    });

    Schema::create('member_statuses', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->string('description');
      $table->timestamps();

    });

    Schema::create('professional_statuses', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->timestamps();

    });

    Schema::create('office_locations', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->timestamps();

    });

    Schema::create('office_types', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->timestamps();

    });

    Schema::create('organisation_types', function ($table) 
    {
      $table->increments('id');
      $table->string('name');
      $table->string('display_name');
      $table->timestamps();

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('nationalities')) {
      Schema::drop('nationalities');
    }
    if (Schema::hasTable('bases')) {
      Schema::drop('bases');
    }
    if (Schema::hasTable('member_statuses')) {
      Schema::drop('member_statuses');
    }
    if (Schema::hasTable('professional_statuses')) {
      Schema::drop('professional_statuses');
    }
    if (Schema::hasTable('office_locations')) {
      Schema::drop('office_locations');
    }
    if (Schema::hasTable('office_types')) {
      Schema::drop('office_types');
    }
    if (Schema::hasTable('organisation_types')) {
      Schema::drop('organisation_types');
    }
  }

}
