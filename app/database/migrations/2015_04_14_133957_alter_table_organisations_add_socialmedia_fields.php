<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrganisationsAddSocialmediaFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->string('facebook_url')->nullable();
				$table->string('twitter_url')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('organisations')) {
			Schema::table('organisations', function($table)
			{
				$table->dropColumn('facebook_url');
				$table->dropColumn('twitter_url');
			});
		}
	}

}
