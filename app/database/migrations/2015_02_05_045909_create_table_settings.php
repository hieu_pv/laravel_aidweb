<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSettings extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    //
    Schema::create('settings', function ($table) 
    {
      $table->increments('id');
      $table->string('key');
      $table->text('value')->nullable();
      $table->text('default_value');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('settings')) {
      Schema::drop('settings');
    }
  }

}
