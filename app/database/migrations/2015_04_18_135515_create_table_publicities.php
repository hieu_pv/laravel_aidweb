<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePublicities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('publicities', function ($table)
		{
			$table->increments('id');
			// type big
			$table->string('a_title')->nullable();
			$table->string('a_title_color')->nullable();
			$table->text('a_text')->nullable();
			$table->string('a_text_color')->nullable();
			$table->string('a_button_text')->nullable();
			$table->string('a_button_text_color')->nullable();
			$table->string('a_position')->nullable();
			$table->string('a_image')->nullable()->default('/images/noimage.png');
			$table->string('a_url')->nullable();

			// type small, header
			$table->string('b_title')->nullable();
			$table->string('b_image')->nullable()->default('/images/noimage.png');
			$table->string('b_url')->nullable();

			// type sidebar
			$table->string('c_title')->nullable();
			$table->string('c_image')->nullable()->default('/images/noimage.png');
			$table->string('c_url')->nullable();

			$table->integer('type_id')->nullable()->unsigned()->index();
      $table->foreign('type_id')->references('id')->on('types_of_publicities');

			$table->boolean('published')->default(true);
			$table->boolean('deleted')->default(false);
			$table->boolean('featured')->default(false);

			$table->timestamps();

			$table->integer('organisation_id')->nullable()->unsigned()->index();
			$table->foreign('organisation_id')->references('id')->on('organisations');
			
			$table->integer('user_profile_id')->nullable()->unsigned()->index();
			$table->foreign('user_profile_id')->references('id')->on('user_profiles');

			$table->integer('user_id')->nullable()->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('publicities')) {
			Schema::drop('publicities');
		}
	}

}
