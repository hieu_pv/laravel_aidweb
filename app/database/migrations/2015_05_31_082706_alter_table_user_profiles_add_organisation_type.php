<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserProfilesAddOrganisationType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('user_profiles')) {
			Schema::table('user_profiles', function($table)
			{
				$table->integer('employer_organisation_type')->unsigned()->index()->nullable();
				$table->foreign('employer_organisation_type')->references('id')->on('organisation_types');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('user_profiles')) {
			Schema::table('user_profiles', function($table)
			{
				// <table_name>_<foreign_table_name>_<column_name>_foreign
				$table->dropForeign('user_profiles_employer_organisation_type_foreign');
				$table->dropColumn('employer_organisation_type');
			});
		}
	}

}
