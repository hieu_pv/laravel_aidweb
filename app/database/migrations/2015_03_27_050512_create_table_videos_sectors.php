<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideosSectors extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('videos_sectors', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('video_id')->unsigned()->index();
      $table->foreign('video_id')->references('id')->on('videos');
      $table->integer('sector_id')->unsigned()->index();
      $table->foreign('sector_id')->references('id')->on('sectors');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('videos_sectors')) {
      Schema::drop('videos_sectors');
    }
  }

}
