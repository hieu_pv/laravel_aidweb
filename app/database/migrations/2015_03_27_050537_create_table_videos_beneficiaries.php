<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideosBeneficiaries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('videos_beneficiaries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('video_id')->unsigned()->index();
      $table->foreign('video_id')->references('id')->on('videos');
      $table->integer('beneficiary_id')->unsigned()->index();
      $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('videos_beneficiaries')) {
      Schema::drop('videos_beneficiaries');
    }
  }

}
