<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideosThemes extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('videos_themes', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('video_id')->unsigned()->index();
      $table->foreign('video_id')->references('id')->on('videos');
      $table->integer('theme_id')->unsigned()->index();
      $table->foreign('theme_id')->references('id')->on('themes');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('videos_themes')) {
      Schema::drop('videos_themes');
    }
  }

}
