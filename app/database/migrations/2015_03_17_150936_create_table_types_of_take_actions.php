<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTypesOfTakeActions extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('types_of_take_actions', function ($table) {
      //$table->integer('id');
      $table->increments('id');
      $table->string('name');
      $table->string('color');
      $table->boolean('deleted')->default(false);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('types_of_take_actions');
    if (Schema::hasTable('types_of_take_actions')) {
      Schema::drop('types_of_take_actions');
    }
  }

}
