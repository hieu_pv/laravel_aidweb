<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlogPosts extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('blog_posts', function ($table) {
      $table->increments('id');
      $table->string('name');
      $table->string('title');
      $table->string('slug');
      $table->longtext('content');

      $table->string('image')->default('/images/noimage.png');
      $table->string('image_source')->nullable();
      $table->string('image_legend')->nullable();

      $table->integer('comments_count')->default(0);
      
      $table->boolean('published')->default(true);
      $table->boolean('deleted')->default(false);
      $table->boolean('featured')->default(false);

      $table->timestamps();

      $table->integer('organisation_id')->nullable()->unsigned()->index();
      $table->foreign('organisation_id')->references('id')->on('organisations');
      
      $table->integer('user_profile_id')->nullable()->unsigned()->index();
      $table->foreign('user_profile_id')->references('id')->on('user_profiles');

      $table->integer('user_id')->nullable()->unsigned()->index();
      $table->foreign('user_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
    // Schema::table('blog_posts', function ($table) {
    //   $table->dropIndex('search_posts');
    // });
    if (Schema::hasTable('blog_posts')) {
      Schema::drop('blog_posts');
    }
  }

}
