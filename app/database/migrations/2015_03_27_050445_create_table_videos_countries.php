<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideosCountries extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('videos_countries', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('video_id')->unsigned()->index();
      $table->foreign('video_id')->references('id')->on('videos');
      $table->integer('country_id')->unsigned()->index();
      $table->foreign('country_id')->references('id')->on('countries');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('videos_countries')) {
      Schema::drop('videos_countries');
    }
  }

}
