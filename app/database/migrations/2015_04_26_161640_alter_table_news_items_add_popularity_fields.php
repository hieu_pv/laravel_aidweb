<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNewsItemsAddPopularityFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		if (Schema::hasTable('news_items')) {
			Schema::table('news_items', function($table)
			{
				$table->integer('view_count')->default(0);
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		if (Schema::hasTable('news_items')) {
			Schema::table('news_items', function($table)
			{
				$table->dropColumn('view_count');
			});
		}
	}

}
