<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTakeActionsCrisis extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('take_actions_crisis', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('takeaction_id')->unsigned()->index();
      $table->foreign('takeaction_id')->references('id')->on('take_actions');
      $table->integer('crisis_id')->unsigned()->index();
      $table->foreign('crisis_id')->references('id')->on('crisis');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('take_actions_crisis')) {
      Schema::drop('take_actions_crisis');
    }
  }

}
