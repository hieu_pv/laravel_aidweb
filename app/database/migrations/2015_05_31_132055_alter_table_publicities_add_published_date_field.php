<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePublicitiesAddPublishedDateField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('publicities')) {
			Schema::table('publicities', function($table)
			{
				$table->dateTime('published_at')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('publicities')) {
			Schema::table('publicities', function($table)
			{
				$table->dropColumn('published_at');
			});
		}

	}

}
