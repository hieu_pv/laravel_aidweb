<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideosRegions extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('videos_regions', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('video_id')->unsigned()->index();
      $table->foreign('video_id')->references('id')->on('videos');
      $table->integer('region_id')->unsigned()->index();
      $table->foreign('region_id')->references('id')->on('regions');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('videos_regions')) {
      Schema::drop('videos_regions');
    }
  }

}
