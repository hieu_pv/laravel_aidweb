<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideos extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('videos', function ($table) {
      $table->increments('id');
      $table->string('title');
      $table->string('slug');
      $table->string('author')->nullable();
      $table->string('image')->default('/images/noimage.png');
      $table->string('url');
      $table->string('youtube_id')->nullable();

      $table->boolean('published')->default(true);
      $table->boolean('deleted')->default(false);
      $table->boolean('featured')->default(false);

      $table->timestamps();

      $table->integer('organisation_id')->nullable()->unsigned()->index();
      $table->foreign('organisation_id')->references('id')->on('organisations');
      
      $table->integer('user_profile_id')->nullable()->unsigned()->index();
      $table->foreign('user_profile_id')->references('id')->on('user_profiles');

      $table->integer('user_id')->nullable()->unsigned()->index();
      $table->foreign('user_id')->references('id')->on('users');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('videos')) {
      Schema::drop('videos');
    }
  }

}
