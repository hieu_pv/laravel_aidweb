<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsItemsCrisis extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_items_crisis', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('newsitem_id')->unsigned()->index();
      $table->foreign('newsitem_id')->references('id')->on('news_items');
      $table->integer('crisis_id')->unsigned()->index();
      $table->foreign('crisis_id')->references('id')->on('crisis');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('news_items_crisis');
    if (Schema::hasTable('news_items_crisis')) {
      Schema::drop('news_items_crisis');
    }
  }
  
}
