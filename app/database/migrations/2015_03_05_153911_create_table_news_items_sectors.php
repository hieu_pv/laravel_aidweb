<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsItemsSectors extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_items_sectors', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('newsitem_id')->unsigned()->index();
      $table->foreign('newsitem_id')->references('id')->on('news_items');
      $table->integer('sector_id')->unsigned()->index();
      $table->foreign('sector_id')->references('id')->on('sectors');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Schema::drop('news_items_sectors');
    if (Schema::hasTable('news_items_sectors')) {
      Schema::drop('news_items_sectors');
    }
  }

}
