<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNewsItemsDropReadmore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('news_items')) {
			if (Schema::hasColumn('news_items', 'read_more'))
			{
				Schema::table('news_items', function($table)
				{
						$table->dropColumn('read_more');
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// no rollback, the intention is to delete / clean up this column
		// if (Schema::hasTable('news_items')) {
		// 	if (!Schema::hasColumn('news_items', 'read_more'))
		// 	{
		// 		Schema::table('news_items', function($table)
		// 		{
						
		// 		});
		// 	}
			
		// }
	}

}
