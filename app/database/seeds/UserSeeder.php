<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Libraries\Models\User;
use Libraries\Models\UserProfile;
use Libraries\Models\Organisation;
use Jenssegers\Date\Date;
use Libraries\Support\Modules;

class UserSeeder extends Seeder {

  public function run()
  {
    $dv = new \DataValues();

    // orgs
    for ($i=0; $i < 50; $i++) { 
        $user = new User();
        $user->username = 'organisation_'.$i;
        $user->password = Hash::make('fleava');
        $user->email = 'organisation'.$i.'@mailinator.com';
        $user->profile_type = Modules::PROFILE_TYPE_ORGANISATION;
        $user->verified = true;
        $user->is_new = false;
        $user->save();
        $profile = new UserProfile();
        $profile->user_id = $user->id;

        $fn = $i % 100;
        $profile->first_name = $dv->firstnames[$fn];
        
        $ln = $i % 100;
        $profile->last_name = $dv->lastnames[$ln];
        
        $profile->job_title = 'owner';
        $profile->employer_name = 'self';
        $profile->thought = 'Lorem Ipsum Dolor Sit Amet';
        $profile->save();
        $organisation = new Organisation();
        $organisation->cp_id = $user->id;
        
        $on = $i % 100;
        $organisation->org_name = $dv->orgnames[$on];

        $organisation->org_email = 'info@organisation'.$i.'.com';
        $organisation->org_website = 'www.organisation'.$i.'.com';
        $organisation->office_location = rand(1, count($dv->countries));
        $organisation->office_type = rand(1, count($dv->office_types));
        $organisation->org_type = rand(1, count($dv->organisation_types));
        
        // if ($i % 2 === 0) {
        //     $organisation->top_agency = true;
        // } else {
        //     $organisation->top_agency = false;
        // }

        $organisation->org_logo = '/images/logos/dummy/'.($i + 1).'.jpg';
        $organisation->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum hoc idem saepe faciamus. Quasi ego id curem, quid ille aiat aut neget. Duo Reges: constructio interrete. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum.';
        $organisation->org_phone = '1234567890';

        $d = rand(1391588787, 1423124787);
        $d2 = rand(1391588787, 1423124787);
        $organisation->created_at = Date::createFromTimeStamp($d);
        $organisation->updated_at = Date::createFromTimeStamp($d2);
        $organisation->level_of_activity = 3;
        $organisation->save();

        $date2 = Date::now()->format('Y-m-d H:i');
        DB::table('platforms_profiles')->insert(array(
            'platform_id' => 1,
            'profile_id' => $organisation->id,
            'profile_type' => Modules::PROFILE_TYPE_ORGANISATION,
            'created_at' => $date2,
            'updated_at' => $date2
        ));
        if ($i % 2 === 0) {
            DB::table('platforms_profiles')->insert(array(
                'platform_id' => 2,
                'profile_id' => $organisation->id,
                'profile_type' => Modules::PROFILE_TYPE_ORGANISATION,
                'created_at' => $date2,
                'updated_at' => $date2
            ));
            DB::table('platforms_top_profiles')->insert(array(
                'platform_id' => 2,
                'profile_id' => $organisation->id,
                'profile_type' => Modules::PROFILE_TYPE_ORGANISATION,
                'created_at' => $date2,
                'updated_at' => $date2
            ));
        } else {
            DB::table('platforms_profiles')->insert(array(
                'platform_id' => 3,
                'profile_id' => $organisation->id,
                'profile_type' => Modules::PROFILE_TYPE_ORGANISATION,
                'created_at' => $date2,
                'updated_at' => $date2
            ));
            DB::table('platforms_top_profiles')->insert(array(
                'platform_id' => 3,
                'profile_id' => $organisation->id,
                'profile_type' => Modules::PROFILE_TYPE_ORGANISATION,
                'created_at' => $date2,
                'updated_at' => $date2
            ));
        }
        DB::table('platforms_top_profiles')->insert(array(
            'platform_id' => 1,
            'profile_id' => $organisation->id,
            'profile_type' => Modules::PROFILE_TYPE_ORGANISATION,
            'created_at' => $date2,
            'updated_at' => $date2
        ));
    }
    
    // inds
    for ($i=0; $i < 50; $i++) { 
        $user = new User();
        $user->username = 'individual_'.$i;
        $user->password = Hash::make('fleava');
        $user->email = 'individual'.$i.'@mailinator.com';
        $user->profile_type = Modules::PROFILE_TYPE_INDIVIDUAL;
        $user->verified = true;
        $user->is_new = false;
        $user->save();
        $profile = new UserProfile();
        $profile->user_id = $user->id;

        $fn = $i % 100;
        $profile->first_name = $dv->firstnames[$fn];
        
        $ln = $i % 100;
        $profile->last_name = $dv->lastnames[$ln];
        
        $profile->job_title = 'myself';
        $profile->employer_name = 'myself';
        $profile->thought = 'Lorem Ipsum';
        $profile->nationality = rand(1, count($dv->nationalities));
        $profile->based_in = rand(1, count($dv->countries));
        $profile->member_status = rand(1, count($dv->member_statuses));
        $profile->professional_status = rand(1, count($dv->professional_statuses));
        $profile->avatar = '/images/users/'.($i + 1).'.png';

        // if ($i % 2 === 0) {
        //     $profile->top_member = true;
        // } else {
        //     $profile->top_member = false;
        // }
        // //$profile->top_member = true;

        $profile->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum hoc idem saepe faciamus. Quasi ego id curem, quid ille aiat aut neget. Duo Reges: constructio interrete. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum.';
        $profile->level_of_responsibility = 3;

        $d = rand(1391588787, 1423124787);
        $d2 = rand(1391588787, 1423124787);
        $profile->created_at = Date::createFromTimeStamp($d);
        $profile->updated_at = Date::createFromTimeStamp($d2);

        $profile->employer_organisation_type = rand(1, count($dv->organisation_types));

        $profile->save();

        $date3 = Date::now()->format('Y-m-d H:i');
        DB::table('platforms_profiles')->insert(array(
            'platform_id' => 1,
            'profile_id' => $profile->id,
            'profile_type' => Modules::PROFILE_TYPE_INDIVIDUAL,
            'created_at' => $date3,
            'updated_at' => $date3
        ));
        if ($i % 2 === 0) {
            DB::table('platforms_profiles')->insert(array(
                'platform_id' => 2,
                'profile_id' => $profile->id,
                'profile_type' => Modules::PROFILE_TYPE_INDIVIDUAL,
                'created_at' => $date3,
                'updated_at' => $date3
            ));
            DB::table('platforms_top_profiles')->insert(array(
                'platform_id' => 2,
                'profile_id' => $profile->id,
                'profile_type' => Modules::PROFILE_TYPE_INDIVIDUAL,
                'created_at' => $date3,
                'updated_at' => $date3
            ));
        } else {
            DB::table('platforms_profiles')->insert(array(
                'platform_id' => 3,
                'profile_id' => $profile->id,
                'profile_type' => Modules::PROFILE_TYPE_INDIVIDUAL,
                'created_at' => $date3,
                'updated_at' => $date3
            ));
            DB::table('platforms_top_profiles')->insert(array(
                'platform_id' => 3,
                'profile_id' => $profile->id,
                'profile_type' => Modules::PROFILE_TYPE_INDIVIDUAL,
                'created_at' => $date3,
                'updated_at' => $date3
            ));
        }
        DB::table('platforms_top_profiles')->insert(array(
            'platform_id' => 1,
            'profile_id' => $profile->id,
            'profile_type' => Modules::PROFILE_TYPE_INDIVIDUAL,
            'created_at' => $date3,
            'updated_at' => $date3
        ));
    }

    $user = new User();
    $user->username = 'fleava_admin';
    $user->password = Hash::make('fleava');
    $user->email = 'fleava.admin@mailinator.com';
    $user->profile_type = Modules::PROFILE_TYPE_ADMIN;
    $user->verified = true;
    $user->is_new = false;
    $user->save();
    $profile = new UserProfile();
    $profile->user_id = $user->id;
    $profile->first_name = 'fleava';
    $profile->last_name = 'admin';
    $profile->save();
  }
  
}