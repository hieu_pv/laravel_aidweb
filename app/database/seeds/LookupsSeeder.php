<?php

use Jenssegers\Date\Date;
use Libraries\Models\Lookups\Nationality;
use Libraries\Models\Lookups\MemberStatus;
use Libraries\Models\Lookups\ProfessionalStatus;
use Libraries\Models\Lookups\OfficeType;
use Libraries\Models\Lookups\OrganisationType;

class LookupsSeeder extends Seeder {

  public function run()
  {
    $dv = new \DataValues();
    $nationalities = $dv->nationalities;
    $member_statuses = $dv->member_statuses;
    $member_statuses_descriptions = $dv->member_statuses_descriptions;
    $professional_statuses = $dv->professional_statuses;
    $office_types = $dv->office_types;
    $organisation_types = $dv->organisation_types;
    $organisation_types2 = $dv->organisation_types_display_names;

    for ($i=0; $i < count($nationalities); $i++) {
      $obj = new Nationality();
      $obj->name = $nationalities[$i];
      $obj->save();
    }

    for ($i=0; $i < count($member_statuses); $i++) {
      $obj = new MemberStatus();
      $obj->name = $member_statuses[$i];
      $obj->description = $member_statuses_descriptions[$i];
      $obj->save();
    }

    for ($i=0; $i < count($professional_statuses); $i++) {
      $obj = new ProfessionalStatus();
      $obj->name = $professional_statuses[$i];
      $obj->save();
    }

    for ($i=0; $i < count($office_types); $i++) {
      $obj = new OfficeType();
      $obj->name = $office_types[$i];
      $obj->save();
    }

    for ($i=0; $i < count($organisation_types); $i++) {
      $obj = new OrganisationType();
      $obj->name = $organisation_types[$i];
      $obj->display_name = $organisation_types2[$i];
      $obj->save();
    }
  }

}