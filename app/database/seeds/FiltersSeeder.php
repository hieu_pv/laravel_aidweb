<?php

use Jenssegers\Date\Date;
use Libraries\Models\Filters\Country;
use Libraries\Models\Filters\Beneficiary;
use Libraries\Models\Filters\Crisis;
use Libraries\Models\Filters\Region;
use Libraries\Models\Filters\Theme;
use Libraries\Models\Filters\Intervention;
use Libraries\Models\Filters\Sector;

class FiltersSeeder extends Seeder {

  public function run()
  {
    $dv = new \DataValues();
    $regions = $dv->regions;
    $countries = $dv->countries;
    $crisis = $dv->crisis;
    $sectors = $dv->sectors;
    $themes = $dv->themes;
    $beneficiaries = $dv->beneficiaries;
    $interventions = $dv->interventions;

    for ($i=0; $i < count($regions); $i++) {
      $region = new Region();
      $region->name = $regions[$i];
      $region->save();
    }

    for ($i=0; $i < count($countries); $i++) {
      $country = new Country();
      $country->name = $countries[$i];
      $country->save();
    }

    for ($i=0; $i < count($crisis); $i++) {
      $crs = new Crisis();
      $crs->name = $crisis[$i];
      $crs->save();
    }

    for ($i=0; $i < count($sectors); $i++) {
      $sector = new Sector();
      $sector->name = $sectors[$i];
      $sector->save();
    }

    for ($i=0; $i < count($themes); $i++) {
      $theme = new Theme();
      $theme->name = $themes[$i];
      $theme->save();
    }

    for ($i=0; $i < count($interventions); $i++) {
      $intervention = new Intervention();
      $intervention->name = $interventions[$i];
      $intervention->save();
    }

    for ($i=0; $i < count($beneficiaries); $i++) {
      $beneficiary = new Beneficiary();
      $beneficiary->name = $beneficiaries[$i];
      $beneficiary->save();
    }
  }

}