<?php

use Libraries\Models\News\NewsItem;
use Jenssegers\Date\Date;
use Libraries\Models\Filters\Country;
use Libraries\Models\Filters\Beneficiary;
use Libraries\Models\Filters\Crisis;
use Libraries\Models\Filters\Region;
use Libraries\Models\Filters\Theme;
use Libraries\Models\Filters\Sector;
use Libraries\Facades\GenericUtility;
use Libraries\Models\Organisation;

class NewsSeeder extends Seeder {

  public function run()
  {    
    $dv = new \DataValues();
    $regions = $dv->regions;
    $countries = $dv->countries;
    $crisis = $dv->crisis;
    $sectors = $dv->sectors;
    $themes = $dv->themes;
    $beneficiaries = $dv->beneficiaries;
    $interventions = $dv->interventions;

    $dummyContent = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Iam id ipsum absurdum, maximum malum neglegi. Haec et tu ita posuisti, et verba vestra sunt. </p><p><i>Cur iustitia laudatur?</i> Transfer idem ad modestiam vel temperantiam, quae est moderatio cupiditatum rationi oboediens. Duo Reges: constructio interrete. <code>Idem iste, inquam, de voluptate quid sentit?</code> Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis. <code>Primum quid tu dicis breve?</code> </p><p>An vero, inquit, quisquam potest probare, quod perceptfum, quod. <b>Cur haec eadem Democritus?</b> <a href="http://loripsum.net/" target="_blank">Quid iudicant sensus?</a> <code>At multis malis affectus.</code> Non enim iam stirpis bonum quaeret, sed animalis. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Non autem hoc: igitur ne illud quidem. Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur. Istam voluptatem, inquit, Epicurus ignorat? </p><ol> <li>Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.</li> <li>Verum hoc idem saepe faciamus.</li> <li>Tu autem negas fortem esse quemquam posse, qui dolorem malum putet.</li> <li>Omnes enim iucundum motum, quo sensus hilaretur.</li> </ol><pre> At vero facere omnia, ut adipiscamur, quae secundum naturam sint, etiam si ea non assequamur, id esse et honestum et solum per se expetendum et solum bonum Stoici dicunt. Negat esse eam, inquit, propter se expetendam. </pre><dl> <dt><dfn>Memini vero, inquam;</dfn></dt> <dd>Quae cum dixisset paulumque institisset, Quid est?</dd> <dt><dfn>Etiam beatissimum?</dfn></dt> <dd>Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali.</dd> </dl><blockquote cite="http://loripsum.net"> Aderamus nos quidem adolescentes, sed multi amplissimi viri, quorum nemo censuit plus Fadiae dandum, quam posset ad eam lege Voconia pervenire. </blockquote><ul> <li>Esse enim quam vellet iniquus iustus poterat inpune.</li> <li>Sed finge non solum callidum eum, qui aliquid improbe faciat, verum etiam praepotentem, ut M.</li> <li>Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.</li> <li>Quae diligentissime contra Aristonem dicuntur a Chryippo.</li> <li>Quae duo sunt, unum facit.</li> </ul><p>Ratio quidem vestra sic cogit. <mark>Suo enim quisque studio maxime ducitur.</mark> Suo enim quisque studio maxime ducitur. Quippe: habes enim a rhetoribus; Cui Tubuli nomen odio non est? <code>Que Manilium, ab iisque M.</code> Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Non est igitur voluptas bonum. </p><p><a href="http://loripsum.net/" target="_blank">Quis hoc dicit?</a> Color egregius, integra valitudo, summa gratia, vita denique conferta voluptatum omnium varietate. <a href="http://loripsum.net/" target="_blank">Non est igitur summum malum dolor.</a> Id est enim, de quo quaerimus. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? <code>Itaque contra est, ac dicitis;</code> Quae diligentissime contra Aristonem dicuntur a Chryippo. Nonne igitur tibi videntur, inquit, mala? Quare ad ea primum, si videtur; Ipse Epicurus fortasse redderet, ut Sextus Peducaeus.</p>';

    for ($i = 0; $i < 100; $i++) {
      $model = new NewsItem();
      $model->name = GenericUtility::GUID();
      $model->title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->slug = Str::slug($model->title);
      $model->content = $dummyContent;

      $d = rand(1391588787, 1423124787);
      $d2 = rand(1391588787, 1423124787);
      $model->created_at = Date::createFromTimeStamp($d);
      $model->updated_at = Date::createFromTimeStamp($d2);

      $org = Organisation::find(rand(1, 50));
      $model->user_id = $org->cp_id;
      $model->organisation_id = $org->id;

      $model->image = '/images/dummy/'.($i + 1).'.jpg';
      $model->featured = true;

      $model->save();

      $r = rand(1,count($regions));
      $model->regions()->attach($r);

      $c = rand(1,count($countries));
      $model->countries()->attach($c);

      $cr = rand(1,count($crisis));
      $model->crisis()->attach($cr);

      $s = rand(1,count($sectors));
      $model->sectors()->attach($s);

      $t = rand(1,count($themes));
      $model->themes()->attach($t);

      $in = rand(1,count($interventions));
      $model->interventions()->attach($in);

      $b = rand(1,count($beneficiaries));
      $model->beneficiaries()->attach($b);
    }

    for ($i = 0; $i < 23; $i++) {
      $model = new NewsItem();
      $model->name = GenericUtility::GUID();
      $model->title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->slug = Str::slug($model->title);
      $model->content = $dummyContent;

      $d = rand(1391588787, 1423124787);
      $d2 = rand(1391588787, 1423124787);
      $model->created_at = Date::createFromTimeStamp($d);
      $model->updated_at = Date::createFromTimeStamp($d2);

      $org = Organisation::find(rand(1, 50));
      $model->user_id = 7;
      $model->organisation_id = 7;

      $model->image = '/images/dummy/'.($i + 1).'.jpg';
      $model->featured = true;

      $model->save();

      $r = rand(1,count($regions));
      $model->regions()->attach($r);

      $c = rand(1,count($countries));
      $model->countries()->attach($c);

      $cr = rand(1,count($crisis));
      $model->crisis()->attach($cr);

      $s = rand(1,count($sectors));
      $model->sectors()->attach($s);

      $t = rand(1,count($themes));
      $model->themes()->attach($t);

      $in = rand(1,count($interventions));
      $model->interventions()->attach($in);

      $b = rand(1,count($beneficiaries));
      $model->beneficiaries()->attach($b);
    }
  }

}