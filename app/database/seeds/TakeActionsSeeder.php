<?php

use Libraries\Models\TakeActions\TakeAction;
use Libraries\Models\TakeActions\Type;
use Jenssegers\Date\Date;
use Libraries\Facades\GenericUtility;
use Libraries\Models\Organisation;

class TakeActionsSeeder extends Seeder {

  public function run()
  {    
    $dv = new \DataValues();
    $types = $dv->typesOfTakeActions;
    $colors = $dv->typesOfTakeActionsColors;
    $regions = $dv->regions;
    $countries = $dv->countries;
    $crisis = $dv->crisis;
    $sectors = $dv->sectors;
    $themes = $dv->themes;
    $beneficiaries = $dv->beneficiaries;
    $interventions = $dv->interventions;

    $dummyContent = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ille enim occurrentia nescio quae comminiscebatur; Ostendit pedes et pectus. Falli igitur possumus. Cur deinde Metrodori liberos commendas? Pugnant Stoici cum Peripateticis. </p><p>Duo Reges: constructio interrete. Praeclare hoc quidem. A mene tu? Erat enim Polemonis. <i>Hoc est non dividere, sed frangere.</i> Falli igitur possumus. Philosophi autem in suis lectulis plerumque moriuntur. </p>';

    for ($i = 0; $i < count($types); $i++) {
      $type = new Type();
      $type->name = $types[$i];
      $type->color = $colors[$i];
      $type->save();
    }

    for ($i = 0; $i < 50; $i++) {
      $model = new TakeAction();
      $model->name = GenericUtility::GUID();
      $model->title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->slug = Str::slug($model->title);
      $model->content = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->url = 'http://www.google.com';

      $d = rand(1391588787, 1423124787);
      $d2 = rand(1391588787, 1423124787);
      $model->created_at = Date::createFromTimeStamp($d);
      $model->updated_at = Date::createFromTimeStamp($d2);

      $org = Organisation::find(rand(1, 50));
      $model->user_id = $org->cp_id;
      $model->organisation_id = $org->id;

      $ttt = rand(1, count($types));
      $model->type_id = $ttt;

      $model->image = '/images/dummy/'.($i + 1).'.jpg';
      $model->featured = true;
      
      $model->save();

      $r = rand(1,count($regions));
      $model->regions()->attach($r);

      $c = rand(1,count($countries));
      $model->countries()->attach($c);

      $cr = rand(1,count($crisis));
      $model->crisis()->attach($cr);

      $s = rand(1,count($sectors));
      $model->sectors()->attach($s);

      $t = rand(1,count($themes));
      $model->themes()->attach($t);

      $in = rand(1,count($interventions));
      $model->interventions()->attach($in);

      $b = rand(1,count($beneficiaries));
      $model->beneficiaries()->attach($b);
    }

    for ($i = 0; $i < 24; $i++) {
      $model = new TakeAction();
      $model->name = GenericUtility::GUID();
      $model->title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->slug = Str::slug($model->title);
      $model->content = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->url = 'http://www.google.com';

      $d = rand(1391588787, 1423124787);
      $d2 = rand(1391588787, 1423124787);
      $model->created_at = Date::createFromTimeStamp($d);
      $model->updated_at = Date::createFromTimeStamp($d2);

      $org = Organisation::find(rand(1, 50));
      $model->user_id = 7;
      $model->organisation_id = 7;

      $ttt = rand(1, count($types));
      $model->type_id = $ttt;

      $model->image = '/images/dummy/'.($i + 1).'.jpg';
      $model->featured = true;
      
      $model->save();

      $r = rand(1,count($regions));
      $model->regions()->attach($r);

      $c = rand(1,count($countries));
      $model->countries()->attach($c);

      $cr = rand(1,count($crisis));
      $model->crisis()->attach($cr);

      $s = rand(1,count($sectors));
      $model->sectors()->attach($s);

      $t = rand(1,count($themes));
      $model->themes()->attach($t);

      $in = rand(1,count($interventions));
      $model->interventions()->attach($in);

      $b = rand(1,count($beneficiaries));
      $model->beneficiaries()->attach($b);
    }
  }

}