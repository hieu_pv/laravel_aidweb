<?php

use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

class PlatformSeeder extends Seeder {

  public function run()
  {
    $date = Date::now()->format('Y-m-d H:i');
    DB::table('platforms')->insert([
      'name' => 'International',
      'display_text' => 'International',
      'related_country_id' => 0,
      'created_at' => $date,
      'updated_at' => $date
    ]);

    DB::table('platforms')->insert([
      'name' => 'Indonesia',
      'display_text' => 'Indonesia',
      'related_country_id' => 102,
      'created_at' => $date,
      'updated_at' => $date
    ]);

    DB::table('platforms')->insert([
      'name' => 'Canada',
      'display_text' => 'Canada',
      'related_country_id' => 38,
      'created_at' => $date,
      'updated_at' => $date
    ]);
  }

}