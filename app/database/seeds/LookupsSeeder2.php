<?php

use Jenssegers\Date\Date;
use Libraries\Models\Lookups\LevelOfResponsibility;
use Libraries\Models\Lookups\LevelOfActivity;

class LookupsSeeder2 extends Seeder {

  public function run()
  {
    $dv = new \DataValues();

    $loa = $dv->levelofactivities;
    $lor = $dv->levelofresponsibilities;

    for ($i=0; $i < count($lor); $i++) {
      $obj = new LevelOfResponsibility();
      $obj->name = $lor[$i];
      $obj->save();
    }

    for ($i=0; $i < count($loa); $i++) {
      $obj = new LevelOfActivity();
      $obj->name = $loa[$i];
      $obj->save();
    }
  }

}