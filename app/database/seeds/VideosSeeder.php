<?php

use Libraries\Models\Videos\Video;
use Jenssegers\Date\Date;
use Libraries\Models\Organisation;

class VideosSeeder extends Seeder {

  public function run()
  {
    $dv = new \DataValues();
    $regions = $dv->regions;
    $countries = $dv->countries;
    $crisis = $dv->crisis;
    $sectors = $dv->sectors;
    $themes = $dv->themes;
    $beneficiaries = $dv->beneficiaries;
    $interventions = $dv->interventions;

    $videos = array(
      'https://www.youtube.com/watch?v=9bZkp7q19f0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=1',
      'https://www.youtube.com/watch?v=kffacxfA7G4&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=2',
      'https://www.youtube.com/watch?v=0KSOMA3QBU0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=3',
      'https://www.youtube.com/watch?v=CevxZvSJLk8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=4',
      'https://www.youtube.com/watch?v=KQ6zr6kCPj8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=5',
      'https://www.youtube.com/watch?v=uelHwf8o7_U&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=6',
      'https://www.youtube.com/watch?v=pRpeEdMmmQ0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=7',
      'https://www.youtube.com/watch?v=t4H_Zoh7G5A&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=8',
      'https://www.youtube.com/watch?v=ASO_zypdnsQ&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=9',
      'https://www.youtube.com/watch?v=_OBlgSz8sSM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=10',
      'https://www.youtube.com/watch?v=NUsoVlDFqZg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=11',
      'https://www.youtube.com/watch?v=My2FRPA3Gf8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=12',
      'https://www.youtube.com/watch?v=hT_nvWreIhg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=13',
      'https://www.youtube.com/watch?v=7PCkvCPvDXk&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=14',
      'https://www.youtube.com/watch?v=e-ORhEE9VVg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=15',
      'https://www.youtube.com/watch?v=nfWlot6h_JM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=16',
      'https://www.youtube.com/watch?v=QK8mJJJvaes&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=17',
      'https://www.youtube.com/watch?v=fWNaR-rxAic&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=18',
      'https://www.youtube.com/watch?v=7zp1TbLFPp8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=19',
      'https://www.youtube.com/watch?v=fLexgOxsZu0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=20',
      'https://www.youtube.com/watch?v=QFs3PIZb3js&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=21',
      'https://www.youtube.com/watch?v=2vjPBrBU-TM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=22',
      'https://www.youtube.com/watch?v=rYEDA3JcQqw&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=23',
      'https://www.youtube.com/watch?v=hcm55lU9knw&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=24',
      'https://www.youtube.com/watch?v=QJO3ROT-A4E&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=25',
      'https://www.youtube.com/watch?v=8UVNT4wvIGY&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=26',
      'https://www.youtube.com/watch?v=qrO4YZeyl0I&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=27',
      'https://www.youtube.com/watch?v=y6Sxv-sUYtM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=28',
      'https://www.youtube.com/watch?v=j5-yKhDd64s&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=29',
      'https://www.youtube.com/watch?v=IcrbM1l_BoI&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=30',
      'https://www.youtube.com/watch?v=RBumgq5yVrA&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=31',
      'https://www.youtube.com/watch?v=SmM0653YvXU&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=32',
      'https://www.youtube.com/watch?v=hHUbLv4ThOo&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=33',
      'https://www.youtube.com/watch?v=wcLNteez3c4&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=34',
      'https://www.youtube.com/watch?v=QGJuMBdaqIw&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=35',
      'https://www.youtube.com/watch?v=lWA2pjMjpBs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=36',
      'https://www.youtube.com/watch?v=dhsy6epaJGs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=37',
      'https://www.youtube.com/watch?v=astISOttCQ0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=38',
      'https://www.youtube.com/watch?v=LjhCEhWiKXk&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=39',
      'https://www.youtube.com/watch?v=LrUvu1mlWco&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=40',
      'https://www.youtube.com/watch?v=gCYcHz2k5x0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=41',
      'https://www.youtube.com/watch?v=hiP14ED28CA&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=42',
      'https://www.youtube.com/watch?v=KYniUCGPGLs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=43',
      'https://www.youtube.com/watch?v=bdOXnTbyk0g&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=44',
      'https://www.youtube.com/watch?v=O-zpOMYRi0w&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=45',
      'https://www.youtube.com/watch?v=o3mP3mJDL2k&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=46',
      'https://www.youtube.com/watch?v=jofNR_WkoCE&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=47',
      'https://www.youtube.com/watch?v=3O1_3zBUKM8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=48',
      'https://www.youtube.com/watch?v=7-7knsP2n5w&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=49',
      'https://www.youtube.com/watch?v=CGyEd0aKWZE&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=50',
      'https://www.youtube.com/watch?v=4JipHEz53sU&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=51',
      'https://www.youtube.com/watch?v=HP-MbfHFUqs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=52',
      'https://www.youtube.com/watch?v=iS1g8G_njx8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=53',
      'https://www.youtube.com/watch?v=Ys7-6_t7OEQ&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=54',
      'https://www.youtube.com/watch?v=_Z5-P9v3F8w&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=55',
      'https://www.youtube.com/watch?v=yCjJyiqpAuU&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=56',
      'https://www.youtube.com/watch?v=PIh2xe4jnpk&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=57',
      'https://www.youtube.com/watch?v=hLQl3WQQoQ0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=58',
      'https://www.youtube.com/watch?v=KlyXNRrsk4A&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=59',
      'https://www.youtube.com/watch?v=kYtGl1dX5qI&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=60',
      'https://www.youtube.com/watch?v=tg00YEETFzg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=61',
      'https://www.youtube.com/watch?v=nlcIKh6sBtc&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=62',
      'https://www.youtube.com/watch?v=M11SvDtPBhA&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=63',
      'https://www.youtube.com/watch?v=450p7goxZqg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=64',
      'https://www.youtube.com/watch?v=moSFlvxnbgk&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=65',
      'https://www.youtube.com/watch?v=EPo5wWmKEaI&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=66',
      'https://www.youtube.com/watch?v=OPf0YbXqDm0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=67',
      'https://www.youtube.com/watch?v=SR6iYWJxHqs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=68',
      'https://www.youtube.com/watch?v=U0CGsw6h60k&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=69',
      'https://www.youtube.com/watch?v=ebXbLfLACGM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=70',
      'https://www.youtube.com/watch?v=LDZX4ooRsWs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=71',
      'https://www.youtube.com/watch?v=CdXesX6mYUE&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=72',
      'https://www.youtube.com/watch?v=VuNIsY6JdUw&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=73',
      'https://www.youtube.com/watch?v=iibZa2tbGEg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=74',
      'https://www.youtube.com/watch?v=7OHMqdMpsj8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=75',
      'https://www.youtube.com/watch?v=bbEoRnaOIbs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=76',
      'https://www.youtube.com/watch?v=qMxX-QOV9tI&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=77',
      'https://www.youtube.com/watch?v=CHVhwcOg6y8&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=78',
      'https://www.youtube.com/watch?v=4GuqB1BQVr4&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=79',
      'https://www.youtube.com/watch?v=pa14VNsdSYM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=80',
      'https://www.youtube.com/watch?v=JRfuAukYTKg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=81',
      'https://www.youtube.com/watch?v=L0MK7qz13bU&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=82',
      'https://www.youtube.com/watch?v=JF8BRvqGCNs&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=83',
      'https://www.youtube.com/watch?v=Y1xs_xPb46M&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=84',
      'https://www.youtube.com/watch?v=yyDUC1LUXSU&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=85',
      'https://www.youtube.com/watch?v=OpQFFLBMEPI&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=86',
      'https://www.youtube.com/watch?v=1G4isv_Fylg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=87',
      'https://www.youtube.com/watch?v=4m1EFMoRFvY&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=88',
      'https://www.youtube.com/watch?v=BGa3AqeqRy0&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=89',
      'https://www.youtube.com/watch?v=AbPED9bisSc&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=90',
      'https://www.youtube.com/watch?v=lp-EO5I60KA&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=91',
      'https://www.youtube.com/watch?v=TGtWWb9emYI&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=92',
      'https://www.youtube.com/watch?v=fwK7ggA3-bU&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=93',
      'https://www.youtube.com/watch?v=8SbUC-UaAxE&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=94',
      'https://www.youtube.com/watch?v=6BTjG-dhf5s&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=95',
      'https://www.youtube.com/watch?v=k0BWlvnBmIE&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=96',
      'https://www.youtube.com/watch?v=kHue-HaXXzg&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=97',
      'https://www.youtube.com/watch?v=W-TE_Ys4iwM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=98',
      'https://www.youtube.com/watch?v=L8eRzOYhLuw&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=99',
      'https://www.youtube.com/watch?v=t5Sd5c4o9UM&list=PLirAqAtl_h2r5g8xGajEwdXd3x1sZh8hC&index=100'
    );

    // $videos = array(
    //   'https://www.youtube.com/watch?v=09R8_2nJtjg&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=1',
    //   'https://www.youtube.com/watch?v=kt0g4dWxEBo&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=2',
    //   'https://www.youtube.com/watch?v=AJtDXIazrMo&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=3',
    //   'https://www.youtube.com/watch?v=OPf0YbXqDm0&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=4',
    //   'https://www.youtube.com/watch?v=aPxVSCfoYnU&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=5',
    //   'https://www.youtube.com/watch?v=KWZGAExj-es&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=6',
    //   'https://www.youtube.com/watch?v=e-ORhEE9VVg&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=7',
    //   'https://www.youtube.com/watch?v=M6t47RI4bns&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=8',
    //   'https://www.youtube.com/watch?v=okGcksYM0N8&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=9',
    //   'https://www.youtube.com/watch?v=k4YRWT_Aldo&list=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&index=10',
    // );

    for ($i = 0; $i < 30; $i++) {
      $model = new Video();
      $model->title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->slug = Str::slug($model->title);
      $model->author = 'author';

      $imod = $i % 100;
      $model->url = $videos[$imod];

      $model->setVideoDetailsBasedOnUrl();

      $d = rand(1391588787, 1423124787);
      $d2 = rand(1391588787, 1423124787);
      $model->created_at = Date::createFromTimeStamp($d);
      $model->updated_at = Date::createFromTimeStamp($d2);

      $org = Organisation::find(rand(1, 50));
      $model->user_id = $org->cp_id;
      $model->organisation_id = $org->id;

      $model->featured = true;
      $model->description = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];

      $model->save();

      $r = rand(1,count($regions));
      $model->regions()->attach($r);

      $c = rand(1,count($countries));
      $model->countries()->attach($c);

      $cr = rand(1,count($crisis));
      $model->crisis()->attach($cr);

      $s = rand(1,count($sectors));
      $model->sectors()->attach($s);

      $t = rand(1,count($themes));
      $model->themes()->attach($t);

      $in = rand(1,count($interventions));
      $model->interventions()->attach($in);

      $b = rand(1,count($beneficiaries));
      $model->beneficiaries()->attach($b);
    }

    for ($i = 31; $i < 55; $i++) {
      $model = new Video();
      $model->title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
      $model->slug = Str::slug($model->title);
      $model->author = 'author';

      $imod = $i % 100;
      $model->url = $videos[$imod];

      $model->setVideoDetailsBasedOnUrl();

      $d = rand(1391588787, 1423124787);
      $d2 = rand(1391588787, 1423124787);
      $model->created_at = Date::createFromTimeStamp($d);
      $model->updated_at = Date::createFromTimeStamp($d2);

      $org = Organisation::find(rand(1, 50));
      $model->user_id = 7;
      $model->organisation_id = 7;

      $model->featured = true;
      $model->description = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];

      $model->save();

      $r = rand(1,count($regions));
      $model->regions()->attach($r);

      $c = rand(1,count($countries));
      $model->countries()->attach($c);

      $cr = rand(1,count($crisis));
      $model->crisis()->attach($cr);

      $s = rand(1,count($sectors));
      $model->sectors()->attach($s);

      $t = rand(1,count($themes));
      $model->themes()->attach($t);

      $in = rand(1,count($interventions));
      $model->interventions()->attach($in);

      $b = rand(1,count($beneficiaries));
      $model->beneficiaries()->attach($b);
    }
  }

}