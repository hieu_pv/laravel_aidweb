<?php

use Libraries\Models\Settings;

class SettingsSeeder extends Seeder {

  public function run()
  {
    $settings = new Settings();
    $settings->key = 'urls_blog_blogpost_prefix';
    $settings->value = 'blogs';
    $settings->default_value = 'blogs';
    $settings->save();

    $settings = new Settings();
    $settings->key = 'urls_news_newsitem_prefix';
    $settings->value = 'news';
    $settings->default_value = 'news';
    $settings->save();

    $settings = new Settings();
    $settings->key = 'urls_videos_video_prefix';
    $settings->value = 'videos';
    $settings->default_value = 'videos';
    $settings->save();

    $settings = new Settings();
    $settings->key = 'urls_takeactions_takeaction_prefix';
    $settings->value = 'get-involved';
    $settings->default_value = 'take-actions';
    $settings->save();
  }
  
}