<?php

class DatabaseSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Eloquent::unguard();

    $this->call('FiltersSeeder');
    $this->call('SettingsSeeder');
    $this->call('LookupsSeeder');
    $this->call('LookupsSeeder2');
    $this->call('UserSeeder');
    $this->call('PostSeeder');
    $this->call('NewsSeeder');
    $this->call('TakeActionsSeeder');
    $this->call('VideosSeeder');
    $this->call('PlatformSeeder');
    $this->call('PublicitiesSeeder');
  }

}
