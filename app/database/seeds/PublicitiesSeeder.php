<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Libraries\Models\Organisation;
use Libraries\Models\Publicities\Publicity;
use Libraries\Models\Publicities\Type;
use Libraries\Models\User;
use Libraries\Models\UserProfile;
use Libraries\Facades\GenericUtility;
use Libraries\Support\Modules;

class PublicitiesSeeder extends Seeder {

  public function run()
  {
    $dv = new \DataValues();

    $types = $dv->typesOfPublicities;

    for ($i = 1; $i <= count($types); $i++) {
      $type = new Type();
      $type->name = $types[$i-1];
      $type->save();
    }

    // orgs
    for ($i=0; $i < 50; $i++) { 
      $pub = new Publicity();
      $type = ($i % 3) + 1;

      $pub->name = 'Publicity '.$i;

      if ($type === 1) {
        $a_title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
        $pub->a_title = $i.'. '.$a_title;
        $pub->a_title_color = '#ffffff';
        $pub->a_text = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
        $pub->a_text_color = '#ffffff';
        $pub->a_button_text = "See Offer";
        $pub->a_button_text_color = '#ffffff';
        $pub->a_position = 'LeftTop';
        $pub->a_image = '/images/dummy/'.($i + 1).'.jpg';
        $pub->a_url = 'http://www.nationalgeographic.com';
        $pub->a_show_org_logo = true;
      }

      if ($type === 2) {
        $b_title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
        $pub->b_title = $i.'. '.$b_title;
        $pub->b_image = '/images/dummy/'.($i + 1).'.jpg';
        $pub->b_url = 'http://news.google.com';
      }

      if ($type === 3) {
        $c_title = $dv->mockTitles[rand(0, count($dv->mockTitles) - 1)];
        $pub->c_title = $i.'. '.$c_title;
        $pub->c_image = '/images/dummy/'.($i + 1).'.jpg';
        $pub->c_url = 'http://www.bbc.com';
      }

      $pub->type_id = $type;
      $pub->user_id = 7;
      $pub->organisation_id = 7;
      //$pub->published_start_date = '2015-06-01 00:00:00';
      $pub->published_start_date = '01-06-2015';
      $pub->published_interval = 'P0Y7M0DT0H0M0S';
      $pub->published_interval_in_seconds = (7 * 30 * 24 * 3600);
      $pub->save();

      DB::table('publicities_platforms')->insert([
        'publicity_id' => $pub->id,
        'platform_id' => 0
      ]);

      // if ($i % 5 === 0) {
      //   DB::table('publicities_platforms')->insert([
      //     'publicity_id' => $pub->id,
      //     'platform_id' => 1
      //   ]);
      // }

      // if ($i % 2 === 0) {
      //   DB::table('publicities_platforms')->insert([
      //     'publicity_id' => $pub->id,
      //     'platform_id' => 2
      //   ]);
      // }

      // if ($i % 3 === 0) {
      //   DB::table('publicities_platforms')->insert([
      //     'publicity_id' => $pub->id,
      //     'platform_id' => 3
      //   ]);
      // }
    }
  }
  
}