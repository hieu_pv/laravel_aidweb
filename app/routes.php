<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('', array('as' => 'cms_home', 'uses' => 'CmsHomeController@index'));
Route::get('/', array('as' => 'cms_home1', 'uses' => 'CmsHomeController@index'));
Route::get('home', array('as' => 'cms_home2', 'uses' => 'CmsHomeController@index'));

Route::get('about', array('as' => 'about', 'uses' => 'CmsHomeController@about'));
Route::get('user-agreement', array('as' => 'user_agreement', 'uses' => 'CmsHomeController@userAgreement'));
Route::get('privacy-policy', array('as' => 'privacy_policy', 'uses' => 'CmsHomeController@privacyPolicy'));
//Route::get('contact', array('as' => 'contact', 'uses' => 'CmsHomeController@contact'));
Route::get('feedback', array('as' => 'feedback', 'uses' => 'CmsHomeController@contact'));
Route::post('postfeedback', array('as' => 'postfeedback', 'uses' => 'CmsMethodsController@postContact'));
Route::post('postfeedback2', array('as' => 'postfeedback2', 'uses' => 'CmsMethodsController@postContact2'));

// Route::get('phpinfo', function () {
//   phpinfo();
// });

//$newsPrefix = SettingsProvider::get('urls_news_newsitem_prefix');
Route::get('news', array('as' => 'cms_news', 'uses' => 'CmsNewsController@index'));
Route::get('news/preview', array('as' => 'cms_news_preview', 'uses' => 'CmsNewsController@preview'));


//$blogPrefix = SettingsProvider::get('urls_blog_blogpost_prefix');
Route::get('blogs', array('as' => 'cms_blogs', 'uses' => 'CmsBlogsController@index'));
Route::get('blogs/preview', array('as' => 'cms_blogs_preview', 'uses' => 'CmsBlogsController@preview'));


Route::get('videos', array('as' => 'cms_videos', 'uses' => 'CmsVideosController@index'));
Route::get('videos/preview', array('as' => 'cms_videos_preview', 'uses' => 'CmsVideosController@preview'));
Route::post('videos/viewed', array('as' => 'cms_videos_viewed', 'uses' => 'CmsVideosController@viewed'));


Route::get('get-involved', array('as' => 'cms_takeactions', 'uses' => 'CmsTakeActionsController@index'));
Route::get('get-involved/preview', array('as' => 'cms_takeactions_preview', 'uses' => 'CmsTakeActionsController@preview'));


Route::get('publicity/preview', array('as' => 'cms_publicity_preview', 'uses' => 'CmsPublicityController@preview'));


Route::get('profile/ind/{id}', array('as' => 'cms_profile_ind', 'uses' => 'CmsProfileController@getIndividualProfile'));
Route::get('profile/org/{id}', array('as' => 'cms_profile_org', 'uses' => 'CmsProfileController@getOrganisationProfile'));


Route::post('changeplatform', array('uses' => 'CmsMethodsController@changePlatform'));
Route::post('sandbox', array('uses' => 'SandboxController@index'));


Route::get('organisations', array('as' => 'cms_organisations', 'uses' => 'CmsOrganisationsController@index'));
Route::get('individuals', array('as' => 'cms_individuals', 'uses' => 'CmsIndividualsController@index'));


/*--------------------------------------------------------------------------
 * APIs
 */
Route::group(array('prefix' => 'api'), function()
{
  Route::post('news/_getdata', array('as' => 'cms_news_getdata', 'uses' => 'CmsNewsController@getData'));
  Route::post('news/_getnewsitemsbyorganisationid', array('as' => 'cms_news_getnewsitemsbyorganisationid', 'uses' => 'CmsNewsController@getNewsByOrganisationId'));

  Route::post('blogs/_getdata', array('as' => 'cms_blogs_getdata', 'uses' => 'CmsBlogsController@getData'));
  Route::post('blogs/_getpostsbyuserprofileid', array('as' => 'cms_blogs_getpostsbyuserprofileid', 'uses' => 'CmsBlogsController@getPostsByUserProfileId'));

  Route::post('videos/_getdata', array('as' => 'cms_videos_getdata', 'uses' => 'CmsVideosController@getData'));
  Route::post('videos/_getvideosbyorganisationid', array('as' => 'cms_blogs_getvideosbyorganisationid', 'uses' => 'CmsVideosController@getVideosByOrganisationId'));

  Route::post('takeactions/_getdata', array('as' => 'cms_takeactions_getdata', 'uses' => 'CmsTakeActionsController@getData'));
  Route::post('takeactions/_gettakeactionsbyorganisationid', array('as' => 'cms_blogs_gettakeactionsbyorganisationid', 'uses' => 'CmsTakeActionsController@getByOrganisationId'));

  Route::get('_filters/{moduleId}', array('as' => 'cms_filters', 'uses' => 'CmsFilterController@getFilters'));
  Route::post('_like', array('before' => 'auth', 'as' => 'cms_like_posting', 'uses' => 'CmsMethodsController@likePosting'));
  Route::post('_givecomment', array('before' => 'auth', 'as' => 'cms_comment_posting', 'uses' => 'CmsMethodsController@giveComment'));
  Route::post('_getcomments', array('as' => 'cms_get_posting_comments', 'uses' => 'CmsMethodsController@getComments'));
  Route::post('_getnewestcomments', array('as' => 'cms_get_posting_newest_comments', 'uses' => 'CmsMethodsController@getNewestComments'));

  Route::post('organisations/_getdata', array('as' => 'cms_organisations_getdata', 'uses' => 'CmsOrganisationsController@getData'));
  Route::get('organisations/_getfilters', array('as' => 'cms_organisations_getfilters', 'uses' => 'CmsOrganisationsController@getFilters'));

  Route::post('individuals/_getdata', array('as' => 'cms_individuals_getdata', 'uses' => 'CmsIndividualsController@getData'));
  Route::get('individuals/_getfilters', array('as' => 'cms_individuals_getfilters', 'uses' => 'CmsIndividualsController@getFilters'));
});


/*--------------------------------------------------------------------------
 * PERMALINKS
 */
Route::get(PermalinkEngine::prefix_blog().'/{permalink}', 'CmsBlogsController@getByPermalink');
Route::get(PermalinkEngine::prefix_news().'/{permalink}', 'CmsNewsController@getByPermalink');
Route::get(PermalinkEngine::prefix_takeactions().'/{permalink}', 'BlogPostTestController@getByPermalink3');
Route::get(PermalinkEngine::prefix_videos().'/{permalink}', 'BlogPostTestController@getByPermalink4');


/*--------------------------------------------------------------------------
 * Special Routes
 */
Route::post('resetpassword', array('as' => 'account_resetpassword', 'uses' => 'AdminRegistrationController@resetPassword'));
Route::post('resendreminder', array('as' => 'account_resendreminder', 'uses' => 'AdminRegistrationController@resendPasswordReminder'));
Route::get('dopasswordreset', array('as' => 'account_do_resetpassword', 'uses' => 'AdminRegistrationController@doResetPassword'));
Route::post('dopasswordreset2', array('as' => 'account_do_resetpassword2', 'uses' => 'AdminRegistrationController@doResetPassword2'));
// register
Route::get('register/{mode}', array('as' => 'account_register', 'uses' => 'AdminRegistrationController@register'));
Route::post('postregister', array('before' => 'csrf', 'as' => 'account_doregister', 'uses' => 'AdminRegistrationController@postRegister'));
Route::get('verify', array('as' => 'account_verify', 'uses' => 'AdminRegistrationController@verify'));
Route::get('resendverification', array('as' => 'account_resend_verification', 'uses' => 'AdminRegistrationController@resendVerification'));
Route::post('resendverification2', array('as' => 'account_resend_verification2', 'uses' => 'AdminRegistrationController@resendVerification2'));
Route::get('registered', array('as' => 'account_registered', 'uses' => 'AdminRegistrationController@registered'));
Route::get('pingEmailRegistration', array('as' => 'pingEmailRegistration', 'uses' => 'AdminRegistrationController@pingEmailRegistration'));


/*--------------------------------------------------------------------------
 * Admin Section
 */
Route::group(array('prefix' => 'oadmin'), function()
{
  Route::get('impersonate', array('uses' => 'AdminImpersonateController@index'));
  Route::post('doimpersonate', array('uses' => 'AdminImpersonateController@doImpersonate'));

  Route::get('last-posts', array('uses' => 'AdminMethodsController@getLastPosts'));

  // authentication
  Route::get('login', array('as' => 'admin_login', 'uses' => 'AdminRegistrationController@login'));
  Route::post('postlogin', array('as' => 'admin_dologin', 'uses' => 'AdminRegistrationController@postLogin'));


  // NOTE: move outside oadmin
  // // registration
  // Route::get('register/{mode}', array('as' => 'admin_register', 'uses' => 'AdminRegistrationController@register'));
  //Route::post('postregister', array('before' => 'csrf', 'as' => 'admin_doregister', 'uses' => 'AdminRegistrationController@postRegister'));


  // accounts
  Route::group(array('prefix' => 'account'), function()
  {
    // logout, need auth filter
    Route::get('logout', array('as' => 'admin_logout', 'uses' => 'AdminAccountController@logout'));

    // need auth
    Route::post('changepassword', array('as' => 'admin_account_changepassword', 'uses' => 'AdminAccountController@changePassword'));
  });


  // profile
  Route::get('profile', array('as' => 'admin_profile', 'uses' => 'AdminUserController@profile'));
  Route::post('postprofile', array('before' => 'csrf', 'as' => 'admin_changeprofile', 'uses' => 'AdminUserController@postProfile'));
  Route::get('profile/ind/{profileId}/readonly', array('as' => 'admin_readonly_ind_profile', 'uses' => 'AdminUserController@readonlyIndProfile'));
  Route::get('profile/org/{organisationId}/readonly', array('as' => 'admin_readonly_org_profile', 'uses' => 'AdminUserController@readonlyOrgProfile'));


  // Dashboard
  Route::get('/', array('as' => 'admin_dashboard', 'uses' => 'AdminDashboardController@index'));
  Route::get('dashboard', array('as' => 'admin_dashboard2', 'uses' => 'AdminDashboardController@index'));


  // blog manager (posts)
  Route::group(array('prefix' => 'posts'), function()
  {
    Route::get('/', array('as' => 'admin_all_posts', 'uses' => 'AdminBlogPostController@index'));
    Route::get('{id}/edit', array('as' => 'admin_edit_post', 'uses' => 'AdminBlogPostController@edit'));
    Route::post('update', array('before' => 'csrf', 'as' => 'admin_update_post', 'uses' => 'AdminBlogPostController@update'));
    Route::get('create', array('as' => 'admin_create_post', 'uses' => 'AdminBlogPostController@create'));
    Route::post('insert', array('before' => 'csrf', 'as' => 'admin_insert_post', 'uses' => 'AdminBlogPostController@insert'));
    Route::post('{id}/destroy', array('as' => 'admin_destroy_post', 'uses' => 'AdminBlogPostController@destroy'));
    Route::post('{id}/publish', array('as' => 'admin_publish_post', 'uses' => 'AdminBlogPostController@publish'));
    Route::post('{id}/unpublish', array('as' => 'admin_unpublish_post', 'uses' => 'AdminBlogPostController@unpublish'));
    Route::get('getdata', array('as' => 'admin_get_posts', 'uses' => 'AdminBlogPostController@getData'));
    Route::get('authors', array('as' => 'admin_get_posts_authors', 'uses' => 'AdminBlogPostController@getAuthors'));
    Route::get('{id}/preview', array('as' => 'admin_preview_post', 'uses' => 'AdminBlogPostController@preview'));
    Route::post('{id}/fp-publish', array('as' => 'admin_publish_post_fp', 'uses' => 'AdminBlogPostController@fpPublish'));
    Route::post('{id}/fp-unpublish', array('as' => 'admin_unpublish_post_fp', 'uses' => 'AdminBlogPostController@fpUnpublish'));
    Route::post('getpreviewurl', array('as' => 'admin_post_get_preview', 'uses' => 'AdminBlogPostController@getPreviewUrl'));
    Route::get('{id}/readonly', array('as' => 'admin_readonly_post', 'uses' => 'AdminBlogPostController@readonly'));
  });


  // news manager
  Route::group(array('prefix' => 'news'), function()
  {
    Route::get('/', array('as' => 'admin_all_newsitems', 'uses' => 'AdminNewsItemController@index'));
    Route::get('{id}/edit', array('as' => 'admin_edit_newsitem', 'uses' => 'AdminNewsItemController@edit'));
    Route::post('update', array('before' => 'csrf', 'as' => 'admin_update_newsitem', 'uses' => 'AdminNewsItemController@update'));
    Route::get('create', array('as' => 'admin_create_newsitem', 'uses' => 'AdminNewsItemController@create'));
    Route::post('insert', array('as' => 'admin_insert_newsitem', 'uses' => 'AdminNewsItemController@insert'));
    Route::post('{id}/destroy', array('as' => 'admin_destroy_newsitem', 'uses' => 'AdminNewsItemController@destroy'));
    Route::post('{id}/publish', array('as' => 'admin_publish_newsitem', 'uses' => 'AdminNewsItemController@publish'));
    Route::post('{id}/unpublish', array('as' => 'admin_unpublish_newsitem', 'uses' => 'AdminNewsItemController@unpublish'));
    Route::get('getdata', array('as' => 'admin_get_newsitems', 'uses' => 'AdminNewsItemController@getData'));
    Route::get('authors', array('as' => 'admin_get_newsitems_authors', 'uses' => 'AdminNewsItemController@getAuthors'));
    Route::get('{id}/preview', array('as' => 'admin_preview_newsitem', 'uses' => 'AdminNewsItemController@preview'));
    Route::post('{id}/fp-publish', array('as' => 'admin_publish_newsitem_fp', 'uses' => 'AdminNewsItemController@fpPublish'));
    Route::post('{id}/fp-unpublish', array('as' => 'admin_unpublish_newsitem_fp', 'uses' => 'AdminNewsItemController@fpUnpublish'));
    Route::post('getpreviewurl', array('as' => 'admin_newsitem_get_preview', 'uses' => 'AdminNewsItemController@getPreviewUrl'));
    Route::get('{id}/readonly', array('as' => 'admin_readonly_newsitem', 'uses' => 'AdminNewsItemController@readonly'));
  });


  // take actions
  Route::group(array('prefix' => 'takeactions'), function()
  {
    Route::get('/', array('as' => 'admin_all_takeactions', 'uses' => 'AdminTakeActionController@index'));
    Route::get('create', array('as' => 'admin_create_takeaction', 'uses' => 'AdminTakeActionController@create'));
    Route::get('getdata', array('as' => 'admin_get_takeactions', 'uses' => 'AdminTakeActionController@getData'));
    Route::post('{id}/destroy', array('as' => 'admin_destroy_takeaction', 'uses' => 'AdminTakeActionController@destroy'));
    Route::post('{id}/publish', array('as' => 'admin_publish_takeaction', 'uses' => 'AdminTakeActionController@publish'));
    Route::post('{id}/unpublish', array('as' => 'admin_unpublish_takeaction', 'uses' => 'AdminTakeActionController@unpublish'));
    Route::get('authors', array('as' => 'admin_get_takeactions_authors', 'uses' => 'AdminTakeActionController@getAuthors'));
    Route::get('types', array('as' => 'admin_get_takeactions_types', 'uses' => 'AdminTakeActionController@getTakeActionTypes'));
    Route::get('create', array('as' => 'admin_create_takeaction', 'uses' => 'AdminTakeActionController@create'));
    Route::post('insert', array('before' => 'csrf', 'as' => 'admin_insert_takeaction', 'uses' => 'AdminTakeActionController@insert'));
    Route::get('{id}/edit', array('as' => 'admin_edit_takeaction', 'uses' => 'AdminTakeActionController@edit'));
    Route::post('update', array('before' => 'csrf', 'as' => 'admin_update_takeaction', 'uses' => 'AdminTakeActionController@update'));
    Route::get('{id}/preview', array('as' => 'admin_preview_takeaction', 'uses' => 'AdminTakeActionController@preview'));
    Route::post('{id}/fp-publish', array('as' => 'admin_publish_takeaction_fp', 'uses' => 'AdminTakeActionController@fpPublish'));
    Route::post('{id}/fp-unpublish', array('as' => 'admin_unpublish_takeaction_fp', 'uses' => 'AdminTakeActionController@fpUnpublish'));
    Route::post('getpreviewurl', array('as' => 'admin_takeaction_get_preview', 'uses' => 'AdminTakeActionController@getPreviewUrl'));
    Route::get('{id}/readonly', array('as' => 'admin_readonly_takeaction', 'uses' => 'AdminTakeActionController@readonly'));
  });


  // videos
  Route::group(array('prefix' => 'videos'), function()
  {
    Route::get('/', array('as' => 'admin_all_videos', 'uses' => 'AdminVideoController@index'));
    Route::get('create', array('as' => 'admin_create_video', 'uses' => 'AdminVideoController@create'));
    Route::get('getdata', array('as' => 'admin_get_videos', 'uses' => 'AdminVideoController@getData'));
    Route::post('{id}/destroy', array('as' => 'admin_destroy_video', 'uses' => 'AdminVideoController@destroy'));
    Route::post('{id}/publish', array('as' => 'admin_publish_video', 'uses' => 'AdminVideoController@publish'));
    Route::post('{id}/unpublish', array('as' => 'admin_unpublish_video', 'uses' => 'AdminVideoController@unpublish'));
    Route::get('authors', array('as' => 'admin_get_videos_authors', 'uses' => 'AdminVideoController@getAuthors'));
    Route::get('types', array('as' => 'admin_get_videos_types', 'uses' => 'AdminVideoController@getVideoTypes'));
    Route::get('create', array('as' => 'admin_create_video', 'uses' => 'AdminVideoController@create'));
    Route::post('insert', array('before' => 'csrf', 'as' => 'admin_insert_video', 'uses' => 'AdminVideoController@insert'));
    Route::get('{id}/edit', array('as' => 'admin_edit_video', 'uses' => 'AdminVideoController@edit'));
    Route::post('update', array('before' => 'csrf', 'as' => 'admin_update_video', 'uses' => 'AdminVideoController@update'));
    Route::get('{id}/preview', array('as' => 'admin_preview_video', 'uses' => 'AdminVideoController@preview'));
    Route::post('{id}/fp-publish', array('as' => 'admin_publish_video_fp', 'uses' => 'AdminVideoController@fpPublish'));
    Route::post('{id}/fp-unpublish', array('as' => 'admin_unpublish_video_fp', 'uses' => 'AdminVideoController@fpUnpublish'));
    Route::post('getpreviewurl', array('as' => 'admin_video_get_preview', 'uses' => 'AdminVideoController@getPreviewUrl'));
    Route::get('{id}/readonly', array('as' => 'admin_readonly_video', 'uses' => 'AdminVideoController@readonly'));
  });


  // publicities
  Route::group(array('prefix' => 'publicities'), function()
  {
    Route::get('/', array('as' => 'admin_all_publicities', 'uses' => 'AdminPublicityController@index'));
    Route::get('getdata', array('as' => 'admin_get_publicities', 'uses' => 'AdminPublicityController@getData'));
    Route::get('create', array('as' => 'admin_create_publicity', 'uses' => 'AdminPublicityController@create'));
    Route::post('insert', array('before' => 'csrf', 'as' => 'admin_insert_publicity', 'uses' => 'AdminPublicityController@insert'));
    Route::get('{id}/edit', array('as' => 'admin_edit_publicity', 'uses' => 'AdminPublicityController@edit'));
    Route::post('update', array('before' => 'csrf', 'as' => 'admin_update_publicity', 'uses' => 'AdminPublicityController@update'));
    Route::post('{id}/destroy', array('as' => 'admin_destroy_publicity', 'uses' => 'AdminPublicityController@destroy'));
    Route::post('{id}/publish', array('as' => 'admin_publish_publicity', 'uses' => 'AdminPublicityController@publish'));
    Route::post('{id}/unpublish', array('as' => 'admin_unpublish_publicity', 'uses' => 'AdminPublicityController@unpublish'));
    Route::get('authors', array('as' => 'admin_get_publicity_authors', 'uses' => 'AdminPublicityController@getAuthors'));
    Route::get('types', array('as' => 'admin_get_publicity_types', 'uses' => 'AdminPublicityController@getPublicityTypes'));
    Route::get('{id}/preview', array('as' => 'admin_preview_publicity', 'uses' => 'AdminPublicityController@preview'));
    Route::post('{id}/fp-publish', array('as' => 'admin_publish_publicity_fp', 'uses' => 'AdminPublicityController@fpPublish'));
    Route::post('{id}/fp-unpublish', array('as' => 'admin_unpublish_publicity_fp', 'uses' => 'AdminPublicityController@fpUnpublish'));
    Route::post('getpreviewurl', array('as' => 'admin_publicity_get_preview', 'uses' => 'AdminPublicityController@getPreviewUrl'));
    Route::get('{id}/readonly', array('as' => 'admin_readonly_publicity', 'uses' => 'AdminPublicityController@readonly'));
  });


  // media manager
  Route::group(array('prefix' => 'gallery'), function()
  {
    Route::get('/', array('as' => 'admin_media_gallery', 'uses' => 'AdminMediaGalleryController@index'));
    Route::get('getfiles', 'AdminMediaGalleryController@getFiles');
    Route::post('createfolder', 'AdminMediaGalleryController@createFolder');
    Route::post('quickrename', 'AdminMediaGalleryController@quickRename');
    Route::post('destroy', 'AdminMediaGalleryController@destroy');
    Route::post('uploads', 'AdminMediaGalleryController@uploads');
  });


  // organisations manager
  Route::group(array('prefix' => 'organisations'), function()
  {
    Route::get('/', array('as' => 'admin_all_organisations', 'uses' => 'AdminOrganisationController@index'));
    Route::get('getdata', array('as' => 'admin_get_organisations', 'uses' => 'AdminOrganisationController@getData'));
    Route::post('revoke', array('as' => 'admin_revoke_organisations', 'uses' => 'AdminOrganisationController@revokeMembership'));
    Route::post('activate', array('as' => 'admin_activate_organisations', 'uses' => 'AdminOrganisationController@activateMembership'));
    // Route::post('settop', array('as' => 'admin_settopagency_organisations', 'uses' => 'AdminOrganisationController@setAsTopAgency'));
    // Route::post('unsettop', array('as' => 'admin_unsettopagency_organisations', 'uses' => 'AdminOrganisationController@unsetFromTopAgency'));
    Route::get('types', array('as' => 'admin_organisation_types', 'uses' => 'AdminOrganisationController@getOrganisationTypes'));

  });


  // individuals manager
  Route::group(array('prefix' => 'individuals'), function()
  {
    Route::get('/', array('as' => 'admin_all_individuals', 'uses' => 'AdminIndividualController@index'));
    Route::get('getdata', array('as' => 'admin_get_individuals', 'uses' => 'AdminIndividualController@getData'));
    Route::post('revoke', array('as' => 'admin_revoke_individuals', 'uses' => 'AdminIndividualController@revokeMembership'));
    Route::post('activate', array('as' => 'admin_activate_individuals', 'uses' => 'AdminIndividualController@activateMembership'));
    // Route::post('settop', array('as' => 'admin_settopmember_individuals', 'uses' => 'AdminIndividualController@setAsTopMember'));
    // Route::post('unsettop', array('as' => 'admin_unsettopmember_individuals', 'uses' => 'AdminIndividualController@unsetFromTopMember'));

  });


  // settings manager
  Route::group(array('prefix' => 'settings'), function()
  {
    Route::get('urls', array('as' => 'admin_settings_urls', 'uses' => 'AdminSettingsController@urls'));
    Route::post('store', array('as' => 'admin_save_settings', 'uses' => 'AdminSettingsController@store'));
  });


  // filters manager
  Route::group(array('prefix' => 'filters'), function()
  {
    Route::get('', array('as' => 'admin_manage_filters', 'uses' => 'AdminManageFiltersController@index'));
    Route::get('getall', array('uses' => 'AdminManageFiltersController@getAllFilters'));
    Route::post('delete', array('uses' => 'AdminManageFiltersController@deleteFilter'));
    Route::post('update', array('uses' => 'AdminManageFiltersController@updateFilter'));
    Route::post('create', array('uses' => 'AdminManageFiltersController@createFilter'));
  });


  // platform manager
  Route::group(array('prefix' => 'platforms'), function()
  {
    Route::get('', array('as' => 'admin_manage_platforms', 'uses' => 'AdminManagePlatformsController@index'));
    Route::post('create', array('uses' => 'AdminManagePlatformsController@create'));
    Route::get('countries', array('uses' => 'AdminManagePlatformsController@countries'));
    Route::get('getall', array('uses' => 'AdminManagePlatformsController@getAll'));
    
    Route::post('activate/{id}', array('uses' => 'AdminManagePlatformsController@activate'));
    Route::post('deactivate/{id}', array('uses' => 'AdminManagePlatformsController@deactivate'));
    
    Route::get('getorgplatformshavingtopstatus&orgid={orgId}', array('uses' => 'AdminManagePlatformsController@getOrgPlatformsHavingTopStatus'));
    Route::post('markorgastopatplatform', array('uses' => 'AdminManagePlatformsController@markOrgAsTopAtPlatform'));
    Route::post('unmarkorgfromtopatplatform', array('uses' => 'AdminManagePlatformsController@unmarkOrgFromTopAtPlatform'));
    Route::get('getorgassignedplatforms&orgid={orgId}', array('uses' => 'AdminManagePlatformsController@getOrgAssignedPlatforms'));

    Route::get('getindplatformshavingtopstatus&indid={indId}', array('uses' => 'AdminManagePlatformsController@getIndPlatformsHavingTopStatus'));
    Route::post('markindastopatplatform', array('uses' => 'AdminManagePlatformsController@markIndAsTopAtPlatform'));
    Route::post('unmarkindfromtopatplatform', array('uses' => 'AdminManagePlatformsController@unmarkIndFromTopAtPlatform'));
    Route::get('getindassignedplatforms&indid={indId}', array('uses' => 'AdminManagePlatformsController@getIndAssignedPlatforms'));
  });


  // common
  Route::group(array('prefix' => 'methods'), function()
  {
    Route::get('getslug', array('as' => 'admin_methods_getslug', 'uses' => 'AdminMethodsController@getSlug'));
    //Route::post('ping', 'AdminMethodsController@ping');
    Route::get('getvideodetails', array('as' => 'admin_methods_getvideodetails', 'uses' => 'AdminMethodsController@getVideoDetails'));
    Route::get('getcountries', array('as' => 'admin_methods_getcountries', 'uses' => 'AdminMethodsController@getCountries'));
    Route::get('getnationalities', array('as' => 'admin_methods_getnationalities', 'uses' => 'AdminMethodsController@getNationalities'));
    Route::get('getmemberstatuses', array('as' => 'admin_methods_getmemberstatuses', 'uses' => 'AdminMethodsController@getMemberStatuses'));
    Route::get('getprofessionalstatuses', array('as' => 'admin_methods_getprofessionalstatuses', 'uses' => 'AdminMethodsController@getProfessionalStatuses'));
  });


});

Route::filter('posting_limitation_in_pilot_phase', function()
{
  $service = App::make('\Libraries\Admin\Services\PostingService');
  if (Config::get('app.pilotphase')) {
    $origin = strtolower(Input::get('q'));
    $service->checkForPostingLimitationInPilotPhase($origin);
  } else {
    $service->removePostingLimitationInPilotPhase();
  }
});

Route::filter('posting_limitation', function()
{
  $service = App::make('\Libraries\Admin\Services\PostingService');
  if (Config::get('app.posting_limitation')) {
    $service->checkPostingLimitation();
  } else {
    $service->removePostingLimitation();
  }
});

Route::filter('disable_new_post', function()
{
  $service = App::make('\Libraries\Admin\Services\PostingService');
  if (Config::get('app.posting_limitation')) {
    $service->disable_new_post();
  }
});
