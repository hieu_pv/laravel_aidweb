<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

  app_path().'/commands',
  app_path().'/controllers',
  app_path().'/models',
  app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
  Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
  return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';



// http://stackoverflow.com/questions/17972276/laravel-how-to-respond-with-custom-404-error-depending-on-route
App::missing(function($exception)
{
  if (Request::is('oadmin/*')) {
    return Response::view('admin.404', array(), 404);
  // } else if (Request::is('site/*')) {
  //  return Response::view('site.missing',array(),404);
  } else {
    return Response::view('default.404', array(), 404);
  }
});



Form::macro('rawLabel', function($name, $value = null, $options = array())
{
  $label = Form::label($name, '%s', $options);

  return sprintf($label, $value);
});



Event::listen('posting.view', '\Libraries\Cms\EventHandlers\GenericViewPostingEventHandler');
Event::listen('posting.like', '\Libraries\Cms\EventHandlers\GenericLikePostingEventHandler');
Event::listen('posting.comment', '\Libraries\Cms\EventHandlers\GenericCommentPostingEventHandler');



/**
 * image intervention library
 * move these methods to other classes
 */

function intervention_360_211($image)
{
  $image->fit(360, 211, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'top')->encode('jpg', 75);
  return $image;
}

function intervention_370_216($image)
{
  $image->fit(370, 216, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'top')->encode('jpg', 75);
  return $image;
}

function intervention_1333_600($image)
{
  $image->fit(1366, 600, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'top')->encode('jpg', 75);
  return $image;
}

function intervention_370_278_c($image)
{
  $image->fit(370, 278, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_380_222_c($image) 
{
  $image->fit(380, 222, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_680_160_c($image) 
{
  $image->fit(680, 160, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_760_400($image) 
{
   $image->fit(760, 400, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_1366_auto_c($image) 
{
  $image->resize(1366, null, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_760_auto_c($image)
{
  $image->resize(760, null, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_100_auto_c($image)
{
  $image->resize(100, null, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_211_auto_c($image)
{
  $image->resize(211, null, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_57_c($image)
{
  $image->resize(57, 57, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_255_c($image)
{
  $image->resize(255, 255, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

function intervention_r_370_auto_c($image)
{
  $image->resize(370, null, function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
  }, 'center')->encode('jpg', 75);
  return $image;
}

/* end image intervention library */


//\Debugbar::disable();