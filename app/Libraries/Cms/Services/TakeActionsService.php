<?php namespace Libraries\Cms\Services;

use Libraries\Repositories\TakeActions\ITakeActionRepository;
use Libraries\Repositories\TakeActions\ITakeActionTypeRepository;
use Libraries\Models\TakeActions\TakeAction;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TakeActionsService {

  private $_takeActionRepository;
  private $_takeActionTypeRepository;

  public function __construct(ITakeActionRepository $takeActionRepository,
                              ITakeActionTypeRepository $takeActionTypeRepository)
  {
    $this->_takeActionRepository = $takeActionRepository;
    $this->_takeActionTypeRepository = $takeActionTypeRepository;
  }
  
  public function getTopOnePerType(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        return $this->_takeActionRepository->getTopOnePerType();
      } else {
        return $this->_takeActionRepository->getTopOnePerTypeInCountry($platform['related_country_id']);
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopByTypes(array $platform) 
  {
    try {
      $data = new \stdClass();
      $data->types = array();
      $types = $this->_takeActionTypeRepository->getAll();
      foreach ($types as $type) {
        $topRecords = new \Illuminate\Database\Eloquent\Collection();
        if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
          $topRecords = $this->_takeActionRepository->getTopInTypes($type->id);
        } else {
          $topRecords = $this->_takeActionRepository->getTopInTypesByCountry($type->id, $platform['related_country_id']);
        }
        $type->top_records = $topRecords;
        array_push($data->types, $type);
      }
      return $data;
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getData($pageNumber, $pageSize, $filters, $sortBy, $searchQuery = '', array $platform)
  {
    try {
      $result = new \stdClass();

      if ($platform['id'] !== Modules::PLATFORM_INTERNATIONAL) {
        //$filters->officelocation = intval($platform['related_country_id']);
        $filters->platform_id = intval($platform['id']);
        $filters->platform_country_id = intval($platform['related_country_id']);
      }
      
      $data = $this->_takeActionRepository->filterAll($pageNumber, $pageSize, $filters, $sortBy, $searchQuery);
      $count1 = $this->_takeActionRepository->countFiltered($filters, $searchQuery);
      $count2 = $this->_takeActionRepository->countOrganisationsFromFiltered($filters, $searchQuery);

      $result->raw_data = $data;
      $result->data = array();
      $result->record_count = $count1;
      $result->page_count = ceil($result->record_count / $pageSize);
      $result->org_count = $count2;

      $filterOptionsRecordCount = $this->_takeActionRepository->getFilterOptionsRecordCount($filters, $searchQuery);
      $result->filterOptionsRecordCount = $filterOptionsRecordCount;

      return $result;
    } catch (\Exception $ex) {
      //Log::error($ex);
      $result = new \stdClass();
      $result->raw_data = array();
      $result->data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      $result->org_count = 0;
      $result->filterOptionsRecordCount = [];
      return $result;
    }
  }

  public function getByOrganisationId($organisationId, $pageNumber)
  {
    try {
      $result = new \stdClass();
      $result->raw_data = $this->_takeActionRepository->getByOrganisationId($organisationId, $pageNumber, 10);
      $result->record_count = $this->_takeActionRepository->countByOrganisationId($organisationId);
      $result->page_count = ceil($result->record_count / 10);
      return $result;
    } catch (\Exception $ex) {
      $result = new \stdClass();
      $result->raw_data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      return $result;
    }
  }

}