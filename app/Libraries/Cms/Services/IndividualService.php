<?php namespace Libraries\Cms\Services;

use Libraries\Facades\Directories;
use Libraries\Support\Modules;
use Libraries\Models\UserProfile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class IndividualService {

  private $_userProfileRepository;

  public function __construct()
  {
    $this->_userProfileRepository = App::make('\Libraries\Repositories\IUserProfileRepository');
  }

  public function getData($pageNumber, $pageSize, $filters, $sortBy, $searchQuery = '', array $platform)
  {
    try {
      $result = new \stdClass();

      if ($platform['id'] !== Modules::PLATFORM_INTERNATIONAL) {
        $filters->basedin = intval($platform['related_country_id']);
      }
      
      $data = $this->_userProfileRepository->filter($pageNumber, $pageSize, $filters, $sortBy, $searchQuery);
      $count = $this->_userProfileRepository->countFiltered($filters, $searchQuery);
      $count2 = $this->_userProfileRepository->countBasedInFromFilteredIndividuals($filters, $searchQuery);

      $result->raw_data = $data;
      $result->data = $data; 
      $result->record_count = $count;
      $result->page_count = ceil($result->record_count / $pageSize);
      $result->based_in_count = $count2;

      $filtersCounts = $this->_userProfileRepository->getFiltersCounts($filters, $searchQuery);
      $result->filtersCounts = $filtersCounts;

      return $result;
    } catch (\Exception $ex) {
      //Log::error($ex);
      $result = new \stdClass();
      $result->raw_data = array();
      $result->data = array(); 
      $result->record_count = 0;
      $result->page_count = 0;
      $result->based_in_count = 0;
      $result->filtersCounts = 0;
      return $result;
    }
  }
}