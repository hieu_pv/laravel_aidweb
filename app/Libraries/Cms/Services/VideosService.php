<?php namespace Libraries\Cms\Services;

use Libraries\Models\Videos\Video;
use Libraries\Repositories\Videos\IVideoRepository;
use Libraries\Repositories\Filters\IRegionRepository;
use Libraries\Repositories\Filters\IThemeRepository;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VideosService {

  private $_videoRepository;
  private $_regionRepository;
  private $_themeRepository;

  public function __construct(IVideoRepository $videoRepository,
                              IRegionRepository $regionRepository,
                              IThemeRepository $themeRepository)
  {
    $this->_videoRepository = $videoRepository;
    $this->_regionRepository = $regionRepository;
    $this->_themeRepository = $themeRepository;
  }
  
  public function getFeaturedVideos(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        return $this->_videoRepository->getFeaturedVideos();
      } else {
        return $this->_videoRepository->getFeaturedVideosByCountry($platform['related_country_id']);
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getMostPopularVideos(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_videoRepository->getMostPopularVideos();
        return $data;
      } else {
        $data = $this->_videoRepository->getMostPopularVideosInCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopVideos(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_videoRepository->getTopVideos();
        return $data;
      } else {
        $data = $this->_videoRepository->getTopVideosFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopVideosByRegions() 
  {
    try {
      $data = new \stdClass();
      $data->regions = array();
      $regions = $this->_regionRepository->getAll();
      foreach ($regions as $region) {
        $topVideosInRegion = $this->_videoRepository->getTopVideosInRegion($region->id);
        $region->top_videos = $topVideosInRegion;
        array_push($data->regions, $region);
      }
      return $data;
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopVideosByThemes(array $platform) 
  {
    try {
      $data = new \stdClass();
      $data->themes = array();
      $themes = $this->_themeRepository->getNotDeleted();
      foreach ($themes as $theme) {
        $videos = $this->_videoRepository->getTopVideosInThemeFromCountry($theme->id, $platform['related_country_id']);
        $theme->top_videos = $videos;
        array_push($data->themes, $theme);
      }
      return $data;
    } catch (\Exception $ex) {
      Log::error($ex);
      return array();
    }
  }

  public function getData($pageNumber, $pageSize, $filters, $sortBy, $searchQuery = '', array $platform)
  {
    try {
      $result = new \stdClass();

      if ($platform['id'] !== Modules::PLATFORM_INTERNATIONAL) {
        //$filters->officelocation = intval($platform['related_country_id']);
        $filters->platform_id = intval($platform['id']);
        $filters->platform_country_id = intval($platform['related_country_id']);
      }
      
      $data = $this->_videoRepository->filterAllVideos($pageNumber, $pageSize, $filters, $sortBy, $searchQuery);
      $count1 = $this->_videoRepository->countFilteredVideos($filters, $searchQuery);
      $count2 = $this->_videoRepository->countOrganisationsFromFilteredVideos($filters, $searchQuery);

      $result->raw_data = $data;
      $result->data = array();
      $result->record_count = $count1;
      $result->page_count = ceil($result->record_count / $pageSize);
      $result->org_count = $count2;

      $filterOptionsRecordCount = $this->_videoRepository->getFilterOptionsRecordCount($filters, $searchQuery);
      $result->filterOptionsRecordCount = $filterOptionsRecordCount;

      return $result;
    } catch (\Exception $ex) {
      $result = new \stdClass();
      $result->raw_data = array();
      $result->data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      $result->org_count = 0;
      $result->filterOptionsRecordCount = [];
      return $result;
    }
  }

  public function getVideosByOrganisationId($organisationId, $pageNumber)
  {
    try {
      $result = new \stdClass();
      $result->raw_data = $this->_videoRepository->getVideosByOrganisationId($organisationId, $pageNumber, 10);
      $result->record_count = $this->_videoRepository->countVideosByOrganisationId($organisationId);
      $result->page_count = ceil($result->record_count / 10);
      return $result;
    } catch (\Exception $ex) {
      $result = new \stdClass();
      $result->raw_data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      return $result;
    }
  }

}