<?php namespace Libraries\Cms\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Libraries\Models\Blog\Post;
use Libraries\Repositories\Blog\IPostRepository;
use Libraries\Repositories\Filters\IRegionRepository;
use Libraries\Repositories\Filters\IThemeRepository;
use Libraries\Support\Modules;

class BlogService {

  private $_postRepository;
  private $_regionRepository;
  private $_themeRepository;

  public function __construct(IPostRepository $postRepository,
                              IRegionRepository $regionRepository,
                              IThemeRepository $themeRepository)
  {
    $this->_postRepository = $postRepository;
    $this->_regionRepository = $regionRepository;
    $this->_themeRepository = $themeRepository;
  }

  public function getByIdAndSlug($id, $slug)
  {
    try {
      $result = $this->_postRepository->getByIdAndSlug($id, $slug);
      return $result;
    } catch (\Exception $ex) {
      return null;
    }
  }

  public function getLatestPosts(array $platform)
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_postRepository->getLatestPosts();
        return $data;
      } else {
        $data = $this->_postRepository->getLatestPostsFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopPosts(array $platform)
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_postRepository->getTopPosts();
        return $data;
      } else {
        $data = $this->_postRepository->getTopPostsFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopPostsByRegions()
  {
    try {
      $data = new \stdClass();
      $data->regions = array();
      $regions = $this->_regionRepository->getAll();
      foreach ($regions as $region) {
        $topPostsInRegion = $this->_postRepository->getTopPostsInRegion($region->id);
        $region->top_posts = $topPostsInRegion;
        array_push($data->regions, $region);
      }
      return $data;
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopPostsByThemes(array $platform)
  {
    try {
      $data = new \stdClass();
      $data->themes = array();
      $themes = $this->_themeRepository->getNotDeleted();
      foreach ($themes as $theme) {
        $topPostsInTheme = $this->_postRepository->getTopPostsInThemeFromCountry($theme->id, $platform['related_country_id']);
        $theme->top_posts = $topPostsInTheme;
        array_push($data->themes, $theme);
      }
      return $data;
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getMostPopularPosts(array $platform)
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_postRepository->getMostPopularPosts();
        return $data;
      } else {
        $data = $this->_postRepository->getMostPopularPostsFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getData($pageNumber, $pageSize, $filters, $sortBy, $searchQuery = '', array $platform)
  {
    try {
      $result = new \stdClass();

      if ($platform['id'] !== Modules::PLATFORM_INTERNATIONAL) {
        //$filters->basedin = intval($platform['related_country_id']);
        $filters->platform_id = intval($platform['id']);
        $filters->platform_country_id = intval($platform['related_country_id']);
      }

      $data = $this->_postRepository->filterAllPosts($pageNumber, $pageSize, $filters, $sortBy, $searchQuery);
      $count1 = $this->_postRepository->countFilteredPosts($filters, $searchQuery);
      $count2 = $this->_postRepository->countMembersFromFilteredPosts($filters, $searchQuery);

      $result->raw_data = $data;
      $result->data = array();
      $result->record_count = $count1;
      $result->page_count = ceil($result->record_count / $pageSize);
      $result->ind_count = $count2;

      $filterOptionsRecordCount = $this->_postRepository->getFilterOptionsRecordCount($filters, $searchQuery);
      $result->filterOptionsRecordCount = $filterOptionsRecordCount;

      return $result;
    } catch (\Exception $ex) {
      //Log::error($ex);
      $result = new \stdClass();
      $result->raw_data = array();
      $result->data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      $result->ind_count = 0;
      $result->filterOptionsRecordCount = [];
      return $result;
    }
  }

  public function getPostsByUserProfileId($userProfileId, $pageNumber)
  {
    try {
      $result = new \stdClass();
      $result->raw_data = $this->_postRepository->getPostsByUserProfileId($userProfileId, $pageNumber, 10);
      $result->record_count = $this->_postRepository->countPostsByUserProfileId($userProfileId);
      $result->page_count = ceil($result->record_count / 10);
      return $result;
    } catch (\Exception $ex) {
      $result = new \stdClass();
      $result->raw_data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      return $result;
    }
  }

}
