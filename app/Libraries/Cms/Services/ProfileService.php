<?php namespace Libraries\Cms\Services;

use Illuminate\Support\Facades\DB;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Repositories\IUserProfileRepository;
use Libraries\Repositories\IUserRepository;
use Libraries\Support\Modules;
use Libraries\Models\Organisation;
use Libraries\Models\UserProfile;

class ProfileService {

  private $_userRepository;
  private $_userProfileRepository;
  private $_organisationRepository;

  public function __construct(IUserRepository $userRepository,
                              IUserProfileRepository $userProfileRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_userRepository = $userRepository;
    $this->_userProfileRepository = $userProfileRepository;
    $this->_organisationRepository = $organisationRepository;
  }

  private function getUser($userId)
  {
    $user = $this->_userRepository->getById($userId);
    if ($user !== null) {
      // should we check for verification? no for now
      // sanitize for display
      $user->sanitize();
      return $user;
    } else {
      return null;
    }
  }

  public function getIndividual($profileId)
  {
    $profile = UserProfile::find($profileId);
    if ($profile === null) {
      return null;
    }

    $userId = $profile->user_id;
    $user = $this->getUser($userId);
    if ($user === null) {
      return null;
    }

    if (!$user->isIndividual()) {
      return null;
    }

    return $user;
  }

  public function getOrganisation($organisationId)
  {
    $organisation = Organisation::find($organisationId);
    if ($organisation === null) {
      return null;
    }

    $userId = $organisation->cp_id;
    $user = $this->getUser($userId);
    if ($user === null) {
      return null;
    }

    if (!$user->isOrganisation()) {
      return null;
    }

    return $user;
  }

}
