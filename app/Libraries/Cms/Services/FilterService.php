<?php namespace Libraries\Cms\Services;

use Libraries\Repositories\Filters\IRegionRepository;
use Libraries\Repositories\Filters\ICountryRepository;
use Libraries\Repositories\Filters\ICrisisRepository;
use Libraries\Repositories\Filters\IThemeRepository;
use Libraries\Repositories\Filters\ISectorRepository;
use Libraries\Repositories\Filters\IBeneficiaryRepository;
use Libraries\Repositories\Filters\IInterventionRepository;
use Libraries\Repositories\Lookups\INationalityRepository;
use Libraries\Repositories\Lookups\IMemberStatusRepository;
use Libraries\Repositories\Lookups\IProfessionalStatusRepository;
use Libraries\Repositories\Lookups\IOfficeTypeRepository;
use Libraries\Repositories\Lookups\IOrganisationTypeRepository;
use Libraries\Repositories\Lookups\ILevelOfActivityRepository;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\App;

class FilterService {

  private $_regionRepository;
  private $_countryRepository;
  private $_crisisRepository;
  private $_themeRepository;
  private $_sectorRepository;
  private $_beneficiaryRepository;
  private $_interventionRepository;

  private $_nationalityRepository;
  private $_memberStatusRepository;
  private $_professionalStatusRepository;
  private $_officeTypeRepository;
  private $_organisationTypeRepository;
  private $_levelOfActivityRepository;

  public function __construct(IRegionRepository $regionRepository,
                              ICountryRepository $countryRepository,
                              ICrisisRepository $crisisRepository,
                              IThemeRepository $themeRepository,
                              ISectorRepository $sectorRepository,
                              IBeneficiaryRepository $beneficiaryRepository,
                              IInterventionRepository $interventionRepository,
                              INationalityRepository $nationalityRepository,
                              IMemberStatusRepository $memberStatusRepository,
                              IProfessionalStatusRepository $professionalStatusRepository,
                              IOfficeTypeRepository $officeTypeRepository,
                              IOrganisationTypeRepository $organisationTypeRepository,
                              ILevelOfActivityRepository $levelOfActivityRepository)
  {
    $this->_regionRepository = $regionRepository;
    $this->_countryRepository = $countryRepository;
    $this->_crisisRepository = $crisisRepository;
    $this->_themeRepository = $themeRepository;
    $this->_sectorRepository = $sectorRepository;
    $this->_beneficiaryRepository = $beneficiaryRepository;
    $this->_interventionRepository = $interventionRepository;

    $this->_nationalityRepository = $nationalityRepository;
    $this->_memberStatusRepository = $memberStatusRepository;
    $this->_professionalStatusRepository = $professionalStatusRepository;
    $this->_officeTypeRepository = $officeTypeRepository;
    $this->_organisationTypeRepository = $organisationTypeRepository;
    $this->_levelOfActivityRepository = $levelOfActivityRepository;
  }

  private function addAllOption($results)
  {
    // $def = new \stdClass();
    // $def->id = -1;
    // $def->name = 'All';
    // $def->display_name = 'All';
    // array_unshift($results, $def);
    return $results;
  }

  public function getCountriesForFilter($moduleId)
  {
    // get countries that has postings in it >.> @.@
    $results = $this->_countryRepository->getWhereHasPostings($moduleId);
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getNationalitiesForFilter($moduleId)
  {
    $results = $this->_nationalityRepository->getWhereHasPostings($moduleId);
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getRegionsForFilter($moduleId)
  {
    $results = $this->_regionRepository->getWhereHasPostings($moduleId);
    //$results = $this->_regionRepository->getAll()->toArray();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getSectorsForFilter($moduleId)
  {
    $results = $this->_sectorRepository->getWhereHasPostings($moduleId);
    //$results = $this->_sectorRepository->getAll()->toArray();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getCrisisForFilter($moduleId)
  {
    $results = $this->_crisisRepository->getWhereHasPostings($moduleId);
    //$results = $this->_crisisRepository->getAll()->toArray();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getThemesForFilter($moduleId)
  {
    $results = $this->_themeRepository->getWhereHasPostings($moduleId);
    //$results = $this->_themeRepository->getAll()->toArray();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getInterventionsForFilter($moduleId)
  {
    $results = $this->_interventionRepository->getWhereHasPostings($moduleId);
    //$results = $this->_interventionRepository->getAll()->toArray();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getBeneficiariesForFilter($moduleId)
  {
    $results = $this->_beneficiaryRepository->getWhereHasPostings($moduleId);
    //$results = $this->_beneficiaryRepository->getAll()->toArray();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getOrganisationTypesForFilter($moduleId)
  {
    //$results = $this->_organisationTypeRepository->getAll()->toArray();
    $results = $this->_organisationTypeRepository->getWhereHasPostings($moduleId);
    usort($results, function ($a, $b) {
      return strcmp($a['display_name'], $b['display_name']);
    });
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getTakeActionTypesForFilter($moduleId)
  {
    $takeActionTypeRepository = App::make('\Libraries\Repositories\TakeActions\ITakeActionTypeRepository');
    //$results = $takeActionTypeRepository->getAll()->toArray();
    $results = $takeActionTypeRepository->getWhereHasPostings($moduleId);
    $results = $this->addAllOption($results);
    return $results;
  }

  // public function getRecordCountInFilters($moduleId)
  // {
  //   // any repository is good because it will call method in BaseRepository
  //   return $this->_regionRepository->recordCountInFilters($moduleId);
  // }

  /*-------------------------------------------------------------------------------*/

  public function getCountriesHavingOrganisations()
  {
    //$results = $this->_countryRepository->getAll()->toArray();
    $results = $this->_countryRepository->getWhereHasOrganisations();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getOrganisationTypesHavingOrganisations()
  {
    //$results = $this->_organisationTypeRepository->getAll()->toArray();
    //$results = $this->_organisationTypeRepository->getWhereHasPostings();
    $results = $this->_organisationTypeRepository->getWhereHasOrganisations();
    usort($results, function ($a, $b) {
      return strcmp($a['display_name'], $b['display_name']);
    });
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getLevelOfActivitiesHavingOrganisations()
  {
    //$results = $this->_levelOfActivityRepository->getAll()->toArray();
    $results = $this->_levelOfActivityRepository->getWhereHasOrganisations();
    $results = $this->addAllOption($results);
    return $results;
  }

  // public function getRecordCountForOrganisationsFilters()
  // {
  //   // any repository is good because it will call method in BaseRepository
  //   return $this->_regionRepository->recordCountForOrganisationsFilters();
  // }

  /*-------------------------------------------------------------------------------*/

  // public function getCountriesHavingIndividuals()
  // {
  //   //$results = $this->_countryRepository->getAll()->toArray();
  //   $results = $this->_countryRepository->getWhereHasIndividuals();
  //   $results = $this->addAllOption($results);
  //   return $results;
  // }
  
  public function getNationalitiesHavingIndividuals()
  {
    $results = $this->_nationalityRepository->getWhereHasIndividuals();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getOrganisationTypesHavingIndividuals()
  {
    //$results = $this->_organisationTypeRepository->getAll()->toArray();
    $results = $this->_organisationTypeRepository->getWhereHasIndividuals();
    usort($results, function ($a, $b) {
      return strcmp($a['display_name'], $b['display_name']);
    });
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getMemberStatusesHavingIndividuals()
  {
    //$results = $this->_memberStatusRepository->getAll()->toArray();
    $results = $this->_memberStatusRepository->getWhereHasIndividuals();
    $results = $this->addAllOption($results);
    return $results;
  }

  public function getProfessionalStatusesHavingIndividuals()
  {
    //$results = $this->_professionalStatusRepository->getAll()->toArray();
    $results = $this->_professionalStatusRepository->getWhereHasIndividuals();
    $results = $this->addAllOption($results);
    return $results;
  }

  // public function getRecordCountForIndividualsFilters()
  // {
  //   // any repository is good because it will call method in BaseRepository
  //   return $this->_regionRepository->recordCountForIndividualsFilters();
  // }

}