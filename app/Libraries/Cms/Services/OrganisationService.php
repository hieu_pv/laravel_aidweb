<?php namespace Libraries\Cms\Services;

use Libraries\Facades\Directories;
use Libraries\Models\Organisation;
use Illuminate\Support\Facades\App;

class OrganisationService {

  private $_organisationRepository;

  public function __construct()
  {
    $this->_organisationRepository = App::make('\Libraries\Repositories\IOrganisationRepository'); //$organisationRepository;
  }

  public function getData($pageNumber, $pageSize, $filters, $sortBy, $searchQuery = '', array $platform)
  {
    try {
      $result = new \stdClass();

      // do we consider platform?
      // if ($platform['id'] !== Modules::PLATFORM_INTERNATIONAL) {
      //   $filters->officelocation = intval($platform['related_country_id']);
      // }

      $data = $this->_organisationRepository->filter($pageNumber, $pageSize, $filters, $sortBy, $searchQuery);
      $count = $this->_organisationRepository->countFiltered($filters, $searchQuery);

      $result->raw_data = $data;
      $result->data = $data; 
      $result->record_count = $count;
      $result->page_count = ceil($result->record_count / $pageSize);

      $filtersCounts = $this->_organisationRepository->getFiltersCounts($filters, $searchQuery);
      $result->filtersCounts = $filtersCounts;

      return $result;
    } catch (\Exception $ex) {
      $result = new \stdClass();
      $result->raw_data = [];
      $result->data = []; 
      $result->record_count = 0;
      $result->page_count = 0;
      $result->filtersCounts = [];
      return $result;
    }
  }

}