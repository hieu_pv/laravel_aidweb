<?php namespace Libraries\Cms\Services;

use Libraries\Repositories\News\INewsItemRepository;
use Libraries\Repositories\Filters\IRegionRepository;
use Libraries\Repositories\Filters\IThemeRepository;
use Libraries\Models\News\NewsItem;
use Libraries\Support\Modules;
use Libraries\Support\GenericUtility;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NewsService {

  private $_newsItemRepository;
  private $_regionRepository;
  private $_themeRepository;

  public function __construct(INewsItemRepository $newsItemRepository,
                              IRegionRepository $regionRepository,
                              IThemeRepository $themeRepository)
  {
    $this->_newsItemRepository = $newsItemRepository;
    $this->_regionRepository = $regionRepository;
    $this->_themeRepository = $themeRepository;
  }

  /**
   * [getByIdAndSlug description]
   * @param  [type] $id   [description]
   * @param  [type] $slug [description]
   * @return [type]       [description]
   */
  public function getByIdAndSlug($id, $slug)
  {
    try {
      $result = $this->_newsItemRepository->getByIdAndSlug($id, $slug);
      return $result;
    } catch (\Exception $ex) {
      return null;
    }
  }
  
  public function getLatestNews(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_newsItemRepository->getLatestNews();
        return $data;
      } else {
        $data = $this->_newsItemRepository->getLatestNewsFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }
  
  public function getTopNews(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_newsItemRepository->getTopNews();
        return $data;
      } else {
        $data = $this->_newsItemRepository->getTopNewsFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      //Log::error($ex);
      return array();
    }
  }

  /**
   * [getTopNewsByRegions description]
   * @return [type] [description]
   */
  public function getTopNewsByRegions() 
  {
    try {
      $data = new \stdClass();
      $data->regions = array();
      $regions = $this->_regionRepository->getAll();
      foreach ($regions as $region) {
        $topNewsInRegion = $this->_newsItemRepository->getTopNewsInRegion($region->id);
        $region->top_news = $topNewsInRegion;
        array_push($data->regions, $region);
      }
      return $data;
    } catch (\Exception $ex) {
      return array();
    }
  }

  public function getTopNewsByThemes(array $platform) 
  {
    try {
      $data = new \stdClass();
      $data->themes = array();
      $themes = $this->_themeRepository->getNotDeleted();
      foreach ($themes as $theme) {
        $newsItems = $this->_newsItemRepository->getTopNewsInThemeFromCountry($theme->id, $platform['related_country_id']);
        $theme->top_news = $newsItems;
        array_push($data->themes, $theme);
      }
      return $data;
    } catch (\Exception $ex) {
      //Log::error($ex);
      return array();
    }
  }

  public function getMostPopularNews(array $platform) 
  {
    try {
      if ($platform['id'] === Modules::PLATFORM_INTERNATIONAL) {
        $data = $this->_newsItemRepository->getMostPopularNews();
        return $data;
      } else {
        $data = $this->_newsItemRepository->getMostPopularNewsFromCountry($platform['related_country_id']);
        return $data;
      }
    } catch (\Exception $ex) {
      return array();
    }
  }

  /**
   * [getData description]
   * @param  [type] $pageNumber  [description]
   * @param  [type] $pageSize    [description]
   * @param  [type] $filters     [description]
   * @param  [type] $sortBy      [description]
   * @param  string $searchQuery [description]
   * @return [type]              [description]
   */
  public function getData($pageNumber, $pageSize, $filters, $sortBy, $searchQuery = '', array $platform)
  {
    try {
      $result = new \stdClass();

      if ($platform['id'] !== Modules::PLATFORM_INTERNATIONAL) {
        //$filters->officelocation = intval($platform['related_country_id']);
        $filters->platform_id = intval($platform['id']);
        $filters->platform_country_id = intval($platform['related_country_id']);
      }
      
      $data = $this->_newsItemRepository->filterAllNews($pageNumber, $pageSize, $filters, $sortBy, $searchQuery);
      $count1 = $this->_newsItemRepository->countFilteredNews($filters, $searchQuery);
      $count2 = $this->_newsItemRepository->countOrganisationsFromFilteredNews($filters, $searchQuery);

      $result->raw_data = $data;
      $result->data = array();
      $result->record_count = $count1;
      $result->page_count = ceil($result->record_count / $pageSize);
      $result->org_count = $count2;

      $filterOptionsRecordCount = $this->_newsItemRepository->getFilterOptionsRecordCount($filters, $searchQuery);
      $result->filterOptionsRecordCount = $filterOptionsRecordCount;

      return $result;
    } catch (\Exception $ex) {
      //Log::error($ex);
      $result = new \stdClass();
      $result->raw_data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      $result->org_count = 0;
      $result->filterOptionsRecordCount = [];
      return $result;
    }
  }

  /**
   * [getNewsByOrganisationId description]
   * @param  [type] $organisationId [description]
   * @param  [type] $pageNumber     [description]
   * @return [type]                 [description]
   */
  public function getNewsByOrganisationId($organisationId, $pageNumber)
  {
    try {
      $result = new \stdClass();
      $result->raw_data = $this->_newsItemRepository->getNewsByOrganisationId($organisationId, $pageNumber, 10);
      $result->record_count = $this->_newsItemRepository->countNewsByOrganisationId($organisationId);
      $result->page_count = ceil($result->record_count / 10);
      return $result;
    } catch (\Exception $ex) {
      $result = new \stdClass();
      $result->raw_data = array();
      $result->record_count = 0;
      $result->page_count = 0;
      return $result;
    }
  }

}