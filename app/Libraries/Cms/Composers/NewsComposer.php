<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\NewsItemForListTransformer;

class NewsComposer {
  
  public function compose($view)
  {
    $newsService = App::make('\Libraries\Cms\Services\NewsService');
    $transformer = new NewsItemForListTransformer();
    $selectedPlatform = GenericUtility::selectedPlatform();

    // Handle top news
    $tabsOfTopNews = new \stdClass();
    $tabsOfTopNews->tabs = [];

    if ($selectedPlatform->id === Modules::platform_international()) {
      $regionsForTopNews = $newsService->getTopNewsByRegions();
      if (!empty($regionsForTopNews)) {
        foreach ($regionsForTopNews->regions as $item) {
          $transformedRegionTopNews = $transformer->transformCollection($item->top_news);
          $item->top_news = $transformedRegionTopNews->toArray();

          array_push($tabsOfTopNews->tabs, $item);
        }
      }
    } else {
      $themesForTopNews = $newsService->getTopNewsByThemes((array)$selectedPlatform);
      if (!empty($themesForTopNews)) {
        foreach ($themesForTopNews->themes as $item) {
          $transformedThemeTopNews = $transformer->transformCollection($item->top_news);
          $item->top_news = $transformedThemeTopNews->toArray();

          array_push($tabsOfTopNews->tabs, $item);
        }
      }
    }
    $view->tabsOfTopNews = $tabsOfTopNews;

    // Handle latest news
    $latestNews = $newsService->getLatestNews((array)$selectedPlatform);
    $transformedLatestNews = $transformer->transformCollection($latestNews);
    $latestNews = $transformedLatestNews->toArray();
    $view->latestNews = $latestNews;
  }

}