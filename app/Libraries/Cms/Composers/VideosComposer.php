<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\VideoForListTransformer;

class VideosComposer {
  
  public function compose($view)
  {
    $videosService = App::make('\Libraries\Cms\Services\VideosService');
    $transformer = new VideoForListTransformer();
    $selectedPlatform = GenericUtility::selectedPlatform();

    $tabsOfTopVideos = new \stdClass();
    $tabsOfTopVideos->tabs = [];

    if ($selectedPlatform->id === Modules::platform_international()) {
      $regionsForTopVideos = $videosService->getTopVideosByRegions();
      if (!empty($regionsForTopVideos)) {
        foreach ($regionsForTopVideos->regions as $item) {
          $transformedRegionTopVideos = $transformer->transformCollection($item->top_videos);
          $item->top_videos = $transformedRegionTopVideos->toArray();

          array_push($tabsOfTopVideos->tabs, $item);
        }
      }
    } else {
      $themesForTopVideos = $videosService->getTopVideosByThemes((array)$selectedPlatform);
      if (!empty($themesForTopVideos)) {
        foreach ($themesForTopVideos->themes as $item) {
          $transformedThemeTopVideos = $transformer->transformCollection($item->top_videos);
          $item->top_videos = $transformedThemeTopVideos->toArray();

          array_push($tabsOfTopVideos->tabs, $item);
        }
      }
    }
    $view->tabsOfTopVideos = $tabsOfTopVideos;
  }

}