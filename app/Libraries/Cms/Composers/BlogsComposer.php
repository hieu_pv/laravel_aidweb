<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\NewsItemForListTransformer;
use Libraries\Cms\Transformers\VideoForListTransformer;
use Libraries\Cms\Transformers\BlogPostForListTransformer;

class BlogsComposer {
  
  public function compose($view)
  {
    $blogService = App::make('\Libraries\Cms\Services\BlogService');
    $transformer = new BlogPostForListTransformer();
    $selectedPlatform = GenericUtility::selectedPlatform();

    $tabsOfTopPosts = new \stdClass();
    $tabsOfTopPosts->tabs = [];

    if ($selectedPlatform->id === Modules::platform_international()) {
      $regionsForTopPosts = $blogService->getTopPostsByRegions();
      if (!empty($regionsForTopPosts)) {
        foreach ($regionsForTopPosts->regions as $item) {
          $transformedRegionTopPosts = $transformer->transformCollection($item->top_posts);
          $item->top_posts = $transformedRegionTopPosts->toArray();

          array_push($tabsOfTopPosts->tabs, $item);
        }
      }
    } else {
      $themesForTopPosts = $blogService->getTopPostsByThemes((array)$selectedPlatform);
      if (!empty($themesForTopPosts)) {
        foreach ($themesForTopPosts->themes as $item) {
          $transformedThemeTopPosts = $transformer->transformCollection($item->top_posts);
          $item->top_posts = $transformedThemeTopPosts->toArray();

          array_push($tabsOfTopPosts->tabs, $item);
        }
      }
    }
    $view->tabsOfTopPosts = $tabsOfTopPosts;

    $latestPosts = $blogService->getLatestPosts((array)$selectedPlatform);
    $transformedLatestPosts = $transformer->transformCollection($latestPosts);
    $latestPosts = $transformedLatestPosts->toArray();
    $view->latestPosts = $latestPosts;
  }

}