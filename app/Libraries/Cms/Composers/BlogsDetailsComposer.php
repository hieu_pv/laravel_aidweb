<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\BlogPostForListTransformer;
use Libraries\Cms\Transformers\NewsItemForListTransformer;

class BlogsDetailsComposer {
  
  public function compose($view)
  {
    $blogService = App::make('\Libraries\Cms\Services\BlogService');
    $transformer = new BlogPostForListTransformer();
    $selectedPlatform = GenericUtility::selectedPlatform();

    $topPosts = $blogService->getTopPosts((array)$selectedPlatform);
    $topPosts = $transformer->transformCollection($topPosts)->toArray();
    $topPosts = array_slice($topPosts, 0, 3);
    $view->topPosts = $topPosts;
    
    $mostPopularPosts = $blogService->getMostPopularPosts((array)$selectedPlatform);
    $mostPopularPosts = $transformer->transformCollection($mostPopularPosts);
    $view->mostPopularPosts = $mostPopularPosts;

    $newsService = App::make('\Libraries\Cms\Services\NewsService');
    $transformer2 = new NewsItemForListTransformer();
    // Handle most popular news - for sidebar
    $mostPopularNews = $newsService->getMostPopularNews((array)$selectedPlatform);
    $mostPopularNews = $transformer2->transformCollection($mostPopularNews);
    $view->mostPopularNews = $mostPopularNews;
  }

}