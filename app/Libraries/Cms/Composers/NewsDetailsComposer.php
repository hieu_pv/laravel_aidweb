<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\NewsItemForListTransformer;
use Libraries\Cms\Transformers\VideoForListTransformer;
use Libraries\Cms\Transformers\BlogPostForListTransformer;

class NewsDetailsComposer {
  
  public function compose($view)
  {
    $newsService = App::make('\Libraries\Cms\Services\NewsService');
    $transformer = new NewsItemForListTransformer();
    $selectedPlatform = GenericUtility::selectedPlatform();

    // Handle top news - for sidebar
    $topNews = $newsService->getTopNews((array)$selectedPlatform);
    $topNews = $transformer->transformCollection($topNews)->toArray();
    $topNews = array_slice($topNews, 0, 3);
    $view->topNews = $topNews;
    
    // Handle most popular news - for sidebar
    $mostPopularNews = $newsService->getMostPopularNews((array)$selectedPlatform);
    $mostPopularNews = $transformer->transformCollection($mostPopularNews);
    $view->mostPopularNews = $mostPopularNews;

    $blogService = App::make('\Libraries\Cms\Services\BlogService');
    $transformer2 = new BlogPostForListTransformer();
    $mostPopularPosts = $blogService->getMostPopularPosts((array)$selectedPlatform);
    $mostPopularPosts = $transformer2->transformCollection($mostPopularPosts);
    $view->mostPopularPosts = $mostPopularPosts;
  }

}