<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\NewsItemForListTransformer;
use Libraries\Cms\Transformers\VideoForListTransformer;
use Libraries\Cms\Transformers\BlogPostForListTransformer;

class HomeComposer {
  
  public function compose($view)
  {
    $selectedPlatform = GenericUtility::selectedPlatform();

    // news
    $newsService = App::make('\Libraries\Cms\Services\NewsService');
    $newsItemForListTransformer = new NewsItemForListTransformer();

    $view->topNews = $newsService->getTopNews((array)$selectedPlatform);
    
    $mostPopularNews = $newsService->getMostPopularNews((array)$selectedPlatform);
    $mostPopularNews = $newsItemForListTransformer->transformCollection($mostPopularNews);
    $view->mostPopularNews = $mostPopularNews;

    // take action / get involved
    $takeActionsService = App::make('\Libraries\Cms\Services\TakeActionsService');
    $view->topOneTakeActionPerType = $takeActionsService->getTopOnePerType((array)$selectedPlatform);

    // videos
    $videosService = App::make('\Libraries\Cms\Services\VideosService');
    $videoForListTransformer = new VideoForListTransformer();

    $view->featuredVideos = $videosService->getFeaturedVideos((array)$selectedPlatform);
    
    $mostPopularVideos = $videosService->getMostPopularVideos((array)$selectedPlatform);
    $mostPopularVideos = $videoForListTransformer->transformCollection($mostPopularVideos);
    $view->mostPopularVideos = $mostPopularVideos;

    // blog
    $blogService = App::make('\Libraries\Cms\Services\BlogService');
    $blogPostForListTransformer = new BlogPostForListTransformer();

    $mostPopularBlogPosts = $blogService->getMostPopularPosts((array)$selectedPlatform);
    $mostPopularBlogPosts = $blogPostForListTransformer->transformCollection($mostPopularBlogPosts);
    $view->mostPopularBlogPosts = $mostPopularBlogPosts;
  }

}