<?php namespace Libraries\Cms\Composers;

use Illuminate\Support\Facades\App;
use Libraries\Facades\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\TakeActionForListTransformer;

class TakeActionsComposer {
  
  public function compose($view)
  {
    $service = App::make('\Libraries\Cms\Services\TakeActionsService');
    $transformer = new TakeActionForListTransformer();
    $selectedPlatform = GenericUtility::selectedPlatform();

    $typesForTop = $service->getTopByTypes((array)$selectedPlatform);
    if (!empty($typesForTop)) {
      foreach ($typesForTop->types as $item) {
        $transformed = $transformer->transformCollection($item->top_records);
        $item->top_records = $transformed->toArray();
      }
    }
    $view->typesForTop = $typesForTop;
  }

}