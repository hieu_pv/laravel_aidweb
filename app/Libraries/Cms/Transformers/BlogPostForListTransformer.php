<?php namespace Libraries\Cms\Transformers;

use Libraries\Models\Blog\Post;
use Libraries\Models\BaseModel;

class BlogPostForListTransformer extends BaseTransformer {

  public function transform(BaseModel $item) 
  {
    if (is_a($item, '\Libraries\Models\Blog\Post')) {
      $oitem = $item;

      $t = [
        'id' => $oitem->id,
        'image' => $oitem->image,
        'view_url' => $oitem->present()->viewUrl(),
        'title' => $oitem->title,
        'time_ago' => $oitem->present()->timeAgo(),
        'user_profile_info' => $oitem->present()->userProfileInfo(),
        'view_count' => $oitem->view_count,
        'num_of_likes' => $oitem->num_of_likes,
        'teaser' => $oitem->present()->teaser(),
      ];

      return (object)$t;
    } 
    return $item;
  }
  
}