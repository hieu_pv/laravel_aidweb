<?php namespace Libraries\Cms\Transformers;

use Libraries\Models\BaseModel;

abstract class BaseTransformer {

  public function transformCollection($items)
  {
    if (is_a($items, '\Illuminate\Database\Eloquent\Collection')) {
      $a = $this;
      return $items->transform(function ($item) use ($a) {
        return $a->transform($item);
      });
    } else {
      return new \Illuminate\Database\Eloquent\Collection();
    }
    //return $items;
  }

  public abstract function transform(BaseModel $item); 

}