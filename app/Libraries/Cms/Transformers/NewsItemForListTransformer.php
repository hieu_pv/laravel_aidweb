<?php namespace Libraries\Cms\Transformers;

use Libraries\Models\News\NewsItem;
use Libraries\Models\BaseModel;

class NewsItemForListTransformer extends BaseTransformer {

  public function transform(BaseModel $item) 
  {
    if (is_a($item, '\Libraries\Models\News\NewsItem')) {
      $oitem = $item;

      $t = [
        'id' => $oitem->id,
        'image' => $oitem->image,
        'view_url' => $oitem->present()->viewUrl(),
        'title' => $oitem->title,
        'time_ago' => $oitem->present()->timeAgo(),
        'org_info' => $oitem->present()->organisationInfo(),
        'view_count' => $oitem->view_count,
        'num_of_likes' => $oitem->num_of_likes,
        'teaser' => $oitem->present()->teaser(),
        'sector_name' => $oitem->sector_name
      ];

      return (object)$t;
    } 
    return $item;
  }
  
}