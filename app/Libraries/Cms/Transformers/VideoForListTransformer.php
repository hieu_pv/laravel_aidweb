<?php namespace Libraries\Cms\Transformers;

use Libraries\Models\Videos\Video;
use Libraries\Models\BaseModel;

class VideoForListTransformer extends BaseTransformer {

  public function transform(BaseModel $item) 
  {
    if (is_a($item, '\Libraries\Models\Videos\Video')) {
      $oitem = $item;

      $t = [
        'id' => $oitem->id,
        'image' => $oitem->image,
        'watch_url' => $oitem->watchUrl(),
        'embed_url' => $oitem->embedUrl(),
        'title' => $oitem->title,
        'time_ago' => $oitem->present()->timeAgo(),
        'org_info' => $oitem->present()->organisationInfo(),
        'view_count' => $oitem->view_count,
        'num_of_likes' => $oitem->num_of_likes,
        'description' => $oitem->description,
        'teaser' => $oitem->present()->teaser,
        'duration' => $oitem->displayed_duration,
        'sector_name' => $oitem->sector_name
      ];

      return (object)$t;
    } 
    return $item;
  }
  
}