<?php namespace Libraries\Cms\Transformers;

use Libraries\Models\TakeActions\TakeAction;
use Libraries\Models\BaseModel;

class TakeActionForListTransformer extends BaseTransformer {

  public function transform(BaseModel $item) 
  {
    if (is_a($item, '\Libraries\Models\TakeActions\TakeAction')) {
      $oitem = $item;

      $t = [
        'id' => $oitem->id,
        'image' => $oitem->image,
        'title' => $oitem->title,
        'content' => $oitem->content,
        'teaser' => $oitem->present()->teaser,
        'view_url' => $oitem->url,
        'type_id' => $oitem->typeOfTakeAction->id,
        'type_name' => $oitem->typeOfTakeAction->name,
        //'view_url' => $oitem->present()->viewUrl(),
        //'time_ago' => $oitem->present()->timeAgo(),
        'org_info' => $oitem->present()->organisationInfo(),
        //'view_count' => $oitem->view_count,
        //'num_of_likes' => $oitem->num_of_likes,
        //'teaser' => $oitem->present()->teaser(),
        'sector_name' => $oitem->sector_name
      ];

      return (object)$t;
    } 
    return $item;
  }
  
}