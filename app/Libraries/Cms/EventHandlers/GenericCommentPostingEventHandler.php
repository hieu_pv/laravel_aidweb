<?php namespace Libraries\Cms\EventHandlers;

use Libraries\Models\Blog\Post;
use Libraries\Models\News\NewsItem;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Session\Store;
use Carbon\Carbon;

class GenericCommentPostingEventHandler {

  //private $session;

  public function __construct() //(Store $session)
  {
    // Let Laravel inject the session Store instance,
    // and assign it to our $session variable.
    //$this->session = $session;
  }

  public function handle(\stdClass $args) 
  {
    $module = $args->module;
    $id = $args->id;
    $content = $args->content;

    if ($content === null || strlen($content) === 0) {
      return;
    }

    $uid = Auth::user()->id;

    $dt = Carbon::now()->format("Y-m-d H:i:s");

    // $table->increments('id');
    // $table->integer('module_id');
    // $table->integer('item_id');
    // $table->integer('commented_by');
    // $table->text('content');
    // $table->timestamps();
    DB::table('comments')->insert(
      array(
        'module_id' => $module, 
        'commented_by' => $uid,
        'item_id' => $id,
        'content' => $content,
        'created_at' => $dt,
        'updated_at' => $dt
      )
    );

    // TODO
    // possible update "comments_count" field in the future
    if ($module === Modules::NEWS) {
      $n = NewsItem::find($id);
      if ($n !== null) {
        $n->comments_count += 1;
        $n->save();
      }
    } else if ($module === Modules::BLOG) {
      $b = Post::find($id);
      if ($b !== null) {
        $b->comments_count += 1;
        $b->save();
      }
    } else if ($module === Modules::TAKE_ACTIONS) {
    } else if ($module === Modules::VIDEOS) {
    }
  }

}