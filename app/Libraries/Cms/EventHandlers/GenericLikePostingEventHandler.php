<?php namespace Libraries\Cms\EventHandlers;

use Libraries\Models\Blog\Post;
use Libraries\Models\News\NewsItem;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Session\Store;
use Carbon\Carbon;

class GenericLikePostingEventHandler {

  //private $session;

  public function __construct() //(Store $session)
  {
    // Let Laravel inject the session Store instance,
    // and assign it to our $session variable.
    //$this->session = $session;
  }

  public function handle(\stdClass $args) 
  {
    $module = $args->module;
    $id = $args->id;
    $uid = Auth::user()->id;

    if ($this->alreadyLiked($module, $id)) {
      return;
    }

    $dt = Carbon::now()->format("Y-m-d H:i:s");

    DB::table('likes')->insert(
      array(
        'module_id' => $module, 
        'liked_by' => $uid,
        'item_id' => $id,
        'created_at' => $dt,
        'updated_at' => $dt
      )
    );

    // if ($module === Modules::NEWS) {
    // } else if ($module === Modules::BLOG) {
    // } else if ($module === Modules::TAKE_ACTIONS) {
    // } else if ($module === Modules::VIDEOS) {
    // }
  }

  public function alreadyLiked($module, $id)
  {
    if (Auth::check()) {
      $uid = Auth::user()->id;

      $existing = DB::table('likes')->where('module_id', $module)
                                    ->where('liked_by', $uid)
                                    ->where('item_id', $id)
                                    ->first();

      if ($existing !== null) {
        // exist, do nothing
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

}