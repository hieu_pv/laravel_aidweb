<?php namespace Libraries\Cms\EventHandlers;

use Libraries\Models\Blog\Post;
use Libraries\Models\News\NewsItem;
use Libraries\Models\TakeActions\TekeAction;
use Libraries\Models\Videos\Video;
use Illuminate\Session\Store;
use Libraries\Support\Modules;

class GenericViewPostingEventHandler {

  private $session;

  public function __construct(Store $session)
  {
    // Let Laravel inject the session Store instance,
    // and assign it to our $session variable.
    $this->session = $session;
  }

  public function handle(\stdClass $args) 
  {
    $module = $args->module;
    $id = $args->id;

    // check if the posting is already viewed, for now use session based
    // possible enhancement to store viewed status in DB
    if (!$this->isViewed($module, $id)) {
      if ($module === Modules::NEWS) {
        $n = NewsItem::find($id);
        if ($n !== null) {
          $n->view_count += 1;
          $n->save();
        }
      } else if ($module === Modules::BLOG) {
        $b = Post::find($id);
        if ($b !== null) {
          $b->view_count += 1;
          $b->save();
        }
      } else if ($module === Modules::TAKE_ACTIONS) {

      } else if ($module === Modules::VIDEOS) {
        $v = Video::find($id);
        if ($v !== null) {
          $v->view_count += 1;
          $v->save();
        }
      }

      // posting viewed, store in the "viewed" storage
      $this->storeViewed($module, $id);
    }
  }

  private function isViewed($module, $id)
  {
    $viewed = array();

    if ($module === Modules::NEWS) {
      $viewed = $this->session->get('viewed_news', []);
    } else if ($module === Modules::BLOG) {
      $viewed = $this->session->get('viewed_blog_posts', []);
    } else if ($module === Modules::TAKE_ACTIONS) {
      $viewed = $this->session->get('viewed_take_actions', []);
    } else if ($module === Modules::VIDEOS) {
      $viewed = $this->session->get('viewed_videos', []);
    }
    
    return in_array($id, $viewed);
  }

  private function storeViewed($module, $id)
  {
    if ($module === Modules::NEWS) {
      $this->session->push('viewed_news', $id);
    } else if ($module === Modules::BLOG) {
      $this->session->push('viewed_blog_posts', $id);
    } else if ($module === Modules::TAKE_ACTIONS) {
      $this->session->push('viewed_take_actions', $id);
    } else if ($module === Modules::VIDEOS) {
      $this->session->push('viewed_videos', $id);
    }
  }
  
}


