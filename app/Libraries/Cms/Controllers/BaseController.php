<?php namespace Libraries\Cms\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class BaseController extends \Illuminate\Routing\Controller {

  public function __construct()
  {
    $platforms = DB::table('platforms')->where('active', '1')->get();
    View::share('platforms', $platforms);
  }

  /**
   * Setup the layout used by the controller.
   *
   * @return void
   */
  protected function setupLayout()
  {
    if (!is_null($this->layout))
    {
      $this->layout = View::make($this->layout);
    }
  }

}
