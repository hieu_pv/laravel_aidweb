<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;

class HomeController extends BaseController {

  public function index()
  {
    return View::make('pages.home.index');
  }

  public function about()
  {
    return View::make('pages.about');
  }

  public function userAgreement()
  {
    return View::make('pages.user_agreement');
  }

  public function privacyPolicy()
  {
    return View::make('pages.privacy_policy');
  }

  public function contact()
  {
    return View::make('pages.contact_us');
  }

}