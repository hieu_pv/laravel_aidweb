<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;
use Libraries\Cms\Transformers\NewsItemForListTransformer;
use Libraries\Facades\GenericUtility;

class IndividualsController extends BaseController {

  private $_individualService;
  private $_filterService;
  //private $_lookupService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_individualService = App::make('\Libraries\Cms\Services\IndividualService');
    $this->_filterService = App::make('\Libraries\Cms\Services\FilterService');
    //$this->_lookupService = App::make('\Libraries\Cms\Services\LookupService');
  }

  public function index()
  {
    return View::make('pages.individuals.index')->with('active_menu', 'menu_6');
  }

  public function getData()
  {
    $inputs = Input::all();

    $query = $inputs['query'];
    $searchQuery = $inputs['searchQuery'];

    $result = $this->_individualService->getData(
      $query['pageNumber'], 
      $query['pageSize'], 
      (object)$query['filters'],
      $query['sortBy'],
      $searchQuery,
      (array)GenericUtility::selectedPlatform()
    );

    return Response::json(array('result' => [
      'data' => $result->data,
      'record_count' => $result->record_count,
      'page_count' => $result->page_count,
      'based_in_count' => $result->based_in_count,
      'counts' => $result->filtersCounts,
    ]));
  }

  public function getFilters()
  {
    $data = new \stdClass();

    //$data->countries = $this->_filterService->getCountriesHavingIndividuals();
    $data->nationalities = $this->_filterService->getNationalitiesHavingIndividuals();
    $data->professional_statuses = $this->_filterService->getProfessionalStatusesHavingIndividuals();
    $data->member_statuses = $this->_filterService->getMemberStatusesHavingIndividuals();
    $data->organisation_types = $this->_filterService->getOrganisationTypesHavingIndividuals();

    $filters = $data;

    //$counts = $this->_filterService->getRecordCountForIndividualsFilters();

    return Response::json(array('filters' => $filters));
  }

}