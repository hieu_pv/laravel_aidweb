<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;
use Libraries\Cms\Transformers\TakeActionForListTransformer;
use Libraries\Facades\GenericUtility;

class TakeActionsController extends BaseController {

  private $_takeActionsService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_takeActionsService = App::make('\Libraries\Cms\Services\TakeActionsService');
  }

  public function index()
  {
    return View::make('pages.takeactions.index')->with('active_menu', 'menu_4');
  }

  public function getData()
  {
    $inputs = Input::all();

    $query = $inputs['query'];
    $searchQuery = $inputs['searchQuery'];
    
    $result = $this->_takeActionsService->getData(
      $query['pageNumber'], 
      $query['pageSize'], 
      (object)$query['filters'],
      $query['sortBy'],
      $searchQuery,
      (array)GenericUtility::selectedPlatform()
    );

    $transformer = new TakeActionForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);
    $result->data = $tdata->toArray();

    return Response::json(array('result' => [
      'data' => $result->data,
      'record_count' => $result->record_count,
      'page_count' => $result->page_count,
      'org_count' => $result->org_count,
      'counts' => $result->filterOptionsRecordCount,
    ]));
  }

  public function preview()
  {
    $skey = Input::get('skey');
    $m = Session::get('preview_takeactions_'.$skey, null);
    if ($m !== null) {
      $m->image = $m->image_base64;

      return View::make('pages.takeactions.preview')->with('model', $m)
                                                    ->with('liked', false)
                                                    ->with('active_menu', 'menu_4');
    }

    return App::abort(404);
  }

  public function getByOrganisationId()
  {
    $organisationId = Input::get('organisationId');
    $pageNumber = Input::get('pageNumber');

    $result = $this->_takeActionsService->getByOrganisationId($organisationId, $pageNumber);

    $transformer = new TakeActionForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);

    return Response::json(array('result' => $tdata, 'record_count' => $result->record_count, 'page_count' => $result->page_count));
  }

}