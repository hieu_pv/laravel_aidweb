<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;
use Libraries\Facades\GenericUtility;
use Libraries\Cms\Transformers\VideoForListTransformer;

class VideosController extends BaseController {

  private $_videosService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_videosService = App::make('\Libraries\Cms\Services\VideosService');
  }

  public function index()
  {
    return View::make('pages.videos.index')->with('active_menu', 'menu_3');
  }

  public function getData()
  {
    $inputs = Input::all();

    $query = $inputs['query'];
    $searchQuery = $inputs['searchQuery'];
    
    $result = $this->_videosService->getData(
      $query['pageNumber'], 
      $query['pageSize'], 
      (object)$query['filters'],
      $query['sortBy'],
      $searchQuery,
      (array)GenericUtility::selectedPlatform()
    );

    $transformer = new VideoForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);
    $result->data = $tdata->toArray();

    return Response::json(array('result' => [
      'data' => $result->data,
      'record_count' => $result->record_count,
      'page_count' => $result->page_count,
      'org_count' => $result->org_count,
      'counts' => $result->filterOptionsRecordCount,
    ]));
  }
  
  public function preview()
  {
    $skey = Input::get('skey');
    $m = Session::get('preview_videos_'.$skey, null);
    if ($m !== null) {
      //$m->image = $m->image_base64;

      return View::make('pages.videos.preview')->with('model', $m)
                                               ->with('liked', false)
                                               ->with('active_menu', 'menu_4');
    }

    return App::abort(404);
  }

  public function getVideosByOrganisationId()
  {
    $organisationId = Input::get('organisationId');
    $pageNumber = Input::get('pageNumber');

    $result = $this->_videosService->getVideosByOrganisationId($organisationId, $pageNumber);

    $transformer = new VideoForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);

    return Response::json(array('result' => $tdata, 'record_count' => $result->record_count, 'page_count' => $result->page_count));
  }

  public function viewed()
  {
    $id = intval(Input::get('id', 0));

    // update view_count first
    $args = new \stdClass();
    $args->module = Modules::VIDEOS;
    $args->id = $id;
    Event::fire('posting.view', $args);

    return Response::make($id, 200);
  }

}