<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;
use Libraries\Facades\GenericUtility;

class PublicityController extends BaseController {

  public function __construct()
  {
    parent::__construct();
  }

  public function preview()
  {
    $skey = Input::get('skey');
    $m = Session::get('preview_publicity_'.$skey, null);
    if ($m !== null) {
      $m->image = $m->image_base64;
      return View::make('pages.publicities.view')->with('model', $m)
                                                 ->with('active_menu', 'menu_0');
    }

    return App::abort(404);
  }

}