<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;
use Libraries\Cms\Transformers\NewsItemForListTransformer;
use Libraries\Facades\GenericUtility;
use Libraries\Models\User;

class NewsController extends BaseController {

  private $_newsService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_newsService = App::make('\Libraries\Cms\Services\NewsService');
  }

  public function index()
  {
    return View::make('pages.news.index')->with('active_menu', 'menu_1');
  }

  public function getData()
  {
    $inputs = Input::all();

    $query = $inputs['query'];
    $searchQuery = $inputs['searchQuery'];
    
    $result = $this->_newsService->getData(
      $query['pageNumber'], 
      $query['pageSize'], 
      (object)$query['filters'],
      $query['sortBy'],
      $searchQuery,
      (array)GenericUtility::selectedPlatform()
    );

    $transformer = new NewsItemForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);
    $result->data = $tdata->toArray();

    return Response::json(array('result' => [
      'data' => $result->data,
      'record_count' => $result->record_count,
      'page_count' => $result->page_count,
      'org_count' => $result->org_count,
      'counts' => $result->filterOptionsRecordCount,
    ]));
  }

  public function getByPermalink($permalink) 
  {
    $fragments = PermalinkEngine::resolvePermalink(Modules::NEWS, $permalink);

    $id = $fragments['id'];
    $slug = $fragments['slug'];

    // update view_count first
    $args = new \stdClass();
    $args->module = Modules::NEWS;
    $args->id = $id;
    Event::fire('posting.view', $args);

    // then get the actual model
    $model = $this->_newsService->getByIdAndSlug($id, $slug);
    if ($model === null) {
      App::abort(404);
    }

    $liked = (new \Libraries\Cms\EventHandlers\GenericLikePostingEventHandler())->alreadyLiked(Modules::NEWS, $id);
    return View::make('pages.news.view')->with('model', $model)
                                        ->with('liked', $liked)
                                        ->with('active_menu', 'menu_1');
  }

  public function preview()
  {
    $skey = Input::get('skey');
    $m = Session::get('preview_news_'.$skey, null);
    if ($m !== null) {
      $m->image = $m->image_base64;
      return View::make('pages.news.view')->with('model', $m)
                                          ->with('liked', false)
                                          ->with('active_menu', 'menu_1');
    }

    return App::abort(404);
  }

  public function getNewsByOrganisationId()
  {
    $organisationId = Input::get('organisationId');
    $pageNumber = Input::get('pageNumber');

    $result = $this->_newsService->getNewsByOrganisationId($organisationId, $pageNumber);

    $transformer = new NewsItemForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);

    return Response::json(array('result' => $tdata, 'record_count' => $result->record_count, 'page_count' => $result->page_count));
  }

}