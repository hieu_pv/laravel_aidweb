<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;
use Libraries\Cms\Transformers\NewsItemForListTransformer;
use Libraries\Facades\GenericUtility;

class OrganisationsController extends BaseController {

  private $_organisationService;
  private $_filterService;
  //private $_lookupService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_organisationService = App::make('\Libraries\Cms\Services\OrganisationService');
    $this->_filterService = App::make('\Libraries\Cms\Services\FilterService');
    //$this->_lookupService = App::make('\Libraries\Cms\Services\LookupService');
  }

  public function index()
  {
    return View::make('pages.organisations.index')->with('active_menu', 'menu_5');
  }

  public function getData()
  {
    $inputs = Input::all();

    $query = $inputs['query'];
    $searchQuery = $inputs['searchQuery'];

    $result = $this->_organisationService->getData(
      $query['pageNumber'], 
      $query['pageSize'], 
      (object)$query['filters'],
      $query['sortBy'],
      $searchQuery,
      (array)GenericUtility::selectedPlatform()
    );

    return Response::json(array('result' => [
      'data' => $result->data,
      'record_count' => $result->record_count,
      'page_count' => $result->page_count,
      'counts' => $result->filtersCounts,
    ]));
  }

  public function getFilters()
  {
    $data = new \stdClass();

    $data->countries = $this->_filterService->getCountriesHavingOrganisations();
    $data->level_of_activities = $this->_filterService->getLevelOfActivitiesHavingOrganisations();
    $data->organisation_types = $this->_filterService->getOrganisationTypesHavingOrganisations();

    $filters = $data;

    //$counts = $this->_filterService->getRecordCountForOrganisationsFilters();

    return Response::json(array('filters' => $filters));
  }

}