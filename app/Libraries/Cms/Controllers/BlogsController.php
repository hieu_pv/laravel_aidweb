<?php namespace Libraries\Cms\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Libraries\Cms\Transformers\BlogPostForListTransformer;
use Libraries\Facades\PermalinkEngine;
use Libraries\Facades\GenericUtility;
use Libraries\Models\Blog\Post;
use Libraries\Support\Modules;

class BlogsController extends BaseController {

  private $_blogService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_blogService = App::make('\Libraries\Cms\Services\BlogService');
  }

  /**
   * [showIndex description]
   * @return [type]
   */
  public function index()
  {
    return View::make('pages.blogs.index')->with('active_menu', 'menu_2');
  }

  public function getData()
  {
    $inputs = Input::all();

    $query = $inputs['query'];
    $searchQuery = $inputs['searchQuery'];

    $result = $this->_blogService->getData(
      $query['pageNumber'],
      $query['pageSize'],
      (object)$query['filters'],
      $query['sortBy'],
      $searchQuery,
      (array)GenericUtility::selectedPlatform()
    );

    if ($result === null || empty($result) || count($result) === 0) {
      $result = new \stdClass();
      $result->data = [];
      $result->record_count = 0;
      $result->page_count = 0;
      $result->ind_count = 0;
    } else {
      $transformer = new BlogPostForListTransformer();
      $tdata = $transformer->transformCollection($result->raw_data);
      $result->data = $tdata->toArray();
    }

    return Response::json(array('result' => [
      'data' => $result->data,
      'record_count' => $result->record_count,
      'page_count' => $result->page_count,
      'ind_count' => $result->ind_count,
      'counts' => $result->filterOptionsRecordCount,
    ]));
  }

  public function getByPermalink($permalink)
  {
    $fragments = PermalinkEngine::resolvePermalink(Modules::BLOG, $permalink);

    $id = $fragments['id'];
    $slug = $fragments['slug'];

    // update view_count first
    $args = new \stdClass();
    $args->module = Modules::BLOG;
    $args->id = $id;
    Event::fire('posting.view', $args);

    // then get the actual model
    $model = $this->_blogService->getByIdAndSlug($id, $slug);
    if ($model === null) {
      App::abort(404);
    }

    $liked = (new \Libraries\Cms\EventHandlers\GenericLikePostingEventHandler())->alreadyLiked(Modules::BLOG, $id);

    return View::make('pages.blogs.view')->with('model', $model)
                                         ->with('liked', $liked)
                                         ->with('active_menu', 'menu_2');
  }

  public function preview()
  {
    $skey = Input::get('skey');
    $m = Session::get('preview_blogs_'.$skey, null);
    if ($m !== null) {
      $m->image = $m->image_base64;
      return View::make('pages.blogs.view')->with('model', $m)
                                           ->with('liked', false)
                                           ->with('active_menu', 'menu_2');
    }

    return App::abort(404);
  }

  public function getPostsByUserProfileId()
  {
    $userProfileId = Input::get('userProfileId');
    $pageNumber = Input::get('pageNumber');

    $result = $this->_blogService->getPostsByUserProfileId($userProfileId, $pageNumber);

    $transformer = new BlogPostForListTransformer();
    $tdata = $transformer->transformCollection($result->raw_data);

    return Response::json(array('result' => $tdata, 'record_count' => $result->record_count, 'page_count' => $result->page_count));
  }

}
