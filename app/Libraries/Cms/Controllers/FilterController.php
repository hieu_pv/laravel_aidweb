<?php namespace Libraries\Cms\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;

class FilterController extends BaseController {
  
  protected $_filterService;
  //protected $_lookupService;

  public function __construct()
  {
    parent::__construct();
    $this->_filterService = App::make('\Libraries\Cms\Services\FilterService');
    //$this->_lookupService = App::make('\Libraries\Cms\Services\LookupService');
  }

  public function getFilters($moduleId)
  {
    // get filters for postings. 
    // - regions and countries are merged

    $filters = $this->privatelyGetFilters($moduleId);

    //$counts = $this->_filterService->getRecordCountInFilters($moduleId);

    return Response::json(array('filters' => $filters));
  }

  private function privatelyGetFilters($moduleId)
  {
    $data = new \stdClass();

    $data->countries = $this->_filterService->getCountriesForFilter($moduleId);
    $data->nationalities = $this->_filterService->getNationalitiesForFilter($moduleId);
    $data->regions = $this->_filterService->getRegionsForFilter($moduleId);
    $data->crisis = $this->_filterService->getCrisisForFilter($moduleId);
    $data->sectors = $this->_filterService->getSectorsForFilter($moduleId);
    $data->themes = $this->_filterService->getThemesForFilter($moduleId);
    $data->interventions = $this->_filterService->getInterventionsForFilter($moduleId);
    $data->beneficiaries = $this->_filterService->getBeneficiariesForFilter($moduleId);
    $data->actiontypes = $this->_filterService->getTakeActionTypesForFilter($moduleId);
    $data->organisation_types = $this->_filterService->getOrganisationTypesForFilter($moduleId);

    return $data;
  }

}
