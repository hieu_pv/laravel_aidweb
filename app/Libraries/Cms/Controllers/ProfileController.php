<?php namespace Libraries\Cms\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;

class ProfileController extends BaseController {

  private $_profileService;

  public function __construct()
  {
    parent::__construct();
    $this->_profileService = App::make('\Libraries\Cms\Services\ProfileService');
  }

  public function getIndividualProfile($profileId)
  {
    $user = $this->_profileService->getIndividual($profileId);
    if ($user === null) {
      return App::abort(404);
    }

    return View::make('pages.profile.individual')->with('model', $user)->with('active_menu', 'menu_6');
  }

  public function getOrganisationProfile($organisationId)
  {
    $user = $this->_profileService->getOrganisation($organisationId);
    if ($user === null) {
      return App::abort(404);
    }

    return View::make('pages.profile.organisation')->with('model', $user)->with('active_menu', 'menu_5');
  }

}
