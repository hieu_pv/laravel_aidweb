<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Libraries\Facades\PermalinkEngine;
use Libraries\Facades\GenericUtility;
use Libraries\Facades\SettingsProvider;
use Libraries\Facades\Messages;
//use Libraries\Facades\Publicities;
use Libraries\Support\Modules;
use Libraries\Models\Comment;
use Jenssegers\Date\Date;
use Laracasts\Flash\Flash;

class MethodsController extends BaseController {

  //private $_newsService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    //$this->_newsService = App::make('\Libraries\Cms\Services\NewsService');
  }

  /**
   * [likePosting description]
   * @return [type] [description]
   */
  public function likePosting()
  {
    $inputs = Input::all();
    if (array_key_exists('id', $inputs) && array_key_exists('module', $inputs)) {
      $args = new \stdClass();
      $args->module = (int)$inputs['module'];
      $args->id = (int)$inputs['id'];
      Event::fire('posting.like', $args);

      return Response::json(array('ok' => true));
    } else {
      return App::abort(500);
    }
  }

  /**
   * [giveComment description]
   * @return [type] [description]
   */
  public function giveComment()
  {
    $inputs = Input::all();
    if (array_key_exists('id', $inputs) && array_key_exists('module', $inputs) && array_key_exists('content', $inputs)) {
      $args = new \stdClass();
      $args->module = (int)$inputs['module'];
      $args->id = (int)$inputs['id'];
      $args->content = $inputs['content'];
      Event::fire('posting.comment', $args);

      return Response::json(array('ok' => true));
    } else {
      return App::abort(500);
    }
  }

  private function mapComments($cs) 
  {
    foreach ($cs as $item) {
      if ($item->profile_type === Modules::PROFILE_TYPE_INDIVIDUAL) {
        $item->profile_name = $item->first_name.' '.$item->last_name;
        $item->profile_image = $item->avatar;
        $item->profile_url = '#';
        $item->profile_url = PermalinkEngine::resolveProfileUrl($item->user_profile_id, Modules::PROFILE_TYPE_INDIVIDUAL);
      } else if ($item->profile_type === Modules::PROFILE_TYPE_ORGANISATION) {
        $item->profile_name = $item->org_name;
        $item->profile_image = $item->org_logo;
        $item->profile_url = '#';
        $item->profile_url = PermalinkEngine::resolveProfileUrl($item->organisation_id, Modules::PROFILE_TYPE_ORGANISATION);
      }
      $item->time_ago = \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->diffForHumans();
    }
  }

  /**
   * [getComments description]
   * @return [type] [description]
   */
  public function getComments()
  {
    // TODO
    // MOVE THIS TO SERVICE OR REPO
    $inputs = Input::all();
    if (array_key_exists('id', $inputs) && array_key_exists('module', $inputs)) {
      $response = new \stdClass();

      $cs = DB::table('comments')->select(DB::raw("comments.id, comments.content, comments.commented_by, user_profiles.id as 'user_profile_id', user_profiles.first_name, user_profiles.last_name, user_profiles.avatar, organisations.id as 'organisation_id', organisations.org_name, organisations.org_logo, comments.created_at, users.profile_type"))
                                 ->join('users', 'users.id', '=', 'comments.commented_by')
                                 ->leftjoin('user_profiles', 'comments.commented_by', '=', 'user_profiles.user_id')
                                 ->leftjoin('organisations', 'comments.commented_by', '=', 'organisations.cp_id')
                                 ->where('comments.item_id', $inputs['id'])
                                 ->where('comments.module_id', $inputs['module'])
                                 ->orderBy('comments.created_at', 'desc')
                                 ->get();

      $this->mapComments($cs);

      return Response::json($cs);
    } else {
      return Response::json(new \stdClass());
    }
  }

  /**
   * [getNewestComments description]
   * @return [type] [description]
   */
  public function getNewestComments()
  {
    // TODO
    // MOVE THIS TO SERVICE OR REPO
    $inputs = Input::all();
    if (array_key_exists('id', $inputs) && array_key_exists('module', $inputs) && array_key_exists('pivot_id', $inputs)) {
      $response = new \stdClass();

      $cs = DB::table('comments')->select(DB::raw("comments.id, comments.content, comments.commented_by, user_profiles.id as 'user_profile_id', user_profiles.first_name, user_profiles.last_name, user_profiles.avatar, organisations.id as 'organisation_id', organisations.org_name, organisations.org_logo, comments.created_at, users.profile_type"))
                                 ->join('users', 'users.id', '=', 'comments.commented_by')
                                 ->leftjoin('user_profiles', 'comments.commented_by', '=', 'user_profiles.user_id')
                                 ->leftjoin('organisations', 'comments.commented_by', '=', 'organisations.cp_id')
                                 ->where('comments.item_id', $inputs['id'])
                                 ->where('comments.module_id', $inputs['module'])
                                 ->where('comments.id', '>', $inputs['pivot_id'])
                                 ->orderBy('comments.created_at', 'desc')
                                 ->get();

      $this->mapComments($cs);

      return Response::json($cs);
    } else {
      return Response::json(new \stdClass());
    }
  }

  /**
   * [changePlatform description]
   * @return [type] [description]
   */
  public function changePlatform()
  {
    $platformId = Input::get('platform_id');
    GenericUtility::changePlatform($platformId);
    \Publicities::clearPublicitySessions();
    //return Response::route('cms_home');
    return Redirect::back();
  }

  public function postContact()
  {
    $subject = Input::get('subject');
    $name = Input::get('name');
    $email = Input::get('email');
    $message = Input::get('message');

    $ok = $this->postContactInner($subject, $name, $email, $message);
    if ($ok) {
      Flash::success(Messages::successContact());
      return Redirect::back();
    } else {
      Flash::error(Messages::failedContact());
      return Redirect::back();
    }
  }

  public function postContact2()
  {
    // $subject = Input::get('quick-contact-form-subject');
    // $name = Input::get('quick-contact-form-name');
    // $email = Input::get('quick-contact-form-email');
    // $message = Input::get('quick-contact-form-message');
    $subject = Input::get('subject');
    $name = Input::get('name');
    $email = Input::get('email');
    $message = Input::get('message');

    $ok = $this->postContactInner($subject, $name, $email, $message);
    if ($ok) {
      return Response::json(array('message' => Messages::successContact2()));
    } else {
      Flash::error(Messages::failedContact());
      return Response::json(array('message' => Messages::failedContact2()));
    }
  }

  private function postContactInner($subject, $name, $email, $message)
  {
    $valid = isset($subject) && $subject !== null && !is_null($subject) && strlen($subject) > 0 &&
             isset($name) && $name !== null && !is_null($name) && strlen($name) > 0 &&
             isset($email) && $email !== null && !is_null($email) && strlen($email) > 0 &&
             isset($message) && $message !== null && !is_null($message) && strlen($message) > 0;

    if ($valid) {
      // create a record in db
      $date = Date::now()->format('Y-m-d H:i');
      $id = DB::table('contacts')->insertGetId([
        'subject' => $subject,
        'name' => $name,
        'email' => $email,
        'message' => $message,
        'created_at' => $date,
        'updated_at' => $date
      ]);
      // get the contact object
      $contact = DB::table('contacts')->where('id', $id)->first();
      // send email
      try {
        // get the subject
        $subjects = SettingsProvider::contactSubjects();
        $subject = $subjects[$subject - 1];
        // interate targets
        $targets = SettingsProvider::contactTargets();
        foreach ($targets as $key => $value) {
          // compose the mail and send
          $md = array('model' => $contact, 'subject' => $subject);
          Mail::send('emails.feedback', $md, function ($message) use ($value) {
            $message->to($value)->subject(Messages::emailSubjectContact());
          });
        }
      } catch (\Exception $ex) {
        Log::error($ex);
      }

      return true;
    } else {
      return false;
    }
  }

}