<?php namespace Libraries\Cms\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;

class SandboxController extends BaseController {

  public function index()
  {
    return View::make('pages.sandbox.index');
  }

}