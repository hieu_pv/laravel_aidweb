<?php namespace Libraries\Support;

class Modules {

  const PLATFORM_INTERNATIONAL = 1;

  public function platform_international()
  {
    return self::PLATFORM_INTERNATIONAL;
  }

  /*================================================*/

  const PROFILE_TYPE_INDIVIDUAL = 1;
  const PROFILE_TYPE_ORGANISATION = 2;
  const PROFILE_TYPE_ADMIN = 99;

  public function profile_type_individual()
  {
    return self::PROFILE_TYPE_INDIVIDUAL;
  }

  public function profile_type_organisation()
  {
    return self::PROFILE_TYPE_ORGANISATION;
  }

  public function profile_type_admin()
  {
    return self::PROFILE_TYPE_ADMIN;
  }

  /*================================================*/

  const BLOG = 1;
  const NEWS = 2;
  const TAKE_ACTIONS = 3;
  const VIDEOS = 4;
  const PUBLICITY = 11;

  public function blog()
  {
    return self::BLOG;
  }
  
  public function news()
  {
    return self::NEWS;
  }

  public function takeactions()
  {
    return self::TAKE_ACTIONS;
  }

  public function videos()
  {
    return self::VIDEOS;
  }

  public function publicity() 
  {
    return self::PUBLICITY;
  }

  /*================================================*/

  const PUBLICITY_TYPE_BIG = 1;
  const PUBLICITY_TYPE_SMALL = 2;
  const PUBLICITY_TYPE_SIDE = 3;

  public function publicity_type_big() 
  {
    return self::PUBLICITY_TYPE_BIG;
  }

  public function publicity_type_small() 
  {
    return self::PUBLICITY_TYPE_SMALL;
  }

  public function publicity_type_side() 
  {
    return self::PUBLICITY_TYPE_SIDE;
  }

  /*================================================*/

  const LEVEL_INTERNATIONAL = 3;

  public function level_international() 
  {
    return self::LEVEL_INTERNATIONAL;
  }

  /*================================================*/

  const ALL_PLATFORMS = 0;

  public function all_platforms() 
  {
    return self::ALL_PLATFORMS;
  }
}