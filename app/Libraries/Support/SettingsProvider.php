<?php namespace Libraries\Support;

use Libraries\Models\Settings;
use Illuminate\Support\Facades\Schema;

class SettingsProvider {

  public function get($key) 
  {
    $value = '';

    try {
      if (Schema::hasTable('settings'))
      {
        $db_record = Settings::where('key', '=', $key)->first();
        // got to check if it's null or not and make a default value so there's no error 
        // "access attribute of non-object bla bla"
        if($db_record === null) {
          $db_record = new Settings();
        }
        if ($db_record !== null && isset($db_record) && !is_null($db_record)) {
          $value = $db_record->value;
        } else {
          $value = $db_record->default_value;
        }
      }
      return $value;
    } catch (\Exception $e) {
      return $value;
    }
  }

  // public function set($key) 
  // {
    
  // }
  
  public function batchSave($input)
  {
    foreach ($input as $key => $val) {
      $the = Settings::where('key', '=', $key)->first();
      $the->value = $val;
      $the->save();
    }
  }

  public function urls()
  {
    $result = array();
    try {
      if (Schema::hasTable('settings'))
      {
        $db_record = Settings::where('key', 'LIKE', 'urls_%')->get();
        $result = $db_record;
      }
      return $result;
    } catch (\Exception $e) {
      return $result;
    }
  }

  public function contactTargets()
  {
    return [
      'info@aidpost.org',
      'haritianpinio@gmail.com'
    ];
  }

  public function contactSubjects()
  {
    return [
      'Complaints',
      'Feedback',
      'Questions'
    ];
  }

}