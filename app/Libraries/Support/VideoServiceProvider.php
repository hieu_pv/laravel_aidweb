<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class VideoServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\Videos\IVideoRepository', function() {
      return new \Libraries\Repositories\Implementations\Videos\VideoRepository(new \Libraries\Models\Videos\Video());
    });

    $this->app->bind('\Libraries\Cms\Services\VideosService', function()
    {
      return new \Libraries\Cms\Services\VideosService(
        $this->app->make('\Libraries\Repositories\Videos\IVideoRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IRegionRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IThemeRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\VideosService', function() {
      return new \Libraries\Admin\Services\VideosService(
        $this->app->make('\Libraries\Repositories\Videos\IVideoRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });

    $this->app->view->composer('pages.videos.index', 'Libraries\Cms\Composers\VideosComposer');
  }

}