<?php namespace Libraries\Support;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GenericUtility {

  public function GUID()
  {
    if (function_exists('com_create_guid') === true) {
      return trim(com_create_guid(), '{}');
    }

    return sprintf
    (
      '%04X%04X-%04X-%04X-%04X-%04X%04X%04X', 
      mt_rand(0, 65535), 
      mt_rand(0, 65535), 
      mt_rand(0, 65535), 
      mt_rand(16384, 20479),
      mt_rand(32768, 49151), 
      mt_rand(0, 65535), 
      mt_rand(0, 65535), 
      mt_rand(0, 65535)
    );
  }

  public function prefixWithHttpIfNeeded($path)
  {
    // return empty if empty :D
    if (strlen($path) === 0) {
      return $path;
    }
    $notStartWithHttp = !starts_with(strtolower($path), 'http://');
    $notStartWithHttps = !starts_with(strtolower($path), 'https://');
    if ($notStartWithHttp && $notStartWithHttps) {
      return 'http://'.$path;
    }
    return $path;
  }

  public function selectedPlatform()
  {
    try {
      $default = DB::table('platforms')->where('id', Modules::PLATFORM_INTERNATIONAL)->where('active', '1')->first();
      if (!Session::has('selected_platform')) {
        Session::put('selected_platform', json_encode($default));
      }
      $selectedPlatform = Session::get('selected_platform', json_encode($default));
      return json_decode($selectedPlatform);
    } catch (\Exception $ex) {
    }
  }

  public function changePlatform($platformId)
  {
    $p = DB::table('platforms')->where('id', $platformId)->where('active', '1')->first();
    if ($p !== null) {
      Session::put('selected_platform', json_encode($p));
    }
  }

  public function startsWith($haystack, $needle)
  {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
  }

  public function endsWith($haystack, $needle)
  {
    $length = strlen($needle);
    if ($length == 0) {
      return true;
    }

    return (substr($haystack, -$length) === $needle);
  }

  public function parseIso8601Interval($iso8601IntervalStr)
  {
    // from ISO 8601 interval string format
    //^P(\d+)+Y(\d+)M(\d+)DT(\d+)H(\d+)M(\d+)S$
    $pattern = '/^P(\d+)Y(\d+)M(\d+)DT(\d+)H(\d+)M(\d+)S$/';
    $matches = [];
    $match = preg_match($pattern, $iso8601IntervalStr, $matches); //, PREG_OFFSET_CAPTURE);
    if ($match) {
      $interval = new Iso8601Interval($matches[1], $matches[2], $matches[3], $matches[4], $matches[5], $matches[6]);
      return $interval;
    }
    return new Iso8601Interval(0, 1, 0, 0, 0, 0);
  }

}