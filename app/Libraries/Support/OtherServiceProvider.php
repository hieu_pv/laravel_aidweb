<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class OtherServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Admin\Services\FileService', function () {
      return new \Libraries\Admin\Services\FileService();
    });
    
    $this->app->view->composer('pages.home.index', 'Libraries\Cms\Composers\HomeComposer');

    $this->app->view->composer('pages.about', 'Libraries\Cms\Composers\StaticPageComposer');
    $this->app->view->composer('pages.user_agreement', 'Libraries\Cms\Composers\StaticPageComposer');
    $this->app->view->composer('pages.privacy_policy', 'Libraries\Cms\Composers\StaticPageComposer');
    $this->app->view->composer('pages.contact_us', 'Libraries\Cms\Composers\StaticPageComposer');
    
  }

}