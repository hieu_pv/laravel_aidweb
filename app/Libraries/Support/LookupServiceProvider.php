<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class LookupServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\Lookups\INationalityRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\NationalityRepository(new \Libraries\Models\Lookups\Nationality());
    });

    $this->app->bind('\Libraries\Repositories\Lookups\IMemberStatusRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\MemberStatusRepository(new \Libraries\Models\Lookups\MemberStatus());
    });

    $this->app->bind('\Libraries\Repositories\Lookups\IProfessionalStatusRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\ProfessionalStatusRepository(new \Libraries\Models\Lookups\ProfessionalStatus());
    });

    $this->app->bind('\Libraries\Repositories\Lookups\IOfficeTypeRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\OfficeTypeRepository(new \Libraries\Models\Lookups\OfficeType());
    });

    $this->app->bind('\Libraries\Repositories\Lookups\IOrganisationTypeRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\OrganisationTypeRepository(new \Libraries\Models\Lookups\OrganisationType());
    });

    $this->app->bind('\Libraries\Repositories\Lookups\ILevelOfActivityRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\LevelOfActivityRepository(new \Libraries\Models\Lookups\LevelOfActivity());
    });

    $this->app->bind('\Libraries\Repositories\Lookups\ILevelOfResponsibilityRepository', function() {
      return new \Libraries\Repositories\Implementations\Lookups\LevelOfResponsibilityRepository(new \Libraries\Models\Lookups\LevelOfResponsibility());
    });

    // $this->app->bind('\Libraries\Cms\Services\LookupService', function() {
    //   // orderly
    //   return new \Libraries\Cms\Services\LookupService(
    //     $this->app->make('\Libraries\Repositories\Lookups\INationalityRepository'),
    //     $this->app->make('\Libraries\Repositories\Lookups\IMemberStatusRepository'),
    //     $this->app->make('\Libraries\Repositories\Lookups\IProfessionalStatusRepository'),
    //     $this->app->make('\Libraries\Repositories\Lookups\IOfficeTypeRepository'),
    //     $this->app->make('\Libraries\Repositories\Lookups\IOrganisationTypeRepository'),
    //     $this->app->make('\Libraries\Repositories\Filters\ICountryRepository'),
    //     $this->app->make('\Libraries\Repositories\Lookups\ILevelOfActivityRepository')
    //   );
    // });

    $this->app->bind('\Libraries\Admin\Services\LookupService', function() {
      // orderly
      return new \Libraries\Admin\Services\LookupService(
        $this->app->make('\Libraries\Repositories\Lookups\INationalityRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IMemberStatusRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IProfessionalStatusRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IOfficeTypeRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IOrganisationTypeRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ICountryRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\ILevelOfActivityRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\ILevelOfResponsibilityRepository')
      );
    });
  }

}