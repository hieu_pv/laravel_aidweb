<?php namespace Libraries\Support;

class Messages {

  public function __construct()
  {

  }

  public function successPosting($title) 
  {
    return '<i class="fa fa-check-circle"></i> <strong>Success!</strong> Thank you for your posting. If you are a TOP agency/member your posting has been instantly displayed on AidPost. If you are not a TOP agency/member, please wait for AidPost admin to publish your posting.';
  }

  public function successEditPosting($title) 
  {
    return '<i class="fa fa-check-circle"></i> <strong>Success!</strong> You have edited a posting with title: <strong>"'.$title.'"</strong>.';
  }

  public function successPostPublicity()
  {
    return '<i class="fa fa-check-circle"></i> <strong>Success!</strong> Thank you for your posting. Your Publicity will be published by AidPost admin after all prerequisites met.';
  }

  public function successCreatePlatform($name)
  {
    return '<i class="fa fa-check-circle"></i> <strong>Success!</strong> You have created platform: <strong>'.$name.'</strong>';
  }

  public function emailSubjectAccountActivation()
  {
    return 'Activate your AidPost account';
  }

  public function validationMessageOldPasswordRequired()
  {
    return 'Old Password is required.';
  }

  public function validationMessageNewPasswordRequired()
  {
    return 'New Password is required.';
  }

  public function validationMessagePasswordNotValid()
  {
    return 'The Current Password is not valid.';
  }

  public function validationMessagePasswordConfirmationDifferent()
  {
    return 'Password confirmation failed.';
  }

  public function validationMessageUserNotFound()
  {
    return 'The User is not found.';
  }

  public function validationMessageEmailUsernameNotRegistered()
  {
    return 'Email and Username is not registered in the system.';
  }

  public function validationMessagePasswordResetTokenInvalid()
  {
    return 'Invalid Password Reset Token.';
  }

  public function validationMessagePasswordResetTokenExpired()
  {
    return 'Password Reset Token is expired.';
  }

  public function emailSubjectContact()
  {
    return 'Message Submitted';
  }

  public function successContact()
  {
    return '<h3>Thank You!</h3> <p>Your message has been received.</p>';
  }

  public function failedContact()
  {
    return '<h3>Ooops!</h3> <p>Failed to send your message, probably due to technical error. Please send message directly to <a href="mailto:info@aidpost.org">info@aidpost.org</a></p>';
  }

  public function successContact2()
  {
    return '<strong>Thank You!</strong><br/><span>Your message has been received.</span><br/>';
  }

  public function failedContact2()
  {
    return '<strong>Ooops!</strong><br/><span>Failed to send your message, probably due to technical error. Please send message directly to <a href="mailto:info@aidpost.org">info@aidpost.org</a></span><br/>';
  }
  
}