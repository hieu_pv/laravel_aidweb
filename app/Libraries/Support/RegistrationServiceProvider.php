<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class RegistrationServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\IUserRepository', function() {
      return new \Libraries\Repositories\Implementations\UserRepository(new \Libraries\Models\User());
    });

    $this->app->bind('\Libraries\Repositories\IUserProfileRepository', function() {
      return new \Libraries\Repositories\Implementations\UserProfileRepository(new \Libraries\Models\UserProfile());
    });

    $this->app->bind('\Libraries\Repositories\IOrganisationRepository', function() {
      return new \Libraries\Repositories\Implementations\OrganisationRepository(new \Libraries\Models\Organisation());
    });

    $this->app->bind('\Libraries\Cms\Services\ProfileService', function()
    {
      return new \Libraries\Cms\Services\ProfileService(
        $this->app->make('\Libraries\Repositories\IUserRepository'),
        $this->app->make('\Libraries\Repositories\IUserProfileRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\UserService', function() {
      return new \Libraries\Admin\Services\UserService(
        $this->app->make('\Libraries\Repositories\IUserRepository'),
        $this->app->make('\Libraries\Repositories\IUserProfileRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\OrganisationService', function() {
      return new \Libraries\Admin\Services\OrganisationService(
        $this->app->make('\Libraries\Repositories\IUserRepository'),
        $this->app->make('\Libraries\Repositories\IUserProfileRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\RegistrationService', function() {
      return new \Libraries\Admin\Services\RegistrationService(
        $this->app->make('\Libraries\Repositories\IUserRepository'),
        $this->app->make('\Libraries\Repositories\IUserProfileRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\INationalityRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IMemberStatusRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IProfessionalStatusRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IOfficeTypeRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IOrganisationTypeRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ICountryRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\ILevelOfActivityRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\ILevelOfResponsibilityRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\PasswordService', function() {
      return new \Libraries\Admin\Services\PasswordService(
        $this->app->make('\Libraries\Repositories\IUserRepository'),
        $this->app->make('\Libraries\Repositories\IUserProfileRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });
  }

}