<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class TakeActionServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\TakeActions\ITakeActionRepository', function() {
      return new \Libraries\Repositories\Implementations\TakeActions\TakeActionRepository(new \Libraries\Models\TakeActions\TakeAction());
    });

    $this->app->bind('\Libraries\Repositories\TakeActions\ITakeActionTypeRepository', function() {
      return new \Libraries\Repositories\Implementations\TakeActions\TakeActionTypeRepository(new \Libraries\Models\TakeActions\Type());
    });

    $this->app->bind('\Libraries\Cms\Services\TakeActionsService', function()
    {
      return new \Libraries\Cms\Services\TakeActionsService(
        $this->app->make('\Libraries\Repositories\TakeActions\ITakeActionRepository'),
        $this->app->make('\Libraries\Repositories\TakeActions\ITakeActionTypeRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\TakeActionsService', function() {
      return new \Libraries\Admin\Services\TakeActionsService(
        $this->app->make('\Libraries\Repositories\TakeActions\ITakeActionRepository'),
        $this->app->make('\Libraries\Repositories\TakeActions\ITakeActionTypeRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });

    $this->app->view->composer('pages.takeactions.index', 'Libraries\Cms\Composers\TakeActionsComposer');
  }

}