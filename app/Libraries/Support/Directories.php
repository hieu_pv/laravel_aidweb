<?php namespace Libraries\Support;

use Illuminate\Support\Facades\Auth;
use Libraries\Support\Modules;

class Directories {

  private function ensureDirectoryExistance($dir)
  {
    if (!file_exists($_SERVER['DOCUMENT_ROOT'].$dir)) {
      mkdir($_SERVER['DOCUMENT_ROOT'].$dir, 0777, true);
    }
  }

  public function getMemberMediaDirectory($memberId = 0) 
  {
    $dir = '';
    if (Auth::check() && $memberId === 0) {
        // The user is logged in...
        if (Auth::user()->isAdmin()) {
          $dir = '/uploads/media/';
        } else {
          $dir = '/uploads/media/'.Auth::user()->id.'/';
        }
    } else {
      $dir = '/uploads/media/'.$memberId.'/';
    }
    $this->ensureDirectoryExistance($dir);
    return $dir;
  }

  public function getMemberModuleDirectory($module)
  {
    $base = '';
    switch ($module) {
      case Modules::BLOG:
        $base = 'posts';
        break;
      case Modules::NEWS:
        $base = 'news';
        break;
      case Modules::TAKE_ACTIONS:
        $base = 'takeactions';
        break;
      case Modules::VIDEOS:
        $base = 'videos';
        break;
      case Modules::PUBLICITY:
        $base = 'publicities';
        break;
      default:
        break;
    }

    $memberId = Auth::user()->id;

    $dir = '/uploads/media/'.$memberId.'/'.$base.'/';

    $this->ensureDirectoryExistance($dir);

    return $dir;
  }
  
}