<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\Blog\IPostRepository', function() {
      return new \Libraries\Repositories\Implementations\Blog\PostRepository(new \Libraries\Models\Blog\Post());
    });
    
    $this->app->bind('\Libraries\Cms\Services\BlogService', function()
    {
      return new \Libraries\Cms\Services\BlogService(
        $this->app->make('\Libraries\Repositories\Blog\IPostRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IRegionRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IThemeRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\BlogService', function() {
      return new \Libraries\Admin\Services\BlogService(
        $this->app->make('\Libraries\Repositories\Blog\IPostRepository'),
        $this->app->make('\Libraries\Repositories\IUserProfileRepository')
      );
    });

    $this->app->view->composer('pages.blogs.index', 'Libraries\Cms\Composers\BlogsComposer');
    $this->app->view->composer('pages.blogs.view', 'Libraries\Cms\Composers\BlogsDetailsComposer');
  }

}