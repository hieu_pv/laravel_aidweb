<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class FilterServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\Filters\IRegionRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\RegionRepository(new \Libraries\Models\Filters\Region());
    });

    $this->app->bind('\Libraries\Repositories\Filters\ICountryRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\CountryRepository(new \Libraries\Models\Filters\Country());
    });

    $this->app->bind('\Libraries\Repositories\Filters\ICrisisRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\CrisisRepository(new \Libraries\Models\Filters\Crisis());
    });

    $this->app->bind('\Libraries\Repositories\Filters\ISectorRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\SectorRepository(new \Libraries\Models\Filters\Sector());
    });

    $this->app->bind('\Libraries\Repositories\Filters\IThemeRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\ThemeRepository(new \Libraries\Models\Filters\Theme());
    });

    $this->app->bind('\Libraries\Repositories\Filters\IInterventionRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\InterventionRepository(new \Libraries\Models\Filters\Intervention());
    });

    $this->app->bind('\Libraries\Repositories\Filters\IBeneficiaryRepository', function() {
      return new \Libraries\Repositories\Implementations\Filters\BeneficiaryRepository(new \Libraries\Models\Filters\Beneficiary());
    });

    $this->app->bind('\Libraries\Cms\Services\FilterService', function() {
      // orderly
      return new \Libraries\Cms\Services\FilterService(
        $this->app->make('\Libraries\Repositories\Filters\IRegionRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ICountryRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ICrisisRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IThemeRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ISectorRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IBeneficiaryRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IInterventionRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\INationalityRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IMemberStatusRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IProfessionalStatusRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IOfficeTypeRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\IOrganisationTypeRepository'),
        $this->app->make('\Libraries\Repositories\Lookups\ILevelOfActivityRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\FilterService', function() {
      // orderly
      return new \Libraries\Admin\Services\FilterService(
        $this->app->make('\Libraries\Repositories\Filters\IRegionRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ICountryRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ICrisisRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IThemeRepository'),
        $this->app->make('\Libraries\Repositories\Filters\ISectorRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IBeneficiaryRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IInterventionRepository')
      );
    });
  }

}