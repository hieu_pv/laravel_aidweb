<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider {

  /**
   * [register description]
   * @return [type] [description]
   */
  public function register()
  {
    $this->app['settingsprovider'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\SettingsProvider;
    });

    $this->app['fileutility'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\FileUtility;
    });

    $this->app['genericutility'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\GenericUtility;
    });

    $this->app['directories'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\Directories;
    });

    $this->app['messages'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\Messages;
    });

    $this->app['permalinkengine'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\PermalinkEngine;
    });

    $this->app['modules'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\Modules;
    });

    $this->app['publicities'] = $this->app->share(function($app) 
    {
      return new \Libraries\Support\Publicities;
    });

    // // Shortcut so developers don't need to add an Alias in app/config/app.php
    // $this->app->booting(function()
    // {
    //   $loader = \Illuminate\Foundation\AliasLoader::getInstance();
    //   $loader->alias('UnderlyingClass', 'Fideloper\Example\Facades\UnderlyingClass');
    // });
  }
}