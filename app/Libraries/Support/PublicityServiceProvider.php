<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class PublicityServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\Publicities\IPublicityRepository', function() {
      return new \Libraries\Repositories\Implementations\Publicities\PublicityRepository(new \Libraries\Models\Publicities\Publicity());
    });

    $this->app->bind('\Libraries\Repositories\Publicities\IPublicityTypeRepository', function() {
      return new \Libraries\Repositories\Implementations\Publicities\PublicityTypeRepository(new \Libraries\Models\Publicities\Type());
    });
    
    $this->app->bind('\Libraries\Admin\Services\PublicitiesService', function() {
      return new \Libraries\Admin\Services\PublicitiesService(
        $this->app->make('\Libraries\Repositories\Publicities\IPublicityRepository'),
        $this->app->make('\Libraries\Repositories\Publicities\IPublicityTypeRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });
  }

}