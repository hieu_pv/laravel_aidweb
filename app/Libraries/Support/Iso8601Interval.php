<?php namespace Libraries\Support;

class Iso8601Interval {

  public $Years;
  public $Months;
  public $Days;
  public $Hours;
  public $Minutes;
  public $Seconds;

  public function __construct($years, $months, $days, $hours, $minutes, $seconds)
  {
    $this->Years = $years;
    $this->Months = $months;
    $this->Days = $days;
    $this->Hours = $hours;
    $this->Minutes = $minutes;
    $this->Seconds = $seconds;
  }

  public function toIso8601IntervalStr() 
  {
    return 'P'.$this->Years.'Y'.$this->Months.'M'.$this->Days.'DT'.$this->Hours.'H'.$this->Minutes.'M'.$this->Seconds.'S';
  }

}