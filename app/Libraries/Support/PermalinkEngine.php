<?php namespace Libraries\Support;

use Illuminate\Support\Facades\App;
use Libraries\Models\Blog\Post;
use Libraries\Models\User;
use Libraries\Support\Modules;

class PermalinkEngine {
  
  private $_postRepository;
  private $_newsRepository;
  private $_takeActionRepository;
  private $_vidoeRepository;

  const PREFIX_BLOG = 'blog-posts';
  const PREFIX_NEWS = 'news';
  const PREFIX_TAKE_ACTIONS = 'take-actions';
  const PREFIX_VIDEOS = 'videos';

  public function prefix_blog()
  {
    return self::PREFIX_BLOG;
  }

  public function prefix_news()
  {
    return self::PREFIX_NEWS;
  }

  public function prefix_takeactions()
  {
    return self::PREFIX_TAKE_ACTIONS;
  }

  public function prefix_videos()
  {
    return self::PREFIX_VIDEOS;
  }

  public function __construct()
  {
    $this->_postRepository = App::make('\Libraries\Repositories\Blog\IPostRepository');
    $this->_newsRepository = App::make('\Libraries\Repositories\News\INewsItemRepository');
    $this->_takeActionRepository = App::make('\Libraries\Repositories\TakeActions\ITakeActionRepository');
    $this->_videoRepository = App::make('\Libraries\Repositories\Videos\IVideoRepository');
  }

  /*************************************************************************
  /* 
  /* BLOG PERMALINK - http://[host]/blog/[post-title]-[post-id]
  /* NEWS PERMALINK - http://[host]/news/[post-title]-[post-id]
  /* 
  *************************************************************************/

  public function getPermalinkById($module, $id) {
    $model = null;

    switch ($module) {
      case Modules::BLOG:
        $model = $this->_postRepository->getById($id);
        break;
      case Modules::NEWS:
        $model = $this->_newsRepository->getById($id);
        break;
      case Modules::TAKE_ACTIONS:
        $model = $this->_takeActionRepository->getById($id);
        break;
      case Modules::VIDEOS:
        $model = $this->_videoRepository->getById($id);
        break;
      default:
        break;
    }
    
    return $this->getPermalink($module, $model);
  }

  public function getPermalink($module, $model)
  {
    if ($model === null) {
      return '';
    }

    $base = '';
    switch ($module) {
      case Modules::BLOG:
        $base = self::PREFIX_BLOG;
        break;
      case Modules::NEWS:
        $base = self::PREFIX_NEWS;
        break;
      case Modules::TAKE_ACTIONS:
        $base = self::PREFIX_TAKE_ACTIONS;
        break;
      case Modules::VIDEOS:
        $base = self::PREFIX_VIDEOS;
        break;
      default:
        break;
    }

    $url = '/'.$base.'/'.$model->slug.'-'.$model->id;

    return strtolower(url($url));
  }

  public function resolvePermalink($module, $permalink)
  {
    $exploded = explode('-', $permalink);
    $id = intval(end($exploded));
    
    $slug = '';
    $model = null;

    switch ($module) {
      case Modules::BLOG:
        $model = $this->_postRepository->getById($id);
        break;
      case Modules::NEWS:
        $model = $this->_newsRepository->getById($id);
        break;
      case Modules::TAKE_ACTIONS:
        $model = $this->_takeActionRepository->getById($id);
        break;
      case Modules::VIDEOS:
        $model = $this->_videoRepository->getById($id);
        break;
      default:
        break;
    }

    if ($model !== null) {
      $slug = $model->slug;
    }

    return array('id' => $id, 'slug' => $slug);
  }

  public function resolveProfileUrl($idOfProfileOrOrganisation, $memberType)
  {
    if ($memberType === Modules::PROFILE_TYPE_INDIVIDUAL) {
      return '/profile/ind/'.$idOfProfileOrOrganisation;
    } else if ($memberType === Modules::PROFILE_TYPE_ORGANISATION) {
      return '/profile/org/'.$idOfProfileOrOrganisation;
    }
    return '#';
  }

}