<?php namespace Libraries\Support;

use Libraries\Models\Publicities\Publicity;
// use Libraries\Support\Modules;
// use Libraries\Support\GenericUtility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
//use Barryvdh\Debugbar\Facade as Debugbar;

class Publicities {
  
  private $session;
  private $publicityRepository;

  public function __construct()
  {
    $this->session = Auth::getSession();
    $this->publicityRepository = App::make('\Libraries\Repositories\Publicities\IPublicityRepository');
  }

  private function getPublicity($typeId, $sessionKeyId, $sessionKeyModel)
  {
    try {
      $selectedPlatformId = \GenericUtility::selectedPlatform()->id;

      // get stored id
      $currentId = $this->session->get($sessionKeyId, -1);
      if ($currentId > 0) {
        // get the next publicity 
        $nextPub = $this->publicityRepository->getNext($typeId, $currentId, $selectedPlatformId);
        // next pub found
        if ($nextPub !== null) {
          // store to session
          $this->session->put($sessionKeyId, $nextPub->id);
          $this->session->put($sessionKeyModel, $nextPub);
        }
      } else {
        // no publicty stored, get random publicity
        $pub = $this->publicityRepository->getRandom($typeId, $selectedPlatformId);
        // random pub found
        if ($pub !== null) {
          // store the publicity in the session (possible for page refresh)
          $this->session->put($sessionKeyId, $pub->id);
          $this->session->put($sessionKeyModel, $pub);
        }
      }

      return $this->session->get($sessionKeyModel, null); // $this->getDefaultModel());
    } catch (\Exception $ex) {
      //Log::error($ex);
      //Debugbar::error($ex->getMessage());
      return null;
    }
  }

  public function getSmallPublicity() {
    return $this->getPublicity(Modules::PUBLICITY_TYPE_SMALL, 'publicity_small_current_id', 'publicity_small_current');
  }

  public function getBigPublicity() {
    return $this->getPublicity(Modules::PUBLICITY_TYPE_BIG, 'publicity_big_current_id', 'publicity_big_current');
  }

  public function getSidePublicity() {
    return $this->getPublicity(Modules::PUBLICITY_TYPE_SIDE, 'publicity_side_current_id', 'publicity_side_current');
  }

  public function clearPublicitySessions()
  {
    if ($this->session->has('publicity_small_current_id')) {
      $this->session->forget('publicity_small_current_id');
    }
    if ($this->session->has('publicity_small_current')) {
      $this->session->forget('publicity_small_current');
    }
    if ($this->session->has('publicity_big_current_id')) {
      $this->session->forget('publicity_big_current_id');
    }
    if ($this->session->has('publicity_big_current')) {
      $this->session->forget('publicity_big_current');
    }
    if ($this->session->has('publicity_side_current_id')) {
      $this->session->forget('publicity_side_current_id');
    }
    if ($this->session->has('publicity_side_current')) {
      $this->session->forget('publicity_side_current');
    }
  }

}