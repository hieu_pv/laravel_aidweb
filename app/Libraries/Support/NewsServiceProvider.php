<?php namespace Libraries\Support;

use Illuminate\Support\ServiceProvider;

class NewsServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind('\Libraries\Repositories\News\INewsItemRepository', function() {
      return new \Libraries\Repositories\Implementations\News\NewsItemRepository(new \Libraries\Models\News\NewsItem());
    });

    $this->app->bind('\Libraries\Cms\Services\NewsService', function()
    {
      return new \Libraries\Cms\Services\NewsService(
        $this->app->make('\Libraries\Repositories\News\INewsItemRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IRegionRepository'),
        $this->app->make('\Libraries\Repositories\Filters\IThemeRepository')
      );
    });

    $this->app->bind('\Libraries\Admin\Services\NewsService', function() {
      return new \Libraries\Admin\Services\NewsService(
        $this->app->make('\Libraries\Repositories\News\INewsItemRepository'),
        $this->app->make('\Libraries\Repositories\IOrganisationRepository')
      );
    });

    $this->app->view->composer('pages.news.index', 'Libraries\Cms\Composers\NewsComposer');
    $this->app->view->composer('pages.news.view', 'Libraries\Cms\Composers\NewsDetailsComposer');
  }

}