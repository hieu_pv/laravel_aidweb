<?php namespace Libraries\Repositories\Implementations;

use Libraries\Models\Settings;

class SettingsRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\ISettingsRepository {

  public function __construct(Settings $model)
  {
    $this->model = $model;
  }

}