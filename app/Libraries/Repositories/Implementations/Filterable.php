<?php namespace Libraries\Repositories\Implementations;

use Illuminate\Support\Facades\DB;
use Libraries\Facades\Modules;
use Session;

class Filterable {

  /**
   * [displayable description]
   * @param  [type] $query [description]
   * @return [type]        [description]
   */
  public function displayable($query, $tableName)
  {
    $query->where($tableName.'.published', '=', '1')
          ->where($tableName.'.deleted', '=', '0');
  }

  /**
   * [selectingLikes description]
   * @param  [type] $query [description]
   * @return [type]        [description]
   */
  public function selectingLikes($query, $tableName, $moduleId)
  {
    $query->select(DB::raw($tableName.'.*, count(likes.id) as \'num_of_likes\''))
          ->leftjoin('likes', function ($join) use ($tableName, $moduleId) {
            $join->on($tableName.'.id', '=', 'likes.item_id');
            $join->on('likes.module_id', '=', DB::raw('convert('.$moduleId.', UNSIGNED INTEGER)'));
          })
          ->groupBy($tableName.'.id');
  }

  /**
   * [fromActiveTopAgenciesLocatedAt description]
   * @param  [type] $query     [description]
   * @param  [type] $countryId [description]
   * @return [type]            [description]
   */
  public function fromActiveTopAgenciesLocatedAt($query, $countryId, $tableName)
  {
    $query->whereHas('relatedOrganisation', function ($q) use ($countryId, $tableName)
    {
      $q->where('is_active', '=', '1');
      //$q->where('office_location', '=', $countryId);
      $q->whereExists(function($query2) use ($countryId, $tableName) 
      {
      	$platform=array();
        $platform=json_decode(Session::get('selected_platform'),true);
        if($platform['id']==Modules::platform_international()){
          $query2->select(DB::raw(1))
               ->from('platforms_top_profiles')
               ->where('platforms_top_profiles.profile_type', '=', Modules::profile_type_organisation())
               ->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.organisation_id');
        }else{
          $query2->select(DB::raw(1))
               ->from('platforms_top_profiles')
               ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
               ->where('platforms.related_country_id', '=', $countryId)
               ->where('platforms_top_profiles.profile_type', '=', Modules::profile_type_organisation())
               ->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.organisation_id');
        }
        //$query2->select(DB::raw(1)) 27 agustus 2015
               //->from('platforms_top_profiles')
               //->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
               //->where('platforms.related_country_id', '=', $countryId)
               //->where('platforms_top_profiles.profile_type', '=', Modules::profile_type_organisation())
               //->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.organisation_id');
      });
    });
  }

  /**
   * [fromActiveTopAgencies description]
   * @param  [type] $query [description]
   * @return [type]        [description]
   */
  public function fromActiveTopAgencies($query, $tableName)
  {
    $query->whereHas('relatedOrganisation', function ($q) use ($tableName)
    {
      $q->where('is_active', '=', '1');
      $q->whereExists(function($query2) use ($tableName) 
      {
      	$platform=array();
        $platform=json_decode(Session::get('selected_platform'),true);
        if($platform['id']==Modules::platform_international()){
          $query2->select(DB::raw(1))
               ->from('platforms_top_profiles')
               ->where('platforms_top_profiles.profile_type', '=', Modules::profile_type_organisation())
               ->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.organisation_id');
        }else{
          $query2->select(DB::raw(1))
               ->from('platforms_top_profiles')
               ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
               ->where('platforms.related_country_id', '=', 0)
               ->where('platforms_top_profiles.profile_type', '=', Modules::profile_type_organisation())
               ->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.organisation_id');
        }
        //$query2->select(DB::raw(1))
          //     ->from('platforms_top_profiles')
            //   ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
              // ->where('platforms.related_country_id', '=', 0)
               //->where('platforms_top_profiles.profile_type', '=', Modules::profile_type_organisation())
               //->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.organisation_id');
      });
    });
  }

  /**
   * [fromActiveAgencies description]
   * @param  [type] $query [description]
   * @return [type]        [description]
   */
  public function fromActiveAgencies($query)
  {
    $query->whereHas('relatedOrganisation', function ($q)
    {
      $q->where('is_active', '=', '1');
    });
  }

  /**
   * [fromActiveAgenciesLocatedAt description]
   * @param  [type] $query     [description]
   * @param  [type] $countryId [description]
   * @return [type]            [description]
   */
  public function fromActiveAgenciesLocatedAt($query, $countryId)
  {
    $query->whereHas('relatedOrganisation', function ($q) use ($countryId)
    {
      $q->where('is_active', '=', '1');
      $q->where('office_location', '=', $countryId);
    });
  }  

  /**
   * [inRegion description]
   * @param  [type] $query    [description]
   * @param  [type] $regionId [description]
   * @return [type]           [description]
   */
  public function inRegion($query, $regionId)
  {
    if ($regionId > 0) {
      $query->whereHas('regions' , function ($q) use ($regionId) {
        $q->where('region_id', '=', $regionId);
      });
    }
  }

  /**
   * [inTheme description]
   * @param  [type] $query   [description]
   * @param  [type] $themeId [description]
   * @return [type]          [description]
   */
  public function inTheme($query, $themeId)
  {
    if ($themeId > 0) {
      $query->whereHas('themes' , function ($q) use ($themeId) {
        $q->where('theme_id', '=', $themeId);
      });
    }
  }

  /**
   * [inType description]
   * @param  [type] $query  [description]
   * @param  [type] $typeId [description]
   * @return [type]         [description]
   */
  public function inType($query, $typeId)
  {
    if ($typeId > 0) {
      $query->whereHas('typeOfTakeAction' , function ($q) use ($typeId) {
        $q->where('id', '=', $typeId);
      });
    }
  }

  /**
   * [applyFilters description]
   * @param  [type] $query   [description]
   * @param  [type] $filters [description]
   * @return [type]          [description]
   */
  public function applyFilters($query, $filters)
  {
    if (isset($filters->region)) {
      if ($filters->region > 0) {
        $query->whereHas('regions' , function ($q) use ($filters) {
          $q->where('region_id', '=', $filters->region);
        });
      }
    }

    if (isset($filters->country)) {
      if ($filters->country > 0) {
        $query->whereHas('countries' , function ($q) use ($filters) {
          $q->where('country_id', '=', $filters->country);
        });
      } 
    }

    if (isset($filters->crisis)) {
      if ($filters->crisis > 0) {
        $query->whereHas('crisis' , function ($q) use ($filters) {
          $q->where('crisis_id', '=', $filters->crisis);
        });
      }
    }

    if (isset($filters->sector)) {
      if ($filters->sector > 0) {
        $query->whereHas('sectors' , function ($q) use ($filters) {
          $q->where('sector_id', '=', $filters->sector);
        });
      }
    }
      
    if (isset($filters->intervention)) {
      if ($filters->intervention > 0) {
        $query->whereHas('interventions' , function ($q) use ($filters) {
          $q->where('intervention_id', '=', $filters->intervention);
        });
      }
    }

    if (isset($filters->theme)) {
      if ($filters->theme > 0) {
        $query->whereHas('themes' , function ($q) use ($filters) {
          $q->where('theme_id', '=', $filters->theme);
        });
      }
    }
      
    if (isset($filters->beneficiary)) {
      if ($filters->beneficiary > 0) {
        $query->whereHas('beneficiaries' , function ($q) use ($filters) {
          $q->where('beneficiary_id', '=', $filters->beneficiary);
        });
      }
    }
      
    if (isset($filters->organisationType)) {
      if ($filters->organisationType > 0) {
        $query->whereHas('relatedOrganisation' , function ($q) use ($filters) {
          $q->where('is_active', '=', '1');
          $q->where('org_type', '=', $filters->organisationType);
        });
      }
    }

    if (isset($filters->officelocation)) {
      if ($filters->officelocation > 0) {
        $query->whereHas('relatedOrganisation' , function ($q) use ($filters) {
          $q->where('is_active', '=', '1');
          $q->where('office_location', '=', $filters->officelocation);
        });
      }
    }

    if (isset($filters->nationality)) {
      if ($filters->nationality > 0) {
        $query->whereHas('relatedUserProfile' , function ($q) use ($filters) {
          $q->where('nationality', '=', $filters->nationality);
        });
      }
    }

    if (isset($filters->basedin)) {
      if ($filters->basedin > 0) {
        $query->whereHas('relatedUserProfile' , function ($q) use ($filters) {
          $q->where('based_in', '=', $filters->basedin);
        });
      }
    }

    if (isset($filters->actiontype)) {
      if ($filters->actiontype > 0) {
        $query->where('type_id', $filters->actiontype);
      }  
    }
  }

}