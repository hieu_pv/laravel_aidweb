<?php namespace Libraries\Repositories\Implementations\Videos;

use Libraries\Models\Videos\Video;
use Libraries\Models\Organisation;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;
use Libraries\Repositories\Implementations\Filterable;

class VideoRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Videos\IVideoRepository {

  private $_filterable;

  public function __construct(Video $model)
  {
    $this->model = $model;
    $this->_filterable = new Filterable();
  }

  public function deleteById($id)
  {
    $todelete = $this->model->find($id);

    $todelete->countries()->detach();
    $todelete->regions()->detach();
    $todelete->crisis()->detach();
    $todelete->sectors()->detach();
    $todelete->themes()->detach();
    $todelete->beneficiaries()->detach();
    $todelete->interventions()->detach();

    $todelete->delete();
  }

  public function getFeaturedVideos()
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'videos');
    $this->_filterable->displayable($query, 'videos');

    $query->where('featured', '=', '1')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(3);
    return $query->get();
  }

  public function getFeaturedVideosByCountry($countryId)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'videos');
    $this->_filterable->displayable($query, 'videos');

    $query->where('featured', '=', '1')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(3);

    return $query->get();
  }

  public function getMostPopularVideos()
  {
    $query = Video::where('videos.id', '>', '0')
                  ->where(DB::raw('DATEDIFF((select max(videos.created_at) from videos), videos.created_at)'), '<=', '30');

    $this->_filterable->selectingLikes($query, 'videos', Modules::VIDEOS);
    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'videos');

    $query->orderBy('num_of_likes', 'desc')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(3);

    return $query->get();
  }

  public function getMostPopularVideosInCountry($countryId)
  {
    $query = Video::where('videos.id', '>', '0')
                  ->where(DB::raw('DATEDIFF((select max(videos.created_at) from videos), videos.created_at)'), '<=', '30');

    $this->_filterable->selectingLikes($query, 'videos', Modules::VIDEOS);
    $this->_filterable->fromActiveAgenciesLocatedAt($query, $countryId);
    $this->_filterable->displayable($query, 'videos');

    $query->orderBy('num_of_likes', 'desc')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(3);

    return $query->get();
  }

  public function getTopVideos()
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'videos');
    $this->_filterable->displayable($query, 'videos');

    $query->where('featured', '=', '1')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(7);

    $data = $query->get();
    return $data;
  }

  public function getTopVideosFromCountry($countryId)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'videos');
    $this->_filterable->displayable($query, 'videos');

    $query->where('featured', '=', '1')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(7);

    $data = $query->get();
    return $data;
  }

  public function getTopVideosInRegion($regionId)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'videos');
    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->inRegion($query, $regionId);

    $query->where('featured', '=', '1')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();
    return $data;
  }

  public function getTopVideosInThemeFromCountry($themeId, $countryId)
  {
    $query = Video::where('videos.id', '>', '0');

    // requirement changes to include international orgs
    //$this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'videos');
    $query->where(function ($query1) use ($countryId)
    {
      // from agency in national platform - based on office location
      $query1->whereHas('relatedOrganisation', function ($q) use ($countryId)
      {
        $q->where('is_active', '=', '1');
        //$q->where('office_location', '=', $countryId);
        $q->whereExists(function ($query5) use ($countryId) 
        {
          $query5->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
                 ->where('platforms.related_country_id', '=', $countryId)
                 ->whereRaw('platforms_top_profiles.profile_type = 2')
                 ->whereRaw('platforms_top_profiles.profile_id = videos.organisation_id');
        });
      });
      // or from platform = int and country filter = platform country
      $query1->orWhere(function ($query2) use ($countryId) 
      {
        $query2->whereExists(function ($query3) {
          // select platform = international (platform_id = 1)
          // and profile type = organisation (profile_type = 2)
          $query3->select(DB::raw(1))
                 ->from('platforms_profiles')
                 ->whereRaw('videos.organisation_id = platforms_profiles.profile_id and platforms_profiles.profile_type = 2 and platforms_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query3) {
          $query3->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->whereRaw('videos.organisation_id = platforms_top_profiles.profile_id and platforms_top_profiles.profile_type = 2 and platforms_top_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query4) use ($countryId) {
          $query4->select(DB::raw(1))
                 ->from('videos_countries')
                 ->whereRaw('videos.id = videos_countries.video_id')
                 ->where('videos_countries.country_id', $countryId);
        });
      });
    });

    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->inTheme($query, $themeId);

    $query->where('featured', '=', '1')
          ->orderBy('videos.created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();
    return $data;
  }

  public function filterAllVideos($pageNumber, $pageSize, $filters, $sortBy, $searchQuery)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->selectingLikes($query, 'videos', Modules::VIDEOS);

    if ($sortBy === 'recent') {
      $query->orderBy('videos.created_at', 'desc');
    } else if ($sortBy === 'popularity') {
      $query->orderBy('num_of_likes', 'desc');
      $query->orderBy('videos.created_at', 'desc');
    }

    $query->skip($pageSize * ($pageNumber -1));
    $query->take($pageSize);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->get();

    return $data;
  }

  public function getFilterOptionsRecordCount($filters, $searchQuery)
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->regions = [];
    $obj->crisis = [];
    $obj->themes = [];
    $obj->sectors = [];
    $obj->beneficiaries = [];
    $obj->organisationtypes = [];

    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->fromActiveAgencies($query);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $ids = $query->lists('id');
    
    // countries
    $obj->countries = 
      DB::table('videos_countries')
      ->whereIn('video_id', $ids)
      ->select('country_id as item_id', DB::raw('count(video_id) as record_count'))
      ->groupBy('country_id')
      ->get();

    $obj->regions =  
      DB::table('videos_regions')
      ->whereIn('video_id', $ids)
      ->select('region_id as item_id', DB::raw('count(video_id) as record_count'))
      ->groupBy('region_id')
      ->get();

    $obj->crisis =  
      DB::table('videos_crisis')
      ->whereIn('video_id', $ids)
      ->select('crisis_id as item_id', DB::raw('count(video_id) as record_count'))
      ->groupBy('crisis_id')
      ->get();

    $obj->themes =  
      DB::table('videos_themes')
      ->whereIn('video_id', $ids)
      ->select('theme_id as item_id', DB::raw('count(video_id) as record_count'))
      ->groupBy('theme_id')
      ->get();

    $obj->sectors =  
      DB::table('videos_sectors')
      ->whereIn('video_id', $ids)
      ->select('sector_id as item_id', DB::raw('count(video_id) as record_count'))
      ->groupBy('sector_id')
      ->get();

    $obj->beneficiaries =  
      DB::table('videos_beneficiaries')
      ->whereIn('video_id', $ids)
      ->select('beneficiary_id as item_id', DB::raw('count(video_id) as record_count'))
      ->groupBy('beneficiary_id')
      ->get();

    $obj->organisationtypes =  
      DB::table('videos')
      ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
      ->whereIn('videos.id', $ids)
      ->select('organisations.org_type as item_id', DB::raw('count(videos.id) as record_count'))
      ->groupBy('organisations.org_type')
      ->get();
    
    //Log::info(DB::getQueryLog());

    return $obj;            
  }

  public function countFilteredVideos($filters, $searchQuery)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->fromActiveAgencies($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  public function countOrganisationsFromFilteredVideos($filters, $searchQuery)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->fromActiveAgencies($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select(DB::raw('organisation_id'))
                  ->groupBy('organisation_id')
                  ->get();

    if ($data === null) {
      return 0;
    }

    return count($data);
  }

  private function applyPlatformFilter($query, $filters)
  {
    // if national platform is selected (>1) 1 is International
    if (isset($filters->platform_id) && isset($filters->platform_country_id)) {
      if ($filters->platform_id > 1) {
        $query->where(function ($query) use ($filters) 
        {
          //platform_id = 1 = international
          //profile_type = 2 = organisation
          $sql = '
          exists 
          (
            select  1 
            from    platforms_profiles 
            where   platforms_profiles.profile_id = videos.organisation_id 
            and     platforms_profiles.profile_type = 2 
            and     platforms_profiles.platform_id = '.$filters->platform_id.'
          )
          or
          (
            exists
            (
              select  1
              from    platforms_profiles 
              where   platforms_profiles.profile_id = videos.organisation_id 
              and     platforms_profiles.profile_type = 2
              and     platforms_profiles.platform_id = 1
            )
            and
            exists
            (
              select  1
              from    videos_countries
              where   videos_countries.video_id = videos.id
              and     videos_countries.country_id = '.$filters->platform_country_id.'
            )
          )';
          $query->whereRaw($sql);
        });
      }
    }
  }

  public function getVideosByOrganisationId($organisationId, $pageNumber, $pageSize)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'videos');
    $this->_filterable->selectingLikes($query, 'videos', Modules::VIDEOS);

    $query->where('videos.organisation_id', '=', $organisationId);

    $toSkip = $pageSize * ($pageNumber - 1);
    $query->orderBy('videos.created_at', 'desc')
          ->skip($toSkip)
          ->take($pageSize);

    $data = $query->get();
    return $data;
  }

  public function countVideosByOrganisationId($organisationId)
  {
    $query = Video::where('videos.id', '>', '0');

    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'videos');

    $query->where('videos.organisation_id', '=', $organisationId);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  /*==========================================================================================*/

  private function applySearchQuery($query, $searchQuery)
  {
    if ($searchQuery !== null && !is_null($searchQuery) && isset($searchQuery) && gettype($searchQuery) === 'string' && strlen($searchQuery) > 0) {
      $query->where(function ($iquery) use ($searchQuery) {
        return $iquery->where('title', 'like', '%'.$searchQuery.'%')
                      ->orwhere('author', 'like', '%'.$searchQuery.'%');
      });
    }
  }

}
