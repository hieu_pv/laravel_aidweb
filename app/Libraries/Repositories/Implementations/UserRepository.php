<?php namespace Libraries\Repositories\Implementations;

use Libraries\Models\User;
use Libraries\Models\UserProfile;

class UserRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\IUserRepository {

  public function __construct(User $model)
  {
    $this->model = $model;
  }

}