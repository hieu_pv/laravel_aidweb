<?php namespace Libraries\Repositories\Implementations\Publicities;

use Illuminate\Support\Facades\DB;
use Libraries\Models\Publicities\Publicity;
use Libraries\Models\Organisation;
//use Libraries\Support\Modules;

class PublicityRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Publicities\IPublicityRepository {

  public function __construct(Publicity $model)
  {
    $this->model = $model;
  }

  public function deleteById($id)
  {
    $todelete = $this->model->find($id);
    
    $todelete->delete();
  }

  private function baseQuery($query)
  {
    $query->select(
      'publicities.id',
      'publicities.name',
      'publicities.a_title',
      'publicities.a_title_color',
      'publicities.a_text',
      'publicities.a_text_color',
      'publicities.a_button_text',
      'publicities.a_button_text_color',
      'publicities.a_position',
      'publicities.a_image',
      'publicities.a_url',
      'publicities.a_show_org_logo',
      'publicities.b_title',
      'publicities.b_image',
      'publicities.b_url',
      'publicities.c_title',
      'publicities.c_image',
      'publicities.c_url',
      'publicities.type_id',
      'publicities.published',
      'publicities.deleted',
      'publicities.featured',
      'publicities.created_at',
      'publicities.updated_at',
      'publicities.published_at',
      'publicities.organisation_id',
      'publicities.user_profile_id',
      'publicities.user_id',
      'publicities.published_start_date',
      'publicities.published_interval',
      DB::raw('if(type_id = 1, a_title, if(type_id = 2, b_title, if(type_id = 3, c_title, ""))) as title'),
      DB::raw('if(type_id = 1, a_image, if(type_id = 2, b_image, if(type_id = 3, c_image, ""))) as image'),
      DB::raw('if(type_id = 1, a_url, if(type_id = 2, b_url, if(type_id = 3, c_url, ""))) as url')
    );
  }

  public function getById($id, array $relations = null)
  {
    $query = $this->model->newQuery();

    $this->baseQuery($query);

    $this->loadRelations($query, $relations);

    return $query->find($id);
  }

  public function getPaged(array $conditions = null, array $orderByColumns = null, $pageNumber, $pageSize, array $relations = null)
  {
    $query = $this->model->newQuery();
    
    $this->baseQuery($query);

    $this->applyConditions($query, $conditions);

    $this->applySort($query, $orderByColumns);

    $this->loadRelations($query, $relations);

    $toSkip = $pageSize * ($pageNumber - 1);
    $data = $query->skip($toSkip)->take($pageSize)->get();

    return $data;
  }

  public function getSingle(array $conditions = null, array $relations = null)
  {
    $query = $this->model->newQuery();

    $this->baseQuery($query);

    $this->applyConditions($query, $conditions);

    $this->loadRelations($query, $relations);

    return $query->first();
  }

  public function getNext($typeId, $currentId, $platformId)
  {
    $allPlatformsId = \Modules::all_platforms(); 
    $platforms = DB::table('publicities_platforms')->where('platform_id', $platformId)
                                                   ->orWhere('platform_id', $allPlatformsId)
                                                   ->lists('publicity_id');

    $query = Publicity::where('id', '>', '0')
                      ->where('id', '>', $currentId)
                      ->where('type_id', $typeId)
                      ->where('published', '1')
                      ->where('deleted', '0')
                      ->whereIn('id', $platforms)
                      ->whereRaw('DATE(NOW()) >= published_start_date AND DATE(NOW()) < date_add(published_start_date, interval (published_interval_in_seconds/3600/24/30) month)')
                      ->orderBy('id', 'asc');
    
    $this->baseQuery($query);

    $data = $query->first();

    // not found, get the first one
    if ($data === null) {
      $query2 = Publicity::where('id', '>', '0')
                         ->where('type_id', $typeId)
                         ->where('published', '1')
                         ->where('deleted', '0')
                         ->orderBy('id', 'asc');
      
      $this->baseQuery($query2);

      $data2 = $query2->first();

      return $data2;
    } else {
      return $data;
    }
  }

  public function getRandom($typeId, $platformId)
  {
    $allPlatformsId = \Modules::all_platforms(); 
    $platforms = DB::table('publicities_platforms')->where('platform_id', $platformId)
                                                   ->orWhere('platform_id', $allPlatformsId)
                                                   ->lists('publicity_id');

    $query = DB::table('publicities')->where('id', '>', '0')
                                     ->where('type_id', $typeId)
                                     ->where('published', '1')
                                     ->where('deleted', '0')
                                     ->whereIn('id', $platforms)
                                     ->whereRaw('DATE(NOW()) >= published_start_date AND DATE(NOW()) < date_add(published_start_date, interval (published_interval_in_seconds/3600/24/30) month)')
                                     ->orderBy('id', 'asc')
                                     ->lists('id');

    $data = $query;
    $randomKey = array_rand($data);

    if (array_key_exists($randomKey, $data) && !empty($data[$randomKey])) {
      $id = $data[$randomKey]; //->id;
      $result = $this->getById($id);
      return $result;
    } else {

    }

    return null;
  }

}