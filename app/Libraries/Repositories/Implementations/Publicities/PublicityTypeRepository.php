<?php namespace Libraries\Repositories\Implementations\Publicities;

use Libraries\Models\Publicities\Type;
use Libraries\Models\Organisation;

class PublicityTypeRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Publicities\IPublicityTypeRepository {

  public function __construct(Type $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = Type::lists('name', 'id');
    $results[0] = '--- No Type';

    asort($results);

    return $results;
  }

}