<?php namespace Libraries\Repositories\Implementations;

class FilterRecordCounterForOrganisations {

  public function recordCountForOrganisationsFilters()
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->levelofactivities = [];
    $obj->organisationtypes = [];

    $obj->countries = DB::table('organisations')
                        ->join('users', 'users.id', '=', 'organisations.cp_id')
                        ->where('organisations.is_active', '1')
                        ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
                        ->where('users.verified', '1')
                        ->where('users.is_new', '0')
                        ->select('office_location as item_id', DB::raw('count(organisations.id) as record_count'))
                        ->groupBy('office_location')
                        ->get();

    $obj->organisationtypes = DB::table('organisations')
                                ->join('users', 'users.id', '=', 'organisations.cp_id')
                                ->where('organisations.is_active', '1')
                                ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
                                ->where('users.verified', '1')
                                ->where('users.is_new', '0')
                                ->select('org_type as item_id', DB::raw('count(organisations.id) as record_count'))
                                ->groupBy('org_type')
                                ->get();

    $obj->levelofactivities = DB::table('organisations')
                                ->join('users', 'users.id', '=', 'organisations.cp_id')
                                ->where('organisations.is_active', '1')
                                ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
                                ->where('users.verified', '1')
                                ->where('users.is_new', '0')
                                ->select('level_of_activity as item_id', DB::raw('count(organisations.id) as record_count'))
                                ->groupBy('level_of_activity')
                                ->get();

    return $obj;
  }

}