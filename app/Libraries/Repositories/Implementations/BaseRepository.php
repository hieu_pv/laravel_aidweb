<?php namespace Libraries\Repositories\Implementations;

use Illuminate\Support\Facades\DB;
use Libraries\Support\Modules;

abstract class BaseRepository implements \Libraries\Repositories\IBaseRepository {

  protected $model;

  /**
   * [getAll description]
   * @return [type] [description]
   */
  public function getAll()
  {
    return $this->model->all();
  }

  /**
   * [getNotDeleted description]
   * @return [type] [description]
   */
  public function getNotDeleted()
  {
    try {
      return $this->model->where('deleted', '0')->get();
    } catch (\Exception $ex) {
      return $this->model->get();
    }
  }

  /**
   * [getById description]
   * @param  [type]
   * @param  array|null
   * @return [type]
   */
  public function getById($id, array $relations = null)
  {
    $query = $this->model->newQuery();

    $this->loadRelations($query, $relations);

    return $query->find($id);
  }

  /**
   * [getPaged description]
   * @param  array|null $conditions     [description]
   * @param  array|null $orderByColumns [description]
   * @param  [type]     $pageNumber     [description]
   * @param  [type]     $pageSize       [description]
   * @param  array|null $relations      [description]
   * @return [type]                     [description]
   */
  public function getPaged(array $conditions = null, array $orderByColumns = null, $pageNumber, $pageSize, array $relations = null)
  {
    $query = $this->model->newQuery();

    $this->applyConditions($query, $conditions);

    $this->applySort($query, $orderByColumns);

    $this->loadRelations($query, $relations);

    $toSkip = $pageSize * ($pageNumber - 1);
    $data = $query->skip($toSkip)->take($pageSize)->get();
    // if(isset($conditions) && !is_null($conditions) && count($conditions) > 0)
    // {
    // }
    return $data;
  }

  /**
   * [getCount description]
   * @param  array|null $conditions [description]
   * @return [type]                 [description]
   */
  public function getCount(array $conditions = null)
  {
    $query = $this->model->newQuery();

    $this->applyConditions($query, $conditions);

    $data = $query->count();
    return $data;
  }

  /**
   * [loadRelations description]
   * @param  [type]
   * @param  array|null
   * @return [type]
   */
  protected function loadRelations($query, array $relations = null)
  {
    if(isset($relations) && !is_null($relations))
    {
      foreach($relations as $relation)
      {
        $query->with($relation);
      }
    }
  }

  /**
   * [applyConditions description]
   * @param  [type]     $query      [description]
   * @param  array|null $conditions [description]
   * @return [type]                 [description]
   */
  protected function applyConditions($query, array $conditions = null)
  {
    if(isset($conditions) && !is_null($conditions))
    {
      foreach($conditions as $condition)
      {
        $conditionType = gettype($condition);
        if ($conditionType === 'array') {
          $query->where($condition["column"], $condition["operator"], $condition["value"]); 
        } else if ($conditionType === 'object') {
          $query->where($condition);
        }
      }
    }
  }

  /**
   * [applySort description]
   * @param  [type] $query          [description]
   * @param  array  $orderByColumns [description]
   * @return [type]                 [description]
   */
  protected function applySort($query, array $orderByColumns = null)
  {
    if(isset($orderByColumns) && !is_null($orderByColumns))
    {
      foreach($orderByColumns as $orderByColumn)
      {
        $query->orderBy($orderByColumn["column"], $orderByColumn["direction"]);
      }
    }
  }

  /**
   * [getSingle description]
   * @param  array|null $conditions [description]
   * @param  array|null $relations  [description]
   * @return [type]                 [description]
   */
  public function getSingle(array $conditions = null, array $relations = null)
  {
    $query = $this->model->newQuery();

    $this->applyConditions($query, $conditions);

    $this->loadRelations($query, $relations);

    return $query->first();
  }

  /**
   * [deleteById description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteById($id)
  {
    $model = $this->model->find($id);
    
    $model->delete();
  }

  /**
   * [lastPostByOrg description]
   * @param  [type] $orgId [description]
   * @return [type]        [description]
   */
  public function lastPostByOrg($orgId) 
  {
    $data = $this->model
                 ->where('organisation_id', $orgId)
                 ->orderBy('created_at', 'desc')
                 ->take(1)
                 ->get();

    $result = null;
    if ($data !== null && !empty($data) && count($data) > 0) {
      $result = $data[0];
    }
    return $result;
  }

  /**
   * [lastPostByInd description]
   * @param  [type] $userProfileId [description]
   * @return [type]                [description]
   */
  public function lastPostByInd($userProfileId)
  {
    $data = $this->model
                 ->where('user_profile_id', $userProfileId)
                 ->orderBy('created_at', 'desc')
                 ->take(1)
                 ->get();

    $result = null;
    if ($data !== null && !empty($data) && count($data) > 0) {
      $result = $data[0];
    }
    return $result;
  }

  // public function recordCountInFilters($moduleId)
  // {
  //   $o = new FilterRecordCounter();
  //   return $o->recordCountInFilters();
  // }

  public function recordCountForOrganisationsFilters()
  {
    $o = new FilterRecordCounterForOrganisations();
    return $o->recordCountForOrganisationsFilters();
  }

  public function recordCountForIndividualsFilters()
  {
    $o = new FilterRecordCounterForIndividuals();
    return $o->recordCountForIndividualsFilters();
  }

}