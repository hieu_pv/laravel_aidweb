<?php namespace Libraries\Repositories\Implementations\Lookups;

use Libraries\Models\Lookups\Nationality;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class NationalityRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\INationalityRepository {

  public function __construct(Nationality $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = Nationality::lists('name', 'id');
    $results[0] = '--- No Nationality';

    asort($results);

    return $results;
  }

  public function getWhereHasPostings($moduleId)
  {
    $tableName = '';

    if ((int)$moduleId === Modules::BLOG) {
      $ids = DB::table('blog_posts')
               ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
               ->where('user_profiles.is_active', '1')
               ->where('blog_posts.deleted', '0')
               ->where('blog_posts.published', '1')
               ->distinct('nationality')
               ->lists('nationality');
      $records = Nationality::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    return array();
  }

  public function getWhereHasIndividuals()
  {
    $ids = DB::table('user_profiles')
             ->join('users', 'users.id', '=', 'user_profiles.user_id')
             ->where('user_profiles.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('nationality')
             ->lists('nationality');
    $records = Nationality::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

}