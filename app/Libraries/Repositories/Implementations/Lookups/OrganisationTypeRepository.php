<?php namespace Libraries\Repositories\Implementations\Lookups;

use Libraries\Models\Lookups\OrganisationType;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class OrganisationTypeRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\IOrganisationTypeRepository {

  public function __construct(OrganisationType $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = OrganisationType::lists('name', 'id');
    $results[0] = '--- No Organisation Type';

    asort($results);

    return $results;
  }

  public function getWhereHasPostings($moduleId)
  {
    if ((int)$moduleId === Modules::NEWS) {
      $ids = DB::table('news_items')
               ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('news_items.deleted', '0')
               ->where('news_items.published', '1')
               ->distinct('org_type')
               ->lists('org_type');
      $records = OrganisationType::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::VIDEOS) {
      $ids = DB::table('videos')
               ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('videos.deleted', '0')
               ->where('videos.published', '1')
               ->distinct('org_type')
               ->lists('org_type');
      $records = OrganisationType::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::TAKE_ACTIONS) {
      $ids = DB::table('take_actions')
               ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('take_actions.deleted', '0')
               ->where('take_actions.published', '1')
               ->distinct('org_type')
               ->lists('org_type');
      $records = OrganisationType::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    return array();
  }

  public function getWhereHasOrganisations()
  {
    $ids = DB::table('organisations')
             ->join('users', 'users.id', '=', 'organisations.cp_id')
             ->where('organisations.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('org_type')
             ->lists('org_type');
    $records = OrganisationType::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

  public function getWhereHasIndividuals()
  {
    $ids = DB::table('user_profiles')
             ->join('users', 'users.id', '=', 'user_profiles.user_id')
             ->where('user_profiles.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('employer_organisation_type')
             ->lists('employer_organisation_type');
    $records = OrganisationType::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

}