<?php namespace Libraries\Repositories\Implementations\Lookups;

use Libraries\Models\Lookups\LevelOfActivity;
use Illuminate\Support\Facades\DB;
use Libraries\Support\Modules;

class LevelOfActivityRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\ILevelOfActivityRepository {

  public function __construct(LevelOfActivity $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = LevelOfActivity::lists('name', 'id');
    $results[0] = '--- No Level of Activity';

    //asort($results);

    return $results;
  }

  public function getWhereHasOrganisations()
  {
    $ids = DB::table('organisations')
             ->join('users', 'users.id', '=', 'organisations.cp_id')
             ->where('organisations.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('level_of_activity')
             ->lists('level_of_activity');
    $records = LevelOfActivity::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

}