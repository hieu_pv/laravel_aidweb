<?php namespace Libraries\Repositories\Implementations\Lookups;

use Illuminate\Support\Facades\DB;
use Libraries\Models\Lookups\ProfessionalStatus;
use Libraries\Support\Modules;

class ProfessionalStatusRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\IProfessionalStatusRepository {

  public function __construct(ProfessionalStatus $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = ProfessionalStatus::lists('name', 'id');
    $results[0] = '--- No Professional Status';

    asort($results);

    return $results;
  }

  public function getWhereHasIndividuals()
  {
    $ids = DB::table('user_profiles')
             ->join('users', 'users.id', '=', 'user_profiles.user_id')
             ->where('user_profiles.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('professional_status')
             ->lists('professional_status');
    $records = ProfessionalStatus::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

}