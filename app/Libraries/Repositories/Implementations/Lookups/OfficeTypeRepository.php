<?php namespace Libraries\Repositories\Implementations\Lookups;

use Libraries\Models\Lookups\OfficeType;

class OfficeTypeRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\IOfficeTypeRepository {

  public function __construct(OfficeType $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = OfficeType::lists('name', 'id');
    $results[0] = '--- No Office Type';

    asort($results);

    return $results;
  }

}