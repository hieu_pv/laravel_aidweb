<?php namespace Libraries\Repositories\Implementations\Lookups;

use Libraries\Models\Lookups\LevelOfResponsibility;

class LevelOfResponsibilityRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\ILevelOfResponsibilityRepository {

  public function __construct(LevelOfResponsibility $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = LevelOfResponsibility::lists('name', 'id');
    $results[0] = '--- No Level of Responsibility';

    //asort($results);

    return $results;
  }

}