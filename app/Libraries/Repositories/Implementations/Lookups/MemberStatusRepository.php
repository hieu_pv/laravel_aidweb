<?php namespace Libraries\Repositories\Implementations\Lookups;

use Illuminate\Support\Facades\DB;
use Libraries\Models\Lookups\MemberStatus;
use Libraries\Support\Modules;

class MemberStatusRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Lookups\IMemberStatusRepository {

  public function __construct(MemberStatus $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = MemberStatus::select('id', DB::raw('CONCAT(name, " - ", description) AS \'name\''))->lists('name', 'id');
    //$results = MemberStatus::lists('name', 'id');
    $results[0] = '--- No Member Status';

    asort($results);

    return $results;
  }

  public function getWhereHasIndividuals()
  {
    $ids = DB::table('user_profiles')
             ->join('users', 'users.id', '=', 'user_profiles.user_id')
             ->where('user_profiles.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('member_status')
             ->lists('member_status');
    $records = MemberStatus::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

}