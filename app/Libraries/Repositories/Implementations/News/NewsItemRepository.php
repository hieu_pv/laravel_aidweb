<?php namespace Libraries\Repositories\Implementations\News;

use Libraries\Models\News\NewsItem;
use Libraries\Models\Organisation;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Libraries\Repositories\Implementations\Filterable;

class NewsItemRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\News\INewsItemRepository {

  private $_filterable;

  /**
   * [__construct description]
   * @param NewsItem $model [description]
   */
  public function __construct(NewsItem $model)
  {
    $this->model = $model;
    $this->_filterable = new Filterable();
  }

  /**
   * [deleteById description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteById($id)
  {
    $todelete = $this->model->find($id);

    $todelete->countries()->detach();
    $todelete->regions()->detach();
    $todelete->crisis()->detach();
    $todelete->sectors()->detach();
    $todelete->themes()->detach();
    $todelete->beneficiaries()->detach();
    $todelete->interventions()->detach();

    $todelete->delete();
  }

  public function getByIdAndSlug($id, $slug)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->selectingLikes($query, 'news_items', Modules::NEWS);

    $query->where('news_items.slug', '=', $slug)
          ->where('news_items.id', '=', $id);

    $d = $query->first();
    return $d;
  }

  public function getLatestNews()
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'news_items');

    $query->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(10);

    $data = $query->get();
    return $data;
  }

  public function getLatestNewsFromCountry($countryId)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveAgenciesLocatedAt($query, $countryId);
    $this->_filterable->displayable($query, 'news_items');

    $query->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(10);

    return $query->get();
  }

  public function getMostPopularNews()
  {
    $query = NewsItem::where('news_items.id', '>', '0')
                     ->where(DB::raw('DATEDIFF((select max(news_items.created_at) from news_items), news_items.created_at)'), '<=', '7');

    $this->_filterable->selectingLikes($query, 'news_items', Modules::NEWS);
    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'news_items');

    $query->orderBy('num_of_likes', 'desc')
          ->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(3);

    $data = $query->get();
    return $data;
  }

  public function getMostPopularNewsFromCountry($countryId)
  {
    $query = NewsItem::where('news_items.id', '>', '0')
                     ->where(DB::raw('DATEDIFF((select max(news_items.created_at) from news_items), news_items.created_at)'), '<=', '7');

    $this->_filterable->selectingLikes($query, 'news_items', Modules::NEWS);
    $this->_filterable->fromActiveAgenciesLocatedAt($query, $countryId);
    $this->_filterable->displayable($query, 'news_items');

    $query->orderBy('num_of_likes', 'desc')
          ->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(3);

    $data = $query->get();
    return $data;
  }

  public function getTopNews()
  {
    // get top news from international platform
    // TODO: please check the correct logic for getting the top news
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'news_items');
    $this->_filterable->displayable($query, 'news_items');

    $query->where('featured', '=', '1')
          ->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(7);

    $data = $query->get();
    return $data;
  }

  public function getTopNewsFromCountry($countryId)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    // requirement alteration, also display news from INTERNATIONAL orgs with country = platform country
    //$this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'news_items');
    $query->where(function ($query1) use ($countryId)
    {
      // get from office location / platform national
      $query1->whereHas('relatedOrganisation', function ($q) use ($countryId)
      {
        $q->where('is_active', '=', '1');
        //$q->where('office_location', '=', $countryId);
        $q->whereExists(function ($query5) use ($countryId) 
        {
          $query5->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
                 ->where('platforms.related_country_id', '=', $countryId)
                 ->whereRaw('platforms_top_profiles.profile_type = 2')
                 ->whereRaw('platforms_top_profiles.profile_id = news_items.organisation_id');
        });
      });
      // or from the international platform with country filter = platform country
      $query1->orWhere(function ($query2) use ($countryId)
      {
        $query2->whereExists(function ($query3) {
          // select platform = international (platform_id = 1)
          // and profile type = organisation (profile_type = 2)
          $query3->select(DB::raw(1))
                 ->from('platforms_profiles')
                 ->whereRaw('news_items.organisation_id = platforms_profiles.profile_id and platforms_profiles.profile_type = 2 and platforms_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query4) use ($countryId) {
          $query4->select(DB::raw(1))
                 ->from('news_items_countries')
                 ->whereRaw('news_items.id = news_items_countries.newsitem_id')
                 ->where('news_items_countries.country_id', $countryId);
        });
      });
    });
    
    $this->_filterable->displayable($query, 'news_items');

    $query->where('featured', '=', '1')
          ->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(7);

    $data = $query->get();

    return $data;
  }

  public function getTopNewsInRegion($regionId)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'news_items');
    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->inRegion($query, $regionId);

    $query->where('featured', '=', '1')
          ->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();

    return $data;
  }

  public function getTopNewsInThemeFromCountry($themeId, $countryId)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    // requirement alteration, also display news from INTERNATIONAL orgs with country = platform country
    //$this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'news_items');
    $query->where(function ($query1) use ($countryId)
    {
      // from agency in national platform - based on office location
      $query1->whereHas('relatedOrganisation', function ($q) use ($countryId)
      {
        $q->where('is_active', '=', '1');
        //$q->where('office_location', '=', $countryId);
        $q->whereExists(function ($query5) use ($countryId) 
        {
          $query5->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
                 ->where('platforms.related_country_id', '=', $countryId)
                 ->whereRaw('platforms_top_profiles.profile_type = 2')
                 ->whereRaw('platforms_top_profiles.profile_id = news_items.organisation_id');
        });
      });
      // or from platform = int and country filter = platform country
      $query1->orWhere(function ($query2) use ($countryId) 
      {
        $query2->whereExists(function ($query3) {
          // select platform = international (platform_id = 1)
          // and profile type = organisation (profile_type = 2)
          $query3->select(DB::raw(1))
                 ->from('platforms_profiles')
                 ->whereRaw('news_items.organisation_id = platforms_profiles.profile_id and platforms_profiles.profile_type = 2 and platforms_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query3) {
          $query3->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->whereRaw('news_items.organisation_id = platforms_top_profiles.profile_id and platforms_top_profiles.profile_type = 2 and platforms_top_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query4) use ($countryId) {
          $query4->select(DB::raw(1))
                 ->from('news_items_countries')
                 ->whereRaw('news_items.id = news_items_countries.newsitem_id')
                 ->where('news_items_countries.country_id', $countryId);
        });
      });
    });

    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->inTheme($query, $themeId);

    $query->where('featured', '=', '1')
          ->orderBy('news_items.created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();

    return $data;
  }

  public function filterAllNews($pageNumber, $pageSize, $filters, $sortBy, $searchQuery)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->selectingLikes($query, 'news_items', Modules::NEWS);

    if ($sortBy === 'recent') {
      $query->orderBy('news_items.created_at', 'desc');
    } else if ($sortBy === 'popularity') {
      $query->orderBy('num_of_likes', 'desc');
      $query->orderBy('news_items.created_at', 'desc');
    }

    $query->skip($pageSize * ($pageNumber -1));
    $query->take($pageSize);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->get();
    //Log::info(DB::getQueryLog());

    return $data;
  }

  public function getFilterOptionsRecordCount($filters, $searchQuery)
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->regions = [];
    $obj->crisis = [];
    $obj->themes = [];
    $obj->sectors = [];
    $obj->beneficiaries = [];
    $obj->organisationtypes = [];

    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->fromActiveAgencies($query);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);
    $this->applyPlatformFilter($query, $filters);

    $ids = $query->lists('id');
    
    // countries
    $obj->countries = 
      DB::table('news_items_countries')
      ->whereIn('newsitem_id', $ids)
      ->select('country_id as item_id', DB::raw('count(newsitem_id) as record_count'))
      ->groupBy('country_id')
      ->get();

    $obj->regions =  
      DB::table('news_items_regions')
      ->whereIn('newsitem_id', $ids)
      ->select('region_id as item_id', DB::raw('count(newsitem_id) as record_count'))
      ->groupBy('region_id')
      ->get();

    $obj->crisis =  
      DB::table('news_items_crisis')
      ->whereIn('newsitem_id', $ids)
      ->select('crisis_id as item_id', DB::raw('count(newsitem_id) as record_count'))
      ->groupBy('crisis_id')
      ->get();

    $obj->themes =  
      DB::table('news_items_themes')
      ->whereIn('newsitem_id', $ids)
      ->select('theme_id as item_id', DB::raw('count(newsitem_id) as record_count'))
      ->groupBy('theme_id')
      ->get();

    $obj->sectors =  
      DB::table('news_items_sectors')
      ->whereIn('newsitem_id', $ids)
      ->select('sector_id as item_id', DB::raw('count(newsitem_id) as record_count'))
      ->groupBy('sector_id')
      ->get();

    $obj->beneficiaries =  
      DB::table('news_items_beneficiaries')
      ->whereIn('newsitem_id', $ids)
      ->select('beneficiary_id as item_id', DB::raw('count(newsitem_id) as record_count'))
      ->groupBy('beneficiary_id')
      ->get();

    $obj->organisationtypes =  
      DB::table('news_items')
      ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
      ->whereIn('news_items.id', $ids)
      ->select('organisations.org_type as item_id', DB::raw('count(news_items.id) as record_count'))
      ->groupBy('organisations.org_type')
      ->get();
    
    //Log::info(DB::getQueryLog());

    return $obj;            
  }

  public function countFilteredNews($filters, $searchQuery)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->fromActiveAgencies($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  public function countOrganisationsFromFilteredNews($filters, $searchQuery)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->fromActiveAgencies($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select(DB::raw('organisation_id'))
                  ->groupBy('organisation_id')
                  ->get();

    if ($data === null) {
      return 0;
    }

    return count($data);
  }

  private function applyPlatformFilter($query, $filters)
  {
    // if national platform is selected (>1) 1 is International
    if (isset($filters->platform_id) && isset($filters->platform_country_id)) {
      if ($filters->platform_id > 1) {
        $query->where(function ($query) use ($filters) 
        {
          //platform_id = 1 = international
          //profile_type = 2 = organisation
          $sql = '
          exists 
          (
            select  1 
            from    platforms_profiles 
            where   platforms_profiles.profile_id = news_items.organisation_id 
            and     platforms_profiles.profile_type = 2 
            and     platforms_profiles.platform_id = '.$filters->platform_id.'
          )
          or
          (
            exists
            (
              select  1
              from    platforms_profiles 
              where   platforms_profiles.profile_id = news_items.organisation_id 
              and     platforms_profiles.profile_type = 2
              and     platforms_profiles.platform_id = 1
            )
            and
            exists
            (
              select  1
              from    news_items_countries
              where   news_items_countries.newsitem_id = news_items.id
              and     news_items_countries.country_id = '.$filters->platform_country_id.'
            )
          )';
          $query->whereRaw($sql);
        });
      }
    }
  }

  public function getNewsByOrganisationId($organisationId, $pageNumber, $pageSize)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'news_items');
    $this->_filterable->selectingLikes($query, 'news_items', Modules::NEWS);

    $query->where('news_items.organisation_id', '=', $organisationId);

    $toSkip = $pageSize * ($pageNumber - 1);
    $query->orderBy('news_items.created_at', 'desc')
          ->skip($toSkip)
          ->take($pageSize);

    $data = $query->get();
    return $data;
  }

  public function countNewsByOrganisationId($organisationId)
  {
    $query = NewsItem::where('news_items.id', '>', '0');

    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'news_items');

    $query->where('news_items.organisation_id', '=', $organisationId);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  /*==========================================================================================*/

  private function applySearchQuery($query, $searchQuery)
  {
    if ($searchQuery !== null && !is_null($searchQuery) && isset($searchQuery) && gettype($searchQuery) === 'string' && strlen($searchQuery) > 0) {
      $query->where(function ($iquery) use ($searchQuery) {
        return $iquery->where('title', 'like', '%'.$searchQuery.'%')
                      ->orwhere('content', 'like', '%'.$searchQuery.'%')
                      ->orwhere('image_legend', 'like', '%'.$searchQuery.'%');
      });
    }
  }

}
