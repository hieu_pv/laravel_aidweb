<?php namespace Libraries\Repositories\Implementations\Filters;

use Libraries\Models\Filters\Country;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class CountryRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Filters\ICountryRepository {

  public function __construct(Country $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = Country::lists('name', 'id');
    $results[0] = '--- No Country';

    asort($results);

    return $results;
  }

  public function getWhereHasPostings($moduleId)
  {
    if ((int)$moduleId === Modules::BLOG) {
      $ids = DB::table('blog_posts_countries')
               ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_countries.post_id')
               ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
               ->where('user_profiles.is_active', '1')
               ->where('blog_posts.deleted', '0')
               ->where('blog_posts.published', '1')
               ->distinct('country_id')
               ->lists('country_id');
      $records = Country::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::NEWS) {
      $ids = DB::table('news_items_countries')
               ->join('news_items', 'news_items.id', '=', 'news_items_countries.newsitem_id')
               ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('news_items.deleted', '0')
               ->where('news_items.published', '1')
               ->distinct('country_id')
               ->lists('country_id');
      $records = Country::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::TAKE_ACTIONS) {
      $ids = DB::table('take_actions_countries')
               ->join('take_actions', 'take_actions.id', '=', 'take_actions_countries.takeaction_id')
               ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('take_actions.deleted', '0')
               ->where('take_actions.published', '1')
               ->distinct('country_id')
               ->lists('country_id');
      $records = Country::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::VIDEOS) {
      $ids = DB::table('videos_countries')
               ->join('videos', 'videos.id', '=', 'videos_countries.video_id')
               ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('videos.deleted', '0')
               ->where('videos.published', '1')
               ->distinct('country_id')
               ->lists('country_id');
      $records = Country::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    return array();
  }

  public function getWhereHasOrganisations()
  {
    $ids = DB::table('organisations')
             ->join('users', 'users.id', '=', 'organisations.cp_id')
             ->where('organisations.is_active', '1')
             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
             ->where('users.verified', '1')
             ->where('users.is_new', '0')
             ->distinct('office_location')
             ->lists('office_location');
    $records = Country::whereIn('id', $ids)->get()->toArray();
    if ($records === null) {
      $records = [];
    }
    return $records;
  }

  // public function getWhereHasIndividuals()
  // {
  //   $ids = DB::table('user_profiles')
  //            ->join('users', 'users.id', '=', 'user_profiles.user_id')
  //            ->where('user_profiles.is_active', '1')
  //            ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
  //            ->where('users.verified', '1')
  //            ->where('users.is_new', '0')
  //            ->distinct('nationality')
  //            ->lists('nationality');
  //   $records = Country::whereIn('id', $ids)->get()->toArray();
  //   if ($records === null) {
  //     $records = [];
  //   }
  //   return $records;
  // }

}