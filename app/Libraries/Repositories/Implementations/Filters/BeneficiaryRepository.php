<?php namespace Libraries\Repositories\Implementations\Filters;

use Libraries\Models\Filters\Beneficiary;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class BeneficiaryRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Filters\IBeneficiaryRepository {

  public function __construct(Beneficiary $model)
  {
    $this->model = $model;
  }

  public function getAllNotDeleted()
  {
    $results = Beneficiary::where('deleted', '=', '0')->get()->toArray();
    asort($results);
    return $results;
  }

  public function getAsList() 
  {
    $results = Beneficiary::where('deleted', '=', '0')->lists('name', 'id');
    $results[0] = '--- No Beneficiary';
    asort($results);
    return $results;
  }

  public function getWhereHasPostings($moduleId)
  {
    if ((int)$moduleId === Modules::BLOG) {
      $ids = DB::table('blog_posts_beneficiaries')
               ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_beneficiaries.post_id')
               ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
               ->where('user_profiles.is_active', '1')
               ->where('blog_posts.deleted', '0')
               ->where('blog_posts.published', '1')
               ->distinct('beneficiary_id')
               ->lists('beneficiary_id');
      $records = Beneficiary::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::NEWS) {
      $ids = DB::table('news_items_beneficiaries')
               ->join('news_items', 'news_items.id', '=', 'news_items_beneficiaries.newsitem_id')
               ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('news_items.deleted', '0')
               ->where('news_items.published', '1')
               ->distinct('beneficiary_id')
               ->lists('beneficiary_id');
      $records = Beneficiary::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::TAKE_ACTIONS) {
      $ids = DB::table('take_actions_beneficiaries')
               ->join('take_actions', 'take_actions.id', '=', 'take_actions_beneficiaries.takeaction_id')
               ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('take_actions.deleted', '0')
               ->where('take_actions.published', '1')
               ->distinct('beneficiary_id')
               ->lists('beneficiary_id');
      $records = Beneficiary::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::VIDEOS) {
      $ids = DB::table('videos_beneficiaries')
               ->join('videos', 'videos.id', '=', 'videos_beneficiaries.video_id')
               ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('videos.deleted', '0')
               ->distinct('beneficiary_id')
               ->lists('beneficiary_id');
      $records = Beneficiary::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    return array();
  }

}