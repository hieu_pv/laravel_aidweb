<?php namespace Libraries\Repositories\Implementations\Filters;

use Libraries\Models\Filters\Region;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class RegionRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Filters\IRegionRepository {

  public function __construct(Region $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = Region::lists('name', 'id');
    $results[0] = '--- No Region';

    asort($results);

    return $results;
  }

  public function getWhereHasPostings($moduleId)
  {
    if ((int)$moduleId === Modules::BLOG) {
      $ids = DB::table('blog_posts_regions')
               ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_regions.post_id')
               ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
               ->where('user_profiles.is_active', '1')
               ->where('blog_posts.deleted', '0')
               ->where('blog_posts.published', '1')
               ->distinct('region_id')
               ->lists('region_id');
      $records = Region::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::NEWS) {
      $ids = DB::table('news_items_regions')
               ->join('news_items', 'news_items.id', '=', 'news_items_regions.newsitem_id')
               ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('news_items.deleted', '0')
               ->where('news_items.published', '1')
               ->distinct('region_id')
               ->lists('region_id');
      $records = Region::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::TAKE_ACTIONS) {
      $ids = DB::table('take_actions_regions')
               ->join('take_actions', 'take_actions.id', '=', 'take_actions_regions.takeaction_id')
               ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('take_actions.deleted', '0')
               ->where('take_actions.published', '1')
               ->distinct('region_id')
               ->lists('region_id');
      $records = Region::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    if ((int)$moduleId === Modules::VIDEOS) {
      $ids = DB::table('videos_regions')
               ->join('videos', 'videos.id', '=', 'videos_regions.video_id')
               ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('videos.deleted', '0')
               ->where('videos.published', '1')
               ->distinct('region_id')
               ->lists('region_id');
      $records = Region::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    return array();
  }

}