<?php namespace Libraries\Repositories\Implementations;

use Libraries\Models\UserProfile;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class UserProfileRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\IUserProfileRepository {

  public function __construct(UserProfile $model)
  {
    $this->model = $model;
  }

  public function getAuthoringUsers()
  {
    $profiles = UserProfile::join('users', 'users.id', '=', 'user_profiles.user_id')
                  ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
                  ->where('user_profiles.is_active', '1')
                  ->where('users.verified', '1')
                  ->where('users.is_new', '0')
                  ->orwhere('users.profile_type', '=', Modules::PROFILE_TYPE_ADMIN)
                  ->orderBy('user_profiles.first_name', 'asc')
                  ->select('user_profiles.id','user_profiles.user_id', 'user_profiles.first_name', 'user_profiles.last_name')
                  ->get();

    return $profiles;
  }

  public function getIndividuals(array $conditions = null, array $orderByColumns = null, $pageNumber, $pageSize, array $relations = null)
  {
    $query = $this->model->newQuery();

    $query->join('users', 'users.id', '=', 'user_profiles.user_id');
    $query->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL);

    $this->applyConditions($query, $conditions);

    $this->applySort($query, $orderByColumns);

    $this->loadRelations($query, $relations);

    $toSkip = $pageSize * ($pageNumber - 1);
    $data = $query->skip($toSkip)->take($pageSize)->get();

    return $data;
  }

  public function getIndividualsCount(array $conditions = null)
  {
    $query = $this->model->newQuery();

    $query->join('users', 'users.id', '=', 'user_profiles.user_id');
    $query->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL);

    $this->applyConditions($query, $conditions);

    $data = $query->count();
    return $data;
  }

  public function filter($pageNumber, $pageSize, $filters, $sortBy, $searchQuery)
  {
    $query = UserProfile::where('user_profiles.id', '>', '0');

    $query->where('user_profiles.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'user_profiles.user_id');
    $query->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL);
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');

    if ($sortBy === 'name') {
      $query->orderBy('last_name', 'asc');
      $query->orderBy('first_name', 'asc');
    } else if ($sortBy === 'recent') {
      // $query->orderBy('num_of_likes', 'desc');
      $query->orderBy('created_at', 'desc');
    }

    $query->skip($pageSize * ($pageNumber -1));
    $query->take($pageSize);

    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $query->select('user_profiles.*');

    $data = $query->get();

    return $data;
  }

  public function getFiltersCounts($filters, $searchQuery)
  {
    $obj = new \stdClass();
    $obj->nationalities = [];
    $obj->professionalstatuses = [];
    $obj->memberstatuses = [];
    $obj->employertypes = [];

    $query = UserProfile::where('user_profiles.id', '>', '0');

    $query->where('user_profiles.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'user_profiles.user_id');
    $query->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL);
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');

    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $ids = $query->select('user_profiles.*')->lists('user_profiles.id');

    $obj->nationalities = 
      DB::table('user_profiles')
      ->whereIn('user_profiles.id', $ids)
      ->select('nationality as item_id', DB::raw('count(user_profiles.id) as record_count'))
      ->groupBy('nationality')
      ->get();

    $obj->professionalstatuses = 
      DB::table('user_profiles')
      ->whereIn('user_profiles.id', $ids)
      ->select('professional_status as item_id', DB::raw('count(user_profiles.id) as record_count'))
      ->groupBy('professional_status')
      ->get();

    $obj->memberstatuses = 
      DB::table('user_profiles')
      ->whereIn('user_profiles.id', $ids)
      ->select('member_status as item_id', DB::raw('count(user_profiles.id) as record_count'))
      ->groupBy('member_status')
      ->get();

    $obj->employertypes = 
      DB::table('user_profiles')
      ->whereIn('user_profiles.id', $ids)
      ->select('employer_organisation_type as item_id', DB::raw('count(user_profiles.id) as record_count'))
      ->groupBy('employer_organisation_type')
      ->get();

    return $obj;
  }

  public function countFiltered($filters, $searchQuery)
  {
    $query = UserProfile::where('user_profiles.id', '>', '0');

    $query->where('user_profiles.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'user_profiles.user_id');
    $query->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL);
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');
    
    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $data = $query->select('user_profiles.id')
                  ->count();

    return $data;
  }

  public function countBasedInFromFilteredIndividuals($filters, $searchQuery)
  {
    $query = UserProfile::where('user_profiles.id', '>', '0');

    $query->where('user_profiles.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'user_profiles.user_id');
    $query->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL);
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');
    
    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $data = $query->select('based_in')
                  ->groupBy('based_in')
                  ->get();

    if ($data === null) {
      return 0;
    }

    return count($data);
  }

  private function applySearchQuery($query, $searchQuery)
  {
    if ($searchQuery !== null && !is_null($searchQuery) && isset($searchQuery) && gettype($searchQuery) === 'string' && strlen($searchQuery) > 0) {
      $query->where(function ($iquery) use ($searchQuery) {
        return $iquery->where('first_name', 'like', '%'.$searchQuery.'%')
                      ->orwhere('last_name', 'like', '%'.$searchQuery.'%');
      });
    }
  }

  private function applyFilters($query, $filters) 
  {
    if (isset($filters->nationality)) {
      if ($filters->nationality > 0) {
        $query->where('nationality', $filters->nationality);
      }
    }

    if (isset($filters->member_status)) {
      if ($filters->member_status > 0) {
        $query->where('member_status', $filters->member_status);
      }
    }

    if (isset($filters->professional_status)) {
      if ($filters->professional_status > 0) {
        $query->where('professional_status', $filters->professional_status);
      }
    }

    if (isset($filters->employer_organisation_type)) {
      if ($filters->employer_organisation_type > 0) {
        $query->where('employer_organisation_type', $filters->employer_organisation_type);
      }
    }

    if (isset($filters->basedin)) {
      if ($filters->basedin > 0) {
        $query->where('based_in', $filters->basedin);
      }
    }
  }

  public function getPlatformsAssignedToIndividual($indId)
  {
    $platforms = DB::table('platforms_profiles')
                    ->join('platforms', 'platforms_profiles.platform_id', '=', 'platforms.id')
                    ->where('platforms_profiles.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
                    ->where('platforms_profiles.profile_id', $indId)
                    ->select('platforms.*')
                    ->get();

    return $platforms;
  }

}