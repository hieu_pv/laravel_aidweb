<?php namespace Libraries\Repositories\Implementations;

class FilterRecordCounter {

  public function recordCountInFilters($moduleId)
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->regions = [];
    $obj->crisis = [];
    $obj->themes = [];
    $obj->sectors = [];
    $obj->beneficiaries = [];
    $obj->interventions = [];
    $obj->nationalities = [];
    $obj->organisationtypes = [];

    if (\Modules::blog() === intval($moduleId)) {
      $obj->beneficiaries = DB::table('blog_posts_beneficiaries')
                              ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_beneficiaries.post_id')
                              ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                              ->where('user_profiles.is_active', '1')
                              ->where('blog_posts.deleted', '0')
                              ->where('blog_posts.published', '1')
                              ->select('beneficiary_id as item_id', DB::raw('count(post_id) as record_count'))
                              ->groupBy('beneficiary_id')
                              ->get();

      $obj->countries = DB::table('blog_posts_countries')
                          ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_countries.post_id')
                          ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                          ->where('user_profiles.is_active', '1')
                          ->where('blog_posts.deleted', '0')
                          ->where('blog_posts.published', '1')
                          ->select('country_id as item_id', DB::raw('count(post_id) as record_count'))
                          ->groupBy('country_id')
                          ->get();

      $obj->regions = DB::table('blog_posts_regions')
                        ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_regions.post_id')
                        ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                        ->where('user_profiles.is_active', '1')
                        ->where('blog_posts.deleted', '0')
                        ->where('blog_posts.published', '1')
                        ->select('region_id as item_id', DB::raw('count(post_id) as record_count'))
                        ->groupBy('region_id')
                        ->get();

      $obj->crisis = DB::table('blog_posts_crisis')
                       ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_crisis.post_id')
                       ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                       ->where('user_profiles.is_active', '1')
                       ->where('blog_posts.deleted', '0')
                       ->where('blog_posts.published', '1')
                       ->select('crisis_id as item_id', DB::raw('count(post_id) as record_count'))
                       ->groupBy('crisis_id')
                       ->get();

      $obj->interventions = DB::table('blog_posts_interventions')
                              ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_interventions.post_id')
                              ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                              ->where('user_profiles.is_active', '1')
                              ->where('blog_posts.deleted', '0')
                              ->where('blog_posts.published', '1')
                              ->select('intervention_id as item_id', DB::raw('count(post_id) as record_count'))
                              ->groupBy('intervention_id')
                              ->get();

      $obj->sectors = DB::table('blog_posts_sectors')
                        ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_sectors.post_id')
                        ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                        ->where('user_profiles.is_active', '1')
                        ->where('blog_posts.deleted', '0')
                        ->where('blog_posts.published', '1')
                        ->select('sector_id as item_id', DB::raw('count(post_id) as record_count'))
                        ->groupBy('sector_id')
                        ->get();

      $obj->themes = DB::table('blog_posts_themes')
                       ->join('blog_posts', 'blog_posts.id', '=', 'blog_posts_themes.post_id')
                       ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                       ->where('user_profiles.is_active', '1')
                       ->where('blog_posts.deleted', '0')
                       ->where('blog_posts.published', '1')
                       ->select('theme_id as item_id', DB::raw('count(post_id) as record_count'))
                       ->groupBy('theme_id')
                       ->get();

      $obj->nationalities = DB::table('blog_posts')
                              ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
                              ->where('user_profiles.is_active', '1')
                              ->where('blog_posts.deleted', '0')
                              ->where('blog_posts.published', '1')
                              ->select('user_profiles.nationality as item_id', DB::raw('count(blog_posts.id) as record_count'))
                              ->groupBy('user_profiles.nationality')
                              ->get();
    }

    if (\Modules::news() === intval($moduleId)) {
      $obj->beneficiaries = DB::table('news_items_beneficiaries')
                              ->join('news_items', 'news_items.id', '=', 'news_items_beneficiaries.newsitem_id')
                              ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                              ->where('organisations.is_active', '1')
                              ->where('news_items.deleted', '0')
                              ->where('news_items.published', '1')
                              ->select('beneficiary_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                              ->groupBy('beneficiary_id')
                              ->get();

      $obj->countries = DB::table('news_items_countries')
                          ->join('news_items', 'news_items.id', '=', 'news_items_countries.newsitem_id')
                          ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                          ->where('organisations.is_active', '1')
                          ->where('news_items.deleted', '0')
                          ->where('news_items.published', '1')
                          ->select('country_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                          ->groupBy('country_id')
                          ->get();

      $obj->regions = DB::table('news_items_regions')
                        ->join('news_items', 'news_items.id', '=', 'news_items_regions.newsitem_id')
                        ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                        ->where('organisations.is_active', '1')
                        ->where('news_items.deleted', '0')
                        ->where('news_items.published', '1')
                        ->select('region_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                        ->groupBy('region_id')
                        ->get();

      $obj->crisis = DB::table('news_items_crisis')
                       ->join('news_items', 'news_items.id', '=', 'news_items_crisis.newsitem_id')
                       ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                       ->where('organisations.is_active', '1')
                       ->where('news_items.deleted', '0')
                       ->where('news_items.published', '1')
                       ->select('crisis_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                       ->groupBy('crisis_id')
                       ->get();

      $obj->interventions = DB::table('news_items_interventions')
                              ->join('news_items', 'news_items.id', '=', 'news_items_interventions.newsitem_id')
                              ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                              ->where('organisations.is_active', '1')
                              ->where('news_items.deleted', '0')
                              ->where('news_items.published', '1')
                              ->select('intervention_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                              ->groupBy('intervention_id')
                              ->get();

      $obj->sectors = DB::table('news_items_sectors')
                        ->join('news_items', 'news_items.id', '=', 'news_items_sectors.newsitem_id')
                        ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                        ->where('organisations.is_active', '1')
                        ->where('news_items.deleted', '0')
                        ->where('news_items.published', '1')
                        ->select('sector_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                        ->groupBy('sector_id')
                        ->get();

      $obj->themes = DB::table('news_items_themes')
                       ->join('news_items', 'news_items.id', '=', 'news_items_themes.newsitem_id')
                       ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                       ->where('organisations.is_active', '1')
                       ->where('news_items.deleted', '0')
                       ->where('news_items.published', '1')
                       ->select('theme_id as item_id', DB::raw('count(newsitem_id) as record_count'))
                       ->groupBy('theme_id')
                       ->get();

      $obj->organisationtypes = DB::table('news_items')
                                  ->join('organisations', 'news_items.organisation_id', '=', 'organisations.id')
                                  ->where('organisations.is_active', '1')
                                  ->where('news_items.deleted', '0')
                                  ->where('news_items.published', '1')
                                  ->select('organisations.org_type as item_id', DB::raw('count(news_items.id) as record_count'))
                                  ->groupBy('organisations.org_type')
                                  ->get();
    }

    if (\Modules::videos() === intval($moduleId)) {
      $obj->beneficiaries = DB::table('videos_beneficiaries')
                              ->join('videos', 'videos.id', '=', 'videos_beneficiaries.video_id')
                              ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                              ->where('organisations.is_active', '1')
                              ->where('videos.deleted', '0')
                              ->select('beneficiary_id as item_id', DB::raw('count(video_id) as record_count'))
                              ->groupBy('beneficiary_id')
                              ->get();

      $obj->countries = DB::table('videos_countries')
                          ->join('videos', 'videos.id', '=', 'videos_countries.video_id')
                          ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                          ->where('organisations.is_active', '1')
                          ->where('videos.deleted', '0')
                          ->where('videos.published', '1')
                          ->select('country_id as item_id', DB::raw('count(video_id) as record_count'))
                          ->groupBy('country_id')
                          ->get();

      $obj->regions = DB::table('videos_regions')
                        ->join('videos', 'videos.id', '=', 'videos_regions.video_id')
                        ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                        ->where('organisations.is_active', '1')
                        ->where('videos.deleted', '0')
                        ->where('videos.published', '1')
                        ->select('region_id as item_id', DB::raw('count(video_id) as record_count'))
                        ->groupBy('region_id')
                        ->get();

      $obj->crisis = DB::table('videos_crisis')
                       ->join('videos', 'videos.id', '=', 'videos_crisis.video_id')
                       ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                       ->where('organisations.is_active', '1')
                       ->where('videos.deleted', '0')
                       ->where('videos.published', '1')
                       ->select('crisis_id as item_id', DB::raw('count(video_id) as record_count'))
                       ->groupBy('crisis_id')
                       ->get();

      $obj->interventions = DB::table('videos_interventions')
                              ->join('videos', 'videos.id', '=', 'videos_interventions.video_id')
                              ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                              ->where('organisations.is_active', '1')
                              ->where('videos.deleted', '0')
                              ->where('videos.published', '1')
                              ->select('intervention_id as item_id', DB::raw('count(video_id) as record_count'))
                              ->groupBy('intervention_id')
                              ->get();

      $obj->sectors = DB::table('videos_sectors')
                        ->join('videos', 'videos.id', '=', 'videos_sectors.video_id')
                        ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                        ->where('organisations.is_active', '1')
                        ->where('videos.deleted', '0')
                        ->where('videos.published', '1')
                        ->select('sector_id as item_id', DB::raw('count(video_id) as record_count'))
                        ->groupBy('sector_id')
                        ->get();

      $obj->themes = DB::table('videos_themes')
                       ->join('videos', 'videos.id', '=', 'videos_themes.video_id')
                       ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                       ->where('organisations.is_active', '1')
                       ->where('videos.deleted', '0')
                       ->where('videos.published', '1')
                       ->select('theme_id as item_id', DB::raw('count(video_id) as record_count'))
                       ->groupBy('theme_id')
                       ->get();

      $obj->organisationtypes = DB::table('videos')
                                  ->join('organisations', 'videos.organisation_id', '=', 'organisations.id')
                                  ->where('organisations.is_active', '1')
                                  ->where('videos.deleted', '0')
                                  ->where('videos.published', '1')
                                  ->select('organisations.org_type as item_id', DB::raw('count(videos.id) as record_count'))
                                  ->groupBy('organisations.org_type')
                                  ->get();
    }

    if (\Modules::takeactions() === intval($moduleId)) {
      $obj->beneficiaries = DB::table('take_actions_beneficiaries')
                              ->join('take_actions', 'take_actions.id', '=', 'take_actions_beneficiaries.takeaction_id')
                              ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                              ->where('organisations.is_active', '1')
                              ->where('take_actions.deleted', '0')
                              ->where('take_actions.published', '1')
                              ->select('beneficiary_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                              ->groupBy('beneficiary_id')
                              ->get();

      $obj->countries = DB::table('take_actions_countries')
                          ->join('take_actions', 'take_actions.id', '=', 'take_actions_countries.takeaction_id')
                          ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                          ->where('organisations.is_active', '1')
                          ->where('take_actions.deleted', '0')
                          ->where('take_actions.published', '1')
                          ->select('country_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                          ->groupBy('country_id')
                          ->get();

      $obj->regions = DB::table('take_actions_regions')
                        ->join('take_actions', 'take_actions.id', '=', 'take_actions_regions.takeaction_id')
                        ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                        ->where('organisations.is_active', '1')
                        ->where('take_actions.deleted', '0')
                        ->where('take_actions.published', '1')
                        ->select('region_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                        ->groupBy('region_id')
                        ->get();

      $obj->crisis = DB::table('take_actions_crisis')
                       ->join('take_actions', 'take_actions.id', '=', 'take_actions_crisis.takeaction_id')
                       ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                       ->where('organisations.is_active', '1')
                       ->where('take_actions.deleted', '0')
                       ->where('take_actions.published', '1')
                       ->select('crisis_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                       ->groupBy('crisis_id')
                       ->get();

      $obj->interventions = DB::table('take_actions_interventions')
                              ->join('take_actions', 'take_actions.id', '=', 'take_actions_interventions.takeaction_id')
                              ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                              ->where('organisations.is_active', '1')
                              ->where('take_actions.deleted', '0')
                              ->where('take_actions.published', '1')
                              ->select('intervention_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                              ->groupBy('intervention_id')
                              ->get();

      $obj->sectors = DB::table('take_actions_sectors')
                        ->join('take_actions', 'take_actions.id', '=', 'take_actions_sectors.takeaction_id')
                        ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                        ->where('organisations.is_active', '1')
                        ->where('take_actions.deleted', '0')
                        ->where('take_actions.published', '1')
                        ->select('sector_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                        ->groupBy('sector_id')
                        ->get();

      $obj->themes = DB::table('take_actions_themes')
                       ->join('take_actions', 'take_actions.id', '=', 'take_actions_themes.takeaction_id')
                       ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                       ->where('organisations.is_active', '1')
                       ->where('take_actions.deleted', '0')
                       ->where('take_actions.published', '1')
                       ->select('theme_id as item_id', DB::raw('count(takeaction_id) as record_count'))
                       ->groupBy('theme_id')
                       ->get();

      $obj->organisationtypes = DB::table('take_actions')
                                  ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                                  ->where('organisations.is_active', '1')
                                  ->where('take_actions.deleted', '0')
                                  ->where('take_actions.published', '1')
                                  ->select('organisations.org_type as item_id', DB::raw('count(take_actions.id) as record_count'))
                                  ->groupBy('organisations.org_type')
                                  ->get();

      $obj->actiontypes = DB::table('take_actions')
                            ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
                            ->where('organisations.is_active', '1')
                            ->where('take_actions.deleted', '0')
                            ->where('take_actions.published', '1')
                            ->select('type_id as item_id', DB::raw('count(take_actions.id) as record_count'))
                            ->groupBy('type_id')
                            ->get();
    }

    //dd(json_encode($obj));
    return $obj;
  }


}