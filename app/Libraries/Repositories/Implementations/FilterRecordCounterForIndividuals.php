<?php namespace Libraries\Repositories\Implementations;

class FilterRecordCounterForIndividuals {

  public function recordCountForIndividualsFilters()
  {
    $obj = new \stdClass();
    $obj->nationalities = [];
    $obj->professionalstatuses = [];
    $obj->memberstatuses = [];
    $obj->employertypes = [];

    $obj->nationalities = DB::table('user_profiles')
                            ->join('users', 'users.id', '=', 'user_profiles.user_id')
                            ->where('user_profiles.is_active', '1')
                            ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
                            ->where('users.verified', '1')
                            ->where('users.is_new', '0')
                            ->select('nationality as item_id', DB::raw('count(user_profiles.id) as record_count'))
                            ->groupBy('nationality')
                            ->get();

    $obj->professionalstatuses = DB::table('user_profiles')
                                   ->join('users', 'users.id', '=', 'user_profiles.user_id')
                                   ->where('user_profiles.is_active', '1')
                                   ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
                                   ->where('users.verified', '1')
                                   ->where('users.is_new', '0')
                                   ->select('professional_status as item_id', DB::raw('count(user_profiles.id) as record_count'))
                                   ->groupBy('professional_status')
                                   ->get();

    $obj->memberstatuses = DB::table('user_profiles')
                             ->join('users', 'users.id', '=', 'user_profiles.user_id')
                             ->where('user_profiles.is_active', '1')
                             ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
                             ->where('users.verified', '1')
                             ->where('users.is_new', '0')
                             ->select('member_status as item_id', DB::raw('count(user_profiles.id) as record_count'))
                             ->groupBy('member_status')
                             ->get();

    $obj->employertypes = DB::table('user_profiles')
                            ->join('users', 'users.id', '=', 'user_profiles.user_id')
                            ->where('user_profiles.is_active', '1')
                            ->where('users.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
                            ->where('users.verified', '1')
                            ->where('users.is_new', '0')
                            ->select('employer_organisation_type as item_id', DB::raw('count(user_profiles.id) as record_count'))
                            ->groupBy('employer_organisation_type')
                            ->get();

    return $obj;
  }

}