<?php namespace Libraries\Repositories\Implementations\Blog;

use Illuminate\Support\Facades\DB;
use Libraries\Models\Blog\Post;
use Libraries\Models\UserProfile;
use Libraries\Support\Modules;
use Libraries\Repositories\Implementations\Filterable;

class PostRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\Blog\IPostRepository {

  private $_filterable;

  /**
   * [__construct description]
   * @param Post
   */
  public function __construct(Post $model)
  {
    $this->model = $model;
    $this->_filterable = new Filterable();
  }

  /**
   * [deleteById description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteById($id)
  {
    $post = $this->model->find($id);

    // $post->tags()->detach();
    // $post->categories()->detach();

    $post->countries()->detach();
    $post->regions()->detach();
    $post->crisis()->detach();
    $post->sectors()->detach();
    $post->themes()->detach();
    $post->beneficiaries()->detach();
    $todelete->interventions()->detach();

    $post->delete();
  }

  public function getByIdAndSlug($id, $slug)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveMembers($query);
    $this->_filterable->displayable($query, 'blog_posts');
    $this->_filterable->selectingLikes($query, 'blog_posts', Modules::BLOG);

    $query->where('blog_posts.slug', '=', $slug)
          ->where('blog_posts.id', '=', $id);

    $d = $query->first();
    return $d;
  }

  public function getLatestPosts()
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveMembers($query);
    $this->_filterable->displayable($query, 'blog_posts');

    $query->orderBy('created_at', 'desc')
          ->skip(0)
          ->take(10);

    return $query->get();
  }

  public function getLatestPostsFromCountry($countryId)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveMembersBasedIn($query, $countryId);
    $this->_filterable->displayable($query, 'blog_posts');

    $query->orderBy('created_at', 'desc')
          ->skip(0)
          ->take(10);

    return $query->get();
  }

  public function getMostPopularPosts()
  {
    $query = Post::where('blog_posts.id', '>', '0')
                 ->where(DB::raw('DATEDIFF((select max(blog_posts.created_at) from blog_posts), blog_posts.created_at)'), '<=', '30');

    $this->_filterable->selectingLikes($query, 'blog_posts', Modules::BLOG);
    $this->fromActiveMembers($query);
    $this->_filterable->displayable($query, 'blog_posts');

    $query->orderBy('num_of_likes', 'desc')
          ->orderBy('blog_posts.created_at', 'desc')
          ->skip(0)
          ->take(3);

    return $query->get();
  }

  public function getMostPopularPostsFromCountry($countryId)
  {
    $query = Post::where('blog_posts.id', '>', '0')
                 ->where(DB::raw('DATEDIFF((select max(blog_posts.created_at) from blog_posts), blog_posts.created_at)'), '<=', '30');

    $this->_filterable->selectingLikes($query, 'blog_posts', Modules::BLOG);
    $this->fromActiveMembersBasedIn($query, $countryId);
    $this->_filterable->displayable($query, 'blog_posts');

    $query->orderBy('num_of_likes', 'desc')
          ->orderBy('blog_posts.created_at', 'desc')
          ->skip(0)
          ->take(3);

    return $query->get();
  }

  public function getTopPosts()
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveTopMembers($query, 'blog_posts');
    $this->_filterable->displayable($query, 'blog_posts');

    $query->where('featured', '=', '1')
          ->orderBy('created_at', 'desc')
          ->skip(0)
          ->take(7);

    $data = $query->get();
    return $data;
  }

  public function getTopPostsFromCountry($countryId)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveTopMembersBasedIn($query, $countryId, 'blog_posts');
    $this->_filterable->displayable($query, 'blog_posts');

    $query->where('featured', '=', '1')
          ->orderBy('created_at', 'desc')
          ->skip(0)
          ->take(7);

    $data = $query->get();
    return $data;
  }

  public function getTopPostsInRegion($regionId)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    //$this->fromActiveTopMembers($query, 'blog_posts'); 27 agustus 2015
    $this->_filterable->displayable($query, 'blog_posts');
    $this->_filterable->inRegion($query, $regionId);

    $query->where('featured', '=', '1')
          ->orderBy('created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();
    return $data;
  }

  public function getTopPostsInThemeFromCountry($themeId, $countryId)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    // requirement changes, select also posts from international ind having country filter = platform filter
    //$this->fromActiveTopMembersBasedIn($query, $countryId, 'blog_posts');
    $query->where(function ($query1) use ($countryId)
    {
      $query1->whereHas('relatedUserProfile', function ($q) use ($countryId)
      {
        $q->where('is_active', '=', '1');
        //$q->where('based_in', '=', $countryId);
        $q->whereExists(function($query2) use ($countryId) 
        {
          $query2->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
                 ->where('platforms.related_country_id', '=', $countryId)
                 ->whereRaw('platforms_top_profiles.profile_type = 1')
                 ->whereRaw('platforms_top_profiles.profile_id = blog_posts.user_profile_id');
        });
      });
      $query1->orWhere(function ($subQuery1) use ($countryId) 
      {
        $subQuery1->whereExists(function ($query3) {
          // select platform = international (platform_id = 1)
          // and profile type = individual (profile_type = 1)
          $query3->select(DB::raw(1))
                 ->from('platforms_profiles')
                 ->whereRaw('blog_posts.user_profile_id = platforms_profiles.profile_id and platforms_profiles.profile_type = 1 and platforms_profiles.platform_id = 1');
        });
        $subQuery1->whereExists(function ($query3) {
          $query3->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->whereRaw('blog_posts.user_profile_id = platforms_top_profiles.profile_id and platforms_top_profiles.profile_type = 1 and platforms_top_profiles.platform_id = 1');
        });
        $subQuery1->whereExists(function ($query4) use ($countryId) {
          $query4->select(DB::raw(1))
                 ->from('blog_posts_countries')
                 ->whereRaw('blog_posts.id = blog_posts_countries.post_id')
                 ->where('blog_posts_countries.country_id', $countryId);
        });
      });
    });

    $this->_filterable->displayable($query, 'blog_posts');
    $this->_filterable->inTheme($query, $themeId);

    $query->where('featured', '=', '1')
          ->orderBy('created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();

    return $data;
  }

  public function filterAllPosts($pageNumber, $pageSize, $filters, $sortBy, $searchQuery)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->_filterable->displayable($query, 'blog_posts');
    $this->fromActiveMembers($query);
    $this->_filterable->selectingLikes($query, 'blog_posts', Modules::BLOG);

    if ($sortBy === 'recent') {
      $query->orderBy('created_at', 'desc');
    } else if ($sortBy === 'popularity') {
      $query->orderBy('num_of_likes', 'desc');
      $query->orderBy('created_at', 'desc');
    }

    $query->skip($pageSize * ($pageNumber -1));
    $query->take($pageSize);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->get();

    return $data;
  }

  public function getFilterOptionsRecordCount($filters, $searchQuery)
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->regions = [];
    $obj->crisis = [];
    $obj->themes = [];
    $obj->sectors = [];
    $obj->beneficiaries = [];
    $obj->nationalities = [];

    $query = Post::where('blog_posts.id', '>', '0');

    $this->_filterable->displayable($query, 'blog_posts');
    $this->fromActiveMembers($query);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $ids = $query->lists('id');
    
    // countries
    $obj->countries = 
      DB::table('blog_posts_countries')
      ->whereIn('post_id', $ids)
      ->select('country_id as item_id', DB::raw('count(post_id) as record_count'))
      ->groupBy('country_id')
      ->get();

    $obj->regions =  
      DB::table('blog_posts_regions')
      ->whereIn('post_id', $ids)
      ->select('region_id as item_id', DB::raw('count(post_id) as record_count'))
      ->groupBy('region_id')
      ->get();

    $obj->crisis =  
      DB::table('blog_posts_crisis')
      ->whereIn('post_id', $ids)
      ->select('crisis_id as item_id', DB::raw('count(post_id) as record_count'))
      ->groupBy('crisis_id')
      ->get();

    $obj->themes =  
      DB::table('blog_posts_themes')
      ->whereIn('post_id', $ids)
      ->select('theme_id as item_id', DB::raw('count(post_id) as record_count'))
      ->groupBy('theme_id')
      ->get();

    $obj->sectors =  
      DB::table('blog_posts_sectors')
      ->whereIn('post_id', $ids)
      ->select('sector_id as item_id', DB::raw('count(post_id) as record_count'))
      ->groupBy('sector_id')
      ->get();

    $obj->beneficiaries =  
      DB::table('blog_posts_beneficiaries')
      ->whereIn('post_id', $ids)
      ->select('beneficiary_id as item_id', DB::raw('count(post_id) as record_count'))
      ->groupBy('beneficiary_id')
      ->get();

    $obj->nationalities =  
      DB::table('blog_posts')
      ->join('user_profiles', 'blog_posts.user_profile_id', '=', 'user_profiles.id')
      ->whereIn('blog_posts.id', $ids)
      ->select('user_profiles.nationality as item_id', DB::raw('count(blog_posts.id) as record_count'))
      ->groupBy('user_profiles.nationality')
      ->get();
    
    //Log::info(DB::getQueryLog());

    return $obj;            
  }

  public function countFilteredPosts($filters, $searchQuery)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->_filterable->displayable($query, 'blog_posts');
    $this->fromActiveMembers($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  public function countMembersFromFilteredPosts($filters, $searchQuery)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->_filterable->displayable($query, 'blog_posts');
    $this->fromActiveMembers($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select(DB::raw('user_profile_id'))
                  ->groupBy('user_profile_id')
                  ->get();

    if ($data === null) {
      return 0;
    }

    return count($data);
  }

  private function applyPlatformFilter($query, $filters) 
  {
    // if national platform is selected (>1) 1 is International
    if (isset($filters->platform_id) && isset($filters->platform_country_id)) {
      if ($filters->platform_id > 1) {
        $query->where(function ($query) use ($filters) 
        {
          //platform_id = 1 = international
          //profile_type = 1 = individual
          $sql = '
          exists 
          (
            select  1 
            from    platforms_profiles 
            where   platforms_profiles.profile_id = blog_posts.user_profile_id 
            and     platforms_profiles.profile_type = 1 
            and     platforms_profiles.platform_id = '.$filters->platform_id.'
          )
          or
          (
            exists
            (
              select  1
              from    platforms_profiles 
              where   platforms_profiles.profile_id = blog_posts.user_profile_id 
              and     platforms_profiles.profile_type = 1
              and     platforms_profiles.platform_id = 1
            )
            and
            exists
            (
              select  1
              from    blog_posts_countries
              where   blog_posts_countries.post_id = blog_posts.id
              and     blog_posts_countries.country_id = '.$filters->platform_country_id.'
            )
          )';
          $query->whereRaw($sql);
        });
      }
    }
  }

  public function getPostsByUserProfileId($userProfileId, $pageNumber, $pageSize)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveMembers($query);
    $this->_filterable->displayable($query, 'blog_posts');
    $this->_filterable->selectingLikes($query, 'blog_posts', Modules::BLOG);

    $query->where('blog_posts.user_profile_id', '=', $userProfileId);

    $toSkip = $pageSize * ($pageNumber - 1);
    $query->orderBy('created_at', 'desc')
          ->skip($toSkip)
          ->take($pageSize);

    $data = $query->get();

    return $data;
  }

  public function countPostsByUserProfileId($userProfileId)
  {
    $query = Post::where('blog_posts.id', '>', '0');

    $this->fromActiveMembers($query);
    $this->_filterable->displayable($query, 'blog_posts');

    $query->where('blog_posts.user_profile_id', '=', $userProfileId);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  /*==========================================================================================*/

  private function fromActiveTopMembers($query, $tableName)
  {
    $query->whereHas('relatedUserProfile', function ($q) use ($tableName)
    {
      $q->where('is_active', '=', '1');
      $q->whereExists(function($query2) use ($tableName) 
      {
        $query2->select(DB::raw(1))
               ->from('platforms_top_profiles')
               ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
               ->where('platforms.related_country_id', '=', 0)
               ->where('platforms_top_profiles.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
               ->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.user_profile_id');
      });
    });
  }

  private function fromActiveTopMembersBasedIn($query, $countryId, $tableName)
  {
    $query->whereHas('relatedUserProfile', function ($q) use ($countryId, $tableName)
    {
      $q->where('is_active', '=', '1');
      //$q->where('based_in', '=', $countryId);
      $q->whereExists(function($query2) use ($countryId, $tableName) 
      {
        $query2->select(DB::raw(1))
               ->from('platforms_top_profiles')
               ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
               ->where('platforms.related_country_id', '=', $countryId)
               ->where('platforms_top_profiles.profile_type', '=', Modules::PROFILE_TYPE_INDIVIDUAL)
               ->whereRaw('platforms_top_profiles.profile_id = '.$tableName.'.user_profile_id');
      });
    });
  }

  private function fromActiveMembers($query)
  {
    $query->whereHas('relatedUserProfile', function ($q)
    {
      $q->where('is_active', '=', '1');
    });
  }

  private function fromActiveMembersBasedIn($query, $countryId)
  {
    $query->whereHas('relatedUserProfile', function ($q) use ($countryId)
    {
      $q->where('is_active', '=', '1');
      $q->where('based_in', '=', $countryId);
    });
  }

  private function applySearchQuery($query, $searchQuery)
  {
    if ($searchQuery !== null && !is_null($searchQuery) && isset($searchQuery) && gettype($searchQuery) === 'string' && strlen($searchQuery) > 0) {
      $query->where(function ($iquery) use ($searchQuery) {
        return $iquery->where('title', 'like', '%'.$searchQuery.'%')
                      ->orwhere('content', 'like', '%'.$searchQuery.'%')
                      ->orwhere('image_legend', 'like', '%'.$searchQuery.'%');
      });
    }
  }

}
