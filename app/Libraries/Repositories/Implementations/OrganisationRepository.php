<?php namespace Libraries\Repositories\Implementations;

use Libraries\Models\Organisation;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class OrganisationRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\IOrganisationRepository {

  public function __construct(Organisation $model)
  {
    $this->model = $model;
  }

  public function getAuthoringOrganisations()
  {
    $orgs = Organisation::join('users', 'users.id', '=', 'organisations.cp_id')
              ->where('users.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
              ->where('organisations.is_active', '1')
              ->where('users.verified', '1')
              ->where('users.is_new', '0')
              ->orwhere('users.profile_type', '=', Modules::PROFILE_TYPE_ADMIN)
              ->orderBy('organisations.org_name', 'asc')
              ->select('organisations.id','organisations.cp_id', 'organisations.org_name')
              ->get();

    return $orgs;
  }

  public function filter($pageNumber, $pageSize, $filters, $sortBy, $searchQuery)
  {
    $query = Organisation::where('organisations.id', '>', '0');

    $query->where('organisations.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'organisations.cp_id');
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');

    if ($sortBy === 'org_name') {
      $query->orderBy('org_name', 'asc');
    } else if ($sortBy === 'recent') {
      // $query->orderBy('num_of_likes', 'desc');
      $query->orderBy('created_at', 'desc');
    }

    $query->skip($pageSize * ($pageNumber -1));
    $query->take($pageSize);

    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $query->select('organisations.*');

    $data = $query->get();

    return $data;
  }

  public function getFiltersCounts($filters, $searchQuery)
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->levelofactivities = [];
    $obj->organisationtypes = [];

    $query = Organisation::where('organisations.id', '>', '0');

    $query->where('organisations.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'organisations.cp_id');
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');

    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $ids = $query->select('organisations.*')->lists('organisations.id');

    $obj->countries = 
      DB::table('organisations')
      ->whereIn('organisations.id', $ids)
      ->select('office_location as item_id', DB::raw('count(organisations.id) as record_count'))
      ->groupBy('office_location')
      ->get();

    $obj->organisationtypes = 
      DB::table('organisations')
      ->whereIn('organisations.id', $ids)
      ->select('org_type as item_id', DB::raw('count(organisations.id) as record_count'))
      ->groupBy('org_type')
      ->get();

    $obj->levelofactivities = 
      DB::table('organisations')
      ->whereIn('organisations.id', $ids)
      ->select('level_of_activity as item_id', DB::raw('count(organisations.id) as record_count'))
      ->groupBy('level_of_activity')
      ->get();

    return $obj;
  }

  public function countFiltered($filters, $searchQuery)
  {
    $query = Organisation::where('organisations.id', '>', '0');

    $query->where('organisations.is_active', '1');
    // verified and profile completed
    $query->join('users', 'users.id', '=', 'organisations.cp_id');
    $query->where('users.verified', '1');
    $query->where('users.is_new', '0');
    
    $this->applySearchQuery($query, $searchQuery);
    $this->applyFilters($query, $filters);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  private function applySearchQuery($query, $searchQuery)
  {
    if ($searchQuery !== null && !is_null($searchQuery) && isset($searchQuery) && gettype($searchQuery) === 'string' && strlen($searchQuery) > 0) {
      $query->where(function ($iquery) use ($searchQuery) {
        return $iquery->where('org_name', 'like', '%'.$searchQuery.'%');
                      // ->orwhere('content', 'like', '%'.$searchQuery.'%')
                      // ->orwhere('image_legend', 'like', '%'.$searchQuery.'%');
      });
    }
  }

  private function applyFilters($query, $filters) 
  {
    if (isset($filters->country)) {
      if ($filters->country > 0) {
        $query->where('office_location', $filters->country);
      }
    }

    if (isset($filters->level_of_activity)) {
      if ($filters->level_of_activity > 0) {
        $query->where('level_of_activity', $filters->level_of_activity);
      }
    }

    if (isset($filters->organisation_type)) {
      if ($filters->organisation_type > 0) {
        $query->where('org_type', $filters->organisation_type);
      }
    }
  }

  public function getPlatformsAssignedToOrganisation($orgId) 
  {
    $platforms = DB::table('platforms_profiles')
                    ->join('platforms', 'platforms_profiles.platform_id', '=', 'platforms.id')
                    ->where('platforms_profiles.profile_type', '=', Modules::PROFILE_TYPE_ORGANISATION)
                    ->where('platforms_profiles.profile_id', $orgId)
                    ->select('platforms.*')
                    ->get();

    return $platforms;
  }

}