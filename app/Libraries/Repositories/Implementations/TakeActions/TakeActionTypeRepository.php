<?php namespace Libraries\Repositories\Implementations\TakeActions;

use Libraries\Models\TakeActions\Type;
use Libraries\Models\Organisation;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;

class TakeActionTypeRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\TakeActions\ITakeActionTypeRepository {

  public function __construct(Type $model)
  {
    $this->model = $model;
  }

  public function getAsList() 
  {
    $results = Type::lists('name', 'id');
    $results[0] = '--- No Type';

    asort($results);

    return $results;
  }

  public function getWhereHasPostings($moduleId)
  {
    $tableName = '';

    if ((int)$moduleId === Modules::TAKE_ACTIONS) {
      $ids = DB::table('take_actions')
               ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
               ->where('organisations.is_active', '1')
               ->where('take_actions.deleted', '0')
               ->where('take_actions.published', '1')
               ->distinct('type_id')
               ->lists('type_id');
      $records = Type::whereIn('id', $ids)->get()->toArray();
      if ($records === null) {
        $records = [];
      }
      return $records;
    }

    return array();
  }

}