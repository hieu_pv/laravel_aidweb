<?php namespace Libraries\Repositories\Implementations\TakeActions;

use Libraries\Models\TakeActions\TakeAction;
use Libraries\Models\Organisation;
use Libraries\Support\Modules;
use Illuminate\Support\Facades\DB;
use Libraries\Repositories\Implementations\Filterable;

class TakeActionRepository extends \Libraries\Repositories\Implementations\BaseRepository implements \Libraries\Repositories\TakeActions\ITakeActionRepository {

  private $_filterable;

  public function __construct(TakeAction $model)
  {
    $this->model = $model;
    $this->_filterable = new Filterable();
  }

  public function deleteById($id)
  {
    $todelete = $this->model->find($id);

    $todelete->countries()->detach();
    $todelete->regions()->detach();
    $todelete->crisis()->detach();
    $todelete->sectors()->detach();
    $todelete->themes()->detach();
    $todelete->beneficiaries()->detach();
    $todelete->interventions()->detach();
    
    $todelete->delete();
  }

  public function getTopOnePerType() 
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'take_actions');
    $this->_filterable->displayable($query, 'take_actions');

    // $query->select(DB::raw('*, max(take_actions.created_at)')) file asli asreds 27 agustus 2015
    //       ->where('featured', '=', '1')
    //       ->groupBy('type_id');

    $query->orderBy('id',"DESC");;
          
    return $query->get();
  }

  public function getTopOnePerTypeInCountry($countryId) 
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'take_actions');
    $this->_filterable->displayable($query, 'take_actions');

    // $query->select(DB::raw('*, max(take_actions.created_at)')) file asli asreds 27 agustus 2015
    //       ->where('featured', '=', '1')
    //       ->groupBy('type_id');

    $query->orderBy('id',"DESC");
          
    return $query->get();
  }

  public function getTopInTypes($typeId)
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->fromActiveTopAgencies($query, 'take_actions');
    $this->_filterable->displayable($query, 'take_actions');
    $this->_filterable->inType($query, $typeId);

    $query->where('featured', '=', '1')
          ->orderBy('take_actions.created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();

    return $data;
  }

  public function getTopInTypesByCountry($typeId, $countryId)
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    // requirement changes, include international orgs 
    //$this->_filterable->fromActiveTopAgenciesLocatedAt($query, $countryId, 'take_actions');
    $query->where(function ($query1) use ($countryId)
    {
      // from agency in national platform - based on office location
      $query1->whereHas('relatedOrganisation', function ($q) use ($countryId)
      {
        $q->where('is_active', '=', '1');
        //$q->where('office_location', '=', $countryId);
        $q->whereExists(function ($query5) use ($countryId) 
        {
          $query5->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->join('platforms', 'platforms_top_profiles.platform_id', '=', 'platforms.id')
                 ->where('platforms.related_country_id', '=', $countryId)
                 ->whereRaw('platforms_top_profiles.profile_type = 2')
                 ->whereRaw('platforms_top_profiles.profile_id = take_actions.organisation_id');
        });
      });
      // or from platform = int and country filter = platform country
      $query1->orWhere(function ($query2) use ($countryId) 
      {
        $query2->whereExists(function ($query3) {
          // select platform = international (platform_id = 1)
          // and profile type = organisation (profile_type = 2)
          $query3->select(DB::raw(1))
                 ->from('platforms_profiles')
                 ->whereRaw('take_actions.organisation_id = platforms_profiles.profile_id and platforms_profiles.profile_type = 2 and platforms_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query3) {
          $query3->select(DB::raw(1))
                 ->from('platforms_top_profiles')
                 ->whereRaw('take_actions.organisation_id = platforms_top_profiles.profile_id and platforms_top_profiles.profile_type = 2 and platforms_top_profiles.platform_id = 1');
        });
        $query2->whereExists(function ($query4) use ($countryId) {
          $query4->select(DB::raw(1))
                 ->from('take_actions_countries')
                 ->whereRaw('take_actions.id = take_actions_countries.takeaction_id')
                 ->where('take_actions_countries.country_id', $countryId);
        });
      });
    });

    $this->_filterable->displayable($query, 'take_actions');
    $this->_filterable->inType($query, $typeId);

    $query->where('featured', '=', '1')
          ->orderBy('take_actions.created_at', 'desc')
          ->skip(0)
          ->take(4);

    $data = $query->get();

    return $data;
  }

  public function filterAll($pageNumber, $pageSize, $filters, $sortBy, $searchQuery)
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->displayable($query, 'take_actions');
    $this->_filterable->fromActiveAgencies($query);

    if ($sortBy === 'recent') {
      $query->orderBy('take_actions.created_at', 'desc');
    } else if ($sortBy === 'popularity') {
      // $query->orderBy('num_of_likes', 'desc');
      // $query->orderBy('take_actions.created_at', 'desc');
      $query->orderBy('take_actions.created_at', 'desc');
    }

    $query->skip($pageSize * ($pageNumber -1));
    $query->take($pageSize);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->get();

    return $data;
  }

  public function getFilterOptionsRecordCount($filters, $searchQuery)
  {
    $obj = new \stdClass();
    $obj->countries = [];
    $obj->regions = [];
    $obj->crisis = [];
    $obj->themes = [];
    $obj->sectors = [];
    $obj->beneficiaries = [];
    $obj->organisationtypes = [];
    $obj->actiontypes = [];

    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->displayable($query, 'take_actions');
    $this->_filterable->fromActiveAgencies($query);

    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $ids = $query->lists('id');
    
    // countries
    $obj->countries = 
      DB::table('take_actions_countries')
      ->whereIn('takeaction_id', $ids)
      ->select('country_id as item_id', DB::raw('count(takeaction_id) as record_count'))
      ->groupBy('country_id')
      ->get();

    $obj->regions =  
      DB::table('take_actions_regions')
      ->whereIn('takeaction_id', $ids)
      ->select('region_id as item_id', DB::raw('count(takeaction_id) as record_count'))
      ->groupBy('region_id')
      ->get();

    // $obj->crisis =  
    //   DB::table('take_actions_crisis')
    //   ->whereIn('takeaction_id', $ids)
    //   ->select('crisis_id as item_id', DB::raw('count(takeaction_id) as record_count'))
    //   ->groupBy('crisis_id')
    //   ->get();

    $obj->themes =  
      DB::table('take_actions_themes')
      ->whereIn('takeaction_id', $ids)
      ->select('theme_id as item_id', DB::raw('count(takeaction_id) as record_count'))
      ->groupBy('theme_id')
      ->get();

    $obj->sectors =  
      DB::table('take_actions_sectors')
      ->whereIn('takeaction_id', $ids)
      ->select('sector_id as item_id', DB::raw('count(takeaction_id) as record_count'))
      ->groupBy('sector_id')
      ->get();

    $obj->beneficiaries =  
      DB::table('take_actions_beneficiaries')
      ->whereIn('takeaction_id', $ids)
      ->select('beneficiary_id as item_id', DB::raw('count(takeaction_id) as record_count'))
      ->groupBy('beneficiary_id')
      ->get();

    $obj->organisationtypes =  
      DB::table('take_actions')
      ->join('organisations', 'take_actions.organisation_id', '=', 'organisations.id')
      ->whereIn('take_actions.id', $ids)
      ->select('organisations.org_type as item_id', DB::raw('count(take_actions.id) as record_count'))
      ->groupBy('organisations.org_type')
      ->get();

    $obj->actiontypes = 
      DB::table('take_actions')
      ->whereIn('take_actions.id', $ids)
      ->select('take_actions.type_id as item_id', DB::raw('count(take_actions.id) as record_count'))
      ->groupBy('take_actions.type_id')
      ->get();
    
    //Log::info(DB::getQueryLog());

    return $obj;            
  }

  public function countFiltered($filters, $searchQuery)
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->displayable($query, 'take_actions');
    $this->_filterable->fromActiveAgencies($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select('id')
                  ->count();

    return $data;
  }

  public function countOrganisationsFromFiltered($filters, $searchQuery) 
  {
    $query = TakeAction::where('take_actions.id', '>', '0');

    $this->_filterable->displayable($query, 'take_actions');
    $this->_filterable->fromActiveAgencies($query);
    $this->applySearchQuery($query, $searchQuery);
    $this->_filterable->applyFilters($query, $filters);

    $this->applyPlatformFilter($query, $filters);

    $data = $query->select(DB::raw('organisation_id'))
                  ->groupBy('organisation_id')
                  ->get();

    if ($data === null) {
      return 0;
    }

    return count($data);
  }

  private function applyPlatformFilter($query, $filters)
  {
    // if national platform is selected (>1) 1 is International
    if (isset($filters->platform_id) && isset($filters->platform_country_id)) {
      if ($filters->platform_id > 1) {
        $query->where(function ($query) use ($filters) 
        {
          //platform_id = 1 = international
          //profile_type = 2 = organisation
          $sql = '
          exists 
          (
            select  1 
            from    platforms_profiles 
            where   platforms_profiles.profile_id = take_actions.organisation_id 
            and     platforms_profiles.profile_type = 2 
            and     platforms_profiles.platform_id = '.$filters->platform_id.'
          )
          or
          (
            exists
            (
              select  1
              from    platforms_profiles 
              where   platforms_profiles.profile_id = take_actions.organisation_id 
              and     platforms_profiles.profile_type = 2
              and     platforms_profiles.platform_id = 1
            )
            and
            exists
            (
              select  1
              from    take_actions_countries
              where   take_actions_countries.takeaction_id = take_actions.id
              and     take_actions_countries.country_id = '.$filters->platform_country_id.'
            )
          )';
          $query->whereRaw($sql);
        });
      }
    }
  }

  public function getByOrganisationId($organisationId, $pageNumber, $pageSize)
  {
    $query = TakeAction::where('take_actions.id', '>', '0');
      
    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'take_actions');

    $query->where('take_actions.organisation_id', '=', $organisationId);
    
    $toSkip = $pageSize * ($pageNumber - 1);
    $query->orderBy('take_actions.created_at', 'desc')
          ->skip($toSkip)
          ->take($pageSize);

    $data = $query->get();
    return $data;
  }

  public function countByOrganisationId($organisationId)
  {
    $query = TakeAction::where('take_actions.id', '>', '0');
      
    $this->_filterable->fromActiveAgencies($query);
    $this->_filterable->displayable($query, 'take_actions');

    $query->where('take_actions.organisation_id', '=', $organisationId);
    
    $data = $query->select('id')
                  ->count();

    return $data;
  }

  /*==========================================================================================*/

  private function applySearchQuery($query, $searchQuery) 
  {
    if ($searchQuery !== null && !is_null($searchQuery) && isset($searchQuery) && gettype($searchQuery) === 'string' && strlen($searchQuery) > 0) {
      $query->where(function ($iquery) use ($searchQuery) {
        return $iquery->where('title', 'like', '%'.$searchQuery.'%')
                      ->orwhere('content', 'like', '%'.$searchQuery.'%');
      });
    }
  }

}