<?php namespace Libraries\Repositories;

interface IBaseRepository {

  public function getById($id, array $relations = null);

  public function getPaged(array $conditions = null, array $orderByColumns = null, $pageNumber, $pageSize, array $relations = null);

  public function getSingle(array $conditions = null, array $relations = null);

  public function deleteById($id);

  public function getCount(array $conditions = null);

  public function lastPostByOrg($orgId);

  public function lastPostByInd($userProfileId);

}