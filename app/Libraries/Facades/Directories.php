<?php namespace Libraries\Facades;

use Illuminate\Support\Facades\Facade;

class Directories extends Facade {

  /**
   * [getFacadeAccessor description]
   * @return [type] [description]
   */
  protected static function getFacadeAccessor() { return 'directories'; }

}