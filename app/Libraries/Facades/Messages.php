<?php namespace Libraries\Facades;

use Illuminate\Support\Facades\Facade;

class Messages extends Facade {

  /**
   * [getFacadeAccessor description]
   * @return [type] [description]
   */
  protected static function getFacadeAccessor() { return 'messages'; }

}