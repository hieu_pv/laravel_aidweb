<?php namespace Libraries\Validators;

class PublicityTypeBValidator extends BaseValidator {

  protected $rules = array(
    'id'                     => 'required|integer',
    'b_image_x'              => 'required',
  );

  protected $perRuleMessages = array(
    'b_image_x.required' => 'The Publicty Image is required'
  );

}

