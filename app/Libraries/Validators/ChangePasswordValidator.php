<?php namespace Libraries\Validators;

class ChangePasswordValidator extends BaseValidator {

  protected $rules = array(
    'old_password'   => 'required',
    'new_password'   => 'required|min:8|confirmed',
  );

}

