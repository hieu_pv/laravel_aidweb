<?php namespace Libraries\Validators;

class UserValidator extends BaseValidator {

  /*
  The field under validation must have a matching field of foo_confirmation. 
  For example, if the field under validation is password, 
  a matching password_confirmation field must be present in the input.
  */

  protected $rules = array(
    'id'             => 'required|integer',
    'username'       => 'required|min:8|unique:users',
    'password'       => 'required|min:8|confirmed',
    'email'          => 'required|email|confirmed|unique:users',
    'first_name'     => 'required'
  );

}

