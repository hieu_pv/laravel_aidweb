<?php namespace Libraries\Validators;

class RegisterValidator extends BaseValidator {

  /*
  The field under validation must have a matching field of foo_confirmation. 
  For example, if the field under validation is password, 
  a matching password_confirmation field must be present in the input.
  */

  protected $rules = array(
    'username'                => 'required|unique:users', //|min:8
    'password'                => 'required', //|min:8
    'email'                   => 'required|email', //|confirmed|unique:users',
  );

}

