<?php namespace Libraries\Validators;

use Libraries\Support\Modules;

class UpdateOrganisationValidator extends BaseValidator {

  /*
  The field under validation must have a matching field of foo_confirmation. 
  For example, if the field under validation is password, 
  a matching password_confirmation field must be present in the input.
  */

  protected $rules = array(
    'id'                      => 'required|integer',
    //'username'                => 'required|min:8|unique:users',
    //'password'                => 'required|min:8|confirmed',
    
    // email validation is specific, it's in the controller or service class
    //'email'                   => 'required|email|confirmed|unique:users',
    'first_name'              => 'required',
    'last_name'               => 'required',
    //'job_title'               => 'required',
    
    //'organisation_logo'       => 'required',
    'organisation_name'       => 'required',
    'organisation_email'      => 'required',
    //'organisation_website'    => 'required',
    'office_location'         => 'required|integer',
    // 'office_type'             => 'required|integer',
    'organisation_types'      => 'required|integer',
    'level_of_activities'     => 'required|integer',

    'profile_type'            => 'required|numeric|min:2|max:2',

    'description'             => 'required'
  );

  // protected $rules = array(
  //   'id'                      => 'required|integer',
  //   'username'                => 'required|min:8|unique:users',
  //   'password'                => 'required|min:8|confirmed',
  //   // TODO, email is not unique, further discussion is needed
  //   'email'                   => 'required|email|confirmed', //|unique:users',
  //   'first_name'              => 'required',
  //   'last_name'               => 'required',
  //   'job_title'               => 'required',
  //   'organisation_logo'       => 'required',
  //   'organisation_name'       => 'required',
  //   'organisation_email'      => 'required',
  //   'organisation_website'    => 'required',
  //   'office_location'         => 'required|integer',
  //   'office_type'             => 'required|integer',
  //   'organisation_types'      => 'required|integer',
  //   'profile_type'            => 'required|numeric|min:2|max:2'
  // );

}

