<?php namespace Libraries\Validators;

class PublicityTypeCValidator extends BaseValidator {

  protected $rules = array(
    'id'                     => 'required|integer',
    'c_image_x'              => 'required',
  );

  protected $perRuleMessages = array(
    'c_image_x.required' => 'The Publicty Image is required'
  );

}

