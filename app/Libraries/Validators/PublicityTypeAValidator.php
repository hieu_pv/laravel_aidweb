<?php namespace Libraries\Validators;

class PublicityTypeAValidator extends BaseValidator {

  protected $rules = array(
    'id'                     => 'required|integer',
    //'a_title'                => 'required', // this is now actually a publicity name
    //'a_title_color'          => 'required',
    //'a_text'                 => 'required',
    //'a_text_color'           => 'required', // text color is for both title and text
    //'a_button_text'          => 'required',
    //'a_button_text_color'    => 'required', // button text color
    //'a_position'             => 'required', // position of the publicity caption
    'a_image_x'              => 'required', // image of the pub
  );

  protected $perRuleMessages = array(
    'a_image_x.required' => 'The Publicty Image is required'
  );

}

