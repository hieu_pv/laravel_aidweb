<?php namespace Libraries\Validators;

class TakeActionValidator extends BaseValidator {

  protected $rules = array(
    'id'          => 'required|integer',
    'name'        => 'required',
    'title'       => 'required',
    'slug'        => 'required',
    'content'     => 'required',
    'url'         => 'required',
    'type_id'     => 'required|integer|min:1'
  );

  protected $perRuleMessages = array(
    'type_id.min' => 'The Action type field is required',
  );

}

