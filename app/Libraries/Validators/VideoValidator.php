<?php namespace Libraries\Validators;

class VideoValidator extends BaseValidator {

  protected $rules = array(
    'id'          => 'required|integer',
    'title'       => 'required',
    'slug'        => 'required',
    'url'         => 'required',
    'region'      => 'required|integer|min:1',
    'theme'       => 'required|integer|min:1',
  );

  protected $perRuleMessages = array(
    'region.min' => 'The Region field is required',
    'theme.min' => 'The Theme field is required',
  );

}

