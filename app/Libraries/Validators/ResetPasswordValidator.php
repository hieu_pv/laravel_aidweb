<?php namespace Libraries\Validators;

class ResetPasswordValidator extends BaseValidator {

  protected $rules = array(
    'username'       => 'required',
    'email'          => 'required',
    'new_password'   => 'required|min:8|confirmed',
    'token'          => 'required'
  );

}

