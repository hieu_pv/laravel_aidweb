<?php namespace Libraries\Validators;

use Illuminate\Support\Facades\Validator;

class BaseValidator {

  protected $rules = array();

  protected $errors;
  protected $messages;

  protected $perRuleMessages;

  public function validate($data, $additionalRules = array()) 
  {
    $oRules = array_merge($this->rules, $additionalRules);
    
    $v = Validator::make($data, $oRules);
    if (isset($this->perRuleMessages) && !is_null($this->perRuleMessages) && $this->perRuleMessages !== null && is_array($this->perRuleMessages)) {
      $v = Validator::make($data, $oRules, $this->perRuleMessages);
    }

    // check for failure
    if ($v->fails())
    {
      // set errors and return false
      $this->errors = $v->errors();
      $this->messages = $v->messages();
      return false;
    }

    // validation pass
    return true;
  }

  public function errors()
  {
    return $this->errors;
  }

  public function messages()
  {
    return $this->messages;
  }

}

