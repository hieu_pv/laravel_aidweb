<?php namespace Libraries\Validators;

use Libraries\Support\Modules;

class UpdateIndividualValidator extends BaseValidator {

  /*
  The field under validation must have a matching field of foo_confirmation. 
  For example, if the field under validation is password, 
  a matching password_confirmation field must be present in the input.
  */

  protected $rules = array(
    'id'                        => 'required|integer',
    // 'username'                => 'required|min:8|unique:users',
    // 'password'                => 'required|min:8|confirmed',
    // 'email'                   => 'required|email|confirmed|unique:users,email,', // << this validation in controller cause include the ID
    'first_name'                => 'required',
    'last_name'                 => 'required',
    
    'nationality'               => 'required|integer',
    'bases'                     => 'required|integer',
    'gender'                    => 'required',

    //'thought'                 => 'required',

    'member_statuses'           => 'required',
    'professional_statuses'     => 'required',

    //'avatar'                  => 'required',
    'profile_type'              => 'required|numeric|min:1|max:1',

    'description'               => 'required',

    //'level_of_responsibilities' => 'required|integer',
  );

  // protected $rules = array(
  //   'id'                      => 'required|integer',
  //   'username'                => 'required|min:8|unique:users',
  //   'password'                => 'required|min:8|confirmed',
  //   // TODO, email is not unique, further discussion is needed
  //   'email'                   => 'required|email|confirmed', //|unique:users',
  //   'first_name'              => 'required',
  //   'last_name'               => 'required',
  //   'nationality'             => 'required|integer',
  //   'bases'                   => 'required|integer',
  //   'thought'                 => 'required',
  //   'member_statuses'         => 'required',
  //   'professional_statuses'   => 'required',
  //   'avatar'                  => 'required',
  //   'profile_type'            => 'required|numeric|min:1|max:1',
  //   'gender'                  => 'required'
  // );

}

