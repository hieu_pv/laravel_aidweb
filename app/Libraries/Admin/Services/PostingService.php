<?php namespace Libraries\Admin\Services;

use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class PostingService
{

    private $_publicityRepository;
    private $_postRepository;
    private $_newsRepository;
    private $_videoRepository;
    private $_takeactionRepository;

    public function __construct()
    {
        $this->_publicityRepository = App::make('\Libraries\Repositories\Publicities\IPublicityRepository');
        $this->_postRepository = App::make('\Libraries\Repositories\Blog\IPostRepository');
        $this->_newsRepository = App::make('\Libraries\Repositories\News\INewsItemRepository');
        $this->_videoRepository = App::make('\Libraries\Repositories\Videos\IVideoRepository');
        $this->_takeactionRepository = App::make('\Libraries\Repositories\TakeActions\ITakeActionRepository');
    }

    /**
     * [getLastPosts description]
     * @return [type] [description]
     */
    public function getLastPosts()
    {
        $id = 0;
        if (Auth::user()->isOrganisation()) {
            $id = Auth::user()->organisation->id;
        } else if (Auth::user()->isIndividual()) {
            $id = Auth::user()->userProfile->id;
        }

        $o = new \stdClass();
        $o->lastposts = [];
        if ($id > 0) {
            if (Auth::user()->isIndividual()) {
                $lastBlog = $this->_postRepository->lastPostByInd($id);
                $item = new \stdClass();
                $item->kind = 'Blog';
                $item->title = $lastBlog !== null ? $lastBlog->title : '-';
                $item->datetime = $lastBlog !== null ? $lastBlog->created_at : '-';
                $item->humandatetime = $lastBlog !== null ? Carbon::createFromTimestamp(strtotime($lastBlog->created_at))->diffForHumans() : '-';
                array_push($o->lastposts, $item);
            }

            if (Auth::user()->isOrganisation()) {
                $lastNews = $this->_newsRepository->lastPostByOrg($id);
                $item = new \stdClass();
                $item->kind = 'News';
                $item->title = $lastNews !== null ? $lastNews->title : '-';
                $item->datetime = $lastNews !== null ? $lastNews->created_at : '-';
                $item->humandatetime = $lastNews !== null ? Carbon::createFromTimestamp(strtotime($lastNews->created_at))->diffForHumans() : '-';
                array_push($o->lastposts, $item);

                $lastVideos = $this->_videoRepository->lastPostByOrg($id);
                $item = new \stdClass();
                $item->kind = 'Video';
                $item->title = $lastVideos !== null ? $lastVideos->title : '-';
                $item->datetime = $lastVideos !== null ? $lastVideos->created_at : '-';
                $item->humandatetime = $lastVideos !== null ? Carbon::createFromTimestamp(strtotime($lastVideos->created_at))->diffForHumans() : '-';
                array_push($o->lastposts, $item);

                $lastTakeActions = $this->_takeactionRepository->lastPostByOrg($id);
                $item = new \stdClass();
                $item->kind = 'Get Involved';
                $item->title = $lastTakeActions !== null ? $lastTakeActions->title : '-';
                $item->datetime = $lastTakeActions !== null ? $lastTakeActions->created_at : '-';
                $item->humandatetime = $lastTakeActions !== null ? Carbon::createFromTimestamp(strtotime($lastTakeActions->created_at))->diffForHumans() : '-';
                array_push($o->lastposts, $item);
            }
        } else {

        }
        return $o;
    }
    public function removePostingLimitation()
    {
        $categories = ['posts', 'news', 'takeactions', 'videos', 'publicities'];
        foreach ($categories as $category) {
            Session::forget('PostingLimitation_' . $category);
        }
    }
    public function removePostingLimitationInPilotPhase()
    {
        $this->forgetSessionCanPostMoreThanOneIn24Hours('publicities');
        $this->forgetSessionCanPostMoreThanOneIn24Hours('news');
        $this->forgetSessionCanPostMoreThanOneIn24Hours('takeactions');
        $this->forgetSessionCanPostMoreThanOneIn24Hours('videos');
    }

    private function forgetSessionCanPostMoreThanOneIn24Hours($category)
    {
        Session::forget('canPostMoreThanOneIn24Hours_' . $category);
        Session::forget('canPostMoreThanOneIn24Hours_lastpost_' . $category);
    }
    public function disable_new_post()
    {
        $this->checkPostingLimitation();
        $request_uri = $_SERVER['REQUEST_URI'];
        if (strpos($request_uri, '/publicities') !== false) {
            if (Session::has('PostingLimitation_publicities')) {
                die();
            }
        } else if (strpos($request_uri, '/news') !== false) {
            if (Session::has('PostingLimitation_news')) {
                die();
            }
        } else if (strpos($request_uri, '/takeactions') !== false) {
            if (Session::has('PostingLimitation_take')) {
                die();
            }
        } else if (strpos($request_uri, '/videos') !== false) {
            if (Session::has('PostingLimitation_vide')) {
                die();
            }
        }
    }
    public function checkPostingLimitation()
    {
        if (Auth::check()) {
            // get org/ind id
            $id = 0;
            if (Auth::user()->isOrganisation()) {
                $id = Auth::user()->organisation->id;
            } else if (Auth::user()->isIndividual()) {
                $id = Auth::user()->userProfile->id;
            }
            $request_uri = $_SERVER['REQUEST_URI'];
            if ($id > 0) {
                // get last posting based on category/module
                if (strpos($request_uri, '/publicities/create') !== false) {
                    $lastPost = $this->_publicityRepository->lastPostByOrg($id);
                    if (isset($lastPost)) {
                        $this->handlePostingLimitation($lastPost, 'publicities');
                    } else {
                        Session::forget('PostingLimitation_publicities');
                    }
                } else if (strpos($request_uri, '/news/create') !== false) {
                    $lastNews = $this->_newsRepository->lastPostByOrg($id);
                    if (isset($lastNews)) {
                        $this->handlePostingLimitation($lastNews, 'news');
                    } else {
                        Session::forget('PostingLimitation_news');
                    }
                } else if (strpos($request_uri, '/takeactions/create') !== false) {
                    $lastTakeAction = $this->_takeactionRepository->lastPostByOrg($id);
                    if (isset($lastTakeAction)) {
                        $this->handlePostingLimitation($lastTakeAction, 'takeactions');
                    } else {
                        Session::forget('PostingLimitation_takeactions');
                    }
                } else if (strpos($request_uri, '/videos/create') !== false) {
                    $lastVideo = $this->_videoRepository->lastPostByOrg($id);
                    if (isset($lastVideo)) {
                        $this->handlePostingLimitation($lastVideo, 'videos');
                    } else {
                        Session::forget('PostingLimitation_videos');
                    }
                } else if (strpos($request_uri, '/posts/create') !== false) {
                    $lastPost = $this->_postRepository->lastPostByInd($id);
                    if (isset($lastPost)) {
                        $this->handlePostingLimitation($lastPost, 'posts');
                    } else {
                        Session::forget('PostingLimitation_lastPost');
                    }
                }
            }
        }
    }
    public function checkForPostingLimitationInPilotPhase($url)
    {
        if (Auth::check()) {
            // get org/ind id
            $id = 0;
            if (Auth::user()->isOrganisation()) {
                $id = Auth::user()->organisation->id;
            } else if (Auth::user()->isIndividual()) {
                $id = Auth::user()->userProfile->id;
            }

            if ($id > 0) {
                // get last posting based on category/module
                if (strpos($url, '/posts/') !== false) {
                    $lastBlog = $this->_postRepository->lastPostByInd($id);
                    $this->handlePostingMoreThanOneIn24Hours($lastBlog, 'posts');
                } else if (strpos($url, '/news/') !== false) {
                    $lastNews = $this->_newsRepository->lastPostByOrg($id);
                    $this->handlePostingMoreThanOneIn24Hours($lastNews, 'news');
                } else if (strpos($url, '/takeactions/') !== false) {
                    $lastTakeAction = $this->_takeactionRepository->lastPostByOrg($id);
                    $this->handlePostingMoreThanOneIn24Hours($lastTakeAction, 'takeactions');
                } else if (strpos($url, '/videos/') !== false) {
                    $lastVideo = $this->_videoRepository->lastPostByOrg($id);
                    $this->handlePostingMoreThanOneIn24Hours($lastVideo, 'videos');
                }
            }
        }
    }
    private function handlePostingLimitation($lastPost, $category)
    {
        $last_post_time = new DateTime($lastPost->created_at);
        $current_time = new DateTime();
        $time_diff = $current_time->diff($last_post_time);
        if ($time_diff->days < Config::get('app.posting_limitation_day')) {
            Session::put('PostingLimitation_' . $category, $time_diff);
        } else {
            if (Session::has('PostingLimitation_' . $category)) {
                Session::forget('PostingLimitation_' . $category);
            }
        }
    }
    private function handlePostingMoreThanOneIn24Hours($lastPost, $category)
    {
        $hoursDiff = 24;

        // check the hours diff
        if ($lastPost !== null) {
            $hoursDiff = Carbon::createFromTimestamp(strtotime($lastPost->created_at))->diffInHours();
        } else {
            $hoursDiff = 999999;
        }

        // check if can post within 24 hours
        $canPostMoreThanOneIn24Hours = false;
        if ($hoursDiff > 24) {
            $canPostMoreThanOneIn24Hours = true;
        }

        Session::put('canPostMoreThanOneIn24Hours_' . $category, $canPostMoreThanOneIn24Hours);
        Session::put('canPostMoreThanOneIn24Hours_lastpost_' . $category, $lastPost);
    }

}
