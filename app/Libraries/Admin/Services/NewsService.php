<?php namespace Libraries\Admin\Services;

use Libraries\Models\News\NewsItem;
use Libraries\Repositories\News\INewsItemRepository;
use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Support\Modules;
use Libraries\Facades\Directories;
use Libraries\Facades\GenericUtility;

class NewsService {
  
  private $_newsItemRepository;
  private $_organisationRepository;
  private $_filtersHandler;

  /**
   * [__construct description]
   * @param INewsItemRepository $modelRepository [description]
   */
  public function __construct(INewsItemRepository $modelRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_newsItemRepository = $modelRepository;
    $this->_organisationRepository = $organisationRepository;
    $this->_filtersHandler = new FiltersHandler();
  }

  /**
   * [getPaginatedNewsItems description]
   * @param  [type] $pageNumber [description]
   * @param  [type] $pageSize   [description]
   * @param  array  $sorts      [description]
   * @param  array  $conditions [description]
   * @return [type]             [description]
   */
  public function getPaginatedNewsItems($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'created_at', 'direction' => 'desc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array('regions', 'countries', 'crisis', 'sectors', 'themes', 'beneficiaries');

    $results = $this->_newsItemRepository->getPaged($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  /**
   * [getNewsItemsCount description]
   * @param  array  $conditions [description]
   * @return [type]             [description]
   */
  public function getNewsItemsCount(array $conditions)
  {
    $result = $this->_newsItemRepository->getCount($conditions);

    return $result;
  }

  /**
   * [getById description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getById($id) 
  {
    $model = $this->_newsItemRepository->getById($id);
    
    return $model;
  }

  /**
   * [insertNewsItem description]
   * @param  array  $input [description]
   * @param  [type] $user  [description]
   * @return [type]        [description]
   */
  public function insertNewsItem(array $input, $user) 
  {
    $model = new NewsItem();
    $model->id = 0;
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->image_legend = $input['image_legend'];
    $model->photographer_name = $input['photographer_name'];

    $model->published = true;
    $model->featured = true;

    // news is for organisation, so force to enter only the user profile and organisation
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    $model->save();

    $this->_filtersHandler->process($model, $input);

    $this->handleImage($input, $model);

    return $model;
  }

  /**
   * [updateNewsItem description]
   * @param  array  $input [description]
   * @return [type]        [description]
   */
  public function updateNewsItem(array $input, $user)
  {
    $id = $input['id'];
    $model = $this->_newsItemRepository->getById($id);
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->image_legend = $input['image_legend'];
    $model->photographer_name = $input['photographer_name'];

    $model->save();

    $this->_filtersHandler->process($model, $input);

    $this->handleImage($input, $model);

    return $model;
  }

  /**
   * [handleImage description]
   * @param  [type] $model [description]
   * @return [type]           [description]
   */
  private function handleImage(array $input, $model)
  {
    try {
      if (array_key_exists('image', $input)) {
        $file = $input['image'];
        if ($file !== null && isset($file) && !is_null($file) && ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) && $file->isValid()) {
          $content = file_get_contents($file->getRealPath());
          $stored = Directories::getMemberModuleDirectory(Modules::NEWS).rand().'_'.$file->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          // save image path to db
          $model->image = $stored;
          $model->save();
          return true;
        }
      }
      return false;
    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * [deleteNewsItem description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteNewsItem($id)
  {
    $model = $this->_newsItemRepository->deleteById($id);
  }

  /**
   * [publishNewsItem description]
   * @param  [type] $id   [description]
   * @param  [type] $user [description]
   * @return [type]       [description]
   */
  public function publishNewsItem($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_newsItemRepository->getById($id);
      $model->published = true;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  /**
   * [unpublishNewsItem description]
   * @param  [type] $id   [description]
   * @param  [type] $user [description]
   * @return [type]       [description]
   */
  public function unpublishNewsItem($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_newsItemRepository->getById($id);
      $model->published = false;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  /**
   * [getAllAuthors description]
   * @return [type] [description]
   */
  public function getAllAuthors() 
  {
    return $this->_organisationRepository->getAuthoringOrganisations();
  }

  public function fromInputToModelForPreview(array $input, $user) 
  {
    $model = new NewsItem();
    $model->id = 0;
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->image_legend = $input['image_legend'];
    $model->photographer_name = $input['photographer_name'];
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    try {
      $file = null;
      $path = '';

      if (array_key_exists('image', $input)) { 
        $file = $input['image'];

        if ($file !== null && isset($file) && !is_null($file) && !empty($file)) {
          $path = $file->getRealPath();
        } else {
          $file = $input['original_image']; 
          $path = $_SERVER['DOCUMENT_ROOT'].$file;
        }
      } else {
        $file = $input['original_image'];  
        $path = $_SERVER['DOCUMENT_ROOT'].$file;
      }

      if ($file !== null) {
        $content = file_get_contents($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($content);
        $model->image_base64 = $base64;
      }
    } catch (\Exception $e) {
    }

    return $model;
  }

}