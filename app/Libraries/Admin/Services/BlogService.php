<?php namespace Libraries\Admin\Services;

//use Symfony\Component\HttpFoundation\File\UploadedFile;
use Libraries\Models\Blog\Post;
use Libraries\Repositories\Blog\IPostRepository;
use Libraries\Repositories\IUserProfileRepository;
use Libraries\Facades\Directories;
use Libraries\Support\Modules;
use Libraries\Facades\GenericUtility;

class BlogService {
  
  private $_postRepository;
  private $_userProfileRepository;
  private $_filtersHandler;

  /**
   * [__construct description]
   * @param IPostRepository $_postRepository [description]
   */
  public function __construct(IPostRepository $modelRepository,
                              IUserProfileRepository $userProfileRepository)
  {
    $this->_postRepository = $modelRepository;
    $this->_userProfileRepository = $userProfileRepository;
    $this->_filtersHandler = new FiltersHandler();
  }

  /**
   * [getPaginatedPosts description]
   * @param  [type] $pageNumber [description]
   * @param  [type] $pageSize   [description]
   * @param  array  $sorts      [description]
   * @param  array  $conditions [description]
   * @return [type]             [description]
   */
  public function getPaginatedPosts($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'created_at', 'direction' => 'desc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array('regions', 'countries', 'crisis', 'sectors', 'themes', 'beneficiaries');

    $results = $this->_postRepository->getPaged($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  /**
   * [getPostsCount description]
   * @param  array  $conditions [description]
   * @return [type]             [description]
   */
  public function getPostsCount(array $conditions)
  {
    $result = $this->_postRepository->getCount($conditions);

    return $result;
  }

  /**
   * [getById description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getById($id) 
  {
    $model = $this->_postRepository->getById($id);
    
    return $model;
  }

  /**
   * [insertPost description]
   * @param  [type] $input [description]
   * @return [type]        [description]
   */
  public function insertPost(array $input, $user) 
  {
    $model = new Post();
    $model->id = 0;
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->image_source = $input['image_source'];
    $model->image_legend = $input['image_legend'];

    $model->published = true;
    $model->featured = true;
    
    // post is for individual, so force to enter only the user profile
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;

    $model->save();

    $this->_filtersHandler->process($model, $input);

    $this->handleImage($input, $model);

    return $model;
  }

  /**
   * [updatePost description]
   * @param  [type] $input [description]
   * @return [type]        [description]
   */
  public function updatePost(array $input, $user)
  {
    $id = $input['id'];
    $model = $this->_postRepository->getById($id);
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->image_source = $input['image_source'];
    $model->image_legend = $input['image_legend'];

    $model->save();

    $this->_filtersHandler->process($model, $input);
    
    $this->handleImage($input, $model);

    return $model;
  }

  /**
   * [handleImage description]
   * @param  [type] $model [description]
   * @return [type]       [description]
   */
  private function handleImage(array $input, $model)
  {
    try {
      if(array_key_exists('image', $input)) {
        $file = $input['image'];
        if ($file !== null && isset($file) && !is_null($file) && ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) && $file->isValid()) {
          $content = file_get_contents($file->getRealPath());
          $stored = Directories::getMemberModuleDirectory(Modules::BLOG).rand().'_'.$file->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          // save image path to db
          $model->image = $stored;
          $model->save();
          return true;
        }
      }
      return false;
    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * [deletePost description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deletePost($id)
  {
    $model = $this->_postRepository->deleteById($id);
  }

  /**
   * [publishPost description]
   * @param  [type] $id   [description]
   * @param  [type] $user [description]
   * @return [type]       [description]
   */
  public function publishPost($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_postRepository->getById($id);
      $model->published = true;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  /**
   * [unpublishPost description]
   * @param  [type] $id   [description]
   * @param  [type] $user [description]
   * @return [type]       [description]
   */
  public function unpublishPost($id, $user)
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_postRepository->getById($id);
      $model->published = false;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  /**
   * [getAllAuthors description]
   * @return [type] [description]
   */
  public function getAllAuthors()
  {
    return $this->_userProfileRepository->getAuthoringUsers();
  }

  public function fromInputToModelForPreview(array $input, $user) 
  {
    $model = new Post();
    $model->id = 0;
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->image_legend = $input['image_legend'];
    $model->image_source = $input['image_source'];
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;

    try {
      $file = null;
      $path = '';

      if (array_key_exists('image', $input)) { 
        $file = $input['image'];

        if ($file !== null && isset($file) && !is_null($file) && !empty($file)) {
          $path = $file->getRealPath();
        } else {
          $file = $input['original_image']; 
          $path = $_SERVER['DOCUMENT_ROOT'].$file;
        }
      } else {
        $file = $input['original_image'];  
        $path = $_SERVER['DOCUMENT_ROOT'].$file;
      }

      if ($file !== null) {
        $content = file_get_contents($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($content);
        $model->image_base64 = $base64;
      }
    } catch (\Exception $e) {
    }

    return $model;
  }

}