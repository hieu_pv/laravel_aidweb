<?php namespace Libraries\Admin\Services;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as FilesystemAdapter;
//use League\Flysystem\Dropbox\DropboxAdapter;
//use Dropbox\Client;

class FileService {

  //private $_filesystem;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    //$this->_filesystem = new Filesystem(new FilesystemAdapter($this->getMediaFullPath()));
  }

  /**
   * [getFilesInDirectory description]
   * @param  [type] $directoryRelativePath [description]
   * @return [type]               [description]
   */
  public function getFilesInDirectory($directoryRelativePath)
  {
    $directory = $_SERVER['DOCUMENT_ROOT'].'/'.ltrim($directoryRelativePath, '/');
    
    $filesystem = new Filesystem(new FilesystemAdapter($directory));
    $contents = $filesystem->listContents();

    $files = array();

    // $iterator = new \DirectoryIterator($directory);
    // foreach ($iterator as $fileinfo) {
    //   if (($fileinfo->isFile() || $fileinfo->isDir()) && !$fileinfo->isDot()) {
    //     $files[] = $fileinfo->getFilename();
    //   }
    // }
    foreach ($contents as $key => $value) {
      $files[] = $contents[$key]['basename'];
    }

    $fileModels = array();
    foreach ($files as $file) {
      if ($file === '.DS_Store' || $file === '.gitignore') {
        continue;
      }
      // create model object
      $fileModel = new \Libraries\Models\FileModel($directoryRelativePath, $file);
      $fileModels[] = $fileModel;
    }
    
    usort($fileModels, function ($a, $b) {
      return strcmp($a->fileName, $b->fileName);
    });

    return $fileModels;
  }

  public function createFolder($fullFolderPath)
  {
    if (!file_exists($fullFolderPath)) {
      mkdir($fullFolderPath, 0777, true);
    }
  }

  public function renameFile($oldName, $newName)
  {
    rename($oldName, $newName);
  }

  public function destroyFile($file) 
  {
    if (is_dir($file)) {
      $iterator = new \DirectoryIterator($file);
      foreach ($iterator as $fileinfo) {
        if ($fileinfo->isFile() && !$fileinfo->isDot()) {
          unlink($fileinfo->getPathname());
        } else if ($fileinfo->isDir() && !$fileinfo->isDot()) {
          //recursive here 
          $this->destroyFile($fileinfo->getPathname());
        }
      }
      rmdir($file);
    } else {
      unlink($file);
    }
  }

  public function uploadFiles($destinationFolder, $destinationUrl, $files)
  {
    $results = array();

    if ($files) {
      if (is_array($files['tmp_name'])) {
        // param_name is an array identifier like "files[]",
        // $file is a multi-dimensional array:
        foreach ($files['tmp_name'] as $index => $value) {
          $fullDestinationPath = $destinationFolder.$files['name'][$index];
          $fullDestinationRelativePath = $destinationUrl.$files['name'][$index];

          move_uploaded_file($files['tmp_name'][$index], $fullDestinationPath);

          $result = new \stdClass();
          $result->name = $files['name'][$index];
          $result->thumbnailUrl = $fullDestinationRelativePath;
          clearstatcache();
          $result->size = filesize($fullDestinationPath);

          $results[] = $result;
        }
      } else {
        // skip, TODO
      }
    }

    return $results;
  }

}