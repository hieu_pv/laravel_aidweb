<?php namespace Libraries\Admin\Services;

use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IUserProfileRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Facades\Directories;
use Libraries\Models\UserProfile;

class UserService {

  private $_userRepository;
  private $_userProfileRepository;
  private $_organisationRepository;

  public function __construct(IUserRepository $userRepository,
                              IUserProfileRepository $userProfileRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_userRepository = $userRepository;
    $this->_userProfileRepository = $userProfileRepository;
    $this->_organisationRepository = $organisationRepository;
  }

  public function getIndividuals($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'first_name', 'direction' => 'asc'),
        array('column' => 'last_name', 'direction' => 'asc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array();

    $results = $this->_userProfileRepository->getIndividuals($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  public function getIndividualsCount(array $conditions)
  {
    $result = $this->_userProfileRepository->getIndividualsCount($conditions);

    return $result;
  }

  public function revokeMembership($indId)
  {
    $ret = new \stdClass();
    $ret->revoked = false;
    $ret->original = null;
    $ret->int_id = $indId;
    try {
      $individual = $this->_userProfileRepository->getById($indId);
      if ($individual !== null && isset($individual) && !is_null($individual)) {
        $individual->is_active = false;
        $individual->save();
        $ret->revoked = true;
        $ret->original = $individual;
      }
      return $ret;
    } catch (\Exception $ex) {
      $ret->ex_message = $ex;
      return $ret;
    }
  }

  public function activateMembership($indId)
  {
    $ret = new \stdClass();
    $ret->activated = false;
    $ret->original = null;
    $ret->int_id = $indId;
    try {
      $individual = $this->_userProfileRepository->getById($indId);
      if ($individual !== null && isset($individual) && !is_null($individual)) {
        $individual->is_active = true;
        $individual->save();
        $ret->activated = true;
        $ret->original = $individual;
      }
      return $ret;
    } catch (\Exception $ex) {
      $ret->ex_message = $ex;
      return $ret;
    }
  }

  public function getPlatformsAssignedToIndividual($indId)
  {
    $platforms = $this->_userProfileRepository->getPlatformsAssignedToIndividual($indId);
    return $platforms;
  }

  // public function setTopMember($indId) 
  // {
  //   $ret = new \stdClass();
  //   $ret->ok = false;
  //   $ret->agency = null;
  //   $ret->int_id = $indId;

  //   try {
  //     $individual = $this->_userProfileRepository->getById($indId);
      
  //     if ($individual !== null && isset($individual) && !is_null($individual)) {
  //       $individual->top_member = true;
  //       $individual->save();
      
  //       $ret->ok = true;
  //       $ret->agency = $individual;
  //     }

  //     return $ret;
  //   } catch (\Exception $ex) {
  //     $ret->ex_message = $ex;
  //     return $ret;
  //   }
  // }

  // public function unsetTopMember($indId) 
  // {
  //   $ret = new \stdClass();
  //   $ret->ok = false;
  //   $ret->agency = null;
  //   $ret->int_id = $indId;

  //   try {
  //     $individual = $this->_userProfileRepository->getById($indId);
      
  //     if ($individual !== null && isset($individual) && !is_null($individual)) {
  //       $individual->top_member = false;
  //       $individual->save();
      
  //       $ret->ok = true;
  //       $ret->agency = $individual;
  //     }
      
  //     return $ret;
  //   } catch (\Exception $ex) {
  //     $ret->ex_message = $ex;
  //     return $ret;
  //   }
  // }

  private function getUser($userId)
  {
    $user = $this->_userRepository->getById($userId);
    if ($user !== null) {
      // should we check for verification? no for now
      // sanitize for display
      $user->sanitize();
      return $user;
    } else {
      return null;
    }
  }

  public function getIndividual($profileId)
  {
    $profile = UserProfile::find($profileId);
    if ($profile === null) {
      return null;
    }

    $userId = $profile->user_id;
    $user = $this->getUser($userId);
    if ($user === null) {
      return null;
    }

    if (!$user->isIndividual()) {
      return null;
    }

    return $user;
  }

}