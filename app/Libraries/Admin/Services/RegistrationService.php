<?php namespace Libraries\Admin\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IUserProfileRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Repositories\Lookups\INationalityRepository;
use Libraries\Repositories\Lookups\IMemberStatusRepository;
use Libraries\Repositories\Lookups\IProfessionalStatusRepository;
use Libraries\Repositories\Lookups\IOfficeTypeRepository;
use Libraries\Repositories\Lookups\IOrganisationTypeRepository;
use Libraries\Repositories\Lookups\ILevelOfActivityRepository;
use Libraries\Repositories\Lookups\ILevelOfResponsibilityRepository;
use Libraries\Repositories\Filters\ICountryRepository;
use Libraries\Models\User;
use Libraries\Models\UserProfile;
use Libraries\Models\PasswordReminder;
use Libraries\Models\Organisation;
use Libraries\Models\Registration;
use Libraries\Facades\Directories;
use Libraries\Facades\GenericUtility;
use Libraries\Facades\Messages;
use Libraries\Facades\Modules;

class RegistrationService {

  private $_userRepository;
  private $_userProfileRepository;
  private $_organisationRepository;
  private $_nationalityRepository;
  private $_memberStatusRepository;
  private $_professionalStatusRepository;
  private $_officeTypeRepository;
  private $_organisationTypeRepository;
  private $_countryRepository;
  private $_levelOfActivityRepository;
  private $_levelOfResponsibilityRepository;

  public function __construct(IUserRepository $userRepository,
                              IUserProfileRepository $userProfileRepository,
                              IOrganisationRepository $organisationRepository,
                              INationalityRepository $nationalityRepository,
                              IMemberStatusRepository $memberStatusRepository,
                              IProfessionalStatusRepository $professionalStatusRepository,
                              IOfficeTypeRepository $officeTypeRepository,
                              IOrganisationTypeRepository $organisationTypeRepository,
                              ICountryRepository $countryRepository,
                              ILevelOfActivityRepository $levelOfActivityRepository,
                              ILevelOfResponsibilityRepository $levelOfResponsibilityRepository)
  {
    $this->_userRepository = $userRepository;
    $this->_userProfileRepository = $userProfileRepository;
    $this->_organisationRepository = $organisationRepository;
    $this->_nationalityRepository = $nationalityRepository;
    $this->_memberStatusRepository = $memberStatusRepository;
    $this->_professionalStatusRepository = $professionalStatusRepository;
    $this->_officeTypeRepository = $officeTypeRepository;
    $this->_organisationTypeRepository = $organisationTypeRepository;
    $this->_countryRepository = $countryRepository;
    $this->_levelOfActivityRepository = $levelOfActivityRepository;
    $this->_levelOfResponsibilityRepository = $levelOfResponsibilityRepository;
  }

  /**
   * [createEmptyRegistrationModel description]
   * @return [type] [description]
   */
  public function createEmptyRegistrationModel()
  {
    $model = new \stdClass();
    $model->id = 0;
    $model->username = '';
    $model->password = '';
    $model->email = '';
    $model->email_confirmation = '';

    $model->first_name = '';
    $model->last_name = '';
    $model->nationality = 0;
    $model->based_in = 0;
    $model->job_title = '';
    $model->employer_name = '';
    $model->employer_organisation_types = '';

    $model->thought = '';
    $model->member_statuses = 0;
    $model->professional_statuses = 0;
    $model->avatar = '/images/noimage.png';

    $model->organisation_name = '';
    $model->organisation_email = '';
    $model->organisation_website = '';
    $model->organisation_phone = '';
    $model->organisation_logo = '/images/noimage.png';
    $model->office_location = '';
    $model->office_type = '';
    $model->organisation_types = '';
    $model->level_of_activities = '';
    $model->level_of_responsibilities = '';

    $model->description = '';
    $model->gender = '';

    $model->org_name_acronym = '';
    $model->use_org_name_acronym = 0;

    $model->website_url = '';
    $model->twitter_url = '';
    $model->facebook_url = '';

    $model->cover_image = '/images/noimage2.jpg';

    return $model;
  }

  /**
   * [createIndividualRegistrationModel description]
   * @return [type] [description]
   */
  public function createIndividualRegistrationModel($user = null)
  {
    $model = new \stdClass();
    // registration model
    if (is_null($user) || $user === null) {
      $model = $this->createEmptyRegistrationModel();
    } else {
      // update profile model
      $model->id = $user->id;
      $model->username = $user->username;
      $model->password = '';
      $model->first_name = $user->userProfile->first_name;
      $model->last_name = $user->userProfile->last_name;
      $model->nationality = $user->userProfile->relatedNationality === null ? 0 : $user->userProfile->relatedNationality->id;
      $model->based_in = $user->userProfile->relatedBase === null ? 0 : $user->userProfile->relatedBase->id;
      $model->email = $user->email;
      $model->email_confirmation = $user->email;
      $model->job_title = $user->userProfile->job_title;
      $model->employer_name = $user->userProfile->employer_name;
      $model->employer_organisation_types = $user->userProfile->relatedEmployerOrganisationType === null ? 0 : $user->userProfile->relatedEmployerOrganisationType->id;
      $model->thought = $user->userProfile->thought;
      $model->member_statuses = $user->userProfile->relatedMemberStatus === null ? 0 : $user->userProfile->relatedMemberStatus->id;
      $model->professional_statuses = $user->userProfile->relatedProfessionalStatus === null ? 0 : $user->userProfile->relatedProfessionalStatus->id;
      $model->avatar = $user->userProfile->avatar;
      $model->cover_image = $user->userProfile->cover_image;
      $model->description = $user->userProfile->description;
      $model->gender = $user->userProfile->gender;
      $model->website_url = GenericUtility::prefixWithHttpIfNeeded($user->userProfile->website_url);
      $model->facebook_url = GenericUtility::prefixWithHttpIfNeeded($user->userProfile->facebook_url);
      $model->twitter_url = GenericUtility::prefixWithHttpIfNeeded($user->userProfile->twitter_url);
      $model->user_profile_id = $user->userProfile->id;
      $model->level_of_responsibilities = $user->userProfile->relatedLevelOfResponsibility === null ? 0 : $user->userProfile->relatedLevelOfResponsibility->id;
    }
    return $model;
  }

  /**
   * [createOrganisationRegistrationModel description]
   * @return [type] [description]
   */
  public function createOrganisationRegistrationModel($user = null)
  {
    $model = new \stdClass();
    // registration model
    if (is_null($user) || $user === null) {
      $model = $this->createEmptyRegistrationModel();
    } else {
      // update profile model
      $model->id = $user->id;
      $model->username = $user->username;
      $model->password = '';
      $model->first_name = $user->userProfile->first_name;
      $model->last_name = $user->userProfile->last_name;
      $model->email = $user->email;
      $model->email_confirmation = $user->email;
      $model->job_title = $user->userProfile->job_title;
      $model->organisation_name = $user->organisation->org_name;
      $model->organisation_email = $user->organisation->org_email;
      $model->organisation_website = GenericUtility::prefixWithHttpIfNeeded($user->organisation->org_website);
      $model->organisation_logo = $user->organisation->org_logo;
      $model->cover_image = $user->organisation->cover_image;
      $model->organisation_phone = $user->organisation->org_phone;
      $model->office_location = $user->organisation->relatedOfficeLocation === null ? 0 : $user->organisation->relatedOfficeLocation->id;
      $model->office_type = $user->organisation->relatedOfficeType === null ? 0 : $user->organisation->relatedOfficeType->id;
      $model->organisation_types = $user->organisation->relatedOrganisationType === null ? 0 : $user->organisation->relatedOrganisationType->id;
      $model->description = $user->organisation->description;
      $model->gender = $user->userProfile->gender;
      $model->org_name_acronym = $user->organisation->org_name_acronym;
      $model->use_org_name_acronym = $user->organisation->use_org_name_acronym;
      $model->facebook_url = GenericUtility::prefixWithHttpIfNeeded($user->organisation->facebook_url);
      $model->twitter_url = GenericUtility::prefixWithHttpIfNeeded($user->organisation->twitter_url);
      $model->organisation_id = $user->organisation->id;
      $model->level_of_activities = $user->organisation->relatedLevelOfActivity === null ? 0 : $user->organisation->relatedLevelOfActivity->id;
    }
    return $model;
  }

  private function createDefaultUserProfile($user)
  {
    $obj = new UserProfile();
    $obj->user_id = $user->id;
    $obj->first_name = '';
    $obj->last_name = '';

    $obj->save();
  }

  private function createDefaultOrganisation($user)
  {
    $obj = new Organisation();
    $obj->cp_id = $user->id;
    $obj->org_name = '';
    $obj->org_email = '';
    $obj->org_website = '';

    $obj->save();
  }

  /**
   * Begin Registration Process - register user to the system
   * @param  [type] $input [description]
   * @return [type]        [description]
   */
  public function registerUser($input)
  {
    $response = new \stdClass();
    $response->ok = false;
    $response->messages = [];

    try {
      // create user, they can login already
      $user = $this->doCreateUser($input);

      $this->createDefaultUserProfile($user);

      if ($user->isOrganisation()) {
        $this->createDefaultOrganisation($user);
      }

      // create registration record and send verification email
      $registration = $this->createRegistration($user);
      $this->sendRegistration($user, $registration);

      $response->ok = true;

      return $response;

    } catch (\Exception $ex) {
      array_push($response->messages, $ex->getMessage());
      return $response;
    }
  }

  /**
   * Create Registration Entry/Record in DB
   * @param  [type] $user [description]
   * @return [type]       [description]
   */
  public function createRegistration($user)
  {
    $token = $this->createRegistrationToken($user);

    $registration = new Registration();
    $registration->user_id = $user->id;
    $registration->token = $token;
    $registration->save();

    return $registration;
  }

  private function createRegistrationToken($user)
  {
    // token
    $token = $user->username.':'.$user->email.':'.uniqid(mt_rand(), true);

    // hash the token
    $hashedToken = Hash::make($token);

    return $hashedToken;
  }

  /**
   * Send Registration Email
   * @param  [type] $user         [description]
   * @param  [type] $registration [description]
   * @return [type]               [description]
   */
  public function sendRegistration($user, $registration)
  {
    try {
      Mail::send('emails.register.verify', array('token' => base64_encode($registration->token), 'user' => $user), function ($message) use ($user) {
        $message->to($user->email)->subject(Messages::emailSubjectAccountActivation());
      });
    } catch (\Exception $ex) {
      Log::error($ex);
    }
  }

  /**
   * Resend Registration Email
   * @param  [type] $userId [description]
   * @return [type]         [description]
   */
  public function resendRegistration($userId)
  {
    try {
      $user = $this->_userRepository->getById($userId);
      $registration = Registration::where('user_id', '=', $user->id)->first();
      Mail::send('emails.register.verify', array('token' => base64_encode($registration->token), 'user' => $user), function ($message) use ($user) {
        $message->to($user->email)->subject(Messages::emailSubjectAccountActivation());
      });
    } catch (\Exception $ex) {
      Log::error($ex);
    }
  }

  /**
   * Resend Registration Email - alt
   * @param  [type] $username [description]
   * @param  [type] $email    [description]
   * @return [type]           [description]
   */
  public function resendRegistration2($username, $email)
  {
    try {
      $filter = array(
        array('column' => 'username', 'operator' => '=', 'value' => $username),
        array('column' => 'email', 'operator' => '=', 'value' => $email)
      );
      $user = $this->_userRepository->getSingle($filter);
      $this->resendRegistration($user->id);
    } catch (\Exception $ex) {
      Log::error($ex);
    }
  }

  /**
   * Verify Registration (user click "Verify Account" in the registration email)
   * @param  [type] $userId [description]
   * @param  [type] $token  [description]
   * @return [type]         [description]
   */
  public function verifyRegistration($userId, $token)
  {
    $response = new \stdClass();
    $response->ok = false;
    $response->user = null;

    $registration = Registration::where('user_id', '=', $userId)->where('token', '=', $token)->first();

    if ($registration !== null) {
      $user = $this->_userRepository->getById($userId);
      $user->verified = true;
      $user->save();

      $response->ok = true;
      $response->user = $user;

      return $response;
    } else {
      return $response;
    }
  }

  private function doCreateUser($input)
  {
    $model = new User();
    $model->id = 0;
    $model->username = $input['username'];
    $model->email = $input['email'];
    $model->password = Hash::make($input['password']);
    $model->profile_type = (int)$input['profile_type'];
    $model->save();

    return $model;
  }

  /**
   * Update Profile
   * @param  [type] $input [description]
   * @return [type]        [description]
   */
  public function updateProfile($input)
  {
    $id = (int)$input['id'];

    if ($id > 0) {
      $user = $this->_userRepository->getById($id);

      if ($user->isIndividual()) {
        $this->updateProfileIndividual($input, $user);
      } else if ($user->isOrganisation()) {
        $this->updateProfileOrganisation($input, $user);
      }
    }
  }

  private function updateProfileIndividual($input, $user)
  {
    // get related model which is userProfile but not from parent model, eloquent has not support update related model well.
    // bored about repository, I'm gonna access eloquent directly, please refactor later
    $profile = $this->_userProfileRepository->getById($user->UserProfile->id); //UserProfile::find($user->UserProfile->id);

    $user->email = $input['email'];

    $profile->first_name = $input['first_name'];
    $profile->last_name = $input['last_name'];
    $profile->job_title = $input['job_title'];
    $profile->employer_name = $input['employer_name'];

    if (array_key_exists('employer_organisation_types', $input)) {
      $employer_organisation_type_input = (int)$input['employer_organisation_types'];
      $employer_organisation_type = $this->_organisationTypeRepository->getById($employer_organisation_type_input);
      if ($employer_organisation_type !== null) {
        $profile->relatedEmployerOrganisationType()->associate($employer_organisation_type);
      }
    }

    $profile->thought = $input['thought'];

    $bases_input = (int)$input['bases'];
    $base = $this->_countryRepository->getById($bases_input);
    if ($base !== null) {
      $profile->relatedBase()->associate($base);
    }

    $nationalityInput = (int)$input['nationality'];
    $nationality = $this->_nationalityRepository->getById($nationalityInput);
    if ($nationality !== null) {
      $profile->relatedNationality()->associate($nationality);
    }

    $member_statuses_input0 = (int)$input['member_statuses'];
    $member_statuses = $this->_memberStatusRepository->getById($member_statuses_input0);
    if ($member_statuses !== null) {
      $profile->relatedMemberStatus()->associate($member_statuses);
    }

    $professional_statuses_input0 = (int)$input['professional_statuses'];
    $professional_statuses = $this->_professionalStatusRepository->getById($professional_statuses_input0);
    if ($professional_statuses !== null) {
      $profile->relatedProfessionalStatus()->associate($professional_statuses);
    }

    if (array_key_exists('level_of_responsibilities', $input)) {
      $levelofresponsibilities_input = (int)$input['level_of_responsibilities'];
      $levelofresponsibilities = $this->_levelOfResponsibilityRepository->getById($levelofresponsibilities_input);
      if ($levelofresponsibilities !== null) {
        $profile->relatedLevelOfResponsibility()->associate($levelofresponsibilities);
      }
    } else {

    }

    $profile->description = $input['description'];
    $profile->gender = $input['gender'];

    $profile->website_url = GenericUtility::prefixWithHttpIfNeeded($input['website_url']);
    $profile->facebook_url = GenericUtility::prefixWithHttpIfNeeded($input['facebook_url']);
    $profile->twitter_url = GenericUtility::prefixWithHttpIfNeeded($input['twitter_url']);

    $profile->save();

    $this->handleAvatar($input, $profile);

    $storedCoverImage = $this->handleCoverImage($input, $profile->user_id);
    if (strlen($storedCoverImage) > 0) {
      $profile->cover_image = $storedCoverImage;
      $profile->save();
    }

    /*************/
    /* Platforms */
    //$hasToHandlePlatform = false;
    $hasToHandlePlatform = true;
    if ($user->is_new) {
      $user->is_new = false;
      //$hasToHandlePlatform = true;
    }
    $user->save();

    if ($hasToHandlePlatform) {      
      $platformService = App::make('\Libraries\Admin\Services\PlatformService');
      // remove the users from old platform, then assign new platforms based on profile (based_in and level of responsibilities)
      // BEGIN remove from old platforms
      $removed = $platformService->unassignIndFromAllPlatforms($profile->id);
      // BEGIN assign
      $level = -1;
      if ($level = $profile->relatedLevelOfResponsibility !== null) {
        $level = $profile->relatedLevelOfResponsibility->id;
      }
      // if international selected, assign to international platform
      if ($level === Modules::level_international()) {
        $platformService->assignIndToPlatform($profile->id, Modules::platform_international());
        // mark as TOP
        $platformService->markIndAsTopAtPlatform($profile->id, Modules::platform_international());
      }
      // then assign to national platform
      $countryId = $profile->relatedBase->id;
      $platform = $platformService->getPlatformByCountryId($countryId);
      if ($platform !== null) {
        $platformService->assignIndToPlatform($profile->id, $platform->id);
        // mark as TOP
        $platformService->markIndAsTopAtPlatform($profile->id, $platform->id);
      }
    }
  }

  private function updateProfileOrganisation($input, $user)
  {
    // get related model which is userProfile but not from parent model, eloquent has not support update related model well.
    // bored about repository, I'm gonna access eloquent directly, please refactor later
    //$profile = UserProfile::find($user->UserProfile->id);
    $profile = $this->_userProfileRepository->getById($user->UserProfile->id);

    $user->email = $input['email'];

    $profile->first_name = $input['first_name'];
    $profile->last_name = $input['last_name'];
    $profile->job_title = $input['job_title'];
    $profile->save();

    $organisation = $this->_organisationRepository->getById($user->organisation->id); //Organisation::find($user->organisation->id);

    $organisation->org_name = $input['organisation_name'];
    $organisation->org_email = $input['organisation_email'];
    $organisation->org_website = GenericUtility::prefixWithHttpIfNeeded($input['organisation_website']);
    $organisation->org_phone = $input['organisation_phone'];

    $officelocation_input = (int)$input['office_location'];
    $officelocation = $this->_countryRepository->getById($officelocation_input);
    if ($officelocation !== null) {
      $organisation->relatedOfficeLocation()->associate($officelocation);
    }

    if (array_key_exists('office_type', $input)) {
      $officetype_input = (int)$input['office_type'];
      $officetype = $this->_officeTypeRepository->getById($officetype_input);
      if ($officetype !== null) {
        $organisation->relatedOfficeType()->associate($officetype);
      }
    }

    $organisationtypes_input = (int)$input['organisation_types'];
    $organisationtypes = $this->_organisationTypeRepository->getById($organisationtypes_input);
    if ($organisationtypes !== null) {
      $organisation->relatedOrganisationType()->associate($organisationtypes);
    }

    $levelofactivities_input = (int)$input['level_of_activities'];
    $levelofactivities = $this->_levelOfActivityRepository->getById($levelofactivities_input);
    if ($levelofactivities !== null) {
      $organisation->relatedLevelOfActivity()->associate($levelofactivities);
    }

    $organisation->description = $input['description'];

    $organisation->org_name_acronym = $input['org_name_acronym'];
    if (array_key_exists('use_org_name_acronym', $input)) {
      $organisation->use_org_name_acronym = $input['use_org_name_acronym'] === '1';
    } else {
      $organisation->use_org_name_acronym = false;
    }

    $organisation->facebook_url = GenericUtility::prefixWithHttpIfNeeded($input['facebook_url']);
    $organisation->twitter_url = GenericUtility::prefixWithHttpIfNeeded($input['twitter_url']);

    $organisation->save();

    $this->handleOrganisationLogo($input, $organisation);

    $storedCoverImage = $this->handleCoverImage($input, $organisation->cp_id);
    if (strlen($storedCoverImage) > 0) {
      $organisation->cover_image = $storedCoverImage;
      $organisation->save();
    }

    /*************/
    /* Platforms */
    //$hasToHandlePlatform = false;
    $hasToHandlePlatform = true;
    if ($user->is_new) {
      $user->is_new = false;
      //$hasToHandlePlatform = true;
    }
    $user->save();

    if ($hasToHandlePlatform) {
      $platformService = App::make('\Libraries\Admin\Services\PlatformService');
      // remove the users from old platform, then assign new platforms based on profile (office_location and level of activity)
      // BEGIN remove from all platforms 
      $platformService->unassignOrgFromAllPlatforms($organisation->id);
      // BEGIN assign
      $level = $organisation->relatedLevelOfActivity->id;
      // if international selected, assign to international platform
      if ($level === Modules::level_international()) {
        $platformService->assignOrgToPlatform($organisation->id, Modules::platform_international());
        // mark as TOP
        $platformService->markOrgAsTopAtPlatform($organisation->id, Modules::platform_international());
      }
      // then assign to national platform
      $countryId = $organisation->relatedOfficeLocation->id;
      $platform = $platformService->getPlatformByCountryId($countryId);
      if ($platform !== null) {
        $platformService->assignOrgToPlatform($organisation->id, $platform->id);
        // mark as TOP
        $platformService->markOrgAsTopAtPlatform($organisation->id, $platform->id);
      }
    }
  }

  private function handleCoverImage($input, $userId)
  {
    try {
      if (array_key_exists('cover_image', $input)) {
        $file = $input['cover_image'];
        if ($file !== null && isset($file) && !is_null($file) && ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) && $file->isValid()) {
          $content = file_get_contents($file->getRealPath());
          $stored = Directories::getMemberMediaDirectory($userId).rand().'_'.$file->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          return $stored;
        }
      }
      return '';
    } catch (\Exception $e) {
      return '';
    }
  }

  /**
   * [handleAvatar description]
   * @param  [type] $input   [description]
   * @param  [type] $profile [description]
   * @return [type]          [description]
   */
  private function handleAvatar($input, $profile)
  {
    try {
      if (array_key_exists('avatar', $input)) {
        $file = $input['avatar'];
        if ($file !== null && isset($file) && !is_null($file) && ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) && $file->isValid()) {
          $content = file_get_contents($file->getRealPath());
          $stored = Directories::getMemberMediaDirectory($profile->user_id).rand().'_'.$file->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          // save image path to db
          $profile->avatar = $stored;
          $profile->save();
          return true;
        }
      }
      return false;
    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * [handleOrganisationLogo description]
   * @param  [type] $input        [description]
   * @param  [type] $organisation [description]
   * @return [type]               [description]
   */
  private function handleOrganisationLogo($input, $organisation)
  {
    try {
      if (array_key_exists('organisation_logo', $input)) {
        $file = $input['organisation_logo'];
        if ($file !== null && isset($file) && !is_null($file) && ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) && $file->isValid()) {
          $content = file_get_contents($file->getRealPath());
          $stored = Directories::getMemberMediaDirectory($organisation->cp_id).rand().'_'.$file->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          // save image path to db
          $organisation->org_logo = $stored;
          $organisation->save();
          return true;
        }
      }
      return false;
    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * Check Username prior to login
   * @param  [type] $username [description]
   * @return [type]           [description]
   */
  public function checkUsernamePreLogin($username)
  {
    $response = new \stdClass();
    $response->somethingWrong = false;

    $filter = array(
      array('column' => 'username', 'operator' => '=', 'value' => $username)
    );
    $user = $this->_userRepository->getSingle($filter);

    if ($user !== null) {
      if ($user->verified) {
        // do nothing, let it pass
      } else {
        $response->somethingWrong = true;
        $response->verified = false;
        $response->user_id = $user->id;
      }
    }

    return $response;
  }

}
