<?php namespace Libraries\Admin\Services;

use Libraries\Models\TakeActions\TakeAction;
use Libraries\Repositories\TakeActions\ITakeActionRepository;
use Libraries\Repositories\TakeActions\ITakeActionTypeRepository;
use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Facades\Directories;
use Libraries\Support\Modules;
use Libraries\Facades\GenericUtility;

class TakeActionsService {

  private $_takeActionRepository;
  private $_takeActionTypeRepository;
  private $_organisationRepository;
  private $_filtersHandler;

  public function __construct(ITakeActionRepository $takeActionRepository,
                              ITakeActionTypeRepository $takeActionTypeRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_takeActionRepository = $takeActionRepository;
    $this->_takeActionTypeRepository = $takeActionTypeRepository;
    $this->_organisationRepository = $organisationRepository;
    $this->_filtersHandler = new FiltersHandler();
  }

  public function getPaginatedTakeActions($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'created_at', 'direction' => 'desc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array('typeOfTakeAction'); //array('relatedOrganisation', 'typeOfTakeAction');

    $results = $this->_takeActionRepository->getPaged($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  public function getTakeActionsCount(array $conditions)
  {
    $result = $this->_takeActionRepository->getCount($conditions);

    return $result;
  }

  public function getById($id) 
  {
    $model = $this->_takeActionRepository->getById($id);
    
    return $model;
  }

  public function insertTakeAction(array $input, $user)
  {
    $model = new TakeAction();
    $model->id = 0;
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->type_id = $input['type_id'];
    $model->url = GenericUtility::prefixWithHttpIfNeeded($input['url']);

    $model->published = true;
    $model->featured = true;

    // take actions is for organisation, so force to enter only the user profile and organisation
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    $model->save();

    $this->_filtersHandler->process($model, $input);

    $this->handleImage($input, $model);

    return $model;
  }

  public function updateTakeAction(array $input, $user)
  {
    $id = $input['id'];
    $model = $this->_takeActionRepository->getById($id);
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->type_id = $input['type_id'];
    $model->url = GenericUtility::prefixWithHttpIfNeeded($input['url']);

    $model->save();

    $this->_filtersHandler->process($model, $input);

    $this->handleImage($input, $model);

    return $model;
  }

  private function handleImage(array $input, $model)
  {
    try {
      if (array_key_exists('image', $input)) {
        $file = $input['image'];
        if ($file !== null && isset($file) && !is_null($file) && ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) && $file->isValid()) {
          $content = file_get_contents($file->getRealPath());
          $stored = Directories::getMemberModuleDirectory(Modules::TAKE_ACTIONS).rand().'_'.$file->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          // save image path to db
          $model->image = $stored;
          $model->save();
          return true;
        }
      }
      return false;
    } catch (\Exception $e) {
      return false;
    }
  }

  public function deleteTakeAction($id)
  {
    $model = $this->_takeActionRepository->deleteById($id);
  }

  public function publishTakeAction($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_takeActionRepository->getById($id);
      $model->published = true;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  public function unpublishTakeAction($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_takeActionRepository->getById($id);
      $model->published = false;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  public function getAllAuthors() 
  {
    return $this->_organisationRepository->getAuthoringOrganisations();
  }

  public function getTakeActionTypes()
  {
    return $this->_takeActionTypeRepository->getAll();
  }

  public function getTakeActionTypesAsList()
  {
    return $this->_takeActionTypeRepository->getAsList();
  }

  public function fromInputToModelForPreview(array $input, $user) 
  {
    $model = new TakeAction();
    $model->id = 0;
    $model->name = $input['name'];
    $model->title = $input['title'];
    $model->slug = $input['slug'];
    $model->content = $input['content'];
    $model->url = GenericUtility::prefixWithHttpIfNeeded($input['url']);
    $model->type_id = $input['type_id'];
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    try {
      $file = null;
      $path = '';

      if (array_key_exists('image', $input)) { 
        $file = $input['image'];

        if ($file !== null && isset($file) && !is_null($file) && !empty($file)) {
          $path = $file->getRealPath();
        } else {
          $file = $input['original_image']; 
          $path = $_SERVER['DOCUMENT_ROOT'].$file;
        }
      } else {
        $file = $input['original_image'];  
        $path = $_SERVER['DOCUMENT_ROOT'].$file;
      }

      if ($file !== null) {
        $content = file_get_contents($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($content);
        $model->image_base64 = $base64;
      }
    } catch (\Exception $e) {
    }

    return $model;
  }

}