<?php namespace Libraries\Admin\Services;

use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IUserProfileRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Facades\Directories;
use Libraries\Models\Organisation;

class OrganisationService {

  private $_userRepository;
  private $_userProfileRepository;
  private $_organisationRepository;

  public function __construct(IUserRepository $userRepository,
                              IUserProfileRepository $userProfileRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_userRepository = $userRepository;
    $this->_userProfileRepository = $userProfileRepository;
    $this->_organisationRepository = $organisationRepository;
  }

  public function getOrganisations($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'org_name', 'direction' => 'asc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array('relatedOrganisationType');

    $results = $this->_organisationRepository->getPaged($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  public function getOrganisationsCount(array $conditions)
  {
    $result = $this->_organisationRepository->getCount($conditions);

    return $result;
  }

  public function revokeMembership($orgId)
  {
    $ret = new \stdClass();
    $ret->revoked = false;
    $ret->original = null;
    $ret->org_id = $orgId;
    try {
      $organisation = $this->_organisationRepository->getById($orgId);
      if ($organisation !== null && isset($organisation) && !is_null($organisation)) {
        $organisation->is_active = false;
        $organisation->save();
        $ret->revoked = true;
        $ret->original = $organisation;
      }
      return $ret;
    } catch (\Exception $ex) {
      $ret->ex_message = $ex;
      return $ret;
    }
  }

  public function activateMembership($orgId)
  {
    $ret = new \stdClass();
    $ret->activated = false;
    $ret->original = null;
    $ret->org_id = $orgId;
    try {
      $organisation = $this->_organisationRepository->getById($orgId);
      if ($organisation !== null && isset($organisation) && !is_null($organisation)) {
        $organisation->is_active = true;
        $organisation->save();
        $ret->activated = true;
        $ret->original = $organisation;
      }
      return $ret;
    } catch (\Exception $ex) {
      $ret->ex_message = $ex;
      return $ret;
    }
  }

  public function getPlatformsAssignedToOrganisation($orgId)
  {
    $platforms = $this->_organisationRepository->getPlatformsAssignedToOrganisation($orgId);
    return $platforms;
  }

  // public function setTopAgency($orgId) 
  // {
  //   $ret = new \stdClass();
  //   $ret->ok = false;
  //   $ret->agency = null;
  //   $ret->org_id = $orgId;

  //   try {
  //     $organisation = $this->_organisationRepository->getById($orgId);
      
  //     if ($organisation !== null && isset($organisation) && !is_null($organisation)) {
  //       $organisation->top_agency = true;
  //       $organisation->save();
      
  //       $ret->ok = true;
  //       $ret->agency = $organisation;
  //     }

  //     return $ret;
  //   } catch (\Exception $ex) {
  //     $ret->ex_message = $ex;
  //     return $ret;
  //   }
  // }

  // public function unsetTopAgency($orgId) 
  // {
  //   $ret = new \stdClass();
  //   $ret->ok = false;
  //   $ret->agency = null;
  //   $ret->org_id = $orgId;

  //   try {
  //     $organisation = $this->_organisationRepository->getById($orgId);
      
  //     if ($organisation !== null && isset($organisation) && !is_null($organisation)) {
  //       $organisation->top_agency = false;
  //       $organisation->save();
      
  //       $ret->ok = true;
  //       $ret->agency = $organisation;
  //     }
      
  //     return $ret;
  //   } catch (\Exception $ex) {
  //     $ret->ex_message = $ex;
  //     return $ret;
  //   }
  // }

  private function getUser($userId)
  {
    $user = $this->_userRepository->getById($userId);
    if ($user !== null) {
      // should we check for verification? no for now
      // sanitize for display
      $user->sanitize();
      return $user;
    } else {
      return null;
    }
  }

  public function getOrganisation($organisationId)
  {
    $organisation = Organisation::find($organisationId);
    if ($organisation === null) {
      return null;
    }

    $userId = $organisation->cp_id;
    $user = $this->getUser($userId);
    if ($user === null) {
      return null;
    }

    if (!$user->isOrganisation()) {
      return null;
    }

    return $user;
  }

}