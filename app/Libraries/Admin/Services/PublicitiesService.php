<?php namespace Libraries\Admin\Services;

use Illuminate\Support\Facades\DB;
use Libraries\Models\Publicities\Publicity;
use Libraries\Repositories\Publicities\IPublicityRepository;
use Libraries\Repositories\Publicities\IPublicityTypeRepository;
use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Facades\Directories;
use Libraries\Support\Modules;
use Libraries\Support\Iso8601Interval;
use Libraries\Facades\GenericUtility;
use Carbon\Carbon;

class PublicitiesService {

  private $_publicityRepository;
  private $_publicityTypeRepository;
  private $_organisationRepository;

  public function __construct(IPublicityRepository $publicityRepository,
                              IPublicityTypeRepository $publicityTypeRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_publicityRepository = $publicityRepository;
    $this->_publicityTypeRepository = $publicityTypeRepository;
    $this->_organisationRepository = $organisationRepository;
  }

  public function getPaginatedPublicities($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'created_at', 'direction' => 'desc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array('typeOfPublicity');

    $results = $this->_publicityRepository->getPaged($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  public function getPublicitiesCount(array $conditions)
  {
    $result = $this->_publicityRepository->getCount($conditions);

    return $result;
  }

  public function getById($id) 
  {
    $model = $this->_publicityRepository->getById($id);
    
    return $model;
  }

  public function insertPublicity(array $input, $user)
  {
    $model = new Publicity();
    $model->id = 0;
    
    $model->type_id = intval($input['type_id']);
    $model->name = $input['name'];

    $model->a_title = $input['a_title'];
    $model->a_title_color = $input['a_title_color']; //[OBSOLETE]
    $model->a_text = $input['a_text'];
    $model->a_text_color = $input['a_text_color'];
    $model->a_button_text = $input['a_button_text'];
    $model->a_button_text_color = $input['a_button_text_color']; //[OBSOLETE]
    $model->a_position = $input['a_position'];
    $model->a_url = GenericUtility::prefixWithHttpIfNeeded($input['a_url']);
    $model->a_show_org_logo = (int)$input['a_show_org_logo'];

    $model->b_title = $input['b_title']; //[OBSOLETE]
    $model->b_url = GenericUtility::prefixWithHttpIfNeeded($input['b_url']);

    $model->c_title = $input['c_title']; //[OBSOLETE]
    $model->c_url = GenericUtility::prefixWithHttpIfNeeded($input['c_url']);

    $model->published = true;
    $model->featured = true;

    // for organisation, so force to enter only the user profile and organisation
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    // publishing times
    $model->published_start_date = $input['published_start_date'];
    $interval = new Iso8601Interval(0, intval($input['num_of_months']), 0, 0, 0, 0);
    $model->published_interval = $interval->toIso8601IntervalStr();
    // assume one month is 30 days
    $seconds = $interval->Months * 30 * 24 * 3600;
    $model->published_interval_in_seconds = $seconds;

    $model->save();

    $this->handlePublishingPlatforms($model, $input);

    $this->handleImage('a_image', $input, $model);
    $this->handleImage('b_image', $input, $model);
    $this->handleImage('c_image', $input, $model);

    return $model;
  }

  public function updatePublicity(array $input, $user)
  {
    $id = $input['id'];
    $model = $this->_publicityRepository->getById($id);

    $model->type_id = intval($input['type_id']);
    $model->name = $input['name'];

    if ($model->type_id === Modules::PUBLICITY_TYPE_BIG) {

      $model->a_title = $input['a_title'];
      $model->a_title_color = $input['a_title_color']; //[OBSOLETE]
      $model->a_text = $input['a_text'];
      $model->a_text_color = $input['a_text_color'];
      $model->a_button_text = $input['a_button_text']; 
      $model->a_button_text_color = $input['a_button_text_color']; //[OBSOLETE]
      $model->a_position = $input['a_position'];
      $model->a_url = GenericUtility::prefixWithHttpIfNeeded($input['a_url']);
      $model->a_show_org_logo = (int)$input['a_show_org_logo'];

    } else if ($model->type_id === Modules::PUBLICITY_TYPE_SMALL) {

      $model->b_title = $input['b_title']; //[OBSOLETE]
      $model->b_url = GenericUtility::prefixWithHttpIfNeeded($input['b_url']);

    } else if ($model->type_id === Modules::PUBLICITY_TYPE_SIDE) {

      $model->c_title = $input['c_title']; //[OBSOLETE]
      $model->c_url = GenericUtility::prefixWithHttpIfNeeded($input['c_url']);

    }
    
    // TODO: should we publish if the user edit?
    //$model->published = false;

    // publishing times
    $model->published_start_date = $input['published_start_date'];
    $interval = new Iso8601Interval(0, intval($input['num_of_months']), 0, 0, 0, 0);
    $model->published_interval = $interval->toIso8601IntervalStr();
    // assume one month is 30 days
    $seconds = $interval->Months * 30 * 24 * 3600;
    $model->published_interval_in_seconds = $seconds;

    $model->save();

    $this->handlePublishingPlatforms($model, $input);

    $this->handleImage('a_image', $input, $model);
    $this->handleImage('b_image', $input, $model);
    $this->handleImage('c_image', $input, $model);

    return $model;
  }

  private function handlePublishingPlatforms($model, $input)
  {
    // publishing platform
    if (!empty($input['selected_platforms'])) {
      // direct handling, I knot it's not best practice, but to save time :D
      // delete previous platforms
      DB::table('publicities_platforms')->where('publicity_id', $model->id)->delete();
      // iterate selected platforms, insert
      foreach ($input['selected_platforms'] as $key => $value) {
        DB::table('publicities_platforms')->insert([
          'publicity_id' => $model->id,
          'platform_id' => $value,
        ]);
      }
    }
  }

  private function handleImage($key, $input, $model)
  {
    try {
      if (array_key_exists($key, $input)) {
        $image = $input[$key];
        if ($image !== null && isset($image) && !is_null($image) && $image->isValid()) {
          $content = file_get_contents($image->getRealPath());
          $stored = Directories::getMemberModuleDirectory(Modules::PUBLICITY).rand().'_'.$image->getClientOriginalName();
          file_put_contents($_SERVER['DOCUMENT_ROOT'].$stored, $content);
          $model[$key] = $stored;
          $model->save();
          return true;
        }
      }
      return false;
    } catch (\Exception $e) {
      return false;
    }
  }

  public function deletePublicity($id)
  {
    $model = $this->_publicityRepository->deleteById($id);
  }

  public function publishPublicity($id, $user) 
  {
    if ($user->isAdmin()) {
      $model = $this->_publicityRepository->getById($id);
      $model->published = true;
      $model->timestamps = false;
      $model->published_at = Carbon::now()->format("Y-m-d H:i:s");
      $model->save();
      return true;
    }
    return false;
  }

  public function unpublishPublicity($id, $user) 
  {
    if ($user->isAdmin()) {
      $model = $this->_publicityRepository->getById($id);
      $model->published = false;
      $model->timestamps = false;
      //$model->published_at = null;
      $model->save();
      return true;
    }
    return false;
  }

  public function getAllAuthors() 
  {
    return $this->_organisationRepository->getAuthoringOrganisations();
  }

  public function getPublicityTypes()
  {
    return $this->_publicityTypeRepository->getAll();
  }

  public function getPublicityTypesAsList()
  {
    return $this->_publicityTypeRepository->getAsList();
  }

  public function fromInputToModelForPreview(array $input, $user) 
  {
    $model = new Publicity();
    $model->id = 0;

    $model->type_id = intval($input['type_id']);
    $model->name = $input['name'];

    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    if ($model->type_id === Modules::PUBLICITY_TYPE_BIG) {
      
      $model->a_title = $input['a_title'];
      $model->a_title_color = $input['a_title_color']; //[OBSOLETE]
      $model->a_text = $input['a_text'];
      $model->a_text_color = $input['a_text_color'];
      $model->a_button_text = $input['a_button_text'];
      $model->a_button_text_color = $input['a_button_text_color']; //[OBSOLETE]
      $model->a_position = $input['a_position'];
      $model->a_url = GenericUtility::prefixWithHttpIfNeeded($input['a_url']);
      $model->a_show_org_logo = (int)$input['a_show_org_logo'];
      $this->handleImageForPreview('a_image', $input, $model);
    
    } else if ($model->type_id === Modules::PUBLICITY_TYPE_SMALL) {
      
      $model->b_title = $input['b_title']; //[OBSOLETE]
      $model->b_url = GenericUtility::prefixWithHttpIfNeeded($input['b_url']);
      $this->handleImageForPreview('b_image', $input, $model);
    
    } else if ($model->type_id === Modules::PUBLICITY_TYPE_SIDE) {
      
      $model->c_title = $input['c_title']; //[OBSOLETE]
      $model->c_url = GenericUtility::prefixWithHttpIfNeeded($input['c_url']);
      $this->handleImageForPreview('c_image', $input, $model);
    
    }

    return $model;
  }

  private function handleImageForPreview($key, $input, $model)
  {
    try {
      $file = null;
      $path = '';

      if (array_key_exists($key, $input)) { 
        $file = $input[$key];

        if ($file !== null && isset($file) && !is_null($file) && !empty($file)) {
          $path = $file->getRealPath();
        } else {
          $file = $input[$key.'_original_image']; 
          $path = $_SERVER['DOCUMENT_ROOT'].$file;
        }
      } else {
        $file = $input[$key.'_original_image'];  
        $path = $_SERVER['DOCUMENT_ROOT'].$file;
      }

      if ($file !== null) {
        $content = file_get_contents($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($content);
        $model->image_base64 = $base64;
      }
    } catch (\Exception $e) {
      $model->image_base64 = $e->getMessage();
    }
  }

}