<?php namespace Libraries\Admin\Services;

use Libraries\Repositories\Filters\IRegionRepository;
use Libraries\Repositories\Filters\ICountryRepository;
use Libraries\Repositories\Filters\ICrisisRepository;
use Libraries\Repositories\Filters\IThemeRepository;
use Libraries\Repositories\Filters\ISectorRepository;
use Libraries\Repositories\Filters\IBeneficiaryRepository;
use Libraries\Repositories\Filters\IInterventionRepository;

class FilterService {

  private $_regionRepository;
  private $_countryRepository;
  private $_crisisRepository;
  private $_themeRepository;
  private $_sectorRepository;
  private $_beneficiaryRepository;
  private $_interventionRepository;

  public function __construct(IRegionRepository $regionRepository,
                              ICountryRepository $countryRepository,
                              ICrisisRepository $crisisRepository,
                              IThemeRepository $themeRepository,
                              ISectorRepository $sectorRepository,
                              IBeneficiaryRepository $beneficiaryRepository,
                              IInterventionRepository $interventionRepository)
  {
    $this->_regionRepository = $regionRepository;
    $this->_countryRepository = $countryRepository;
    $this->_crisisRepository = $crisisRepository;
    $this->_themeRepository = $themeRepository;
    $this->_sectorRepository = $sectorRepository;
    $this->_beneficiaryRepository = $beneficiaryRepository;
    $this->_interventionRepository = $interventionRepository;
  }

  /**
   * [getCountries description]
   * @return [type] [description]
   */
  public function getCountriesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_countryRepository->getAsList();

    return $results;
  }

  /**
   * [getCountries description]
   * @return [type] [description]
   */
  public function getCountries()
  {
    $results = $this->_countryRepository->getAll();

    return $results;
  }

  /**
   * [getRegions description]
   * @return [type] [description]
   */
  public function getRegionsAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_regionRepository->getAsList();

    return $results;
  }

  /**
   * [getRegions description]
   * @return [type] [description]
   */
  public function getRegions()
  {
    $results = $this->_regionRepository->getAll();

    return $results;
  }

  /**
   * [getSectors description]
   * @return [type] [description]
   */
  public function getSectorsAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_sectorRepository->getAsList();

    return $results;
  }

  /**
   * [getSectors description]
   * @return [type] [description]
   */
  public function getSectors()
  {
    $results = $this->_sectorRepository->getAllNotDeleted();

    return $results;
  }

  /**
   * [getCrisis description]
   * @return [type] [description]
   */
  public function getCrisisAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_crisisRepository->getAsList();

    return $results;
  }

  /**
   * [getCrisis description]
   * @return [type] [description]
   */
  public function getCrisis()
  {
    $results = $this->_crisisRepository->getAllNotDeleted();

    return $results;
  }

  /**
   * [getThemes description]
   * @return [type] [description]
   */
  public function getThemesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_themeRepository->getAsList();

    return $results;
  }

  /**
   * [getThemes description]
   * @return [type] [description]
   */
  public function getThemes()
  {
    $results = $this->_themeRepository->getAllNotDeleted();

    return $results;
  }

  /**
   * [getInterventionsAsList description]
   * @return [type] [description]
   */
  public function getInterventionsAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_interventionRepository->getAsList();

    return $results;
  }

  /**
   * [getInterventions description]
   * @return [type] [description]
   */
  public function getInterventions()
  {
    $results = $this->_interventionRepository->getAllNotDeleted();

    return $results;
  }

  /**
   * [getBeneficiaries description]
   * @return [type] [description]
   */
  public function getBeneficiariesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $relations = array();

    $results = $this->_beneficiaryRepository->getAsList();

    return $results;
  }

  /**
   * [getBeneficiaries description]
   * @return [type] [description]
   */
  public function getBeneficiaries()
  {
    $results = $this->_beneficiaryRepository->getAllNotDeleted();

    return $results;
  }

}