<?php namespace Libraries\Admin\Services;

class FiltersHandler {

  public function process($model, $input) 
  {
    $model->regions()->detach();
    if(array_key_exists('region', $input)) {
      $regions = $input['region'];
      if(!empty($regions)) {
        $model->regions()->sync(array($regions));
      }
    }

    $model->countries()->detach();
    if(array_key_exists('country', $input)) {
      $countries = $input['country'];
      if(!empty($countries)) {
        $model->countries()->sync(array($countries));
      }
    }

    $model->crisis()->detach();
    if(array_key_exists('crisis', $input)) {
      $crisis = $input['crisis'];
      if(!empty($crisis)) {
        $model->crisis()->sync(array($crisis));
      }
    }

    $model->sectors()->detach();
    if(array_key_exists('sector', $input)) {
      $sectors = $input['sector'];
      if(!empty($sectors)) {
        $model->sectors()->sync(array($sectors));
      }
    }

    $model->themes()->detach();
    if(array_key_exists('theme', $input)) {
      $themes = $input['theme'];
      if(!empty($themes)) {
        $model->themes()->sync(array($themes));
      }
    }

    $model->interventions()->detach();
    if(array_key_exists('intervention', $input)) {
      $interventions = $input['intervention'];
      if(!empty($interventions)) {
        $model->interventions()->sync(array($interventions));
      }
    }

    $model->beneficiaries()->detach();
    if(array_key_exists('beneficiary', $input)) {
      $beneficiaries = $input['beneficiary'];
      if(!empty($beneficiaries)) {
        $model->beneficiaries()->sync(array($beneficiaries));
      }
    }
  }

}