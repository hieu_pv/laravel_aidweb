<?php namespace Libraries\Admin\Services;

use Libraries\Models\Videos\Video;
use Libraries\Repositories\Videos\IVideoRepository;
use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Facades\Directories;
use Libraries\Facades\GenericUtility;

class VideosService {

  private $_videoRepository;
  private $_organisationRepository;
  private $_filtersHandler;

  public function __construct(IVideoRepository $videoRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_videoRepository = $videoRepository;
    $this->_organisationRepository = $organisationRepository;
    $this->_filtersHandler = new FiltersHandler();
  }

  public function getPaginatedVideos($pageNumber, $pageSize, array $sorts, array $conditions)
  {
    if ($sorts === null || is_null($sorts) || !isset($sorts)) {
      $sorts = array(
        array('column' => 'created_at', 'direction' => 'desc')
      );
    }

    if ($conditions === null || is_null($conditions) || !isset($conditions)) {
      $conditions = array(
        array('column' => 'id', 'operator' => '>', 'value' => 0)
      );
    }

    $relations = array();

    $results = $this->_videoRepository->getPaged($conditions, $sorts, $pageNumber, $pageSize, $relations);

    return $results;
  }

  public function getVideosCount(array $conditions)
  {
    $result = $this->_videoRepository->getCount($conditions);

    return $result;
  }

  public function getById($id) 
  {
    $model = $this->_videoRepository->getById($id);
    
    return $model;
  }

  public function insertVideo(array $input, $user)
  {
    $model = new Video();
    $model->id = 0;
    $model->title = $input['title'];
    $model->description = $input['description'];
    $model->slug = $input['slug'];

    // if (array_key_exists('author_obj', $input)) {
    //   $author_obj = json_decode($input['author_obj']);
    //   $authorNames = array_map(function ($item) {
    //     return $item->text;
    //   }, $author_obj);
    //   $model->author = implode('|', $authorNames);
    // }
    $model->author = $input['author'];

    $model->url = GenericUtility::prefixWithHttpIfNeeded($input['url']);
    $model->setVideoDetailsBasedOnUrl();

    $model->published = true;
    $model->featured = true;

    // videos is for organisation, so force to enter only the user profile and organisation
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    $model->save();

    $this->_filtersHandler->process($model, $input);

    return $model;
  }

  public function updateVideo(array $input, $user)
  {
    $id = $input['id'];
    $model = $this->_videoRepository->getById($id);

    $model->title = $input['title'];
    $model->description = $input['description'];
    $model->slug = $input['slug'];

    // if (array_key_exists('author_obj', $input)) {
    //   $author_obj = json_decode($input['author_obj']);
    //   $authorNames = array_map(function ($item) {
    //     return $item->text;
    //   }, $author_obj);
    //   $model->author = implode('|', $authorNames);
    // }
    $model->author = $input['author'];

    $model->url = GenericUtility::prefixWithHttpIfNeeded($input['url']);
    $model->setVideoDetailsBasedOnUrl();

    $model->save();

    $this->_filtersHandler->process($model, $input);

    return $model;
  }

  public function getAllAuthors() 
  {
    return $this->_organisationRepository->getAuthoringOrganisations();
  }

  public function deleteVideo($id)
  {
    $model = $this->_videoRepository->deleteById($id);
  }

  public function publishVideo($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_videoRepository->getById($id);
      $model->published = true;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  public function unpublishVideo($id, $user) 
  {
    // rule check?
    // - if user is admin
    if ($user->isAdmin()) {
      $model = $this->_videoRepository->getById($id);
      $model->published = false;
      $model->timestamps = false;
      $model->save();
      return true;
    }
    return false;
  }

  public function fromInputToModelForPreview(array $input, $user) 
  {
    $model = new Video();
    $model->id = $input['id'];
    $model->title = $input['title'];
    $model->description = $input['description'];
    $model->slug = $input['slug'];
    $model->author = $input['author'];
    $model->url = GenericUtility::prefixWithHttpIfNeeded($input['url']);
    $model->setVideoDetailsBasedOnUrl();
    // videos is for organisation, so force to enter only the user profile and organisation
    $model->user_id = $user->id;
    $model->user_profile_id = $user->userProfile->id;
    $model->organisation_id = $user->organisation->id;

    try {
      $file = $model->image;
      // $content = file_get_contents($file);      
      // $base64 = 'data:image/jpg;base64,' . base64_encode($content);
      // $model->image_base64 = $base64;
    } catch (\Exception $e) {
    }

    return $model;
  }

}