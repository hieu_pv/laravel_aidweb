<?php namespace Libraries\Admin\Services;

use Jenssegers\Date\Date;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Libraries\Facades\Messages;
use Libraries\Facades\SettingsProvider;
use Libraries\Facades\Modules;
use Libraries\Models\Filters\Country;
use Laracasts\Flash\Flash;

class PlatformService {

  /**
   * [getActivePlatforms description]
   * @return [type] [description]
   */
  public function getActivePlatforms()
  {
    return DB::table('platforms')->where('active', '1')->get();
  }

  public function getAllPlatforms()
  {
    return DB::table('platforms')->get();
  }

  public function markOrgAsTopAtPlatform($orgId, $platformId)
  {
    // get data first, see if the org already mark as top/not
    $d = DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_organisation())
                                            ->where('profile_id', $orgId)
                                            ->where('platform_id', $platformId)
                                            ->first();
    // if no data - admin has not handle the top status, then assign as top                                        
    if ($d === null) {
      $date = Date::now()->format('Y-m-d H:i');
      $id = DB::table('platforms_top_profiles')->insertGetId(array(
        'platform_id' => $platformId,
        'profile_id' => $orgId,
        'profile_type' => Modules::profile_type_organisation(),
        'created_at' => $date,
        'updated_at' => $date
      ));
      return $id;
    }

    return 0;
  }

  public function markIndAsTopAtPlatform($indId, $platformId)
  {
    // get data first, see if the ind already mark as top/not
    $d = DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_individual())
                                            ->where('profile_id', $indId)
                                            ->where('platform_id', $platformId)
                                            ->first();
    // if no data - admin has not handle the top status, then assign as top
    if ($d === null) {
      $date = Date::now()->format('Y-m-d H:i');
      $id = DB::table('platforms_top_profiles')->insertGetId(array(
        'platform_id' => $platformId,
        'profile_id' => $indId,
        'profile_type' => Modules::profile_type_individual(),
        'created_at' => $date,
        'updated_at' => $date
      ));
      return $id;
    }

    return 0;
  }

  /**
   * [getPlatformByCountryId description]
   * @param  [type] $countryId [description]
   * @return [type]            [description]
   */
  public function getPlatformByCountryId($countryId)
  {
    $d = DB::table('platforms')->where('active', '1')->where('related_country_id', $countryId)->first();

    if ($d === null) {
      $c = DB::table('countries')->where('id', $countryId)->first();
      if ($c !== null) {
        $this->create(['id' => $c->id, 'name' => $c->name]);
        $d = DB::table('platforms')->where('active', '1')->where('related_country_id', $countryId)->first();
        return $d;
      }
    } else {
    }

    return $d;
  }

  /**
   * [create description]
   */
  public function create($country)
  {
    try {
      $date = Date::now()->format('Y-m-d H:i');
      DB::table('platforms')->insertGetId([
        'name' => $country['name'],
        'display_text' => $country['name'],
        'related_country_id' => $country['id'],
        'created_at' => $date,
        'updated_at' => $date
      ]);

      return true;
    } catch (\Exception $ex) {
      return false;
    }
  }

  public function deactivatePlatform($id)
  {
    DB::table('platforms')->where('id', $id)->update(array('active' => 0));
  }

  public function activatePlatform($id)
  {
    DB::table('platforms')->where('id', $id)->update(array('active' => 1));
  }


  public function assignOrgToPlatform($orgId, $platformId)
  {
    $d = DB::table('platforms_profiles')->where('profile_type', Modules::profile_type_organisation())
                                        ->where('profile_id', $orgId)
                                        ->where('platform_id', $platformId)
                                        ->first();

    if ($d === null) {
      $date = Date::now()->format('Y-m-d H:i');
      $id = DB::table('platforms_profiles')->insertGetId(array(
        'platform_id' => $platformId,
        'profile_id' => $orgId,
        'profile_type' => Modules::profile_type_organisation(),
        'created_at' => $date,
        'updated_at' => $date
      ));
      return $id;
    }

    return 0;
  }

  public function assignIndToPlatform($indId, $platformId)
  {
    $d = DB::table('platforms_profiles')->where('profile_type', Modules::profile_type_individual())
                                        ->where('profile_id', $indId)
                                        ->where('platform_id', $platformId)
                                        ->first();

    if ($d === null) {
      $date = Date::now()->format('Y-m-d H:i');
      $id = DB::table('platforms_profiles')->insertGetId(array(
        'platform_id' => $platformId,
        'profile_id' => $indId,
        'profile_type' => Modules::profile_type_individual(),
        'created_at' => $date,
        'updated_at' => $date
      ));
      return $id;
    }

    return 0;
  }

  public function unassignIndFromAllPlatforms($indId)
  {
    try {
      DB::table('platforms_profiles')->where('profile_type', Modules::profile_type_individual())
                                     ->where('profile_id', $indId)
                                     ->delete();

      DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_individual())
                                         ->where('profile_id', $indId)
                                         ->delete();
      return true;
    } catch (\Exception $ex) {
      return false;
    }
  }

  public function unassignOrgFromAllPlatforms($orgId)
  {
    try {
      DB::table('platforms_profiles')->where('profile_type', Modules::profile_type_organisation())
                                     ->where('profile_id', $orgId)
                                     ->delete();

      DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_organisation())
                                         ->where('profile_id', $orgId)
                                         ->delete();

      return true;
    } catch (\Exception $ex) {
      return false;
    }
  }

}