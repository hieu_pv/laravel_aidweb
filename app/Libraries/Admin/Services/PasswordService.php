<?php namespace Libraries\Admin\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Libraries\Repositories\IUserRepository;
use Libraries\Repositories\IUserProfileRepository;
use Libraries\Repositories\IOrganisationRepository;
use Libraries\Repositories\Lookups\INationalityRepository;
use Libraries\Repositories\Lookups\IMemberStatusRepository;
use Libraries\Repositories\Lookups\IProfessionalStatusRepository;
use Libraries\Repositories\Lookups\IOfficeTypeRepository;
use Libraries\Repositories\Lookups\IOrganisationTypeRepository;
use Libraries\Repositories\Filters\ICountryRepository;
use Libraries\Models\User;
use Libraries\Models\UserProfile;
use Libraries\Models\PasswordReminder;
use Libraries\Models\Organisation;
use Libraries\Facades\Directories;
use Libraries\Facades\Messages;

class PasswordService {

  private $_userRepository;
  private $_userProfileRepository;
  private $_organisationRepository;

  public function __construct(IUserRepository $userRepository,
                              IUserProfileRepository $userProfileRepository,
                              IOrganisationRepository $organisationRepository)
  {
    $this->_userRepository = $userRepository;
    $this->_userProfileRepository = $userProfileRepository;
    $this->_organisationRepository = $organisationRepository;
  }

  /**
   * [changePassword description]
   * @param  [type] $userId [description]
   * @param  [type] $input  [description]
   * @return [type]         [description]
   */
  public function changePassword($userId, $input) 
  {
    $ret = new \stdClass();
    $ret->changed = false;
    $ret->messages = new \Illuminate\Support\MessageBag();

    try {
      if (!array_key_exists('old_password', $input)) {
        $ret->messages->add('old_password', Messages::validationMessageOldPasswordRequired(), $ret->messages);
        return $ret;
      }

      if (!array_key_exists('new_password', $input)) {
        $ret->messages->add('new_password', Messages::validationMessageNewPasswordRequired(), $ret->messages);
        return $ret;
      }

      $oldPassword = $input['old_password'];
      $newPassword = $input['new_password'];

      // get user
      $user = $this->_userRepository->getById($userId);
      if ($user !== null && isset($user) && !is_null($user)) { 
        // user found, check old password
        $credentials = [
          'username' => $user->username,
          'password' => $oldPassword
        ];

        $validPass = Auth::validate($credentials);
        
        if ($validPass) {
          $user->password = Hash::make($newPassword);
          $user->save();
        } else {
          $ret->messages->add('old_password', Messages::validationMessagePasswordNotValid(), $ret->messages);
          return $ret;
        }
      } else {
        $ret->messages->add('user', Messages::validationMessageUserNotFound(), $ret->messages);
        return $ret;
      }

      $ret->changed = true;

      return $ret;
    } catch (\Exception $ex) {
      $ret->messages->add('exception', $ex->getMessage());
      return $ret;
    }
  }

  /**
   * [resetPassword description]
   * @param  [type] $username [description]
   * @param  [type] $email    [description]
   * @return [type]           [description]
   */
  public function resetPassword($username, $email)
  {
    $ret = new \stdClass();
    $ret->reset = false;
    $ret->messages = [];

    try {
      $filter = array(
        array('column' => 'username', 'operator' => '=', 'value' => $username),
        array('column' => 'email', 'operator' => '=', 'value' => $email)
      );
      $user = $this->_userRepository->getSingle($filter);
      if ($user !== null && !is_null($user) && isset($user)) {
        // NOTE !!!
        // MANUALLY DO PASSWORD REMINDER, becuase this system allows DUPLICATE EMAIL, cannot use the Laravel password reminders framework
        $passwordReminder = $this->generatePasswordReminder($user);
        $ret->username = $passwordReminder->username;
        $ret->email = $passwordReminder->email;
        $ret->reset = true;
        return $ret;
      } else {
        array_push($ret->messages, Messages::validationMessageEmailUsernameNotRegistered());
        return $ret;
      }
    } catch (\Exception $ex) {
      array_push($ret->messages, $ex->getMessage()); //.':'.$ex->getTraceAsString());
      return $ret;
    }
  }

  private function generatePasswordReminder($user)
  {
    $this->deleteExistingPasswordReminder($user);

    $data = $this->createPasswordReminderToken($user);
    
    // store the username, password, and token to DB
    $passwordReminder = $this->createPasswordReminder($user, $data['pepper'], $data['token']);

    // send reset password email > do this async
    $this->sendReminder($user, $passwordReminder);

    return $passwordReminder;
  }

  private function createPasswordReminderToken($user)
  {
    // base prefix for the token
    $prefix = $user->username.':'.$user->email.':';
    // create pepper for the token
    $pepper = uniqid(mt_rand(), true);
    // token
    $token = $prefix.$pepper;
    // hash the token, token used as reset password token
    $hashedToken = Hash::make($token);

    return array('pepper' => $pepper, 'token' => $hashedToken);
  }

  private function checkPasswordReminderToken($username, $email, $pepper, $token)
  {
    // base prefix for the token
    $prefix = $username.':'.$email.':';

    // token
    $genToken = $prefix.$pepper;

    $match = Hash::check($genToken, $token);
    
    return $match;
  }

  private function deleteExistingPasswordReminder($user)
  {
    DB::delete('delete from password_reminders where email = ? and username = ?', array($user->email, $user->username));
  }

  private function createPasswordReminder($user, $pepper, $hashedToken)
  {
    // TODO: move to repository
    $passwordReminder = new PasswordReminder();
    $passwordReminder->username = $user->username;
    $passwordReminder->email = $user->email;
    $passwordReminder->pepper = $pepper;
    $passwordReminder->token = $hashedToken;
    $passwordReminder->save();
    return $passwordReminder;
  }

  /**
   * [resendReminder description]
   * @param  [type] $username [description]
   * @param  [type] $email    [description]
   * @return [type]           [description]
   */
  public function resendReminder($username, $email)
  {
    $response = new \stdClass();
    $response->ok = false;
    $response->messages = [];

    try {
      $filter = array(
        array('column' => 'username', 'operator' => '=', 'value' => $username),
        array('column' => 'email', 'operator' => '=', 'value' => $email)
      );
      $user = $this->_userRepository->getSingle($filter);

      if ($user !== null && !is_null($user) && isset($user)) {
        $passwordReminder = PasswordReminder::where('username', '=', $username)->where('email', '=', $email)->first();

        if ($passwordReminder === null) {
          array_push($response->messages, Messages::validationMessagePasswordResetTokenInvalid());
          return $response;
        } else {
          if ($passwordReminder->is_expired) {
            array_push($response->messages, Messages::validationMessagePasswordResetTokenExpired());
            return $response;
          }

          $this->sendReminder($user, $passwordReminder);
          $response->ok = true;
          return $response;
        }
      } else {
        array_push($ret->messages, Messages::validationMessageEmailUsernameNotRegistered());
        return $ret;
      }
    } catch (\Exception $ex) {
      array_push($response->messages, $ex->getMessage());
      return $response;
    }
  }

  /**
   * [sendReminder description]
   * @param  [type] $user             [description]
   * @param  [type] $passwordReminder [description]
   * @return [type]                   [description]
   */
  public function sendReminder($user, $passwordReminder)
  {
    // move to settings email sender
    Mail::send('emails.auth.reminder2', array('token' => base64_encode($passwordReminder->token), 'user' => $user), function ($message) use ($user) {
      $message->to($user->email, $user->userProfile->fullname)->subject('Password Reset');
    });
  }

  /**
   * [getPasswordReminderByToken description]
   * @param  [type] $token [description]
   * @return [type]        [description]
   */
  public function getPasswordReminderByToken($token)
  {
    $response = new \stdClass();
    $response->ok = false;
    $response->passwordReminder = new PasswordReminder();
    $response->messages = [];

    try {
      $passwordReminder = PasswordReminder::where('token', '=', $token)->first();

      if ($passwordReminder === null) {
        array_push($response->messages, Messages::validationMessagePasswordResetTokenInvalid());

        return $response;
      } else {
        $response->passwordReminder = $passwordReminder;

        if ($passwordReminder->is_expired) {
          array_push($response->messages, Messages::validationMessagePasswordResetTokenExpired());

          return $response;
        }

        $response->ok = true;
        
        return $response;
      }
    } catch (\Exception $ex) {
      array_push($response->messages, $ex->getMessage());
      return $response;
    }
  }

  /**
   * [doResetPassword description]
   * @param  [type] $credentials [description]
   * @return [type]              [description]
   */
  public function doResetPassword($credentials)
  {
    $response = new \stdClass();
    $response->ok = false;
    $response->messages = [];

    try {

      // check if new password confirmed
      if ($credentials->new_password !== $credentials->new_password_confirmation) {
        array_push($response->messages, Messages::validationMessagePasswordConfirmationDifferent());
        return $response;
      }

      // check if it's a valid reset for uname and email
      $passwordReminder = PasswordReminder::where('email', '=', $credentials->email)
                                          ->where('username', '=', $credentials->username)
                                          ->first();

      if ($passwordReminder !== null) {
        //check the token if it's valid token
        $valid = $this->checkPasswordReminderToken(
          $passwordReminder->username, 
          $passwordReminder->email, 
          $passwordReminder->pepper, 
          $credentials->token
        );

        // if valid token
        if ($valid) {
          $filter = array(
            array('column' => 'username', 'operator' => '=', 'value' => $passwordReminder->username),
            array('column' => 'email', 'operator' => '=', 'value' => $passwordReminder->email)
          );
          $user = $this->_userRepository->getSingle($filter);

          // check if user existed
          if ($user !== null && !is_null($user) && isset($user)) {
            $user->password = Hash::make($credentials->new_password);
            $user->save();
            $this->deleteExistingPasswordReminder($user);
            $response->ok = true;
            return $response;
          } else {
            array_push($response->messages, Messages::validationMessageUserNotFound());
            return $response;
          }

          return $response;
        } else { // not valid token
          array_push($response->messages, Messages::validationMessagePasswordResetTokenInvalid());
          return $response;
        }
      } else { // not valid uname and email
        array_push($response->messages, Messages::validationMessagePasswordResetTokenInvalid());
        return $response;
      }
    } catch (\Exception $ex) {
      array_push($response->messages, $ex->getMessage());
      return $response;
    }
  }
  
}
