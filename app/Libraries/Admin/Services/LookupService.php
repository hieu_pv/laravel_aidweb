<?php namespace Libraries\Admin\Services;

use Libraries\Repositories\Lookups\INationalityRepository;
use Libraries\Repositories\Lookups\IMemberStatusRepository;
use Libraries\Repositories\Lookups\IProfessionalStatusRepository;
use Libraries\Repositories\Lookups\IOfficeTypeRepository;
use Libraries\Repositories\Lookups\IOrganisationTypeRepository;
use Libraries\Repositories\Lookups\ILevelOfActivityRepository;
use Libraries\Repositories\Lookups\ILevelOfResponsibilityRepository;
use Libraries\Repositories\Filters\ICountryRepository;

class LookupService {
 
  private $_nationalityRepository;
  private $_memberStatusRepository;
  private $_professionalStatusRepository;
  private $_officeTypeRepository;
  private $_organisationTypeRepository;
  private $_countryRepository;
  private $_levelOfActivityRepository;
  private $_levelOfResponsibilityRepository;

  public function __construct(INationalityRepository $nationalityRepository,
                              IMemberStatusRepository $memberStatusRepository,
                              IProfessionalStatusRepository $professionalStatusRepository,
                              IOfficeTypeRepository $officeTypeRepository,
                              IOrganisationTypeRepository $organisationTypeRepository,
                              ICountryRepository $countryRepository,
                              ILevelOfActivityRepository $levelOfActivityRepository,
                              ILevelOfResponsibilityRepository $levelOfResponsibilityRepository)
  {
    $this->_nationalityRepository = $nationalityRepository;
    $this->_memberStatusRepository = $memberStatusRepository;
    $this->_professionalStatusRepository = $professionalStatusRepository;
    $this->_officeTypeRepository = $officeTypeRepository;
    $this->_organisationTypeRepository = $organisationTypeRepository;
    $this->_countryRepository = $countryRepository;
    $this->_levelOfActivityRepository = $levelOfActivityRepository;
    $this->_levelOfResponsibilityRepository = $levelOfResponsibilityRepository;
  }

  public function getBasesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $results = $this->_countryRepository->getAsList();

    return $results;
  }

  public function getBases()
  {
    $results = $this->_countryRepository->getAll();

    return $results;
  }

  public function getNationalitiesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $results = $this->_nationalityRepository->getAsList();

    return $results;
  }

  public function getNationalities()
  {
    $results = $this->_nationalityRepository->getAll();

    return $results;
  }

  public function getMemberStatusesAsList()
  {
    // $sorts = array(
    //   array('column' => 'name', 'direction' => 'asc')
    // );

    $results = $this->_memberStatusRepository->getAsList();

    return $results;
  }

  public function getMemberStatuses()
  {
    $results = $this->_memberStatusRepository->getAll();

    return $results;
  }

  public function getProfessionalStatusesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $results = $this->_professionalStatusRepository->getAsList();

    return $results;
  }

  public function getProfessionalStatuses()
  {
    $results = $this->_professionalStatusRepository->getAll();

    return $results;
  }

  public function getOfficeLocationsAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $results = $this->_countryRepository->getAsList();

    return $results;
  }

  public function getOfficeLocations()
  {
    $results = $this->_countryRepository->getAll();

    return $results;
  }

  public function getOfficeTypesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $results = $this->_officeTypeRepository->getAsList();

    return $results;
  }

  public function getOfficeTypes()
  {
    $results = $this->_officeTypeRepository->getAll();

    return $results;
  }

  public function getOrganisationTypesAsList()
  {
    $sorts = array(
      array('column' => 'name', 'direction' => 'asc')
    );

    $results = $this->_organisationTypeRepository->getAsList();

    return $results;
  }

  public function getOrganisationTypes()
  {
    $results = $this->_organisationTypeRepository->getAll();

    return $results;
  }

  public function getLevelOfActivitiesAsList()
  {
    $sorts = array(
      array('column' => 'id', 'direction' => 'asc')
    );

    $results = $this->_levelOfActivityRepository->getAsList();

    return $results;
  }

  public function getLevelOfActivities()
  {
    $results = $this->_levelOfActivityRepository->getAll();

    return $results;
  }

  public function getLevelOfResponsibilitiesAsList()
  {
    $sorts = array(
      array('column' => 'id', 'direction' => 'asc')
    );

    $results = $this->_levelOfResponsibilityRepository->getAsList();

    return $results;
  }

  public function getLevelOfResponsibilities()
  {
    $results = $this->_levelOfResponsibilityRepository->getAll();

    return $results;
  }

}