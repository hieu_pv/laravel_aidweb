<?php namespace Libraries\Admin\Controllers; 

use Jenssegers\Date\Date;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Libraries\Facades\Messages;
use Libraries\Facades\SettingsProvider;
use Libraries\Facades\Modules;
use Libraries\Models\Filters\Country;
use Laracasts\Flash\Flash;

class ManagePlatformsController extends AuthBaseController {

  private $_userService;
  private $_platformService;

  public function __construct()
  {
    parent::__construct();
    $this->_userService = App::make('\Libraries\Admin\Services\UserService');
    $this->_platformService = App::make('\Libraries\Admin\Services\PlatformService');
  }

  /**
   * [index description]
   * @return [type] [description]
   */
  public function index() 
  {
    View::share('activeLinkKey', 'manageplatforms.index');

    //$d = $this->_platformService->getActivePlatforms();
    $d = $this->_platformService->getAllPlatforms();

    return View::make('admin.platforms.index')->with('platforms', $d);
  }

  /**
   * [countries description]
   * @return [type] [description]
   */
  public function countries()
  {
    $countries = $this->_filterService->getCountries()->toArray();
    $usedCountries = DB::table('platforms')->where('active', '1')->lists('related_country_id');
    $rems = [];
    foreach ($countries as $key => $value) {
      if (!in_array($value['id'], $usedCountries)) {
        array_push($rems, $value);
      }
    }
    return Response::json(array('countries' => $rems));
  }

  /**
   * [create description]
   * @return [type] [description]
   */
  public function create()
  {
    $country = Input::get('selected_country');

    $ok = $this->_platformService->create($country);
    if ($ok) {
      Flash::success(Messages::successCreatePlatform($country['name']));
      //return Redirect::route('admin_manage_platforms');
      return Response::json(array('ok' => true));
    } else { 
      return Response::json(array('ok' => false));
    }
  }

  public function deactivate($id)
  {
    try {
      $this->_platformService->deactivatePlatform($id);
      return Response::json(array('ok' => true));
    } catch (\Exception $ex) {
      return Response::json(array('ok' => false));
    }
  }

  public function activate($id)
  {
    try {
      $this->_platformService->activatePlatform($id);
      return Response::json(array('ok' => true));
    } catch (\Exception $ex) {
      return Response::json(array('ok' => false));
    }
  }

  /**
   * [getAll description]
   * @return [type] [description]
   */
  public function getAll()
  {
    $d = $this->_platformService->getActivePlatforms();

    return Response::json(array('platforms' => $d));
  }


  //-------------------------------------------------------------------------------------------------------- 

  public function getOrgPlatformsHavingTopStatus($orgId)
  {
    $d = DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_organisation())
                                            ->where('profile_id', $orgId)->get();

    return Response::json(array('platforms_having_top_status' => $d));
  }

  public function markOrgAsTopAtPlatform()
  {
    $platformId = Input::get('platformId');
    $orgId = Input::get('orgId');

    $id = $this->_platformService->markOrgAsTopAtPlatform($orgId, $platformId);
    if ($id > 0) {
      return Response::json(array('ok' => true));
    } else {
      return Response::json(array('ok' => false));
    }
  }

  public function unmarkOrgFromTopAtPlatform()
  {
    $platformId = Input::get('platformId');
    $orgId = Input::get('orgId');

    DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_organisation())
                                       ->where('profile_id', $orgId)
                                       ->where('platform_id', $platformId)
                                       ->delete();

    return Response::json(array('ok' => true));
  }

  public function getIndPlatformsHavingTopStatus($indId)
  {
    $d = DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_individual())
                                            ->where('profile_id', $indId)->get();

    return Response::json(array('platforms_having_top_status' => $d));
  }

  public function markIndAsTopAtPlatform()
  {
    $platformId = Input::get('platformId');
    $indId = Input::get('indId');

    $id = $this->_platformService->markIndAsTopAtPlatform($indId, $platformId);
    if ($id > 0) {
      return Response::json(array('ok' => true));
    } else {
      return Response::json(array('ok' => false));
    }
  }

  public function unmarkIndFromTopAtPlatform()
  {
    $platformId = Input::get('platformId');
    $indId = Input::get('indId');

    DB::table('platforms_top_profiles')->where('profile_type', Modules::profile_type_individual())
                                       ->where('profile_id', $indId)
                                       ->where('platform_id', $platformId)
                                       ->delete();

    return Response::json(array('ok' => true));
  }

  //--------------------------------------------------------------------------------------------------------

  public function getOrgAssignedPlatforms($orgId)
  {
    $orgService = App::make('\Libraries\Admin\Services\OrganisationService');
    $platforms = $orgService->getPlatformsAssignedToOrganisation($orgId);
    return Response::json(array('platforms' => $platforms));
  }

  public function getIndAssignedPlatforms($indId)
  {
    $orgService = App::make('\Libraries\Admin\Services\UserService');
    $platforms = $orgService->getPlatformsAssignedToIndividual($indId);
    return Response::json(array('platforms' => $platforms));
  }

  //--------------------------------------------------------------------------------------------------------

}