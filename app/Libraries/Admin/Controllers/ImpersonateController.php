<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Libraries\Models\User;

class ImpersonateController extends BaseController {
  
  public function __construct()
  {
    
  }

  public function index()
  {
    return View::make('admin.impersonate.index');
  }

  public function doImpersonate()
  {
    $inputs = Input::all();
    if (array_key_exists('secret', $inputs)) {
      if ($inputs['secret'] === 'levinoadrielwinata') {
        $userId = $inputs['userId'];
        $user = User::find($userId);
        Auth::login($user);
        //return View::make('admin.dashboard.index');
        return Redirect::route('admin_dashboard2');
      }
    }

    return App::abort('404');
  }

} 