<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Libraries\Facades\SettingsProvider;

class OrganisationController extends AuthBaseController {
  
  private $_organisationService;

  public function __construct()
  {
    parent::__construct();
    $this->_organisationService = App::make('\Libraries\Admin\Services\OrganisationService');
  }

  public function index() 
  {
    View::share('activeLinkKey', 'organisations.allorganisations');

    return View::make('admin.organisations.index');
  }

  public function getData()
  {
    // NOTE:
    // all code below is optimized for dataTables. Changing the code, ensure the integrity with the dataTables in Client-side
    // 
    $input = Input::all();

    $pageSize = (int)$input['iDisplayLength'];
    $pageNumber = ((int)$input['iDisplayStart']) / $pageSize + 1;

    // SORTING
    $sorts = array(
      array('column' => 'org_name', 'direction' => 'asc')
    );
    if (Input::has('iSortCol_0')) {
      // assume iSortingCols = 1
      $sortingColIdx = (int)$input['iSortCol_0'];
      $sortingCol = $input['mDataProp_'.$sortingColIdx];
      $sortingDirection = $input['sSortDir_0'];
      $sorts = array(
        array('column' => $sortingCol, 'direction' => $sortingDirection)
      );
    }

    // FILTERS
    $conditions = array();
    // ------------------------------------------------------------------------------------
    // SPECIAL FILTER / SEARCH - handle OR
    // 
    // NOTE
    // Conditions algorithm: 
    // - get search string from dataTables
    // - check what columns are searchable
    // - construct and array of conditions as 'column', 'operator', 'value' in a sub_conditions, 
    //   because we need to support closure for OR based where
    $sub_conditions = array();
    $sSearch = $input['sSearch'];
    if (strlen(strval($sSearch)) > 0) {
      $iColumns = (int)$input['iColumns'];
      for ($i=0; $i < $iColumns; $i++) { 
        $mProp = $input['mDataProp_'.$i];
        $searchable = $input['bSearchable_'.$i] === 'true';
        if ($searchable) {
          $sub_conditions[] = array('column' => $mProp, 'operator' => 'LIKE', 'value' => '%'.$sSearch.'%');
        }
      }
    }
    // NOTE
    // use closure to wrap the where conditions (sub_conditions) because we need to support OR query, 
    // pass the closure to the query engine (repository)
    // PS: this thing might be similar to linq to sql
    if (count($sub_conditions) > 0) {
      $conditions[] = function ($query) use ($sub_conditions) {
        for ($i=0; $i < count($sub_conditions); $i++) { 
          $sub_condition = $sub_conditions[$i];
          $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
          // hack, special case for date column
          if ($sub_condition['column'] === 'created_at' || $sub_condition['column'] === 'updated_at') {
            $query->orwhere(function ($query2) use ($sub_condition) {
              $query2->whereRaw("date_format(".$sub_condition['column'].", '%d %M %Y %H:%i') ".$sub_condition['operator']." '".$sub_condition['value']."'");
            });
          }
        }
      };
    }
    // END SPECIAL FILTER / SEARCH
    // ------------------------------------------------------------------------------------

    // type filter
    if (Input::has('orgTypeFilter')) {
      $orgTypeFilter = (int)$input['orgTypeFilter'];
      if ($orgTypeFilter > 0) {
        $conditions[] = array('column' => 'org_type', 'operator' => '=', 'value' => $orgTypeFilter);
      }
    }

    // office location filter
    if (Input::has('officeLocationFilter')) {
      $officeLocationFilter = (int)$input['officeLocationFilter'];
      if ($officeLocationFilter > 0) {
        $conditions[] = array('column' => 'office_location', 'operator' => '=', 'value' => $officeLocationFilter);
      }
    }

    // membership status filter
    if (Input::has('membershipStatusFilter')) {
      $membershipStatusFilter = (int)$input['membershipStatusFilter'];
      if ($membershipStatusFilter > -1) {
        $conditions[] = array('column' => 'is_active', 'operator' => '=', 'value' => $membershipStatusFilter);
      }
    }

    // // top status filter
    // if (Input::has('topStatusFilter')) {
    //   $topStatusFilter = (int)$input['topStatusFilter'];
    //   if ($topStatusFilter > -1) {
    //     $conditions[] = array('column' => 'top_agency', 'operator' => '=', 'value' => $topStatusFilter);
    //   }
    // }

    $data = array();

    $iTotalRecords = 0;
    $iTotalDisplayRecords = 0;

    if (Auth::user()->isAdmin()) {
      $data = $this->_organisationService->getOrganisations($pageNumber, $pageSize, $sorts, $conditions);
    } else {
      
    }
    $iTotalRecords = $this->_organisationService->getOrganisationsCount($conditions);
    $iTotalDisplayRecords = $iTotalRecords; 

    return Response::json(array(
      'sEcho' => $input['sEcho'],
      'aaData' => $data,
      'iTotalRecords' => $iTotalRecords,
      'iTotalDisplayRecords' => $iTotalDisplayRecords
    ));
  }

  public function revokeMembership() 
  {
    $orgId = Input::get('org_id');

    $revoked = $this->_organisationService->revokeMembership($orgId);

    return Response::json($revoked);
  }

  public function activateMembership()
  {
    $orgId = Input::get('org_id');

    $activated = $this->_organisationService->activateMembership($orgId);

    return Response::json($activated);
  }

  // public function setAsTopAgency() 
  // {
  //   $orgId = Input::get('org_id');

  //   $top = $this->_organisationService->setTopAgency($orgId);

  //   return Response::json($top);
  // }

  // public function unsetFromTopAgency()
  // {
  //   $orgId = Input::get('org_id');

  //   $top = $this->_organisationService->unsetTopAgency($orgId);

  //   return Response::json($top);
  // }

  public function getOrganisationTypes()
  {
    $types = $this->_lookupService->getOrganisationTypes();

    return Response::json(array('orgTypes' => $types));
  }

}