<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Libraries\Models\User;

class UserController extends AuthBaseController {

  private $_registrationService;
  private $_passwordService;
  private $_userService;
  private $_organisationService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_registrationService = App::make('\Libraries\Admin\Services\RegistrationService');
    $this->_passwordService = App::make('\Libraries\Admin\Services\PasswordService');
    $this->_userService = App::make('\Libraries\Admin\Services\UserService');
    $this->_organisationService = App::make('\Libraries\Admin\Services\OrganisationService');
  }


  /**
   * [profile description]
   * @return [type] [description]
   */
  public function profile()
  {
    // require AUTH filter to pass
    
    $lookups = $this->getLookupsForProfile();
    $loggedInUser = Auth::user();
    // get from flash session if we have to stay in current page 1 time only (redirect when refresh)
    $stayInStartupForm = Session::has('stayInStartupForm') && Session::get('stayInStartupForm');

    if ($loggedInUser->isIndividual()) {

      $model = $this->_registrationService->createIndividualRegistrationModel($loggedInUser);

      $viewName = 'admin.accounts.profile_ind';
      if ($stayInStartupForm || $loggedInUser->is_new) {
        $viewName = 'admin.accounts.profile_ind_new';
      }
      return View::make($viewName)->with('lookups', $lookups)->with('model', $model);

    } else if ($loggedInUser->isOrganisation()) {

      $model = $this->_registrationService->createOrganisationRegistrationModel($loggedInUser);

      $viewName = 'admin.accounts.profile_org';
      if ($stayInStartupForm || $loggedInUser->is_new) {
        $viewName = 'admin.accounts.profile_org_new';
      }
      return View::make($viewName)->with('lookups', $lookups)->with('model', $model);

    }
  }

  public function readonlyIndProfile($profileId)
  {
    // require AUTH filter to pass
    $lookups = $this->getLookupsForProfile();

    $user = $this->_userService->getIndividual($profileId);

    $model = $this->_registrationService->createIndividualRegistrationModel($user);

    $viewName = 'admin.accounts.readonly_profile_ind';

    return View::make($viewName)->with('lookups', $lookups)->with('model', $model);
  }

  public function readonlyOrgProfile($organisationId)
  {
    // require AUTH filter to pass
    $lookups = $this->getLookupsForProfile();

    $user = $this->_organisationService->getOrganisation($organisationId);

    $model = $this->_registrationService->createOrganisationRegistrationModel($user);

    $viewName = 'admin.accounts.readonly_profile_org';

    return View::make($viewName)->with('lookups', $lookups)->with('model', $model);
  }

  /**
   * [postProfile description]
   * @return [type] [description]
   */
  public function postProfile()
  {
    // require AUTH filter to pass 
    $inputs = Input::all();
    $id = (int)Input::get('id');
    
    if ($id > 0) {
      $user = new User();
      $user->profile_type = (int)Input::get('profile_type');
      $validator = Validator::make($inputs, array());
      $additionalRules = array();

      if ($user->isIndividual()) { 

        $validator = new \Libraries\Validators\UpdateIndividualValidator();
        // TODO, email is not unique, further discussion is needed
        // |confirmed|unique:users,email,'.$id
        $additionalRules = array('email' => 'required|email');

      } else if ($user->isOrganisation()) {

        $validator = new \Libraries\Validators\UpdateOrganisationValidator();
        // TODO, email is not unique, further discussion is needed
        // |unique:users,email,'.$id
        $additionalRules = array('email' => 'required|email');

      } else {

        // not individual or organisation? why? just throw validator error
        //$validator->getMessageBag()->add('Validator', 'Validator is invalid');

        //return Redirect::route('admin_profile')->withInput(); //->withErrors($validator->errors());
        return Redirect::back()->withInput();

      }

      // validator determined, proceed to validation and process input
      if ($validator->validate($inputs, $additionalRules)) {

        // recheck for org logo or user avatar
        if ($user->isIndividual()) { 

          $avatar = $inputs['avatar'];
          if (Auth::user()->is_new) {
            if ($avatar === null || !isset($avatar) || is_null($avatar)) {
              $errors = new MessageBag();
              $errors->add('', 'Photo is required');
              // redirect immediately
              return Redirect::back()->withInput()->withErrors($errors);
            }
          }

        } else if ($user->isOrganisation()) {

          $orgLogo = $inputs['organisation_logo'];
          if (Auth::user()->is_new) {
            if ($orgLogo === null || !isset($orgLogo) || is_null($orgLogo)) {
              $errors = new MessageBag();
              $errors->add('', 'Organisation logo is required');
              // redirect immediately
              return Redirect::back()->withInput()->withErrors($errors);
            }
          }

        }

        $this->_registrationService->updateProfile($inputs);
        Session::flash('ok_updateProfile', true);
        Session::flash('hideUpdateProfileForm', true);

        // add a flag so that it will stay in the page 1 time
        $stayInStartupForm = Auth::user()->is_new;
        Session::flash('stayInStartupForm', $stayInStartupForm);
        
        return Redirect::route('admin_profile');

      } else {

        return Redirect::back()->withInput()->withErrors($validator->errors());

      }
    }
  }

  private function getLookupsForProfile()
  {
    $lookups = new \stdClass();

    $lookups->bases = $this->_lookupService->getBasesAsList();
    unset($lookups->bases[0]);

    $lookups->nationalities = $this->_lookupService->getNationalitiesAsList();
    unset($lookups->nationalities[0]);

    $lookups->member_statuses = $this->_lookupService->getMemberStatusesAsList();
    //dd($lookups);
    unset($lookups->member_statuses[0]);

    $lookups->professional_statuses = $this->_lookupService->getProfessionalStatusesAsList();
    unset($lookups->professional_statuses[0]);

    $lookups->office_locations = $this->_lookupService->getOfficeLocationsAsList();
    unset($lookups->office_locations[0]);

    $lookups->office_types = $this->_lookupService->getOfficeTypesAsList();
    unset($lookups->office_types[0]);

    $lookups->organisation_types = $this->_lookupService->getOrganisationTypesAsList();
    unset($lookups->organisation_types[0]);

    $lookups->level_of_activities = $this->_lookupService->getLevelOfActivitiesAsList();
    unset($lookups->level_of_activities[0]);

    $lookups->level_of_responsibilities = $this->_lookupService->getLevelOfResponsibilitiesAsList();
    unset($lookups->level_of_responsibilities[0]);

    return $lookups;
  }

}