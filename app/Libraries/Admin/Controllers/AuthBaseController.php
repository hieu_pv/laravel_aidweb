<?php namespace Libraries\Admin\Controllers;

use Illuminate\Support\Facades\App;

class AuthBaseController extends BaseController {

  protected $_filterService;
  protected $_lookupService;

  public function __construct()
  {
    $this->beforeFilter('auth');

    $this->beforeFilter('new_member');

    $this->_filterService = App::make('\Libraries\Admin\Services\FilterService');
    $this->_lookupService = App::make('\Libraries\Admin\Services\LookupService');
  }

  protected function getSelectedFilters($model)
  {
    $filters = new \stdClass();
    if (!is_null($model) && isset($model)) {
      $filters->originalRegion = $model->present()->original->region();
      $filters->originalCountry = $model->present()->original->country();
      $filters->originalCrisis = $model->present()->original->crisis();
      $filters->originalSector = $model->present()->original->sector();
      $filters->originalTheme = $model->present()->original->theme();
      $filters->originalIntervention = $model->present()->original->intervention();
      $filters->originalBeneficiary = $model->present()->original->beneficiary();
    }
    return $filters;
  }

  protected function getFilters()
  {
    $data = new \stdClass();
    $data->countries = $this->_filterService->getCountriesAsList();
    $data->regions = $this->_filterService->getRegionsAsList();
    $data->crisis = $this->_filterService->getCrisisAsList();
    $data->sectors = $this->_filterService->getSectorsAsList();
    $data->themes = $this->_filterService->getThemesAsList();
    $data->interventions = $this->_filterService->getInterventionsAsList();
    $data->beneficiaries = $this->_filterService->getBeneficiariesAsList();

    return $data;
  }

}
