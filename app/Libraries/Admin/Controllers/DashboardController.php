<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class DashboardController extends AuthBaseController {
  
  private $_blogService;
  private $_newsService;
  private $_takeActionsService;
  private $_videosService;
  private $_publicitiesService;
  private $_organisationService;
  private $_userService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_blogService = App::make('\Libraries\Admin\Services\BlogService');
    $this->_newsService = App::make('\Libraries\Admin\Services\NewsService');
    $this->_takeActionsService = App::make('\Libraries\Admin\Services\TakeActionsService');
    $this->_videosService = App::make('\Libraries\Admin\Services\VideosService');
    $this->_publicitiesService = App::make('\Libraries\Admin\Services\PublicitiesService');
    $this->_organisationService = App::make('\Libraries\Admin\Services\OrganisationService');
    $this->_userService = App::make('\Libraries\Admin\Services\UserService');
  }

  /**
   * [showIndex description]
   * @return [type]
   */
  public function index()
  {
    View::share('activeLinkKey', 'dashboard');

    // count records
    $model = new \stdClass();

    $conditions = array();
    $platforms = [];
    $model->platformText = '';

    if (Auth::user()->isIndividual()) {
      $conditions[] = array('column' => 'user_id', 'operator' => '=', 'value' => Auth::user()->id);
      $conditions[] = array('column' => 'user_profile_id', 'operator' => '=', 'value' => Auth::user()->userProfile->id);

      $platforms = $this->_userService->getPlatformsAssignedToIndividual(Auth::user()->userProfile->id);
    } else if (Auth::user()->isOrganisation()) {
      $conditions[] = array('column' => 'user_id', 'operator' => '=', 'value' => Auth::user()->id);
      $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => Auth::user()->organisation->id);

      $platforms = $this->_organisationService->getPlatformsAssignedToOrganisation(Auth::user()->organisation->id);
    }

    foreach ($platforms as $key => $value) {
      $model->platformText = $model->platformText.', '.strtoupper($value->display_text);
    }

    if (strlen($model->platformText) > 0) {
      $model->platformText = substr($model->platformText, 1);
    }

    $model->postsTotalRecords = $this->_blogService->getPostsCount($conditions);
    $model->newsTotalRecords = $this->_newsService->getNewsItemsCount($conditions);
    $model->takeActionsTotalRecords = $this->_takeActionsService->getTakeActionsCount($conditions);
    $model->videosTotalRecords = $this->_videosService->getVideosCount($conditions);
    $model->publicitiesTotalRecords = $this->_publicitiesService->getPublicitiesCount($conditions);

    return View::make('admin.dashboard.index')->with('model', $model);
  }

} 