<?php namespace Libraries\Admin\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;
use Libraries\Facades\GenericUtility;
use Libraries\Facades\Messages;
use Libraries\Models\News\NewsItem;

class NewsItemController extends AuthBaseController
{

    private $_newsService;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->_newsService = App::make('\Libraries\Admin\Services\NewsService');

        $this->beforeFilter('csrf', array('only' => array('insert')));

        $this->beforeFilter('posting_limitation', array('only' => array('create')));
        
        $this->beforeFilter('disable_new_post', array('only' => array('insert')));

    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        View::share('activeLinkKey', 'news.allnewsitems');

        return View::make('admin.news.index');
    }

    public function getData()
    {
        // NOTE:
        // all code below is optimized for dataTables. Changing the code, ensure the integrity with the dataTables in Client-side
        //
        $input = Input::all();

        $pageSize = (int) $input['iDisplayLength'];
        $pageNumber = ((int) $input['iDisplayStart']) / $pageSize + 1;

        // SORTING
        $sorts = array(
            array('column' => 'created_at', 'direction' => 'desc'),
        );
        if (Input::has('iSortCol_0')) {
            // assume iSortingCols = 1
            $sortingColIdx = (int) $input['iSortCol_0'];
            $sortingCol = $input['mDataProp_' . $sortingColIdx];
            $sortingDirection = $input['sSortDir_0'];
            $sorts = array(
                array('column' => $sortingCol, 'direction' => $sortingDirection),
            );
        }

        // FILTERS
        $conditions = array();
        // ------------------------------------------------------------------------------------
        // SPECIAL FILTER / SEARCH - handle OR
        //
        // NOTE
        // Conditions algorithm:
        // - get search string from dataTables
        // - check what columns are searchable
        // - construct and array of conditions as 'column', 'operator', 'value' in a sub_conditions,
        //   because we need to support closure for OR based where
        $sub_conditions = array();
        $sSearch = $input['sSearch'];
        if (strlen(strval($sSearch)) > 0) {
            $iColumns = (int) $input['iColumns'];
            for ($i = 0; $i < $iColumns; $i++) {
                $mProp = $input['mDataProp_' . $i];
                $searchable = $input['bSearchable_' . $i] === 'true';
                if ($searchable) {
                    $sub_conditions[] = array('column' => $mProp, 'operator' => 'LIKE', 'value' => '%' . $sSearch . '%');
                }
            }
        }
        // NOTE
        // use closure to wrap the where conditions (sub_conditions) because we need to support OR query,
        // pass the closure to the query engine (repository)
        // PS: this thing might be similar to linq to sql
        if (count($sub_conditions) > 0) {
            $conditions[] = function ($query) use ($sub_conditions) {
                for ($i = 0; $i < count($sub_conditions); $i++) {
                    $sub_condition = $sub_conditions[$i];
                    $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
                    // hack, special case for date column
                    if ($sub_condition['column'] === 'created_at' || $sub_condition['column'] === 'updated_at') {
                        $query->orwhere(function ($query2) use ($sub_condition) {
                            $query2->whereRaw("date_format(" . $sub_condition['column'] . ", '%d %M %Y %H:%i') " . $sub_condition['operator'] . " '" . $sub_condition['value'] . "'");
                        });
                    }
                }
            };
        }
        // END SPECIAL FILTER / SEARCH
        // ------------------------------------------------------------------------------------

        // author filter
        if (Input::has('authorFilter')) {
            $f_author = (int) $input['authorFilter'];
            if ($f_author > 0) {
                $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => $f_author);
            }
        }

        // published filter
        if (Input::has('publishedFilter')) {
            $f_published = (int) $input['publishedFilter'];
            if ($f_published > -1) {
                // 0 = false
                // 1 = true
                // -1 not chosen
                $conditions[] = array('column' => 'published', 'operator' => '=', 'value' => $f_published);
            }
        }

        $newsitems = array();

        $iTotalRecords = 0;
        $iTotalDisplayRecords = 0;

        if (Auth::user()->isAdmin()) {
            $newsitems = $this->_newsService->getPaginatedNewsItems($pageNumber, $pageSize, $sorts, $conditions);
        } else {
            $conditions[] = array('column' => 'user_id', 'operator' => '=', 'value' => Auth::user()->id);
            $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => Auth::user()->organisation->id);
            $newsitems = $this->_newsService->getPaginatedNewsItems($pageNumber, $pageSize, $sorts, $conditions);
        }
        $iTotalRecords = $this->_newsService->getNewsItemsCount($conditions);
        $iTotalDisplayRecords = $iTotalRecords;

        return Response::json(array(
            'sEcho' => $input['sEcho'],
            'aaData' => $newsitems,
            'iTotalRecords' => $iTotalRecords,
            'iTotalDisplayRecords' => $iTotalDisplayRecords,
        ));
    }

    public function getAuthors()
    {
        return Response::json(array('authors' => $this->_newsService->getAllAuthors()));
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        $newsitem = new NewsItem();
        $newsitem->id = 0;
        $newsitem->image = '/images/noimage.png';
        $newsitem->name = GenericUtility::GUID();

        $lookups = $this->getFilters();
        $filters = $this->getSelectedFilters($newsitem);

        View::share('activeLinkKey', 'news.addnewsitem');

        $entityName = Auth::user()->entityName;

        return View::make('admin.news.create')->with('model', $newsitem)
            ->with('lookups', $lookups)
            ->with('filters', $filters)
            ->with('disclaimerEntityName', $entityName)
            ->with('disclaimerManageLink', 'News Manager -> All News');
    }

    /**
     * [insert description]
     * @return [type] [description]
     */
    public function insert()
    {
        // validations
        $inputs = Input::all();
        $validator = new \Libraries\Validators\NewsItemValidator();

        if ($validator->validate($inputs)) {
            // save to storage
            $newsitem = $this->_newsService->insertNewsItem($inputs, Auth::user());

            // redirect
            Flash::success(Messages::successPosting($newsitem->title));
            return Redirect::route('admin_all_newsitems');
        } else {
            return Redirect::route('admin_create_newsitem')->withErrors($validator->errors())
                ->withInput();
        }
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        $newsitem = $this->_newsService->getById($id);
        $lookups = $this->getFilters();
        $filters = $this->getSelectedFilters($newsitem);

        View::share('activeLinkKey', 'news');

        $entityName = Auth::user()->entityName;

        return View::make('admin.news.edit')->with('model', $newsitem)
            ->with('lookups', $lookups)
            ->with('filters', $filters)
            ->with('disclaimerEntityName', $entityName)
            ->with('disclaimerManageLink', 'News Manager -> All News');
    }

    /**
     * [update description]
     * @return [type] [description]
     */
    public function update()
    {
        // validations
        $inputs = Input::all();
        $validator = new \Libraries\Validators\NewsItemValidator();

        if ($validator->validate($inputs)) {
            // update newsitem
            $newsitem = $this->_newsService->updateNewsItem($inputs, Auth::user());

            // redirect
            Flash::success(Messages::successEditPosting($newsitem->title));
            return Redirect::route('admin_all_newsitems');
        } else {
            $id = Input::get('id');
            return Redirect::route('admin_edit_newsitem', array($id))->withErrors($validator->errors())
                ->withInput();
        }
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $this->_newsService->deleteNewsItem($id);

        return Response::json(array('id' => $id, 'deleted' => true));
    }

    public function publish($id)
    {
        $published = $this->_newsService->publishNewsItem($id, Auth::user());
        return Response::json(array('id' => $id, 'published' => $published));
    }

    public function unpublish($id)
    {
        $unpublished = $this->_newsService->unpublishNewsItem($id, Auth::user());
        return Response::json(array('id' => $id, 'unpublished' => $unpublished));
    }

    public function fpPublish($id)
    {
        $published = $this->_newsService->publishNewsItem($id, Auth::user());
        return Redirect::back();
    }

    public function fpUnpublish($id)
    {
        $unpublished = $this->_newsService->unpublishNewsItem($id, Auth::user());
        return Redirect::back();
    }

    public function preview($id)
    {
        $model = $this->_newsService->getById($id);

        if ($model === null) {
            App::abort(404);
        }

        return View::make('admin.news.preview')->with('model', $model);
    }

    public function getPreviewUrl()
    {
        $formdata = Input::all(); //get('formdata');
        $d = GenericUtility::GUID();
        $m = $this->_newsService->fromInputToModelForPreview($formdata, Auth::user());
        Session::flash('preview_news_' . $d, $m);
        $u = route('cms_news_preview', array('skey' => $d));
        return Response::json(array('ok' => true, 'url' => $u));
    }

    public function readonly($id)
    {
        $newsitem = $this->_newsService->getById($id);
        $lookups = $this->getFilters();
        $filters = $this->getSelectedFilters($newsitem);

        View::share('activeLinkKey', 'news');

        $entityName = Auth::user()->entityName;

        return View::make('admin.news.readonly')->with('model', $newsitem)
            ->with('lookups', $lookups)
            ->with('filters', $filters)
            ->with('disclaimerEntityName', $entityName)
            ->with('disclaimerManageLink', 'News Manager -> All News');
    }

}
