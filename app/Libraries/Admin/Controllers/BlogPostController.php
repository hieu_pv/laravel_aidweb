<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Libraries\Models\Blog\Post;
use Libraries\Facades\Directories;
use Libraries\Facades\Messages;
use Libraries\Facades\GenericUtility;
use Laracasts\Flash\Flash;

class BlogPostController extends AuthBaseController {

  private $_blogService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_blogService = App::make('\Libraries\Admin\Services\BlogService');

    $this->beforeFilter('posting_limitation_in_pilot_phase', array('only' => array('create')));
    $this->beforeFilter('posting_limitation', array('only' => array('create')));
    $this->beforeFilter('disable_new_post', array('only' => array('insert')));
  }

  /**
   * [showIndex description]
   * @return [type]
   */
  public function index()
  {
    View::share('activeLinkKey', 'blog.allposts');

    return View::make('admin.blog.index');
  }

  /**
   * [getData description]
   * @return [type] [description]
   */
  public function getData()
  {
    // NOTE:
    // all code below is optimized for dataTables. Changing the code, ensure the integrity with the dataTables in Client-side
    // 
    $input = Input::all();

    $pageSize = (int)$input['iDisplayLength'];
    $pageNumber = ((int)$input['iDisplayStart']) / $pageSize + 1;

    // SORTING
    $sorts = array(
      array('column' => 'created_at', 'direction' => 'desc')
    );
    if (Input::has('iSortCol_0')) {
      // assume iSortingCols = 1
      $sortingColIdx = (int)$input['iSortCol_0'];
      $sortingCol = $input['mDataProp_'.$sortingColIdx];
      $sortingDirection = $input['sSortDir_0'];
      $sorts = array(
        array('column' => $sortingCol, 'direction' => $sortingDirection)
      );
    }

    // FILTERS
    $conditions = array();
    // ------------------------------------------------------------------------------------
    // SPECIAL FILTER / SEARCH - handle OR
    // 
    // NOTE
    // Conditions algorithm: 
    // - get search string from dataTables
    // - check what columns are searchable
    // - construct and array of conditions as 'column', 'operator', 'value' in a sub_conditions, 
    //   because we need to support closure for OR based where
    $sub_conditions = array();
    $sSearch = $input['sSearch'];
    if (strlen(strval($sSearch)) > 0) {
      $iColumns = (int)$input['iColumns'];
      for ($i=0; $i < $iColumns; $i++) { 
        $mProp = $input['mDataProp_'.$i];
        $searchable = $input['bSearchable_'.$i] === 'true';
        if ($searchable) {
          $sub_conditions[] = array('column' => $mProp, 'operator' => 'LIKE', 'value' => '%'.$sSearch.'%');
        }
      }
    }
    // NOTE
    // use closure to wrap the where conditions (sub_conditions) because we need to support OR query, 
    // pass the closure to the query engine (repository)
    // PS: this thing might be similar to linq to sql
    if (count($sub_conditions) > 0) {
      $conditions[] = function ($query) use ($sub_conditions) {
        for ($i=0; $i < count($sub_conditions); $i++) { 
          $sub_condition = $sub_conditions[$i];
          $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
          // hack, special case for date column
          if ($sub_condition['column'] === 'created_at' || $sub_condition['column'] === 'updated_at') {
            $query->orwhere(function ($query2) use ($sub_condition) {
              $query2->whereRaw("date_format(".$sub_condition['column'].", '%d %M %Y %H:%i') ".$sub_condition['operator']." '".$sub_condition['value']."'");
            });
          }
        }
      };
    }
    // END SPECIAL FILTER / SEARCH
    // ------------------------------------------------------------------------------------

    // author filter
    // the ID / integer value returned from the client side is the PROFILE ID
    if (Input::has('authorFilter')) {
      $f_author = (int)$input['authorFilter'];
      if ($f_author > 0) {
        $conditions[] = array('column' => 'user_profile_id', 'operator' => '=', 'value' => $f_author);
      }
    }
    // published filter
    if (Input::has('publishedFilter')) {
      $f_published = (int)$input['publishedFilter'];
      if ($f_published > -1) {
        // 0 = false
        // 1 = true
        // -1 not chosen
        $conditions[] = array('column' => 'published', 'operator' => '=', 'value' => $f_published);
      }
    }

    $posts = array();

    $iTotalRecords = 0;
    $iTotalDisplayRecords = 0;

    if (Auth::user()->isAdmin()) {
      $posts = $this->_blogService->getPaginatedPosts($pageNumber, $pageSize, $sorts, $conditions);
    } else {
      $conditions[] = array('column' => 'user_id', 'operator' => '=', 'value' => Auth::user()->id);
      $conditions[] = array('column' => 'user_profile_id', 'operator' => '=', 'value' => Auth::user()->userProfile->id);
      $posts = $this->_blogService->getPaginatedPosts($pageNumber, $pageSize, $sorts, $conditions);
    }
    $iTotalRecords = $this->_blogService->getPostsCount($conditions);
    $iTotalDisplayRecords = $iTotalRecords; 

    return Response::json(array(
      'sEcho' => $input['sEcho'],
      'aaData' => $posts,
      'iTotalRecords' => $iTotalRecords,
      'iTotalDisplayRecords' => $iTotalDisplayRecords
    ));
  }

  public function getAuthors()
  {
    return Response::json(array('authors' => $this->_blogService->getAllAuthors()));
  }

  /** 
   * [edit description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function edit($id) 
  {
    $post = $this->_blogService->getById($id);
    $lookups = $this->getFilters();
    $filters = $this->getSelectedFilters($post);

    View::share('activeLinkKey', 'blog');

    $entityName = Auth::user()->entityName;

    return View::make('admin.blog.edit')//->with('post', $post)
                                        ->with('model', $post)
                                        ->with('lookups', $lookups)
                                        ->with('filters', $filters)
                                        ->with('disclaimerEntityName', $entityName)
                                        ->with('disclaimerManageLink', 'Blog Manager -> All Posts');
  }

  /**
   * [update description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function update()
  {
    // validations
    $inputs = Input::all();
    $validator = new \Libraries\Validators\BlogPostValidator();
    
    // checks
    if ($validator->validate($inputs)) {
      // update post
      $post = $this->_blogService->updatePost($inputs, Auth::user());

      // redirect
      Flash::success(Messages::successEditPosting($post->title));
      return Redirect::route('admin_all_posts');
    } else {
      $id = Input::get('id');
      return Redirect::route('admin_edit_post', array($id))->withErrors($validator->errors())
                                                           ->withInput();
    }
  }

  /**
   * [create description]
   * @return [type] [description]
   */
  public function create() 
  {
    $post = new Post();
    $post->id = 0;
    $post->image = '/images/noimage.png';
    $post->name = GenericUtility::GUID();

    $lookups = $this->getFilters();
    $filters = $this->getSelectedFilters($post);

    View::share('activeLinkKey', 'blog.addpost');

    $entityName = Auth::user()->entityName;

    return View::make('admin.blog.create')//->with('post', $post)
                                          ->with('model', $post)
                                          ->with('lookups', $lookups)
                                          ->with('filters', $filters)
                                          ->with('disclaimerEntityName', $entityName)
                                          ->with('disclaimerManageLink', 'Blog Manager -> All Posts');
  }

  /**
   * [insert description]
   * @return [type] [description]
   */
  public function insert()
  {
    // validations
    $inputs = Input::all();
    //dd($inputs);
    $validator = new \Libraries\Validators\BlogPostValidator();

    // checks
    if ($validator->validate($inputs)) {
      // insert page to storage
      $post = $this->_blogService->insertPost($inputs, Auth::user());

      // redirect
      Flash::success(Messages::successPosting($post->title));
      return Redirect::route('admin_all_posts');
    } else {
      return Redirect::route('admin_create_post')->withErrors($validator->errors())
                                                 ->withInput();
    }
  }

  /**
   * [destroy description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function destroy($id)
  {
    $this->_blogService->deletePost($id);

    return Response::json(array('id' => $id, 'deleted' => true));
  }

  /**
   * [publish description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function publish($id) 
  { 
    $published = $this->_blogService->publishPost($id, Auth::user());
    return Response::json(array('id' => $id, 'published' => $published));
  }

  /**
   * [unpublish description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function unpublish($id) 
  { 
    $unpublished = $this->_blogService->unpublishPost($id, Auth::user());
    return Response::json(array('id' => $id, 'unpublished' => $unpublished));
  }

  /**
   * [fpPublish description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function fpPublish($id) 
  { 
    $published = $this->_blogService->publishPost($id, Auth::user());
    return Redirect::back();
  }

  /**
   * [fpUnpublish description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function fpUnpublish($id) 
  {
    $unpublished = $this->_blogService->unpublishPost($id, Auth::user());
    return Redirect::back();
  }

  /**
   * [preview description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function preview($id)
  {
    $model = $this->_blogService->getById($id);

    if ($model === null) {
      App::abort(404);
    }

    return View::make('admin.blog.preview')->with('model', $model);
  }

  /**
   * [getPreviewUrl description]
   * @return [type] [description]
   */
  public function getPreviewUrl()
  {
    $formdata = Input::all(); //get('formdata');
    $d = GenericUtility::GUID();
    $m = $this->_blogService->fromInputToModelForPreview($formdata, Auth::user());
    Session::flash('preview_blogs_'.$d, $m);
    $u = route('cms_blogs_preview', array('skey' => $d));
    return Response::json(array('ok' => true, 'url' => $u));
  }

  /**
   * [readonly description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function readonly($id)
  {
    $post = $this->_blogService->getById($id);
    $lookups = $this->getFilters();
    $filters = $this->getSelectedFilters($post);

    View::share('activeLinkKey', 'blog');

    $entityName = Auth::user()->entityName;

    return View::make('admin.blog.readonly')->with('model', $post)
                                            ->with('lookups', $lookups)
                                            ->with('filters', $filters)
                                            ->with('disclaimerEntityName', $entityName)
                                            ->with('disclaimerManageLink', 'Blog Manager -> All Posts');
  }

} 