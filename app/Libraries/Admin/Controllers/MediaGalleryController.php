<?php namespace Libraries\Admin\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as FilesystemAdapter;
use Libraries\Facades\Directories;

class MediaGalleryController extends AuthBaseController {

  //private $_filesystem;
  private $_mediaPath;
  private $_fileService;
  
  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
    $this->_mediaPath = Directories::getMemberMediaDirectory();
    $this->_fileService = App::make('\Libraries\Admin\Services\FileService');
    //$this->_filesystem = new Filesystem(new FilesystemAdapter($this->getMediaFullPath()));
  }

  private function getMediaRelativePath($folder = null)
  {
    $path = '';
    if (!is_null($folder) && isset($folder) && strlen($folder) > 0) {
      $path = '/'.ltrim(rtrim($this->_mediaPath, '/'), '/').'/'.ltrim(rtrim($folder, '/'), '/').'/';
    } else {
      $path = '/'.ltrim(rtrim($this->_mediaPath, '/'), '/').'/';
    }
    return $path;
  }

  /**
   * [getMediaFullPath description]
   * @param  [type] $folder [description]
   * @return [type]         [description]
   */
  private function getMediaFullPath($folder = null) 
  {
    $path = '';
    if (!is_null($folder) && isset($folder) && strlen($folder) > 0) {
      $path = $_SERVER['DOCUMENT_ROOT'].'/'.ltrim(rtrim($this->_mediaPath, '/'), '/').'/'.ltrim(rtrim($folder, '/'), '/').'/';
    } else {
      $path = $_SERVER['DOCUMENT_ROOT'].'/'.ltrim(rtrim($this->_mediaPath, '/'), '/').'/';
    }
    return $path;
  }

  /**
   * [index description]
   * @return [type] [description]
   */
  public function index() 
  {
    View::share('activeLinkKey', 'media.gallery');

    return View::make('admin.mediagallery.index');
  }

  /**
   * [getFiles description]
   * @param  [type] $folder [description]
   * @return [type]         [description]
   */
  public function getFiles() 
  {
    $folder = Input::get('folder');
    $relativePath = $this->getMediaRelativePath($folder);

    $fileModels = $this->_fileService->getFilesInDirectory($relativePath);

    return Response::json(array(
      'files' => $fileModels, 
    ));
  }

  /**
   * [createFolder description]
   * @param  [type] $folder [description]
   * @return [type]             [description]
   */
  public function createFolder() 
  {
    $folder = Input::get('folder');
    $fullFolderPath = $this->getMediaFullPath().ltrim($folder, '/');
    $this->_fileService->createFolder($fullFolderPath);
  }

  /**
   * [rename description]
   * @return [type] [description]
   */
  public function quickRename()
  {
    $folder = Input::get('folder');
    $oldName = $this->getMediaFullPath($folder).ltrim(Input::get('oldName'), '/');
    $newName = $this->getMediaFullPath($folder).ltrim(Input::get('newName'), '/');

    $this->_fileService->renameFile($oldName, $newName);

    $fileModel = new \Libraries\Models\FileModel($this->getMediaRelativePath($folder), Input::get('newName'));

    return Response::json(array(
      'success' => true, 
      'oldName' => $oldName, 
      'newName' => $newName,
      'model' => $fileModel
    ));
  }

  /**
   * [destroy description]
   * @return [type] [description]
   */
  public function destroy() 
  {
    $folder = Input::get('folder');
    $fileName = Input::get('fileName');
    $file = $this->getMediaFullPath($folder).ltrim($fileName, '/');
    $this->_fileService->destroyFile($file);
  }

  /**
   * [uploads description]
   * @return [type] [description]
   */
  public function uploads() 
  {
    $folder = Input::get('folder');
    $files = $_FILES['files'];

    $destinationFolder = $this->getMediaFullPath($folder);
    $destinationUrl = $this->getMediaRelativePath($folder); 

    $results = $this->_fileService->uploadFiles($destinationFolder, $destinationUrl, $files);

    $filejson = new \stdClass();
    $filejson->files = $results;

    return json_encode($filejson);
  }

}  