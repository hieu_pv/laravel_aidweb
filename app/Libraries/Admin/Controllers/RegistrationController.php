<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Libraries\Models\User;

class RegistrationController extends BaseController {

  private $_registrationService;
  private $_lookupService;
  private $_passwordService;

  /**
   * [__construct description]
   */
  public function __construct()
  {
    //parent::__construct();
    /*
    Within our constructor, we call the beforeFilter() method and pass in the string csrf, as the first argument. csrf is the filter that we want to apply to our actions. Then we pass in an array as the second argument and tell it to only apply this filter on POST requests. By doing this, our forms will pass along a CSRF token whenever they are submitted. This CSRF before filter will ensure that all POST requests to our app contain this token, giving us confidence that POST requests are not being issued to our application from other external sources.
     */
    //$this->beforeFilter('csrf', array('on'=>'post'));

    $this->_registrationService = App::make('\Libraries\Admin\Services\RegistrationService');
    $this->_lookupService = App::make('\Libraries\Admin\Services\LookupService');
    $this->_passwordService = App::make('\Libraries\Admin\Services\PasswordService');
  }

  public function register($mode)
  {
    if ($mode === 'individual') {

      return View::make('admin.accounts.register_ind');

    } else if ($mode === 'organisation') {

      return View::make('admin.accounts.register_org');

    }
  }
   
  public function postRegister()
  {
    $inputs = Input::all();
    $validator = new \Libraries\Validators\RegisterValidator();

    if ($validator->validate($inputs)) {
      $response = $this->_registrationService->registerUser($inputs);

      if ($response->ok) {
        return Redirect::route('account_registered');
      } else {
        // $errors = $validator->errors();
        // foreach ($response->messages as $msg) {
        //   $errors->add('', $msg);
        // }
        // return Redirect::back()->withErrors($validator->errors());
      }
      
    } else {
      return Redirect::back()->withInput()->withErrors($validator->errors());
    }
  }

  public function registered()
  {
    return View::make('admin.accounts.confirm');
  }

  public function verify()
  {
    $token = Input::get('token');
    $user_id = Input::get('user_id');

    $decodedToken = base64_decode($token);

    $response = $this->_registrationService->verifyRegistration($user_id, $decodedToken);

    if ($response->ok) {
      if ($response->user !== null) {
        // auto login
        Auth::login($response->user);
        // redirect to profile
        return Redirect::route('admin_profile');
      }
    }
  }

  private function getLookupsForProfile()
  {
    $lookups = new \stdClass();

    $lookups->bases = $this->_lookupService->getBasesAsList();
    unset($lookups->bases[0]);

    $lookups->nationalities = $this->_lookupService->getNationalitiesAsList();
    unset($lookups->nationalities[0]);

    $lookups->member_statuses = $this->_lookupService->getMemberStatusesAsList();
    unset($lookups->member_statuses[0]);

    $lookups->professional_statuses = $this->_lookupService->getProfessionalStatusesAsList();
    unset($lookups->professional_statuses[0]);

    $lookups->office_locations = $this->_lookupService->getOfficeLocationsAsList();
    unset($lookups->office_locations[0]);

    $lookups->office_types = $this->_lookupService->getOfficeTypesAsList();
    unset($lookups->office_types[0]);

    $lookups->organisation_types = $this->_lookupService->getOrganisationTypesAsList();
    unset($lookups->organisation_types[0]);

    $lookups->level_of_activities = $this->_lookupService->getLevelOfActivitiesAsList();
    unset($lookups->level_of_activities[0]);

    $lookups->level_of_responsibilities = $this->_lookupService->getLevelOfResponsibilitiesAsList();
    unset($lookups->level_of_responsibilities[0]);

    return $lookups;
  }

  /**
   * [login description]
   * @return [type] [description]
   */
  public function login()
  {
    return View::make('admin.login');
  }

  /**
   * [postLogin description]
   * @return [type] [description]
   */
  public function postLogin()
  {
    $username = Input::get('username');
    $secret = Input::get('secret');
    
    $remember = false;
    if (Input::has('remember')) {
      $rememberValue = Input::get('remember');
      $remember = $rememberValue === 'remember-me';
    }

    $checkResponse = $this->_registrationService->checkUsernamePreLogin($username);
    if ($checkResponse->somethingWrong) {
      if (!$checkResponse->verified) {
        return Redirect::route('admin_login')->with('login_errors', 'Your account is not activated yet. If you have not received an email containing instructions to activate your account, please click <a href="#" data-url="'.URL::route('account_resend_verification', array('user_id' => $checkResponse->user_id)).'" class="resend-verification"><strong>here</strong></a>.')
                                             ->withInput();
      }
    }

    if (Auth::attempt(array('username' => $username, 'password' => $secret), $remember)) {
      $intended = Session::pull('url.intended');
      if ($intended === null) {
        $previousUrl = Input::get('previousUrl');
        if ($previousUrl !== null && strlen($previousUrl) > 0) {
          if (strpos($previousUrl, '/oadmin/login') !== false) {
            $previousUrl = '/oadmin';
          }
          Session::put('url.intended', $previousUrl);
        }
      } else {
        if (strpos($intended, '/oadmin/login') !== false) {
          $intended = '/oadmin';
        }
        Session::put('url.intended', $intended);
      }
      return Redirect::intended(URL::route('admin_dashboard2'));
    } else {
      return Redirect::route('admin_login')->with('login_errors', 'Your username/password combination was incorrect.')
                                           ->withInput();
    }
  }

  public function resendVerification()
  {
    $userId = Input::get('user_id');
    $this->_registrationService->resendRegistration($userId);
    return Response::json(array('ok' => true));
  }

  public function resendVerification2()
  {
    $username = Input::get('username_to_resend_verification');
    $email = Input::get('email_to_resend_verification');

    $this->_registrationService->resendRegistration2($username, $email);

    return Response::json(array('ok' => true));
  }

  /**
   * [resetPassword description] >> GET REMINDER EMAIL
   * @return [type] [description]
   */
  public function resetPassword()
  {
    if (Input::has('email_to_reset_password') && Input::has('username_to_reset_password')) {
      // check valid email
      $email = Input::get('email_to_reset_password');
      $username = Input::get('username_to_reset_password');

      $response = $this->_passwordService->resetPassword($username, $email);

      return Response::json($response);
    } else {
      return Response::json(array('reset' => false, 'messages' => array('No Email Address and/or Username Specified')));
    }
  }

  /**
   * [resendPasswordReminder description]
   * @return [type] [description]
   */
  public function resendPasswordReminder()
  {
    if (Input::has('email_to_reset_password') && Input::has('username_to_reset_password')) {
      $username = Input::get('username_to_reset_password');
      $email = input::get('email_to_reset_password');

      $response = $this->_passwordService->resendReminder($username, $email);

      return Response::json($response);
    } else {
      return Response::json(array('ok' => false, 'messages' => array('No Email Address and/or Username Specified')));
    }
  }

  /**
   * [doResetPassword description] >> SERVE NEW PASSWORD FORM
   * @return [type] [description]
   */
  public function doResetPassword()
  {
    $token = Input::get('token');
    $decodedToken = base64_decode($token);

    $response = $this->_passwordService->getPasswordReminderByToken($decodedToken);

    return View::make('admin.accounts.reset_password')->with('ok', $response->ok)
                                                      ->with('messages', $response->messages)
                                                      ->with('passwordReminder', $response->passwordReminder);
  }

  /**
   * [doResetPassword2 description] >> POST FORM / FORM ACTION
   * @return [type] [description]
   */
  public function doResetPassword2()
  {
    $credentials = Input::only('username', 'email', 'new_password', 'new_password_confirmation', 'token');
    $encodedToken = base64_encode(Input::get('token'));

    $validator = new \Libraries\Validators\ResetPasswordValidator();
    
    if ($validator->validate($credentials)) {
      
      $response = $this->_passwordService->doResetPassword((object)$credentials);

      if ($response->ok) {
        Session::flash('afterReset', true);
        return Redirect::route('account_do_resetpassword');
      } else {
        // $errors = $validator->errors();
        // foreach ($response->messages as $msg) {
        //   $errors->add('', $msg);
        // }
        // return Redirect::route('account_do_resetpassword', array('token' => $encodedToken))->withErrors($errors);
      }

    } else {
      return Redirect::route('account_do_resetpassword', array('token' => $encodedToken))->withErrors($validator->errors());
    }
  }

  public function pingEmailRegistration()
  {
    $user = User::find(1);
    return View::make('emails.register.verify')->with('token', base64_encode('dummy'))
                                               ->with('user', $user);
  }
  
}