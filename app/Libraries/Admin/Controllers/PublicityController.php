<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Form;
use Libraries\Models\User;
use Libraries\Models\Publicities\Publicity;
use Libraries\Facades\Messages;
use Libraries\Support\Modules;
use Laracasts\Flash\Flash;
use Libraries\Facades\GenericUtility;

class PublicityController extends AuthBaseController {

  private $_publicitiesService;
  private $_platformService;

  public function __construct()
  {
    parent::__construct();
    $this->_publicitiesService = App::make('\Libraries\Admin\Services\PublicitiesService');
    $this->_platformService = App::make('\Libraries\Admin\Services\PlatformService');
    $this->beforeFilter('posting_limitation', array('only' => array('create')));
  }

  public function index()
  {
    View::share('activeLinkKey', 'publicities.allpublicities');

    return View::make('admin.publicities.index'); 
  }

  public function create() 
  {
    $model = new Publicity();
    $model->id = 0;
    $model->type_id = Modules::PUBLICITY_TYPE_BIG;
    $model->a_title_color = '#222222';
    $model->a_text_color = '#222222';
    $model->a_button_text_color = '#222222';
    $model->a_position = 'LeftTop';
    $model->a_image = '/images/noimage.png';
    $model->b_image = '/images/noimage.png';
    $model->c_image = '/images/noimage.png';
    $model->a_show_org_logo = true;
    $model->selected_platforms = [];

    View::share('activeLinkKey', 'publicities.addpublicity');

    $types = $this->_publicitiesService->getPublicityTypes();
    $ping = $this->preserveOldInput($model);
    $entityName = Auth::user()->entityName;
    $platforms = $this->_platformService->getActivePlatforms();

    return View::make('admin.publicities.create')->with('model', $model)
                                                 ->with('types', $types)
                                                 ->with('ping', $ping)
                                                 ->with('disclaimerEntityName', $entityName)
                                                 ->with('disclaimerManageLink', 'Publicities Manager -> All Publicities')
                                                 ->with('platforms', $platforms);
  }

  public function insert()
  {
    // validations
    $inputs = Input::all();

    $inputs['a_image_x'] = Input::hasFile('a_image', $inputs) ? 'x' : '';
    $inputs['b_image_x'] = Input::hasFile('b_image') ? 'x' : '';
    $inputs['c_image_x'] = Input::hasFile('c_image', $inputs) ? 'x' : '';

    //return Redirect::route('admin_create_publicity')->withInput();

    $validator = null;
    if (intval($inputs['type_id']) === Modules::PUBLICITY_TYPE_BIG) {
      $validator = new \Libraries\Validators\PublicityTypeAValidator();
    }
    if (intval($inputs['type_id']) === Modules::PUBLICITY_TYPE_SMALL) {
      $validator = new \Libraries\Validators\PublicityTypeBValidator();
    }
    if (intval($inputs['type_id']) === Modules::PUBLICITY_TYPE_SIDE) {
      $validator = new \Libraries\Validators\PublicityTypeCValidator();
    }

    if ($validator->validate($inputs)) {
      // save to storage
      $model = $this->_publicitiesService->insertPublicity($inputs, Auth::user());
      // redirect
      Flash::success(Messages::successPostPublicity());
      return Redirect::route('admin_all_publicities');
    } else {
      return Redirect::route('admin_create_publicity')->withErrors($validator->errors())
                                                      ->withInput();
    }
  }

  public function edit($id)
  {
    $model = $this->_publicitiesService->getById($id);

    $model->a_image_x = $model->a_image === '/images/noimage.png' ? '' : 'x';
    $model->b_image_x = $model->b_image === '/images/noimage.png' ? '' : 'x';
    $model->c_image_x = $model->c_image === '/images/noimage.png' ? '' : 'x';

    View::share('activeLinkKey', 'publicities');

    $types = $this->_publicitiesService->getPublicityTypes();
    $ping = $this->preserveOldInput($model);
    $entityName = Auth::user()->entityName;
    $platforms = $this->_platformService->getActivePlatforms();

    return View::make('admin.publicities.edit')->with('model', $model)
                                               ->with('types', $types)
                                               ->with('ping', $ping)
                                               ->with('disclaimerEntityName', $entityName)
                                               ->with('disclaimerManageLink', 'Publicities Manager -> All Publicities')
                                               ->with('platforms', $platforms);
  }

  public function update()
  {
    // validations
    $inputs = Input::all();

    if ($inputs['a_image_x'] === '') {
      $inputs['a_image_x'] = Input::hasFile('a_image', $inputs) ? 'x' : '';
    }
    if ($inputs['b_image_x'] === '') {
      $inputs['b_image_x'] = Input::hasFile('b_image', $inputs) ? 'x' : '';
    }
    if ($inputs['c_image_x'] === '') {
      $inputs['c_image_x'] = Input::hasFile('c_image', $inputs) ? 'x' : '';
    }

    // $id = Input::get('id');
    // return Redirect::route('admin_edit_publicity', array($id))->withInput();

    $validator = null;
    if (intval($inputs['type_id']) === Modules::PUBLICITY_TYPE_BIG) {
      $validator = new \Libraries\Validators\PublicityTypeAValidator();
    }
    if (intval($inputs['type_id']) === Modules::PUBLICITY_TYPE_SMALL) {
      $validator = new \Libraries\Validators\PublicityTypeBValidator();
    }
    if (intval($inputs['type_id']) === Modules::PUBLICITY_TYPE_SIDE) {
      $validator = new \Libraries\Validators\PublicityTypeCValidator();
    }
    
    if ($validator->validate($inputs)) {
      // update 
      $model = $this->_publicitiesService->updatePublicity($inputs, Auth::user());
      // redirect
      Flash::success(Messages::successPostPublicity($model->title));
      return Redirect::route('admin_all_publicities');
    } else {
      $id = Input::get('id');
      return Redirect::route('admin_edit_publicity', array($id))->withErrors($validator->errors())
                                                                ->withInput();
    }
  }

  private function preserveOldInput($model)
  {
    // ping, for js
    $ping = new \stdClass();
    $ping->type_id = $model->type_id;
    $ping->a_title_color = $model->a_title_color;
    $ping->a_text_color = $model->a_text_color;
    $ping->a_button_text_color = $model->a_button_text_color;
    $ping->a_position = $model->a_position;
    $ping->a_show_org_logo = $model->a_show_org_logo;
    $ping->selected_platforms = $model->selected_platforms;
    if (!Form::oldInputIsEmpty()) {
      $ping->type_id = Form::old('type_id');
      $ping->a_title_color = Form::old('a_title_color');
      $ping->a_text_color = Form::old('a_text_color');
      $ping->a_button_text_color = Form::old('a_button_text_color');
      $ping->a_position = Form::old('a_position');
      $ping->a_show_org_logo = Form::old('a_show_org_logo');
      $ping->selected_platforms = Form::old('selected_platforms');
    }
    return $ping;
  }

  public function getData()
  {
    // NOTE:
    // all code below is optimized for dataTables. Changing the code, ensure the integrity with the dataTables in Client-side
    // 
    $input = Input::all();

    $pageSize = (int)$input['iDisplayLength'];
    $pageNumber = ((int)$input['iDisplayStart']) / $pageSize + 1;

    // SORTING
    $sorts = array(
      array('column' => 'created_at', 'direction' => 'desc')
    );
    if (Input::has('iSortCol_0')) {
      // assume iSortingCols = 1
      $sortingColIdx = (int)$input['iSortCol_0'];
      $sortingCol = $input['mDataProp_'.$sortingColIdx];
      $sortingDirection = $input['sSortDir_0'];
      $sorts = array(
        array('column' => $sortingCol, 'direction' => $sortingDirection)
      );
    }

    // FILTERS
    $conditions = array();
    // ------------------------------------------------------------------------------------
    // SPECIAL FILTER / SEARCH - handle OR
    // 
    // NOTE
    // Conditions algorithm: 
    // - get search string from dataTables
    // - check what columns are searchable
    // - construct and array of conditions as 'column', 'operator', 'value' in a sub_conditions, 
    //   because we need to support closure for OR based where
    $sub_conditions = array();
    $sSearch = $input['sSearch'];
    if (strlen(strval($sSearch)) > 0) {
      $iColumns = (int)$input['iColumns'];
      for ($i=0; $i < $iColumns; $i++) { 
        $mProp = $input['mDataProp_'.$i];
        $searchable = $input['bSearchable_'.$i] === 'true';
        if ($searchable) {
          $sub_conditions[] = array('column' => $mProp, 'operator' => 'LIKE', 'value' => '%'.$sSearch.'%');
        }
      }
    }
    // NOTE
    // use closure to wrap the where conditions (sub_conditions) because we need to support OR query, 
    // pass the closure to the query engine (repository)
    // PS: this thing might be similar to linq to sql
    if (count($sub_conditions) > 0) {
      $conditions[] = function ($query) use ($sub_conditions) {
        for ($i=0; $i < count($sub_conditions); $i++) { 
          $sub_condition = $sub_conditions[$i];
          // hack, special case for date column
          // if(type_id = 1, a_title, if(type_id = 2, b_title, if(type_id = 3, c_title, ''))) as title
          if ($sub_condition['column'] === 'title') {
            //$query->orwhere('if(type_id = 1, a_title, if(type_id = 2, b_title, if(type_id = 3, c_title, ""))) as title', $sub_condition['operator'], $sub_condition['value']);
            $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
          } else if ($sub_condition['column'] === 'created_at' || $sub_condition['column'] === 'updated_at') {
            $query->orwhere(function ($query2) use ($sub_condition) {
              $query2->whereRaw("date_format(".$sub_condition['column'].", '%d %M %Y %H:%i') ".$sub_condition['operator']." '".$sub_condition['value']."'");
            });
          } else {
            $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
          } 
        }
      };
    }
    // END SPECIAL FILTER / SEARCH
    // ------------------------------------------------------------------------------------
    
    // author filter
    if (Input::has('authorFilter')) {
      $f_author = (int)$input['authorFilter'];
      if ($f_author > 0) {
        $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => $f_author);
      }
    }

    // type filter
    if (Input::has('typeFilter')) {
      $f_type = (int)$input['typeFilter'];
      if ($f_type > 0) {
        $conditions[] = array('column' => 'type_id', 'operator' => '=', 'value' => $f_type);
      }
    }

    // published filter
    if (Input::has('publishedFilter')) {
      $f_published = (int)$input['publishedFilter'];
      if ($f_published > -1) {
        // 0 = false
        // 1 = true
        // -1 not chosen
        $conditions[] = array('column' => 'published', 'operator' => '=', 'value' => $f_published);
      }
    }

    $models = array();

    $iTotalRecords = 0;
    $iTotalDisplayRecords = 0;

    if (Auth::user()->isAdmin()) {
      $models = $this->_publicitiesService->getPaginatedPublicities($pageNumber, $pageSize, $sorts, $conditions);
    } else {
      $conditions[] = array('column' => 'user_id', 'operator' => '=', 'value' => Auth::user()->id);
      $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => Auth::user()->organisation->id);
      $models = $this->_publicitiesService->getPaginatedPublicities($pageNumber, $pageSize, $sorts, $conditions);
    }
    $iTotalRecords = $this->_publicitiesService->getPublicitiesCount($conditions);
    $iTotalDisplayRecords = $iTotalRecords;

    return Response::json(array(
      'sEcho' => $input['sEcho'],
      'aaData' => $models,
      'iTotalRecords' => $iTotalRecords,
      'iTotalDisplayRecords' => $iTotalDisplayRecords
    ));
  }

  public function getAuthors()
  {
    return Response::json(array('authors' => $this->_publicitiesService->getAllAuthors()));
  }

  public function getPublicityTypes()
  {
    return Response::json(array('types' => $this->_publicitiesService->getPublicityTypes()));
  }

  public function publish($id) 
  { 
    $published = $this->_publicitiesService->publishPublicity($id, Auth::user());
    return Response::json(array('id' => $id, 'published' => $published));
  }

  public function unpublish($id) 
  { 
    $unpublished = $this->_publicitiesService->unpublishPublicity($id, Auth::user());
    return Response::json(array('id' => $id, 'unpublished' => $unpublished));
  }

  public function fpPublish($id) 
  { 
    $published = $this->_publicitiesService->publishPublicity($id, Auth::user());
    return Redirect::back();
  }

  public function fpUnpublish($id) 
  {
    $unpublished = $this->_publicitiesService->unpublishPublicity($id, Auth::user());
    return Redirect::back();
  }

  public function preview($id)
  {
    $model = $this->_publicitiesService->getById($id);

    if ($model === null) {
      App::abort(404);
    }

    return View::make('admin.publicities.preview')->with('model', $model);
  }

  public function destroy($id)
  {
    $this->_publicitiesService->deletePublicity($id);

    return Response::json(array('id' => $id, 'deleted' => true));
  }

  public function getPreviewUrl()
  {
    $formdata = Input::all(); //get('formdata');
    $d = GenericUtility::GUID();
    $m = $this->_publicitiesService->fromInputToModelForPreview($formdata, Auth::user());
    Session::flash('preview_publicity_'.$d, $m);
    $u = route('cms_publicity_preview', array('skey' => $d));
    return Response::json(array('ok' => true, 'url' => $u));
  }

  public function readonly($id)
  {
    $model = $this->_publicitiesService->getById($id);

    $model->a_image_x = $model->a_image === '/images/noimage.png' ? '' : 'x';
    $model->b_image_x = $model->b_image === '/images/noimage.png' ? '' : 'x';
    $model->c_image_x = $model->c_image === '/images/noimage.png' ? '' : 'x';

    View::share('activeLinkKey', 'publicities');

    $types = $this->_publicitiesService->getPublicityTypes();
    $ping = $this->preserveOldInput($model);
    $entityName = Auth::user()->entityName;
    $platforms = $this->_platformService->getActivePlatforms();

    return View::make('admin.publicities.readonly')->with('model', $model)
                                                   ->with('types', $types)
                                                   ->with('ping', $ping)
                                                   ->with('disclaimerEntityName', $entityName)
                                                   ->with('disclaimerManageLink', 'Publicities Manager -> All Publicities')
                                                   ->with('platforms', $platforms);
  }

}
