<?php namespace Libraries\Admin\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Libraries\Facades\GenericUtility;
use Carbon\Carbon;

class MethodsController extends AuthBaseController {

  /**
   * [__construct description]
   */
  public function __construct()
  {
    parent::__construct();
  }

  public function getSlug() 
  {
    if (Input::has('title')) {
      $title = Input::get('title');
      $slug = Str::slug($title);
      return Response::json(array('title' => $title, 'slug' => $slug));
    } else {
      return Response::json(array('title' => '', 'slug' => ''));
    }
  }

  public function getVideoDetails()
  {
    if (Input::has('url')) {
      $url = Input::get('url');
      
      $model = new \Libraries\Models\Videos\Video();
      $model->url = GenericUtility::prefixWithHttpIfNeeded($url);
      $model->setVideoDetailsBasedOnUrl();

      $ret = new \stdClass();
      $ret->youtube_id = $model->youtube_id;
      $ret->thumbnail = $model->image;
      $ret->duration = $model->displayed_duration;

      return Response::json(array('details' => $ret));
    }
  }

  public function getCountries()
  {
    $countries = $this->_filterService->getCountries();

    return Response::json(array('countries' => $countries));
  }

  public function getNationalities()
  {
    $data = $this->_lookupService->getNationalities();

    return Response::json(array('nationalities' => $data));
  }

  public function getMemberStatuses()
  {
    $data = $this->_lookupService->getMemberStatuses();

    return Response::json(array('memberStatuses' => $data));
  }

  public function getProfessionalStatuses()
  {
    $data = $this->_lookupService->getProfessionalStatuses();

    return Response::json(array('professionalStatuses' => $data));
  }

  public function getLastPosts()
  {
    $service = App::make('\Libraries\Admin\Services\PostingService');
    $o = $service->getLastPosts();

    return Response::json($o);
  }

}