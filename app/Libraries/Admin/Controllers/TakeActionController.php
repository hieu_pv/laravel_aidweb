<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Form;
use Libraries\Facades\Directories;
use Libraries\Facades\Messages;
use Libraries\Facades\GenericUtility;
use Libraries\Models\TakeActions\TakeAction;
use Laracasts\Flash\Flash;

class TakeActionController extends AuthBaseController {

  private $_takeActionsService;

  public function __construct()
  {
    parent::__construct();
    $this->_takeActionsService = App::make('\Libraries\Admin\Services\TakeActionsService');

    $this->beforeFilter('posting_limitation', array('only' => array('create')));
  }

  public function index()
  {
    View::share('activeLinkKey', 'takeactions.alltakeactions');

    return View::make('admin.takeactions.index'); 
  }

  public function getData()
  {
    // NOTE:
    // all code below is optimized for dataTables. Changing the code, ensure the integrity with the dataTables in Client-side
    // 
    $input = Input::all();

    $pageSize = (int)$input['iDisplayLength'];
    $pageNumber = ((int)$input['iDisplayStart']) / $pageSize + 1;

    // SORTING
    $sorts = array(
      array('column' => 'created_at', 'direction' => 'desc')
    );
    if (Input::has('iSortCol_0')) {
      // assume iSortingCols = 1
      $sortingColIdx = (int)$input['iSortCol_0'];
      $sortingCol = $input['mDataProp_'.$sortingColIdx];
      $sortingDirection = $input['sSortDir_0'];
      $sorts = array(
        array('column' => $sortingCol, 'direction' => $sortingDirection)
      );
    }

    // FILTERS
    $conditions = array();
    // ------------------------------------------------------------------------------------
    // SPECIAL FILTER / SEARCH - handle OR
    // 
    // NOTE
    // Conditions algorithm: 
    // - get search string from dataTables
    // - check what columns are searchable
    // - construct and array of conditions as 'column', 'operator', 'value' in a sub_conditions, 
    //   because we need to support closure for OR based where
    $sub_conditions = array();
    $sSearch = $input['sSearch'];
    if (strlen(strval($sSearch)) > 0) {
      $iColumns = (int)$input['iColumns'];
      for ($i=0; $i < $iColumns; $i++) { 
        $mProp = $input['mDataProp_'.$i];
        $searchable = $input['bSearchable_'.$i] === 'true';
        if ($searchable) {
          $sub_conditions[] = array('column' => $mProp, 'operator' => 'LIKE', 'value' => '%'.$sSearch.'%');
        }
      }
    }
    // NOTE
    // use closure to wrap the where conditions (sub_conditions) because we need to support OR query, 
    // pass the closure to the query engine (repository)
    // PS: this thing might be similar to linq to sql
    if (count($sub_conditions) > 0) {
      $conditions[] = function ($query) use ($sub_conditions) {
        for ($i=0; $i < count($sub_conditions); $i++) { 
          $sub_condition = $sub_conditions[$i];
          $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
          // hack, special case for date column
          if ($sub_condition['column'] === 'created_at' || $sub_condition['column'] === 'updated_at') {
            $query->orwhere(function ($query2) use ($sub_condition) {
              $query2->whereRaw("date_format(".$sub_condition['column'].", '%d %M %Y %H:%i') ".$sub_condition['operator']." '".$sub_condition['value']."'");
            });
          }
        }
      };
    }
    // END SPECIAL FILTER / SEARCH
    // ------------------------------------------------------------------------------------

    // author filter
    if (Input::has('authorFilter')) {
      $f_author = (int)$input['authorFilter'];
      if ($f_author > 0) {
        $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => $f_author);
      }
    }

    // type filter
    if (Input::has('typeFilter')) {
      $f_type = (int)$input['typeFilter'];
      if ($f_type > 0) {
        $conditions[] = array('column' => 'type_id', 'operator' => '=', 'value' => $f_type);
      }
    }

    // published filter
    if (Input::has('publishedFilter')) {
      $f_published = (int)$input['publishedFilter'];
      if ($f_published > -1) {
        // 0 = false
        // 1 = true
        // -1 not chosen
        $conditions[] = array('column' => 'published', 'operator' => '=', 'value' => $f_published);
      }
    }

    $models = array();

    $iTotalRecords = 0;
    $iTotalDisplayRecords = 0;

    if (Auth::user()->isAdmin()) {
      $models = $this->_takeActionsService->getPaginatedTakeActions($pageNumber, $pageSize, $sorts, $conditions);
    } else {
      $conditions[] = array('column' => 'user_id', 'operator' => '=', 'value' => Auth::user()->id);
      $conditions[] = array('column' => 'organisation_id', 'operator' => '=', 'value' => Auth::user()->organisation->id);
      $models = $this->_takeActionsService->getPaginatedTakeActions($pageNumber, $pageSize, $sorts, $conditions);
    }
    $iTotalRecords = $this->_takeActionsService->getTakeActionsCount($conditions);
    $iTotalDisplayRecords = $iTotalRecords; 

    return Response::json(array(
      'sEcho' => $input['sEcho'],
      'aaData' => $models,
      'iTotalRecords' => $iTotalRecords,
      'iTotalDisplayRecords' => $iTotalDisplayRecords
    ));
  }

  public function getAuthors() 
  {
    return Response::json(array('authors' => $this->_takeActionsService->getAllAuthors()));
  }

  public function getTakeActionTypes()
  {
    return Response::json(array('types' => $this->_takeActionsService->getTakeActionTypes()));
  }

  public function destroy($id)
  {
    $this->_takeActionsService->deleteTakeAction($id);

    return Response::json(array('id' => $id, 'deleted' => true));
  }

  public function publish($id) 
  { 
    $published = $this->_takeActionsService->publishTakeAction($id, Auth::user());
    return Response::json(array('id' => $id, 'published' => $published));
  }

  public function unpublish($id) 
  { 
    $unpublished = $this->_takeActionsService->unpublishTakeAction($id, Auth::user());
    return Response::json(array('id' => $id, 'unpublished' => $unpublished));
  }

  public function create() 
  {
    $model = new TakeAction();
    $model->id = 0;
    $model->image = '/images/noimage.png';
    $model->name = GenericUtility::GUID();

    View::share('activeLinkKey', 'takeactions.addtakeaction');

    $lookups = $this->getFilters();
    $filters = $this->getSelectedFilters($model);
    $types = $this->_takeActionsService->getTakeActionTypesAsList();
    $jtypes = $this->_takeActionsService->getTakeActionTypes();

    $seltypeid = 0;
    if (!Form::oldInputIsEmpty()) {
      $seltypeid = Form::old('type_id');
    }

    $entityName = Auth::user()->entityName;

    return View::make('admin.takeactions.create')->with('model', $model)
                                                 ->with('lookups', $lookups)
                                                 ->with('types', $types)
                                                 ->with('jtypes', $jtypes)
                                                 ->with('seltypeid', $seltypeid)
                                                 ->with('filters', $filters)
                                                 ->with('disclaimerEntityName', $entityName)
                                                 ->with('disclaimerManageLink', 'Get Involved Manager -> All Get Involved');
  }

  public function insert()
  {
    // validations
    $inputs = Input::all();
    $validator = new \Libraries\Validators\TakeActionValidator();

    if ($validator->validate($inputs)) {
      if (Input::get('type_id') <= 0) {
        $errors = array();
        array_push($errors, 'Type is required.');
        return Redirect::route('admin_create_takeaction')->withErrors($errors)
                                                         ->withInput();
      }

      // save to storage
      $model = $this->_takeActionsService->insertTakeAction($inputs, Auth::user());

      // redirect
      Flash::success(Messages::successPosting($model->title));
      return Redirect::route('admin_all_takeactions');
    } else {
      return Redirect::route('admin_create_takeaction')->withErrors($validator->errors())
                                                       ->withInput();
    }
  }

  public function edit($id) 
  {
    $model = $this->_takeActionsService->getById($id);
    $lookups = $this->getFilters();
    $filters = $this->getSelectedFilters($model);

    View::share('activeLinkKey', 'takeactions');

    $entityName = Auth::user()->entityName;
    $types = $this->_takeActionsService->getTakeActionTypesAsList();
    $jtypes = $this->_takeActionsService->getTakeActionTypes();

    $seltypeid = $model->type_id;
    if (!Form::oldInputIsEmpty()) {
      $seltypeid = Form::old('type_id');
    }

    return View::make('admin.takeactions.edit')->with('model', $model)
                                               ->with('lookups', $lookups)
                                               ->with('types', $types)
                                               ->with('jtypes', $jtypes)
                                               ->with('seltypeid', $seltypeid)
                                               ->with('filters', $filters)
                                               ->with('disclaimerEntityName', $entityName)
                                               ->with('disclaimerManageLink', 'News Manager -> All News');
  }

  public function update()
  {
    // validations
    $inputs = Input::all();
    $validator = new \Libraries\Validators\TakeActionValidator();
    
    if ($validator->validate($inputs)) {
      if (Input::get('type_id') <= 0) {
        $id = Input::get('id');
        $errors = array();
        array_push($errors, 'Type is required.');
        return Redirect::route('admin_edit_takeaction', array($id))->withErrors($errors)
                                                                   ->withInput();
      }

      // update 
      $model = $this->_takeActionsService->updateTakeAction($inputs, Auth::user());

      // redirect
      Flash::success(Messages::successEditPosting($model->title));
      return Redirect::route('admin_all_takeactions');
    } else {
      $id = Input::get('id');
      return Redirect::route('admin_edit_takeaction', array($id))->withErrors($validator->errors())
                                                                 ->withInput();
    }
  }

  public function fpPublish($id) 
  { 
    $published = $this->_takeActionsService->publishTakeAction($id, Auth::user());
    return Redirect::back();
  }

  public function fpUnpublish($id) 
  {
    $unpublished = $this->_takeActionsService->unpublishTakeAction($id, Auth::user());
    return Redirect::back();
  }

  public function preview($id)
  {
    $model = $this->_takeActionsService->getById($id);

    if ($model === null) {
      App::abort(404);
    }

    return View::make('admin.takeactions.preview')->with('model', $model);
  }

  public function getPreviewUrl()
  {
    $formdata = Input::all(); 
    $d = GenericUtility::GUID();
    $m = $this->_takeActionsService->fromInputToModelForPreview($formdata, Auth::user());
    Session::flash('preview_takeactions_'.$d, $m);
    $u = route('cms_takeactions_preview', array('skey' => $d));
    return Response::json(array('ok' => true, 'url' => $u));
  }

  public function readonly($id) 
  {
    $model = $this->_takeActionsService->getById($id);
    $lookups = $this->getFilters();
    $filters = $this->getSelectedFilters($model);

    View::share('activeLinkKey', 'takeactions');

    $entityName = Auth::user()->entityName;
    $types = $this->_takeActionsService->getTakeActionTypesAsList();
    $jtypes = $this->_takeActionsService->getTakeActionTypes();

    $seltypeid = $model->type_id;
    if (!Form::oldInputIsEmpty()) {
      $seltypeid = Form::old('type_id');
    }

    return View::make('admin.takeactions.readonly')->with('model', $model)
                                                   ->with('lookups', $lookups)
                                                   ->with('types', $types)
                                                   ->with('jtypes', $jtypes)
                                                   ->with('seltypeid', $seltypeid)
                                                   ->with('filters', $filters)
                                                   ->with('disclaimerEntityName', $entityName)
                                                   ->with('disclaimerManageLink', 'News Manager -> All News');
  }

}
