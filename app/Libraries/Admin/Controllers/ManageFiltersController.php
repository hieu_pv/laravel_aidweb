<?php namespace Libraries\Admin\Controllers; 

use Jenssegers\Date\Date;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Libraries\Facades\SettingsProvider;

class ManageFiltersController extends AuthBaseController {

  private $_userService;

  public function __construct()
  {
    parent::__construct();
    $this->_userService = App::make('\Libraries\Admin\Services\UserService');
  }

  /**
   * [index description]
   * @return [type] [description]
   */
  public function index() 
  {
    View::share('activeLinkKey', 'managefilters.index');

    return View::make('admin.filters.index');
  }

  public function getAllFilters()
  {
    $data = new \stdClass();
    $data->countries = $this->_filterService->getCountries();
    $data->regions = $this->_filterService->getRegions();
    $data->crisis = $this->_filterService->getCrisis();
    $data->sectors = $this->_filterService->getSectors();
    $data->themes = $this->_filterService->getThemes();
    $data->interventions = $this->_filterService->getInterventions();
    $data->beneficiaries = $this->_filterService->getBeneficiaries();

    $d = $data;

    return Response::json(array('filters' => $d));
  }

  /**
   * [deleteFilter description]
   * @return [type] [description]
   */
  public function deleteFilter() 
  {
    $filterName = Input::get('filterName');
    $id = Input::get('itemId');
    // to shorten the time, directly do it from here
    // TODO: refactor
    try {
      DB::table($filterName)->where('id', '=', $id)->update(['deleted' => true]);
      // TODO: broadcast event that filter is deleted, remove db record related to the filter
      return Response::json(array('ok' => true));
    } catch (\Exception $ex) {
      return Response::json(array('ok' => false));
    }
  }

  /**
   * [updateFilter description]
   * @return [type] [description]
   */
  public function updateFilter()
  {
    $filterName = Input::get('filterName');
    $id = Input::get('itemId');
    $name = Input::get('itemName');
    // to shorten the time, directly do it from here
    // TODO: refactor
    try {
      $date = Date::now()->format('Y-m-d H:i');
      DB::table($filterName)->where('id', '=', $id)->update(['name' => $name, 'updated_at' => $date]);
      return Response::json(array('ok' => true));
    } catch (\Exception $ex) {
      return Response::json(array('ok' => false));
    }
  }

  /**
   * [createFilter description]
   * @return [type] [description]
   */
  public function createFilter()
  {
    $filterName = Input::get('filterName');
    $name = Input::get('itemName');
    // to shorten the time, directly do it from here
    // TODO: refactor
    try {
      $date = Date::now()->format('Y-m-d H:i');
      $id = DB::table($filterName)->insertGetId(['name' => $name, 'created_at' => $date, 'updated_at' => $date]);
      return Response::json(array('ok' => true, 'id' => $id));
    } catch (\Exception $ex) {
      return Response::json(array('ok' => false));
    }
  }

}