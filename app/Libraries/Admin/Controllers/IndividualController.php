<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Libraries\Facades\SettingsProvider;

class IndividualController extends AuthBaseController {

  private $_userService;

  public function __construct()
  {
    parent::__construct();
    $this->_userService = App::make('\Libraries\Admin\Services\UserService');
  }

  /**
   * [index description]
   * @return [type] [description]
   */
  public function index() 
  {
    View::share('activeLinkKey', 'individuals.allindividuals');

    return View::make('admin.individuals.index');
  }

  /**
   * [getData description]
   * @return [type] [description]
   */
  public function getData()
  {
    // NOTE:
    // all code below is optimized for dataTables. Changing the code, ensure the integrity with the dataTables in Client-side
    // 
    $input = Input::all();

    $pageSize = (int)$input['iDisplayLength'];
    $pageNumber = ((int)$input['iDisplayStart']) / $pageSize + 1;

    // SORTING
    $sorts = array(
      array('column' => 'first_name', 'direction' => 'asc'),
      array('column' => 'last_name', 'direction' => 'asc')
    );
    if (Input::has('iSortCol_0')) {
      // assume iSortingCols = 1
      $sortingColIdx = (int)$input['iSortCol_0'];
      $sortingCol = $input['mDataProp_'.$sortingColIdx];
      $sortingDirection = $input['sSortDir_0'];
      if ($sortingCol === 'full_name') {
        $sorts = array(
          array('column' => 'first_name', 'direction' => $sortingDirection),
          array('column' => 'last_name', 'direction' => $sortingDirection)
        );
      } else {
        $sorts = array(
          array('column' => $sortingCol, 'direction' => $sortingDirection)
        );
      }
    }

    // FILTERS
    $conditions = array();
    // ------------------------------------------------------------------------------------
    // SPECIAL FILTER / SEARCH - handle OR
    // 
    // NOTE
    // Conditions algorithm: 
    // - get search string from dataTables
    // - check what columns are searchable
    // - construct and array of conditions as 'column', 'operator', 'value' in a sub_conditions, 
    //   because we need to support closure for OR based where
    $sub_conditions = array();
    $sSearch = $input['sSearch'];
    if (strlen(strval($sSearch)) > 0) {
      $iColumns = (int)$input['iColumns'];
      for ($i=0; $i < $iColumns; $i++) { 
        $mProp = $input['mDataProp_'.$i];
        $searchable = $input['bSearchable_'.$i] === 'true';
        if ($searchable) {
          $sub_conditions[] = array('column' => $mProp, 'operator' => 'LIKE', 'value' => '%'.$sSearch.'%');
        }
      }
    }
    // NOTE
    // use closure to wrap the where conditions (sub_conditions) because we need to support OR query, 
    // pass the closure to the query engine (repository)
    // PS: this thing might be similar to linq to sql
    if (count($sub_conditions) > 0) {
      $conditions[] = function ($query) use ($sub_conditions) {
        for ($i=0; $i < count($sub_conditions); $i++) { 
          $sub_condition = $sub_conditions[$i];
          // hack, special case for full_name column, normal for other columns
          if ($sub_condition['column'] === 'full_name') {
            $query->orwhere('first_name', $sub_condition['operator'], $sub_condition['value']);
            $query->orwhere('last_name', $sub_condition['operator'], $sub_condition['value']);
          } else {
            $query->orwhere($sub_condition['column'], $sub_condition['operator'], $sub_condition['value']);
          }
          // // hack, special case for date column
          // if ($sub_condition['column'] === 'created_at' || $sub_condition['column'] === 'updated_at') {
          //   $query->orwhere(function ($query2) use ($sub_condition) {
          //     $query2->whereRaw("date_format(".$sub_condition['column'].", '%d %M %Y %H:%i') ".$sub_condition['operator']." '".$sub_condition['value']."'");
          //   });
          // }
        }
      };
    }
    // END SPECIAL FILTER / SEARCH
    // ------------------------------------------------------------------------------------

    // nationality filter
    if (Input::has('nationalityFilter')) {
      $nationalityFilter = (int)$input['nationalityFilter'];
      if ($nationalityFilter > 0) {
        $conditions[] = array('column' => 'nationality', 'operator' => '=', 'value' => $nationalityFilter);
      }
    }

    // based in
    if (Input::has('basedInFilter')) {
      $basedInFilter = (int)$input['basedInFilter'];
      if ($basedInFilter > 0) {
        $conditions[] = array('column' => 'based_in', 'operator' => '=', 'value' => $basedInFilter);
      }
    }

    // member status
    if (Input::has('memberStatusFilter')) {
      $memberStatusFilter = (int)$input['memberStatusFilter'];
      if ($memberStatusFilter > 0) {
        $conditions[] = array('column' => 'member_status', 'operator' => '=', 'value' => $memberStatusFilter);
      }
    }

    // professional status
    if (Input::has('professionalStatusFilter')) {
      $professionalStatus = (int)$input['professionalStatusFilter'];
      if ($professionalStatus > 0) {
        $conditions[] = array('column' => 'professional_status', 'operator' => '=', 'value' => $professionalStatus);
      }
    }

    // membership status filter
    if (Input::has('membershipStatusFilter')) {
      $membershipStatusFilter = (int)$input['membershipStatusFilter'];
      if ($membershipStatusFilter > -1) {
        $conditions[] = array('column' => 'is_active', 'operator' => '=', 'value' => $membershipStatusFilter);
      }
    }

    // // top status filter
    // if (Input::has('topStatusFilter')) {
    //   $topStatusFilter = (int)$input['topStatusFilter'];
    //   if ($topStatusFilter > -1) {
    //     $conditions[] = array('column' => 'top_member', 'operator' => '=', 'value' => $topStatusFilter);
    //   }
    // }

    $data = array();

    $iTotalRecords = 0;
    $iTotalDisplayRecords = 0;

    if (Auth::user()->isAdmin()) {
      $data = $this->_userService->getIndividuals($pageNumber, $pageSize, $sorts, $conditions);
    } else {
      
    }
    $iTotalRecords = $this->_userService->getIndividualsCount($conditions);
    $iTotalDisplayRecords = $iTotalRecords; 

    return Response::json(array(
      'sEcho' => $input['sEcho'],
      'aaData' => $data,
      'iTotalRecords' => $iTotalRecords,
      'iTotalDisplayRecords' => $iTotalDisplayRecords
    ));
  }

  /**
   * [revokeMembership description]
   * @return [type] [description]
   */
  public function revokeMembership() 
  {
    $indId = Input::get('ind_id');

    $revoked = $this->_userService->revokeMembership($indId);

    return Response::json($revoked);
  }

  /**
   * [activateMembership description]
   * @return [type] [description]
   */
  public function activateMembership()
  {
    $indId = Input::get('ind_id');

    $activated = $this->_userService->activateMembership($indId);

    return Response::json($activated);
  }

  // public function setAsTopMember() 
  // {
  //   $indId = Input::get('ind_id');

  //   $top = $this->_userService->setTopMember($indId);

  //   return Response::json($top);
  // }

  // public function unsetFromTopMember()
  // {
  //   $indId = Input::get('ind_id');

  //   $top = $this->_userService->unsetTopMember($indId);

  //   return Response::json($top);
  // }

}