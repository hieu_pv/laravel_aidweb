<?php namespace Libraries\Admin\Controllers; 

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Libraries\Models\Blog\Post;
use Libraries\Facades\Directories;
use Libraries\Facades\Messages;
use Libraries\Facades\GenericUtility;

class AccountController extends AuthBaseController {
  
  private $_passwordService;

  public function __construct()
  {
    parent::__construct();
    $this->_passwordService = App::make('\Libraries\Admin\Services\PasswordService');
  }

  /**
   * [changePassword description]
   * @return [type] [description]
   */
  public function changePassword()
  {
    $input = Input::all();
    $validator = new \Libraries\Validators\ChangePasswordValidator();

    if ($validator->validate($input)) {
        $ret = $this->_passwordService->changePassword(Auth::user()->id, $input);
        if (!$ret->changed) {
          return Response::json(array('changed' => false, 'messages' => $ret->messages->toArray()));
        }
        return Response::json($ret);
    } else {
        return Response::json(array('changed' => false, 'messages' => $validator->errors()->toArray()));
    }
  }


  /**
   * [logout description]
   * @return [type] [description]
   */
  public function logout()
  {
    try {
      $service = App::make('\Libraries\Admin\Services\PostingService');
      $service->removePostingLimitation();
    } catch (\Exception $ex) {
      
    }

    Auth::logout();
    $prev = URL::previous();
    if (strpos($prev, '/oadmin') !== false) {
      return Redirect::to('/');
    } else {
      return Redirect::back();
    }
    //return Redirect::route('admin_login');
  }


}