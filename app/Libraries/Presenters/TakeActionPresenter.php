<?php namespace Libraries\Presenters;

use Libraries\Facades\SettingsProvider;
use Laracasts\Presenter\Presenter;

class TakeActionPresenter extends Presenter {

  public function original() 
  {
    return new \Libraries\Presenters\Original($this);
  }

  public function organisationInfo()
  {
    $info = new \Libraries\Presenters\OrganisationInfo($this);
    return $info->get();
  }

  public function teaser() 
  {
    if (strlen($this->content) > 150) {
      $s = substr($this->content, 0, 150);
      $result = substr($s, 0, strrpos($s, ' ')).'...';
      return $result;
    }
    return $this->content;
  }

}