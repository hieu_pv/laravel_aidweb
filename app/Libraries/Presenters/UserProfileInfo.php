<?php namespace Libraries\Presenters;

class UserProfileInfo {

  private $model;

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function get()
  {
    $info = new \stdClass();
    $info->id = -1;
    $info->full_name = '';
    $info->first_name = '';
    $info->last_name = '';
    $info->avatar = '/images/noimage.png';
    $info->avatar_big = '/images/noimage.png';
    $info->profile_url = '#';
    //$info->top_member = false;
    $info->employer_name = '';
    $info->about = '';
    $info->job_title = '';
    $info->based_in_name = '';
    $info->facebook_url = '#';
    $info->twitter_url = '#';
    $info->website_url = '#';
    $info->nationality_name = '';
    $info->member_status_name = '';
    $info->professional_status_name = '';

    try {
      $profile = $this->model->relatedUserProfile;
      if ($profile === null) {
        $profile = $this->model->userProfile;
      }

      if ($profile !== null && isset($profile) && !is_null($profile)) {
        $info->id = $profile->id;
        $info->full_name = $profile->fullname();
        $info->first_name = $profile->first_name;
        $info->last_name = $profile->last_name;
        
        $info->avatar = '/imagecache/r_57_c'.$profile->avatar;
        $info->avatar_big = '/imagecache/r_255_c'.$profile->avatar;

        $info->profile_url = $profile->profile_url;
        //$info->top_member = $profile->top_member;
        $info->employer_name = $profile->employer_name;
        $info->about = $profile->description;
        $info->job_title = $profile->job_title;

        if ($profile->relatedBase !== null) {
          $info->based_in_name = $profile->relatedBase->name;
        }

        $info->website_url = $profile->website_url;
        $info->facebook_url = $profile->facebook_url;
        $info->twitter_url = $profile->twitter_url;

        if ($profile->relatedNationality !== null) {
          $info->nationality_name = $profile->relatedNationality->name;
        }

        if ($profile->relatedMemberStatus !== null) {
          $info->member_status_name = $profile->relatedMemberStatus->name;
        }

        if ($profile->relatedProfessionalStatus !== null) {
          $info->professional_status_name = $profile->relatedProfessionalStatus->name;
        }
      }
    } catch (\Exception $ex) {
    }

    return $info;
  }

}
