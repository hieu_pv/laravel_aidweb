<?php namespace Libraries\Presenters;

class OrganisationInfo {

  private $model;

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function get()
  {
    $info = new \stdClass();
    $info->id = -1;
    $info->name = '';
    $info->org_name_acronym = '';
    $info->use_org_name_acronym = '';
    $info->logo = '/images/noimage.png';
    $info->logo_big = '/images/noimage.png';
    $info->profile_url = '#';
    $info->type = '';
    $info->type_full_name = '';
    //$info->top_agency = false;
    $info->about = '';
    $info->office_location_name = '';
    $info->office_type_name = '';
    $info->facebook_url = '#';
    $info->twitter_url = '#';
    $info->website_url = '#';

    try {
      $org = $this->model->relatedOrganisation;
      if ($org === null) {
        $org = $this->model->organisation;
      }

      if ($org !== null && isset($org) && !is_null($org)) {
        $info->id = $org->id;

        $info->name = $org->org_name;
        $info->org_name_acronym = $org->org_name_acronym;
        $info->use_org_name_acronym = $org->use_org_name_acronym;

        $info->logo = '/imagecache/r_57_c'.$org->org_logo;
        $info->logo_big = '/imagecache/r_255_c'.$org->org_logo;

        $info->profile_url = $org->profile_url;

        if ($org->relatedOrganisationType !== null) {
          $info->type = $org->relatedOrganisationType->display_name;
          $info->type_full_name = $org->relatedOrganisationType->name;
        }

        //$info->top_agency = $org->top_agency;
        $info->about = $org->description;

        $info->facebook_url = $org->facebook_url;
        $info->twitter_url = $org->twitter_url;
        $info->website_url = $org->org_website;

        if ($org->relatedOfficeLocation !== null) {
          $info->office_location_name = $org->relatedOfficeLocation->name;
        }

        if ($org->relatedOfficeType !== null) {
          $info->office_type_name = $org->relatedOfficeType->name;
        }
      }
    } catch (\Exception $ex) {
      
    }

    return $info;
  }

}
