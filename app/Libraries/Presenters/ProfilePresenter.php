<?php namespace Libraries\Presenters;

use Libraries\Facades\PermalinkEngine;
use Libraries\Facades\Modules;
use Libraries\Facades\SettingsProvider;
use Laracasts\Presenter\Presenter;

class ProfilePresenter extends Presenter {

  public function userProfileInfo()
  {
    $info = new \Libraries\Presenters\UserProfileInfo($this);
    return $info->get();
  }

  public function organisationInfo()
  {
    $info = new \Libraries\Presenters\OrganisationInfo($this);
    return $info->get();
  }

}
