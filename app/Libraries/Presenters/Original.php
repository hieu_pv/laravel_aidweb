<?php namespace Libraries\Presenters;

class Original {

  private $model;

  public function __construct($model)
  {
    $this->model = $model;
  }

  public function region()
  {
    $original = 0;
    if (!is_null($this->model->regions)) {
      $c = count($this->model->regions);
      if ($c > 0) {
        $original = $this->model->regions[0]->id;
      }
    }
    return $original;
  }

  public function country()
  {
    $original = 0;
    if (!is_null($this->model->countries)) {
      $c = count($this->model->countries);
      if ($c > 0) {
        $original = $this->model->countries[0]->id;
      }
    }
    return $original;
  }

  public function crisis()
  {
    $original = 0;
    if (!is_null($this->model->crisis)) {
      $c = count($this->model->crisis);
      if ($c > 0) {
        $original = $this->model->crisis[0]->id;
      }
    }
    return $original;
  }

  public function sector()
  {
    $original = 0;
    if (!is_null($this->model->sectors)) {
      $c = count($this->model->sectors);
      if ($c > 0) {
        $original = $this->model->sectors[0]->id;
      }
    }
    return $original;
  }

  public function theme()
  {
    $original = 0;
    if (!is_null($this->model->themes)) {
      $c = count($this->model->themes);
      if ($c > 0) {
        $original = $this->model->themes[0]->id;
      }
    }
    return $original;
  }

  public function intervention()
  {
    $original = 0;
    if (!is_null($this->model->interventions)) {
      $c = count($this->model->interventions);
      if ($c > 0) {
        $original = $this->model->interventions[0]->id;
      }
    }
    return $original;
  }

  public function beneficiary()
  {
    $original = 0;
    if (!is_null($this->model->beneficiaries)) {
      $c = count($this->model->beneficiaries);
      if ($c > 0) {
        $original = $this->model->beneficiaries[0]->id;
      }
    }
    return $original;
  }

}