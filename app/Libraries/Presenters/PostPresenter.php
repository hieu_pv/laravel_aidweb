<?php namespace Libraries\Presenters;

use Libraries\Facades\PermalinkEngine;
use Libraries\Facades\Modules;
use Libraries\Facades\SettingsProvider;
use Laracasts\Presenter\Presenter;

class PostPresenter extends Presenter {

  public function original() 
  {
    return new \Libraries\Presenters\Original($this);
  }

  public function userProfileInfo()
  {
    $info = new \Libraries\Presenters\UserProfileInfo($this);
    return $info->get();
  }

  public function timeAgo() 
  {
    return \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
  }

  public function viewUrl()
  {
    $url = PermalinkEngine::getPermalink(Modules::blog(), $this);
    return url($url);
  }

  public function teaser() 
  {
    if (strlen($this->content) > 150) {
      $s = substr($this->content, 0, 150);
      $result = substr($s, 0, strrpos($s, ' ')).'...';
      return $result;
    }
    return $this->content;
  }

  public function displayDateInDetails()
  {
    return \Carbon\Carbon::parse($this->created_at)->format('jS F Y');
  }

}