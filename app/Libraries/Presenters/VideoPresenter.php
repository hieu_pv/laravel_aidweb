<?php namespace Libraries\Presenters;

use Libraries\Facades\SettingsProvider;
use Laracasts\Presenter\Presenter;

class VideoPresenter extends Presenter {

  public function original() 
  {
    return new \Libraries\Presenters\Original($this);
  }

  public function organisationInfo()
  {
    $info = new \Libraries\Presenters\OrganisationInfo($this);
    return $info->get();
  }

  public function timeAgo() 
  {
    return \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
  }

  public function teaser() 
  {
    if (strlen($this->description) > 150) {
      $s = substr($this->description, 0, 150);
      $result = substr($s, 0, strrpos($s, ' ')).'...';
      return $result;
    }
    return $this->description;
  }

}