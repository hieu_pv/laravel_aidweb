<?php namespace Libraries\Presenters;

use Laracasts\Presenter\Presenter;
use Libraries\Facades\Modules;
use Libraries\Facades\PermalinkEngine;
use Libraries\Facades\SettingsProvider;
use Libraries\Facades\GenericUtility;

class NewsItemPresenter extends Presenter {

  public function original() 
  {
    return new \Libraries\Presenters\Original($this);
  }

  public function organisationInfo()
  {
    $info = new \Libraries\Presenters\OrganisationInfo($this);
    return $info->get();
  }

  public function viewUrl()
  {
    $url = PermalinkEngine::getPermalink(Modules::news(), $this);
    return url($url);
  }

  public function timeAgo() 
  {
    return \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
  }

  public function teaser() 
  {
    if (strlen($this->content) > 150) {
      $s = substr($this->content, 0, 150);
      $result = substr($s, 0, strrpos($s, ' ')).'...';
      return $result;
    }
    return $this->content;
  }

  public function displayDateInDetails()
  {
    return \Carbon\Carbon::parse($this->created_at)->format('jS F Y');
  }

}