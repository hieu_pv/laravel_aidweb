<?php namespace Libraries\Presenters;

use Libraries\Facades\SettingsProvider;
use Laracasts\Presenter\Presenter;

class PublicityPresenter extends Presenter {

  public function original() 
  {
    return new \Libraries\Presenters\Original($this);
  }

  public function organisationInfo()
  {
    $info = new \Libraries\Presenters\OrganisationInfo($this);
    return $info->get();
  }

}