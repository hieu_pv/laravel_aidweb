<?php namespace Libraries\Models\Lookups;

class ProfessionalStatus extends \Libraries\Models\BaseModel
{
  protected $table = 'professional_statuses';
}