<?php namespace Libraries\Models\TakeActions;

use Laracasts\Presenter\PresentableTrait;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class TakeAction extends \Libraries\Models\BaseModel {

  use PresentableTrait;

  protected $presenter = '\Libraries\Presenters\NewsItemPresenter';

  protected $table = 'take_actions';

  protected $appends = ['author', 'action_type_name', 'sector_name'];

  public function relatedOrganisation()
  {
    return $this->belongsTo('\Libraries\Models\Organisation', 'organisation_id', 'id');
  }

  public function typeOfTakeAction()
  {
    return $this->belongsTo('\Libraries\Models\TakeActions\Type', 'type_id', 'id');
  }

  public function getActionTypeNameAttribute()
  {
    $o = $this->typeOfTakeAction;
    if ($o !== null && isset($o) && !is_null($o)) {
      return $o->name;
    }
    return '';
  }

  public function getAuthorAttribute()
  {
    $org = $this->relatedOrganisation;
    if ($org !== null && isset($org) && !is_null($org)) {
      return $org->org_name;
    }
    return 'N/A';
  }

  public function getSectorNameAttribute()
  {
    $sectors = $this->sectors;
    if ($sectors !== null && !is_null($sectors)) {
      $c = count($sectors);
      if ($c > 0) {
        return $sectors[0]->name;
      }
    }
    return '-';
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function getUpdatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setUpdatedAtAttribute($value)
  {
    $this->attributes['updated_at'] = $value;
  }

  public function countries() 
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Country', 'take_actions_countries', 'takeaction_id', 'country_id');
  }

  public function regions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Region', 'take_actions_regions', 'takeaction_id', 'region_id');
  }

  public function crisis()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Crisis', 'take_actions_crisis', 'takeaction_id', 'crisis_id');
  }

  public function sectors()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Sector', 'take_actions_sectors', 'takeaction_id', 'sector_id');
  }

  public function themes()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Theme', 'take_actions_themes', 'takeaction_id', 'theme_id');
  }

  public function interventions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Intervention', 'take_actions_interventions', 'takeaction_id', 'intervention_id');
  }

  public function beneficiaries()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Beneficiary', 'take_actions_beneficiaries', 'takeaction_id', 'beneficiary_id');
  } 

}