<?php namespace Libraries\Models;

use Jenssegers\Date\Date;
use Carbon\Carbon;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;

class Organisation extends \Illuminate\Database\Eloquent\Model {

  protected $table = 'organisations';

  protected $appends = ['profile_url', 'related_organisation_type_name', 'related_office_location_name'];

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function relatedOfficeLocation()
  {
    return $this->belongsTo('\Libraries\Models\Filters\Country', 'office_location');
  }

  public function relatedOfficeType()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\OfficeType', 'office_type');
  }

  public function relatedOrganisationType()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\OrganisationType', 'org_type');
  }

  public function relatedLevelOfActivity()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\LevelOfActivity', 'level_of_activity');
  }

  public function getRelatedOrganisationTypeNameAttribute()
  {
    $type = $this->relatedOrganisationType;
    if ($type !== null && isset($type) && !is_null($type)) {
      return $type->name;
    }
    return '';
  }

  public function getRelatedOfficeLocationNameAttribute()
  {
    $ol = $this->relatedOfficeLocation;
    if ($ol !== null && isset($ol) && !is_null($ol)) {
      return $ol->name;
    }
    return '';
  }

  public function getProfileUrlAttribute()
  {
    return PermalinkEngine::resolveProfileUrl($this->id, Modules::PROFILE_TYPE_ORGANISATION);
  }

  public function NewsItem() {
    return $this->hasMany('\Libraries\Models\News\NewsItem', 'organisation_id');
  }

}