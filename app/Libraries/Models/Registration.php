<?php namespace Libraries\Models;

use Jenssegers\Date\Date;
use Carbon\Carbon;

class Registration extends \Illuminate\Database\Eloquent\Model {

  protected $table = 'registrations';

  public $timestamps = false;

}