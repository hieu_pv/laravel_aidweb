<?php namespace Libraries\Models;

use Jenssegers\Date\Date;
use Carbon\Carbon;
use Libraries\Facades\PermalinkEngine;
use Libraries\Support\Modules;

class UserProfile extends \Illuminate\Database\Eloquent\Model {

  protected $table = 'user_profiles';
  protected $appends = ['full_name', 'profile_url', 'related_employer_organisation_type_name'];

  public function relatedNationality()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\Nationality', 'nationality');
  }

  public function relatedBase()
  {
    return $this->belongsTo('\Libraries\Models\Filters\Country', 'based_in');
  }

  public function relatedMemberStatus()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\MemberStatus', 'member_status');
  }

  public function relatedProfessionalStatus()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\ProfessionalStatus', 'professional_status');
  }

  public function relatedLevelOfResponsibility()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\LevelOfResponsibility', 'level_of_responsibility');
  }

  public function relatedEmployerOrganisationType()
  {
    return $this->belongsTo('\Libraries\Models\Lookups\OrganisationType', 'employer_organisation_type');
  }

  public function getRelatedEmployerOrganisationTypeNameAttribute()
  {
    $type = $this->relatedEmployerOrganisationType;
    if ($type !== null && isset($type) && !is_null($type)) {
      return $type->name;
    }
    return '';
  }

  public function fullname()
  {
    return trim($this->first_name.' '.$this->last_name);
  }

  public function getFullNameAttribute()
  {
    return $this->fullname();
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function getProfileUrlAttribute()
  {
    return PermalinkEngine::resolveProfileUrl($this->id, Modules::PROFILE_TYPE_INDIVIDUAL);
  }

}