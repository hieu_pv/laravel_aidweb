<?php namespace Libraries\Models\Blog;

use Laracasts\Presenter\PresentableTrait;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class Post extends \Libraries\Models\BaseModel {

  use PresentableTrait;

  protected $presenter = '\Libraries\Presenters\PostPresenter';

  protected $table = 'blog_posts';

  //protected $hidden = array('content');
  protected $appends = array('author');

  public function getAuthorAttribute()
  {
    $usrpro = $this->relatedUserProfile;
    if ($usrpro !== null && isset($usrpro) && !is_null($usrpro)) {
      return $usrpro->fullname();
    }
    return 'N/A';
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function getUpdatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setUpdatedAtAttribute($value)
  {
    $this->attributes['updated_at'] = $value;
  }

  public function countries() 
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Country', 'blog_posts_countries', 'post_id', 'country_id');
  }

  public function regions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Region', 'blog_posts_regions', 'post_id', 'region_id');
  }

  public function crisis()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Crisis', 'blog_posts_crisis', 'post_id', 'crisis_id');
  }

  public function sectors()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Sector', 'blog_posts_sectors', 'post_id', 'sector_id');
  }

  public function themes()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Theme', 'blog_posts_themes', 'post_id', 'theme_id');
  }

  public function interventions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Intervention', 'blog_posts_interventions', 'post_id', 'intervention_id');
  }

  public function beneficiaries()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Beneficiary', 'blog_posts_beneficiaries', 'post_id', 'beneficiary_id');
  } 

  public function relatedUserProfile()
  {
    return $this->belongsTo('\Libraries\Models\UserProfile', 'user_profile_id', 'id');
  }

}