<?php namespace Libraries\Models;

use Libraries\Facades\FileUtility;

class FileModel 
{

  public $fileId = 0;
  public $fileName = '';
  public $fileExtension = '';
  public $mimeType = '';
  public $url = '';
  private $directoryRelativePath = '';
  private $filePath = '';
  public $fileSize = 0;
  public $imageSize = array();

  // either Images, Audio, Video, Documents
  public $fileType = ''; 

  public function __construct($directoryRelativePath, $fileName)
  {
    $this->fileId = mt_rand();
    $this->fileName = $fileName;
    $this->setExtension();
    $this->setFileType();
    $this->directoryRelativePath = $directoryRelativePath;
    // refresh on name change
    $this->setUrl();
    // refresh on name change
    $this->setFilePath();
    clearstatcache();
    $this->fileSize = FileUtility::formatSizeUnits(filesize($this->filePath));
    clearstatcache();
    if ($this->fileType === 'images') {
      $this->imageSize = getimagesize($this->filePath);
    }
  }

  public function changeFileName($newName)
  {
    $this->fileName = $newName;
    // refresh on name change
    $this->setUrl();
    // refresh on name change
    $this->setFilePath();
  }

  private function setFilePath()
  {
    $this->filePath = $_SERVER['DOCUMENT_ROOT'].'/'.ltrim(rtrim($this->directoryRelativePath, '/'), '/').'/'.$this->fileName;
  }

  private function setUrl()
  {
    $this->url = '/'.ltrim(rtrim($this->directoryRelativePath, '/'), '/').'/'.$this->fileName;
  }

  private function setExtension()
  {
    $split = explode('.', $this->fileName);
    
    $c = count($split);

    $fileExtension = $split[$c - 1];
    $loweredFileExtension = strtolower($fileExtension);

    $this->fileExtension = $loweredFileExtension;
  }
  
  private function setFileType()
  {
    switch ($this->fileExtension) {
      case 'jpg':
      case 'jpeg':
      case 'png':
      case 'gif':
      case 'tiff':
      case 'tif':
        $this->fileType = 'images';
        break;
      case 'doc':
      case 'docx':
      case 'pdf':
        $this->fileType = 'documents';
        break;
      case 'mp3':
        $this->fileType = 'audio';
        break;
      case 'flv':
        $this->fileType = 'video';
        break;
      default:
        $this->fileType = 'folder';
        break;
    }
  }
  
}