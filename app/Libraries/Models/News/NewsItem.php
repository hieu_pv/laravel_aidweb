<?php namespace Libraries\Models\News;

use Laracasts\Presenter\PresentableTrait;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class NewsItem extends \Libraries\Models\BaseModel {

  use PresentableTrait;

  protected $presenter = '\Libraries\Presenters\NewsItemPresenter';

  protected $table = 'news_items';

  protected $appends = array('author', 'sector_name');

  public function getAuthorAttribute()
  {
    $org = $this->relatedOrganisation;
    if ($org !== null && isset($org) && !is_null($org)) {
      return $org->org_name;
    }
    return 'N/A';
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function getUpdatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setUpdatedAtAttribute($value)
  {
    $this->attributes['updated_at'] = $value;
  }

  public function getSectorNameAttribute()
  {
    $sectors = $this->sectors;
    if ($sectors !== null && !is_null($sectors)) {
      $c = count($sectors);
      if ($c > 0) {
        return $sectors[0]->name;
      }
    }
    return '-';
  }

  public function countries() 
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Country', 'news_items_countries', 'newsitem_id', 'country_id');
  }

  public function regions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Region', 'news_items_regions', 'newsitem_id', 'region_id');
  }

  public function crisis()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Crisis', 'news_items_crisis', 'newsitem_id', 'crisis_id');
  }

  public function sectors()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Sector', 'news_items_sectors', 'newsitem_id', 'sector_id');
  }

  public function themes()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Theme', 'news_items_themes', 'newsitem_id', 'theme_id');
  }

  public function interventions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Intervention', 'news_items_interventions', 'newsitem_id', 'intervention_id');
  }

  public function beneficiaries()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Beneficiary', 'news_items_beneficiaries', 'newsitem_id', 'beneficiary_id');
  } 

  public function relatedOrganisation()
  {
    return $this->belongsTo('\Libraries\Models\Organisation', 'organisation_id', 'id');
  }

}