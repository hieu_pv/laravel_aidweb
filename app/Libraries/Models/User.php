<?php namespace Libraries\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Libraries\Models\UserProfile;
use Libraries\Models\Organisation;
use Libraries\Support\Modules;
use Laracasts\Presenter\PresentableTrait;

class User extends \Illuminate\Database\Eloquent\Model implements UserInterface, RemindableInterface {

  use UserTrait, RemindableTrait, PresentableTrait;

  protected $presenter = '\Libraries\Presenters\ProfilePresenter';

  protected $table = 'users';

  protected $hidden = array('password', 'remember_token');
  protected $appends = array('entityName', 'profile_url', 'image');

  public function isIndividual()
  {
    return $this->profile_type === Modules::PROFILE_TYPE_INDIVIDUAL;
  }

  public function isOrganisation()
  {
    return $this->profile_type === Modules::PROFILE_TYPE_ORGANISATION;
  }

  public function isAdmin()
  {
    return $this->profile_type === Modules::PROFILE_TYPE_ADMIN;
  }

  public function getTypeTextAttribute()
  {
    if ($this->isOrganisation()) {
      return 'organisation';
    } else if ($this->isIndividual()) {
      return 'Individual';
    }
    return '';
  }

  public function userProfile()
  {
    return $this->hasOne('\Libraries\Models\UserProfile', 'user_id', 'id');
  }

  public function organisation()
  {
    return $this->hasOne('\Libraries\Models\Organisation', 'cp_id', 'id');
  }

  
  public function sanitize()
  {
    $this->attributes['password'] = '';
    $this->original['password'] = '';
  }

  public function getEntityNameAttribute()
  {
    if ($this->isOrganisation()) {
      $org = $this->organisation;
      if ($org !== null && isset($org) && !is_null($org)) {
        return $org->org_name;
      }
    }

    if ($this->isIndividual()) {
      $usrpro = $this->userProfile;
      if ($usrpro !== null && isset($usrpro) && !is_null($usrpro)) {
        return $usrpro->fullname();
      }
    }

    return 'N/A';
  }

  public function getProfileUrlAttribute()
  {
    if ($this->isOrganisation()) {
      $org = $this->organisation;
      if ($org !== null && isset($org) && !is_null($org)) {
        return $org->profile_url;
      }
    }

    if ($this->isIndividual()) {
      $usrpro = $this->userProfile;
      if ($usrpro !== null && isset($usrpro) && !is_null($usrpro)) {
        return $usrpro->profile_url;
      }
    }

    return '#';
  }

  public function getImageAttribute()
  {
    if ($this->isOrganisation()) {
      $org = $this->organisation;
      if ($org !== null && isset($org) && !is_null($org)) {
        return $org->org_logo;
      }
    }

    if ($this->isIndividual()) {
      $usrpro = $this->userProfile;
      if ($usrpro !== null && isset($usrpro) && !is_null($usrpro)) {
        return $usrpro->avatar;
      }
    }

    return '/images/noimage.png';
  }

}
