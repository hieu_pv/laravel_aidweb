<?php namespace Libraries\Models\Publicities;

use Laracasts\Presenter\PresentableTrait;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Libraries\Facades\GenericUtility;

class Publicity extends \Libraries\Models\BaseModel {

  use PresentableTrait;

  protected $presenter = '\Libraries\Presenters\PublicityPresenter';

  protected $table = 'publicities';

  protected $appends = ['author', 'num_of_months', 'selected_platforms', 'publicly_published', 'overdue', 'upcoming', 'created_at2'];

  public function getAuthorAttribute()
  {
    $org = $this->relatedOrganisation;
    if ($org !== null && isset($org) && !is_null($org)) {
      return $org->org_name;
    }
    return 'N/A';
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function getCreatedAt2Attribute($value)
  {
    return Carbon::parse($value)->format('d-m-Y');
  }

  public function getUpdatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setUpdatedAtAttribute($value)
  {
    $this->attributes['updated_at'] = $value;
  }

  public function getPublishedAtAttribute($value)
  {
    if ($value !== null) {
      return Carbon::parse($value)->format('d M Y H:i');
    } else {
      return 'N/A';
    }
  }

  public function setPublishedAtAttribute($value)
  {
    $this->attributes['published_at'] = $value;
  }

  public function getPublishedStartDateAttribute($value) 
  {
    if ($value !== null && $value !== '0000-00-00 00:00:00') {
      return Carbon::parse($value)->format('d-m-Y');
    } else {
      return Carbon::now()->format('d-m-Y');
    }
  }

  public function setPublishedStartDateAttribute($value)
  {
    $str = Carbon::createFromFormat('d-m-Y', $value)->toDateString();
    $this->attributes['published_start_date'] = $str;
  }

  public function typeOfPublicity()
  {
    return $this->belongsTo('\Libraries\Models\Publicities\Type', 'type_id', 'id');
  }

  public function relatedOrganisation()
  {
    return $this->belongsTo('\Libraries\Models\Organisation', 'organisation_id', 'id');
  }

  public function getselectedPlatformsAttribute()
  {
    $platforms = DB::table('publicities_platforms')->where('publicity_id', $this->id)->lists('platform_id');
    // if (empty($platforms)) {
    //   return [];
    // }
    return $platforms;
  }

  public function getNumOfMonthsAttribute()
  {
    $interval = GenericUtility::parseIso8601Interval($this->published_interval);
    if ($interval === null) {
      
    }
    return $interval->Months;
  }

  public function getPubliclyPublishedAttribute()
  {
    $startDate = Carbon::parse($this->attributes['published_start_date']);
    $endDate = Carbon::parse($this->attributes['published_start_date'])->addMonths($this->num_of_months);
    $inInterval = Carbon::now()->between($startDate, $endDate);
    return $this->published && $inInterval;
  }

  public function getOverdueAttribute()
  {
    $startDate = Carbon::parse($this->attributes['published_start_date']);
    $endDate = Carbon::parse($this->attributes['published_start_date'])->addMonths($this->num_of_months);
    $due = Carbon::now()->gt($startDate) && Carbon::now()->gt($endDate);
    return $due;
  }

  public function getUpcomingAttribute()
  {
    $startDate = Carbon::parse($this->attributes['published_start_date']);
    $due = Carbon::now()->lt($startDate);
    return $due;
  }

}