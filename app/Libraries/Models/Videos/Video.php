<?php namespace Libraries\Models\Videos;

use Laracasts\Presenter\PresentableTrait;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class Video extends \Libraries\Models\BaseModel {

  use PresentableTrait;

  protected $presenter = '\Libraries\Presenters\VideoPresenter';

  protected $table = 'videos';

  protected $appends = ['organisation_name', 'displayed_duration', 'sector_name'];

  public function relatedOrganisation()
  {
    return $this->belongsTo('\Libraries\Models\Organisation', 'organisation_id', 'id');
  }

  public function getOrganisationNameAttribute()
  {
    $org = $this->relatedOrganisation;
    if ($org !== null && isset($org) && !is_null($org)) {
      return $org->org_name;
    }
    return 'N/A';
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setCreatedAtAttribute($value)
  {
    $this->attributes['created_at'] = $value;
  }

  public function getUpdatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d M Y H:i');
  }

  public function setUpdatedAtAttribute($value)
  {
    $this->attributes['updated_at'] = $value;
  }

  public function getSectorNameAttribute()
  {
    $sectors = $this->sectors;
    if ($sectors !== null && !is_null($sectors)) {
      $c = count($sectors);
      if ($c > 0) {
        return $sectors[0]->name;
      }
    }
    return '-';
  }

  private function getYoutubeId() 
  {
    $url = $this->url;
    $query_string = array();
    parse_str(parse_url($url, PHP_URL_QUERY), $query_string);
    $yid = $query_string["v"];
    return $yid;
  }

  private function isYoutubeVideo()
  {
    $youtubeRegex = '/\b(:\/\/www\.youtube\.com\/|:\/\/youtube\.com\/)\b/i';
    $isYoutubeVideo = preg_match($youtubeRegex, $this->url);
    return $isYoutubeVideo;
  }

  public function setVideoDetailsBasedOnUrl()
  {
    $isYoutubeVideo = $this->isYoutubeVideo();
    
    if ($isYoutubeVideo) {
      $this->youtube_id = $this->getYoutubeId();
      $this->image = 'http://img.youtube.com/vi/'.$this->youtube_id.'/hqdefault.jpg';

      // try get details 
      try {
        $apikey = Config::get('apis.google_browser_key');
        $vUrl = 'https://content.googleapis.com/youtube/v3/videos?part=contentDetails&id='.$this->getYoutubeId().'&key='.$apikey;
        $data = file_get_contents($vUrl);
        $jdata = json_decode($data);

        $duration = $jdata->items[0]->contentDetails->duration;

        $this->duration = $duration; // in ISO_8601#Durations, https://en.wikipedia.org/wiki/ISO_8601#Durations
      } catch (\Exception $ex) {

      }
    }
  }

  public function getDisplayedDurationAttribute()
  {
    if ($this->duration !== null) {

      // TIME ISO_8601#Durations regex
      $pattern = '/^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?$/';
      $matches = array();
      $match = preg_match_all($pattern, $this->duration, $matches);
      
      if ($match) {
        // get matches
        $matches2 = array_map(function ($m) {
          if ($m[0] !== '') {
            return $m[0];
          }
        }, $matches);
        // get except first element, first element should be "PTxxxx"
        $matches3 = array_slice($matches2, 1);
        // concat to one string
        foreach ($matches3 as $key => $value) {
          if ($matches3[$key] === null) {
            //$matches3[$key] = '00';
            continue;
          }
          $matches3[$key] = sprintf('%02d', $matches3[$key]);
        }
        $d = implode(':', $matches3);
        $d = trim($d, ':');
        if (strlen($d) === 2) {
          $d = '00:'.$d;
        }
        return $d;
      }

    }
    return '';
  }

  public function embedUrl()
  {
    $isYoutubeVideo = $this->isYoutubeVideo();
    
    if ($isYoutubeVideo) {
      return 'https://www.youtube.com/embed/'.$this->getYoutubeId();
    }

    return $this->url;
  }

  public function watchUrl()
  {
    $isYoutubeVideo = $this->isYoutubeVideo();
    
    if ($isYoutubeVideo) {
      return 'https://www.youtube.com/watch?v='.$this->getYoutubeId();
    }

    return $this->url;
  }


  public function countries() 
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Country', 'videos_countries', 'video_id', 'country_id');
  }

  public function regions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Region', 'videos_regions', 'video_id', 'region_id');
  }

  public function crisis()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Crisis', 'videos_crisis', 'video_id', 'crisis_id');
  }

  public function sectors()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Sector', 'videos_sectors', 'video_id', 'sector_id');
  }

  public function themes()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Theme', 'videos_themes', 'video_id', 'theme_id');
  }

  public function interventions()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Intervention', 'videos_interventions', 'video_id', 'intervention_id');
  }

  public function beneficiaries()
  {
    return $this->belongsToMany('\Libraries\Models\Filters\Beneficiary', 'videos_beneficiaries', 'video_id', 'beneficiary_id');
  } 


}