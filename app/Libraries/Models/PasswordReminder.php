<?php namespace Libraries\Models;

use Carbon\Carbon;

class PasswordReminder extends \Libraries\Models\BaseModel {

  protected $table = 'password_reminders';

  protected $appends = array('is_expired');

  public function setUpdatedAtAttribute($value)
  {
  // Do nothing.
  }

  public function getIsExpiredAttribute()
  {
    $age = Carbon::now()->diffInMinutes($this->created_at);

    return $age > 60;
  }

}